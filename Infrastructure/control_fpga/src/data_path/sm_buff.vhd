
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

entity sm_buff IS
   PORT( 
      almost_full    : in    std_logic;
      pe             : in    std_logic;
      rxclk          : in    std_logic;
      rxdata         : in    std_logic_vector (31 DOWNTO 0);
      ctrl_data      : in    std_logic_vector (7 DOWNTO 0);
      data_fifo      : out   std_logic_vector (35 DOWNTO 0);
      reset          : in    std_logic;
      wr_fifo        : out   std_logic
   );

end entity sm_buff ;

 
architecture fsm OF sm_buff IS

   constant k281 :unsigned( 7 downto 0):= x"7c";     --! start of frame 
   constant k285 :unsigned( 7 downto 0):= x"bc";    --! idle state 
   constant k286 :unsigned( 7 downto 0):= x"dc";    --! end of frame

   type state_type IS (
      idle,
      sof,
      wt,
      eof      
   );
 
  
   signal current_state : state_type;

begin

   -----------------------------------------------------------------
   clocked_proc : process ( 
      rxclk,
      reset
   )
   -----------------------------------------------------------------
   begin
      if (reset = '1') THEN
         current_state <= idle;
         -- Default Reset Values
         data_fifo <= (others => '0');
         wr_fifo   <= '0';
      elsif (rxclk'event and rxclk = '1') then

         -- Combined Actions
         CASE current_state IS
            when idle => 
               wr_fifo <= '0';
               data_fifo<= pe & "100" & rxdata;               
               if ( ctrl_data = std_logic_vector(k281)) then 
                  current_state <= sof;
               else
                  current_state <= idle;
               end if ;
            when sof => 
               wr_fifo <= '1';
               data_fifo <= pe & "001" &  rxdata;              
               current_state <= wt;              
            when wt => 
               data_fifo<= pe & "000" & rxdata;
                wr_fifo <= '1';
               if (ctrl_data = std_logic_vector(k286)) THEN 
                  current_state <= eof;
                else
                 current_state <= wt;
                end if;    
               when eof => 
               data_fifo <= pe & "010"&  rxdata;
               wr_fifo <= '1';
               if ( ctrl_data = std_logic_vector(k281))then 
                   current_state <= sof;
                else
                 current_state <= idle;
                 end if;
            when others =>
               current_state <= idle;
               data_fifo<= pe & "100" & rxdata;
        end  case;
       end if;
   end process clocked_proc;
 
end architecture fsm;
