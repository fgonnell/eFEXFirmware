
-- Pulse generator

library IEEE;
use IEEE.std_logic_1164.all;


ENTITY led_stretch IS
	PORT(
	input	: in	std_logic;
	clk	    : in	std_logic;
	reset   : in	std_logic;
	output	: out	std_logic
	);
END led_stretch ;

ARCHITECTURE rtl OF led_stretch IS

signal Reg1,Reg2		: std_logic := '0';


BEGIN
 
  process (clk)
     
    begin 
     
--      if  reset ='1' then 
--          Reg1 <= '0';
--          Reg2 <= '0';
        if rising_edge(clk) THEN
          Reg1 <= input;
          Reg2 <= Reg1;
      end if;
      
      end process; 
      
output <= Reg2; --Reg1 and not Reg2;
      


END rtl;



