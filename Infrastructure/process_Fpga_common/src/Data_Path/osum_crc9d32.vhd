library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- 1+x^1+x^3+x^4+x^5+x^6+x^7+x^9;

entity osum_crc9d32 is
    generic(
        REVERSE_BIT_ORDER : boolean := false);
    port(
        clock : in std_logic;
        crc_start : in std_logic;
        d_in      : in std_logic_vector(31 downto 0);
        crc_out   : out std_logic_vector(8 downto 0));

end osum_crc9d32;

architecture behavioral of osum_crc9d32 is

    signal d_in_s : std_logic_vector(31 downto 0) := (others => '0');
    signal crc_r  : std_logic_vector(8 downto 0) := (others => '0');
    signal crc_s  : std_logic_vector(8 downto 0) := (others => '0');

begin

    crc_s <= B"1_1111_1111" when crc_start = '1' else crc_r;

    process(clock)
    begin
        if clock'event and clock = '1' then
            crc_r(0) <= crc_s(0) xor crc_s(2) xor crc_s(3) xor crc_s(6) xor crc_s(8) xor d_in_s(0) xor d_in_s(2) xor d_in_s(3) xor d_in_s(5) xor d_in_s(6) xor d_in_s(7) xor d_in_s(8) xor d_in_s(9) xor d_in_s(10) xor d_in_s(11) xor d_in_s(15) xor d_in_s(18) xor d_in_s(19) xor d_in_s(20) xor d_in_s(21) xor d_in_s(22) xor d_in_s(23) xor d_in_s(25) xor d_in_s(26) xor d_in_s(29) xor d_in_s(31);
            crc_r(1) <= crc_s(1) xor crc_s(2) xor crc_s(4) xor crc_s(6) xor crc_s(7) xor crc_s(8) xor d_in_s(0) xor d_in_s(1) xor d_in_s(2) xor d_in_s(4) xor d_in_s(5) xor d_in_s(12) xor d_in_s(15) xor d_in_s(16) xor d_in_s(18) xor d_in_s(24) xor d_in_s(25) xor d_in_s(27) xor d_in_s(29) xor d_in_s(30) xor d_in_s(31);
            crc_r(2) <= crc_s(2) xor crc_s(3) xor crc_s(5) xor crc_s(7) xor crc_s(8) xor d_in_s(1) xor d_in_s(2) xor d_in_s(3) xor d_in_s(5) xor d_in_s(6) xor d_in_s(13) xor d_in_s(16) xor d_in_s(17) xor d_in_s(19) xor d_in_s(25) xor d_in_s(26) xor d_in_s(28) xor d_in_s(30) xor d_in_s(31);
            crc_r(3) <= crc_s(0) xor crc_s(2) xor crc_s(4) xor d_in_s(0) xor d_in_s(4) xor d_in_s(5) xor d_in_s(8) xor d_in_s(9) xor d_in_s(10) xor d_in_s(11) xor d_in_s(14) xor d_in_s(15) xor d_in_s(17) xor d_in_s(19) xor d_in_s(21) xor d_in_s(22) xor d_in_s(23) xor d_in_s(25) xor d_in_s(27);
            crc_r(4) <= crc_s(1) xor crc_s(2) xor crc_s(5) xor crc_s(6) xor crc_s(8) xor d_in_s(0) xor d_in_s(1) xor d_in_s(2) xor d_in_s(3) xor d_in_s(7) xor d_in_s(8) xor d_in_s(12) xor d_in_s(16) xor d_in_s(19) xor d_in_s(21) xor d_in_s(24) xor d_in_s(25) xor d_in_s(28) xor d_in_s(29) xor d_in_s(31);
            crc_r(5) <= crc_s(0) xor crc_s(7) xor crc_s(8) xor d_in_s(0) xor d_in_s(1) xor d_in_s(4) xor d_in_s(5) xor d_in_s(6) xor d_in_s(7) xor d_in_s(10) xor d_in_s(11) xor d_in_s(13) xor d_in_s(15) xor d_in_s(17) xor d_in_s(18) xor d_in_s(19) xor d_in_s(21) xor d_in_s(23) xor d_in_s(30) xor d_in_s(31);
            crc_r(6) <= crc_s(0) xor crc_s(1) xor crc_s(2) xor crc_s(3) xor crc_s(6) xor d_in_s(0) xor d_in_s(1) xor d_in_s(3) xor d_in_s(9) xor d_in_s(10) xor d_in_s(12) xor d_in_s(14) xor d_in_s(15) xor d_in_s(16) xor d_in_s(21) xor d_in_s(23) xor d_in_s(24) xor d_in_s(25) xor d_in_s(26) xor d_in_s(29);
            crc_r(7) <= crc_s(0) xor crc_s(1) xor crc_s(4) xor crc_s(6) xor crc_s(7) xor crc_s(8) xor d_in_s(0) xor d_in_s(1) xor d_in_s(3) xor d_in_s(4) xor d_in_s(5) xor d_in_s(6) xor d_in_s(7) xor d_in_s(8) xor d_in_s(9) xor d_in_s(13) xor d_in_s(16) xor d_in_s(17) xor d_in_s(18) xor d_in_s(19) xor d_in_s(20) xor d_in_s(21) xor d_in_s(23) xor d_in_s(24) xor d_in_s(27) xor d_in_s(29) xor d_in_s(30) xor d_in_s(31);
            crc_r(8) <= crc_s(1) xor crc_s(2) xor crc_s(5) xor crc_s(7) xor crc_s(8) xor d_in_s(1) xor d_in_s(2) xor d_in_s(4) xor d_in_s(5) xor d_in_s(6) xor d_in_s(7) xor d_in_s(8) xor d_in_s(9) xor d_in_s(10) xor d_in_s(14) xor d_in_s(17) xor d_in_s(18) xor d_in_s(19) xor d_in_s(20) xor d_in_s(21) xor d_in_s(22) xor d_in_s(24) xor d_in_s(25) xor d_in_s(28) xor d_in_s(30) xor d_in_s(31);

        end if;
    end process;

    if_rbo_g :
    if REVERSE_BIT_ORDER generate
        din_for_g :
        for i in 0 to 31 generate
            d_in_s(i) <= d_in(31-i);
        end generate;
        crc_out_for_g :
        for i in 0 to 8 generate
            crc_out(i) <= crc_r(8-i);
        end generate;
    end generate;

    if_not_rbo_g :
    if not REVERSE_BIT_ORDER generate
        d_in_s  <= d_in;
        crc_out <= crc_r;
    end generate;

end architecture behavioral;
