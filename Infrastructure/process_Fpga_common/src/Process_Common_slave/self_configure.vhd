-- 04/06/2015
-- Created by rjs

LIBRARY ieee;
USE ieee.std_logic_1164.all;

entity self_configure is
     generic ( WBSTAR : std_logic_vector(31 downto 0) := x"00000000"); -- Warm Boot Start Address
   port(
      clk       : in     std_logic;--icap_clk,
      led_clk   : in     std_logic;
      reset     : in     std_logic;
      trigger   : in     std_logic;
      indicator : out    std_logic
   );

end self_configure ;

------------------------------------
ARCHITECTURE rtl OF self_configure IS

signal trig_pulse: std_logic := '0';

BEGIN

led: ENTITY work.led_stretch
    port map(
    input => trigger,
    clk   => led_clk,
    reset => reset,
    output => trig_pulse
    );

config: entity work.reconfig
       generic map ( WBSTAR =>   WBSTAR)-- Warm Boot Start Address
  port map(
    trigger => trig_pulse,
    sysclk => clk
    );

    indicator <= trig_pulse;

END rtl;


