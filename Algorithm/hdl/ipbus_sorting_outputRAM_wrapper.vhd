-- generalised from: ipbus_dpram
-- F. Gonnella, October 2017
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
library ipbus_lib;

use ipbus_lib.ipbus.all;
use work.DataTypes.all;
use work.AlgoDataTypes.all;

entity ipbus_sorting_outputRAM_wrapper is
  port(
    clk_ipb   : in  std_logic;
    rst       : in  std_logic;
    ipb_in    : in  ipb_wbus;
    ipb_out   : out ipb_rbus;
    SortedIn  : in  AlgoTriggerObject;
    BCIDIn    : in  BCID_t := (others => '0');
    SpyBCIDIn : in  std_logic := '0';

    rclk      : in  std_logic;  --280
    Sync      : in  std_logic;
    we        : in  std_logic := '0';
    SortedOut : out AlgoTriggerObject
    );

end ipbus_sorting_outputRAM_wrapper;

architecture rtl of ipbus_sorting_outputRAM_wrapper is

COMPONENT SortingOutputRAM
  PORT (
    clka : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    clkb : IN STD_LOGIC;
    enb : IN STD_LOGIC;
    web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addrb : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    dinb : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;

  -- algorithm signals
  signal din        : std_logic_vector(31 downto 0);
  signal q          : std_logic_vector(31 downto 0);
  signal counter    : std_logic_vector(6 downto 0) := "0000000";  --16 BCs*7TOBs
  signal BC_counter : std_logic_vector(3 downto 0) := "0000";      --16 BCs
  signal dSync      : std_logic_vector(4 downto 0);                -- input shift
  signal dOutput    : AlgoTriggerObjects(2 downto 0);              -- output shift register



  --ipbus signals
  signal ack          : std_logic;
  signal ack2         : std_logic;
  signal ipbus_write  : std_logic_vector(0 downto 0);
  signal write_enable : std_logic_vector(0 downto 0);

begin
  IPBUS_RAM : process(clk_ipb)
  begin
    if rising_edge(clk_ipb) then
      if ipb_in.ipb_strobe = '1' and ipb_in.ipb_write = '1' then
        ipbus_write(0) <= '1';
      else
        ipbus_write(0) <= '0';
      end if;
      ack2 <= ipb_in.ipb_strobe and (not ack2) and (not ack);
      ack  <= ack2;

    end if;
  end process;

  ipb_out.ipb_ack <= ack;
  ipb_out.ipb_err <= '0';

  write_enable(0) <= we;

  COUNTER_PROC : process(rclk)
  begin
    if rising_edge(rclk) then
      dSync(0)                   <= Sync;
      dSync(dSync'high downto 1) <= dSync(dSync'high-1 downto 0);

      if dSync(4) = '1' then
        BC_counter <= std_logic_vector(unsigned(BC_counter) + 1);
      end if;

      if BC_Counter = "0000" and dSync(4) = '1' then
        counter <= (others => '0');
      else
        counter <= std_logic_vector(unsigned(counter) + 1);
      end if;

      -- Output is sync'ed with next BC
      dOutput(0)                     <= to_AlgoTriggerObject(q);
      dOutput(dOutput'high downto 1) <= dOutput(dOutput'high-1 downto 0);
    end if;
  end process;

  ALGO_OUTPUT_RAM : SortingOutputRAM
    port map (
      clka  => clk_ipb,
      ena   => ipb_in.ipb_strobe,
      wea   => ipbus_write,
      addra => ipb_in.ipb_addr(6 downto 0),
      dina  => ipb_in.ipb_wdata,
      douta => ipb_out.ipb_rdata,
      clkb  => rclk,
      enb   => '1',
      web   => write_enable,
      addrb => counter,
      dinb  => din,
      doutb => q
      );

  SortedOut <= dOutput(2);
  din       <= to_LogicVector(SortedIn) when spyBCIDIn = '0' else x"f" & BCIDIn & "0" & x"00fff";

end rtl;
