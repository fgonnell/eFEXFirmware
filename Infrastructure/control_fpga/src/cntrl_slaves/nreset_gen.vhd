----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/25/2018 12:30:35 PM
-- Design Name: 
-- Module Name: nreset_gen - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;


ENTITY nreset_gen IS
   PORT( 
      clk    : IN     std_logic;
      en_rst : IN     std_logic;
      reset  : IN     std_logic;
      nreset : OUT    std_logic
   );

-- Declarations

END nreset_gen ;



 
ARCHITECTURE Behavioral OF nreset_gen IS

   -- Architecture Declarations
   signal cntr :unsigned(5 downto 0);

   TYPE STATE_TYPE IS (
      s0,
      s1,
      s2
   );
 
   -- Declare current and next state signals
   SIGNAL current_state : STATE_TYPE;
   SIGNAL next_state : STATE_TYPE;

   -- Declare any pre-registered internal signals
   SIGNAL nreset_cld : std_logic ;

BEGIN

   -----------------------------------------------------------------
   clocked_proc : PROCESS ( 
      clk,
      reset
   )
   -----------------------------------------------------------------
   BEGIN
      IF (reset = '1') THEN
         current_state <= s0;
         -- Default Reset Values
         nreset_cld <= '1';
         cntr <= (others => '0');
      ELSIF (clk'EVENT AND clk = '1') THEN
         current_state <= next_state;

         -- Combined Actions
         CASE current_state IS
            WHEN s0 => 
               cntr <= (others => '0');
               nreset_cld <='1' ;
               IF (en_rst ='1') THEN 
                  nreset_cld <= '0';
               END IF;
            WHEN s1 => 
               if (cntr < 32) then
                    cntr <= cntr +1;
               end if;
            WHEN s2 => 
               nreset_cld <='1' ;
            WHEN OTHERS =>
               NULL;
         END CASE;
      END IF;
   END PROCESS clocked_proc;
 
   -----------------------------------------------------------------
   nextstate_proc : PROCESS ( 
      cntr,
      current_state,
      en_rst
   )
   -----------------------------------------------------------------
   BEGIN
      CASE current_state IS
         WHEN s0 => 
            IF (en_rst ='1') THEN 
               next_state <= s1;
            ELSE
               next_state <= s0;
            END IF;
         WHEN s1 => 
            IF (cntr = 32 and en_rst ='0') THEN 
               next_state <= s2;
            ELSE
               next_state <= s1;
            END IF;
         WHEN s2 => 
            next_state <= s0;
         WHEN OTHERS =>
            next_state <= s0;
      END CASE;
   END PROCESS nextstate_proc;
 
   -- Concurrent Statements
   -- Clocked output assignments
   nreset <= nreset_cld;


end Behavioral;
