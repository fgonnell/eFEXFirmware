--! @file
--! @brief 32b register read only
--! @details 
--! \verbatim
--! This is 32b register, which is read only cable through the IPBus.
--! \endverbatim
--! @author Saeed Taghavi
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library ipbus_lib;
use ieee.numeric_std.all;
use ipbus_lib.ipbus.all;

--! @copydoc ipbus_reg_rdout.vhd
entity ipbus_reg_rdout is
	generic(addr_width: natural := 0);
	port(
	    --! IPBus Clock input
		ipb_clk : in std_logic;
		--! IPBus Reset input
		ipb_rst : in std_logic;
		--! IPBus input bus going from master to slaves
		ipbus_in: in ipb_wbus;
		--! IPBus output bus going from slaves to master
		ipbus_out: out ipb_rbus;
		--! 32-bit input of the register which can only be read via IPBus 
		d : in STD_LOGIC_VECTOR(31 downto 0)
	);
	
end ipbus_reg_rdout;

--! @copydoc ipbus_reg_rdout.vhd
architecture rtl of ipbus_reg_rdout is

	type reg_array is array(2**addr_width-1 downto 0) of std_logic_vector(31 downto 0);
	signal reg: reg_array;
	signal sel: integer range 0 to 2 ** ADDR_WIDTH - 1 := 0;
	signal ack: std_logic;

begin

	sel <= to_integer(unsigned(ipbus_in.ipb_addr(addr_width - 1 downto 0))) when addr_width > 0 else 0;

	process(ipb_clk)
	begin
		if rising_edge(ipb_clk) then
			if ipb_rst = '1' then
				reg <= (others=>(others=>'0'));
			elsif ipbus_in.ipb_strobe='1' and ipbus_in.ipb_write='1' then
			       ipbus_out.ipb_rdata <= (others =>'0');
                end if;
                         
                  if   sel = 0 then
                       ipbus_out.ipb_rdata <=  d;
                       ack <= ipbus_in.ipb_strobe and not ack;
                     end if;	
			   end if;
	end process;
	
	ipbus_out.ipb_ack <= ack;
	ipbus_out.ipb_err <= '0';

end rtl;
