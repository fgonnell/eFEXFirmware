--! @file
--! @brief TOB/XTOB data readout slave registers
--! @details 
--! This module provides IPBus access for all Read Only and Read/Write reigster withing the TOB/XTOB data readout block.
--! @author Saeed Taghavi

library IEEE;
use IEEE.STD_LOGIC_1164.all;

library ipbus_lib;
use ipbus_lib.ipbus.all;

library TOB_rdout_lib;
use TOB_rdout_lib.data_type_pkg.all;
use TOB_rdout_lib.TOB_rdout_ip_pkg.all;
use TOB_rdout_lib.ipbus_decode_efex_tob_readout.all;  -- TOB/XTOB readout package

--! @copydoc slave_TOB_readout.vhd
entity slave_TOB_readout is
  port (
    --! IPBus Reset input
    ipb_rst                                  : in  std_logic;
    --! IPBus Clock input
    ipb_clk                                  : in  std_logic;
    --! IPBus input bus going from master to slaves
    IPb_in                                   : in  ipb_wbus;  -- The signals going from master to slaves
    --! IPBus output bus going from slaves to master
    IPb_out                                  : out ipb_rbus;  -- The signals going from slaves to master
    --! 8b Ext L1A ID from TTC input + 24b L1A ID of counter from TTC FIFO - inserted in the event header
    L1A_ID_Event                             : in  std_logic_vector (31 downto 0);
    --! 8b Extended L1A ID + 24b L1A ID of counter
    L1A_ID                                   : in  std_logic_vector (31 downto 0);
    --! Bunch Crossing number input
    BCN_in                                   : in  std_logic_vector (11 downto 0);
    --! The write address offset pre load for TOB Circular DRPAM 
    TOB_WR_ADDR_OFFSET                       : out std_logic_vector (8 downto 0);
    --! The write address offset pre load for XTOB e/g Circular DRPAM 
    XTOB_EG_WR_ADDR_OFFSET                   : out std_logic_vector (8 downto 0);
    --! The write address offset pre load for XTOB tau Circular DRPAM 
    XTOB_TAU_WR_ADDR_OFFSET                  : out std_logic_vector (8 downto 0);
    --! number of DRP locations (Slices) to read 1 to 5
    TOB_SLICES_TO_RD                         : out std_logic_vector (2 downto 0);
    --! TOB Derandomisation FIFO partial full flag assert threshold
    TOB_FIFO_pFULL_THRESH_ASSERT             : out std_logic_vector (8 downto 0);
    --! TOB Derandomisation FIFO partial full flag negate threshold
    TOB_FIFO_pFULL_THRESH_NEGATE             : out std_logic_vector (8 downto 0);
    --! TOB Derandomisation FIFO FIFO occupancy data count
    TOB_FIFO_DATA_COUNT                      : in  std_logic_vector (8 downto 0);
    --! XTOB e/g Derandomisation FIFO partial full flag assert threshold
    XTOB_EG_FIFO_pFULL_THRESH_ASSERT         : out std_logic_vector (8 downto 0);
    --! XTOB e/g Derandomisation FIFO partial full flag negate threshold
    XTOB_EG_FIFO_pFULL_THRESH_NEGATE         : out std_logic_vector (8 downto 0);
    --! XTOB e/g Derandomisation FIFO FIFO occupancy data count
    XTOB_EG_FIFO_DATA_COUNT                  : in  std_logic_vector (8 downto 0);
    --! XTOB tau Derandomisation FIFO partial full flag assert threshold
    XTOB_TAU_FIFO_pFULL_THRESH_ASSERT        : out std_logic_vector (8 downto 0);
    --! XTOB tau Derandomisation FIFO partial full flag negate threshold
    XTOB_TAU_FIFO_pFULL_THRESH_NEGATE        : out std_logic_vector (8 downto 0);
    --! XTOB tau Derandomisation FIFO FIFO occupancy data count
    XTOB_TAU_FIFO_DATA_COUNT                 : in  std_logic_vector (8 downto 0);
    --! Link output FIFO (before MGT) partial full flag assert threshold
    TOB_Link_output_FIFO_pFULL_THRESH_ASSERT : out std_logic_vector (12 downto 0);
    --! Link output FIFO (before MGT) partial full flag negate threshold
    TOB_Link_output_FIFO_pFULL_THRESH_NEGATE : out std_logic_vector (12 downto 0);
    --! Link output FIFO (before MGT) occupancy data count
    TOB_LINK_OUTPUT_FIFO_RD_DATA_COUNT       : in  std_logic_vector (12 downto 0);
    --! Read only register containing all Empty pFull and Full of TOB/XTOB data block 
    TOB_data_FIFO_flags                      : in  std_logic_vector (11 downto 0);
    --! TOB/XTOB SPY Memory write address (read only register)
    SPY_TOB_mem_wr_addr                      : in  std_logic_vector (10 downto 0);
    --! IPBus signal going to TOB/XTOB SPY DPRAM
    ipbus_out_tob_dpram                      : out ipb_wbus;  -- signal going to TOB SPY DPRAM
    --! IPBus signal coming from TOB/XTOB SPY DPRAM
    ipbus_in_tob_dpram                       : in  ipb_rbus  -- signal coming from TOB SPY DPRAM
    );    
end slave_TOB_readout;

--! @copydoc slave_TOB_readout.vhd
architecture Behavioral of slave_TOB_readout is
  signal ipbw         : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal ipbr, ipbr_d : ipb_rbus_array(N_SLAVES-1 downto 0);

  signal BCN_in_i                                   : std_logic_vector (31 downto 0);
  signal TOB_WR_ADDR_OFFSET_REG_i                   : std_logic_vector (31 downto 0);
  signal XTOB_EG_WR_ADDR_OFFSET_REG_i               : std_logic_vector (31 downto 0);
  signal XTOB_TAU_WR_ADDR_OFFSET_REG_i              : std_logic_vector (31 downto 0);
  signal TOB_SLICES_TO_RD_i                         : std_logic_vector (31 downto 0) := X"00000001";
  signal TOB_FIFO_pFULL_THRESH_ASSERT_i             : std_logic_vector (31 downto 0);
  signal TOB_FIFO_pFULL_THRESH_NEGATE_i             : std_logic_vector (31 downto 0);
  signal TOB_FIFO_DATA_COUNT_i                      : std_logic_vector (31 downto 0);
  signal XTOB_EG_FIFO_pFULL_THRESH_ASSERT_i         : std_logic_vector (31 downto 0);
  signal XTOB_EG_FIFO_pFULL_THRESH_NEGATE_i         : std_logic_vector (31 downto 0);
  signal XTOB_EG_FIFO_DATA_COUNT_i                  : std_logic_vector (31 downto 0);
  signal XTOB_TAU_FIFO_pFULL_THRESH_ASSERT_i        : std_logic_vector (31 downto 0);
  signal XTOB_TAU_FIFO_pFULL_THRESH_NEGATE_i        : std_logic_vector (31 downto 0);
  signal XTOB_TAU_FIFO_DATA_COUNT_i                 : std_logic_vector (31 downto 0);
  signal TOB_Link_output_FIFO_pFULL_THRESH_ASSERT_i : std_logic_vector (31 downto 0);
  signal TOB_Link_output_FIFO_pFULL_THRESH_NEGATE_i : std_logic_vector (31 downto 0);
  signal TOB_LINK_OUTPUT_FIFO_RD_DATA_COUNT_i       : std_logic_vector (31 downto 0);
  signal TOB_data_FIFO_flags_i                      : std_logic_vector (31 downto 0);

  signal SPY_TOB_mem_wr_addr_i : std_logic_vector (31 downto 0);
  signal ipb_strobe_i          : std_logic;
  signal ipb_write_i           : std_logic;
  
begin
  -- input signals
  BCN_in_i                                 <= X"00000" & BCN_in;  -- 12b
  TOB_FIFO_DATA_COUNT_i                    <= X"00000" & "000" & TOB_FIFO_DATA_COUNT;  -- 9b
  XTOB_EG_FIFO_DATA_COUNT_i                <= X"00000" & "000" & XTOB_EG_FIFO_DATA_COUNT;  -- 9b
  XTOB_TAU_FIFO_DATA_COUNT_i               <= X"00000" & "000" & XTOB_TAU_FIFO_DATA_COUNT;  -- 9 bits
  TOB_LINK_OUTPUT_FIFO_RD_DATA_COUNT_i     <= X"0000" & "000" & TOB_LINK_OUTPUT_FIFO_RD_DATA_COUNT;  -- 13b 
  TOB_data_FIFO_flags_i                    <= X"00000" & TOB_data_FIFO_flags;  -- 12 bits
  SPY_TOB_mem_wr_addr_i                    <= X"00000" & "0" & SPY_TOB_mem_wr_addr;
  -- output signals
  TOB_WR_ADDR_OFFSET                       <= TOB_WR_ADDR_OFFSET_REG_i(8 downto 0);
  XTOB_EG_WR_ADDR_OFFSET                   <= XTOB_EG_WR_ADDR_OFFSET_REG_i(8 downto 0);
  XTOB_TAU_WR_ADDR_OFFSET                  <= XTOB_TAU_WR_ADDR_OFFSET_REG_i(8 downto 0);
  TOB_SLICES_TO_RD                         <= TOB_SLICES_TO_RD_i(2 downto 0);
  TOB_FIFO_pFULL_THRESH_ASSERT             <= TOB_FIFO_pFULL_THRESH_ASSERT_i(8 downto 0);
  TOB_FIFO_pFULL_THRESH_NEGATE             <= TOB_FIFO_pFULL_THRESH_NEGATE_i(8 downto 0);
  XTOB_EG_FIFO_pFULL_THRESH_ASSERT         <= XTOB_EG_FIFO_pFULL_THRESH_ASSERT_i(8 downto 0);
  XTOB_EG_FIFO_pFULL_THRESH_NEGATE         <= XTOB_EG_FIFO_pFULL_THRESH_NEGATE_i(8 downto 0);
  XTOB_TAU_FIFO_pFULL_THRESH_ASSERT        <= XTOB_TAU_FIFO_pFULL_THRESH_ASSERT_i(8 downto 0);
  XTOB_TAU_FIFO_pFULL_THRESH_NEGATE        <= XTOB_TAU_FIFO_pFULL_THRESH_NEGATE_i(8 downto 0);
  TOB_Link_output_FIFO_pFULL_THRESH_ASSERT <= TOB_Link_output_FIFO_pFULL_THRESH_ASSERT_i(12 downto 0);
  TOB_Link_output_FIFO_pFULL_THRESH_NEGATE <= TOB_Link_output_FIFO_pFULL_THRESH_NEGATE_i(12 downto 0);


---- access to TOB DPRAM spy memeory
  ipbus_out_tob_dpram          <= ipbw(N_SLV_TOB_DATA_SPY_MEM);  -- signal going to TOB SPY DPRAM
  ipbr(N_SLV_TOB_DATA_SPY_MEM) <= ipbus_in_tob_dpram;  -- signal coming from TOB SPY DPRAM

  ipb_strobe_i <= IPb_in.ipb_strobe;
  ipb_write_i  <= IPb_in.ipb_write;

--U1_ila_TOB_reg : ila_ipbus_fabric_rd_wr
--PORT MAP (
--      clk    => ipb_clk,     
--      probe0 => TOB_WR_ADDR_OFFSET_REG_i,   -- 32b        
--      probe1 => XTOB_EG_WR_ADDR_OFFSET_REG_i,  -- 32b 
--      probe2(0) => ipb_strobe_i,   -- 1b
--      probe3(0) => ipb_write_i,           -- 1b
--      probe4 => (others => '0'),   -- 1b
--      probe5 => XTOB_TAU_WR_ADDR_OFFSET_REG_i,  -- 32b 
--      probe6 => (others => '0'),        -- 32b
--      probe7 => (others => '0') ,             -- 1b
--      probe8 => (others => '0') ,       -- 1b
--    probe9 => (others => '0')               -- 1b
--);  


--! @details
--! \verbatim
--! The IPBus bus fabric, which also has address select logic and data multiplexers.
--! This version selects the addressed slave depending on the state of incoming control lines.
--! \endverbatim
  TOB_rdout_fabric : entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV      => N_SLAVES,
                SEL_WIDTH => ipbus_sel_width)
    port map(
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      sel             => ipbus_sel_efex_tob_readout(ipb_in.ipb_addr),
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      ); 


  U1_L1A_ID_counter : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 0, N_STAT => 1)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_L1A_COUNTER),
      ipbus_out => ipbr(N_SLV_L1A_COUNTER),
      d(0)      => L1A_ID,  -- read only reg 8b & 24b of L1A counter
      q         => open,
      stb       => open); 

  U1_L1A_ID_event : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 0, N_STAT => 1)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_L1A_ID_EVENT),
      ipbus_out => ipbr(N_SLV_L1A_ID_EVENT),
      d(0)      => L1A_ID_Event,  -- read only reg 8b & 24b of current event
      q         => open,
      stb       => open); 

  U2_BCN_in : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 0, N_STAT => 1)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_BCN),
      ipbus_out => ipbr(N_SLV_BCN),
      d(0)      => BCN_in_i,            -- read only reg
      q         => open,
      stb       => open); 

  U3_TOB_WR_ADDR_OFFSET_REG : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_TOB_WR_ADDR_OFFSET_REG),
      ipbus_out => ipbr(N_SLV_TOB_WR_ADDR_OFFSET_REG),
      q(0)      => TOB_WR_ADDR_OFFSET_REG_i,  -- read/write reg
      d         => (others => (others => '0')),
      stb       => open); 

  U3_XTOB_EG_WR_ADDR_OFFSET_REG : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_XTOB_EG_WR_ADDR_OFFSET_REG),
      ipbus_out => ipbr(N_SLV_XTOB_EG_WR_ADDR_OFFSET_REG),
      q(0)      => XTOB_EG_WR_ADDR_OFFSET_REG_i,  -- read/write reg
      d         => (others => (others => '0')),
      stb       => open); 

  U3_XTOB_TAU_WR_ADDR_OFFSET_REG : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_XTOB_TAU_WR_ADDR_OFFSET_REG),
      ipbus_out => ipbr(N_SLV_XTOB_TAU_WR_ADDR_OFFSET_REG),
      q(0)      => XTOB_TAU_WR_ADDR_OFFSET_REG_i,  -- read/write reg
      d         => (others => (others => '0')),
      stb       => open); 

  U4_TOB_SLICES_TO_RD : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_TOB_SLICES_TO_RD),
      ipbus_out => ipbr(N_SLV_TOB_SLICES_TO_RD),
      q(0)      => TOB_SLICES_TO_RD_i,  -- read/write reg
      d         => (others => (others => '0')),
      stb       => open); 

  U5_TOB_FIFO_pFULL_THRESH_ASSERT : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_TOB_FIFO_PROG_FULL_THRESH_ASSERT),
      ipbus_out => ipbr(N_SLV_TOB_FIFO_PROG_FULL_THRESH_ASSERT),
      q(0)      => TOB_FIFO_pFULL_THRESH_ASSERT_i,  -- read/write reg
      d         => (others => (others => '0')),
      stb       => open); 

  U6_TOB_FIFO_pFULL_THRESH_NEGATE : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_TOB_FIFO_PROG_FULL_THRESH_NEGATE),
      ipbus_out => ipbr(N_SLV_TOB_FIFO_PROG_FULL_THRESH_NEGATE),
      q(0)      => TOB_FIFO_pFULL_THRESH_NEGATE_i,  -- read/write reg
      d         => (others => (others => '0')),
      stb       => open); 

  U7_TOB_FIFO_DATA_COUNT : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 0, N_STAT => 1)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_TOB_FIFO_DATA_COUNT),
      ipbus_out => ipbr(N_SLV_TOB_FIFO_DATA_COUNT),
      d(0)      => TOB_FIFO_DATA_COUNT_i,  -- read only reg
      q         => open,
      stb       => open);

  U8_XTOB_EG_FIFO_pFULL_THRESH_ASSERT : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_XTOB_EG_FIFO_PROG_FULL_THRESH_ASSERT),
      ipbus_out => ipbr(N_SLV_XTOB_EG_FIFO_PROG_FULL_THRESH_ASSERT),
      q(0)      => XTOB_EG_FIFO_pFULL_THRESH_ASSERT_i,  -- read/write reg
      d         => (others => (others => '0')),
      stb       => open); 

  U9_XTOB_EG_FIFO_pFULL_THRESH_NEGATE : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_XTOB_EG_FIFO_PROG_FULL_THRESH_NEGATE),
      ipbus_out => ipbr(N_SLV_XTOB_EG_FIFO_PROG_FULL_THRESH_NEGATE),
      q(0)      => XTOB_EG_FIFO_pFULL_THRESH_NEGATE_i,  -- read/write reg
      d         => (others => (others => '0')),
      stb       => open); 

  U10_XTOB_EG_FIFO_DATA_COUNT : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 0, N_STAT => 1)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_XTOB_EG_FIFO_DATA_COUNT),
      ipbus_out => ipbr(N_SLV_XTOB_EG_FIFO_DATA_COUNT),
      d(0)      => XTOB_EG_FIFO_DATA_COUNT_i,  -- read only reg
      q         => open,
      stb       => open);

  U8_XTOB_TAU_FIFO_pFULL_THRESH_ASSERT : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_XTOB_TAU_FIFO_PROG_FULL_THRESH_ASSERT),
      ipbus_out => ipbr(N_SLV_XTOB_TAU_FIFO_PROG_FULL_THRESH_ASSERT),
      q(0)      => XTOB_TAU_FIFO_pFULL_THRESH_ASSERT_i,  -- read/write reg
      d         => (others => (others => '0')),
      stb       => open); 

  U9_XTOB_TAU_FIFO_pFULL_THRESH_NEGATE : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_XTOB_TAU_FIFO_PROG_FULL_THRESH_NEGATE),
      ipbus_out => ipbr(N_SLV_XTOB_TAU_FIFO_PROG_FULL_THRESH_NEGATE),
      q(0)      => XTOB_TAU_FIFO_pFULL_THRESH_NEGATE_i,  -- read/write reg
      d         => (others => (others => '0')),
      stb       => open); 

  U10_XTOB_TAU_FIFO_DATA_COUNT : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 0, N_STAT => 1)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_XTOB_TAU_FIFO_DATA_COUNT),
      ipbus_out => ipbr(N_SLV_XTOB_TAU_FIFO_DATA_COUNT),
      d(0)      => XTOB_TAU_FIFO_DATA_COUNT_i,  -- read only reg
      q         => open,
      stb       => open);

  U11_LINK_OUTPUT_FIFO_pFULL_THRESH_ASSERT : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_LINK_OUTPUT_FIFO_PROG_FULL_THRESH_ASSERT),
      ipbus_out => ipbr(N_SLV_LINK_OUTPUT_FIFO_PROG_FULL_THRESH_ASSERT),
      q(0)      => TOB_LINK_OUTPUT_FIFO_pFULL_THRESH_ASSERT_i,  -- read/write reg
      d         => (others => (others => '0')),
      stb       => open); 

  U12_LINK_OUTPUT_FIFO_pFULL_THRESH_NEGATE : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_LINK_OUTPUT_FIFO_PROG_FULL_THRESH_NEGATE),
      ipbus_out => ipbr(N_SLV_LINK_OUTPUT_FIFO_PROG_FULL_THRESH_NEGATE),
      q(0)      => TOB_LINK_OUTPUT_FIFO_pFULL_THRESH_NEGATE_i,  -- read/write reg
      d         => (others => (others => '0')),
      stb       => open); 

  U13_LINK_OUTPUT_FIFO_RD_DATA_COUNT : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 0, N_STAT => 1)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_LINK_OUTPUT_FIFO_RD_DATA_COUNT),
      ipbus_out => ipbr(N_SLV_LINK_OUTPUT_FIFO_RD_DATA_COUNT),
      d(0)      => TOB_LINK_OUTPUT_FIFO_RD_DATA_COUNT_i,  -- read only reg
      q         => open,
      stb       => open);

  U12_TOB_data_FIFO_flags : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 0, N_STAT => 1)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_TOB_DATA_FIFO_FLAGS),
      ipbus_out => ipbr(N_SLV_TOB_DATA_FIFO_FLAGS),
      d(0)      => TOB_data_FIFO_flags_i,  -- read only reg
      q         => open,
      stb       => open);

  U16_TOB_mem_wr_addr : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 0, N_STAT => 1)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_SPY_TOB_MEM_WR_ADDR),
      ipbus_out => ipbr(N_SLV_SPY_TOB_MEM_WR_ADDR),
      d(0)      => SPY_TOB_mem_wr_addr_i,  -- read only reg
      q         => open,
      stb       => open);      


end Behavioral;
