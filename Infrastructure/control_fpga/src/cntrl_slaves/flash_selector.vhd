----------------------------------------------------------------------------------
-- Company: STFC
-- Engineer: Mohammed Siyad
-- 
-- Create Date: 10/22/2015 02:13:20 PM
-- Design Name: 
-- Module Name: flash_selector - Behavioral
-- Project Name: ATLAS Efex board
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity flash_selector is
    Port ( 
           sel : in STD_LOGIC_VECTOR (1 downto 0);
         -- f1_flash_cs : out STD_LOGIC;
           f2_flash_cs : out STD_LOGIC;
           f3_flash_cs : out STD_LOGIC;
           f4_flash_cs : out STD_LOGIC;
           f5_flash_cs : out STD_LOGIC;
           flash_cs : in STD_LOGIC
           );
end flash_selector;

architecture Behavioral of flash_selector is

begin
       
           
 f5_flash_cs <= flash_cs when sel = "00" else '1';
 f2_flash_cs <= flash_cs when sel = "01" else '1';
 f3_flash_cs <= flash_cs when sel = "10" else '1';
 f4_flash_cs <= flash_cs when sel = "11" else '1';
 --f5_flash_cs <= flash_cs when sel = "100" else '1';    
            
end Behavioral;
