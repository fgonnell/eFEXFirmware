----------------------------------------------------------------------------------
-- Company: STFC
-- Engineer: Mohammed Siyad
-- 
-- Create Date: 14.11.2018 16:34:46
-- Design Name: 
-- Module Name: aurora_wrapper - Behavioral
-- Project Name: 
-- Target Devices: virtex7
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity aurora_wrapper_hub2 is
 
  Port ( 
       -- TX Stream Interface
        s_axi_tx_tdata      : in  std_logic_vector(63 downto 0);
        s_axi_tx_tvalid     : in  std_logic;
        s_axi_tx_tready     : out std_logic;
        s_axi_tx_tkeep      : in std_logic_vector(7 downto 0);
        s_axi_tx_tlast      : in  std_logic;
       -- User Flow Control TX Interface
        s_axi_ufc_tx_req    : in std_logic;
        s_axi_ufc_tx_ms     : in std_logic_vector(2 downto 0);
        s_axi_ufc_tx_ack    : out std_logic;  
       -- V7 Serial I/O
        txp                 : out std_logic_vector(0 to 3);
        txn                 : out std_logic_vector(0 to 3);
       -- GT Reference Clock Interface
        
        gt_refclk1_p        : in std_logic;
        gt_refclk1_n        : in std_logic;
       -- Error Detection Interface
        tx_hard_err         : out std_logic;
       -- Status
        tx_channel_up       : out std_logic;
        tx_lane_up          : out std_logic_vector(0 to 3);
       -- System Interface
        user_clk_out        : out std_logic;
        sys_reset_out       : out std_logic;
        gt_reset            : in std_logic;
        tx_system_reset     : in std_logic;
        tx_lock             : out std_logic;
        init_clk            : in  std_logic; 
        init_clk_out        : out  std_logic;
        pll_not_locked      : out std_logic;
        tx_resetdone        : out std_logic

);
end aurora_wrapper_hub2;

architecture Behavioral of aurora_wrapper_hub2 is


 component efex_aurora_hub2_support
       port   (
        -- TX Stream Interface
        s_axi_tx_tdata      : in  std_logic_vector(63 downto 0);
        s_axi_tx_tvalid     : in  std_logic;
        s_axi_tx_tready     : out std_logic;
        s_axi_tx_tkeep      : in std_logic_vector(7 downto 0);
        s_axi_tx_tlast      : in  std_logic;
       -- User Flow Control TX Interface
        s_axi_ufc_tx_req    : in std_logic;
        s_axi_ufc_tx_ms     : in std_logic_vector(2 downto 0);
        s_axi_ufc_tx_ack    : out std_logic;  
       -- V7 Serial I/O
        txp                 : out std_logic_vector(0 to 3);
        txn                 : out std_logic_vector(0 to 3);
       -- GT Reference Clock Interface        
        gt_refclk1_p        : in std_logic;
        gt_refclk1_n        : in std_logic;
       -- Error Detection Interface
        tx_hard_err         : out std_logic;
       -- Status
        tx_channel_up       : out std_logic;
        tx_lane_up          : out std_logic_vector(0 to 3);
       -- System Interface
        user_clk_out        : out std_logic;
        sys_reset_out       : out std_logic;
        gt_reset            : in std_logic;
        tx_system_reset     : in std_logic;
        tx_lock             : out std_logic;
        init_clk            : in  std_logic; 
        init_clk_out        : out  std_logic;
        pll_not_locked_out  : out std_logic;
        tx_resetdone_out    : out std_logic;
        -- drp ports
        drpclk_in           : in   std_logic;
        drpaddr_in          : in   std_logic_vector(8 downto 0);
        drpdi_in            : in   std_logic_vector(15 downto 0);
        drpdo_out           : out  std_logic_vector(15 downto 0);
        drpen_in            : in   std_logic;
        drprdy_out          : out  std_logic;
        drpwe_in            : in   std_logic;
        drpaddr_in_lane1    : in   std_logic_vector(8 downto 0);
        drpdi_in_lane1      : in   std_logic_vector(15 downto 0);
        drpdo_out_lane1     : out  std_logic_vector(15 downto 0);
        drpen_in_lane1      : in   std_logic;
        drprdy_out_lane1    : out  std_logic;
        drpwe_in_lane1      : in   std_logic;
        drpaddr_in_lane2    : in   std_logic_vector(8 downto 0);
        drpdi_in_lane2      : in   std_logic_vector(15 downto 0);
        drpdo_out_lane2     : out  std_logic_vector(15 downto 0);
        drpen_in_lane2      : in   std_logic;
        drprdy_out_lane2    : out  std_logic;
        drpwe_in_lane2      : in   std_logic;
        drpaddr_in_lane3    : in   std_logic_vector(8 downto 0);
        drpdi_in_lane3      : in   std_logic_vector(15 downto 0);
        drpdo_out_lane3     : out  std_logic_vector(15 downto 0);
        drpen_in_lane3      : in   std_logic;
        drprdy_out_lane3    : out  std_logic;
        drpwe_in_lane3      : in   std_logic; 
        power_down          : in std_logic;
        loopback            : in std_logic_vector(2 downto 0)
    );
   end component;
   
signal drpclk_i                 : std_logic;
signal daddr_in_i               : std_logic_vector(8 downto 0);
signal dclk_in_i                : std_logic;
signal den_in_i                 : std_logic;
signal di_in_i                  : std_logic_vector(15 downto 0);
signal drdy_out_unused_i        : std_logic;
signal drpdo_out_unused_i       : std_logic_vector(15 downto 0);
signal dwe_in_i                 : std_logic;
signal daddr_in_LANE1_i         : std_logic_vector(8 downto 0);
signal dclk_in_LANE1_i          : std_logic;
signal den_in_LANE1_i           : std_logic;
signal di_in_LANE1_i            : std_logic_vector(15 downto 0);
signal drdy_out_LANE1_unused_i  : std_logic;
signal drpdo_out_LANE1_unused_i : std_logic_vector(15 downto 0);
signal dwe_in_LANE1_i           : std_logic;
signal daddr_in_LANE2_i         : std_logic_vector(8 downto 0);
signal dclk_in_LANE2_i          : std_logic;
signal den_in_LANE2_i           : std_logic;
signal di_in_LANE2_i            : std_logic_vector(15 downto 0);
signal drdy_out_LANE2_unused_i  : std_logic;
signal drpdo_out_LANE2_unused_i : std_logic_vector(15 downto 0);
signal dwe_in_LANE2_i           : std_logic;
signal daddr_in_LANE3_i         : std_logic_vector(8 downto 0);
signal dclk_in_LANE3_i          : std_logic;
signal den_in_LANE3_i           : std_logic;
signal di_in_LANE3_i            : std_logic_vector(15 downto 0);
signal drdy_out_LANE3_unused_i  : std_logic;
signal drpdo_out_LANE3_unused_i : std_logic_vector(15 downto 0);
signal dwe_in_LANE3_i           : std_logic;
signal power_down_i       : std_logic;                   
signal loopback_i         : std_logic_vector(2 downto 0);

 
begin

 -- System Interface
    power_down_i     <= '0';
    loopback_i       <= "000";
    daddr_in_i        <=  (others=>'0');
    dclk_in_i         <=  '0';
    den_in_i          <=  '0';
    di_in_i           <=  (others=>'0');
    dwe_in_i          <=  '0';
    daddr_in_LANE1_i  <=  (others=>'0');
    dclk_in_LANE1_i   <=  '0';
    den_in_LANE1_i    <=  '0';
    di_in_LANE1_i     <=  (others=>'0');
    dwe_in_LANE1_i    <=  '0';
    daddr_in_LANE2_i  <=  (others=>'0');
    dclk_in_LANE2_i   <=  '0';
    den_in_LANE2_i    <=  '0';
    di_in_LANE2_i     <=  (others=>'0');
    dwe_in_LANE2_i    <=  '0';
    daddr_in_LANE3_i  <=  (others=>'0');
    dclk_in_LANE3_i   <=  '0';
    den_in_LANE3_i    <=  '0';
    di_in_LANE3_i     <=  (others=>'0');
    dwe_in_LANE3_i    <=  '0';


   aurora_module_i : efex_aurora_hub2_support
        port map   (
        -- AXI TX Interface
                   s_axi_tx_tdata   => s_axi_tx_tdata ,
                   s_axi_tx_tkeep   => s_axi_tx_tkeep ,
                   s_axi_tx_tvalid  => s_axi_tx_tvalid,
                   s_axi_tx_tlast   => s_axi_tx_tlast ,
                   s_axi_tx_tready  => s_axi_tx_tready,                                                        
        -- User Flow Control TX Interface
                   s_axi_ufc_tx_req => s_axi_ufc_tx_req,
                   s_axi_ufc_tx_ms  => s_axi_ufc_tx_ms ,
                   s_axi_ufc_tx_ack => s_axi_ufc_tx_ack,
        -- Serial IO
                   txn              => txn,
                   txp              => txp,
                   gt_refclk1_p     => gt_refclk1_p,
                   gt_refclk1_n     => gt_refclk1_n,
        -- Error Detection Interface
                   tx_hard_err      => tx_hard_err,
        -- Status
                   tx_channel_up    => tx_channel_up,
                   tx_lane_up       => tx_lane_up,
        -- System Interface
                   user_clk_out     => user_clk_out,
                   sys_reset_out    => sys_reset_out,
                   tx_system_reset  => tx_system_reset,
                   power_down       => power_down_i,
                   gt_reset         => gt_reset,
                   tx_lock          => tx_lock,
                   init_clk         => INIT_CLK,
                   init_clk_out     => init_clk_out,
            	   pll_not_locked_out => pll_not_locked,
            	   tx_resetdone_out   => tx_resetdone,
       -- drp port
                   drpclk_in    => drpclk_i,
                   drpaddr_in   => daddr_in_i,
                   drpen_in     => den_in_i,
                   drpdi_in     => di_in_i,
                   drprdy_out   => drdy_out_unused_i,
                   drpdo_out    => drpdo_out_unused_i,
                   drpwe_in     => dwe_in_i,
                   drpaddr_in_lane1 => daddr_in_lane1_i,
                   drpen_in_lane1   => den_in_lane1_i,
                   drpdi_in_lane1   => di_in_lane1_i,
                   drprdy_out_lane1 => drdy_out_lane1_unused_i,
                   drpdo_out_lane1  => drpdo_out_lane1_unused_i,
                   drpwe_in_lane1   => dwe_in_lane1_i,
                   drpaddr_in_lane2 => daddr_in_lane2_i,
                   drpen_in_lane2   => den_in_lane2_i,
                   drpdi_in_lane2   => di_in_lane2_i,
                   drprdy_out_lane2 => drdy_out_lane2_unused_i,
                   drpdo_out_lane2  => drpdo_out_lane2_unused_i,
                   drpwe_in_lane2   => dwe_in_lane2_i,
                   drpaddr_in_lane3 => daddr_in_lane3_i,
                   drpen_in_lane3   => den_in_lane3_i,
                   drpdi_in_lane3   => di_in_lane3_i,
                   drprdy_out_lane3 => drdy_out_lane3_unused_i,
                   drpdo_out_lane3  => drpdo_out_lane3_unused_i,
                   drpwe_in_lane3   => dwe_in_lane3_i,
                   loopback         => loopback_i
                 );
  

 



end Behavioral;
