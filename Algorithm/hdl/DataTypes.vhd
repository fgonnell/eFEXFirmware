library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use work.AlgoDataTypes.all;

package DataTypes is
-------------------------------------------------------------------------------
-- USEFUL TYPES
-------------------------------------------------------------------------------
  type array_std_logic_vector is array (natural range <>) of std_logic_vector;

-------------------------------------------------------------------------------
-- INTERNAL DATA
-------------------------------------------------------------------------------
  constant DATA_WIDTH       : integer                                   := 16;
  constant ENERGY_WIDTH     : integer                                   := 16;
  constant TOB_ENERGY_WIDTH : integer                                   := 12;
  constant COMPARE_BITS     : integer                                   := 16;
  constant ENERGY_OF        : std_logic_vector(ENERGY_WIDTH-1 downto 0) := (others => '1');
  constant ENERGY_ZERO      : std_logic_vector(ENERGY_WIDTH-1 downto 0) := (others => '0');

  constant PAR_WIDTH : integer := 8;
  constant REG_WIDTH : integer := 32;

-- Adjust the following according to the IP multiplier:
-- maximum value: PAR_WIDTH+DATA_WIDTH;
  constant MULTIPLIER_OUTPUT_WIDTH     : integer := PAR_WIDTH+DATA_WIDTH;
-- maximum value: DATA_WIDTH;
  constant MULTIPLIER_DATA_INPUT_WIDTH : integer := DATA_WIDTH;
-- maximum value: PAR_WIDTH;
  constant MULTIPLIER_PAR_INPUT_WIDTH  : integer := PAR_WIDTH;


  constant PARAMETER_RAM_ADDR_WIDTH   : integer := 7;
  constant PARAMETER_RAM_DATA_WIDTH   : integer := 512;  --must be 2^ the previous one
  constant PARAMETER_RAM_CLOCK_OFFSET : integer := 2;

  subtype DataWord is std_logic_vector(DATA_WIDTH-1 downto 0);
  type DataWords is array(natural range <>) of DataWord;
  constant ZERO_DATA_WORD : DataWord := (others => '0');

  type TriggerTower is record
    Layer0 : DataWords(LAYER0-1 downto 0);
    Layer1 : DataWords(LAYER1-1 downto 0);
    Layer2 : DataWords(LAYER2-1 downto 0);
    Layer3 : DataWords(LAYER3-1 downto 0);
    Hadron : DataWords(HADRON-1 downto 0);
  end record;

  constant ZERO_TRIGGER_TOWER : TriggerTower := (Layer0 => ((others => (others => '0'))),
                                                 Layer1 => ((others => (others => '0'))),
                                                 Layer2 => ((others => (others => '0'))),
                                                 Layer3 => ((others => (others => '0'))),
                                                 Hadron => ((others => (others => '0'))));

  type TriggerTowers is array(natural range <>) of TriggerTower;
  type TriggerTowersArray is array(natural range <>) of TriggerTowers;

-------------------------------------------------------------------------------
-- EFEX MODULE POSITION
-------------------------------------------------------------------------------
  constant EM_POSITION_ETA_ON_EDGE         : integer := 0;
  constant EM_POSITION_ETA_ON_LEFT_EDGE    : integer := 1;
  constant EM_POSITION_POSITIVE_ETA_LEFT   : integer := 2;
  constant EM_POSITION_POSITIVE_ETA_CENTRE : integer := 3;
  constant EM_POSITION_POSITIVE_ETA_RIGHT  : integer := 4;
  constant HADRONIC_POSTION_BIT_START      : integer := 5;  --! 000 Centre, 001 Near -eta, 010 Near +eta, 011 Far -eta, 100 Far +eta
  constant HADRONIC_POSTION_BIT_END        : integer := 7;

-------------------------------------------------------------------------------
-- Trigger Object
-------------------------------------------------------------------------------
  constant RETA_WIDTH : integer := 2;
  constant WS_WIDTH   : integer := 2;
  constant HAD_WIDTH  : integer := 2;

  constant JET_WIDTH : integer := 2;

  type TriggerObjectCore_eg is record
    LocalMax  : std_logic;
    Max       : std_logic;
    Energy    : std_logic_vector(ENERGY_WIDTH - 1 downto 0);
    EnergyOF  : std_logic;
    REta      : std_logic_vector(RETA_WIDTH - 1 downto 0);
    ws        : std_logic_vector(WS_WIDTH - 1 downto 0);
    Had       : std_logic_vector(HAD_WIDTH - 1 downto 0);
    UpNotDown : std_logic;
    Seed      : std_logic_vector(1 downto 0);
  end record;

  type TriggerObjectCore_tau is record
    IsMax     : std_logic;
    Energy    : std_logic_vector(ENERGY_WIDTH - 1 downto 0);
    EnergyOF  : std_logic;
    Jet       : std_logic_vector(JET_WIDTH - 1 downto 0);
    UpNotDown : std_logic;
    Seed      : std_logic_vector(1 downto 0);
  end record;

  type TriggerObjectPosition is record
    Eta : std_logic_vector(2 downto 0);  -- 0 to 4 column left to right
    Phi : std_logic_vector(2 downto 0);  -- 0 to 7 row bottom up   
  end record;

  type TriggerObject_eg is record
    Core     : TriggerObjectCore_eg;
    Position : TriggerObjectPosition;
  end record;

  type TriggerObject_tau is record
    Core     : TriggerObjectCore_tau;
    Position : TriggerObjectPosition;
  end record;

  constant ZERO_TRIGGER_OBJECT_EG : TriggerObject_eg := (
    Core        => (
      LocalMax  => '0',
      Max       => '0',
      Energy    => (others => '0'),
      EnergyOF  => '0',
      REta      => (others => '0'),
      ws        => (others => '0'),
      Had       => (others => '0'),
      UpNotDown => '0',
      Seed      => (others => '0')),
    Position    => (
      Eta       => (others => '0'),
      Phi       => (others => '0'))
    );

  constant ZERO_TRIGGER_OBJECT_TAU : TriggerObject_tau := (
    Core        => (
      IsMax     => '0',
      Energy    => (others => '0'),
      EnergyOF  => '0',
      Jet       => (others => '0'),
      UpNotDown => '0',
      Seed      => (others => '0')),
    Position    => (
      Eta       => (others => '0'),
      Phi       => (others => '0'))
    );


  type TriggerObjects_eg is array(natural range <>) of TriggerObject_eg;
  type TriggerObjects_tau is array(natural range <>) of TriggerObject_tau;
 
  subtype AlgoParameter is std_logic_vector(PAR_WIDTH-1 downto 0);
  type AlgoParameters is array(natural range <>) of AlgoParameter;

  subtype AlgoRegister is std_logic_vector(REG_WIDTH-1 downto 0);
  type AlgoRegisters is array(natural range <>) of AlgoRegister;

  subtype TriggerTowerMatrix is TriggerTowersArray(INPUT_COLUMNS-1 downto 0)(INPUT_ROWS-1 downto 0);

-------------------------------------------------------------------------------
-- FUNCTIONS
-------------------------------------------------------------------------------

  function TOBEnergy (X : AlgoTriggerObject) return std_logic_vector;

  function to_AlgoTriggerObject (X : TriggerObject_eg; FPGA : integer) return AlgoTriggerObject;
  function to_AlgoTriggerObject (X : TriggerObject_tau; FPGA : integer) return AlgoTriggerObject;

  function to_AlgoXTriggerObject (X : TriggerObject_eg; FPGA : integer) return AlgoXTriggerObject;
  function to_AlgoXTriggerObject (X : TriggerObject_tau; FPGA : integer) return AlgoXTriggerObject;


  function to_TriggerObject_eg (X : AlgoTriggerObject) return TriggerObject_eg;
  function to_TriggerObject_tau (X : AlgoTriggerObject) return TriggerObject_tau;


  function to_TriggerObjects_eg (X : AlgoOutput) return TriggerObjects_eg;
  function to_TriggerObjects_tau (X : AlgoOutput) return TriggerObjects_tau;

  function to_AlgoOutput (X : TriggerObjects_eg; FPGA : integer) return AlgoOutput;
  function to_AlgoOutput (X : TriggerObjects_tau; FPGA : integer) return AlgoOutput;

  function to_AlgoOutput (X : AlgoTriggerObjects) return AlgoOutput;

  function to_AlgoXOutput (X : TriggerObjects_eg; FPGA : integer) return AlgoXOutput;
  function to_AlgoXOutput (X : TriggerObjects_tau; FPGA : integer) return AlgoXOutput;


  function is_valid_core (X : TriggerObjectCore_eg) return std_logic;
  function is_valid_core (X : TriggerObjectCore_tau) return std_logic;

  function is_valid_TOB  (X : AlgoTriggerObject) return std_logic;

  function validate_core (X : TriggerObjectCore_eg) return TriggerObjectCore_eg;
  function validate_core (X : TriggerObjectCore_tau) return TriggerObjectCore_tau;

  function invalidate_core (X : TriggerObjectCore_eg) return TriggerObjectCore_eg;
  function invalidate_core (X : TriggerObjectCore_tau) return TriggerObjectCore_tau;

  function invalidate_TOB (X : AlgoTriggerObject) return AlgoTriggerObject;

  function to_TOBEnergy (X : DataWord) return std_logic_vector;
  function FPGA_to_processor_number (X : integer) return std_logic_vector;

  function BitLeftShift(A : DataWord; B : integer) return DataWord;

end package;

package body DataTypes is

  function TOBEnergy (X : AlgoTriggerObject) return std_logic_vector is
    variable Y : std_logic_vector(11 downto 0);
  begin
    Y := X(11 downto 0);
    return Y;
  end;

  function is_valid_core (X : TriggerObjectCore_eg) return std_logic is
    variable Y : std_logic;
  begin
    if X.LocalMax = '1' then
      Y := '1';
    else
      Y := '0';
    end if;
    return Y;
  end;

  function is_valid_core (X : TriggerObjectCore_tau) return std_logic is
    variable Y : std_logic;
  begin
    if X.IsMax = '1' then
      Y := '1';
    else
      Y := '0';
    end if;
    return Y;
  end;


    function is_valid_TOB (X : AlgoTriggerObject) return std_logic is
    variable Y : std_logic;
  begin
    if X(11 downto 0) = x"000" then
      Y := '0';
    else
      Y := '1';
    end if;
    return Y;
  end;

  function validate_core (X : TriggerObjectCore_eg) return TriggerObjectCore_eg is
    variable Y : TriggerObjectCore_eg;
  begin
    Y := X;
    if is_valid_core(X) = '0' then
      Y.Energy := (others => '0');
    end if;
    return Y;
  end;

  function invalidate_core (X : TriggerObjectCore_tau) return TriggerObjectCore_tau is
    variable Y : TriggerObjectCore_tau;
  begin
    Y          := X;
    Y.Energy   := (others => '0');
    Y.IsMax := '0';
    return Y;
  end;
  
    function validate_core (X : TriggerObjectCore_tau) return TriggerObjectCore_tau is
    variable Y : TriggerObjectCore_tau;
  begin
    Y := X;
    if is_valid_core(X) = '0' then
      Y.Energy := (others => '0');
    end if;
    return Y;
  end;

  function invalidate_core (X : TriggerObjectCore_eg) return TriggerObjectCore_eg is
    variable Y : TriggerObjectCore_eg;
  begin
    Y          := X;
    Y.Energy   := (others => '0');
    Y.LocalMax := '0';
    return Y;
  end;

    function invalidate_TOB (X : AlgoTriggerObject) return AlgoTriggerObject is
    variable Y : AlgoTriggerObject;
  begin
    Y          := X;
    Y(11 downto 0) := x"000";
    return Y;
  end;



  
------------------ eg TOB FORMAT definition ---------------------
  function to_AlgoTriggerObject (X : TriggerObject_eg; FPGA : integer) return AlgoTriggerObject is
    variable Y : AlgoTriggerObject;
  begin
    if X.Core.EnergyOF = '1' or X.Core.Energy(ENERGY_WIDTH-2) = '1' or X.Core.Energy(ENERGY_WIDTH-1) = '1' then
      Y(11 downto 0) := (others => '1');
    else
      -- Convert energy here
      Y(11 downto 0) := (TOB_ENERGY_WIDTH-1 downto 0 => X.Core.Energy(ENERGY_WIDTH-3 downto 2), others => '0');
    end if;
    Y(13 downto 12) := (others => '0');
    Y(14)           := X.Core.Max;
    Y(15)           := X.Core.UpNotDown;
    Y(17 downto 16) := X.Core.Seed;
    Y(19 downto 18) := X.Core.REta;
    Y(21 downto 20) := X.Core.ws;
    Y(23 downto 22) := X.Core.Had;
    Y(26 downto 24) := X.Position.Phi;
    Y(29 downto 27) := X.Position.Eta;
    Y(31 downto 30) := FPGA_to_processor_number(FPGA);
    return Y;
  end;

------------------ XTOB FORMAT definition ---------------------
  function to_AlgoXTriggerObject (X : TriggerObject_eg; FPGA : integer) return AlgoXTriggerObject is
    variable Y : AlgoXTriggerObject;
  begin
    Y(13 downto 0)  := (others => '0');
    Y(14)           := X.Core.Max;
    Y(15)           := X.Core.UpNotDown;
    Y(17 downto 16) := X.Core.Seed;
    Y(19 downto 18) := X.Core.REta;
    Y(21 downto 20) := X.Core.ws;
    Y(23 downto 22) := X.Core.Had;
    Y(26 downto 24) := X.Position.Phi;
    Y(29 downto 27) := X.Position.Eta;
    Y(31 downto 30) := FPGA_to_processor_number(FPGA);
    if X.Core.EnergyOF = '1' then
      Y(47 downto 32) := (others => '1');
    else
      Y(47 downto 32) := X.Core.Energy;
    end if;
    Y(63 downto 48) := (others => '0');
    return Y;
  end;

  function to_TriggerObject_eg (X : AlgoTriggerObject) return TriggerObject_eg is
    variable Y : TriggerObject_eg;
  begin
    if X(ENERGY_WIDTH-1 downto 0) /= ENERGY_ZERO then
      Y.Core.LocalMax := '1';
    else
      Y.Core.LocalMax := '0';
    end if;
    Y.Core.Energy    := "00" & X(11 downto 0) & "00";
    Y.Core.Max       := X(14);
    Y.Core.UpNotDown := X(15);
    Y.Core.Seed      := X(17 downto 16);
    Y.Core.REta      := X(19 downto 18);
    Y.Core.ws        := X(21 downto 20);
    Y.Core.Had       := X(23 downto 22);
    Y.Position.Phi   := X(26 downto 24);
    Y.Position.Eta   := X(29 downto 27);
    if X(ENERGY_WIDTH-1 downto 0) = ENERGY_OF then
      Y.Core.EnergyOF := '1';
    else
      Y.Core.EnergyOF := '0';
    end if;

    return Y;
  end;


------------------ TAU TOB FORMAT definition ---------------------
  function to_AlgoTriggerObject (X : TriggerObject_tau; FPGA : integer) return AlgoTriggerObject is
    variable Y : AlgoTriggerObject;
  begin
    if X.Core.EnergyOF = '1' or X.Core.Energy(ENERGY_WIDTH-2) = '1' or X.Core.Energy(ENERGY_WIDTH-1) = '1' then
      Y(11 downto 0) := (others => '1');
    else
      -- Convert energy here
      Y(11 downto 0) := (TOB_ENERGY_WIDTH-1 downto 0 => X.Core.Energy(ENERGY_WIDTH-3 downto 2), others => '0');
    end if;
    Y(13 downto 12) := (others => '0');
    Y(14)           := '0';
    Y(15)           := X.Core.UpNotDown;
    Y(17 downto 16) := X.Core.Seed;
    Y(19 downto 18) := X.Core.Jet;
    Y(21 downto 20) := "00";
    Y(23 downto 22) := "00";
    Y(26 downto 24) := X.Position.Phi;
    Y(29 downto 27) := X.Position.Eta;
    Y(31 downto 30) := FPGA_to_processor_number(FPGA);
    return Y;
  end;

------------------ TAU XTOB FORMAT definition ---------------------
  function to_AlgoXTriggerObject (X : TriggerObject_tau; FPGA : integer) return AlgoXTriggerObject is
    variable Y : AlgoXTriggerObject;
  begin
    Y(13 downto 0)  := (others => '0');
    Y(14)           := '0';
    Y(15)           := X.Core.UpNotDown;
    Y(17 downto 16) := X.Core.Seed;
    Y(19 downto 18) := X.Core.Jet;
    Y(21 downto 20) := "00";
    Y(23 downto 22) := "00";
    Y(26 downto 24) := X.Position.Phi;
    Y(29 downto 27) := X.Position.Eta;
    Y(31 downto 30) := FPGA_to_processor_number(FPGA);
    if X.Core.EnergyOF = '1' then
      Y(47 downto 32) := (others => '1');
    else
      Y(47 downto 32) := X.Core.Energy;
    end if;
    Y(63 downto 48) := (others => '0');
    return Y;
  end;

  function to_TriggerObject_tau (X : AlgoTriggerObject) return TriggerObject_tau is
    variable Y : TriggerObject_tau;
  begin
    if X(ENERGY_WIDTH-1 downto 0) /= ENERGY_ZERO then
      Y.Core.IsMax := '1';
    else
      Y.Core.IsMax := '0';
    end if;
    Y.Core.Energy    := "00" & X(11 downto 0) & "00";
    Y.Core.UpNotDown := X(15);
    Y.Core.Seed      := X(17 downto 16);
    Y.Core.Jet      := X(19 downto 18);
    Y.Position.Phi   := X(26 downto 24);
    Y.Position.Eta   := X(29 downto 27);
    if X(ENERGY_WIDTH-1 downto 0) = ENERGY_OF then
      Y.Core.EnergyOF := '1';
    else
      Y.Core.EnergyOF := '0';
    end if;

    return Y;
  end;

-------------------------------------------

  function to_TriggerObjects_eg (X : AlgoOutput) return TriggerObjects_eg is
    variable Y : TriggerObjects_eg(7 downto 0);
  begin
    for i in Y'range loop
      Y(i) := to_TriggerObject_eg(X(i));
    end loop;  -- i
    return Y;
  end;

  function to_AlgoOutput (X : TriggerObjects_eg; FPGA: integer) return AlgoOutput is
    variable Y : AlgoOutput;
  begin
    for i in Y'range loop
      Y(i) := to_AlgoTriggerObject(X(i), FPGA);
    end loop;  -- i
    return Y;
  end;

  function to_TriggerObjects_tau (X : AlgoOutput) return TriggerObjects_tau is
    variable Y : TriggerObjects_tau(7 downto 0);
  begin
    for i in Y'range loop
      Y(i) := to_TriggerObject_tau(X(i));
    end loop;  -- i
    return Y;
  end;

  function to_AlgoOutput (X : TriggerObjects_tau; FPGA : integer) return AlgoOutput is
    variable Y : AlgoOutput;
  begin
    for i in Y'range loop
      Y(i) := to_AlgoTriggerObject(X(i), FPGA);
    end loop;  -- i
    return Y;
  end;


function to_AlgoOutput (X : AlgoTriggerObjects) return AlgoOutput is
    variable Y : AlgoOutput;
  begin
    for i in Y'range loop
      Y(i) := X(i);
    end loop;  -- i
    return Y;
  end;

  function to_AlgoXOutput (X : TriggerObjects_eg; FPGA: integer) return AlgoXOutput is
    variable Y : AlgoXOutput;
  begin
    for i in Y'range loop
      Y(i) := to_AlgoXTriggerObject(X(i), FPGA);
    end loop;  -- i
    return Y;
  end;


  function to_AlgoXOutput (X : TriggerObjects_tau; FPGA: integer) return AlgoXOutput is
    variable Y : AlgoXOutput;
  begin
    for i in Y'range loop
      Y(i) := to_AlgoXTriggerObject(X(i), FPGA);
    end loop;  -- i
    return Y;
  end;

  function to_TOBEnergy (X : DataWord) return std_logic_vector is
    variable Y : std_logic_vector(ENERGY_WIDTH - 1 downto 0);
  begin
    if (ENERGY_WIDTH - 1) /= DataWord'high then
      if unsigned(X(X'high downto ENERGY_WIDTH)) /= 0 then
        Y := (others => '1');
      else
        Y := X(ENERGY_WIDTH-1 downto 0);
      end if;
    else
      Y := X;
    end if;

    return Y;
  end;

function FPGA_to_processor_number (X : integer) return std_logic_vector is
variable Y : std_logic_vector( 1 downto 0);
begin
  case X is
      when 1  =>
         Y := "00";
      when 2  =>
         Y := "11";
      when 3  =>
         Y := "01";
      when 4  =>
         Y := "10";
      when others  =>
         Y := "00";
    end case;

return Y;
end function;

  --! Performs bit shift with overflow
  function BitLeftShift (
    A : DataWord;
    B : integer)
    return DataWord is

    variable Y      : DataWord;
    variable AllOne : DataWord := (others => '1');
  begin
    Y := (Y'high downto B => A(A'high - B downto 0), others => '0');

    if unsigned(A(A'high downto A'high -B+1)) = 0 then
      return Y;
    else
      return AllOne;
    end if;
  end function BitLeftShift;

end DataTypes;
