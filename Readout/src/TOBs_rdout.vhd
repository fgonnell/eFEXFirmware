--! @file
--! @brief Sorted TOB/XTOB Readout Logic for process FPGA
--! @details 
--! \verbatim
--! This module received Sorted TOBs & XTOBs data and produces events of 32b words for transmission to control FPGA
--!
--! Different TOB/XTOB data event are generated depending on FPGA Number, buffer levels and control settings.
--!
--! The Merging FPGAs generate TOB/XTOB data events which consist of maximum 20 tau XTOBs, 20 e/g XTOBs and up to 6 Sorted TOBs.
--! The Non-Merging FPGAs generate XTOB data events which consist of maximum 20 tau XTOBs, 20 e/g XTOBs and do not include Sorted TOBs.
--! 
--! Process FPGA 1 is a merging FPGAs, receive sorted e/g TOBs from the other 3 FPGA.
--! Process FPGA 1 creates an event consisting of locally generated e/g and tau XTOBs, together with Sorted e/g TOBs..
--! 
--! Process FPGA 2 is a merging FPGAs, receive sorted tau TOBs from the other 3 FPGA. 
--! Process FPGA 2 creates an event consisting of locally generated e/g and tau XTOBs, together with Sorted tau TOBs..
--!
--! Process FPGA 3 and 4 are non-merging FPGAs, so do not receive any TOB data from other FPGAs. 
--! Process FPGA 3 and 4 create event consisting of locally generated e/g and tau XTOBs, but do not include and Sorted TOBs..
--!
--! 1. Readout operation for Process FPGAs 1 & 2, create  Events which consists of only Valid XTOB/TOBs.
--!    a- TOB & XTOB Event in Normal Operation:
--!       Read 1 Slice - Takes 98 ticks of 280MHz clock to create one TOB & XTOB Event with 1 Slice Readout.
--!    b- TOB & XTOB Event in Normal Operation:
--!       Read 2 Slice - Takes 191 ticks of 280MHz clock to create one TOB & XTOB Event with 2 Slice Readout.
--!    c- TOB & XTOB Event in Normal Operation:
--!       Read 3 Slice - Takes 284 ticks of 280MHz clock to create one TOB & XTOB Event with 3 Slice Readout.
--!    d- TOB & XTOB Event in Normal Operation:
--!       Read 4 Slice - Takes 377 ticks of 280MHz clock to create one TOB & XTOB Event with 4 Slice Readout.
--!    e- TOB & XTOB Event in SAFE Mode Operation:
--!       Read 0 Slice - Takes 8 ticks of 280MHz clock to create one SAFE Mode TOB & XTOB Event.
--!
--! 1. Readout operation for Process FPGAs 3 & 4, create  Events which consists of only Valid e/g and tau XTOBs..
--!    a- XTOB Event in Normal Operation:
--!       Read 1 Slice - Takes 92 ticks of 280MHz clock to create one XTOB Event with 1 Slice Readout.
--!    b- XTOB Event in Normal Operation:
--!       Read 2 Slice - Takes 180 ticks of 280MHz clock to create one XTOB Event with 2 Slice Readout.
--!    c- XTOB Event in Normal Operation:
--!       Read 3 Slice - Takes 267 ticks of 280MHz clock to create one XTOB Event with 3 Slice Readout.
--!    d- XTOB Event in Normal Operation:
--!       Read 4 Slice - Takes 354 ticks of 280MHz clock to create one XTOB Event with 4 Slice Readout.
--!    e- XTOB Event in SAFE Mode Operation:
--!       Read 0 Slice - Takes 8 ticks of 280MHz clock to create one SAFE Mode XTOB Event.
--!
--! Sequence of Buffers occupancy levels:
--! 1. When the Ready signal from Control FPGA is removed, complete TOB/XTOB Events are stored in TOB/XTOB Link Output FIFO.
--!    When the occupancy of TOB Link Output FIFO reaches its prog FULL occupancy level, 
--!    then the construction of TOB/XTOB Events are paused.
--!    Enough headroom must be assigned in Link Output FIFO to be able to store TOB/XTOB Events under Safe Mode operation.
--! 2. At this point, TOB/XTOB data are still transfered from Circular DPRAM into de-randomisation TOB/XTOB Data FIFO.
--!    When the occupancy of de-randomisation TOB/XTOB Data FIFO or TTC FIFO reaches its prog FULL occupancy level,
--!    a Safe Mode Flag is set which is used to create Safe Mode TOB/XTOB events and empty the TOB/XTOB Data FIFO & TTC FIFO.
--!    These Safe Mode events consists of 2 Header words, and one Trailer word. 
--!    The payload consists of two words, a ZERO word together with a sub-trailer word for the slice.
--!    Multi-slice readout contains a number of these double words, equal to the number of slices to be readout.
--!
--!    A BUSY request must be sent to HUB/ROD to reduce L1A rates at this time.
--!
--!    In Safe Mode, all buffers, except TOB/XTOB Link Output FIFO, are flushed, and very small TOB/XTOB Events are generated and stored in Link Output FIFO.
--!
--! Under Safe Mode operation if the occupancy of TTC FIFO or TOB/XTOB Data FIFO FIFO, reaches its FULL occupancy level, then the system synchronisation is lost.
--!
--! \endverbatim 
--! @author Saeed Taghavi

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library ipbus_lib;
use ipbus_lib.ipbus.all;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;
use TOB_rdout_lib.data_type_pkg.all;

library algolib;
use algolib.AlgoDataTypes.all;

--! @copydoc TOBs_rdout.vhd
entity TOBs_rdout is
    Generic
        ( 
    --! Integer used to distinguish different FPGAs having a slightly different firmware
    FPGA_NUMBER : integer
        ) ; 
    Port ( 
       RST                   : in std_logic ;
       --! FPGA Hardware Address
       hw_addr               : in STD_LOGIC_VECTOR(1 downto 0) ;
       --! spy memory write address counter reset Pulse by software command  
       RST_spy_mem_wr_addr         : in std_logic ;    
       --! TOB Readout FIFO reset Pulse by software command
       TOB_FIFO_sw_rst          : in std_logic ;       
       --! array 8 x 64b words XTOB e/g
       XTOB_eg_512b_in        :  in AlgoXOutput;        -- array 8 x 64b words XTOB e/g
       --! 8b XTOB e/g has valid data
       XTOB_eg_Valid_flg_in   : in STD_LOGIC_VECTOR (7 downto 0);
       --! XTOB e/g  sync signal     
       XTOB_eg_sync_in        : in STD_LOGIC;                         
       --! array 8 x 64b words XTOB tau
       XTOB_tau_512b_in       :  in AlgoXOutput;        -- array 8 x 64b words XTOB tau
       --! 8b XTOB tau has valid data
       XTOB_tau_Valid_flg_in  : in STD_LOGIC_VECTOR (7 downto 0);  
       --! XTOB tau  sync signal   
       XTOB_tau_sync_in       : in STD_LOGIC;                         
       --! XTOB BCN with delay through ALGO/sorting block
       OUT_XTOB_BCN          : in std_logic_vector(6 downto 0);                      
       --! Sorted TOB data 32b * 7 in series, F1 reads e/g TOBs and F2 reads tau TOBs. Same firmware in both FPGAs, use hw addr to differentiate
       TOBs_32b_in          : in STD_LOGIC_VECTOR (31 downto 0);  
       --! sorted TOB start signal 
       TOBs_sync_in         : in STD_LOGIC;
       --! sorted TOB write signal                        
       TOBs_valid_flg_in    : in STD_LOGIC;                        
       --! sorted TOB BCN with delay through ALGO/sorting block
       OUT_TOB_BCN           : in std_logic_vector(6 downto 0);
       --! Sorted TOB data readout 32b * 7 in series, only 6 is used - F1 reads e/g TOBs and F2 reads tau TOBs                        
       TOB_type_in          : in STD_LOGIC;
       --! 4b error flags CT + BN2 + TIE + EIE
       TOB_err_4b_in        : in  STD_LOGIC_VECTOR (3 downto 0);     
       --! 40MHz clock input signal
       clk_40M_in           : in  STD_LOGIC;
       --! 200Mhz input signal    
       clk_200M_in : in STD_LOGIC;
       --! 280MHz clock input signal
       clk_280M_in          : in  STD_LOGIC;
       --! 160Mhz clk to read data into MGT
       TOB_TXOUTCLK         : in STD_LOGIC; 
       --! L1A signal input     
       L1A_in               : in  STD_LOGIC;
       --! BC Counter 
       BCN_ID_in            : in  STD_LOGIC_VECTOR (11 downto 0);
       --! 8b Extended L1A ID & 24b LIA_ID of the L1A Counter
       L1A_ID_in            : in  STD_LOGIC_VECTOR (31 downto 0);
       --! Ready signal from control FPGA to receive TOBs data
       ctrl_TOB_ready_in    : in  std_logic ;
       --! data is char to MGT
       TOB_out_to_MGT_is_char : out STD_LOGIC;
       --! Event TOBs 32b out to MGT                            
       TOB_out_to_MGT         : out STD_LOGIC_VECTOR (31 downto 0); 
       --! 8b Ext L1A ID from TTC input + 24b L1A ID of counter from TTC FIFO - inserted in the event header                            
       L1A_ID_Event_out       : out STD_LOGIC_VECTOR (31 downto 0);   -- L1A ID of current event
       --! latency pre load for TOB DRP wr address        
       pre_ld_TOB_wr_addr     : in STD_LOGIC_VECTOR (8 downto 0) ;
       --! latency pre load for e/g XTOB DRP wr address        
       pre_ld_eg_XTOB_wr_addr     : in STD_LOGIC_VECTOR (8 downto 0) ;
       --! latency pre load for tau XTOB DRP wr address        
       pre_ld_tau_XTOB_wr_addr     : in STD_LOGIC_VECTOR (8 downto 0) ;
       --! latency pre-load enable for DRPAM write address
       cntr_load_en           : in STD_LOGIC;
       --! number of DRP locations to read 1 or 2 or 3         
       DPR_locations_to_rd    : in STD_LOGIC_VECTOR (2 downto 0) ;      
       --! TOB data block FIFO flags       
       TOB_data_FIFO_flags    : out STD_LOGIC_VECTOR (11 downto 0);
       --! occupancy counter of e/g XTOB FIFO
       XTOB_eg_FIFO_data_count          : out STD_LOGIC_VECTOR (8 downto 0);
       --! e/g XTOB FIFO prog FULL threshold assert 
       XTOB_eg_FIFO_pFULL_THRESH_assert  : in  STD_LOGIC_VECTOR (8 downto 0); 
       --! e/g XTOB FIFO prog FULL threshold de-assert 
       XTOB_eg_FIFO_pFULL_THRESH_negate  : in  STD_LOGIC_VECTOR (8 downto 0);           
       --! tau XTOBs FIFO occupancy data count    
       XTOB_tau_FIFO_data_count         : out STD_LOGIC_VECTOR (8 downto 0);    -- occupancy of tau XTOB FIFO       
       --! tau XTOB FIFO prog FULL threshold assert 
       XTOB_tau_FIFO_pFULL_THRESH_assert : in  STD_LOGIC_VECTOR (8 downto 0);   -- tau XTOB FIFO
       --! tau XTOB FIFO prog FULL threshold de-assert 
       XTOB_tau_FIFO_pFULL_THRESH_negate : in  STD_LOGIC_VECTOR (8 downto 0);   -- tau XTOB FIFO 
       --! sorted TOBs FIFO occupancy data count
       T_TOB_FIFO_data_count           : out STD_LOGIC_VECTOR (8 downto 0);     -- sorted TOBs FIFO occupancy count
       --! sorted TOBs FIFO prog FULL threshold assert 
       T_TOBs_FIFO_pFULL_THRESH_assert  : in  STD_LOGIC_VECTOR (8 downto 0);    -- TOBs FIFO
       --! sorted TOBs FIFO prog FULL threshold de-assert 
       T_TOBs_FIFO_pFULL_THRESH_negate  : in  STD_LOGIC_VECTOR (8 downto 0);    -- TOBs FIFO
       --! Link output FIFO (before MGT) partial full flag assert threshold
       Link_output_FIFO_pFULL_THRESH_assert   : in  STD_LOGIC_VECTOR (12 downto 0);   -- Link_output_FIFO
       --! Link output FIFO (before MGT) partial full flag negate threshold
       Link_output_FIFO_pFULL_THRESH_negate   : in  STD_LOGIC_VECTOR (12 downto 0);   -- Link_output_FIFO
       --! Link output FIFO (before MGT) occupancy read data count
       Link_output_FIFO_rd_data_count         : out STD_LOGIC_VECTOR (12 downto 0);   -- occupancy of TOB output link MGT FIFO
       --! TOB/XTOB Readout SPY Memory register (read only)
       SPY_TOB_mem_wr_addr        : out STD_LOGIC_VECTOR (10 downto 0);   -- SPY memory wr_addr (read only)
       --! ipb_clk signal is input from master to slaves
       ipb_clk                : in std_logic ;
       --! IPBus signal coming from RAW SPY DPRAM to IPBus
	   ipbus_out_tob_dpram    : out ipb_rbus;
	   --! IPBus signal going to RAW SPY DPRAM
       ipbus_in_tob_dpram     : in  ipb_wbus
       
       );
    end TOBs_rdout;

--! @copydoc TOBs_rdout.vhd
architecture RTL of TOBs_rdout is

--************************** Register Declarations ****************************    
    signal  RST_i               : STD_LOGIC; 
    signal  TOB_FIFO_sw_rst_i   : STD_LOGIC ;                                   
    signal  clk_in_280M_i       : STD_LOGIC; 
    signal  rdout_T_TOB_209b_i  : STD_LOGIC_VECTOR (208 downto 0);  -- T_TOBs 209b = 7b TOB_BCN +4b error+6b valid +192b TOB data
    
    signal  T_TOB_full_i        : STD_LOGIC;
    signal  T_TOB_empty_i       : STD_LOGIC;    
    signal  T_TOB_valid_i       : STD_LOGIC;    
    signal  T_TOB_prog_full_i   : STD_LOGIC;
    signal  T_TOB_FIFO_data_count_i :  STD_LOGIC_VECTOR (8 downto 0);     -- occupancy of sorted TOB FIFO
    
    signal  DPR_XTOBs_eg_in_i       : array_8_of_182b;  -- XTOBs {array 8 of [(5*34b) + 5b valid] + XTOB_BCN}
    signal  DPR_XTOBs_eg_out_i      : array_8_of_182b;  -- XTOBs {array 8 of [(5*34b) + 5b valid] + XTOB_BCN}

    signal  DPR_XTOBs_tau_in_i      : array_8_of_182b;  -- XTOBs {array 8 of [(5*34b) + 5b valid] + XTOB_BCN}
    signal  DPR_XTOBs_tau_out_i     : array_8_of_182b;  -- XTOBs {array 8 of [(5*34b) + 5b valid] + XTOB_BCN}

    signal  link_err_4b_in_i        : STD_LOGIC_VECTOR (3 downto 0); -- 4b error flags CT + BN2 + TIE + EIE                   
    signal  ctrl_TOB_ready_i        : std_logic ;    --! Control FPGA Ready signal internal
    
    signal  L1A_rd_en_i             : std_logic ;
    signal  FIFO_wr_en_i            : std_logic ;
    signal  DRP_rd_en_i             : std_logic ;
    signal  pre_ld_wr_addr_TOB_i    : STD_LOGIC_VECTOR (8 downto 0); 
    signal  DPR_locations_to_rd_i   : STD_LOGIC_VECTOR (2 downto 0);

    signal  FIFO_rd_en_i            : std_logic := '0' ;
    signal  rdout_XTOB_eg_i         : array_8_of_182b;  -- XTOBs {array 8 of [(5*34b) + 5b valid] + XTOB_BCN} 
    signal  rdout_XTOB_tau_i        : array_8_of_182b;  -- XTOBs {array 8 of [(5*34b) + 5b valid] + XTOB_BCN}
    signal  rdout_TOBs_valid_i      : std_logic ;
    signal  valid_XTOBs_eg_i        : STD_LOGIC;
    signal  valid_XTOBs_tau_i       : STD_LOGIC;
    signal  frame_cntr_en_i         : std_logic;
    signal  pre_ld_wr_addr_XTOB_eg_i    : STD_LOGIC_VECTOR (8 downto 0); 
    signal  pre_ld_wr_addr_XTOB_tau_i   : STD_LOGIC_VECTOR (8 downto 0); 
    
    signal  XTOB_eg_full_i          : std_logic;
    signal  XTOB_eg_empty_i         : std_logic;
    signal  XTOB_eg_prog_full_i     : std_logic;
    
    signal  XTOB_tau_full_i          : std_logic;
    signal  XTOB_tau_empty_i         : std_logic;
    signal  XTOB_tau_prog_full_i     : std_logic;
    
    signal  TOB_empty_flag_i        : std_logic;
    signal  TOB_pfull_flag_i        : std_logic;
    signal  TOB_out_valid_i         : STD_LOGIC;
    signal  TOB_out_is_char_i       : STD_LOGIC; 
    signal  TOBs_out_i              :  STD_LOGIC_VECTOR (31 downto 0);  -- TOBs 32b out to FIFO 
    signal  FIFO_TOBs_in_i          :  STD_LOGIC_VECTOR (32 downto 0);
    signal  Link_output_FIFO_TOBs_out_i   :  STD_LOGIC_VECTOR (32 downto 0);   
    signal  TOB_out_to_MGT_i              :  STD_LOGIC_VECTOR (31 downto 0);  -- TOBs 32b out to MGT 
    signal  TOB_out_to_MGT_1dly           :  STD_LOGIC_VECTOR (31 downto 0);  -- TOBs 32b out to MGT 
    signal  TOB_data_out_MGT_i            :  STD_LOGIC_VECTOR (31 downto 0);  -- TOBs 32b out to MGT 
    signal  Link_output_FIFO_rd_en_i, TOB_out_to_MGT_is_char_i,TOB_out_to_MGT_is_char_1dly : STD_LOGIC;
    signal  Link_output_FIFO_TOB_valid_i                : STD_LOGIC;
    signal  T_TOB_out_valid_i     : STD_LOGIC;
    signal  T_TOB_out_valid_1dly  : STD_LOGIC;
    signal  TOB_out_char_MGT_i    : STD_LOGIC;
    
    signal  Link_output_FIFO_full       : std_logic;
    signal  Link_output_FIFO_empty      : std_logic;
    signal  Link_output_FIFO_prog_full  : std_logic;
    signal  Link_output_FIFO_prog_empty : std_logic;
    
    signal  TOB_data_FIFO_flags_i  : STD_LOGIC_VECTOR (11 downto 0);
    
    signal  reg1, reg2              : std_logic := '0' ;
    signal  frame_cntr_dec_en_i     : std_logic;
    signal  frame_counter_dec_en_i  : std_logic := '0' ;
    signal  frame_count_i           :  STD_LOGIC_VECTOR (11 downto 0);      -- Frame counter

    signal  tied_to_vcc_i           : std_logic;
    
    signal  SPY_TOB_mem_wr_addr_i      :  STD_LOGIC_VECTOR (10 downto 0) ;
    signal  SPY_TOB_mem_wr_addr_en_i   : STD_LOGIC ; 
    signal  SPY_FIFO_TOB_in_i          : STD_LOGIC_VECTOR (35 downto 0) ; 
    signal  SPY_TOB_mem_rd_data_i      : STD_LOGIC_VECTOR (35 downto 0) ;
    signal  enable_tob_spy_mem_wr      : std_logic ;
    signal  RST_tob_spy_mem_wr_addr_i  : std_logic ; 
    signal  tst_fsm_cntr_i, q1_int             : STD_LOGIC_VECTOR (31 downto 0) ;

    signal  BCN_FIFO_rd_en_i        : std_logic  ;
    signal  BCN_FIFO_full_i         : std_logic ;
    signal  BCN_FIFO_empty_i        : std_logic ;
    signal  BCN_FIFO_valid_i        : std_logic ;
    signal  BCN_FIFO_prog_full_i    : std_logic ;
    signal  BCN_FIFO_Data_in_i      : STD_LOGIC_VECTOR(46 downto 0) ;
    signal  BCN_FIFO_Data_out_i     : STD_LOGIC_VECTOR(46 downto 0) ;

    signal  FIFO_BCN_in_i           : STD_LOGIC_VECTOR(11 downto 0) ;
    signal  FIFO_L1A_ID_i           : STD_LOGIC_VECTOR(23 downto 0) ;
    signal  FIFO_L1A_ID_EXT_i       : STD_LOGIC_VECTOR(7 downto 0) ; 
    signal  Link_output_FIFO_data_count_i : STD_LOGIC_VECTOR(12 downto 0) ; 
--------------------------------
-- ILA signals
    signal  FIFO_TOBs_in_char_vec     : STD_LOGIC_VECTOR (0 downto 0) ;
    signal  L1A_in_vec                : STD_LOGIC_VECTOR (0 downto 0) ;
    signal  LO_fifo_wr_en_vec         : STD_LOGIC_VECTOR (0 downto 0) ;
    signal  LO_fifo_rd_en_vec         : STD_LOGIC_VECTOR (0 downto 0) ;
    signal  FIFO_TOBs_out_char_vec    : STD_LOGIC_VECTOR (0 downto 0) ;
    
    ------------------------------------------


--  ####### Mark signals  ########
 attribute keep       : string ;
 attribute max_fanout : integer;
 attribute keep of       TOB_FIFO_sw_rst_i : signal is "true" ;
 attribute max_fanout of TOB_FIFO_sw_rst_i : signal is 40;
 attribute keep of       FIFO_rd_en_i : signal is "true" ;
 attribute max_fanout of FIFO_rd_en_i : signal is 40;

--   #######################################

begin

    tied_to_vcc_i   <=   '1';

    clk_in_280M_i   <= clk_280M_in ;
    
    -- input signals
    
    RST_i           <= RST ;
    pre_ld_wr_addr_TOB_i    <= pre_ld_TOB_wr_addr ;     -- latency pre load for DRP wr address
    DPR_locations_to_rd_i   <= DPR_locations_to_rd ;    -- number of DRP locations to wr into FIFO 1 to 5
    link_err_4b_in_i        <= TOB_err_4b_in ;          -- 4b error flags CT + BN2 + TIE + EIE
    
    TOB_FIFO_sw_rst_i        <= RST_i OR TOB_FIFO_sw_rst ; -- rst by s/w or RST

    -- output signals
    -- FIFO flags assignments
    TOB_data_FIFO_flags <= TOB_data_FIFO_flags_i;
    
    TOB_data_FIFO_flags_i(0) <= T_TOB_empty_i   ;
    TOB_data_FIFO_flags_i(1) <= T_TOB_prog_full_i  ;   
    TOB_data_FIFO_flags_i(2) <= T_TOB_full_i ;
                                
    TOB_data_FIFO_flags_i(3) <= XTOB_eg_empty_i   ;
    TOB_data_FIFO_flags_i(4) <= XTOB_eg_prog_full_i ;   
    TOB_data_FIFO_flags_i(5) <= XTOB_eg_full_i ; 
    
    TOB_data_FIFO_flags_i(6) <= XTOB_tau_empty_i  ;    
    TOB_data_FIFO_flags_i(7) <= XTOB_tau_prog_full_i ;   
    TOB_data_FIFO_flags_i(8) <= XTOB_tau_full_i ;

    TOB_data_FIFO_flags_i(9)  <= Link_output_FIFO_empty  ;       
    TOB_data_FIFO_flags_i(10) <= Link_output_FIFO_prog_full ;   
    TOB_data_FIFO_flags_i(11) <= Link_output_FIFO_full ; 

    BCN_FIFO_Data_in_i <= "000" & BCN_ID_in & L1A_ID_in ; -- write L1A number and BC number into FIFO
    
    SPY_TOB_mem_wr_addr      <= SPY_TOB_mem_wr_addr_i ; -- SPY memory address fo iPus Readout register

    
    TOB_out_to_MGT          <=  TOB_data_out_MGT_i;         -- event TOBs to MGT
    TOB_out_to_MGT_is_char  <=  TOB_out_char_MGT_i;  -- TOB data is char
    L1A_ID_Event_out <= FIFO_L1A_ID_EXT_i & FIFO_L1A_ID_i ; -- 8b L1A ID Extended + 24b L1A ID of the current event
    

--! This FDCE regitsters the cFPGA Ready input signal to receive TOB?XTOB Event Data. 
U0_FDCE_inst : FDCE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q    => ctrl_TOB_ready_i,      -- Data output
      C    => clk_in_280M_i,      -- Clock input
      CE   => '1',    -- Clock enable input
      CLR  => TOB_FIFO_sw_rst_i ,  -- async rst by s/w or RST from 40MHz MMCM lock signal
      D    => ctrl_TOB_ready_in       -- Data input
   ); 

--! @details
--! \verbatim
--! This is TTC FIFO for TOB/XTOB Readour, which stores the following values:.
--! bits 46:44 = not used
--! bits 43:32 = Bunch Crossing Number - generated in pFPGA
--! bits 31:24 = Extended L1A - received from CFPGA
--! bits 23:0  = L1A_ID - received from cFPGA
--! \endverbatim
U0_FIFO_BCN_L1A : FIFO_47b_512          -- the flags from this FIFO is used
    PORT MAP (
      rst           => TOB_FIFO_sw_rst_i,   -- sys RST OR RAW_FIFO_sw_rst
      wr_clk        => clk_40M_in, 
      rd_clk        => clk_in_280M_i,
      din           => BCN_FIFO_Data_in_i ,
      wr_en         => L1A_in,
      rd_en         => BCN_FIFO_rd_en_i,
      dout          => BCN_FIFO_Data_out_i,     -- BCN + EXT_L1A_ID + L1A_ID  44b = 12b + 8b + 24b 
      valid         => BCN_FIFO_valid_i ,
      prog_full_thresh_assert => T_TOBs_FIFO_pFULL_THRESH_assert,    --  9b
      prog_full_thresh_negate => T_TOBs_FIFO_pFULL_THRESH_negate,
      prog_full     => BCN_FIFO_prog_full_i,
      full          => BCN_FIFO_full_i,
      empty         => BCN_FIFO_empty_i
    );

    FIFO_L1A_ID_i       <= BCN_FIFO_Data_out_i(23 downto 0) ;   -- 24b L1A ID
    FIFO_L1A_ID_EXT_i   <= BCN_FIFO_Data_out_i(31 downto 24) ;  -- 8b L1A ID Extended
    FIFO_BCN_in_i       <= BCN_FIFO_Data_out_i(43 downto 32) ;  -- 12b BCN ID
    
--! \verbatim
--! This T_TOBs_sorting module receives 7 x 32-bit TOBs together with 7 valid signals in series, 
--! and sorts them into a 6 x 32-bit parallel word and 6 bit valid word to store in Circular DPRAM.
--! The 7th TOB is ignored as a trailer is added to transfer the data to TOPO.
--! In order to be able to read multi-slices, the TOBs and XTOBs must be converted into long parallel data words.
--!
--! This module is only instanstiated for Process FPGA 1 and 2, and is disabled for Process FPGA 3 and 4.
--! Only Process FPGA 1 and 2 transmit e/g and tau events to TOPO, and the off-line Readout,
--! but Process FPGA 3 and 4 do not transmit to TOPO instead they send their TOBs to Process FPGA 1 and 2.
--! Process FPGA 3 and 4 transmit XTOB only events to the Control FPGA.
--! \endverbatim
U1_TOBs_sorting :  entity TOB_rdout_lib.T_TOBs_sorting 
    generic map(FPGA_NUMBER => FPGA_NUMBER)     
    Port map ( 
        L1A_in                 =>  L1A_in,
        TOB_FIFO_sw_rst        =>  TOB_FIFO_sw_rst_i ,  -- RST OR TOB_FIFO_sw_rst
        TOBs_32b_in            =>  TOBs_32b_in ,        -- sorted TOBs 32b * 7 is series
        TOBs_sync_in           =>  TOBs_sync_in ,       -- sorted TOB start signal
        TOBs_valid_flg_in      =>  TOBs_valid_flg_in,   -- sorted TOB write signal
        ALGO_TOB_BCN_in        =>  OUT_TOB_BCN,         -- sorted TOB BC_ID with delay through ALGO/sorting block
        FIFO_rd_en             =>  FIFO_rd_en_i ,
        link_err_4b_in         =>  link_err_4b_in_i,    -- 4b error flags CT + BN2 + TIE + EIE
        clk_280M_in            =>  clk_in_280M_i,
        pre_ld_wr_addr_TOB     =>  pre_ld_wr_addr_TOB_i,
        DPR_locations_to_rd    =>  DPR_locations_to_rd_i,    -- number of DRP locations to read 1 to 5
        TOBs_FIFO_pFULL_THRESH_assert   => T_TOBs_FIFO_pFULL_THRESH_assert,   -- connect to relevant register
        TOBs_FIFO_pFULL_THRESH_negate   => T_TOBs_FIFO_pFULL_THRESH_negate,   -- connect to relevant register
        rdout_T_TOB_209b       =>  rdout_T_TOB_209b_i,       -- sorted TOBs FIFO o/p 209b = 7b TOB_BCN +4b error+6b valid +192b TOB data
        TOBs_FIFO_data_count   =>  T_TOB_FIFO_data_count ,   -- sorted TOBs FIFO occupancy count
        TOBs_data_valid        =>  T_TOB_valid_i,    
        TOBs_data_full         =>  T_TOB_full_i,     
        TOBs_data_empty        =>  T_TOB_empty_i,    
        TOBs_data_prog_full    =>  T_TOB_prog_full_i
        );

    pre_ld_wr_addr_XTOB_eg_i <= pre_ld_eg_XTOB_wr_addr ; -- set correct value register in XML

--! \verbatim        
--! This is the XTOB e/g sorting module.
--! It receives 20 x 64-bit TOBs together with 40 valid signal in series, 
--! and sorts them into a 80 x 32-bit parallel word and 40 bit valid word to store in Circular DPRAM.
--! \endverbatim
U2_XTOBs_eg_sorting :  entity TOB_rdout_lib.XTOBs_sorting
    generic map(FPGA_NUMBER => FPGA_NUMBER)
    Port map ( 
        L1A_in                 =>  L1A_in,
        XTOB_FIFO_sw_rst    =>  TOB_FIFO_sw_rst_i,       -- RST OR TOB_FIFO_sw_rst
        XTOB_512b_in        =>  XTOB_eg_512b_in,          --  XTOB e/g  data in 64b * 8
        XTOB_Valid_flg_in   =>  XTOB_eg_Valid_flg_in,     -- 8b XTOB e/g has valid data
        XTOB_sync_in        =>  XTOB_eg_sync_in,          -- XTOB e/g  sync signal
        ALGO_XTOB_BCN_in     =>  OUT_XTOB_BCN,         -- sorted TOB BCN with delay through ALGO/sorting block
        pre_ld_wr_addr_XTOB  =>  pre_ld_wr_addr_XTOB_eg_i,
        DPR_locations_to_rd  =>  DPR_locations_to_rd_i,    -- number of DRP locations to read 1 to 5
        FIFO_rd_en           =>  FIFO_rd_en_i ,
        clk_200M_in          =>  clk_200M_in   ,       -- i/p                                            
        clk_280M_in          =>  clk_in_280M_i,
        XTOB_FIFO_pFULL_THRESH_assert => XTOB_eg_FIFO_pFULL_THRESH_assert ,
        XTOB_FIFO_pFULL_THRESH_negate => XTOB_eg_FIFO_pFULL_THRESH_negate ,
        FIFO_XTOB_data_out      =>  rdout_XTOB_eg_i,            -- e/g XTOBs output of fifo - XTOBs (5*34b) + 5b valid + 7b XTOB_BCN
        XTOB_FIFO_data_count    =>  XTOB_eg_FIFO_data_count ,   -- occupancy of e/g XTOB FIFO
        XTOB_FIFO_valid         =>  valid_XTOBs_eg_i,
        XTOB_FIFO_full          =>  XTOB_eg_full_i,     
        XTOB_FIFO_empty         =>  XTOB_eg_empty_i,    
        XTOB_FIFO_prog_full     =>  XTOB_eg_prog_full_i
        );                         

    pre_ld_wr_addr_XTOB_tau_i <= pre_ld_tau_XTOB_wr_addr ; -- set correct value register in XML

--! \verbatim    
--! This is the XTOB tau sorting module.
--! It receives 20 x 64-bit TOBs together with 40 valid signal in series, 
--! and sorts them into a 80 x 32-bit parallel word and 40 bit valid word to store in Circular DPRAM.
--! \endverbatim
U3_XTOBs_tau_sorting :  entity TOB_rdout_lib.XTOBs_sorting
    generic map(FPGA_NUMBER => FPGA_NUMBER)
    Port map ( 
        L1A_in                 =>  L1A_in,
        XTOB_FIFO_sw_rst    =>  TOB_FIFO_sw_rst_i,   -- RST OR TOB_FIFO_sw_rst
        XTOB_512b_in        =>  XTOB_tau_512b_in,   --  XTOB e/g  data in 64b * 8
        XTOB_Valid_flg_in   =>  XTOB_tau_Valid_flg_in,     -- 8b XTOB e/g has valid data (x 5)
        XTOB_sync_in        =>  XTOB_tau_sync_in,     -- XTOB e/g  sync signal
        ALGO_XTOB_BCN_in     =>  OUT_XTOB_BCN,         -- sorted TOB BCN with delay through ALGO/sorting block
        pre_ld_wr_addr_XTOB  =>  pre_ld_wr_addr_XTOB_tau_i,
        DPR_locations_to_rd  =>  DPR_locations_to_rd_i,    -- number of DRP locations to read 1 to 5
        FIFO_rd_en           =>  FIFO_rd_en_i ,
        clk_200M_in          =>  clk_200M_in   ,       -- i/p                                            
        clk_280M_in          =>  clk_in_280M_i,
        XTOB_FIFO_pFULL_THRESH_assert => XTOB_tau_FIFO_pFULL_THRESH_assert ,
        XTOB_FIFO_pFULL_THRESH_negate => XTOB_tau_FIFO_pFULL_THRESH_negate ,
        FIFO_XTOB_data_out      =>  rdout_XTOB_tau_i  ,         -- XTOBs tau output of fifo - XTOBs (5*34b) + 5b valid + 7b XTOB_BCN
        XTOB_FIFO_data_count    =>  XTOB_tau_FIFO_data_count ,  -- occupancy of tau XTOB FIFO
        XTOB_FIFO_valid         =>  valid_XTOBs_tau_i,          
        XTOB_FIFO_full          =>  XTOB_tau_full_i,            
        XTOB_FIFO_empty         =>  XTOB_tau_empty_i,           
        XTOB_FIFO_prog_full     =>  XTOB_tau_prog_full_i        
        );                         

---- this FSM only control XTOB data wr to DPRAM & FIFO
--U4_XTOBs_wr_FSM :  entity TOB_rdout_lib.fsm_TOB_wr_to_FIFO
--    PORT map (
--        clk_280M               => clk_in_280M_i ,
--        TOB_FIFO_sw_rst        => TOB_FIFO_sw_rst_i,    -- RST OR TOB_FIFO_sw_rst
--        L1A_in                 => L1A_in ,
--        DPR_locations_to_rd    => DPR_locations_to_rd_i ,   -- number of DRP locations to read 1 to 5
--        DRP_rd_en              => DRP_rd_en_i ,
--        FIFO_wr_en             => FIFO_wr_en_i
--       );

    TOB_empty_flag_i   <= XTOB_eg_empty_i OR T_TOB_empty_i OR BCN_FIFO_empty_i     ;
    TOB_pfull_flag_i   <= XTOB_eg_prog_full_i OR T_TOB_prog_full_i OR BCN_FIFO_prog_full_i ;
--    TOB_empty_flag_i   <= T_TOB_empty_i ;   -- as these are loaded in parallel need only the last FIFO to receive data.
--    TOB_pfull_flag_i   <= T_TOB_prog_full_i ;

--! \verbatim 
--! Generate FSM to write TOBs and XTOBS into an event packet for pFPGA 1 and 2
--! Else Generate FSM to write only XTOBS into an event packet for pFPGA 3 and 4
--! \endverbatim
    U6_rd_mux_fsm :  entity TOB_rdout_lib.fsm_TOBs_to_muxPISO
        generic map(FPGA_NUMBER => FPGA_NUMBER)
        PORT map (
            RST                        => RST_i ,
            tst_fsm_cntr               => tst_fsm_cntr_i ,
            TOB_FIFO_sw_rst            => TOB_FIFO_sw_rst_i,    -- RST OR TOB_FIFO_sw_rst
            hw_addr                    => hw_addr ,             -- FPGA number
            rdout_T_TOB_209b_in        => rdout_T_TOB_209b_i,   -- sorted TOBs FIFO o/p 209b = 7b TOB_BCN +4b error+6b valid +192b TOB data
            valid_T_TOB_in             => T_TOB_valid_i ,       -- indicated input sorted TOB data is valid
            TOB_type_in                => TOB_type_in ,         -- TOB Type 0 = em, 1 = tau
            XTOBs_eg_in                => rdout_XTOB_eg_i ,     -- XTOBs (5*34b) + 5b valid + 7b XTOB_BCN
            valid_XTOBs_eg_in          => valid_XTOBs_eg_i,
            XTOBs_tau_in               => rdout_XTOB_tau_i ,    -- XTOBs (5*34b) + 5b valid + 7b XTOB_BCN
            valid_XTOBs_tau_in         => valid_XTOBs_tau_i ,
            clk_280M_in                => clk_in_280M_i , 
            TOB_FIFO_empty_in          => TOB_empty_flag_i , 
            TOB_prog_full_flag_in      => TOB_pfull_flag_i, 
            DPR_locations_to_rd        => DPR_locations_to_rd_i ,   -- number of DRP locations to read 1 to 5
            rdout_expected             => '0' ,     -- was rdout_expected ,          -- RAW readout expected upon Error flag
            FIFO_BCN_in                => FIFO_BCN_in_i ,   -- 12b BCN ID from counter
            FIFO_L1A_ID                => FIFO_L1A_ID_i ,   -- 24b L1A ID from TTC info
            FIFO_L1A_ID_EXT            => FIFO_L1A_ID_EXT_i ,   -- 8b L1A ID Extended from TTC info
            BCN_FIFO_valid_in          => BCN_FIFO_valid_i ,    -- data from TTC FIFO is valid
            LO_FIFO_prog_full_in       => Link_output_FIFO_prog_full,
            LO_FIFO_data_count_in      => Link_output_FIFO_data_count_i,
            frame_cntr_en              => frame_cntr_en_i ,         -- enable frame counter count up
            BCN_FIFO_rd_en_out         => BCN_FIFO_rd_en_i ,        -- rd enabto BCN & L1A FIFO
            TOB_rdout_fifo_rd_en_out   => FIFO_rd_en_i ,            -- rd enable to TOB & XTOB FIFOs
            TOB_out_valid              => TOB_out_valid_i ,         -- sorted TOBs data valid to U7_Link_output_FIFO
            TOB_out_is_char            => TOB_out_is_char_i,        -- sorted TOBs is CHAR to U7_Link_output_FIFO
            TOBs_out                   => TOBs_out_i                -- sorted TOBs valid to U7_Link_output_FIFO
            );  

    FIFO_TOBs_in_i <= TOB_out_is_char_i & TOBs_out_i;  -- 1b char and 32b data

--! @details
--! \verbatim 
--! This is TOB/XTOB Link Output FIFO, which stores complete TOB/XTOB events (frames) ready to be transmitted to cFPGA via an MGT link at 11.2Gbps.
--! The output of FIFO is 32-bit data word, and 1-bit data is CHAR, which goes directly to MGT under FSM control.
--!
--! The Frames are only transmitted when cFPGA indicates it is ready to receive data by setting ctrl_TOB_ready signal to 1.
--! If ctrl_TOB_ready signal is set to 0 by cFPGA, this indicates cFPGA is not ready to receive data,
--! in this case events accumulate in Link Output FIFO until occupancy reaches a pre-define level of prog_FULL,
--! at this time, Link Output FIFO stops receiving data.
--!
--! Writing to LO FIFO is controlled by fsm_TOBs_to_muxPISO (OR fsm_XTOBs_to_muxPISO) FSM
--! Reading from LO FIFO is controlled by FIFO_to_MGT_TOB_FSM FSM
--! \endverbatim 
U7_Link_output_FIFO : FIFO_33b_8192
    PORT MAP (
        srst        => TOB_FIFO_sw_rst_i,   -- RST OR TOB_FIFO_sw_rst
        clk         => clk_in_280M_i,
        din         => FIFO_TOBs_in_i,
        wr_en       => TOB_out_valid_i,
--        rd_clk      => TOB_TXOUTCLK,   -- 160M clk to rd data to cntl FPGA at 6.4Gbps change to 280MHz
        rd_en       => Link_output_FIFO_rd_en_i,        
        dout        => Link_output_FIFO_TOBs_out_i,
        full        => Link_output_FIFO_full,
        empty       => Link_output_FIFO_empty,
        prog_full   => Link_output_FIFO_prog_full,
        valid       => Link_output_FIFO_TOB_valid_i,
        prog_full_thresh_assert     => Link_output_FIFO_pFULL_THRESH_assert ,
        prog_full_thresh_negate     => Link_output_FIFO_pFULL_THRESH_negate ,
--        rd_data_count => Link_output_FIFO_rd_data_count ,    -- occupancy of TOB output link MGT FIFO
        data_count => Link_output_FIFO_data_count_i      -- occupancy of TOB output link MGT FIFO
    );

    Link_output_FIFO_rd_data_count <= Link_output_FIFO_data_count_i;
    
    FIFO_TOBs_in_char_vec(0) <= FIFO_TOBs_in_i(32) ;
    L1A_in_vec(0) <= L1A_in ;
    LO_fifo_wr_en_vec(0) <= TOB_out_valid_i ;
    LO_fifo_rd_en_vec(0) <= Link_output_FIFO_rd_en_i ;
    FIFO_TOBs_out_char_vec(0) <= Link_output_FIFO_TOBs_out_i(32) ;

--U2_ila_LO_FIFO_TOB_in : ila_ipbus_fabric_rd_wr
--PORT MAP (
--	clk    =>  clk_in_280M_i,     
--	probe0 =>  FIFO_TOBs_in_i(31 downto 0),       -- 32b    
--	probe1 =>  tst_fsm_cntr_i,  -- 32b 
--	probe2 =>  FIFO_TOBs_in_char_vec,   -- 1b
--	probe3 =>  L1A_in_vec,           -- 1b
--	probe4 =>  LO_fifo_wr_en_vec,   -- 1b
--	probe5 =>  (others => '0'),  -- 32b 
--	probe6 =>  (others => '0'),        -- 32b
--	probe7 =>   (others => '0'),             -- 1b
--	probe8 =>   (others => '0') ,       -- 1b
--    probe9 =>   (others => '0')              -- 1b
--);  


--! \verbatim
--! This FSM reads TOB/XTOB frames from Link Output FIFO and writes into MGT to transmit to cFPGA.
--! This FSM handles one full frame at a time without pausing.
--! It monitors the TOB frame counter to find out if there are Frames waiting to be transmitted to cFPGA.
--! The Frames are only transmitted when cFPGA indicates it is ready to receive data by setting ctrl_TOB_ready signal to 1.
--! If ctrl_TOB_ready signal is set to 0 by cFPGA, this indicates cFPGA is not ready to receive data,
--! in this case events accumulate in Link Output FIFO until occupancy reaches a pre-define prog_FULL level,
--! at this time, Link Output FIFO stops receiving data.
--! \endverbatim
U8_TOB_Link_output_FIFO_FSM : entity TOB_rdout_lib.FIFO_to_MGT_TOB_FSM
    Port map ( 
--       RST                      =>  RST_i ,
       TOB_FIFO_sw_rst          =>  TOB_FIFO_sw_rst_i,  -- RST OR TOB_FIFO_sw_rst
       clk_in_280M              =>  TOB_TXOUTCLK,      -- 160M clk to rd data to cntl FPGA at 6.4Gbps change to 280MHz
       ctrl_TOB_ready_in        =>  ctrl_TOB_ready_i ,
       frame_counter            =>  frame_count_i,
       FIFO_MGT_TOBs            =>  Link_output_FIFO_TOBs_out_i,
       FIFO_MGT_TOB_valid       =>  Link_output_FIFO_TOB_valid_i ,
       MGT_FIFO_full            =>  Link_output_FIFO_full,
       MGT_FIFO_empty           =>  Link_output_FIFO_empty,
       MGT_FIFO_prog_full       =>  Link_output_FIFO_prog_full,
       FIFO_MGT_rd_en           =>  Link_output_FIFO_rd_en_i,   -- output
       frame_counter_dec_en     =>  frame_cntr_dec_en_i,
       T_TOBs_out               =>  TOB_out_to_MGT_i,         -- event TOBs to MGT
       T_TOB_is_char            =>  TOB_out_to_MGT_is_char_i,  -- TOB data is char
       T_TOB_out_valid          =>  T_TOB_out_valid_i 
       );

--! @details
--! \verbatim 
--! The TOB_frame_counter is a 12-bit generic up/down counter to count the number of complete Frames in Link Output FIFO to be transmitted to cFPGA.
--! When one complete Frame (event) is written to LO FIFO, this counter is incremented.
--! When one complete Frame (event) is read out LO FIFO, this counter is decremented.
--! If the counter is ZERO, no data is available to be send to cFPGA.
--! \endverbatim 
U9_frame_counter : entity TOB_rdout_lib.cntr_up_dn_generic
    generic map (
         width  => 12
        )
    Port map ( 
       CE   =>  tied_to_vcc_i  ,
       CLK  =>  clk_in_280M_i ,
       RST  =>  TOB_FIFO_sw_rst_i , -- RST OR TOB_FIFO_sw_rst
       UP   =>  frame_cntr_en_i ,
       DOWN =>  frame_cntr_dec_en_i ,   -- this is a 280 MHz signal
       Q    =>  frame_count_i 
       );

--! delay data to SPY RAM
U10_dec_frm_cntr : process (clk_in_280M_i)
    begin
        if clk_in_280M_i'event and clk_in_280M_i = '1' then
            TOB_out_to_MGT_1dly             <= TOB_out_to_MGT_i;
            TOB_data_out_MGT_i              <= TOB_out_to_MGT_1dly;        -- data to MGT
            TOB_out_to_MGT_is_char_1dly     <= TOB_out_to_MGT_is_char_i;
            TOB_out_char_MGT_i              <= TOB_out_to_MGT_is_char_1dly;
            T_TOB_out_valid_1dly            <= T_TOB_out_valid_i;
        end if;
    end process;

    SPY_TOB_mem_wr_addr_en_i <= (T_TOB_out_valid_1dly AND enable_tob_spy_mem_wr);

--! @details
--! \verbatim 
--! The TOB_SPY_mem is a Dual Port Memory with IPBus interface.
--! It is possible to read/write to/from DPRAM using the IPBus interface.
--! The SPY Memory captures the data that is read out of Link Output FIFO to MGT.
--! The SPY Memory is designed to fill up and then stops accepting new data, 
--! therefore the old data is not written over.
--! \endverbatim 
U12_TOB_SPY_mem : entity ipbus_lib.ipbus_dpram
	generic map(
		ADDR_WIDTH => 11      -- DPRAM 512 locations
	)
	port map(
		clk		=> ipb_clk,
		rst		=> '0'  ,
		ipb_in 	=> ipbus_in_tob_dpram ,    -- i/p signal going to TOB SPY DPRAM
		ipb_out	=> ipbus_out_tob_dpram ,    -- o/p signal coming from TOB SPY DPRAM
		rclk	=> TOB_TXOUTCLK ,
		we		=> SPY_TOB_mem_wr_addr_en_i ,   -- wr addr en
		d		=> TOB_out_to_MGT_1dly ,     -- wr data
		q		=> q1_int ,
		addr	=> SPY_TOB_mem_wr_addr_i     -- wr addr
	);

    RST_tob_spy_mem_wr_addr_i <= (RST_i OR RST_spy_mem_wr_addr) ;

--! @details
--! \verbatim 
--! The spy_mem_wr_addr is a generic counter that provides the write address to RAW SPY Memory.
--! The spy memory address is 11-bits wide, so the depth of memory is 2048.
--! The write address of the TOB_SPY_mem is reset to ZERO by asserting RST_spy_mem_wr_addr or system RST signals.
--! The signal RST_spy_mem_wr_addr is bit 5 of register rdout_pulse_reg.
--! The rdout_pulse_reg register is pulsed, and after 1 clock cycle all bits are reset to Zeros.
--! \endverbatim 
U13_spy_mem_wr_addr : entity TOB_rdout_lib.cntr_generic
    generic map (
         width  => 11
        )
    Port map ( 
       CE   =>  T_TOB_out_valid_i  ,  -- if a valid data, then increment the address
       CLK  =>  TOB_TXOUTCLK ,
       RST  =>  RST_tob_spy_mem_wr_addr_i ,
       Q    =>  SPY_TOB_mem_wr_addr_i 
       );

--! @details
--! \verbatim
--! Process U14_stop_wr disables the write enable signal to Spy Memory when it is full.
--! \endverbatim 
U14_stop_tob_wr : Process (TOB_TXOUTCLK)
   begin
     if rising_edge (TOB_TXOUTCLK) then
      if (RST_tob_spy_mem_wr_addr_i = '1') then  
         enable_tob_spy_mem_wr <= '1' ;   -- if not full, en data wr
      else
         if SPY_TOB_mem_wr_addr_i = "11111111111" then      -- if terminal count
          enable_tob_spy_mem_wr <= '0' ;     -- disable memory wr to prevent data corruption
         end if;
      end if;
   end if ;
   end process U14_stop_tob_wr;       
       
end RTL;
