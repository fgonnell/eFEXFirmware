library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
Library UNISIM;
use UNISIM.vcomponents.all;   

 entity  SRL16E_33 is
   port (
      clk      : in std_logic;
      srl_en   : in std_logic;
	  address  : in std_logic_vector (3 downto 0);
	  data_in  : in std_logic_vector (32 downto 0);
	  data_out : out std_logic_vector (32 downto 0)
	  );
	  
  end  SRL16E_33;
   
   
 architecture Behavioral of SRL16E_33 is  
 signal d_i,q_i :std_logic_vector (32 downto 0);
 
begin   
  
data_out <= q_i;
d_i <= data_in;  

  shift_mux33 : for i in 0 to 32
   generate 
   SRL16E_inst_33 : SRL16E
   generic map (
      INIT => X"0000")
   port map (
      Q   => q_i(i)       , -- SRL data output
      A0  => address(0), -- Select(0) input         
      A1  => address(1), -- Select(1) input  
      A2  => address(2),  -- Select(2) input 
      A3  => address(3),  -- Select(3) input  
      CE  => srl_en,        -- Clock enable input
      CLK => CLK,        -- Clock input
      D   => d_i(i)      -- SRL data input
	  );
 end generate  shift_mux33 ;
   
  

end Behavioral;  