library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

library infrastructure_lib;
use infrastructure_lib.ipbus_decode_L1CaloEfexProcessor.all;
use infrastructure_lib.ProcessorFPGAPackage.all;

library ipbus_lib;
use ipbus_lib.ipbus.all;
use ipbus_lib.ipbus_trans_decl.all;
use ipbus_lib.mac_arbiter_decl.all;
library unisim;
use unisim.VComponents.all;

entity top_golden_proc_fpga is
  generic(
    --! Integer used to distinguish different FPGAs having a slightly different firmware
    FLAVOUR     : integer                       := 0;
    --! Date format DDMMYYYY in decimal
    GLOBAL_DATE : std_logic_vector(31 downto 0) := x"00000000";
    --! Time format  00HHMMSS in decimal
    GLOBAL_TIME : std_logic_vector(31 downto 0) := x"00000000";
    --! Short 7-digit git SHA of the repository
    GLOBAL_SHA  : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the repository (format: MMmmcccc in hex)
    GLOBAL_VER  : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the top folder: list files, tcl file
    TOP_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the top folder, see TOP_SHA
    TOP_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the XMLs
    XML_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the XMLs
    XML_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the Hog submodule
    HOG_SHA : std_logic_vector(31 downto 0) := x"00000000";
    HOG_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the Hog submodule
    CON_SHA : std_logic_vector(31 downto 0) := x"00000000";
    CON_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA
    INFRASTRUCTURE_LIB_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of infrastructure library (format: MMmmcccc in hex)
    INFRASTRUCTURE_LIB_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the ipbus submodule
    IPBUS_LIB_SHA : std_logic_vector(31 downto 0) := x"00000000");

  port(
    gt_clk125_p     : in  std_logic;
    gt_clk125_n     : in  std_logic;
    ttc_clk_p       : in  std_logic;
    ttc_clk_n       : in  std_logic;
    master_rx_data  : in  std_logic_vector (9 downto 0);
    master_tx_pause : in  std_logic;
    master_tx_data  : out std_logic_vector (9 downto 0);
    flash_csn       : out std_logic;
    flash_mosi      : out std_logic;
    flash_miso      : in  std_logic;
    flash_led       : out std_logic;
    fpga_geo_addr   : in  std_logic_vector (1 downto 0);  --! geographical address of the fpga
    hardware_addr   : in  std_logic_vector (7 downto 0)

    );

end top_golden_proc_fpga;

architecture Behavioral of top_golden_proc_fpga is

  signal clkin, clk125, clk125_fr, mac_clk, ipb_clk, flash_clk, onehz    : std_logic;
  signal rst_ipb, rst_ipb_ctrl, rst_macclk, rsto_eth, rst, srst, rst_125 : std_logic            := '1';
  signal Module_ID, status                                               : std_logic_vector(31 downto 0);
  signal trigger_reconfig, reconfig, locked, nuke                        : std_logic;
  signal rctr                                                            : unsigned(3 downto 0) := "0000";
  signal nuke_i, nuke_d, nuke_d2, eth_done, eth_locked                   : std_logic            := '0';
  signal d17, d17_d, flash_csn_int, flash_mosi_int                       : std_logic;
  signal control_reg, pulse_reg, reconfig_reg                            : std_logic_vector (31 downto 0);
  signal flash_select                                                    : std_logic_vector (1 downto 0);
  signal probe                                                           : std_logic_vector (23 downto 0);
  signal master_rx_data_int, master_tx_data_int                          : std_logic_vector (9 downto 0);
  signal master_tx_pause_int                                             : std_logic;

  -- additional ipbus signalds
  signal ipbw    : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal ipbr    : ipb_rbus_array(N_SLAVES-1 downto 0);
  signal ipb_in  : ipb_rbus;
  signal ipb_out : ipb_wbus;

  component clk_wiz_0
    port
      (                         -- Clock in ports
        -- Clock out ports
        mac_clk   : out std_logic;
        ipb_clk   : out std_logic;
        -- Status and control signals
        reset     : in  std_logic;
        locked    : out std_logic;
        clk_in1_p : in  std_logic;
        clk_in1_n : in  std_logic
        );
  end component;



begin

--  moddule status assignement
  Module_ID <= (31           => '0',                                        -- user (1) / golden image (0)
                30 downto 28 => std_logic_vector(to_unsigned(FLAVOUR, 3)),  -- FPGA flavour, 0 for control FPGA
                27 downto 26 => fpga_geo_addr,
                25 downto 20 => "000000",                                   -- space for control serial number
                19 downto 16 => "0000",                                     -- shelf address
                15 downto 12 => hardware_addr(3 downto 0),                  -- slot address
                11 downto 0  => X"efe"                                      -- module ID
                );

  status <= x"000000" & hardware_addr;


  trigger_reconfig <= reconfig and locked;
  reconfig         <= reconfig_reg(30);  -- if active it will reconfigure the spi flash.

  clkdiv : entity work.ipbus_clock_div port map(
    clk => mac_clk,
    d17 => d17,
    d28 => onehz
    );

  process(mac_clk)
  begin
    if rising_edge(mac_clk) then
      d17_d <= d17;
      if d17 = '1' and d17_d = '0' then
        rst     <= nuke_d2 or not locked;
        nuke_d  <= nuke_i;      -- ~1ms time bomb (allows return packet to be sent)
        nuke_d2 <= nuke_d;
      end if;
    end if;
  end process;


  srst <= '1' when rctr /= "0000" else '0';

  process(ipb_clk)
  begin
    if rising_edge(ipb_clk) then
      rst_ipb <= rst;
      nuke_i  <= nuke;
      if srst = '1' then        -- or  hard_rst = '1' then
        rctr <= rctr + 1;
      end if;
    end if;
  end process;

  process(mac_clk)
  begin
    if rising_edge(mac_clk) then
      rst_125 <= rst;           --or not eth_done;
    end if;
  end process;

  rst_macclk <= rst_125;

  clk_gen : clk_wiz_0
    port map (
      -- Clock in ports
      clk_in1_p => gt_clk125_p,
      clk_in1_n => gt_clk125_n,
      -- Clock out ports  
      mac_clk   => mac_clk,
      ipb_clk   => ipb_clk,
      -- Status and control signals                
      reset     => '0',         --hard_rst,
      locked    => locked
      );

  U_1 : entity infrastructure_lib.proc_fpgas
    generic map(IPBUSPORT => F_IPBUS_PORT_N(F_FPGA_NUMBER(FLAVOUR)))
    port map (
      ipb_clk         => ipb_clk,
      mac_clk         => mac_clk,
      rst_ipb         => rst_ipb,
      rst_macclk      => rst_macclk,
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      master_rx_data  => master_rx_data,
      master_tx_pause => master_tx_pause_int,
      master_tx_data  => master_tx_data
      );

 process(mac_clk)
  begin
    if rising_edge(mac_clk) then
      master_tx_pause_int <= master_tx_pause;
    end if;
  end process;

  -- ipbus slaves live in the entity below, and can expose top-level ports
  -- The ipbus fabric is instantiated within.

-----------------------------------------------------------------------------------------------------
-- global decoder at the top level.it implements the top xml
-----------------------------------------------------------------------------------------------------

  global_fabric : entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV      => N_SLAVES,  -- defined in ipbus_decode_top_address_table
                SEL_WIDTH => ipbus_sel_width)
    port map(
      ipb_in          => ipb_out,
      ipb_out         => ipb_in,
      sel             => ipbus_sel_L1CaloEfexProcessor(ipb_out.ipb_addr),
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      );

---------------------------------------------------------------------------------
----- common id registers slave
---------------------------------------------------------------------------------------

  common_ID : entity infrastructure_lib.common_id_registers
    port map (
      ipb_clk     => ipb_clk,
      ipb_rst     => rst_ipb,
      ipb_in      => ipbw(N_SLV_COMMON_ID_VERSION),
      ipb_out     => ipbr(N_SLV_COMMON_ID_VERSION),
      Module_ID   => Module_ID,
      xml_version => XML_VER,
      xml_Gitsha  => XML_SHA,
      build_date  => GLOBAL_DATE,
      build_time  => GLOBAL_TIME,
      fw_version  => GLOBAL_VER,
      fw_Gitsha   => GLOBAL_SHA
      );

-------------------------------------------------------------------------------------------------------------------
---library versions slave
--------------------------------------------------------------------------------------------------------------------

  library_reg : entity infrastructure_lib.lib_registers
    port map (
      ipb_clk             => ipb_clk,
      ipb_rst             => rst_ipb,
      ipb_in              => ipbw(N_SLV_EFEX_LIB_VERSION),
      ipb_out             => ipbr(N_SLV_EFEX_LIB_VERSION),
      hog_gitsha          => HOG_SHA,
      hog_version         => HOG_VER,
      top_version         => TOP_VER,
      top_gitsha          => TOP_SHA,
      constraints_version => CON_VER,
      constraints_gitsha  => CON_SHA,
      infra_version       => INFRASTRUCTURE_LIB_VER,
      infra_gitsha        => INFRASTRUCTURE_LIB_SHA,
      algo_version        => x"d15ab1ed",
      algo_gitsha         => x"d15ab1ed",
      readout_version     => x"d15ab1ed",
      readout_gitsha      => x"d15ab1ed",
      ipbus_gitsha        => IPBUS_LIB_SHA
      );


  ----------------------------------------------------------------------------------------------------------------------
  -- ipbus slaves live in the entity below, and can expose top-level ports and the ipbus fabric is instantiated with in.
  ----------------------------------------------------------------------------------------------------------------------
  slaves : entity infrastructure_lib.slaves
    port map(
      ipb_clk      => ipb_clk,
      ipb_rst      => rst_ipb,
      ipb_in       => ipbw(N_SLV_COMMON_INFRA),
      ipb_out      => ipbr(N_SLV_COMMON_INFRA),
      status       => status,
      control_reg  => control_reg,
      reconfig_reg => reconfig_reg,
      flash_select => open,
      flash_miso   => flash_miso,
      flash_le     => flash_csn,
      flash_clko   => flash_clk,
      flash_mosi   => flash_mosi
      );


  cclk_o : entity infrastructure_lib.startup
    port map(
      flash_cclk => flash_clk
      );


  configure : entity infrastructure_lib.self_configure
    port map(
      clk       => ipb_clk,     --icap_clk,
      reset     => rst_ipb,
      led_clk   => onehz,
      trigger   => trigger_reconfig,
      indicator => flash_led
      );
end Behavioral;
