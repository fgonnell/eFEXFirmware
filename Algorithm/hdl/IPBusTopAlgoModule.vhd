--! @file
--! @brief Top feature extracting algorithm module with IPBus interface
--! @details
--! The feature extracting module runs at 200 MHz, i.e. 5 times faster than the BC frequency.
--! So data belonging to a Bunch Crossing (BC) can be handled in 5 clock cycles.
--! An eFEX FPGA handles 60 input Trigger Towers (TT) among which 40 are core towers, i.e.
--! a TOB must be produced for each of them and 20 are used just as environment.
--!
--! In order to produce 40 TOBs per BC, 40 independent Algorithm Cores (ACs) should process data in parallel.
--! Thanks to the higher frequency, 8 ACs are enough to process data presented to the cores in 5 clock cycles.
--! It is possible to handle a region of 0.7 Eta * 0.8 Phi.
--!
--! In case the eFEX module is handling a border region in Eta, a number of environment-only TT must be used as core.
--! This means that real data from the column at the far left end (or right) is not provided by the calorimeter.
--! In this case, dummy data (e.g. all zeroes) are fed to the ACs.
--! This module uses 2 clocks: 200 MHz and 280 MHz, plus a 40 MHz 12% duty cyle Load clock.
--! These clocks must be in phase when the Load is high, as shown in figure.
--! \image html InputTiming.png "Input signals timing diagram"
--!
--! The output frequency of this module is 280 MHz, 7 times 40 MHz.
--! Only 5 TOBs per BC can be produced, so no tob will be transmitted in the
--! last 2 clock cycles
--! The output timing is represented in figure \n
--! \image html OutputTiming.png "Output signals timing diagram"
--! A multicycle constraint of 2 is used to cross the clock domains from 40 to 200MHz and from 200 to 280 MHz.
--! @author Francesco Gonnella

--! Use standard library
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
--! Use ipbus address decode automatically generated from XML
use work.ipbus_decode_efex_algorithm.all;

--! Use internal algorithm data types and functions
use work.DataTypes.all;
--! Use external algorithm data types and functions
use work.AlgoDataTypes.all;

--! Use ipbus ibrary
library ipbus_lib;
--! Use ipbus ibrary
use ipbus_lib.ipbus_reg_types.all;
--! Use ipbus ibrary
use ipbus_lib.ipbus.all;

library infrastructure_lib;


--! @copydoc IPBusTopAlgoModule.vhd
entity IPBusTopAlgoModule is
  generic (FPGA : integer := 1);
  port (CLK200           : in  std_logic;                      --! 200 MHz clock
        CLK280           : in  std_logic;                      --! 280 MHz clock, used in the output stage
        IN_Load          : in  std_logic;                      --! 40 MHz clock, 12% duty cycle
        ipb_clk          : in  std_logic;                      --! IPBus clk
        ipb_rst          : in  std_logic;                      --! IPBus reset
        ipb_in           : in  ipb_wbus;                       --! IPBus write bus
        ipb_out          : out ipb_rbus;                       --! IPBus read bus
        OUT_eFEXPosition : out std_logic_vector(31 downto 0);  --! Geographic position of eFEX Module, to be used in the mapping logic

        IN_BCID : in BCID_array;  --! Bunch crossing number as decoded from input data
        IN_Data : in AlgoInput;   --! Algorithm external data structure, defined in AlgoDataTypes.vhd

        OUT_eg_Sync  : out std_logic;                                 --! Output sync, high on the first clock cycle of the BC
        OUT_eg_Valid : out std_logic_vector(OUTPUT_TOBS-1 downto 0);  --! Output data valid, high when correspondent output data are valid

        OUT_eg_TOB : out AlgoOutput;  --! Algorithm external data structure, defined in AlgoDataTypes.vhd

        OUT_BCID_XTOB      : out std_logic_vector(6 downto 0);              --! Delayed crossing number as decoded from input data
        OUT_eg_sync_XTOB   : out std_logic;
        OUT_tau_sync_XTOB  : out std_logic;
        OUT_eg_XTOB        : out AlgoXOutput;                               --! Algorithm external XTOB data structure, defined in AlgoDataTypes.vhd
        OUT_tau_XTOB       : out AlgoXOutput;                               --! Algorithm external XTOB data structure, defined in AlgoDataTypes.vhd
        OUT_eg_Valid_XTOB  : out std_logic_vector(OUTPUT_TOBS-1 downto 0);  --! Output data valid, high when correspondent output data are valid
        OUT_tau_Valid_XTOB : out std_logic_vector(OUTPUT_TOBS-1 downto 0);  --! Output data valid, high when correspondent output data are valid

        OUT_BCID_TOB : out std_logic_vector(6 downto 0);  --! Delayed crossing number as decoded from input data

        OUT_tau_Sync  : out std_logic;                                 --! Output sync, high on the first clock cycle of the BC
        OUT_tau_Valid : out std_logic_vector(OUTPUT_TOBS-1 downto 0);  --! Output data valid, high when correspondent output data are valid
        OUT_tau_TOB   : out AlgoOutput);                               --! Algorithm external data structure, defined in AlgoDataTypes.vhd
end IPBusTopAlgoModule;

--! @copydoc IPBusTopAlgoModule.vhd
architecture Behavioral of IPBusTopAlgoModule is

  constant N_CTRL : positive := 16;  --! Number of control IPBus reg
  constant N_STAT : positive := 16;  --! Number of status IPBus reg

  --Algorithm parameter
  signal eg_ParWs     : AlgoParameters(2 downto 0);  --!Ws condition thresholds
  signal eg_ParReta   : AlgoParameters(2 downto 0);  --!Reta condition thresholds
  signal eg_ParHadron : AlgoParameters(2 downto 0);  --!Hadronic condition thresholds
  signal tau_ParJet   : AlgoParameters(2 downto 0);  --!Tau Jet condition thresholds


  --Control and status registers
  signal eg_Control      : AlgoRegister := (others => '0');
  signal tau_Control     : AlgoRegister := (others => '0');
  signal eg_Status       : AlgoRegister;
  signal tau_Status      : AlgoRegister;
  signal glob_Position   : AlgoRegister := (others => '0');  --! Bit 0 eFEX on edge, bit 1 eFEX on left edge
  signal glob_Control    : AlgoRegister := (others => '0');
  signal glob_Status     : AlgoRegister;
  signal debug_Status    : AlgoRegisters(15 downto 0);
  signal debug_Control   : AlgoRegisters(15 downto 0);
  signal NoiseThresholds : AlgoRegisters(4 downto 0);

  signal eg_Energy_threshold  : DataWord;
  signal eg_Reta_threshold    : DataWord;
  signal eg_Ws_threshold      : DataWord;
  signal eg_Had_threshold     : DataWord;
  signal tau_Energy_threshold : DataWord;
  signal tau_Jet_threshold    : DataWord;

  --Spy memories signals:
  signal FakeInput : AlgoInput;
  signal FakeBCID  : BCID_array;

  signal BCID280, BCIDOut, BCID_to_output_ram : BCID_t;
  signal BCID200                              : BCID_array;

  signal EnableSpyBCID_eg, EnableSpyBCID_tau : std_logic := '0';

  signal AlgoDataInput : AlgoInput;

  signal FakeEgOutput, FakeTauOutput                            : AlgoTriggerObjects(OUTPUT_TOBS-1 downto 0);
  signal EnableInputSpy, EnableOutputEgSpy, EnableOutputTauSpy  : std_logic := '0';
  signal EnableFakeAlgoInput, EnableFakeTOBeg, EnableFakeTOBtau : std_logic := '0';


-- IPBus signals
  signal ipb_to_slaves   : ipb_wbus_array(N_SLAVES - 1 downto 0);
  signal ipb_from_slaves : ipb_rbus_array(N_SLAVES - 1 downto 0) := (others => IPB_RBUS_NULL);

  signal write_reg : ipb_reg_v(N_STAT - 1 downto 0) := (others => (others => '0'));
  signal read_reg  : ipb_reg_v(N_CTRL - 1 downto 0);


-- Parameter RAM signals
  signal ParRamData    : std_logic_vector(PARAMETER_RAM_DATA_WIDTH-1 downto 0);
  signal ParRamAddress : std_logic_vector(2 downto 0);

  signal Load : std_logic;

  signal IS_Data          : TriggerTowerMatrix;
  signal IS_Threshold_l0  : std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
  signal IS_Threshold_l1  : std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
  signal IS_Threshold_l2  : std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
  signal IS_Threshold_l3  : std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
  signal IS_Threshold_had : std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);



  signal TOB_Start            : std_logic;
  signal TOB_Counter          : std_logic_vector(2 downto 0);
  signal eg_TOBs              : TriggerObjects_eg(OUTPUT_TOBS-1 downto 0);
  signal eg_in_TOBs           : TriggerObjects_eg(OUTPUT_TOBS-1 downto 0);
  signal Input_TOBs_eg        : AlgoTriggerObjects(OUTPUT_TOBS-1 downto 0);
  signal SerialSorterEgInput  : AlgoTriggerObjects(OUTPUT_TOBS-1 downto 0);
  signal eg_sorted_TOBs       : AlgoTriggerObjects(OUTPUT_TOBS-1 downto 0);
  signal eg_sorted_TOB_Start  : std_logic_vector(OUTPUT_TOBS-1 downto 0);
  signal tau_TOBs             : TriggerObjects_tau(OUTPUT_TOBS-1 downto 0);
  signal tau_in_TOBs          : TriggerObjects_tau(OUTPUT_TOBS-1 downto 0);
  signal Input_TOBs_tau       : AlgoTriggerObjects(OUTPUT_TOBS-1 downto 0);
  signal SerialSorterTauInput : AlgoTriggerObjects(OUTPUT_TOBS-1 downto 0);
  signal tau_sorted_TOBs      : AlgoTriggerObjects(OUTPUT_TOBS-1 downto 0);
  signal tau_sorted_TOB_Start : std_logic_vector(OUTPUT_TOBS-1 downto 0);

  signal eg_tob_empty  : std_logic_vector(OUTPUT_TOBS-1 downto 0);
  signal tau_tob_empty : std_logic_vector(OUTPUT_TOBS-1 downto 0);

  signal eg_xtob_valid  : std_logic_vector(OUTPUT_TOBS-1 downto 0);
  signal tau_xtob_valid : std_logic_vector(OUTPUT_TOBS-1 downto 0);


  signal Load280  : std_logic;
  signal Load200  : std_logic;
  signal Count280 : std_logic_vector(2 downto 0) := (others => '0');
  signal Count200 : std_logic_vector(2 downto 0) := (others => '0');
  signal OutLoad  : std_logic;

--! Synchrnoisation phase between 200MHZ and 280MHz clocks
  constant PHASE280 : integer := 5;
--! Synchrnoisation phase between 40MHZ and 200MHz clocks
  constant PHASE200 : integer := 6;


begin
-- Debug registers:
  debug_Status(15 downto 0) <= debug_Control(15 downto 0);

  LOAD_GENERATOR : entity work.LoadGenerator
    generic map(
      PHASE200 => PHASE200,
      PHASE280 => PHASE280)
    port map (
      IN_Load     => IN_Load,
      CLK200      => CLK200,
      CLK280      => CLK280,
      OUT_Load280 => Load280,
      OUT_Load200 => Load200);

-- Data spy MUX
  AlgoDataInput <= FakeInput when EnableFakeAlgoInput = '1' else IN_Data;

  IS_Threshold_l0  <= NoiseThresholds(0)(9 downto 0);
  IS_Threshold_l1  <= NoiseThresholds(1)(9 downto 0);
  IS_Threshold_l2  <= NoiseThresholds(2)(9 downto 0);
  IS_Threshold_l3  <= NoiseThresholds(3)(9 downto 0);
  IS_Threshold_had <= NoiseThresholds(4)(9 downto 0);


  INPUT_STAGE : entity work.AlgoInputStage
    port map (
      CLK200      => CLK200,
      CLK280      => CLK280,
      IN_Load     => Load200,
      IN_Data     => AlgoDataInput,
      IN_Position => glob_Position,
      IN_BCID     => IN_BCID,

      IN_threshold_l0  => IS_Threshold_l0,
      IN_threshold_l1  => IS_Threshold_l1,
      IN_threshold_l2  => IS_Threshold_l2,
      IN_threshold_l3  => IS_Threshold_l3,
      IN_threshold_had => IS_Threshold_had,

      OUT_BCID_200 => BCID200,  -- BCID array synced with 200 MHz clock
      OUT_Load     => Load,
      OUT_Data     => IS_Data);

  OUT_eFEXPosition <= glob_Position;

  BCID_sync_proc : process (CLK280)
  begin
    if rising_edge(CLK280) then
      if Count280 = "010" then  -- to meet multicycle 2 constraint between 200 nad 280
        if EnableFakeAlgoInput = '1' then
          BCID280 <= FakeBCID(0);
        else
          BCID280 <= BCID200(0);
        end if;

      else
        BCID280 <= BCID280;
      end if;
    end if;
  end process BCID_sync_proc;

  TOP_ALGO_MODULE : entity work.TopAlgoModule
    port map (
      CLK200  => CLK200,
      IN_Load => Load,

      IN_Data => IS_Data,

      OUT_ParameterRAMaddress => ParRamAddress,

      IN_eg_ParWs     => eg_ParWs,
      IN_eg_ParReta   => eg_ParReta,
      IN_eg_ParHadron => eg_ParHadron,

      IN_eg_Energy_threshold  => eg_Energy_threshold,
      IN_eg_Reta_threshold    => eg_Reta_threshold,
      IN_eg_Ws_threshold      => eg_Ws_threshold,
      IN_eg_Had_threshold     => eg_Had_threshold,
      IN_tau_Energy_threshold => tau_Energy_threshold,
      IN_tau_Jet_threshold    => tau_Jet_threshold,

      IN_eg_Control => eg_Control,
      OUT_eg_Status => eg_Status,

      IN_tau_ParJet => tau_ParJet,

      IN_tau_Control => tau_Control,
      OUT_tau_Status => tau_Status,



      IN_glob_Position => glob_Position,
      IN_glob_Control  => glob_Control,
      OUT_glob_Status  => glob_Status,

      OUT_TOB_Start   => TOB_Start,
      OUT_TOB_Counter => TOB_Counter,

      OUT_eg_TOB => eg_TOBs,

      OUT_tau_TOB => tau_TOBs);

  Counter_280 : process (CLK280)
  begin  -- process dd
    if rising_edge(CLK280) then
      if Load280 = '1' then     -- Reset the counter accordingly
        Count280 <= (others => '0');
      else
        Count280 <= std_logic_vector(unsigned(Count280)+1);
      end if;
    end if;
  end process;

  Counter_200 : process (CLK200)
  begin  -- process dd
    if rising_edge(CLK200) then
      if Load200 = '1' then     -- Reset the counter accordingly
        Count200 <= (others => '0');
      else
        Count200 <= std_logic_vector(unsigned(Count200)+1);
      end if;
    end if;
  end process;



  out_tob_for : for i in 0 to OUTPUT_TOBS-1 generate

    --Eta is given by the TOB counter
    --Phi is given by the index of this for

    eg_in_TOBs(i).Core          <= validate_core(eg_TOBs(i).Core);
    eg_in_TOBs(i).Position.Phi  <= std_logic_vector(to_unsigned(i, 3));
    tau_in_TOBs(i).Core         <= validate_core(tau_TOBs(i).Core);
    tau_in_TOBs(i).Position.Phi <= std_logic_vector(to_unsigned(i, 3));

    -- Change Eta numbering as a function of position
    -- When eFEX is on Left eta edge "11", eta goes from 0 to 4 (no + 1)
    -- When eFEX is on Right Eta edge "01", eta goes from 1 to 5
    -- When eFex is not on edge "x0", eta goes from 1 to 4
    tau_in_TOBs(i).Position.Eta <= TOB_Counter when glob_Position(1 downto 0) = "11" else std_logic_vector(unsigned(TOB_Counter) + 1);
    eg_in_TOBs(i).Position.Eta  <= TOB_Counter when glob_Position(1 downto 0) = "11" else std_logic_vector(unsigned(TOB_Counter) + 1);

    Input_TOBs_eg(i)  <= to_AlgoTriggerObject(eg_in_TOBs(i), FPGA);
    Input_TOBs_tau(i) <= to_AlgoTriggerObject(tau_in_TOBs(i), FPGA);

    SerialSorterEgInput(i)  <= FakeEgOutput(i)  when EnableFakeTOBeg = '1'  else Input_TOBs_eg(i);
    Serialsortertauinput(i) <= FakeTauOutput(i) when EnableFakeTOBtau = '1' else Input_TOBs_tau(i);

    SerialSorter_eg : entity work.SerialSorter
      port map (
        clk       => CLK200,
        clk_out   => CLK280,
        IN_Load   => Load280,
        IN_Clear  => TOB_Start,
        IN_Data   => SerialSorterEgInput(i),
        OUT_Start => eg_sorted_TOB_Start(i),
        OUT_Data  => eg_sorted_TOBs(i));

    SerialSorter_tau : entity work.SerialSorter
      port map (
        clk       => CLK200,
        clk_out   => CLK280,
        IN_Load   => Load280,
        IN_Clear  => TOB_Start,
        IN_Data   => SerialSorterTauInput(i),
        OUT_Start => tau_sorted_TOB_Start(i),
        OUT_Data  => tau_sorted_TOBs(i));
  end generate out_tob_for;

  valid_for : for i in OUTPUT_TOBS-1 downto 0 generate
    OUT_eg_Valid(i)  <= is_valid_TOB(eg_sorted_TOBs(i));
    OUT_tau_Valid(i) <= is_valid_TOB(tau_sorted_TOBs(i));
  end generate valid_for;
  -- TOBS to be sent out of this module to parallel sorting *280
  OUT_eg_Sync  <= eg_sorted_TOB_Start(0);   -- should be done better, what about
  OUT_tau_Sync <= tau_sorted_TOB_Start(0);  -- the other signals?
  OUT_eg_TOB   <= to_AlgoOutput(eg_sorted_TOBs);
  OUT_tau_TOB  <= to_AlgoOutput(tau_sorted_TOBs);


  -- XTOBS to be sent to out of this module to readout @200
  OUT_tau_Sync_XTOB <= TOB_Start;
  OUT_eg_Sync_XTOB  <= TOB_Start;
  valid_xtob_for : for i in OUTPUT_TOBS-1 downto 0 generate
    eg_xtob_valid(i)      <= is_valid_core(eg_in_TOBS(i).Core);  --this goes to rate monitor
    tau_xtob_valid(i)     <= is_valid_core(tau_in_TOBS(i).Core);
    OUT_eg_Valid_XTOB(i)  <= eg_xtob_valid(i);
    OUT_tau_Valid_XTOB(i) <= tau_xtob_valid(i);
  end generate;
  OUT_eg_XTOB  <= to_AlgoXOutput(eg_in_TOBS, FPGA);
  OUT_tau_XTOB <= to_AlgoXOutput(tau_in_TOBs, FPGA);

  OUT_BCID_XTOB <= BCID_to_output_RAM;

  RATE_MONITOR : entity work.AlgoRateMonitor
    port map (
      ipb_clk => ipb_clk,
      ipb_rst => ipb_rst,
      ipb_in  => ipb_to_slaves(N_SLV_ALGO_RATE_MONITOR),
      ipb_out => ipb_from_slaves(N_SLV_ALGO_RATE_MONITOR),

      clk     => clk200,
      IN_BCID => BCID_to_output_RAM,

      IN_synch => TOB_Start,

      IN_eta => eg_in_TOBS(0).Position.Eta,

      IN_eg_energies       => (7 => eg_in_TOBS(7).Core.Energy, 6 => eg_in_TOBS(6).Core.Energy, 5 => eg_in_TOBS(5).Core.Energy, 4 => eg_in_TOBS(4).Core.Energy,
                         3 => eg_in_TOBS(3).Core.Energy, 2 => eg_in_TOBS(2).Core.Energy, 1 => eg_in_TOBS(1).Core.Energy, 0 => eg_in_TOBS(0).Core.Energy),
      IN_eg_valids         => eg_xtob_valid,

      IN_tau_energies      => (7 => tau_in_TOBS(7).Core.Energy, 6 => tau_in_TOBS(6).Core.Energy, 5 => tau_in_TOBS(5).Core.Energy, 4 => tau_in_TOBS(4).Core.Energy,
                         3 => tau_in_TOBS(3).Core.Energy, 2 => tau_in_TOBS(2).Core.Energy, 1 => tau_in_TOBS(1).Core.Energy, 0 => tau_in_TOBS(0).Core.Energy),
      IN_tau_valids        => tau_xtob_valid

      );

  IPBUS_FABRIC : entity ipbus_lib.ipbus_fabric_sel
    generic map (
      NSLV      => N_SLAVES,                             --defined in ipbus_sel_address_table_ALGO
      SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map (
      sel => ipbus_sel_efex_algorithm(ipb_in.ipb_addr),  --function defined in ipbus_sel_address_table_ALGO

      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      ipb_to_slaves   => ipb_to_slaves,
      ipb_from_slaves => ipb_from_slaves);

  IPBUS_ALGO_REGISTERS : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => N_CTRL,         --number of control reg
      N_STAT => N_STAT)         --number of status reg
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipb_to_slaves(N_SLV_ALGO_REGISTERS),
      ipbus_out => ipb_from_slaves(N_SLV_ALGO_REGISTERS),
      d         => write_reg,
      q         => read_reg,
--    qmask     => (others => '0'),
      stb       => open);

  IPBUS_ALGO_PARAMETER_RAM : entity work.ipbus_dpram_wrapper
    port map (
      clk_ipb => ipb_clk,
      rst     => ipb_rst,
      ipb_in  => ipb_to_slaves(N_SLV_ALGO_PARAMETER_RAM),
      ipb_out => ipb_from_slaves(N_SLV_ALGO_PARAMETER_RAM),

      rclk => CLK200,
      we   => '0',
      q    => ParRamData,
      addr => ParRamAddress);


  IPBUS_INPUT_RAM : entity work.ipbus_inputRAM
    port map (
      clk_ipb => ipb_clk,
      rst     => ipb_rst,
      ipb_in  => ipb_to_slaves(N_SLV_ALGO_INPUT_RAM),    --have to see how it is
      ipb_out => ipb_from_slaves(N_SLV_ALGO_INPUT_RAM),  --  generated by ipbus

      rclk    => CLK200,
      Load    => Load200,
      BCIDIn  => IN_BCID,
      AlgoIn  => IN_Data,         -- connected to IN_Data used for data spy
      we      => EnableInputSpy,  -- enabled in case of data spy
      BCIDOut => FakeBCID,
      AlgoOut => FakeInput);      -- used to feed data into Algorithm block


  IPBUS_OUTPUT_EG_RAM : entity work.ipbus_outputRAM_wrapper
    port map (
      clk_ipb => ipb_clk,
      rst     => ipb_rst,
      ipb_in  => ipb_to_slaves(N_SLV_ALGO_OUTPUT_EG_RAM),    --have to see how it is
      ipb_out => ipb_from_slaves(N_SLV_ALGO_OUTPUT_EG_RAM),  --  generated by ipbus

      BCIDIn    => BCID_to_output_ram,
      SpyBCIDIn => EnableSpyBCID_eg,

      rclk    => CLK200,
      Sync    => TOB_Start,
      AlgoIn  => to_AlgoOutput(Input_TOBs_eg),
      we      => EnableOutputEgSpy,
      AlgoOut => FakeEgOutput);

  IPBUS_OUTPUT_TAU_RAM : entity work.ipbus_outputRAM_wrapper
    port map (
      clk_ipb => ipb_clk,
      rst     => ipb_rst,
      ipb_in  => ipb_to_slaves(N_SLV_ALGO_OUTPUT_TAU_RAM),    --have to see how it is
      ipb_out => ipb_from_slaves(N_SLV_ALGO_OUTPUT_TAU_RAM),  --  generated by ipbus

      BCIDIn    => BCID_to_output_ram,
      SpyBCIDIn => EnableSpyBCID_tau,

      rclk    => CLK200,
      Sync    => TOB_Start,
      AlgoIn  => to_AlgoOutput(Input_TOBs_tau),
      we      => EnableOutputTauSpy,
      AlgoOut => FakeTauOutput);

  BCID_Delay_internal : entity infrastructure_lib.GeneralDelay
    generic map (
      delay => 12,
      size  => 7)
    port map (
      clk      => CLk200,
      data_in  => BCID200(0),
      data_out => BCID_to_output_ram);


  BCID_Delay_to_Readout : entity infrastructure_lib.GeneralDelay
    generic map (
      delay => 24,
      size  => 7)
    port map (
      clk      => CLk280,
      data_in  => BCID280,
      data_out => BCIDOut);

  OUT_BCID_TOB <= BCIDOut;

-- Algor register funcionality
  EnableFakeAlgoInput <= glob_Control(0);
  EnableFakeTOBeg     <= eg_Control(0);
  EnableFakeTOBtau    <= tau_Control(0);


  EnableInputSpy     <= glob_Control(1);
  EnableOutputEgSpy  <= eg_Control(1);
  EnableOutputTauSpy <= tau_Control(1);

  EnableSpyBCID_eg  <= eg_Control(2);
  EnableSpyBCID_tau <= tau_Control(2);


-- Algorithm registers connection
  eg_Control         <= read_reg(00);
  tau_Control        <= read_reg(01);
  glob_Control       <= read_reg(02);
  glob_Position      <= read_reg(03);
  NoiseThresholds(0) <= read_reg(04);
  NoiseThresholds(1) <= read_reg(05);
  NoiseThresholds(2) <= read_reg(06);
  NoiseThresholds(3) <= read_reg(07);
  NoiseThresholds(4) <= read_reg(08);
  debug_Control(00)  <= read_reg(09);
  debug_Control(01)  <= read_reg(10);
  debug_Control(02)  <= read_reg(11);
  debug_Control(03)  <= read_reg(12);
  debug_Control(04)  <= read_reg(13);
  debug_Control(05)  <= read_reg(14);
  debug_Control(06)  <= read_reg(15);

  write_reg(0)  <= eg_Status;
  write_reg(1)  <= tau_Status;
  write_reg(2)  <= glob_Status;
  write_reg(3)  <= (others => '0');
  write_reg(4)  <= x"face000" & '0' & TOB_Counter;
  write_reg(5)  <= x"face000" & '0' & Count280;
  write_reg(6)  <= debug_Status(00);
  write_reg(7)  <= debug_Status(01);
  write_reg(8)  <= debug_Status(02);
  write_reg(9)  <= debug_Status(03);
  write_reg(10) <= debug_Status(04);
  write_reg(11) <= debug_Status(05);
  write_reg(12) <= debug_Status(06);
  write_reg(14) <= debug_Status(08);
  write_reg(15) <= debug_Status(09);

-------------------------------------------------------------------------------
-- ALGORITHM PARAMETER CONNECTION
-------------------------------------------------------------------------------
-- Electromagnetic
  eg_ParReta(0) <= ParRamData(7 downto 0);  --ipbus word 0
  eg_ParReta(1) <= ParRamData(15 downto 8);
  eg_ParReta(2) <= ParRamData(23 downto 16);
  -- 8 unused bits

  eg_ParWs(0) <= ParRamData(7+32 downto 0+32);  --ipbus word 1
  eg_ParWs(1) <= ParRamData(15+32 downto 8+32);
  eg_ParWs(2) <= ParRamData(23+32 downto 16+32);
  -- 8 unused bits

  eg_ParHadron(0) <= ParRamData(7+64 downto 0+64);  --ipbus word 2
  eg_ParHadron(1) <= ParRamData(15+64 downto 8+64);
  eg_ParHadron(2) <= ParRamData(23+64 downto 16+64);
  -- 8 unused bits

  eg_Energy_threshold <= ParRamData(15+96 downto 0+96);  --ipbus word 3
  eg_Reta_threshold   <= ParRamData(31+96 downto 16+96);

  eg_Ws_threshold  <= ParRamData(15+128 downto 0+128);  --ipbus word 4
  eg_Had_threshold <= ParRamData(31+128 downto 16+128);

-- Tau
  tau_ParJet(0) <= ParRamData(7+160 downto 0+160);  --ipbus word 5
  tau_ParJet(1) <= ParRamData(15+160 downto 8+160);
  tau_ParJet(2) <= ParRamData(23+160 downto 16+160);
  -- 8 unused bits

  tau_Energy_threshold <= ParRamData(15+192 downto 0+192);  -- ipbus word 6
  tau_Jet_threshold    <= ParRamData(31+192 downto 16+192);

end architecture;
