----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.09.2017 15:19:24
-- Design Name: 
-- Module Name: aurora_reset - RTL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity aurora_reset is

Port ( init_clk : in STD_LOGIC;
   BTN0 : in STD_LOGIC;
   rst_sw : in STD_LOGIC;
   tx_reset : out STD_LOGIC;          
   tx_GTReset : out STD_LOGIC := '0';
   rx_reset : out STD_LOGIC;
   rx_GTReset : out STD_LOGIC := '0');
end aurora_reset;

architecture RTL of aurora_reset is
signal samp1 : std_logic := '1'; 
signal samp2 : std_logic := '1';
signal btn_samp1 : std_logic := '0';  
signal start_seq : std_logic := '0'; 
signal count : std_logic_vector(15 downto 0) := X"0000";
signal pwrcount : std_logic_vector(15 downto 0) := X"0000";
signal tx_btn_reset : std_logic := '0';
signal rx_btn_reset : std_logic := '0';
signal tx_pwr_reset : std_logic := '0';
signal rx_pwr_reset : std_logic := '0';
signal rx_pwr_GTReset : std_logic := '0';
signal tx_pwr_GTReset : std_logic := '0';
signal rx_btn_GTReset : std_logic := '0';
signal tx_btn_GTReset : std_logic := '0';

signal pwr_on  : std_logic;

begin
--power-on
--use a switch to hold system in power-up until both boards can be configured.  Then release switch to start the sequence of 
-- both resets and GTresets

--normal reset
--use BTN0 to start sequence.   No GTreset should be issued 


--Power_on_reset 
--Simplex power-on sequence:
--1. Deassert TX-side gt_reset (A)
--2. Deassert RX-side gt_reset (C)
--3. Deassert RX-side reset synchronous to user_clk (D)
--4. Deassert TX-side reset synchronous to user_clk (B)
--Note: Care must be taken to ensure that the (D) to (B) time difference is as minimal as possible.


         
      process (init_clk) begin
           if rising_edge (init_clk) then
            samp1 <= rst_sw;
            samp2 <= samp1;
         end if;
      
        end process;
      
        pwr_on <= samp2 and not samp1;


         process (init_clk) begin
          if rising_edge (init_clk) then
             if pwr_on = '1' then
                pwrcount <= X"FFFF";
            elsif pwrcount /= X"0000" then
                pwrcount <= (pwrcount - '1');
            end if;
          end if;
         end process;

	process (init_clk) begin
	    if rising_edge(init_clk) then 
	        if (pwrcount = X"0000" and samp2 = '0') then 
	           tx_pwr_reset <= '0';  
	           rx_pwr_reset <= '0';  
	           tx_pwr_GTReset  <= '0';
               rx_pwr_GTREset  <= '0';
 
            elsif (samp2 = '1') then 
               tx_pwr_reset <= '1';
               rx_pwr_reset <= '1';
               tx_pwr_GTReset  <= '1';
               rx_pwr_GTREset  <= '1';
            end if;             

--1. Deassert TX-side gt_reset (A)
            if (pwrcount = X"8000") then             
               tx_pwr_GTReset <='0';

--2. Deassert RX-side gt_reset (C)       
            elsif (pwrcount = X"7000") then 
                tx_pwr_GTReset <='0';
                rx_pwr_GTReset <='0';

--3. Deassert RX-side reset synchronous to user_clk (D)
            
            elsif (pwrcount = X"0020") then 
                rx_pwr_reset <='0';

--4. Deassert TX-side reset synchronous to user_clk (B)
--Note: Care must be taken to ensure that the (D) to (B) time difference is as minimal as possible.

            elsif (pwrcount = X"001D") then 
                tx_pwr_reset <='0';
                                                      
            end if;
         end if;           
     end process; 


--Button_0_reset 
--1. tx_system_reset and rx_system_reset are asserted for at least six clock
--   user_clk time periods.
--2. tx_channel_up and rx_channel_up are deasserted after three user_clk cycles.
--3. rx_system_reset is deasserted (or) released after tx_system_reset is deasserted.
--   This ensures that the transceiver in the simplex-TX core starts transmitting initialization
--    data much earlier and it enhances the likelihood of the simplex-RX core aligning to the
--    correct data sequence.
--4. rx_channel_up is asserted before tx_channel_up assertion. This condition must be
--   satisfied by the simplex-RX core and simplex timer parameters (C_ALIGNED_TIMER,
--   C_BONDED_TIMER and C_VERIFY_TIMER) in the simplex-TX core need to be adjusted to
--   meet this criteria.
--5. tx_channel_up is asserted when the simplex-TX core completes the Aurora 8B/10B
--   protocol channel initialization sequence transmission for the configured time. Assertion
--   of tx_channel_up last ensures that the simplex-TX core transmits the Aurora
--   initialization sequence when the simplex-RX core is ready.

   process (init_clk) begin
      if rising_edge(init_clk)  then
        if ((BTN0 = '1') and (btn_samp1 = '0')) then btn_samp1 <= '1'; 
        elsif ((BTN0 = '0') and (btn_samp1 = '1')) then start_seq <= '1';
        end if;
        
        if (start_seq ='1') then 
                btn_samp1 <= '0';
                start_seq <= '0';
        end if; 
      end if;
    end process;   
        

	process (init_clk) begin
		if rising_edge(init_clk) then
			if (start_seq = '1') then count <= X"FFFF";			    
			elsif (start_seq = '0' and count = X"0000") then count <= X"0000";
			else count <= (count - '1');  
			end if; 
		end if; 
	end process; 
	
	process (init_clk) begin
	    if rising_edge(init_clk) then 
            if (start_seq = '1') then 
                rx_btn_reset <= '1';          
                tx_btn_reset <= '1';

             elsif (count = X"FEFF") then 
               tx_btn_reset <='1';

             elsif (count = X"FDFF") then 
               tx_btn_GTReset <='1';
               
             elsif (count = X"FDF0") then 
                tx_btn_GTReset <='0'; 
                  
             elsif (count = X"FDE0") then 
                   rx_btn_GTReset <='1';
                     
             elsif (count = X"FDD0") then 
                   rx_btn_GTReset <='0';  
                  
             elsif (count = X"0040") then 
                      tx_btn_reset <='0';                                                                  

--3. rx_system_reset is deasserted (or) released after tx_system_reset is deasserted.

             elsif (count = X"0020") then  

                rx_btn_reset <='0';
              end if;
         end if;           
     end process; 
	

        tx_reset <= tx_pwr_reset or tx_btn_reset;
        rx_reset <= rx_pwr_reset or rx_btn_reset;
        tx_GTReset <= tx_pwr_GTReset or tx_btn_GTReset;
        rx_GTReset <= rx_pwr_GTReset or rx_btn_GTReset;


end RTL;
