

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clk_gen_200M_tst is
    Port ( clk_200M_o : out STD_LOGIC);
end clk_gen_200M_tst;

architecture Behavioral of clk_gen_200M_tst is

    signal clk : std_logic := '0';
    constant clk_period : time := 5 ns;

BEGIN     

    -- Clock process definitions( clock with 50% duty cycle is generated here.
    clk_process :process
    begin
         clk <= '0';
         wait for clk_period/2;  --for 0.5 ns signal is '0'.
         clk <= '1';
         wait for clk_period/2;  --for next 0.5 ns signal is '1'.
    end process;
    
    clk_200M_o <= clk;

end Behavioral;
