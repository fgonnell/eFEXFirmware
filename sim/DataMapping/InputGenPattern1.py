"""
Created on March 7 2020
@author Alexios Stampekis
Pattern 1: Create 1280 BCs. For each BC only one word of a single MGT
is "on", and equal to 1. The rest are zero. Call it "raw" test pattern.
"""

import sys
import InputComponents as InC

def Run():
    MGTs=[]
    for i in range (64):
        mgt=InC.MGT(i, 1281)  # plus one ("empty") BC to account for the offset
        for j in range(0+i*20, 20*(i+1)):
            mgt.set_bc_word(j, j-i*20, 1)    # arguments are: nbc, word#, word_value
            # In the Output Components module just do: InW[j,i,j-i*20]=1
        MGTs.append(mgt)

    print("Writing to files")
    for v in MGTs:
        v.export_file()

    print("Done")


if __name__=="__main__":
    sys.exit(Run() or 0)

