--! @file
--! @brief Load generator
--! @details
--! This module generates a load signal for algorithm data. It is derived from
--! the load clock, having a 40 MHz frequency a 12% duty cycle (shorter than
--! 200MHz and 280MHz.
--! The OUT_Load200 and 280 signals are synchronised with the 200 and 280 MHz clock
--! The two phases should be set according to the simulation and to the
--! multicycle constraints:  40 to 200 and 200 to 280
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

--! @copydoc LoadGenerator.vhd
entity LoadGenerator is
  generic (PHASE200 : integer := 2;
           PHASE280 : integer := 3
           );
  port (IN_Load     : in  std_logic;
        CLK200      : in  std_logic;
        CLK280      : in  std_logic;
        OUT_Load200 : out std_logic;
        OUT_Load280 : out std_logic
        );
end LoadGenerator;

--! @copydoc LoadGenerator.vhd
architecture Behavioral of LoadGenerator is

  signal LoadR         : std_logic                           := '0';
  signal Load280_i     : std_logic                           := '0';
  signal Load_delay280 : std_logic_vector(PHASE280 downto 0) := (others => '0');
  signal Load_delay200 : std_logic_vector(PHASE200 downto 0) := (others => '0');
--  ####### Mark signals  ########
 attribute keep       : string ;
 attribute max_fanout : integer;
 attribute keep of       Load280_i : signal is "true" ;
 attribute max_fanout of Load280_i : signal is 40;

--   #######################################
begin

  process(IN_Load)
  begin
    if rising_edge(IN_Load) then
      LoadR <= not LoadR;
    end if;
  end process;

  process(CLK200)
  begin
    if rising_edge(CLK200) then
      Load_delay200(0)                 <= LoadR;
      Load_delay200(PHASE200 downto 1) <= Load_delay200(PHASE200-1 downto 0);
      OUT_Load200                      <= Load_delay200(PHASE200) xor Load_delay200(PHASE200-1);
    end if;
  end process;

  process(CLK280)
  begin
    if rising_edge(CLK280) then
      Load_delay280(0)                 <= LoadR;
      Load_delay280(PHASE280 downto 1) <= Load_delay280(PHASE280-1 downto 0);
      Load280_i                        <= Load_delay280(PHASE280) xor Load_delay280(PHASE280-1);
    end if;
  end process;
  OUT_Load280  <= Load280_i;

end Behavioral;
