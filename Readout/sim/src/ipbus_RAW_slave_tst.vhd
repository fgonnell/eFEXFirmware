--! @file
--! @brief Test File for RAW calorimeter data readout slave registers
--! @details 
--! This is a test file to write to and read from the RAW calorimeter data readout slave registers
--! @author Saeed Taghavi
-- 
----------------------------------------------------------------------------------
---- The signals going from slaves to master - parallel bus
--	type ipb_rbus is
--    record
--			ipb_rdata: std_logic_vector(31 downto 0);
--			ipb_ack: std_logic;
--			ipb_err: std_logic;
--    end record;

---- The signals going from master to slaves - parallel bus
--	type ipb_wbus is
--		record
--			ipb_addr: std_logic_vector(31 downto 0);
--			ipb_wdata: std_logic_vector(31 downto 0);
--			ipb_strobe: std_logic;
--			ipb_write: std_logic;
--		end record;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library ipbus_lib;
use ipbus_lib.ipbus.all;

entity ipbus_RAW_slave_tst is
--generic(N_SLAVES: integer := 0);
    Port ( 
        ipb_rst          : in std_logic ;
        ipb_clk          : in std_logic ;
        IPb_in           : out ipb_wbus;      -- The signals going from master to slaves
        IPb_out          : in   ipb_rbus;   -- The signals going from slaves to master
        L1A_tst_out      : out std_logic    -- test L1A
           );
end ipbus_RAW_slave_tst;

architecture Behavioral of ipbus_RAW_slave_tst is

    constant Clk_period : time := 25 ns;
--    signal  IPb_in  : ipb_wbus_array(N_SLAVES - 1 downto 0);
--    signal  IPb_out : ipb_rbus_array(N_SLAVES-1 downto 0);

begin
u1_proc : process
    begin
        IPb_in.ipb_addr     <= X"00000000" ;    -- set all to ZERO
        IPb_in.ipb_wdata    <= X"00000000" ;
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        L1A_tst_out         <= '0' ;
        wait for Clk_period * 130;     -- wait 10 clk periods
 
-- RAW_WR_ADDR_OFFSET (write first for simulation)
        IPb_in.ipb_addr     <= X"00000012" ;
        IPb_in.ipb_wdata    <= X"00000014" ;
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;    -- write
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
  
-- RAW_FIFO_PROG_FULL_THRESH_ASSERT
        IPb_in.ipb_addr     <= X"00000000" ;    -- $1FF = 512
        IPb_in.ipb_wdata    <= X"FFFFFFC0" ;    -- $1C0 = 448
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
        
--RAW_FIFO_PROG_FULL_THRESH_NEGATE
        IPb_in.ipb_addr     <= X"00000001" ;    -- $1FF = 512
        IPb_in.ipb_wdata    <= X"FFFFFF00" ;    -- $100 = 256
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
                
-- BCN_FIFO_PROG_FULL_THRESH_ASSERT
        IPb_in.ipb_addr     <= X"00000002" ;    -- $1FF = 512
        IPb_in.ipb_wdata    <= X"FFFFFFF0" ;    -- $1F0 = 496
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
        
--BCN_FIFO_PROG_FULL_THRESH_NEGATE
        IPb_in.ipb_addr     <= X"00000003" ;    -- $1FF = 512
        IPb_in.ipb_wdata    <= X"FFFFFF00" ;    -- $100 = 256
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period * 1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
                
-- LINK_OUTPUT_FIFO_PROG_FULL_THRESH_ASSERT
        IPb_in.ipb_addr     <= X"00000004" ;    -- $1FFF = 8192
        IPb_in.ipb_wdata    <= X"FFFFF770" ;    -- $1770 = 6000
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
        
--LINK_OUTPUT_FIFO_PROG_FULL_THRESH_NEGATE
        IPb_in.ipb_addr     <= X"00000005" ;    -- $1FFF = 8192
        IPb_in.ipb_wdata    <= X"FFFFF000" ;    -- $1000 = 4096
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period * 1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
 
-- RAW_FIFO_FULL_THRESH_ASSERT
        IPb_in.ipb_addr     <= X"00000008" ;    -- $1FF = 512
        IPb_in.ipb_wdata    <= X"FFFFFFF4" ;    -- $1F4 = 500
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
        
--RAW_FIFO_FULL_THRESH_NEGATE
        IPb_in.ipb_addr     <= X"00000009" ;    -- $1FF = 512
        IPb_in.ipb_wdata    <= X"FFFFFF00" ;    -- $100 = 256
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period * 1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
 
        wait;
    end process;

end Behavioral;
