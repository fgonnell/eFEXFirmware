{ head:{
   text:'Time diagram',
  tick:-1},
  signal: [
  //{name: 'clk40', wave:  'p', period : 20},
  //{name: 'clk160', wave: 'pppp', period : 5},
  {name: 'clock', wave: 'p............', period : 1},
  {name: 'data', wave: '0422222222222', data: "data", period : 1, phase : 0, node: ".d"},
    
  {name: 'is local max', wave: '0.10.........', period : 1, node: "..l"},
  {name: 'is global max', wave: '0.10.........', period : 1, node: "..m"},  
  {name: 'seed(2:0)', wave: '0.42222222222', data: "seed", period : 1, node: "..s"},
  
  {name: 'Mux done', wave: '0..10........',data: 'TOB', period : 1, node: "...0"},    

  
  {name: "R add core", wave: '0......20.30.',data: 'sum(6) del_sum', period : 1, node :".......7z.9"},    
  {name: "R add env", wave: '0......30....',data: 'sum(8)', period : 1, node :".......1"},    
  {name: "R mult", wave: '0.........50.',data: 'product', period : 1, node :"..........2"},    

  {name: 'f3 add env', wave: '0.........30.',data: 'sum(32)', period : 1, node :"..........6"},    
  {name: 'f3 add core', wave: '0.....230....',data: 'sum(4) del_sum', period : 1, node :"......38"},   
  {name: 'f3 mult', wave: '0.........50.',data: 'product', period : 1, node :"..........4"},    

  {name: 'TOB', wave: '0..........40',data: 'TOB', period : 1},    

], 
    edge: [
    'd~->s','d~->l','d~->m',
    's~->0',
    'z~>9 delay',
    '1~->2 3 clk delay', '8~->4 3 clk delay', '0~->7', '0~->1', '0~->6','0~->3'
  ],
  config: { hscale: 2}
  }
