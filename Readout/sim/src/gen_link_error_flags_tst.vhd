----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/30/2017 04:07:37 PM
-- Design Name: 
-- Module Name: gen_link_error_flags_tst - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;
use TOB_rdout_lib.data_type_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity gen_link_error_flags_tst is
  Port ( 
  link_error_flags : out link_error_type                   -- array 66 x 4 bit per link
  );
end gen_link_error_flags_tst;

architecture Behavioral of gen_link_error_flags_tst is

begin

link_error_flags <= (others => (others => '0'));

 --   GEN_ERROR_FLG: for j in 0 to 65 generate  
--        link_error_flags(j)  <= conv_std_logic_vector ((j + 1), 4) ;
--    end generate GEN_ERROR_FLG;

end Behavioral;
