
-- MUX into packet engine...
--
-- Monitors each input to feed next en route to Aurora
--
-- Dave Sankey, July 2019
--
-- Heavily based on IPBus mac_arbiter:
-- "Arbitrates access by several packet sources to a single MAC core
-- This version implements simple round-robin polling.
--
-- Dave Newbold, March 2011"

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

LIBRARY infrastructure_lib;
use infrastructure_lib.packet_mux_type.all;

entity efex_packet_mux is
  generic(NSRC: positive);
  port (
	clk					: in std_logic;
	rst_clk				: in std_logic;
	packet_mux_enabled  : IN packet_sl_array(NSRC-1 downto 0);
-- Input signals
	packet_mux_data     : IN packet_data_array(NSRC-1 downto 0);
	packet_mux_valid    : IN packet_sl_array(NSRC-1 downto 0);
   	packet_mux_hdr      : IN packet_sl_array(NSRC-1 downto 0);
	packet_mux_data_end : IN packet_sl_array(NSRC-1 downto 0);
	packet_mux_ready	: OUT packet_sl_array(NSRC-1 downto 0);
-- FIFO signals
	packet_data         : OUT std_logic_vector (63 DOWNTO 0) ;
	packet_valid        : OUT std_logic;
   	packet_hdr          : OUT std_logic;
	packet_data_end     : OUT std_logic;
	packet_ready		: IN std_logic
  );
end efex_packet_mux;

architecture rtl of efex_packet_mux is

	signal src: unsigned(3 downto 0) := (Others => '0'); -- Up to 16 sources...
	signal sel: integer range 0 to NSRC - 1 := 0;
	signal active, gotlast: std_logic;

begin

	sel <= to_integer(src);

	process(clk)
	begin
		if rising_edge(clk) then
			if rst_clk = '1' then
				active <= '0';
				gotlast <= '0';
				src <= "0000";
			elsif active = '0' then
				if (packet_mux_valid(sel) = '1') and (packet_mux_enabled(sel) = '1') then
					active <= '1';
				else
					if src /= (NSRC-1) then
						src <= src + 1;
					else
						src <= (others => '0');
					end if;
				end if;
				gotlast <= '0';
			elsif packet_mux_data_end(sel) = '1' then  -- End of Frame...
				gotlast <= '1';
			elsif (gotlast = '1') and (packet_mux_valid(sel) = '0') then  -- End of Frame...
				active <= '0';
				gotlast <= '0';
			end if;
		end if;
	end process;

-- mask signal when hunting for data...
	packet_valid <= packet_mux_valid(sel) when active = '1' else '0';
	packet_data <= packet_mux_data(sel);
	packet_hdr <= packet_mux_hdr(sel);
	packet_data_end <= packet_mux_data_end(sel);

	ackgen: for i in NSRC - 1 downto 0 generate
	begin
		packet_mux_ready(i) <= packet_ready when (sel = i) and (active = '1') else '0';
	end generate;

end rtl;

