onerror {resume}
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(3 downto 0)} Is_Max
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(7 downto 4)} Up_not_down
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(9 downto 8)} Seed
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(31 downto 16)} ClusterEnergy
quietly virtual signal -install /tb_algocore_eg {/tb_algocore_eg/output_tob(0)  } TOB_IsGlobalMax
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(3 downto 1)} UpNotDownSeed
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(20 downto 4)} TOB_ClusterEnergy
quietly virtual signal -install /tb_algocore_eg {/tb_algocore_eg/UpNotDownSeed  } jjjj
quietly virtual signal -install /tb_algocore_eg {/tb_algocore_eg/UpNotDownSeed  } TOB_Seed_UpNOtDown_Seed
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(3 downto 1)} TOB_UPDownSeed
quietly virtual signal -install /tb_algocore_eg {/tb_algocore_eg/TOB_UPDownSeed  } TOB_Seed
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(3 downto 1)} TOB_Seed001
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(3 downto 1)} TOB_Seed002
quietly virtual signal -install /tb_algocore_eg {/tb_algocore_eg/output_tob(1)  } TOB_UpNotDown
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(4 downto 2)} TOB_Seed003
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(3 downto 2)} TOB_Centre
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(24 downto 8)} TOB_energy
quietly virtual signal -install /tb_algocore_eg {/tb_algocore_eg/output_tob(4)  } TOB_REtaCondition
quietly virtual signal -install /tb_algocore_eg {/tb_algocore_eg/output_tob(5)  } TOB_f3Condition
quietly virtual signal -install /tb_algocore_eg {/tb_algocore_eg/output_tob(6)  } TOB_HadronCondition
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(5 downto 4)} TOB_REta
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(7 downto 6)} TOB_f3
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(9 downto 8)} TOB_Had
quietly virtual signal -install /tb_algocore_eg { /tb_algocore_eg/output_tob(25 downto 10)} TOB_Energy001
quietly virtual signal -install /tb_algocore_eg {/tb_algocore_eg/output_tob(26)  } TOB_Energy_OF
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_algocore_eg/clk40
add wave -noupdate /tb_algocore_eg/clk160
add wave -noupdate /tb_algocore_eg/clk200
add wave -noupdate /tb_algocore_eg/rst
add wave -noupdate /tb_algocore_eg/TowerData
add wave -noupdate /tb_algocore_eg/DataReady
add wave -noupdate /tb_algocore_eg/input_data
add wave -noupdate /tb_algocore_eg/output_tob
add wave -noupdate /tb_algocore_eg/output_tobRd
add wave -noupdate /tb_algocore_eg/ParD3
add wave -noupdate /tb_algocore_eg/ParCEta
add wave -noupdate /tb_algocore_eg/ParHadron
add wave -noupdate /tb_algocore_eg/Control
add wave -noupdate /tb_algocore_eg/Status
add wave -noupdate -divider -height 40 <NULL>
add wave -noupdate /tb_algocore_eg/clk200
add wave -noupdate /tb_algocore_eg/rst
add wave -noupdate /tb_algocore_eg/TowerData
add wave -noupdate /tb_algocore_eg/DataReady
add wave -noupdate /tb_algocore_eg/input_data
add wave -noupdate /tb_algocore_eg/output_tob
add wave -noupdate /tb_algocore_eg/output_tobRd
add wave -noupdate -divider {eg Modules}
add wave -noupdate -group SeedFinder -radixshowbase 0 /tb_algocore_eg/EG_ALGO/SEED_FINDER/CLK
add wave -noupdate -group SeedFinder -radix hexadecimal -radixshowbase 0 /tb_algocore_eg/EG_ALGO/SEED_FINDER/RESET
add wave -noupdate -group SeedFinder -radixshowbase 0 /tb_algocore_eg/EG_ALGO/SEED_FINDER/IN_Data
add wave -noupdate -group SeedFinder -radixshowbase 0 /tb_algocore_eg/EG_ALGO/SEED_FINDER/IN_DataUp
add wave -noupdate -group SeedFinder -radixshowbase 0 /tb_algocore_eg/EG_ALGO/SEED_FINDER/IN_DataDown
add wave -noupdate -group SeedFinder -radixshowbase 0 /tb_algocore_eg/EG_ALGO/SEED_FINDER/Result
add wave -noupdate -group SeedFinder -radixshowbase 0 /tb_algocore_eg/EG_ALGO/SEED_FINDER/IsMaxMax
add wave -noupdate -group SeedFinder -radixshowbase 0 /tb_algocore_eg/EG_ALGO/SEED_FINDER/UpOrDown
add wave -noupdate -group SeedFinder -radixshowbase 0 /tb_algocore_eg/EG_ALGO/SEED_FINDER/OUT_UpNotDown
add wave -noupdate -group SeedFinder -color Cyan /tb_algocore_eg/EG_ALGO/SEED_FINDER/OUT_Seed
add wave -noupdate -group SeedFinder -color Cyan /tb_algocore_eg/EG_ALGO/SEED_FINDER/OUT_IsLocalMax
add wave -noupdate -group SeedFinder -color Cyan /tb_algocore_eg/EG_ALGO/SEED_FINDER/OUT_IsMax
add wave -noupdate -group InputMultiplexer /tb_algocore_eg/EG_ALGO/INPUT_MULTIPLEXER/CLK
add wave -noupdate -group InputMultiplexer /tb_algocore_eg/EG_ALGO/INPUT_MULTIPLEXER/RESET
add wave -noupdate -group InputMultiplexer /tb_algocore_eg/EG_ALGO/INPUT_MULTIPLEXER/IN_Seed
add wave -noupdate -group InputMultiplexer /tb_algocore_eg/EG_ALGO/INPUT_MULTIPLEXER/IN_UpNotDown
add wave -noupdate -group InputMultiplexer /tb_algocore_eg/EG_ALGO/INPUT_MULTIPLEXER/IN_Towers
add wave -noupdate -group InputMultiplexer /tb_algocore_eg/EG_ALGO/INPUT_MULTIPLEXER/Selector
add wave -noupdate -group InputMultiplexer /tb_algocore_eg/EG_ALGO/INPUT_MULTIPLEXER/ShiftTowers
add wave -noupdate -group InputMultiplexer /tb_algocore_eg/EG_ALGO/INPUT_MULTIPLEXER/ShiftTowers2
add wave -noupdate -group InputMultiplexer /tb_algocore_eg/EG_ALGO/INPUT_MULTIPLEXER/OUT_REtaCoreData
add wave -noupdate -group InputMultiplexer /tb_algocore_eg/EG_ALGO/INPUT_MULTIPLEXER/OUT_REtaEnvData
add wave -noupdate -group InputMultiplexer /tb_algocore_eg/EG_ALGO/INPUT_MULTIPLEXER/OUT_f3CoreData
add wave -noupdate -group InputMultiplexer /tb_algocore_eg/EG_ALGO/INPUT_MULTIPLEXER/OUT_f3EnvDataL2
add wave -noupdate -group InputMultiplexer /tb_algocore_eg/EG_ALGO/INPUT_MULTIPLEXER/OUT_f3EnvDataL1
add wave -noupdate -group InputMultiplexer /tb_algocore_eg/EG_ALGO/INPUT_MULTIPLEXER/OUT_HadCoreData
add wave -noupdate -divider Adders
add wave -noupdate -group Adder_RetaEnv /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_ENV/CLK
add wave -noupdate -group Adder_RetaEnv /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_ENV/RESET
add wave -noupdate -group Adder_RetaEnv /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_ENV/IN_Words
add wave -noupdate -group Adder_RetaEnv /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_ENV/connector
add wave -noupdate -group Adder_RetaEnv /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_ENV/carry
add wave -noupdate -group Adder_RetaEnv /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_ENV/DelayedOut
add wave -noupdate -group Adder_RetaEnv /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_ENV/DelayedOverflow
add wave -noupdate -group Adder_RetaEnv -color Cyan /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_ENV/OUT_Overflow
add wave -noupdate -group Adder_RetaEnv -color Cyan /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_ENV/OUT_Word
add wave -noupdate -group Adder_RetaCore /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_CORE/CLK
add wave -noupdate -group Adder_RetaCore /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_CORE/RESET
add wave -noupdate -group Adder_RetaCore /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_CORE/IN_Words
add wave -noupdate -group Adder_RetaCore /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_CORE/connector
add wave -noupdate -group Adder_RetaCore /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_CORE/carry
add wave -noupdate -group Adder_RetaCore /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_CORE/DelayedOut
add wave -noupdate -group Adder_RetaCore /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_CORE/DelayedOverflow
add wave -noupdate -group Adder_RetaCore -color Cyan /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_CORE/OUT_Overflow
add wave -noupdate -group Adder_RetaCore -color Cyan -radix hexadecimal /tb_algocore_eg/EG_ALGO/MULTI_ADDER_RETA_CORE/OUT_Word
add wave -noupdate -group Adder_f3Core /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3_CORE/CLK
add wave -noupdate -group Adder_f3Core /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3_CORE/RESET
add wave -noupdate -group Adder_f3Core /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3_CORE/IN_Words
add wave -noupdate -group Adder_f3Core /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3_CORE/connector
add wave -noupdate -group Adder_f3Core /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3_CORE/carry
add wave -noupdate -group Adder_f3Core /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3_CORE/DelayedOut
add wave -noupdate -group Adder_f3Core /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3_CORE/DelayedOverflow
add wave -noupdate -group Adder_f3Core -color Cyan /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3_CORE/OUT_Overflow
add wave -noupdate -group Adder_f3Core -color Cyan /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3_CORE/OUT_Word
add wave -noupdate -group Adder_f3Env /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3ENV/CLK
add wave -noupdate -group Adder_f3Env /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3ENV/RESET
add wave -noupdate -group Adder_f3Env /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3ENV/IN_Words
add wave -noupdate -group Adder_f3Env /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3ENV/connector
add wave -noupdate -group Adder_f3Env /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3ENV/carry
add wave -noupdate -group Adder_f3Env /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3ENV/DelayedOut
add wave -noupdate -group Adder_f3Env /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3ENV/DelayedOverflow
add wave -noupdate -group Adder_f3Env -color Cyan /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3ENV/OUT_Overflow
add wave -noupdate -group Adder_f3Env -color Cyan /tb_algocore_eg/EG_ALGO/MULTI_ADDER_F3ENV/OUT_Word
add wave -noupdate -group Adder_HadCore /tb_algocore_eg/EG_ALGO/MULTI_ADDER_HAD_CORE/CLK
add wave -noupdate -group Adder_HadCore /tb_algocore_eg/EG_ALGO/MULTI_ADDER_HAD_CORE/RESET
add wave -noupdate -group Adder_HadCore /tb_algocore_eg/EG_ALGO/MULTI_ADDER_HAD_CORE/IN_Words
add wave -noupdate -group Adder_HadCore /tb_algocore_eg/EG_ALGO/MULTI_ADDER_HAD_CORE/connector
add wave -noupdate -group Adder_HadCore /tb_algocore_eg/EG_ALGO/MULTI_ADDER_HAD_CORE/carry
add wave -noupdate -group Adder_HadCore /tb_algocore_eg/EG_ALGO/MULTI_ADDER_HAD_CORE/DelayedOut
add wave -noupdate -group Adder_HadCore /tb_algocore_eg/EG_ALGO/MULTI_ADDER_HAD_CORE/DelayedOverflow
add wave -noupdate -group Adder_HadCore -color Cyan /tb_algocore_eg/EG_ALGO/MULTI_ADDER_HAD_CORE/OUT_Overflow
add wave -noupdate -group Adder_HadCore -color Cyan /tb_algocore_eg/EG_ALGO/MULTI_ADDER_HAD_CORE/OUT_Word
add wave -noupdate -divider Multipliers
add wave -noupdate /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/CLK
add wave -noupdate -radix hexadecimal -radixshowbase 1 /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/IN_Word
add wave -noupdate -radix hexadecimal -radixshowbase 1 /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/IN_parameters
add wave -noupdate -radix hexadecimal -radixshowbase 1 /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/OUT_Overflow
add wave -noupdate -radix hexadecimal -childformat {{/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/OUT_Words(2) -radix hexadecimal} {/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/OUT_Words(1) -radix hexadecimal} {/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/OUT_Words(0) -radix hexadecimal}} -radixshowbase 1 -subitemconfig {/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/OUT_Words(2) {-radix hexadecimal -radixshowbase 1} /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/OUT_Words(1) {-radix hexadecimal -radixshowbase 1} /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/OUT_Words(0) {-radix hexadecimal -radixshowbase 1}} /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/OUT_Words
add wave -noupdate -radix hexadecimal -childformat {{/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Param(2) -radix hexadecimal} {/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Param(1) -radix hexadecimal} {/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Param(0) -radix hexadecimal}} -radixshowbase 0 -expand -subitemconfig {/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Param(2) {-radix hexadecimal -radixshowbase 1} /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Param(1) {-radix hexadecimal -radixshowbase 1} /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Param(0) {-radix hexadecimal -radixshowbase 1}} /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Param
add wave -noupdate -radix hexadecimal -childformat {{/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Data(2) -radix hexadecimal} {/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Data(1) -radix hexadecimal} {/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Data(0) -radix hexadecimal}} -radixshowbase 0 -expand -subitemconfig {/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Data(2) {-radix hexadecimal -radixshowbase 0} /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Data(1) {-radix hexadecimal -radixshowbase 0} /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Data(0) {-radix hexadecimal -radixshowbase 0}} /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Data
add wave -noupdate -radix hexadecimal -childformat {{/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Overflow(2) -radix hexadecimal} {/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Overflow(1) -radix hexadecimal} {/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Overflow(0) -radix hexadecimal}} -radixshowbase 1 -expand -subitemconfig {/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Overflow(2) {-radix hexadecimal -radixshowbase 1} /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Overflow(1) {-radix hexadecimal -radixshowbase 1} /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Overflow(0) {-radix hexadecimal -radixshowbase 1}} /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Overflow
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Result_big
add wave -noupdate -radix hexadecimal -childformat {{/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Result(2) -radix hexadecimal} {/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Result(1) -radix hexadecimal} {/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Result(0) -radix hexadecimal}} -radixshowbase 0 -expand -subitemconfig {/tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Result(2) {-radix hexadecimal -radixshowbase 0} /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Result(1) {-radix hexadecimal -radixshowbase 0} /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Result(0) {-radix hexadecimal -radixshowbase 0}} /tb_algocore_eg/EG_ALGO/RETA_MULTIPLIER/Result
add wave -noupdate -divider Results
add wave -noupdate /tb_algocore_eg/EG_ALGO/MA_REtaEnvOverflow
add wave -noupdate /tb_algocore_eg/EG_ALGO/MA_REtaEnvSum
add wave -noupdate /tb_algocore_eg/EG_ALGO/MA_REtaCoreOverflow
add wave -noupdate /tb_algocore_eg/EG_ALGO/MA_REtaCoreSum
add wave -noupdate -expand /tb_algocore_eg/EG_ALGO/MU_REtaEnvMult
add wave -noupdate -divider <NULL>
add wave -noupdate /tb_algocore_eg/EG_ALGO/MA_f3CoreOverflow
add wave -noupdate /tb_algocore_eg/EG_ALGO/MA_f3CoreSum
add wave -noupdate /tb_algocore_eg/EG_ALGO/MA_f3EnvOverflow
add wave -noupdate /tb_algocore_eg/EG_ALGO/MA_f3EnvSum
add wave -noupdate /tb_algocore_eg/EG_ALGO/MU_f3CoreMult
add wave -noupdate -divider <NULL>
add wave -noupdate /tb_algocore_eg/EG_ALGO/MA_HadCoreOverflow
add wave -noupdate /tb_algocore_eg/EG_ALGO/MA_HadCoreSum
add wave -noupdate /tb_algocore_eg/EG_ALGO/DL_UpNotDown
add wave -noupdate /tb_algocore_eg/EG_ALGO/DL_Seed
add wave -noupdate /tb_algocore_eg/EG_ALGO/DL_IsLocalMax
add wave -noupdate /tb_algocore_eg/EG_ALGO/DL_IsMax
add wave -noupdate -divider TriggerObject
add wave -noupdate -radix hexadecimal -childformat {{/tb_algocore_eg/output_tob(31) -radix hexadecimal} {/tb_algocore_eg/output_tob(30) -radix hexadecimal} {/tb_algocore_eg/output_tob(29) -radix hexadecimal} {/tb_algocore_eg/output_tob(28) -radix hexadecimal} {/tb_algocore_eg/output_tob(27) -radix hexadecimal} {/tb_algocore_eg/output_tob(26) -radix hexadecimal} {/tb_algocore_eg/output_tob(25) -radix hexadecimal} {/tb_algocore_eg/output_tob(24) -radix hexadecimal} {/tb_algocore_eg/output_tob(23) -radix hexadecimal} {/tb_algocore_eg/output_tob(22) -radix hexadecimal} {/tb_algocore_eg/output_tob(21) -radix hexadecimal} {/tb_algocore_eg/output_tob(20) -radix hexadecimal} {/tb_algocore_eg/output_tob(19) -radix hexadecimal} {/tb_algocore_eg/output_tob(18) -radix hexadecimal} {/tb_algocore_eg/output_tob(17) -radix hexadecimal} {/tb_algocore_eg/output_tob(16) -radix hexadecimal} {/tb_algocore_eg/output_tob(15) -radix hexadecimal} {/tb_algocore_eg/output_tob(14) -radix hexadecimal} {/tb_algocore_eg/output_tob(13) -radix hexadecimal} {/tb_algocore_eg/output_tob(12) -radix hexadecimal} {/tb_algocore_eg/output_tob(11) -radix hexadecimal} {/tb_algocore_eg/output_tob(10) -radix hexadecimal} {/tb_algocore_eg/output_tob(9) -radix hexadecimal} {/tb_algocore_eg/output_tob(8) -radix hexadecimal} {/tb_algocore_eg/output_tob(7) -radix hexadecimal} {/tb_algocore_eg/output_tob(6) -radix hexadecimal} {/tb_algocore_eg/output_tob(5) -radix hexadecimal} {/tb_algocore_eg/output_tob(4) -radix hexadecimal} {/tb_algocore_eg/output_tob(3) -radix hexadecimal} {/tb_algocore_eg/output_tob(2) -radix hexadecimal} {/tb_algocore_eg/output_tob(1) -radix hexadecimal} {/tb_algocore_eg/output_tob(0) -radix hexadecimal}} -radixshowbase 0 -subitemconfig {/tb_algocore_eg/output_tob(31) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(30) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(29) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(28) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(27) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(26) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(25) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(24) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(23) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(22) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(21) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(20) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(19) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(18) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(17) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(16) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(15) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(14) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(13) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(12) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(11) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(10) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(9) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(8) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(7) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(6) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(5) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(4) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(3) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(2) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(1) {-height 17 -radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(0) {-height 17 -radix hexadecimal -radixshowbase 0}} /tb_algocore_eg/output_tob
add wave -noupdate /tb_algocore_eg/EG_ALGO/OUT_TOBRd
add wave -noupdate /tb_algocore_eg/TOB_REta
add wave -noupdate /tb_algocore_eg/TOB_f3
add wave -noupdate /tb_algocore_eg/TOB_Had
add wave -noupdate /tb_algocore_eg/TOB_Energy001
add wave -noupdate /tb_algocore_eg/TOB_Energy_OF
add wave -noupdate /tb_algocore_eg/TOB_UpNotDown
add wave -noupdate -radix hexadecimal -childformat {{/tb_algocore_eg/TOB_Centre(3) -radix hexadecimal} {/tb_algocore_eg/TOB_Centre(2) -radix hexadecimal}} -radixshowbase 0 -subitemconfig {/tb_algocore_eg/output_tob(3) {-radix hexadecimal -radixshowbase 0} /tb_algocore_eg/output_tob(2) {-radix hexadecimal -radixshowbase 0}} /tb_algocore_eg/TOB_Centre
add wave -noupdate /tb_algocore_eg/TOB_IsGlobalMax
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 3} {365000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 213
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix -1
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 625
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {332593 ps} {384585 ps}
