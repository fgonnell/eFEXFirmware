--! @file
--! @brief Algo Rate Monitor
--! @details 
--! Algorithm TOB output rate monitor
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library infrastructure_lib;
use infrastructure_lib.counter;

library work;
--! Use internal algorithm data types and functions
use work.DataTypes.all;
--! Use external algorithm data types and functions
use work.AlgoDataTypes.all;

--! Use ipbus ibrary
library ipbus_lib;
--! Use ipbus ibrary
use ipbus_lib.ipbus_reg_types.all;
--! Use ipbus ibrary
use ipbus_lib.ipbus.all;

--! @copydoc AlgoRateMonitor.vhd
entity AlgoRateMonitor is
  port (
    ipb_clk : in  std_logic;    --! IPBus clk
    ipb_rst : in  std_logic;    --! IPBus reset
    ipb_in  : in  ipb_wbus;     --! IPBus write bus
    ipb_out : out ipb_rbus;     --! IPBus read bus

    clk      : in std_logic;
    IN_BCID  : in std_logic_vector(6 downto 0);
    IN_synch : in std_logic;
    IN_eta   : in std_logic_vector(2 downto 0);

    IN_eg_energies : in array_std_logic_vector(OUTPUT_TOBS-1 downto 0)(ENERGY_WIDTH-1 downto 0);
    IN_eg_valids   : in std_logic_vector(OUTPUT_TOBS-1 downto 0);

    IN_tau_energies : in array_std_logic_vector(OUTPUT_TOBS-1 downto 0)(ENERGY_WIDTH-1 downto 0);
    IN_tau_valids   : in std_logic_vector(OUTPUT_TOBS-1 downto 0));

end AlgoRateMonitor;


architecture Behavioral of AlgoRateMonitor is
  constant N_CTRL : positive := 2;   --! Number of control IPBus reg
  constant N_STAT : positive := 42;  --! Number of status IPBus reg

  signal write_reg : ipb_reg_v(N_STAT - 1 downto 0) := (others => (others => '0'));
  signal read_reg  : ipb_reg_v(N_CTRL - 1 downto 0);

  signal enable, counter_reset, reset, sync        : std_logic;
  signal energy_threshold                          : std_logic_vector(ENERGY_WIDTH-1 downto 0);
  signal sync_d                                    : std_logic_vector (2 downto 0);
  signal counter_start, counter_stop, counter_done : std_logic;
  signal eg_or_tau                                 : std_logic;
  signal status, control                           : std_logic_vector(31 downto 0);

  signal right_eta                             : std_logic_vector(4 downto 0);
  signal eta_eg, eta_tau, valid_tob, count_tob : std_logic_vector(7 downto 0);
  type STATE_TYPE is (ready, idle, run);
  signal state                                 : STATE_TYPE;

--! @copydoc AlgoRateMonitor.vhd
begin
  IPBUS_ALGO_REGISTERS : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => N_CTRL,         --number of control reg
      N_STAT => N_STAT)         --number of status reg
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipb_in,
      ipbus_out => ipb_out,
      d         => write_reg,
      q         => read_reg,
--    qmask     => (others => '0'),
      stb       => open);

  control          <= read_reg(0);
  energy_threshold <= read_reg(1)(15 downto 0);

  write_reg(41) <= status;

  -- registers connections
  counter_start <= control(0);
  counter_stop  <= control(1);
  counter_reset <= control(2);
  eg_or_tau     <= control(3);

  --! This process delays sync by 4 so to anticipate it by 1
  synch_delay : process (clk)
  begin
    if rising_edge(clk) then
      sync_d(0)          <= IN_synch;
      sync_d(2 downto 1) <= sync_d(1 downto 0);
      sync               <= sync_d(2);
    end if;
  end process;


  enable_proc : process (clk)
  begin
    if rising_edge(clk) then
      if sync = '1' then
        if counter_reset = '1' then
          state  <= ready;
          reset  <= '1';
          enable <= '0';
        else
          case state is
            when idle =>
              state <= idle;
              reset <= '0';

            when ready =>
              if counter_start = '1' then
                state  <= run;
                reset  <= '0';
                enable <= '1';
              else
                state  <= ready;
                reset  <= '1';
                enable <= '0';
              end if;
              
            when run =>
              if counter_stop = '1' then
                state  <= idle;
                reset  <= '0';
                enable <= '0';
              else
                state  <= run;
                reset  <= '0';
                enable <= '1';
              end if;
          end case;
        end if;
      else
        state  <= state;
        enable <= enable;
        reset  <= reset;
      end if;
    end if;
  end process;

  -- status connections
  status(1 downto 0) <= "11" when state = run else
                        "01" when state = ready else
                        "00";   --idle
  status(2) <= enable;
  status(3) <= reset;


  eg_tau_for : for phi in OUTPUT_TOBS-1 downto 0 generate
    eta_eg(phi)  <= '1' when IN_eg_valids(phi) = '1' and not (unsigned(IN_eg_energies(phi)) < unsigned(energy_threshold))   else '0';
    eta_tau(phi) <= '1' when IN_tau_valids(phi) = '1' and not (unsigned(IN_tau_energies(phi)) < unsigned(energy_threshold)) else '0';
  end generate;

  count_tob <= eta_tau when eg_or_tau = '1' else eta_eg;  --select if counter is enabled by eg or tau

  eta_for : for eta in 4 downto 0 generate
    right_eta(eta) <= '1' when unsigned(IN_eta) = eta else '0';
    phi_for : for phi in OUTPUT_TOBS-1 downto 0 generate
      CNT : entity infrastructure_lib.counter
        generic map (DEPTH => 32)
        port map (
          clk     => clk,
          enable1 => enable and right_eta(eta),
          enable2 => count_tob(phi),
          count   => write_reg(phi*5 + eta),
          reset   => reset
          );
    end generate;
  end generate;


  NORMALISATION_CNT : entity infrastructure_lib.counter
    generic map (DEPTH => 32)
    port map (
      clk     => clk,
      enable1 => enable,
      enable2 => IN_synch,      --this shouldn't be delayed to be in time with data
      count   => write_reg(40),
      reset   => reset
      );

end Behavioral;
