
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;


ENTITY header_data IS
   PORT( 
      clk      : in     std_logic;
      reset    : in     std_logic;
	  l1A      : in     std_logic;
	  bcn_cntr : in     std_logic_vector(11 downto 0);
	  L1ID_in  : in     std_logic_vector(31 downto 0);
	  L1ID_out : out    std_logic_vector(31 downto 0);
	  BCN      : out    std_logic_vector(11 downto 0) 
   );


END ENTITY header_data ;


ARCHITECTURE sm OF header_data IS

   TYPE STATE_TYPE IS (
      idle,
      L1A_st
   );
 
   -- Declare current and next state signals
   SIGNAL current_state : STATE_TYPE;

BEGIN

   -----------------------------------------------------------------
   clocked_proc : PROCESS ( 
      clk,
      reset
   )
   -----------------------------------------------------------------
   BEGIN
      IF (reset = '1') THEN
         L1ID_out <= (others => '0');
         BCN  <= (others => '0');
         current_state <= idle;
      ELSIF (clk'EVENT AND clk = '1') THEN

         -- Combined Actions
         CASE current_state IS
            WHEN idle => 
               L1ID_out <= (others => '0');
               BCN  <= (others => '0');
               IF (l1A ='1') THEN 
                  current_state <=  L1A_st;
               ELSE
                  current_state <= idle;
               END IF;
            WHEN  L1A_st => 
                L1ID_out <=  L1ID_in;
               bcn  <= bcn_cntr;
               IF (l1A ='0') THEN 
                  current_state <= idle;
               ELSE
                  current_state <=  L1A_st;
               END IF;
            WHEN OTHERS =>
               current_state <= idle;
         END CASE;
      END IF;
   END PROCESS clocked_proc;
 
END ARCHITECTURE sm;
