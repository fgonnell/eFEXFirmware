----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 19.07.2018 18:18:54
-- Design Name: 
-- Module Name: testbench_phase - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity testbench_phase is

Port (                                                 
                       
    reset        : out std_logic ;  
    sorted_tob  : out std_logic_vector(31 downto 0);
    valid        : out std_logic ;
    synch        : out std_logic ;
    bcn          : out std_logic_vector ( 3 downto 0) 
                           
 );                                                     
end testbench_phase;

architecture Behavioral of testbench_phase is

constant clk_prd : time := 3.57 ns;  
signal iclk      : std_logic;   

signal cntr :unsigned ( 2 downto 0):= (others => '0');

signal num  :unsigned ( 7 downto 0):= (others => '0');

BEGIN
  
  process 
  

 
   Begin
	
       Reset  <= '1';         
       sorted_TOB <= (others => '0'); 
       bcn        <= (others => '0');
       valid      <= '0';
       synch      <= '0'; 
       wait for 5*clk_prd;
       Reset       <= '0';
       wait for  7*clk_prd  ;
       synch <=  '1';
       valid <= '0';
       --bcn   <= "0001";
       wait for clk_prd;
       synch      <= '0';
       wait for clk_prd*5;
       
num_frame: while num < 10 loop
              wait for clk_prd;           
              num  <= num +1;     
          
               
               synch <=  '1';
  empty_frame: while cntr < 5 loop
                wait for clk_prd; 
                 synch <=  '0';         
                cntr  <= cntr+1;
               
              end loop;
               
          cntr  <= (others => '0'); 
       
   end loop; 
      
        wait for clk_prd;      
       valid <= '1';
       synch <=  '1';
       sorted_TOB <= x"00000001";  
       wait for clk_prd ;
       valid <= '0';
       synch  <=  '0';
       sorted_TOB <= x"00000000"; 
       wait for  clk_prd ;
        sorted_TOB <= x"00000000"; 
       wait for clk_prd ;
        sorted_TOB <= x"00000000"; 
       wait for  clk_prd ; 
       sorted_TOB <= x"00000000"; 
      -- valid <= '0';
       wait for clk_prd ;
       valid <= '0';
       sorted_TOB <= x"00000000";
       wait for clk_prd ; 
       sorted_TOB <= x"00000000"; 
       wait for clk_prd ; 
       valid <= '1';
       synch <=  '1';
        sorted_TOB <= x"00000005"; 
       wait for clk_prd ;
       synch <=  '0';
        sorted_TOB <= x"00000006"; 
       wait for  clk_prd ;
        sorted_TOB <= x"00000000"; 
        valid <= '0';
       wait for clk_prd ;
        sorted_TOB <= x"00000000"; 
       wait for clk_prd ;
        sorted_TOB <= x"00000000";  
       wait for  clk_prd ;
       sorted_TOB <= x"00000000"; 
       wait for  clk_prd ; 
       sorted_TOB <= x"00000000"; 
       wait for  clk_prd ;
       valid <= '0';
       --wait for  clk_prd ;
       synch <=  '1';
       wait for  clk_prd ;  
       synch <=  '0';
       wait for  clk_prd*6 ;
       synch <=  '1';
       wait for  clk_prd ;  
       synch <=  '0';
       wait for  clk_prd*6 ;       
       synch <=  '1';
       valid <= '1';
       sorted_TOB <= x"00000011";  
       wait for clk_prd ;
       synch  <=  '0';
       valid <= '0';
       sorted_TOB <= x"00000000";  
       wait for  clk_prd*6 ;  
       valid <= '1';
       synch <=  '1';          
       sorted_TOB <= x"00000012"; 
       wait for  clk_prd ;
       synch <=  '0';  
       sorted_TOB <= x"00000013"; 
        wait for clk_prd ;
        valid <= '0';
       sorted_TOB <= x"00000000"; 
       wait for  clk_prd*5 ;
       synch <=  '1';
       wait for  clk_prd ;
       synch <=  '0';
       wait for  clk_prd*6 ;
       synch <=  '1';
       wait for  clk_prd ;
       synch <=  '0';
       wait for  clk_prd*6 ;
       synch <=  '1';
       valid <= '1';      
      sorted_TOB <= x"00000014"; 
       wait for  clk_prd ; 
        synch <=  '0';      
       sorted_TOB <= x"00000015"; 
       --valid <= '0';
       wait for clk_prd ;
       sorted_TOB <= x"00000016";
       wait for clk_prd ; 
       sorted_TOB <= x"00000017"; 
       wait for clk_prd ; 
       valid <= '0';
       synch <=  '1';
       sorted_TOB <= x"00000000"; 
       
   
--dataloop: while cntr < 12 loop
--             wait for 10 ns; 
--              datain <=  std_logic_VECTOR(cntr+1);
--              cntr  <= cntr+1;
--          end loop;
--          
          wait for 1 us;
   
   
   
       
     end process;

     
end Behavioral;
