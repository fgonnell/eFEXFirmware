"""
Created on Thu Apr 23 2020
@author Alexios Stampekis
Decoding the output of the Mapping Simulation
"""
import os, sys
import numpy as np
import OutputComponents as OutC

def Run(version, finPath):
    
    fin = open(finPath)  
    print("# eFEX Firmware Mapping Simulation Output file loaded. To be decoded.\n")
    fout= open('Mapping_file.txt', 'w')
    
    version=int(version)
    InW=OutC.InputArray(version)  # array holding the input pattern; to compare with
    
    if version>3:
        sys.exit("# ** Error! Put an Input version < 4 as argument. \nFor now versions 1, 2, 3 have been defined")

    Nbc=InW.shape[0]
    
    BC=0         #counts all BCs, even the offset; it is a counter for the lines to be grouped in packets of 61
    BCreal=0     #skips the offset
    uCounter=0   #flag for number of lines containing u's in one BC
    WFlag=0      #warnings' counter. something to help with printing
    Nlines=0     #we want to count the lines of output file to see if we get all BCs
        
    #-----------List that holds the 16 HEX digits. Used for sanity check per line---------#
    HEX=[]
    for i in range(16):
        v=hex(i)
        v1=v[2:].upper()
        HEX.append(v1)
    #----------------------------------------------------------------#

    B=InW[BCreal:BCreal+1]     #slice the InW to keep only the current BC
    C=B[B!=0]			       #1-D array (as list) storing the Input nonzero word values of current BC
    Cunq=np.unique(C)          #1-D array storing the unique Input nonzero words of current BC. Must be same as C  
    SameWcount=np.zeros(C.size, dtype=int)  #1-D array that stores the number of times a word value appears in one BC; shouldn't be more than once!

    MyMap=[]    # Python List of tuples (mgt, word, "tt+sc"): to hold word mapping "on the run"
    MyMapAux=[] # auxiliary list. Will only hold "active" word mappings (not xxxx's)
    
#-----------------------start work-------------------------#
    fout.write('MGT'+'\t'+'WORD'+'\t'+'eta'+'\t'+'phi'+'\t'+'Layer'+'\t'+'SuperCell'+'\n')

    for i, line in enumerate(fin):
        Nlines+=1
        
        if i==1000000:                    # just use less lines if you want a quick test
            break

        if i<60*(BC+1)+BC:                # we take as granted that each BC is 60 lines "wide"
                                          # someone could also use the empty line after that as new BC checkpoint
            if 'u' in line:
                uCounter+=1
                continue
     
            for v in line.strip():
                if v==' ':
                    continue
                if v not in HEX:
                    print("# * Warning! A non hex digit found in line", i)
                    WFlag+=1
            
            for k, v in enumerate(C):  
                m=hex(v)
                M=m[2:].upper()
                n=M.zfill(3)  #convert word value from int to 3 digit hex
                if n in line:
                    A=np.argwhere(B==v)
                    mapit=OutC.decoder(i, line, n)
                    A1=A[0][1]    # the MGT number
                    A2=A[0][2]    # the WORD number
                    MyMap.append((A1, A2, mapit[0]))
                    MyMapAux.append((A1, A2, mapit[0]))
                    SameWcount[k]+=1 
                    WFlag+=mapit[1]
    
        if i==60*(BC+1)+BC:                           # is an empty line! after that we enter a new BC
            
            if uCounter>0:
                print('# * Warning in BC:', BC, '. It is an offset! ( uFLAG =', uCounter, ') . Ok, we skip it.\n')
                uCounter=0
                BC+=1
                #print(BC, BCreal)  #generally different by 1
                continue
            
            if C.size!=Cunq.size:
                print("# * Warning! Did you provide/input duplicate words in BC:", BCreal, "??")
                WFlag+=1
                
            for k, v in enumerate(SameWcount):
                if v==0:
                    A=np.argwhere(B==C[k])
                    A1=A[0][1]     # the MGT number
                    A2=A[0][2]     # the WORD number
                    MyMap.append((A1, A2, 'xxxx'))

            if np.any(SameWcount>1):
                print('# ** WARNING!! In BC:', BCreal, '. SAME WORDS FOUND!!')
                WFlag=+1
            
            if BC>Nbc-2:
                if BC==Nbc-1 and WFlag>0:
                    print()
                elif BC==Nbc-1 and WFlag==0:
                    print("# ...decoding goes smoothly...")
                print("# approaching end of file... bc and line:", BC, i)   # auxiliary print; we reached the end!
            
            BC+=1                               #reset counters
            BCreal+=1
            B=InW[BCreal:BCreal+1]
            C=B[B!=0]
            Cunq=np.unique(C)
            SameWcount=np.zeros(C.size, dtype=int)  


    if BCreal==Nbc:
        print("\n# OK, we got all", Nbc, "BCs in the output file. The lines were:", Nlines, "\n")
    else:
        print("\n# * Warning. Some lines missing in the output file. The lines were:", Nlines, "\n")
        
    D=[]
    for elem in MyMapAux:
        D.append(elem[2])   # list holding only the "tt+sc" location values
    
    E=set(D)                # remove duplicates by set method
    
    if len(D)==len(E):
        print("# All mapping locations used once.")
    else:
        print("# * Warning! There are duplicate mappings: some locations used more than once! MGT and tt+sc:")
        F=[]
        for v in D:
            F.append(D.count(v))
        for k, v in enumerate(F):
            if v>1:
                print(MyMapAux[k][0], D[k])

    MyMap.sort(key=OutC.takeFirstEl)    #sort it. (always in a separate statement)
    
    for i in range(64):
        my1=[]
        for t in MyMap:              # t stands for tuple
            if t[0]==i:
                my1.append(t)  
                
        #print(len(my1))
        if len(my1)<20:
            print("# * Warning! Some word mapping missing from MGT", i)
    
        my1.sort(key=OutC.takeSecondEl)
    
        for v in my1:
            fout.write(str(v[0])+"\t"+str(v[1])+"\t")
            if 'x' in v[2]:
                fout.write(v[2]+"\n")
            else:
                for k in range(len(v[2])):
                    fout.write(v[2][k]+"\t")
                fout.write("\n")
            
    fout.close()
    fin.close()
    
    print("\n# Finish.")

if __name__=="__main__":
    
    WorkD=os.getcwd()

    OK_flag=0

    for i in range(8):    # I imply a maximum depth of 8 levels. Reasonable limit!
        os.chdir('..')
        MyD=os.getcwd()
        if MyD[-12:]=="eFEXFirmware":
            print("# Starting... Path found")
            OK_flag=1
            break

    if OK_flag==0:
        sys.exit("# ** ERROR!! eFEXFirmware path not found!!! exiting")

    filePath=MyD+"/VivadoProject/process_fpga.1/process_fpga.1.sim/data_mapping_sim/output_file.txt"
    
    os.chdir(WorkD)        # back where we were

    Run(sys.argv[1], filePath)

