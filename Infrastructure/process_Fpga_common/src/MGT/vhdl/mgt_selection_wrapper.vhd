library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
library UNISIM;
use UNISIM.VCOMPONENTS.all;

--***********************************Entity Declaration************************


entity mgt_selection_wrapper is
  generic
    (
      EXAMPLE_SIM_GTRESET_SPEEDUP : string    := "TRUE";  -- simulation setting for GT SecureIP model
      STABLE_CLOCK_PERIOD         : integer   := 16;
      ENABLED                     : std_logic := '1'
      );
  
  port
    (
      clk280           : in   std_logic;    
      SOFT_RESET_TX_IN : in  std_logic;
      SOFT_RESET_RX_IN : in  std_logic;
      RXN_IN           : in  std_logic_vector(3 downto 0);
      RXP_IN           : in  std_logic_vector(3 downto 0);
      TXN_OUT          : out std_logic_vector(3 downto 0);
      TXP_OUT          : out std_logic_vector(3 downto 0);

      Q0_CLK0_GTREFCLK_PAD_N_IN : in std_logic;  --
      Q0_CLK0_GTREFCLK_PAD_P_IN : in std_logic;  --

      GT0_TX_FSM_RESET_DONE_OUT : out std_logic;
      GT0_RX_FSM_RESET_DONE_OUT : out std_logic;
      GT0_DATA_VALID_IN         : in  std_logic;
      GT1_TX_FSM_RESET_DONE_OUT : out std_logic;
      GT1_RX_FSM_RESET_DONE_OUT : out std_logic;
      GT1_DATA_VALID_IN         : in  std_logic;
      GT2_TX_FSM_RESET_DONE_OUT : out std_logic;
      GT2_RX_FSM_RESET_DONE_OUT : out std_logic;
      GT2_DATA_VALID_IN         : in  std_logic;
      GT3_TX_FSM_RESET_DONE_OUT : out std_logic;
      GT3_RX_FSM_RESET_DONE_OUT : out std_logic;
      GT3_DATA_VALID_IN         : in  std_logic;

      GT0_TXUSRCLK_OUT : out std_logic;
      GT0_RXUSRCLK_OUT : out std_logic;

      GT1_TXUSRCLK_OUT : out std_logic;
      GT1_RXUSRCLK_OUT : out std_logic;

      GT2_TXUSRCLK_OUT : out std_logic;
      GT2_RXUSRCLK_OUT : out std_logic;

      GT3_TXUSRCLK_OUT : out std_logic;
      GT3_RXUSRCLK_OUT : out std_logic;

      --_________________________________________________________________________
      --GT0  (X0Y0)
      --____________________________CHANNEL PORTS________________________________
      ------------------------------- Loopback Ports -----------------------------
      gt0_loopback_in         : in  std_logic_vector(2 downto 0);
      ------------------------------ Power-Down Ports ---------------------------- 
      gt0_rxpd_in             : in  std_logic_vector(1 downto 0);
      gt0_txpd_in             : in  std_logic_vector(1 downto 0);
      ------------------ Receive Ports - FPGA RX interface Ports -----------------
      gt0_rxdata_out          : out std_logic_vector(31 downto 0);
      ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
      gt0_rxdisperr_out       : out std_logic_vector(3 downto 0);
      gt0_rxnotintable_out    : out std_logic_vector(3 downto 0);
      -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
      gt0_rxbyteisaligned_out : out std_logic;
      gt0_rxbyterealign_out   : out std_logic;
      gt0_rxcommadet_out      : out std_logic;
      ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
      gt0_rxchariscomma_out   : out std_logic_vector(3 downto 0);
      gt0_rxcharisk_out       : out std_logic_vector(3 downto 0);
      -------------- Receive Ports -RX Initialization and Reset Ports ------------
      gt0_rxresetdone_out     : out std_logic;

      ------------------ Transmit Ports - TX Data Path interface -----------------
      gt0_txdata_in       : in  std_logic_vector(31 downto 0);
      ------------- Transmit Ports - TX Initialization and Reset Ports -----------
      gt0_txresetdone_out : out std_logic;
      ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
      gt0_txcharisk_in    : in  std_logic_vector(3 downto 0);
      ---------------------- Transmit Ports - TX Buffer Ports --------------------
      gt0_txbufstatus_out : out std_logic_vector(1 downto 0);

      --GT1  (X0Y1)
      --____________________________CHANNEL PORTS________________________________
      ------------------------------- Loopback Ports -----------------------------
      gt1_loopback_in         : in  std_logic_vector(2 downto 0);
      ------------------------------ Power-Down Ports ---------------------------- 
      gt1_rxpd_in             : in  std_logic_vector(1 downto 0);
      gt1_txpd_in             : in  std_logic_vector(1 downto 0);
      --------------------- RX Initialization and Reset Ports --------------------
      gt1_rxdata_out          : out std_logic_vector(31 downto 0);
      ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
      gt1_rxdisperr_out       : out std_logic_vector(3 downto 0);
      gt1_rxnotintable_out    : out std_logic_vector(3 downto 0);
      -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
      gt1_rxbyteisaligned_out : out std_logic;
      gt1_rxbyterealign_out   : out std_logic;
      gt1_rxcommadet_out      : out std_logic;
      ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
      gt1_rxchariscomma_out   : out std_logic_vector(3 downto 0);
      gt1_rxcharisk_out       : out std_logic_vector(3 downto 0);
      -------------- Receive Ports -RX Initialization and Reset Ports ------------
      gt1_rxresetdone_out     : out std_logic;
      ------------------ Transmit Ports - TX Data Path interface -----------------
      gt1_txdata_in           : in  std_logic_vector(31 downto 0);
      ------------- Transmit Ports - TX Initialization and Reset Ports -----------
      gt1_txresetdone_out     : out std_logic;
      ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
      gt1_txcharisk_in        : in  std_logic_vector(3 downto 0);
      ---------------------- Transmit Ports - TX Buffer Ports --------------------
      gt1_txbufstatus_out     : out std_logic_vector(1 downto 0);

      --GT2  (X0Y2)
      --____________________________CHANNEL PORTS________________________________
      ------------------------------- Loopback Ports -----------------------------
      gt2_loopback_in         : in  std_logic_vector(2 downto 0);
      ------------------------------ Power-Down Ports ---------------------------- 
      gt2_rxpd_in             : in  std_logic_vector(1 downto 0);
      gt2_txpd_in             : in  std_logic_vector(1 downto 0);
      ------------------ Receive Ports - FPGA RX interface Ports -----------------
      gt2_rxdata_out          : out std_logic_vector(31 downto 0);
      ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
      gt2_rxdisperr_out       : out std_logic_vector(3 downto 0);
      gt2_rxnotintable_out    : out std_logic_vector(3 downto 0);
      -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
      gt2_rxbyteisaligned_out : out std_logic;
      gt2_rxbyterealign_out   : out std_logic;
      gt2_rxcommadet_out      : out std_logic;
      ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
      gt2_rxchariscomma_out   : out std_logic_vector(3 downto 0);
      gt2_rxcharisk_out       : out std_logic_vector(3 downto 0);
      -------------- Receive Ports -RX Initialization and Reset Ports ------------
      gt2_rxresetdone_out     : out std_logic;
      ------------------ Transmit Ports - TX Data Path interface -----------------
      gt2_txdata_in           : in  std_logic_vector(31 downto 0);
      ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
      gt2_txresetdone_out     : out std_logic;
      ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
      gt2_txcharisk_in        : in  std_logic_vector(3 downto 0);
      ---------------------- Transmit Ports - TX Buffer Ports --------------------
      gt2_txbufstatus_out     : out std_logic_vector(1 downto 0);

      --GT3  (X0Y3)
      --____________________________CHANNEL PORTS________________________________
      ------------------------------- Loopback Ports -----------------------------
      gt3_loopback_in         : in  std_logic_vector(2 downto 0);
      ------------------------------ Power-Down Ports ---------------------------- 
      gt3_rxpd_in             : in  std_logic_vector(1 downto 0);
      gt3_txpd_in             : in  std_logic_vector(1 downto 0);
      ------------------ Receive Ports - FPGA RX interface Ports -----------------
      gt3_rxdata_out          : out std_logic_vector(31 downto 0);
      ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
      gt3_rxdisperr_out       : out std_logic_vector(3 downto 0);
      gt3_rxnotintable_out    : out std_logic_vector(3 downto 0);
      -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
      gt3_rxbyteisaligned_out : out std_logic;
      gt3_rxbyterealign_out   : out std_logic;
      gt3_rxcommadet_out      : out std_logic;
      ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
      gt3_rxchariscomma_out   : out std_logic_vector(3 downto 0);
      gt3_rxcharisk_out       : out std_logic_vector(3 downto 0);
      -------------- Receive Ports -RX Initialization and Reset Ports ------------
      gt3_rxresetdone_out     : out std_logic;
      ------------------ Transmit Ports - TX Data Path interface -----------------
      gt3_txdata_in           : in  std_logic_vector(31 downto 0);
      ------------- Transmit Ports - TX Initialization and Reset Ports -----------
      gt3_txresetdone_out     : out std_logic;
      ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
      gt3_txcharisk_in        : in  std_logic_vector(3 downto 0);
      ---------------------- Transmit Ports - TX Buffer Ports --------------------
      gt3_txbufstatus_out     : out std_logic_vector(1 downto 0);

      --____________________________COMMON PORTS________________________________
      GT0_QPLLLOCK_OUT       : out std_logic;
      GT0_QPLLREFCLKLOST_OUT : out std_logic;
      sysclk_in              : in  std_logic

      );


end mgt_selection_wrapper;


architecture RTL of mgt_selection_wrapper is


--  attribute DowngradeIPIdentifiedWarnings        : string;
--  attribute DowngradeIPIdentifiedWarnings of RTL : architecture is "yes";

--  attribute CORE_GENERATION_INFO        : string;
--  attribute CORE_GENERATION_INFO of RTL : architecture is "min_latency_1_quad_rx_tx,gtwizard_v3_6_5,{protocol_file=Start_from_scratch}";

----**************************Component Declarations*****************************



--  signal tied_to_ground_i     : std_logic;
--  signal tied_to_ground_vec_i : std_logic_vector(63 downto 0);
--  signal tied_to_vcc_i        : std_logic;
--  signal tied_to_vcc_vec_i    : std_logic_vector(7 downto 0);

--**************************** Main Body of Code *******************************

begin

--  --  Static signal Assigments
--  tied_to_ground_i     <= '0';
--  tied_to_ground_vec_i <= x"0000000000000000";
--  tied_to_vcc_i        <= '1';
--  tied_to_vcc_vec_i    <= "11111111";


  ----------------------------- The GT Wrapper -----------------------------

  -- Use the instantiation template in the example directory to add the GT wrapper to your design.
  -- In this example, the wrapper is wired up for basic operation with a frame generator and frame 
  -- checker. The GTs will reset, then attempt to align and transmit data. If channel bonding is 
  -- enabled, bonding should occur after alignment.

  QUAD_ENABLED : if ENABLED = '1' generate
    mgt: entity work.min_latency_1quad_11g2_RxTX_wrapper
      generic map (
        EXAMPLE_SIM_GTRESET_SPEEDUP => EXAMPLE_SIM_GTRESET_SPEEDUP,
        STABLE_CLOCK_PERIOD         => STABLE_CLOCK_PERIOD)
      port map (
        clk280                    => clk280,
        SOFT_RESET_TX_IN          => SOFT_RESET_TX_IN,
        SOFT_RESET_RX_IN          => SOFT_RESET_RX_IN,
        RXN_IN                    => RXN_IN,
        RXP_IN                    => RXP_IN,
        TXN_OUT                   => TXN_OUT,
        TXP_OUT                   => TXP_OUT,
        Q0_CLK0_GTREFCLK_PAD_N_IN => Q0_CLK0_GTREFCLK_PAD_N_IN,
        Q0_CLK0_GTREFCLK_PAD_P_IN => Q0_CLK0_GTREFCLK_PAD_P_IN,
        GT0_TX_FSM_RESET_DONE_OUT => GT0_TX_FSM_RESET_DONE_OUT,
        GT0_RX_FSM_RESET_DONE_OUT => GT0_RX_FSM_RESET_DONE_OUT,
        GT0_DATA_VALID_IN         => GT0_DATA_VALID_IN,
        GT1_TX_FSM_RESET_DONE_OUT => GT1_TX_FSM_RESET_DONE_OUT,
        GT1_RX_FSM_RESET_DONE_OUT => GT1_RX_FSM_RESET_DONE_OUT,
        GT1_DATA_VALID_IN         => GT1_DATA_VALID_IN,
        GT2_TX_FSM_RESET_DONE_OUT => GT2_TX_FSM_RESET_DONE_OUT,
        GT2_RX_FSM_RESET_DONE_OUT => GT2_RX_FSM_RESET_DONE_OUT,
        GT2_DATA_VALID_IN         => GT2_DATA_VALID_IN,
        GT3_TX_FSM_RESET_DONE_OUT => GT3_TX_FSM_RESET_DONE_OUT,
        GT3_RX_FSM_RESET_DONE_OUT => GT3_RX_FSM_RESET_DONE_OUT,
        GT3_DATA_VALID_IN         => GT3_DATA_VALID_IN,
        GT0_TXUSRCLK_OUT          => GT0_TXUSRCLK_OUT,
        GT0_RXUSRCLK_OUT          => GT0_RXUSRCLK_OUT,
        GT1_TXUSRCLK_OUT          => GT1_TXUSRCLK_OUT,
        GT1_RXUSRCLK_OUT          => GT1_RXUSRCLK_OUT,
        GT2_TXUSRCLK_OUT          => GT2_TXUSRCLK_OUT,
        GT2_RXUSRCLK_OUT          => GT2_RXUSRCLK_OUT,
        GT3_TXUSRCLK_OUT          => GT3_TXUSRCLK_OUT,
        GT3_RXUSRCLK_OUT          => GT3_RXUSRCLK_OUT,
        gt0_loopback_in           => gt0_loopback_in,
        gt0_rxpd_in               => gt0_rxpd_in,
        gt0_txpd_in               => gt0_txpd_in,
        gt0_rxdata_out            => gt0_rxdata_out,
        gt0_rxdisperr_out         => gt0_rxdisperr_out,
        gt0_rxnotintable_out      => gt0_rxnotintable_out,
        gt0_rxbyteisaligned_out   => gt0_rxbyteisaligned_out,
        gt0_rxbyterealign_out     => gt0_rxbyterealign_out,
        gt0_rxcommadet_out        => gt0_rxcommadet_out,
        gt0_rxchariscomma_out     => gt0_rxchariscomma_out,
        gt0_rxcharisk_out         => gt0_rxcharisk_out,
        gt0_rxresetdone_out       => gt0_rxresetdone_out,
        gt0_txdata_in             => gt0_txdata_in,
        gt0_txresetdone_out       => gt0_txresetdone_out,
        gt0_txcharisk_in          => gt0_txcharisk_in,
        gt0_txbufstatus_out       => gt0_txbufstatus_out,
        gt1_loopback_in           => gt1_loopback_in,
        gt1_rxpd_in               => gt1_rxpd_in,
        gt1_txpd_in               => gt1_txpd_in,
        gt1_rxdata_out            => gt1_rxdata_out,
        gt1_rxdisperr_out         => gt1_rxdisperr_out,
        gt1_rxnotintable_out      => gt1_rxnotintable_out,
        gt1_rxbyteisaligned_out   => gt1_rxbyteisaligned_out,
        gt1_rxbyterealign_out     => gt1_rxbyterealign_out,
        gt1_rxcommadet_out        => gt1_rxcommadet_out,
        gt1_rxchariscomma_out     => gt1_rxchariscomma_out,
        gt1_rxcharisk_out         => gt1_rxcharisk_out,
        gt1_rxresetdone_out       => gt1_rxresetdone_out,
        gt1_txdata_in             => gt1_txdata_in,
        gt1_txresetdone_out       => gt1_txresetdone_out,
        gt1_txcharisk_in          => gt1_txcharisk_in,
        gt1_txbufstatus_out       => gt1_txbufstatus_out,
        gt2_loopback_in           => gt2_loopback_in,
        gt2_rxpd_in               => gt2_rxpd_in,
        gt2_txpd_in               => gt2_txpd_in,
        gt2_rxdata_out            => gt2_rxdata_out,
        gt2_rxdisperr_out         => gt2_rxdisperr_out,
        gt2_rxnotintable_out      => gt2_rxnotintable_out,
        gt2_rxbyteisaligned_out   => gt2_rxbyteisaligned_out,
        gt2_rxbyterealign_out     => gt2_rxbyterealign_out,
        gt2_rxcommadet_out        => gt2_rxcommadet_out,
        gt2_rxchariscomma_out     => gt2_rxchariscomma_out,
        gt2_rxcharisk_out         => gt2_rxcharisk_out,
        gt2_rxresetdone_out       => gt2_rxresetdone_out,
        gt2_txdata_in             => gt2_txdata_in,
        gt2_txresetdone_out       => gt2_txresetdone_out,
        gt2_txcharisk_in          => gt2_txcharisk_in,
        gt2_txbufstatus_out       => gt2_txbufstatus_out,
        gt3_loopback_in           => gt3_loopback_in,
        gt3_rxpd_in               => gt3_rxpd_in,
        gt3_txpd_in               => gt3_txpd_in,
        gt3_rxdata_out            => gt3_rxdata_out,
        gt3_rxdisperr_out         => gt3_rxdisperr_out,
        gt3_rxnotintable_out      => gt3_rxnotintable_out,
        gt3_rxbyteisaligned_out   => gt3_rxbyteisaligned_out,
        gt3_rxbyterealign_out     => gt3_rxbyterealign_out,
        gt3_rxcommadet_out        => gt3_rxcommadet_out,
        gt3_rxchariscomma_out     => gt3_rxchariscomma_out,
        gt3_rxcharisk_out         => gt3_rxcharisk_out,
        gt3_rxresetdone_out       => gt3_rxresetdone_out,
        gt3_txdata_in             => gt3_txdata_in,
        gt3_txresetdone_out       => gt3_txresetdone_out,
        gt3_txcharisk_in          => gt3_txcharisk_in,
        gt3_txbufstatus_out       => gt3_txbufstatus_out,
        GT0_QPLLLOCK_OUT          => GT0_QPLLLOCK_OUT,
        GT0_QPLLREFCLKLOST_OUT    => GT0_QPLLREFCLKLOST_OUT,
        sysclk_in                 => sysclk_in);
 end generate;

  QUAD_DISABLED : if ENABLED = '0' generate
    TXN_OUT                   <= (others => '0');
    TXP_OUT                   <= (others => '0');
    GT0_TX_FSM_RESET_DONE_OUT <= '0';
    GT0_RX_FSM_RESET_DONE_OUT <= '0';
    GT1_TX_FSM_RESET_DONE_OUT <= '0';
    GT1_RX_FSM_RESET_DONE_OUT <= '0';
    GT2_TX_FSM_RESET_DONE_OUT <= '0';
    GT2_RX_FSM_RESET_DONE_OUT <= '0';
    GT3_TX_FSM_RESET_DONE_OUT <= '0';
    GT3_RX_FSM_RESET_DONE_OUT <= '0';
    GT0_TXUSRCLK_OUT          <= '0';
    GT0_RXUSRCLK_OUT          <= '0';
    GT1_TXUSRCLK_OUT          <= '0';
    GT1_RXUSRCLK_OUT          <= '0';
    GT2_TXUSRCLK_OUT          <= '0';
    GT2_RXUSRCLK_OUT          <= '0';
    GT3_TXUSRCLK_OUT          <= '0';
    GT3_RXUSRCLK_OUT          <= '0';
    gt0_rxdata_out            <= (others => '0');
    gt0_rxdisperr_out         <= (others => '0');
    gt0_rxnotintable_out      <= (others => '0');
    gt0_rxbyteisaligned_out   <= '0';
    gt0_rxbyterealign_out     <= '0';
    gt0_rxcommadet_out        <= '0';
    gt0_rxchariscomma_out     <= (others => '0');
    gt0_rxcharisk_out         <= (others => '0');
    gt0_rxresetdone_out       <= '0';
    gt0_txresetdone_out       <= '0';
    gt0_txbufstatus_out       <= (others => '0');
    gt1_rxdata_out            <= (others => '0');
    gt1_rxdisperr_out         <= (others => '0');
    gt1_rxnotintable_out      <= (others => '0');
    gt1_rxbyteisaligned_out   <= '0';
    gt1_rxbyterealign_out     <= '0';
    gt1_rxcommadet_out        <= '0';
    gt1_rxchariscomma_out     <= (others => '0');
    gt1_rxcharisk_out         <= (others => '0');
    gt1_rxresetdone_out       <= '0';
    gt1_txresetdone_out       <= '0';
    gt1_txbufstatus_out       <= (others => '0');

    gt2_rxdata_out          <= (others => '0');
    gt2_rxdisperr_out       <= (others => '0');
    gt2_rxnotintable_out    <= (others => '0');
    gt2_rxbyteisaligned_out <= '0';
    gt2_rxbyterealign_out   <= '0';
    gt2_rxcommadet_out      <= '0';
    gt2_rxchariscomma_out   <= (others => '0');
    gt2_rxcharisk_out       <= (others => '0');
    gt2_rxresetdone_out     <= '0';
    gt2_txresetdone_out     <= '0';
    gt2_txbufstatus_out     <= (others => '0');
    gt3_rxdata_out          <= (others => '0');
    gt3_rxdisperr_out       <= (others => '0');
    gt3_rxnotintable_out    <= (others => '0');
    gt3_rxbyteisaligned_out <= '0';
    gt3_rxbyterealign_out   <= '0';
    gt3_rxcommadet_out      <= '0';
    gt3_rxchariscomma_out   <= (others => '0');
    gt3_rxcharisk_out       <= (others => '0');
    gt3_rxresetdone_out     <= '0';
    gt3_txresetdone_out     <= '0';
    gt3_txbufstatus_out     <= (others => '0');
    GT0_QPLLLOCK_OUT        <= '0';
    GT0_QPLLREFCLKLOST_OUT  <= '0';
  end generate;
end RTL;

