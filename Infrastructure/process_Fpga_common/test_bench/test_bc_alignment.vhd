----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/12/2018 03:42:06 PM
-- Design Name: 
-- Module Name: test_bc_alignment - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library infrastructure_lib;
use infrastructure_lib.all;

use IEEE.NUMERIC_STD.ALL;


entity test_bc_alignment is
--  Port ( );
end test_bc_alignment;

architecture Behavioral of test_bc_alignment is

signal clk,reset,start :std_logic;
signal bcn_ch0,bcn_ch1,bcn_ch2,bcn_ch3:std_logic_vector(6 downto 0);
signal cntr_ch0,cntr_ch1,cntr_ch2,cntr_ch3:std_logic_vector(4 downto 0);
signal aeqb_ch0,aeqb_ch1,aeqb_ch2,aeqb_ch3,ref_orbit:std_logic;


begin






test_bench: entity work.orbit_testbench 
        port map(
              clk        =>   clk    ,
              bcn_ch0    =>   bcn_ch0,
              bcn_ch1    =>   bcn_ch1,
              bcn_ch2    =>   bcn_ch2,
              bcn_ch3    =>   bcn_ch3,
              reset      =>   reset  ,
              ref_orbit  =>  ref_orbit,
              start      =>   start  
              );
            
            
top_bc_align: entity infrastructure_lib.quad_bc_alignment
                 Port map(         
                        reset        =>  reset,
                        clk          =>  clk,
                        start        =>  start,
                        ref_orbit    => ref_orbit,
                        bcn_0        =>  bcn_ch0,
                        bcn_1        =>  bcn_ch1,
                        bcn_2        =>  bcn_ch2,
                        bcn_3        =>  bcn_ch3,
                        aeqb_ch0     =>  aeqb_ch0,
                        aeqb_ch1     =>  aeqb_ch1,
                        aeqb_ch2     =>  aeqb_ch2,
                        aeqb_ch3     =>  aeqb_ch3,
                        cntr_ch0     =>  cntr_ch0,
                        cntr_ch1     =>  cntr_ch1,
                        cntr_ch2     =>  cntr_ch2,
                        cntr_ch3     =>  cntr_ch3
                        );                 
               
          


end Behavioral;
