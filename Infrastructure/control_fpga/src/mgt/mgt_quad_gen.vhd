 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library xil_defaultlib;
use work.mgt_type.all;


entity MGT_quad_gen is
generic( num_quad_tx_rx: natural := 1
               ); 
  Port ( 
  --refclk160                   : IN    std_logic;
  TTC_CLK                     : IN    std_logic;
  MGT_CLK_GTREFCLK_PAD_N_IN   : IN    std_logic_vector(num_quad_tx_rx-1 downto 0);                 
  MGT_CLK_GTREFCLK_PAD_P_IN   : IN    std_logic_vector(num_quad_tx_rx-1 downto 0); 
  
  mgt_TXUSRCLK_OUT            : out   std_logic_vector(4*num_quad_tx_rx -1 downto 0);
  mgt_RXUSRCLK_OUT            : out   std_logic_vector(4*num_quad_tx_rx -1 downto 0);  
  
  mgt_SOFT_RESET_TX_IN        : in    std_logic_vector(num_quad_tx_rx -1 downto 0);
  mgt_SOFT_RESET_RX_IN        : in    std_logic_vector(num_quad_tx_rx -1 downto 0); 
 -- data                    
  RXN_IN                      : IN     mgt_rx_array(num_quad_tx_rx-1 downto 0);
  RXP_IN                      : IN     mgt_rx_array(num_quad_tx_rx-1 downto 0);
  TXN_IN                      : OUT    mgt_tx_array(num_quad_tx_rx-1 downto 0);
  TXP_IN                      : OUT    mgt_tx_array(num_quad_tx_rx-1 downto 0);
  
  rxdata_quad_array           : out    mgt_rxdata_array  (num_quad_tx_rx -1 downto 0); 
  txdata_quad_array           : in     mgt_txdata_array  (num_quad_tx_rx -1 downto 0); 
  -- status and monitoring  
  mgt_TX_FSM_RESET_DONE       : out    std_logic_vector(4*num_quad_tx_rx -1 downto 0); 
  mgt_RX_FSM_RESET_DONE       : out    std_logic_vector(4*num_quad_tx_rx -1 downto 0);
  rxbyteisaligned_quad_array  : out    mgt_rxbyteisaligned_array(num_quad_tx_rx -1 downto 0);  
  rxresetdone_quad_array      : out    mgt_rxresetdone_array    (num_quad_tx_rx -1 downto 0);
  txresetdone_quad_array      : out    mgt_txresetdone_array    (num_quad_tx_rx -1 downto 0); 
   
  gt_rxpd_array               : in     mgt_rxpd_array  (num_quad_tx_rx -1 downto 0); 
  gt_txpd_array               : in     mgt_txpd_array  (num_quad_tx_rx -1 downto 0); 
  loopback_quad_array         : in     mgt_loopback_array      (num_quad_tx_rx-1  downto 0); 
  rxchariscomma_quad_array    : out    mgt_rxchariskcomm_array (num_quad_tx_rx -1 downto 0);
  rxcharisk_quad_array        : out    mgt_rxcharisk_array     (num_quad_tx_rx -1 downto 0);
  txcharisk_quad_array        : in     mgt_txcharisk_array     (num_quad_tx_rx -1 downto 0);  
  txbufstatus_quad_array      : out    mgt_txbufstatus_array   (num_quad_tx_rx -1 downto 0);
  rxbyterealign_quad_array    : out    mgt_rxbyterealign_array (num_quad_tx_rx -1 downto 0);                  
  rxcommadet_quad_array       : out    mgt_rxcommadet_array    (num_quad_tx_rx -1 downto 0);                   
  rxdisperr_quad_array        : out    mgt_rxdisperr_array     (num_quad_tx_rx -1 downto 0);                                               
  rxnotintable_quad_array     : out    mgt_rxnotintable_array  (num_quad_tx_rx -1 downto 0);   
  --mgt_QPLLREFCLKLOST_OUT      : out   std_logic_vector(num_quad_tx_rx-1 downto 0) ;
  --mgt_QPLLLOCK_OUT            : out   std_logic_vector      (num_quad_tx_rx -1 downto 0)
  gt0_cpllfbclklost_out      :  out   std_logic_vector(num_quad_tx_rx -1 downto 0) ; 
  gt0_cplllock_out           :  out   std_logic_vector(num_quad_tx_rx -1 downto 0) ;
  gt1_cpllfbclklost_out      :  out   std_logic_vector(num_quad_tx_rx -1 downto 0) ;
  gt1_cplllock_out           :  out   std_logic_vector(num_quad_tx_rx -1 downto 0) ;
  gt2_cpllfbclklost_out      :  out   std_logic_vector(num_quad_tx_rx -1 downto 0) ;
  gt2_cplllock_out           :  out   std_logic_vector(num_quad_tx_rx -1 downto 0) ;
  gt3_cpllfbclklost_out      :  out   std_logic_vector(num_quad_tx_rx -1 downto 0) ;
  gt3_cplllock_out           :  out   std_logic_vector(num_quad_tx_rx -1 downto 0) 
  
     );
     
end MGT_quad_gen;

architecture Behavioral of MGT_quad_gen is


--- mgt signal declarations
 
signal RXN_IN_tx_rx,RXP_IN_tx_rx  : mgt_rx_array (num_quad_tx_rx -1 downto 0);
signal TXN_IN_tx_rx,TXP_IN_tx_rx  : mgt_tx_array (num_quad_tx_rx -1 downto 0);
  

begin   
 
  
  
  
  


MGT_GEN: for i in 0 to num_quad_tx_rx-1

generate

mgt_quad_Rx_Tx:  entity work.mgt_tx_rx_6g4_wrapper

  -- This part will generate n quads, n*4 = 4n mgts. 
                                    
   Port map ( 
        
       SOFT_RESET_TX_IN                        =>    mgt_SOFT_RESET_TX_IN(i),
       SOFT_RESET_RX_IN                        =>    mgt_SOFT_RESET_RX_IN(i),
       RXN_IN                                  =>    RXN_IN(i).RXN_IN ,       
       RXP_IN                                  =>    RXP_IN(i).RXP_IN,        
       TXN_OUT                                 =>    TXN_IN(i).TXN_OUT,       
       TXP_OUT                                 =>    TXP_IN(i).TXP_OUT,    
       Q0_CLK0_GTREFCLK_PAD_N_IN               =>    MGT_CLK_GTREFCLK_PAD_N_IN(i) ,
       Q0_CLK0_GTREFCLK_PAD_P_IN               =>    MGT_CLK_GTREFCLK_PAD_P_IN(i) , 
         
       GT0_TX_FSM_RESET_DONE_OUT              => mgt_TX_FSM_RESET_DONE(i+3*i),
       GT0_RX_FSM_RESET_DONE_OUT              => mgt_RX_FSM_RESET_DONE(i+3*i),              
       GT1_TX_FSM_RESET_DONE_OUT              => mgt_TX_FSM_RESET_DONE(i+1+3*i),
       GT1_RX_FSM_RESET_DONE_OUT              => mgt_RX_FSM_RESET_DONE(i+1+3*i),              
       GT2_TX_FSM_RESET_DONE_OUT              => mgt_TX_FSM_RESET_DONE(i+2+3*i),
       GT2_RX_FSM_RESET_DONE_OUT              => mgt_RX_FSM_RESET_DONE(i+2+3*i),  
       GT3_TX_FSM_RESET_DONE_OUT              => mgt_TX_FSM_RESET_DONE(i+3+3*i),
       GT3_RX_FSM_RESET_DONE_OUT              => mgt_RX_FSM_RESET_DONE(i+3+3*i),
             
       GT0_TXUSRCLK_OUT                       => mgt_TXUSRCLK_OUT(i+3*i),   
       GT0_RXUSRCLK_OUT                       => mgt_RXUSRCLK_OUT(i+3*i), 
       GT1_TXUSRCLK_OUT                       => mgt_TXUSRCLK_OUT(i+1+3*i),         
       GT1_RXUSRCLK_OUT                       => mgt_RXUSRCLK_OUT(i+1+3*i),   
       GT2_TXUSRCLK_OUT                       => mgt_TXUSRCLK_OUT(i+2+3*i) ,  
       GT2_RXUSRCLK_OUT                       => mgt_RXUSRCLK_OUT(i+2+3*i), 
       GT3_TXUSRCLK_OUT                       => mgt_TXUSRCLK_OUT(i+3+3*i) ,  
       GT3_RXUSRCLK_OUT                       => mgt_RXUSRCLK_OUT(i+3+3*i),   
                                               
       gt0_cpllfbclklost_out                 => gt0_cpllfbclklost_out(i)   ,
       gt0_cplllock_out                      => gt0_cplllock_out(i)        ,
       gt1_cpllfbclklost_out                 => gt1_cpllfbclklost_out(i)   ,
       gt1_cplllock_out                      => gt1_cplllock_out(i)        ,
       gt2_cpllfbclklost_out                 => gt2_cpllfbclklost_out(i)   ,
       gt2_cplllock_out                      => gt2_cplllock_out(i)        ,
       gt3_cpllfbclklost_out                 => gt3_cpllfbclklost_out(i)   ,
       gt3_cplllock_out                      => gt3_cplllock_out(i)        ,
   
       --_________________________________________________________________________
       --GT0  (X0Y0)
       --____________________________CHANNEL PORTS________________________________
       
       gt0_loopback_in                         => loopback_quad_array(i).gt0_loopback_in,
       gt0_rxpd_in                             => gt_rxpd_array(i).gt0_rxpd,
       gt0_txpd_in                             => gt_txpd_array(i).gt0_txpd,        
       gt0_rxdata_out                          => rxdata_quad_array (i).gt0_rxdata_out, 
       gt0_rxdisperr_out                       => rxdisperr_quad_array(i).gt0_rxdisperr,
       gt0_rxnotintable_out                    => rxnotintable_quad_array(i).gt0_rxnotintable,
       gt0_rxbyterealign_out                   => rxbyterealign_quad_array(i).gt0_rxbyterealign,
       gt0_rxcommadet_out                      => rxcommadet_quad_array(i).gt0_rxcommadet,   
       gt0_rxbyteisaligned_out                 => rxbyteisaligned_quad_array(i).gt0_rxbyteisaligned,            
       gt0_rxchariscomma_out                   => rxchariscomma_quad_array(i).gt0_rxchariscomma_out,  
       gt0_rxcharisk_out                       => rxcharisk_quad_array(i).gt0_rxcharisk_out,         
       gt0_rxresetdone_out                     => rxresetdone_quad_array(i).gt0_rxresetdone,        
       gt0_txdata_in                           => txdata_quad_array(i).gt0_txdata_in ,      
       gt0_txresetdone_out                     => txresetdone_quad_array(i).gt0_txresetdone,  
       gt0_txcharisk_in                        => txcharisk_quad_array (i).gt0_txcharisk,
       gt0_txbufstatus_out                     => txbufstatus_quad_array(i).gt0_txbufstatus,
   
       --GT1  (X0Y1)
       --____________________________CHANNEL PORTS________________________________
       gt1_loopback_in                         => loopback_quad_array(i).gt0_loopback_in,
       gt1_rxpd_in                             => gt_rxpd_array(i).gt1_rxpd,
       gt1_txpd_in                             => gt_txpd_array(i).gt1_txpd,
       gt1_rxdata_out                          => rxdata_quad_array (i).gt1_rxdata_out,
       gt1_rxdisperr_out                       => rxdisperr_quad_array(i).gt1_rxdisperr,     
       gt1_rxnotintable_out                    => rxnotintable_quad_array(i).gt1_rxnotintable,  
       gt1_rxbyterealign_out                   => rxbyterealign_quad_array(i).gt1_rxbyterealign, 
       gt1_rxcommadet_out                      => rxcommadet_quad_array(i).gt1_rxcommadet,  
       gt1_rxbyteisaligned_out                 => rxbyteisaligned_quad_array(i).gt1_rxbyteisaligned,        
       gt1_rxchariscomma_out                   => rxchariscomma_quad_array(i).gt1_rxchariscomma_out, 
       gt1_rxcharisk_out                       => rxcharisk_quad_array(i).gt1_rxcharisk_out,        
       gt1_rxresetdone_out                     => rxresetdone_quad_array(i).gt1_rxresetdone,        
       gt1_txdata_in                           => txdata_quad_array(i).gt1_txdata_in,      
       gt1_txresetdone_out                     => txresetdone_quad_array(i).gt1_txresetdone,     
       gt1_txcharisk_in                        => txcharisk_quad_array (i).gt1_txcharisk, 
       gt1_txbufstatus_out                     => txbufstatus_quad_array(i).gt1_txbufstatus,
       --GT2  (X0Y2)
       --____________________________CHANNEL PORTS________________________________
       gt2_loopback_in                         => loopback_quad_array(i).gt0_loopback_in, 
       gt2_rxpd_in                             => gt_rxpd_array(i).gt2_rxpd, 
       gt2_txpd_in                             => gt_txpd_array(i).gt2_txpd,           
       gt2_rxdata_out                          => rxdata_quad_array (i).gt2_rxdata_out,
       gt2_rxdisperr_out                       => rxdisperr_quad_array(i).gt2_rxdisperr,    
       gt2_rxnotintable_out                    => rxnotintable_quad_array(i).gt2_rxnotintable, 
       gt2_rxbyterealign_out                   => rxbyterealign_quad_array(i).gt2_rxbyterealign,
       gt2_rxcommadet_out                      => rxcommadet_quad_array(i).gt2_rxcommadet,            
       gt2_rxbyteisaligned_out                 => rxbyteisaligned_quad_array(i).gt2_rxbyteisaligned,     
       gt2_rxchariscomma_out                   => rxchariscomma_quad_array(i).gt2_rxchariscomma_out, 
       gt2_rxcharisk_out                       => rxcharisk_quad_array(i).gt2_rxcharisk_out,        
       gt2_rxresetdone_out                     => rxresetdone_quad_array(i).gt2_rxresetdone,          
       gt2_txdata_in                           => txdata_quad_array(i).gt2_txdata_in,     
       gt2_txresetdone_out                     => txresetdone_quad_array(i).gt2_txresetdone,    
       gt2_txcharisk_in                        => txcharisk_quad_array (i).gt2_txcharisk,
       gt2_txbufstatus_out                     => txbufstatus_quad_array(i).gt2_txbufstatus,
   
       --GT3  (X0Y3)
       --____________________________CHANNEL PORTS________________________________
       gt3_loopback_in                         => loopback_quad_array(i).gt0_loopback_in,
       gt3_rxpd_in                             => gt_rxpd_array(i).gt3_rxpd,
       gt3_txpd_in                             => gt_txpd_array(i).gt3_txpd,     
       gt3_rxdata_out                          => rxdata_quad_array (i).gt3_rxdata_out,
       gt3_rxdisperr_out                       => rxdisperr_quad_array(i).gt3_rxdisperr,    
       gt3_rxnotintable_out                    => rxnotintable_quad_array(i).gt3_rxnotintable, 
       gt3_rxbyterealign_out                   => rxbyterealign_quad_array(i).gt3_rxbyterealign,
       gt3_rxcommadet_out                      => rxcommadet_quad_array(i).gt3_rxcommadet,          
       gt3_rxbyteisaligned_out                 => rxbyteisaligned_quad_array(i).gt3_rxbyteisaligned,      
       gt3_rxchariscomma_out                   => rxchariscomma_quad_array(i).gt3_rxchariscomma_out, 
       gt3_rxcharisk_out                       => rxcharisk_quad_array(i).gt3_rxcharisk_out,        
       gt3_rxresetdone_out                     => rxresetdone_quad_array(i).gt3_rxresetdone,         
       gt3_txdata_in                           => txdata_quad_array(i).gt3_txdata_in,      
       gt3_txresetdone_out                     => txresetdone_quad_array(i).gt3_txresetdone,     
       gt3_txcharisk_in                        => txcharisk_quad_array (i).gt3_txcharisk, 
       gt3_txbufstatus_out                     => txbufstatus_quad_array(i).gt3_txbufstatus,
   
       --____________________________COMMON PORTS________________________________
       --GT0_QPLLLOCK_OUT                       => mgt_QPLLLOCK_OUT(i),--.GT0_QPLLLOCK_OUT, 
       --GT0_QPLLREFCLKLOST_OUT                 => mgt_QPLLREFCLKLOST_OUT(i),--.GT0_QPLLREFCLKLOST_OUT,
       sysclk_in                              => TTC_CLK
   
       );
    
  end generate MGT_GEN; 

end Behavioral;
