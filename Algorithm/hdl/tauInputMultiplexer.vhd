--! @file
--! @brief Input Data Multiplexer for Tau: addresses SuperCells to the correct sum area
--! @details
--! The selector of this mux is the seed of the cluster (IN_Seed)  plus the phi
--! asimmetry (IN_UpNotDown). According to these values, all the supercells
--! contained in the 9 TTs are addressed to the proper place.
--!
--! This mux is registerd and the latency is 2 clock cycles, to mach the seed
--! finder.
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library work;
use work.DataTypes.all;

--! @copydoc tauInputMultiplexer.vhd
entity tauInputMultiplexer is
  port (
    CLK : in std_logic;         --!200 MHz clock

    IN_Seed      : in std_logic_vector(1 downto 0);
    IN_UpNotDown : in std_logic;
    IN_Towers    : in TriggerTowers(8 downto 0);

    OUT_Energy_L1 : out DataWords(9 downto 0);
    OUT_Energy_L2 : out DataWords(9 downto 0);
    OUT_Energy_L0  : out DataWords(5 downto 0);
    OUT_Energy_L3  : out DataWords(5 downto 0);
    OUT_Energy_HAD : out DataWords(5 downto 0);

    OUT_JetCoreData : out DataWords(5 downto 0);
    OUT_JetEnvData  : out DataWords(11 downto 0)
    );
end tauInputMultiplexer;

--! @copydoc tauInputMultiplexer.vhd
architecture Behavioral of tauInputMultiplexer is
  signal Selector     : std_logic_vector(2 downto 0);
  signal ShiftTowers  : TriggerTowers(8 downto 0) := (others => ZERO_TRIGGER_TOWER);
  signal ShiftTowers2 : TriggerTowers(8 downto 0) := (others => ZERO_TRIGGER_TOWER);


  signal OutEnergy_L0  : DataWords(OUT_Energy_L0'range)  := (others => (others => '0'));
  signal OutEnergy_L1  : DataWords(OUT_Energy_L1'range)  := (others => (others => '0'));
  signal OutEnergy_L2  : DataWords(OUT_Energy_L2'range)  := (others => (others => '0'));
  signal OutEnergy_L3  : DataWords(OUT_Energy_L3'range)  := (others => (others => '0'));
  signal OutEnergy_HAD : DataWords(OUT_Energy_HAD'range) := (others => (others => '0'));

  signal OutJetCoreData : DataWords(OUT_JetCoreData'range) := (others => (others => '0'));
  signal OutJetEnvData  : DataWords(OUT_JetEnvData'range)  := (others => (others => '0'));

begin

  Selector <= IN_UpNotDown & IN_Seed;

  process (CLK)
  begin
    if rising_edge(CLK) then
      -- Pipeline (a delay of 2 is needed to mach the SeedFinder)
      ShiftTowers2 <= IN_Towers;
      ShiftTowers  <= ShiftTowers2;

      -- Multiplexer

      -------------------------------------------------------------------------------
      -- Energy L0, L3, HAD
      -------------------------------------------------------------------------------

      case IN_UpNotDown is
        when '1' =>             --up
          OutEnergy_L0 <= (2 => ShiftTowers(5).Layer0(0), 1 => ShiftTowers(4).Layer0(0), 0 => ShiftTowers(3).Layer0(0),
                           5 => ShiftTowers(8).Layer0(0), 4 => ShiftTowers(7).Layer0(0), 3 => ShiftTowers(6).Layer0(0));

          OutEnergy_L3 <= (2 => ShiftTowers(5).Layer3(0), 1 => ShiftTowers(4).Layer3(0), 0 => ShiftTowers(3).Layer3(0),
                           5 => ShiftTowers(8).Layer3(0), 4 => ShiftTowers(7).Layer3(0), 3 => ShiftTowers(6).Layer3(0));

          OutEnergy_HAD <= (2 => ShiftTowers(5).Hadron(0), 1 => ShiftTowers(4).Hadron(0), 0 => ShiftTowers(3).Hadron(0),
                            5 => ShiftTowers(8).Hadron(0), 4 => ShiftTowers(7).Hadron(0), 3 => ShiftTowers(6).Hadron(0));

        when others =>
          OutEnergy_L0 <= (2 => ShiftTowers(5).Layer0(0), 1 => ShiftTowers(4).Layer0(0), 0 => ShiftTowers(3).Layer0(0),
                           5 => ShiftTowers(2).Layer0(0), 4 => ShiftTowers(1).Layer0(0), 3 => ShiftTowers(0).Layer0(0));

          OutEnergy_L3 <= (2 => ShiftTowers(5).Layer3(0), 1 => ShiftTowers(4).Layer3(0), 0 => ShiftTowers(3).Layer3(0),
                           5 => ShiftTowers(2).Layer3(0), 4 => ShiftTowers(1).Layer3(0), 3 => ShiftTowers(0).Layer3(0));

          OutEnergy_HAD <= (2 => ShiftTowers(5).Hadron(0), 1 => ShiftTowers(4).Hadron(0), 0 => ShiftTowers(3).Hadron(0),
                            5 => ShiftTowers(2).Hadron(0), 4 => ShiftTowers(1).Hadron(0), 3 => ShiftTowers(0).Hadron(0));

      end case;

----------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Energy L1
----------------------------------------------------------------------------------------------------------------------------------------------------------------
      case Selector is
        when "100" =>           --up
          OutEnergy_L1 <= (2 downto 0 => ShiftTowers(4).Layer1(2 downto 0), 4 downto 3 => ShiftTowers(3).Layer1(3 downto 2),
                           7 downto 5 => ShiftTowers(7).Layer1(2 downto 0), 9 downto 8 => ShiftTowers(6).Layer1(3 downto 2));
        when "101" =>
          OutEnergy_L1 <= (3 downto 0 => ShiftTowers(4).Layer1, 4 => ShiftTowers(3).Layer1(3),
                           8 downto 5 => ShiftTowers(7).Layer1, 9 => ShiftTowers(6).Layer1(3));
        when "110" =>
          OutEnergy_L1 <= (3 downto 0 => ShiftTowers(4).Layer1, 4 => ShiftTowers(5).Layer1(0),
                           8 downto 5 => ShiftTowers(7).Layer1, 9 => ShiftTowers(8).Layer1(0));
        when "111" =>
          OutEnergy_L1 <= (2 downto 0 => ShiftTowers(4).Layer1(3 downto 1), 4 downto 3 => ShiftTowers(5).Layer1(1 downto 0),
                           7 downto 5 => ShiftTowers(7).Layer1(3 downto 1), 9 downto 8 => ShiftTowers(8).Layer1(1 downto 0));
        when "000" =>           --down
          OutEnergy_L1 <= (2 downto 0 => ShiftTowers(4).Layer1(2 downto 0), 4 downto 3 => ShiftTowers(3).Layer1(3 downto 2),
                           7 downto 5 => ShiftTowers(1).Layer1(2 downto 0), 9 downto 8 => ShiftTowers(0).Layer1(3 downto 2));
        when "001" =>
          OutEnergy_L1 <= (3 downto 0 => ShiftTowers(4).Layer1, 4 => ShiftTowers(3).Layer1(3),
                           8 downto 5 => ShiftTowers(1).Layer1, 9 => ShiftTowers(0).Layer1(3));
        when "010" =>
          OutEnergy_L1 <= (3 downto 0 => ShiftTowers(4).Layer1, 4 => ShiftTowers(5).Layer1(0),
                           8 downto 5 => ShiftTowers(1).Layer1, 9 => ShiftTowers(2).Layer1(0));
        when others =>          -- 011
          OutEnergy_L1 <= (2 downto 0 => ShiftTowers(4).Layer1(3 downto 1), 4 downto 3 => ShiftTowers(5).Layer1(1 downto 0),
                           7 downto 5 => ShiftTowers(1).Layer1(3 downto 1), 9 downto 8 => ShiftTowers(2).Layer1(1 downto 0));
      end case;

      -------------------------------------------------------------------------------
      -- Energy L2
      -------------------------------------------------------------------------------
      case Selector is
        when "100" =>           --up
          OutEnergy_L2 <= (2 downto 0 => ShiftTowers(4).Layer2(2 downto 0), 4 downto 3 => ShiftTowers(3).Layer2(3 downto 2),
                           7 downto 5 => ShiftTowers(7).Layer2(2 downto 0), 9 downto 8 => ShiftTowers(6).Layer2(3 downto 2));
        when "101" =>
          OutEnergy_L2 <= (3 downto 0 => ShiftTowers(4).Layer2, 4 => ShiftTowers(3).Layer2(3),
                           8 downto 5 => ShiftTowers(7).Layer2, 9 => ShiftTowers(6).Layer2(3));
        when "110" =>
          OutEnergy_L2 <= (3 downto 0 => ShiftTowers(4).Layer2, 4 => ShiftTowers(5).Layer2(0),
                           8 downto 5 => ShiftTowers(7).Layer2, 9 => ShiftTowers(8).Layer2(0));
        when "111" =>
          OutEnergy_L2 <= (2 downto 0 => ShiftTowers(4).Layer2(3 downto 1), 4 downto 3 => ShiftTowers(5).Layer2(1 downto 0),
                           7 downto 5 => ShiftTowers(7).Layer2(3 downto 1), 9 downto 8 => ShiftTowers(8).Layer2(1 downto 0));
        when "000" =>           --down
          OutEnergy_L2 <= (2 downto 0 => ShiftTowers(4).Layer2(2 downto 0), 4 downto 3 => ShiftTowers(3).Layer2(3 downto 2),
                           7 downto 5 => ShiftTowers(1).Layer2(2 downto 0), 9 downto 8 => ShiftTowers(0).Layer2(3 downto 2));
        when "001" =>
          OutEnergy_L2 <= (3 downto 0 => ShiftTowers(4).Layer2, 4 => ShiftTowers(3).Layer2(3),
                           8 downto 5 => ShiftTowers(1).Layer2, 9 => ShiftTowers(0).Layer2(3));
        when "010" =>
          OutEnergy_L2 <= (3 downto 0 => ShiftTowers(4).Layer2, 4 => ShiftTowers(5).Layer2(0),
                           8 downto 5 => ShiftTowers(1).Layer2, 9 => ShiftTowers(2).Layer2(0));
        when others =>          -- 011
          OutEnergy_L2 <= (2 downto 0 => ShiftTowers(4).Layer2(3 downto 1), 4 downto 3 => ShiftTowers(5).Layer2(1 downto 0),
                           7 downto 5 => ShiftTowers(1).Layer2(3 downto 1), 9 downto 8 => ShiftTowers(2).Layer2(1 downto 0));
      end case;



      -----------------------------------------------------------------------
      -- Jet Core Data
      -----------------------------------------------------------------------
      case Selector is
        -- Up
        when "100" =>           --up
          OutJetCoreData <= (0 => ShiftTowers(4).Layer2(1), 1 => ShiftTowers(4).Layer2(0), 2 => ShiftTowers(3).Layer2(3),
                             3 => ShiftTowers(7).Layer2(1), 4 => ShiftTowers(7).Layer2(0), 5 => ShiftTowers(6).Layer2(3));
        when "101" =>
          OutJetCoreData <= (0 => ShiftTowers(4).Layer2(2), 1 => ShiftTowers(4).Layer2(1), 2 => ShiftTowers(4).Layer2(0),
                             3 => ShiftTowers(7).Layer2(2), 4 => ShiftTowers(7).Layer2(1), 5 => ShiftTowers(6).Layer2(0));
        when "110" =>
          OutJetCoreData <= (0 => ShiftTowers(4).Layer2(3), 1 => ShiftTowers(4).Layer2(2), 2 => ShiftTowers(4).Layer2(1),
                             3 => ShiftTowers(7).Layer2(3), 4 => ShiftTowers(7).Layer2(2), 5 => ShiftTowers(7).Layer2(1));
        when "111" =>
          OutJetCoreData <= (0 => ShiftTowers(5).Layer2(0), 1 => ShiftTowers(4).Layer2(3), 2 => ShiftTowers(4).Layer2(2),
                             3 => ShiftTowers(8).Layer2(0), 4 => ShiftTowers(7).Layer2(3), 5 => ShiftTowers(7).Layer2(2));
        when "000" =>           --down
          OutJetCoreData <= (0 => ShiftTowers(4).Layer2(1), 1 => ShiftTowers(4).Layer2(0), 2 => ShiftTowers(3).Layer2(3),
                             3 => ShiftTowers(1).Layer2(1), 4 => ShiftTowers(1).Layer2(0), 5 => ShiftTowers(0).Layer2(3));
        when "001" =>
          OutJetCoreData <= (0 => ShiftTowers(4).Layer2(2), 1 => ShiftTowers(4).Layer2(1), 2 => ShiftTowers(4).Layer2(0),
                             3 => ShiftTowers(1).Layer2(2), 4 => ShiftTowers(1).Layer2(1), 5 => ShiftTowers(1).Layer2(0));
        when "010" =>
          OutJetCoreData <= (0 => ShiftTowers(4).Layer2(3), 1 => ShiftTowers(4).Layer2(2), 2 => ShiftTowers(4).Layer2(1),
                             3 => ShiftTowers(1).Layer2(3), 4 => ShiftTowers(1).Layer2(2), 5 => ShiftTowers(1).Layer2(1));
        when others =>          -- 011
          OutJetCoreData <= (0 => ShiftTowers(5).Layer2(0), 1 => ShiftTowers(4).Layer2(3), 2 => ShiftTowers(4).Layer2(2),
                             3 => ShiftTowers(2).Layer2(0), 4 => ShiftTowers(1).Layer2(3), 5 => ShiftTowers(1).Layer2(2));
      end case;



      -------------------------------------------------------------------------------
      -- Jet Env Data
      -------------------------------------------------------------------------------
      case Selector is
        -- Up
        when "100" =>           --up
          OutJetEnvData <= (0 => ShiftTowers(4).Layer2(3), 1 => ShiftTowers(4).Layer2(2), 2 => ShiftTowers(3).Layer2(2), 3 => ShiftTowers(3).Layer2(1), 4 => ShiftTowers(3).Layer2(0), 5 => ShiftTowers(5).Layer2(0),
                            6 => ShiftTowers(7).Layer2(3), 7 => ShiftTowers(7).Layer2(2), 8 => ShiftTowers(6).Layer2(2), 9 => ShiftTowers(6).Layer2(1), 10 => ShiftTowers(6).Layer2(0), 11 => ShiftTowers(8).Layer2(0)
                            );

        when "101" =>
          OutJetEnvData <= (0 => ShiftTowers(4).Layer2(3), 1 => ShiftTowers(3).Layer2(3), 2 => ShiftTowers(3).Layer2(2), 3 => ShiftTowers(3).Layer2(1), 4 => ShiftTowers(5).Layer2(1), 5 => ShiftTowers(5).Layer2(0),
                            6 => ShiftTowers(7).Layer2(3), 7 => ShiftTowers(6).Layer2(3), 8 => ShiftTowers(6).Layer2(2), 9 => ShiftTowers(6).Layer2(1), 10 => ShiftTowers(8).Layer2(1), 11 => ShiftTowers(8).Layer2(0)
                            );

        when "110" =>
          OutJetEnvData <= (0 => ShiftTowers(4).Layer2(0), 1 => ShiftTowers(3).Layer2(1), 2 => ShiftTowers(3).Layer2(2), 3 => ShiftTowers(5).Layer2(2), 4 => ShiftTowers(5).Layer2(1), 5 => ShiftTowers(5).Layer2(0),
                            6 => ShiftTowers(7).Layer2(0), 7 => ShiftTowers(6).Layer2(1), 8 => ShiftTowers(6).Layer2(2), 9 => ShiftTowers(8).Layer2(2), 10 => ShiftTowers(8).Layer2(1), 11 => ShiftTowers(8).Layer2(0)
                            );

        when "111" =>
          OutJetEnvData <= (0 => ShiftTowers(4).Layer2(1), 1 => ShiftTowers(4).Layer2(0), 2 => ShiftTowers(3).Layer2(3), 3 => ShiftTowers(5).Layer2(3), 4 => ShiftTowers(5).Layer2(2), 5 => ShiftTowers(5).Layer2(1),
                            6 => ShiftTowers(7).Layer2(1), 7 => ShiftTowers(7).Layer2(0), 8 => ShiftTowers(6).Layer2(3), 9 => ShiftTowers(8).Layer2(2), 10 => ShiftTowers(8).Layer2(2), 11 => ShiftTowers(8).Layer2(1)
                            );
-- down
        when "000" =>
          OutJetEnvData <= (0 => ShiftTowers(4).Layer2(3), 1 => ShiftTowers(4).Layer2(2), 2 => ShiftTowers(3).Layer2(2), 3 => ShiftTowers(3).Layer2(1), 4 => ShiftTowers(3).Layer2(0), 5 => ShiftTowers(5).Layer2(0),
                            6 => ShiftTowers(1).Layer2(3), 7 => ShiftTowers(1).Layer2(2), 8 => ShiftTowers(0).Layer2(2), 9 => ShiftTowers(0).Layer2(1), 10 => ShiftTowers(0).Layer2(0), 11 => ShiftTowers(2).Layer2(0)
                            );

        when "001" =>
          OutJetEnvData <= (0 => ShiftTowers(4).Layer2(3), 1 => ShiftTowers(3).Layer2(3), 2 => ShiftTowers(3).Layer2(2), 3 => ShiftTowers(3).Layer2(1), 4 => ShiftTowers(5).Layer2(1), 5 => ShiftTowers(5).Layer2(0),
                            6 => ShiftTowers(1).Layer2(3), 7 => ShiftTowers(0).Layer2(3), 8 => ShiftTowers(0).Layer2(2), 9 => ShiftTowers(0).Layer2(1), 10 => ShiftTowers(2).Layer2(1), 11 => ShiftTowers(2).Layer2(0)
                            );

        when "010" =>
          OutJetEnvData <= (0 => ShiftTowers(4).Layer2(0), 1 => ShiftTowers(3).Layer2(1), 2 => ShiftTowers(3).Layer2(2), 3 => ShiftTowers(5).Layer2(2), 4 => ShiftTowers(5).Layer2(1), 5 => ShiftTowers(5).Layer2(0),
                            6 => ShiftTowers(1).Layer2(0), 7 => ShiftTowers(0).Layer2(1), 8 => ShiftTowers(0).Layer2(2), 9 => ShiftTowers(2).Layer2(2), 10 => ShiftTowers(2).Layer2(1), 11 => ShiftTowers(2).Layer2(0)
                            );

        when others =>          -- 011
          OutJetEnvData <= (0 => ShiftTowers(4).Layer2(1), 1 => ShiftTowers(4).Layer2(0), 2 => ShiftTowers(3).Layer2(3), 3 => ShiftTowers(5).Layer2(3), 4 => ShiftTowers(5).Layer2(2), 5 => ShiftTowers(5).Layer2(1),
                            6 => ShiftTowers(1).Layer2(1), 7 => ShiftTowers(1).Layer2(0), 8 => ShiftTowers(0).Layer2(3), 9 => ShiftTowers(2).Layer2(2), 10 => ShiftTowers(2).Layer2(2), 11 => ShiftTowers(2).Layer2(1)
                            );
      end case;

    end if;
  end process;

----------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Output connections
----------------------------------------------------------------------------------------------------------------------------------------------------------------
      OUT_Energy_L0  <= OutEnergy_L0;
      OUT_Energy_L1  <= OutEnergy_L1;
      OUT_Energy_L2  <= OutEnergy_L2;
      OUT_Energy_L3  <= OutEnergy_L3;
      OUT_Energy_HAD <= OutEnergy_HAD;

      OUT_JetEnvData  <= OutJetEnvData;
      OUT_JetCoreData <= OutJetCoreData;

end Behavioral;
