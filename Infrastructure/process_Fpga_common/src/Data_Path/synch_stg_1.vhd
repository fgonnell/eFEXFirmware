----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/26/2016 03:07:11 PM
-- Design Name: 
-- Module Name: synch_stg_1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.all;

LIBRARY xil_defaultlib;
--use xil_defaultlib.synch_type.all;


entity synch_stage_1 is
  Port ( 
    
     
    clk_280      : in  std_logic;   
    reset        : in  std_logic;
    mux_cntrl    : in  std_logic_vector(3 downto 0);
    latch_enable : in  std_logic;
    enable_mgt   : in  std_logic;
    MGT_Commadet : in  std_logic;
    commdet_delay: out std_logic;
    data_in      : in  std_logic_vector(31 downto 0);
    data_out     : out std_logic_vector(225 downto 0);
    disperr_error: in std_logic;
    notable_error: in std_logic
    
    );
end synch_stage_1;

architecture Behavioral of synch_stage_1 is

signal temp7,temp8,temp9,temp10,temp11,temp12:std_logic_vector(33 downto 0);
signal data_int:std_logic_vector(34 downto 0);
signal data_out_int : std_logic_vector(225 downto 0);
signal cntr_delay: unsigned( 3 downto 0);
signal mux_out,mux_out_i:std_logic_vector(34 downto 0):= (others=>'0');
signal disperr_error_i, notable_error_i: std_logic;

begin

 process (clk_280) -- This register captures the first data from the rx mgt
 
   begin
       if clk_280' event and clk_280 ='1' then          
          if enable_mgt ='1' then                  
             data_int <= MGT_Commadet & disperr_error & notable_error & data_in;
            else 
             data_int <= (others =>'0');
      end if; 
      end if;     
 end process;

--data_int <= MGT_Commadet & data_in;

commdet_delay <= mux_out(34); 


------------------------------------------------------------------------------------------------------ 
 SRL_16E_35: entity work.SRL16E_35 
   port map (
      clk      => clk_280,
	  address  => mux_cntrl,
	  data_in  => data_int,
	  data_out => mux_out_i,
	  srl_en   => '1' --enable_mgt
	  );
reg_mux_out_i: process (clk_280)  -- register for mux_out_i
         
           begin                  
                     
                if clk_280'event and clk_280 ='1' then   
                       mux_out <= mux_out_i;
                    
                  end if;
              end process;                
         
-------------------------------------------------------------------------------------------              
Shifter_1: process (clk_280) -- Second stages of  data shifter performs serial to parallel conversion
            
              begin                    
                                            
                    if clk_280'event and clk_280 ='1' then 
                          temp7  <= mux_out(33 downto 0);
                          temp8  <= temp7;
                          temp9  <= temp8;
                          temp10  <= temp9;
                          temp11 <= temp10;
                          temp12 <= temp11;
                     end if;               
                 end process;        
                                                      
 data_out_int     <= disperr_error_i & notable_error_i & mux_out(31 downto 0) & temp7(31 downto 0) & temp8(31 downto 0) & temp9(31 downto 0) & temp10(31 downto 0) & temp11(31 downto 0) & temp12(31 downto 0) ;    --  Concatenate the shifted data in order to form 224 bits
 notable_error_i  <=   mux_out(32) or temp7( 32) or temp8( 32) or temp9( 32) or temp10( 32) or temp11( 32) or temp12( 32) ;
 disperr_error_i  <=   mux_out(33) or temp7( 33) or temp8( 33) or temp9( 33) or temp10( 33) or temp11( 33) or temp12( 33) ;
 
Sipo_reg: process(clk_280) -- latch when data ready 224 bits, waits 7 clocks of 280MHz 
              begin 

                  if clk_280'event and clk_280 ='1' then                   
                      if latch_enable ='1' then 
                         data_out <= data_out_int( 225 downto 0);
                                  
                      end if;
                  end if;
           end process;  
       
end Behavioral;
