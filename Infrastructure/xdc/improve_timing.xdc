#Timing improvement constraint added 22/11/18





#set_property BLOCK_SYNTH.STRATEGY {PERFORMANCE_OPTIMIZED} [get_cells data_path_module] ;

##### TOB Readout Contraints

#set_max_delay 3.0 -datapath_only -from [get_pins TOB_readout/U1_RAW_readout/U7_rd_RAW_mux_fsm/RAW_out_valid_i_reg/C] -to [get_pins {TOB_readout/U1_RAW_readout/U8_RAW_Link_output_FIFO/U0/inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.mem/gbm.gbmg.gbmga.ngecc.bmg/inst_blk_mem_gen/gnbram.gnativebmg.native_blk_mem_gen/valid.cstr/ramloop[*].ram.r/prim_noinit.ram/DEVICE_7SERIES.NO_BMM_INFO.SDP.SIMPLE_PRIM36.ram/WEA[0]}]
#set_max_delay 3.0 -datapath_only -from [get_pins TOB_readout/U1_RAW_readout/U7_rd_RAW_mux_fsm/RAW_out_valid_i_reg/C] -to [get_pins {TOB_readout/U1_RAW_readout/U8_RAW_Link_output_FIFO/U0/inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.gl0.wr/gwas.gpf.wrpf/gdiff.diff_pntr_pad_reg[*]/D}]
#set_max_delay 2.0 -datapath_only -from [get_pins {ALIGN_AND_TEST_PATTERN.tx_phase_adjust/error_detector/gen_error_detector[0].mgt_error_detect/rx_in_reg[data][13]/C}] -to [get_pins {ALIGN_AND_TEST_PATTERN.tx_phase_adjust/error_detector/gen_error_detector[7].mgt_error_detect/error_counter_reg[*]/CE}]


# set flase path from 280MHz clock to TXOUTCLK on link output FIFO read

#WRONG set_multicycle_path -setup -end -from [get_pins TOB_readout/U0_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/frame_counter_dec_en_i_reg/C] -to [get_pins TOB_readout/U0_TOBs_readout/reg1_reg/D] 2
#WRONG set_multicycle_path -hold -end -from [get_pins TOB_readout/U0_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/frame_counter_dec_en_i_reg/C] -to [get_pins TOB_readout/U0_TOBs_readout/reg1_reg/D] 1

#WRONG set_multicycle_path -setup -end -from [get_pins TOB_readout/U1_RAW_readout/U9_RAW_Link_output_FIFO_FSM/frame_counter_dec_en_i_reg/C] -to [get_pins TOB_readout/U1_RAW_readout/reg1_reg/D] 2
#WRONG set_multicycle_path -hold -end -from [get_pins TOB_readout/U1_RAW_readout/U9_RAW_Link_output_FIFO_FSM/frame_counter_dec_en_i_reg/C] -to [get_pins TOB_readout/U1_RAW_readout/reg1_reg/D] 1

#set_multicycle_path -setup -end -from [get_pins {TOB_readout/U0_TOBs_readout/U9_frame_counter/temp_reg[*]/C}] -to [get_pins TOB_readout/U0_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/MGT_fifo_rd_en_i_reg/D] 2
#set_multicycle_path -hold -end -from [get_pins {TOB_readout/U0_TOBs_readout/U9_frame_counter/temp_reg[*]/C}] -to [get_pins TOB_readout/U0_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/MGT_fifo_rd_en_i_reg/D] 1

#set_multicycle_path -setup -end -from [get_pins {TOB_readout/U0_TOBs_readout/U9_frame_counter/temp_reg[*]/C}] -to [get_pins {TOB_readout/U0_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/FSM_sequential_current_state_reg[*]/D}] 2
#set_multicycle_path -hold -end -from [get_pins {TOB_readout/U0_TOBs_readout/U9_frame_counter/temp_reg[*]/C}] -to [get_pins {TOB_readout/U0_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/FSM_sequential_current_state_reg[*]/D}] 1

#set_multicycle_path -setup -end -from [get_pins {TOB_readout/U1_RAW_readout/U10_RAW_frame_counter/temp_reg[*]/C}] -to [get_pins {TOB_readout/U1_RAW_readout/U9_RAW_Link_output_FIFO_FSM/FSM_sequential_current_state_reg[*]/D}] 2
#set_multicycle_path -hold -end -from [get_pins {TOB_readout/U1_RAW_readout/U10_RAW_frame_counter/temp_reg[*]/C}] -to [get_pins {TOB_readout/U1_RAW_readout/U9_RAW_Link_output_FIFO_FSM/FSM_sequential_current_state_reg[*]/D}] 1

#set_multicycle_path -setup -end -from [get_pins {TOB_readout/U1_RAW_readout/U10_RAW_frame_counter/temp_reg[*]/C}] -to [get_pins TOB_readout/U1_RAW_readout/U9_RAW_Link_output_FIFO_FSM/MGT_fifo_rd_en_i_reg/D] 2
#set_multicycle_path -hold -end -from [get_pins {TOB_readout/U1_RAW_readout/U10_RAW_frame_counter/temp_reg[*]/C}] -to [get_pins TOB_readout/U1_RAW_readout/U9_RAW_Link_output_FIFO_FSM/MGT_fifo_rd_en_i_reg/D] 1
# set mutlticycle from 280 to 160 clk
# WRONG set_multicycle_path -setup -end -from [get_clocks -of_objects [get_pins clock_resources/Inputclk40M/inst/mmcm_adv_inst/CLKOUT2]] -to [get_clocks -of_objects [get_pins clock_resources/clk160_gen/inst/mmcm_adv_inst/CLKOUT0]] 2
# WRONG set_multicycle_path -hold -end -from [get_clocks -of_objects [get_pins clock_resources/Inputclk40M/inst/mmcm_adv_inst/CLKOUT2]] -to [get_clocks -of_objects [get_pins clock_resources/clk160_gen/inst/mmcm_adv_inst/CLKOUT0]] 1

# this is the timing for transmitting data from prcessor FPGA to TOPO

# preserve signal BCN_ID_i in order to set max fan out
#set_property MAX_FANOUT 10 [get_nets BCN_ID_i]

### Following section is for MUXF* reampping on congested modules
### For following modules MUXF* are remapped to remove congestion.
###
set_property BLOCK_SYNTH.MUXF_MAPPING 0 [get_cells data_path_Module/algorithm_block/TOP_ALGO_MODULE]
#set_property BLOCK_SYNTH.MUXF_MAPPING 0 [get_cells TOB_readout/U1_RAW_readout]
###
### End of section for remapping MUXF* on congested modules

#set_output_delay -max 0.700 -clock [get_clocks clk280] [get_ports data_to_fpga_*]
#set_output_delay -min 0.100 -clock [get_clocks clk280] [get_ports data_to_fpga_*]

#set_input_delay -clock [get_clocks clk280] -min 0.7 [get_ports data_from_fpga_*]
#set_input_delay -clock [get_clocks clk280] -max 1.7 [get_ports data_from_fpga_*]

### set max path delay from FIFO RAW data to MUX PISO FSM -- can add another set of registers
#set_max_delay -from [get_pins {TOB_readout/U1_RAW_readout/GEN_CHANNEL[*].U4_FIFO_RAW_Data/U0/inst_fifo_gen/gconvfifo.rf/grf.rf/rstblk/ngwrdrst.grst.g7serrst.gnsckt_wrst.gic_rst.sckt_wr_rst_ic_reg/C}] -to [get_pins {TOB_readout/U1_RAW_readout/GEN_CHANNEL[*].U4_FIFO_RAW_Data/U0/inst_fifo_gen/gconvfifo.rf/grf.rf/gntv_or_sync_fifo.gl0.wr/wpntr/gic0.gc1.count_d1_reg[*]/CLR}] 2.0

## algo

#set_max_delay -from [get_pins {data_path_Module/algorithm_block/IPBUS_INPUT_RAM/d_counter_reg[*]/C}] -to [get_pins {data_path_Module/algorithm_block/IPBUS_INPUT_RAM/RAM_FOR[0].ALGO_INPUT_RAM_WRAPPER/ALGO_INPUT_RAM/U0/inst_blk_mem_gen/gnbram.gnativebmg.native_blk_mem_gen/valid.cstr/ramloop[0].ram.r/prim_noinit.ram/DEVICE_7SERIES.NO_BMM_INFO.TRUE_DP.SIMPLE_PRIM36.ram/ADDRBWRADDR[*]}] 4.6

#set_max_delay -from [get_pins {tx_phase_adjust/phase_align/cntr_reg[2]/C}] -to [get_pins {tx_phase_adjust/phase_align/MGT_phase_alignment[*].Tx_Phase_alignment/dout_2_i_reg[*]/D}] 1.6




set_property IODELAY_GROUP io_delay_group [get_cells {GLOBAL_MERGE.IO_DELAY_BC_A/inst/pins[0].idelaye2_bus}]
set_property IODELAY_GROUP io_delay_group [get_cells {GLOBAL_MERGE.IO_DELAY_BC_B/inst/pins[0].idelaye2_bus}]
set_property IODELAY_GROUP io_delay_group [get_cells {GLOBAL_MERGE.IO_DELAY_BC_C/inst/pins[0].idelaye2_bus}]
