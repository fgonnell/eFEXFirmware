----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Creabe Date:
-- Design Name:
-- Module Name:
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: read data from file:
-- this is the data format, spaces matter
--  samples are 4-nibble words in hex
--  samples are in this order:
--      L0.0<3 spaces>L1.0<space>L1.1<space>L1.2<space>L1.3<3 spaces>L2.0<space>L2.1<space>L2.2<space>L2.3<3 spaces>L3.0<3 spaces>HAD.0<end of line>
--  samples of the same layer are separated by 1 space
--  different layes are separated by 3 spaces
--  the moudule needs 9 trigger towers:
--   6 7 8
--   3 4 5 Tower number 4 is the core
--   0 1 2  
-- different data samples - pertaining to different clock cycles - are separated by an empty line (or a line that will be ignored)
-- the file must end with 2 empy lines (or 2 lines that will be ignored)
-- the file can contain an arbitrary number of data samples
-- example with all zeroes:

-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- <empty line>
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- <empty line>
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- <empty line>
-- <empty line>

-- Dependencies: DataTypes package, Utility
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;


-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library std;
use std.textio.all;                     --TEXT is here


library algolib;
use algolib.DataTypes.all;
use algolib.Utility.all;

entity FileReaderSingle is
  generic(FILENAME : string := "../../../../../../sim/AlgoCore/SingleTowers.txt");
  port(
    DATA_OUT   : out TriggerTowers(8 downto 0);
    DATA_READY : out std_logic;
    CLK40      : out std_logic;
    CLK160     : out std_logic;
    CLK200     : out std_logic;
    RESET      : out std_logic);
end FileReaderSingle;

--
architecture std of FileReaderSingle is
  constant PERIOD     : time := 25 ns;  -- 40 MHz;
  constant OFFSET     : time := 1 ns;  -- delay between data and clock (must be less then half the period of the fastes clock 200MHz  T/2 = 2.5 ns)
  constant RESET_TIME : time := 48 ns;

  file stimulus   : text open read_mode is FILENAME;
  signal clock40  : std_logic;
  signal clock160 : std_logic;
  signal clock200 : std_logic;

begin

  CLK40  <= clock40;
  CLK160 <= clock160;
  CLK200 <= clock200;

  Clock40Generator : process
  begin
    clock40 <= '1';
    wait for 0.5*PERIOD;
    clock40 <= '0';
    wait for 0.5*PERIOD;
  end process;

  Clock160Generator : process
  begin
    clock160 <= '1';
    wait for 0.5*(PERIOD/4);
    clock160 <= '0';
    wait for 0.5*(PERIOD/4);
  end process;

  Clock200Generator : process
  begin
    clock200 <= '1';
    wait for 0.5*(PERIOD/5);
    clock200 <= '0';
    wait for 0.5*(PERIOD/5);
  end process;



  stim_proc : process
    variable l            : line;
    variable s            : string(DATA_WIDTH/4 downto 1);
    variable three_spaces : string(3 downto 1);
    variable space        : string(1 downto 1);
    variable n            : integer;
    variable DataString   : string(DATA_WIDTH downto 1);

  begin
    n          :=0;
    DATA_OUT   <= (others => ZERO_TRIGGER_TOWER);
    DATA_READY <= '0';
    RESET      <= '1';
    wait for RESET_TIME;
    reset      <= '0';

    DATA_OUT   <= (others => ZERO_TRIGGER_TOWER);
    DATA_READY <= '0';

    wait for 9 * PERIOD;

    wait until clock200 = '0';
    wait for (0.5*PERIOD/5 - OFFSET);

    while not endfile(stimulus) loop
      for i in 0 to 8 loop
        readline(stimulus, l);
        n                     :=n+1;
        --layer 0
        read(l, s);
        DATA_OUT(i).Layer0(0) <= to_std_logic_vector(str_hex_to_bin(s));
        --3 spaces
        read(l, three_spaces);
        if (three_spaces /= "   ") then
            report "Syntax error in file in layer 0: `"&s&" '"&three_spaces&"'" severity error;
        end if;

        --layer 1
        for j in 0 to 3 loop
          read(l, s);
          DATA_OUT(i).Layer1(j) <= to_std_logic_vector(str_hex_to_bin(s));
          if j < 3 then
            --1 space
            read(l, space);
            if (space /= " ") then
              report "Syntax error in file in layer 1: "&space severity error;
            end if;
          end if;
        end loop;
        --3 spaces
        read(l, three_spaces);
        if (three_spaces /= "   ") then
          report "Syntax error in file after layer 1: "&three_spaces severity error;
        end if;

        --layer 2
        for j in 0 to 3 loop
          read(l, s);
          DATA_OUT(i).Layer2(j) <= to_std_logic_vector(str_hex_to_bin(s));
          if j < 3 then
            --1 space
            read(l, space);
            if (space /= " ") then
              report "Syntax error in file in layer 2!" severity error;
            end if;
          end if;
        end loop;
        --3 spaces
        read(l, three_spaces);
        if (three_spaces /= "   ") then
          report "Syntax error in file after layer 2!" severity error;
        end if;

        --layer 3
        read(l, s);
        DATA_OUT(i).Layer3(0) <= to_std_logic_vector(str_hex_to_bin(s));
        --3 spaces
        read(l, three_spaces);
        if (three_spaces /= "   ") then
          report "Syntax error in file after layer 2!" severity error;
        end if;

        --hadronic
        read(l, s);
        DATA_OUT(i).Hadron(0) <= to_std_logic_vector(str_hex_to_bin(s));
      end loop;
      readline(stimulus, l);  --Blank line after a set of trigger towers                
      n :=n+1;
      DATA_READY <= '1';
                    
      wait until clock200 = '0';
      wait for (0.5*PERIOD/5 - OFFSET);

--                DATA_READY <= '0';
--                DATA_OUT   <= (others=> ZERO_TRIGGER_TOWER);            
    end loop;

    DATA_OUT   <= (others => ZERO_TRIGGER_TOWER);
    DATA_READY <= '0';
    wait for 1000 us;
  end process;
end std;

