#!/bin/perl
$USAGE =
    '
  Usage: GenerateData.pl <mode> [energy]
    <mode> can be: rand, seq, one_sliding
    ';
if ($#ARGV == -1) {
    print $USAGE;
} else {
    $MODE = $ARGV[0];
    $zero = "0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000\n";
     if ($MODE eq "rand" or $MODE eq "seq") {
	 for $n (0..10) {
	     for ($i = 0; $i< 60; $i++) { 
		 if ($MODE eq "rand") {
		     for ($h = 0; $h< 11; $h++) { 
			 $j[$h] = sprintf"%04x", int(rand(12));
		     }
		     print   "${j[0]}   ${j[1]} ${j[2]} ${j[3]} ${j[4]}   ${j[5]} ${j[6]} ${j[7]} ${j[8]}   ${j[9]}   ${j[1]}\n";
		 } elsif ($MODE eq "seq") {
		     $j = sprintf"%02d", $i;
		     print   "${j}00   ${j}10 ${j}11 ${j}12 ${j}13   ${j}20 ${j}21 ${j}22 ${j}23   ${j}30   ${j}40\n";
		     
		 }
		 
	     }
	     print "\n";
	     
	     for ($i = 0; $i< 60; $i++) { 
		 print $zero;
	     }
	     print "\n";
	 }
     } elsif ($MODE eq "one_sliding") {
	 if ($#ARGV >= 0) {
	     $en = $ARGV[1]
	 } else {
	     $en = 0x10;
	 }
	 $ene = sprintf "%04x", $en;	 

	 for ($i = 0; $i< 9; $i++) {print $zero};
	 print "\n";
	 
	 #1
	 for ($i = 0; $i< 9; $i++) {
	     if ($i == 4 ) {
		 print   "0000   0000 0000 0000 0000   $ene 0000 0000 0000   0000   0000\n";
	     } else {
		 print $zero;
	     }
	 }
	 print "\n";

	 #2
	 for ($i = 0; $i< 9; $i++) {
	     if ($i == 4 ) {
		 print   "0000   0000 0000 0000 0000   0000 $ene 0000 0000   0000   0000\n";
	     } else {
		 print $zero;
	     }
	 }
	 print "\n";

	 #3
	 	 for ($i = 0; $i< 9; $i++) {
	     if ($i == 4 ) {
		 print   "0000   0000 0000 0000 0000   0000 0000 $ene 0000   0000   0000\n";
	     } else {
		 print $zero;
	     }
	 }
	 print "\n";

	 #4
	 for ($i = 0; $i< 9; $i++) {
	     if ($i == 4 ) {
		 print   "0000   0000 0000 0000 0000   0000 0000 0000 $ene   0000   0000\n";
	     } else {
		 print $zero;
	     }
	 }
	 print "\n";
	 
	 for ($i = 0; $i< 9; $i++) {print $zero};
	 print "\n";
	 
     } else {
	 print "Unknown mode $MODE", $USAGE;
     }
}
