library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library algolib;
use algolib.DataTypes.all;
use algolib.AlgoDataTypes.all;

library algo;
use algo.all;


library ipbus_lib;
use ipbus_lib.ipbus.all;

entity tb_AlgoModule is
--  Port ( );
end tb_AlgoModule;

architecture TestBench of tb_AlgoModule is
-- clocks
  signal clk40   : std_logic;
  signal clk40_p : std_logic;
  signal clk40_n : std_logic;
  signal clk200  : std_logic;
  signal clk280  : std_logic;
  signal rst     : std_logic;

-- data signals
  signal DataReady : std_logic;


  signal AlgoData : AlgoInput;

  signal ipb_rst : std_logic := '0';
  signal ipb_clk : std_logic := '0';

  signal ipb_in_algo                   : ipb_wbus;  -- := IPB_WBUS_NULL;
  signal ipb_in_sorting                : ipb_wbus;  -- := IPB_WBUS_NULL;
  signal ipb_out_algo, ipb_out_sorting : ipb_rbus;

  signal eg_Start     : std_logic;
  signal one_eg_Valid : std_logic;
  signal eg_Valid     : std_logic_vector(7 downto 0);
  signal eg_TOB       : AlgoOutput;
  signal eg_XTOB      : AlgoXOutput;

  signal tau_Start     : std_logic;
  signal one_tau_Valid : std_logic;
  signal tau_Valid     : std_logic_vector(7 downto 0);
  signal tau_TOB       : AlgoOutput;
  signal tau_XTOB      : AlgoXOutput;

  signal BCID_XTOB      : std_logic_vector(6 downto 0);
  signal eg_sync_XTOB   : std_logic;
  signal tau_sync_XTOB  : std_logic;
  signal eg_Valid_XTOB  : std_logic_vector(7 downto 0);
  signal tau_Valid_XTOB : std_logic_vector(7 downto 0);

  signal load   : std_logic;
  signal locked : std_logic;

  signal eg_Sync_out   : std_logic;
  signal eg_Valid_out  : std_logic;
  signal eg_TOB_out    : AlgoTriggerObject;
  signal tau_Sync_out  : std_logic;
  signal tau_Valid_out : std_logic;
  signal tau_TOB_out   : AlgoTriggerObject;

  signal eFEXPosition : std_logic_vector(31 downto 0);

  signal BCID_to_sort, SortedBCID : BCID_t;
  signal BCID_int                 : BCID_array;

  component ClockWizard
    port
      (                         -- Clock in ports
        -- Clock out ports
        clk200    : out std_logic;
        load      : out std_logic;
        clk40     : out std_logic;
        clk280    : out std_logic;
        -- Status and control signals
        locked    : out std_logic;
        reset     : in  std_logic;
        clk_in1_p : in  std_logic;
        clk_in1_n : in  std_logic
        );
  end component;

  constant ipb_period : time := 13 ns;

begin

  ipbus_clk : process
  begin
    ipb_clk <= '1';
    wait for ipb_period/2;      --for 25 ns signal is '0'.
    ipb_clk <= '0';
    wait for ipb_period/2;      --for next 25 ns signal is '1'.
  end process;

  TESTER : entity algo.FileReader
    generic map (DEAD_TIME => 3.0 us)

    port map (
      DATA_OUT   => AlgoData,
      BCID_OUT   => BCID_int,
      DATA_READY => DataReady,
      CLK40      => clk40_p,
      CLK160     => open,
      CLK200     => open,
      LOAD       => open,
      CLK280     => open,
      RESET      => rst);

  clk40_n <= not clk40_p;

  CLOCK_WIZARD : ClockWizard
    port map (
      clk_in1_p => clk40_p,
      clk_in1_n => clk40_n,

      reset  => rst,
      clk40  => clk40,
      clk200 => clk200,
      clk280 => clk280,
      load   => load,
      -- Status and control signals                
      locked => locked
      );


  TOPALGO : entity algolib.IPBusTopAlgoModule
    generic map (FPGA        => 1)
    port map (
      CLK200  => clk200,
      CLK280  => clk280,
      IN_Load => load,
      ipb_clk => ipb_clk,
      ipb_rst => rst,
      ipb_in  => ipb_in_algo,
      ipb_out => ipb_out_algo,

      OUT_eFEXPosition => eFEXPosition,
      IN_BCID          => BCID_int,
      IN_Data          => AlgoData,



      OUT_BCID_XTOB      => BCID_XTOB,
      OUT_eg_sync_XTOB   => eg_sync_XTOB,
      OUT_tau_sync_XTOB  => tau_sync_XTOB,
      OUT_eg_XTOB        => eg_XTOB,
      OUT_tau_XTOB       => tau_XTOB,
      OUT_eg_Valid_XTOB  => eg_Valid_XTOB,
      OUT_tau_Valid_XTOB => tau_Valid_XTOB,

      OUT_BCID_TOB => BCID_to_sort,
      OUT_eg_Sync  => eg_Start,
      OUT_eg_Valid => eg_Valid,
      OUT_eg_TOB   => eg_TOB,

      OUT_tau_Sync  => tau_Start,
      OUT_tau_Valid => tau_Valid,
      OUT_tau_TOB   => tau_TOB);









  one_eg_Valid  <= eg_Valid(7) or eg_Valid(6) or eg_Valid(5) or eg_Valid(4) or eg_Valid(3) or eg_Valid(2) or eg_Valid(1) or eg_Valid(0);
  one_tau_Valid <= tau_Valid(7) or tau_Valid(6) or tau_Valid(5) or tau_Valid(4) or tau_Valid(3) or tau_Valid(2) or tau_Valid(1) or tau_Valid(0);

  EG_XTOB_FileWriter : entity algo.FileWriter
    generic map (
      data_width => 64*8,
      filename   => "../../XTOB_eg.txt")
    port map (
      CLK     => clk280,
      RST     => rst,
      DATA_IN => eg_XTOB(7)&eg_XTOB(6)&eg_XTOB(5)&eg_XTOB(4)&eg_XTOB(3)&eg_XTOB(2)&eg_XTOB(1)&eg_XTOB(0),
      SYNC    => eg_Start,
      WE      => '1');

  TAU_XTOB_FileWriter : entity algo.FileWriter
    generic map (
      data_width => 64*8,
      filename   => "../../XTOB_tau.txt")
    port map (
      CLK     => clk280,
      RST     => rst,
      DATA_IN => tau_XTOB(7)&tau_XTOB(6)&tau_XTOB(5)&tau_XTOB(4)&tau_XTOB(3)&tau_XTOB(2)&tau_XTOB(1)&tau_XTOB(0),
      SYNC    => tau_Start,
      WE      => '1');

  ---------------------------------------------------------------------

  SortingModule : entity algolib.IPBusTopSortingModule
    port map (
      CLK         => clk280,
      IN_eg_Data  => eg_TOB,
      IN_tau_Data => tau_TOB,
      IN_Sync     => eg_Start,
      IN_BCID     => BCID_to_sort,

      ipb_clk => ipb_clk,
      ipb_rst => ipb_rst,
      ipb_in  => ipb_in_sorting,
      ipb_out => ipb_out_sorting,

      OUT_BCID      => SortedBCID,
      OUT_eg_Sync   => eg_Sync_out,
      OUT_eg_Valid  => eg_Valid_out,
      OUT_eg_TOB    => eg_TOB_out,
      OUT_tau_Sync  => tau_Sync_out,
      OUT_tau_Valid => tau_Valid_out,
      OUT_tau_TOB   => tau_TOB_out);

  EG_TOB_FileWriter : entity algo.FileWriter
    generic map (
      data_width => eg_TOB_out'high+1,
      filename   => "../../TOB_eg.txt")
    port map (
      CLK     => clk280,
      RST     => rst,
      DATA_IN => eg_TOB_out,
      SYNC    => eg_Sync_out,
      WE      => eg_Valid_out);

  TAU_TOB_FileWriter : entity algo.FileWriter
    generic map (
      data_width => tau_TOB_out'high+1,
      filename   => "../../TOB_tau.txt")
    port map (
      CLK     => clk280,
      RST     => rst,
      DATA_IN => tau_TOB_out,
      SYNC    => tau_Sync_out,
      WE      => tau_Valid_out);

end TestBench;

