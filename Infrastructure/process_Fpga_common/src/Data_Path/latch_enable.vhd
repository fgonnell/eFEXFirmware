----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/11/2016 01:07:56 PM
-- Design Name: 
-- Module Name: latch_enable - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.all;
LIBRARY xil_defaultlib;
--use xil_defaultlib.synch_type.all;

entity latch_enable is
  Port ( 
     clk_280m    : IN     std_logic;
     MGT_COMMADET : IN     std_logic;
     latch_enable  : OUT    std_logic
     
     );
end latch_enable;

architecture Behavioral of latch_enable is

signal cntr     : unsigned(3 downto 0):= "0000";


begin


 process (clk_280m)
 
      begin
          if clk_280m'  event and clk_280m  ='1'  then
          latch_enable  <='0';
          cntr <= cntr +1;
            if  (MGT_COMMADET ='1' )then
                cntr <="0010";
            end if;  
         
            if (cntr = 6  and  MGT_COMMADET ='0'  ) then
                latch_enable  <='1';
                cntr <="0000";
            end if;
       end if;
   end process;    
         
         
end Behavioral;
