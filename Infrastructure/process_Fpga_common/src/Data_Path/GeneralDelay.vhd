--! @file
--! @brief Shift register for data delay
--! @details 
--! General Shift register with custom data width and delay.
--! @author Francesco Gonnella
library IEEE;
use IEEE.STD_LOGIC_1164.all;

--! @copydoc GeneralDelay.vhd
entity GeneralDelay is
  generic (delay : integer := 1;
           size  : integer := 32
           );

  port (
    clk      : in  std_logic;
    data_in  : in  std_logic_vector(size-1 downto 0);
    data_out : out std_logic_vector(size-1 downto 0)
    );
end GeneralDelay;

--! @copydoc GeneralDelay.vhd
architecture Behavioral of GeneralDelay is

  type t_DelayedSignal is array (delay downto 0) of std_logic_vector(size-1 downto 0);
  signal DelayedSignal : t_DelayedSignal := (others => (others => '0'));

begin

  dalay_proc : process (clk)
  begin
    if rising_edge(clk) then
      DelayedSignal(DelayedSignal'high)            <= data_in;
      DelayedSignal(DelayedSignal'high-1 downto 0) <= DelayedSignal(DelayedSignal'high downto 1);
    end if;
  end process;

  data_out <= DelayedSignal(0);
end Behavioral;
