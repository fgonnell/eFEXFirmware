library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
Library UNISIM;
use UNISIM.vcomponents.all;   

 entity  SRLC32E_226 is
   port (
      clk      : in std_logic;
      srl_en   : in std_logic;
	  address  : in std_logic_vector (4 downto 0);
	  data_in  : in std_logic_vector (226 downto 0);
	  data_out : out std_logic_vector (226 downto 0)
	  );
	  
  end  SRLC32E_226;
   
   
 architecture Behavioral of SRLC32E_226 is  
 signal d_i,q_i :std_logic_vector (226 downto 0);
 
begin   
  
data_out <= q_i;
d_i <= data_in;  
  shift_mux227 : for i in 0 to 226 
   generate 
   SRL132E_inst_227 : SRLC32E
   generic map (
      INIT => X"00000000")
   port map (
      Q => q_i(i),   -- SRL data output
      Q31=> open,        -- SRL cascade output pin            
      A => address,      -- 5-bit shift depth select input      
      CE => '1' , --srl_en,       -- Clock enable input
      CLK => CLK,     -- Clock input
      D => d_i(i)        -- SRL data input
	  );
 end generate  shift_mux227 ;
   
  

end Behavioral;  