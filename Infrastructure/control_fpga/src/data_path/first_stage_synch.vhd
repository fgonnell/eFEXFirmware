 
----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/10/2018 03:07:11 PM
-- Design Name: 
-- Module Name: synch_stg_1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.all;

LIBRARY xil_defaultlib;
--use xil_defaultlib.synch_type.all;


entity first_stage_synch is
  Port ( 
    
     
    clk160       : in  std_logic;   
    reset        : in  std_logic;
    mux_cntrl    : in  std_logic_vector(3 downto 0);
    latch_enable : in  std_logic;
    enable_mgt   : in  std_logic;
    MGT_Commadet : in  std_logic;
    commdet_delay: out std_logic;
    data_in      : in  std_logic_vector(31 downto 0); 
    data_out     : out std_logic_vector(127 downto 0)
    
  
    );
  --attribute dont_touch : string;
  --attribute dont_touch of first_stage_synch : entity is "true|yes";  
   
    
    
    
end first_stage_synch;

architecture Behavioral of first_stage_synch is


signal temp0,temp1,temp2,temp3,data_int:std_logic_vector(32 downto 0):= (others => '0');
signal temp4,temp5,temp6:std_logic_vector(31 downto 0) := (others => '0') ;

signal data_out_int : std_logic_vector(127 downto 0);
signal mux_out:std_logic_vector(32 downto 0):= (others=>'0');


begin

 process (clk160) -- This register captures the first data from the rx mgt
 
   begin
    
         if clk160' event and clk160 ='1' then
          if enable_mgt ='1' then                  
             data_int <= MGT_Commadet & data_in;
            else 
             data_int <= data_int ; --(others =>'0');
      end if; 
      end if; 
         
 end process;



commdet_delay <= mux_out(32); 

Shifter_0: process (clk160)  -- First seven stages data shifter
   
     begin
            
               
          if clk160'event and clk160 ='1' then             
                 temp0 <= data_int;
                 temp1 <= temp0;
                 temp2 <= temp1;
                 temp3 <= temp2;
                                               
           end if;
        end process;               
  
Mux: process (clk160) --- This select output data of the shifted data with delay set by the mux_cntrl.   
         
         begin 
         
           if clk160'event and clk160 ='1' then 
            case mux_cntrl is
            
               when "0000" => mux_out <=  data_int;
               when "0001" => mux_out <=  temp0;
               when "0010" => mux_out <=  temp1;
               when "0011" => mux_out <=  temp2;
               when "0100" => mux_out <=  temp3;
               WHEN others => mux_out <=  (others =>'0');
            end case;
          end if;   
         end process; 
         
              
Shifter_1: process (clk160) -- Second six stages of  data shifter
            
              begin                    
                                            
                    if clk160'event and clk160 ='1' then 
                          temp4  <= mux_out(31 downto 0);
                          temp5  <= temp4;
                          temp6  <= temp5;
                                                                                         
                    end if;   
                               
                 end process;        
                                                      
 data_out_int <= mux_out(31 downto 0) & temp4 & temp5 & temp6 ;    --  Concatenate the shifted data in order to form 128 bits
 
 
Sipo_reg: process(clk160) -- latch when data ready 128 bits, waits 4 clocks of 160MHz 
              begin 

                  if clk160'event and clk160 ='1' then
                    
                    if latch_enable ='1' then 
                        data_out <= data_out_int(127 downto 0); 
        
                      end if;
                  end if;
           end process;  
       
   
  
  
       
end Behavioral;
