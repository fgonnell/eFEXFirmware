----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/24/2018 11:54:59 AM
-- Design Name: 
-- Module Name: d_type - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity d_type is
  Port ( 
  
  clk   : in std_logic;
  q     : out std_logic

  
  );
  
end d_type;

architecture Behavioral of d_type is

begin
         process (clk)
        variable d : std_logic := '0';
        begin
           if clk ' event and clk ='1' then
            d    := not d;
          end if ; 
          q <=d;    
   end process;
   
end Behavioral;
