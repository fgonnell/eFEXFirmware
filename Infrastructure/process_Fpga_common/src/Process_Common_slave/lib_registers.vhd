--! @file
--! @brief Version of the various firmware libraries
--! @details
--! Something here
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;
use ipbus_lib.ipbus_reg_types.all;
library infrastructure_lib;
use infrastructure_lib.ipbus_decode_efex_lib_version.all;

--! @copydoc lib_registers.vhd
entity lib_registers is
  
  port(
    ipb_clk             : in  std_logic;
    ipb_rst             : in  std_logic;
    ipb_in              : in  ipb_wbus;
    ipb_out             : out ipb_rbus;
    constraints_gitsha  : in  std_logic_vector(31 downto 0);
    constraints_version : in  std_logic_vector(31 downto 0);
    hog_gitsha          : in  std_logic_vector(31 downto 0);
    hog_version         : in  std_logic_vector(31 downto 0);
    top_version         : in  std_logic_vector(31 downto 0);
    top_gitsha          : in  std_logic_vector(31 downto 0);
    infra_version       : in  std_logic_vector(31 downto 0);
    infra_gitsha        : in  std_logic_vector(31 downto 0);
    algo_version        : in  std_logic_vector(31 downto 0);
    algo_gitsha         : in  std_logic_vector(31 downto 0);
    readout_version     : in  std_logic_vector(31 downto 0);
    readout_gitsha      : in  std_logic_vector(31 downto 0);
    ipbus_gitsha        : in  std_logic_vector(31 downto 0)
    );


end lib_registers;

--! @copydoc lib_registers.vhd
architecture Behavioral of lib_registers is

  signal sel          : integer;
  signal ack          : std_logic;
  signal ipbw         : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal ipbr, ipbr_d : ipb_rbus_array(N_SLAVES-1 downto 0);

begin


  fabric_lib_version : entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV      => N_SLAVES,  --defined in ipbus_decode_efex_libVersion
                SEL_WIDTH => ipbus_sel_width)
    port map(
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      sel             => ipbus_sel_efex_lib_version (ipb_in.ipb_addr),
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      ); 

  constraints : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,
      N_STAT => 2)
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_CONSTRAINTS),
      ipbus_out => ipbr(N_SLV_CONSTRAINTS),
      d         => (1 => constraints_gitsha, 0 => constraints_version),
      q         => open,
      stb       => open);

  hog : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,
      N_STAT => 2)
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_HOG),
      ipbus_out => ipbr(N_SLV_HOG),
      d         => (1 => hog_gitsha, 0 => hog_version),
      q         => open,
      stb       => open);            

  top_directory : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,
      N_STAT => 2)
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_TOP),
      ipbus_out => ipbr(N_SLV_TOP),
      d         => (1 => top_gitsha, 0 => top_version),
      q         => open,
      stb       => open);                                

  infra_lib : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,
      N_STAT => 2)
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_INFRASTRUCTURE),
      ipbus_out => ipbr(N_SLV_INFRASTRUCTURE),
      d         => (1 => infra_gitsha, 0 => infra_version),
      q         => open,
      stb       => open);               

  algo_lib : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,
      N_STAT => 2)
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_ALGORITHM),
      ipbus_out => ipbr(N_SLV_ALGORITHM),
      d         => (1 => algo_gitsha, 0 => algo_version),
      q         => open,
      stb       => open);       

  readout_lib : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,
      N_STAT => 2)
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_READOUT),
      ipbus_out => ipbr(N_SLV_READOUT),
      d         => (1 => readout_gitsha, 0 => readout_version),
      q         => open,
      stb       => open);       

  ipbus_a : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,
      N_STAT => 1)
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_IPBUS),
      ipbus_out => ipbr(N_SLV_IPBUS),
      d         => (0 => ipbus_gitsha),
      q         => open,
      stb       => open);       

end Behavioral;
