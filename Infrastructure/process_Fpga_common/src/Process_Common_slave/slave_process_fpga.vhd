-- This is the top level of the Slaves Ipbus  of the process FPGA.
-- The ipbus slaves live in this entity - modify according to requirements
-- Ports can be added to give ipbus slaves access to the chip top level.

-- Mohammed Siyad 20 January  2016

library IEEE;
use IEEE.STD_LOGIC_1164.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;
use ipbus_lib.all;

use work.spi.all;
use work.ipbus_decode_efex_infrastructure.all;
--! @copydoc slave_process_fpga.vhd
entity slaves is
  generic (FPGA_FLAVOUR : integer := 0);
  port(
    ipb_clk : in  std_logic;
    ipb_rst : in  std_logic;
    ipb_in  : in  ipb_wbus;
    ipb_out : out ipb_rbus;
    --- module status signals
    status  : in  std_logic_vector(31 downto 0);

    --- module control signals
    control_reg      : out std_logic_vector(31 downto 0);
    TOB_BCN_sych_reg : out std_logic_vector(31 downto 0);
    -- TOB delay
    tob_delay_reg    : out std_logic_vector(31 downto 0);
    tob_delay_status : in  std_logic_vector(31 downto 0) := (others => '0');
    -- TOB BC delay
    tob_bc_reg       : out std_logic_vector(31 downto 0);
    tob_bc_status    : in  std_logic_vector(31 downto 0) := (others => '0');
    --  reconfigure signal
    reconfig_reg     : out std_logic_vector(31 downto 0);
    --- flash signals
    flash_miso       : in  std_logic;
    flash_select     : out std_logic_vector(1 downto 0);
    flash_le         : out std_logic;
    flash_clko       : out std_logic;
    flash_mosi       : out std_logic
    );

end slaves;

architecture rtl of slaves is


  --constant NSLV: positive := 6;
  signal ipbw             : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal ipbr, ipbr_d     : ipb_rbus_array(N_SLAVES-1 downto 0);
  signal ctrl_pulse_reg   : std_logic_vector(63 downto 0);
  signal nc_0, nc_1, nc_2 : std_logic_vector(63 downto 0);


  signal flash_spi_in  : spi_mi;
  signal flash_spi_out : spi_mo;

  signal tob_delay_default, tob_bc_default : std_logic_vector(31 downto 0);

begin

-- spi flash pin assignment
  flash_spi_in.miso <= flash_miso;

  flash_mosi <= flash_spi_out.mosi;
  flash_le   <= flash_spi_out.le;
  flash_clko <= flash_spi_out.clk;

-- Default values for tob_delay_reg and tob_bc_reg (only meaningful when FPGA_FLAVOUR = 1 or 2...)

  tob_delay_default <= x"01131412" when FPGA_FLAVOUR = 1 else x"01120f10";  -- corresponding to 18,20,19 or 16,15,18
  tob_bc_default    <= x"07131412" when FPGA_FLAVOUR = 1 else x"071f101f";

  fabric : entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV      => N_SLAVES,  --defined in ipbus_decode_fpga_proc_common_registers
                SEL_WIDTH => ipbus_sel_width)
    port map(
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      sel             => ipbus_sel_efex_infrastructure (ipb_in.ipb_addr),
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      );


  module_control : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 1,              --number of control reg
      N_STAT => 1)              --number of status reg
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_MODULE_REG),
      ipbus_out => ipbr(N_SLV_MODULE_REG),
      d         => (0 => status),
      q(0)      => (control_reg),
--    qmask     => (others => '0'),
      stb       => open
      );

  tob_synch : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 1,              --number of control reg
      N_STAT => 0)              --number of status reg
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_TOB_SYNCH),
      ipbus_out => ipbr(N_SLV_TOB_SYNCH),
      d         => (others => (others => '0')),
      q(0)      => (TOB_BCN_sych_reg),
--    qmask     => (others => '0'),
      stb       => open
      );


  xadc : entity work.ipbus_xadc_drp  -- accessing and monitoring XADC

    port map(
      ipb_clk   => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_XADC),
      ipbus_out => ipbr(N_SLV_XADC)

      );

  reconfig : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 1,              --number of control reg
      N_STAT => 0)              --number of status reg
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_RECONFIGURE),
      ipbus_out => ipbr(N_SLV_RECONFIGURE),
      d         => (others => (others => '0')),
      q(0)      => (reconfig_reg),
      stb       => open
      );

  tob_bus_delay : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 1,              --number of control reg
      N_STAT => 1)              --number of status reg
    port map (
      clk             => ipb_clk,
      reset           => ipb_rst,
      ipbus_in        => ipbw(N_SLV_TOB_DELAY),
      ipbus_out       => ipbr(N_SLV_TOB_DELAY),
      d(0)            => tob_delay_status,
      q(0)            => tob_delay_reg,
      ctrl_default(0) => tob_delay_default,
      stb             => open
      );

  tob_bc_delay : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 1,              --number of control reg
      N_STAT => 1)              --number of status reg
    port map (
      clk             => ipb_clk,
      reset           => ipb_rst,
      ipbus_in        => ipbw(N_SLV_TOB_BC_DELAY),
      ipbus_out       => ipbr(N_SLV_TOB_BC_DELAY),
      d(0)            => tob_bc_status,
      q(0)            => tob_bc_reg,
      ctrl_default(0) => tob_bc_default,
      stb             => open
      );

  spi_flash : entity work.ipbus_spi32  -- spi controller
    generic map(
      BYTE_SPI   => true,
      ADDR_WIDTH => 9
      )
    port map(
      ipbus_clk => ipb_clk,
      reset     => ipb_rst,
      ipb_in    => ipbw(N_SLV_FLASH_SPI_RAM),
      ipb_out   => ipbr(N_SLV_FLASH_SPI_RAM),
      spi_in    => flash_spi_in,
      spi_out   => flash_spi_out,
      selreg    => flash_select

      );

  RAM : entity ipbus_lib.ipbus_ram  -- internal ram for testing the iPbus transactions.
    generic map(
      ADDR_WIDTH => 10
      )
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_RAM),
      ipbus_out => ipbr(N_SLV_RAM)
      );

end rtl;
