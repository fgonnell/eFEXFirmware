--! @file
--! @brief Data lignment module
--! @details
--! This is the Data lignment module
--! @author Ian Brawn
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
library algolib;
use algolib.AlgoDataTypes.all;
library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.all;
use TOB_rdout_lib.data_type_pkg.all;
library work;
use work.synch_type.all;
use work.EfexDataFormats.all;

--! @copydoc data_alignment.vhd
entity data_alignment is
  generic(n_channels  : natural := N_MGT;
          FPGA_NUMBER : integer );
  port (
    TTC_clk         : in  std_logic;
    clk280          : in  std_logic;
    rx_clk280       : in  std_logic_vector(n_channels-1 downto 0);
    MGT_Commadet    : in  std_logic_vector(n_channels-1 downto 0);
    enable_mgt      : in  std_logic_vector(n_channels-1 downto 0);
    BC_Reg_sel      : in  std_logic_vector(255 downto 0);
    mux_sel         : in  std_logic_vector(255 downto 0);
    sel_bcn_or_bc_cnt    : in std_logic;    -- selects between real data BC value and BC delay counter
    pseudo_orbit    : out std_logic;
    start           : in  std_logic;
    start_pulse_rst : out std_logic;
    rx_resetdone    : in  std_logic_vector(n_channels-1 downto 0);
    reset           : in  std_logic;
    DataToAlg       : out Algoinput;
    BCID            : out BCID_array;
    RAW_data        : out RAW_data_227_type;
    bc_cntr_0       : out std_logic_vector(111 downto 0);
    bc_cntr_1       : out std_logic_vector(111 downto 0);
    bc_cntr_2       : out std_logic_vector(111 downto 0);
    bc_cntr_3       : out std_logic_vector(111 downto 0);
    bc_mux_cntr_0   : out std_logic_vector(111 downto 0);
    bc_mux_cntr_1   : out std_logic_vector(111 downto 0);
    bc_mux_cntr_2   : out std_logic_vector(111 downto 0);
    bc_mux_cntr_3   : out std_logic_vector(111 downto 0);
    bcn_synch       : out std_logic_vector(63 downto 0);
    crc_error_chan  : out std_logic_vector(63 downto 0);
    -- for debug
    data_readout_0  : out std_logic_vector(223 downto 0);  -- for debug
    data_readout_1  : out std_logic_vector(223 downto 0);  -- for debug
    data_readout_2  : out std_logic_vector(223 downto 0);  -- for debug
    data_readout_3  : out std_logic_vector(223 downto 0);  -- for debug
    delay_num       : out std_logic_vector(255 downto 0);  -- for debug
    Reg224_latch    : out std_logic_vector(n_channels-1 downto 0);  -- for debug
    ttc_pipe        : out std_logic_vector(n_channels-1 downto 0);  -- for debug
    delay_latch     : out std_logic_vector(n_channels-1 downto 0);  -- for debug
    eFEXPosition    : in  std_logic_vector(31 downto 0);  --! Geographical position of eFEX module for dynamic mapping
    sel_data_in     : in  std_logic;
    kchar           : in  std_logic_vector(63 downto 0);
    ram_data        : in  mgt_data_in;
    MGT_data        : in  mgt_data_in;
    disperr_error   : in std_logic_vector (63 downto 0);  
    notable_error   : in std_logic_vector (63 downto 0)

    );
end data_alignment;

--! @copydoc data_alignment.vhd
architecture Behavioral of data_alignment is

  signal probe0           : std_logic_vector(101 downto 0);
  signal crc_error_chan_i : std_logic_vector(63 downto 0);
  signal MGT_Commadet_tmp : std_logic_vector(63 downto 0);

-------------------------------------

  signal data_in_int, ram_data_tmp     : mgt_data_in;
  signal dataout_after_mux : mgt_data_out;
  --signal data_out_reg224 : mgt_data_out;
  signal dataout_before_mux : mgt_data_out;

--signal data_in_int, data_out_int,data_out_reg224: data_path_array (n_channels-1 downto 0);     

  signal sel_reg, sel_mux                  : std_logic_vector (255 downto 0);
  signal crc_error_flag_tmp                : std_logic_vector (48 downto 0);
  signal reg1, reg2, reg3, start_pulse_280 : std_logic;

  signal temp0, temp1, temp2, temp3, start_40, pseudo_orbit_ref, pseudo_orbit_out                       : std_logic;
  signal bcn_0, bcn_1, bcn_2, bcn_3, bc_mux_out_0, bc_mux_out_1, bc_mux_out_2, bc_mux_out_3             : std_logic_vector(111 downto 0);
  signal bcn_tmp_0,bcn_tmp_1, bcn_tmp_2, bcn_tmp_3 ,bc_mux_tmp_0,bc_mux_tmp_1,bc_mux_tmp_2,bc_mux_tmp_3 : std_logic_vector(111 downto 0);
  signal aeqb_ch0, aeqb_ch1, aeqb_ch2, aeqb_ch3, mux_aeqb_ch0, mux_aeqb_ch1, mux_aeqb_ch2, mux_aeqb_ch3 : std_logic_vector(15 downto 0);

  signal MGT_Commadet_i : std_logic_vector(n_channels-1 downto 0);

  signal fpga      : integer;
  signal EmData    : FibreAllEm;
  signal HadData   : FibreAllHad;
  signal SpareData : FibreAllSpare; 
  type disp_notable_error_type is array( n_channels-1 downto 0) of  std_logic_vector(1 downto 0);
  signal disp_notable_error : disp_notable_error_type;
   type crc_error_40_type is array( n_channels-1 downto 0) of std_logic;
  signal    crc_error_40_i  : crc_error_40_type;
  
  --signal disperr_notable_error_i: disperr_notable_error;
 

--  ####### Mark signals  ########
    attribute keep : string;
  attribute max_fanout                   : integer;
  attribute keep of MGT_Commadet_i       : signal is "true";
  attribute max_fanout of MGT_Commadet_i : signal is 30;
  attribute keep of bc_cntr_0       : signal is "true";
  attribute keep of bc_cntr_1       : signal is "true";
  attribute keep of bc_cntr_2       : signal is "true";
  attribute keep of bc_cntr_3       : signal is "true";
  attribute keep of bc_mux_out_0       : signal is "true";
  attribute keep of bc_mux_out_1       : signal is "true";
  attribute keep of bc_mux_out_2       : signal is "true";
  attribute keep of bc_mux_out_3       : signal is "true";
--   #######################################


begin
  fpga <= FPGA_NUMBER;
  
-- for debugging
  data_readout_0 <= dataout_before_mux(0)( 223 downto 0);
  data_readout_1 <= dataout_before_mux(1)( 223 downto 0);
  data_readout_2 <= dataout_before_mux(2)( 223 downto 0);
  data_readout_3 <= dataout_before_mux(3)( 223 downto 0);
  pseudo_orbit   <= pseudo_orbit_out;

----------------------------------------------------------------------------------------------------
-- bc data before the mux bits asignment
---------------------------------------------------------------------------------------------------- 
  bcn_gen : for i in 0 to (15)
  generate
    bcn_0(i*7+6 downto 7*i) <= MGT_to_SuperCells(dataout_before_mux(i+3*i)  ).BCID(6 downto 0);  
    bcn_1(i*7+6 downto 7*i) <= MGT_to_SuperCells(dataout_before_mux(i+1+3*i)).BCID(6 downto 0);  
    bcn_2(i*7+6 downto 7*i) <= MGT_to_SuperCells(dataout_before_mux(i+2+3*i)).BCID(6 downto 0);  
    bcn_3(i*7+6 downto 7*i) <= MGT_to_SuperCells(dataout_before_mux(i+3+3*i)).BCID(6 downto 0);  
  end generate bcn_gen;


--------------------------------------------------------------------------------------------------
--- Create bc alignment blocks for each mgt 64 before the mux
--------------------------------------------------------------------------------------------------
  bc_alignment_before_mux : for i in 0 to 15
  generate
    bc_align_a : entity work.quad_bc_alignment
      port map (
        reset     => reset,
        clk       => TTC_clk,
        start     => start_40,  -- start pulse for the pseudo orbit state machines
        ref_orbit => pseudo_orbit_ref,  -- pseudo orbit reference signal for all channels
        bcn_0     => bcn_0(7*i+6 downto 7*i),
        bcn_1     => bcn_1(7*i+6 downto 7*i),
        bcn_2     => bcn_2(7*i+6 downto 7*i),
        bcn_3     => bcn_3(7*i+6 downto 7*i),
        aeqb_ch0  => aeqb_ch0(i),
        aeqb_ch1  => aeqb_ch1(i),
        aeqb_ch2  => aeqb_ch2(i),
        aeqb_ch3  => aeqb_ch3(i),
        cntr_ch0  => bcn_tmp_0(7*i+6 downto 7*i), 
        cntr_ch1  => bcn_tmp_1(7*i+6 downto 7*i), 
        cntr_ch2  => bcn_tmp_2(7*i+6 downto 7*i), 
        cntr_ch3  => bcn_tmp_3(7*i+6 downto 7*i) 
        );

  end generate bc_alignment_before_mux;

bcn_befor_mux : process (TTC_clk)  
   begin    
     if rising_edge(TTC_clk) then    -- selects between real data BC value and BC delay counter to IPBUS
        if sel_bcn_or_bc_cnt = '1' then    -- send real data BC value to IPBUS
            bc_cntr_0 <= bcn_0 ;
            bc_cntr_1 <= bcn_1 ;
            bc_cntr_2 <= bcn_2 ;
            bc_cntr_3 <= bcn_3 ;
        else    -- send out the  BC delay counter to IPBUS
            bc_cntr_0 <= bcn_tmp_0 ;
            bc_cntr_1 <= bcn_tmp_1 ;
            bc_cntr_2 <= bcn_tmp_2 ;
            bc_cntr_3 <= bcn_tmp_3 ;
        end if;
     end if; 
   end process;        

------------------------------------------------------------------------------------------------------------------
---  generate the reference pseudo orbit fo all the mgts, in this instance first quad mgt0 was used as reference.
-----------------------------------------------------------------------------------------------------------------
  orbit_ref_pseudo : entity work.pseudo_orbit_gen
    port map (
      clk          => TTC_clk,
      bcn          => bcn_0(4 downto 0),
      pseudo_orbit => pseudo_orbit_ref
      );



-----------------------------------------------------------------------------------------   
---- bc data after the bc mux 
----------------------------------------------------------------------------------------                                   

  
  bcn_mux_gen : for i in 0 to (15)
  generate
    bc_mux_out_0(7*i+6 downto 7*i) <= MGT_to_SuperCells(dataout_after_mux(i+3*i)   ).BCID(6 downto 0);  
    bc_mux_out_1(7*i+6 downto 7*i) <= MGT_to_SuperCells(dataout_after_mux(i+1+3*i) ).BCID(6 downto 0);  
    bc_mux_out_2(7*i+6 downto 7*i) <= MGT_to_SuperCells(dataout_after_mux(i+2+3*i) ).BCID(6 downto 0);  
    bc_mux_out_3(7*i+6 downto 7*i) <= MGT_to_SuperCells(dataout_after_mux(i+3+3*i) ).BCID(6 downto 0);  
  end generate bcn_mux_gen;

--------------------------------------------------------------------------------------                                  
---- Create bc alignment blocks for each mgt 64 after the mux   
---------------------------------------------------------------------------------------                           
  bc_alignment_after_mux : for i in 0 to 15
  generate
    bc_align_b : entity work.quad_bc_alignment
      port map (
        reset     => reset,
        clk       => TTC_clk,
        start     => start_40,  -- start pulse for the pseudo orbit state machines
        ref_orbit => pseudo_orbit_out,  -- pseudo orbit reference signal for all channels
        bcn_0     => bc_mux_out_0(7*i+6 downto 7*i),
        bcn_1     => bc_mux_out_1(7*i+6 downto 7*i),
        bcn_2     => bc_mux_out_2(7*i+6 downto 7*i),
        bcn_3     => bc_mux_out_3(7*i+6 downto 7*i),
        aeqb_ch0  => mux_aeqb_ch0(i),
        aeqb_ch1  => mux_aeqb_ch1(i),
        aeqb_ch2  => mux_aeqb_ch2(i),
        aeqb_ch3  => mux_aeqb_ch3(i),
        cntr_ch0  => bc_mux_tmp_0(7*i+6 downto 7*i),
        cntr_ch1  => bc_mux_tmp_1(7*i+6 downto 7*i),
        cntr_ch2  => bc_mux_tmp_2(7*i+6 downto 7*i),
        cntr_ch3  => bc_mux_tmp_3(7*i+6 downto 7*i)
        );
  end generate bc_alignment_after_mux;
  
  bcn_after_mux : process (TTC_clk)  
   begin    
     if rising_edge(TTC_clk) then    -- selects between real data BC value and BC delay counter to IPBUS
        if sel_bcn_or_bc_cnt = '1' then    -- send real data BC value to IPBUS
            bc_mux_cntr_0 <= bc_mux_out_0 ;
            bc_mux_cntr_1 <= bc_mux_out_1 ;
            bc_mux_cntr_2 <= bc_mux_out_2 ;
            bc_mux_cntr_3 <= bc_mux_out_3 ;
        else    -- send out the  BC delay counter to IPBUS
            bc_mux_cntr_0 <= bc_mux_tmp_0 ;
            bc_mux_cntr_1 <= bc_mux_tmp_1 ;
            bc_mux_cntr_2 <= bc_mux_tmp_2 ;
            bc_mux_cntr_3 <= bc_mux_tmp_3 ;
        end if;
     end if; 
   end process; 

---------------------------------------------------------------------------------------------------------------------   
  ---  Generate the reference pseudo orbit fo all the mgts,. in this instance first quad mgt0 was used as reference.
  --------------------------------------------------------------------------------------------------------------------
  orbit_ref_pseudo_b : entity work.pseudo_orbit_gen
    port map (
      clk          => TTC_clk,
      bcn          => bc_mux_out_0(4 downto 0),
      pseudo_orbit => pseudo_orbit_out
      );

---------------------------------------------------------                                   
-- Generates start pulse for the bc alignment block.                                 
--------------------------------------------------------
  start_pulse_40 : process (TTC_clk)
  begin
    if TTC_clk ' event and TTC_clk = '1' then
      temp0    <= start;
      temp1    <= temp0;
      temp2    <= temp1;
      start_40 <= temp1 and not temp2;
    end if;
  end process;
  start_pulse_rst <= start_40;

---------------------------------------------------------------------------------------
-- asignment of the output of the MGT (rx data) to the input of the synchronising logic
--------------------------------------------------------------------------------------- 


--------------------------------------------------------------------------------------------------------------------------
---- synchronisation logic block that generates n-channels of synchronisations for n_channels of MGTs
--------------------------------------------------------------------------------------------------------------------------
  synch_gen : for i in 0 to (n_channels-1)
  generate
    u0 : entity work.top_synch
      port map (
        rx_clk280           => rx_clk280(i),
        TTC_clk               => TTC_clk,
        reset                  => reset,
        enable_mgt        => enable_mgt(i),
        delay_latch         => delay_latch(i),
        delay_num           => delay_num (i+3+3*i downto i+3*i),
        ttc_pipe               => ttc_pipe(i),
        reg_sel                => sel_reg (i+3+3*i downto i+3*i),
        mux_sel                => sel_mux (i+3+3*i downto i+3*i),
        MGT_Commadet    => MGT_Commadet_i(i),  --! comma detected for incoming data
        start                     => start_pulse_280,  --! enables the synchronising logic
        rx_resetdone        => rx_resetdone(i),
        reg224_latch        => reg224_latch(i),
        bcn_Synch             => bcn_synch(i),  --! outputs orbit generated bcn after mux. set high if bcn = 11111
        data_out_reg224   => dataout_before_mux(i)(223 downto 0),  -- data before mux 
        data_out                => dataout_after_mux(i)(223 downto 0),   --! output data 224 bit after the mux  of the synchronised data wth the 40 MHz clock.
        disp_notable_error => dataout_after_mux(i)(225 downto 224), -- two bit errors
        data_in                  => data_in_int(i),  --! incoming data of 32 bits from the Rx of the MGT
        disperr_error         => disperr_error(i), --! disperr errors in the MGT
        notable_error         => notable_error(i),  --! not in the table error in the  MGT
        crc_error                => dataout_after_mux(i)(226),
        crc_error_40          =>  crc_error_40_i(i)
       
        
        );

--error_out(i) <=  crc_error_chan_i(i) & lock_pll(i) & disperr_error(i) & notable_error(i) ;


    --- crc generation
    u1 : entity work.crc_checker
      port map (
        clk               => rx_clk280(i),
        reset            => reset,
        mgt_commdet => MGT_Commadet_i(i),
        rxdata          => data_in_int(i),
        crc_error      => crc_error_chan_i(i) , --data_out_reg224(i)(226)
        crc_error_40 =>  crc_error_40_i(i) --   dataout_after_mux(i)(226)
        );
        
   --dataout_after_mux(i)(226) <=  crc_error_chan_i(i) ;
    
    u2_CLK_MUX : process (rx_clk280(i))
    begin
      if rx_clk280(i) ' event and rx_clk280(i) = '1' then
          ram_data_tmp(i) <= ram_data(i) ;
          MGT_Commadet_tmp(i) <= kchar(i);
--        data_in_int(i)    <= ram_data(i) when sel_data_in = '1' else MGT_data(i);
--        MGT_Commadet_i(i) <= kchar(i)    when sel_data_in = '1' else MGT_Commadet(i);
      end if;
    end process;

        data_in_int(i)    <= ram_data_tmp(i)      when sel_data_in = '1' else MGT_data(i);
        MGT_Commadet_i(i) <= MGT_Commadet_tmp(i)  when sel_data_in = '1' else MGT_Commadet(i);
  end generate synch_gen;

  crc_error_chan <= crc_error_chan_i;


  sel_reg <= BC_Reg_sel;
  sel_mux <= mux_sel;
  
gen_crc : for i in 0 to 48 generate
   crc_error_flag_tmp(i) <= dataout_after_mux(i)(226) ;
   end generate gen_crc;

--! Pipe line for the start signal in 280m clock domain and make it as a pulse
  start_pulse : process (clk280)
  begin
    if rising_edge (clk280) then
      Reg1            <= start;
      Reg2            <= Reg1;
      Reg3            <= Reg2;
      start_pulse_280 <= Reg2 and not Reg3;
    end if;
  end process;

-----------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
  fibre_map : entity work.fibremap_block
    generic map (FPGA_NUMBER => FPGA_NUMBER)
    port map (
      clk                => TTC_clk,
      eFEXPosition_in => eFEXPosition,
      DataToAlg_out    =>  DataToAlg,
      BCID_out             =>  BCID,
      MGT_data_in     => dataout_after_mux ,
      RAW_data_out   => RAW_data ) ; 

end Behavioral;
