-------------------------------------------------------------------------------
-- Title      : Testbench for design "TopSortingModule"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : TopSortingModule_tb.vhd
-- Author     : Francesco Gonnella  <fg@epldt040.ph.bham.ac.uk>
-- Company    : 
-- Created    : 2017-02-23
-- Last update: 2017-02-23
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2017 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2017-02-23  1.0      fg      Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library algolib;
use algolib.DataTypes.all;
use algolib.AlgoDataTypes.all;

-------------------------------------------------------------------------------

entity TopSortingModule_tb is

end TopSortingModule_tb;

-------------------------------------------------------------------------------

architecture std of TopSortingModule_tb is

  component TopSortingModule
    generic (
      STAGE : integer);
    port (
      CLK400      : in  std_logic;
      OUT_Version : out AlgoRegister;
      IN_Control  : in  AlgoRegister;
      OUT_Status  : out AlgoRegister;
      IN_Start    : in  std_logic;
      IN_Data     : in  TriggerObjects((2**STAGE)-1 downto 0);
      OUT_Start   : out std_logic;
      OUT_Write   : out std_logic;
      OUT_Data    : out TriggerObject);
  end component;

  -- component generics
  constant STAGE : integer := 3;

  -- component ports
  signal CLK400      : std_logic;
  signal OUT_Version : AlgoRegister;
  signal IN_Control  : AlgoRegister                          := (others => '0');
  signal OUT_Status  : AlgoRegister;
  signal IN_Start    : std_logic                             := '0';
  signal IN_Data     : TriggerObjects((2**STAGE)-1 downto 0) := (others => (others => '0'));
  signal OUT_Start   : std_logic;
  signal OUT_Write   : std_logic;
  signal OUT_Data    : TriggerObject;

  -- clock
  signal Clk : std_logic := '1';

begin  -- std

  -- component instantiation
  DUT : TopSortingModule
    generic map (
      STAGE => STAGE)
    port map (
      CLK400      => Clk,
      OUT_Version => OUT_Version,
      IN_Control  => IN_Control,
      OUT_Status  => OUT_Status,
      IN_Start    => IN_Start,
      IN_Data     => IN_Data,
      OUT_Start   => OUT_Start,
      OUT_Write   => OUT_Write,
      OUT_Data    => OUT_Data);

  -- clock generation
  Clk <= not Clk after 1.125 ns;

  -- waveform generation
  WaveGen_Proc : process
  begin
    -- insert signal assignments here
    wait for 50 ns;

--    wait until Clk ='0'; wait for 0.5 ns;
-------------------------------------------------------------------------------
    wait until Clk = '1';
    IN_Start   <= '1';
    IN_Data(0) <= x"10000a0f";
    IN_Data(1) <= x"20000a0d";
    IN_Data(2) <= x"30000a0b";
    IN_Data(3) <= x"40000a09";
    IN_Data(4) <= x"50000a07";
    IN_Data(5) <= x"60000a05";
    IN_Data(6) <= x"70000a03";
    IN_Data(7) <= x"80000a01";
--    wait until Clk ='0'; wait for 0.5 ns;
    wait until Clk = '1';

    IN_Start   <= '0';
    IN_Data(0) <= x"1100090e";
    IN_Data(1) <= x"2100090c";
    IN_Data(2) <= x"3100090a";
    IN_Data(3) <= x"41000908";
    IN_Data(4) <= x"51000906";
    IN_Data(5) <= x"61000904";
    IN_Data(6) <= x"71000902";
    IN_Data(7) <= x"81000900";
--    wait until Clk ='0'; wait for 0.5 ns;
    wait until Clk = '1';

    IN_Start   <= '0';
    IN_Data(0) <= x"1200080f";
    IN_Data(1) <= x"2200080d";
    IN_Data(2) <= x"3200080b";
    IN_Data(3) <= x"42000809";
    IN_Data(4) <= x"52000807";
    IN_Data(5) <= x"62000805";
    IN_Data(6) <= x"72000803";
    IN_Data(7) <= x"82000801";
--    wait until Clk = '0'; wait for 0.5 ns;
    wait until Clk = '1';

    IN_Start   <= '0';
    IN_Data(0) <= x"1300071e";
    IN_Data(1) <= x"2300071c";
    IN_Data(2) <= x"3300071a";
    IN_Data(3) <= x"43000718";
    IN_Data(4) <= x"53000716";
    IN_Data(5) <= x"63000714";
    IN_Data(6) <= x"73000712";
    IN_Data(7) <= x"83000710";
--    wait until Clk = '0'; wait for 0.5 ns;
    wait until Clk = '1';

    IN_Start   <= '0';
    IN_Data(0) <= x"1400062f";
    IN_Data(1) <= x"2400062d";
    IN_Data(2) <= x"3400062b";
    IN_Data(3) <= x"44000629";
    IN_Data(4) <= x"54000627";
    IN_Data(5) <= x"64000625";
    IN_Data(6) <= x"74000623";
    IN_Data(7) <= x"84000621";
--    wait until Clk = '0'; wait for 0.5 ns;
    wait until Clk = '1';

    IN_Start   <= '0';
    IN_Data(0) <= x"10000000";
    IN_Data(1) <= x"20000000";
    IN_Data(2) <= x"30000000";
    IN_Data(3) <= x"40000000";
    IN_Data(4) <= x"50000000";
    IN_Data(5) <= x"60000000";
    IN_Data(6) <= x"70000000";
    IN_Data(7) <= x"80000000";
-------------------------------------------------------------------------------

    wait for 2.5*20 ns;

-------------------------------------------------------------------------------
    wait until Clk = '1';
    IN_Start   <= '1';
    IN_Data(0) <= x"1000000f";
    IN_Data(1) <= x"20000005";
    IN_Data(2) <= x"30000000";
    IN_Data(3) <= x"40000000";
    IN_Data(4) <= x"50000000";
    IN_Data(5) <= x"60000001";
    IN_Data(6) <= x"70000000";
    IN_Data(7) <= x"80000000";
--    wait until Clk ='0'; wait for 0.5 ns;
    wait until Clk = '1';

    IN_Start   <= '0';
    IN_Data(0) <= x"1100000e";
    IN_Data(1) <= x"21000004";
    IN_Data(2) <= x"31000000";
    IN_Data(3) <= x"41000000";
    IN_Data(4) <= x"51000000";
    IN_Data(5) <= x"61000000";
    IN_Data(6) <= x"71000000";
    IN_Data(7) <= x"81000000";
--    wait until Clk ='0'; wait for 0.5 ns;
    wait until Clk = '1';

    IN_Start   <= '0';
    IN_Data(0) <= x"12000002";
    IN_Data(1) <= x"22000000";
    IN_Data(2) <= x"32000000";
    IN_Data(3) <= x"42000000";
    IN_Data(4) <= x"52000000";
    IN_Data(5) <= x"62000000";
    IN_Data(6) <= x"72000000";
    IN_Data(7) <= x"82000000";
--    wait until Clk = '0'; wait for 0.5 ns;
    wait until Clk = '1';

    IN_Start   <= '0';
    IN_Data(0) <= x"13000000";
    IN_Data(1) <= x"23000000";
    IN_Data(2) <= x"33000000";
    IN_Data(3) <= x"43000000";
    IN_Data(4) <= x"53000000";
    IN_Data(5) <= x"63000000";
    IN_Data(6) <= x"73000000";
    IN_Data(7) <= x"83000000";
--    wait until Clk = '0'; wait for 0.5 ns;
    wait until Clk = '1';

    IN_Start   <= '0';
    IN_Data(0) <= x"14000000";
    IN_Data(1) <= x"24000000";
    IN_Data(2) <= x"34000000";
    IN_Data(3) <= x"44000000";
    IN_Data(4) <= x"54000000";
    IN_Data(5) <= x"64000000";
    IN_Data(6) <= x"74000000";
    IN_Data(7) <= x"84000000";
--    wait until Clk = '0'; wait for 0.5 ns;
    wait until Clk = '1';

    IN_Start   <= '0';
    IN_Data(0) <= x"10000000";
    IN_Data(1) <= x"20000000";
    IN_Data(2) <= x"30000000";
    IN_Data(3) <= x"40000000";
    IN_Data(4) <= x"50000000";
    IN_Data(5) <= x"60000000";
    IN_Data(6) <= x"70000000";
    IN_Data(7) <= x"80000000";
-------------------------------------------------------------------------------

    wait for 2.5*20 ns;

-------------------------------------------------------------------------------
    wait until Clk = '1';
    IN_Start   <= '1';
    IN_Data(0) <= x"10000000";
    IN_Data(1) <= x"20000a0d";
    IN_Data(2) <= x"30000a0b";
    IN_Data(3) <= x"40000a09";
    IN_Data(4) <= x"50000007";
    IN_Data(5) <= x"60000005";
    IN_Data(6) <= x"70000003";
    IN_Data(7) <= x"80000000";
--    wait until Clk ='0'; wait for 0.5 ns;
    wait until Clk = '1';

    IN_Start   <= '0';
    IN_Data(0) <= x"11000000";
    IN_Data(1) <= x"2100090c";
    IN_Data(2) <= x"3100090a";
    IN_Data(3) <= x"41000908";
    IN_Data(4) <= x"51000006";
    IN_Data(5) <= x"61000004";
    IN_Data(6) <= x"71000002";
    IN_Data(7) <= x"81000000";
--    wait until Clk ='0'; wait for 0.5 ns;
    wait until Clk = '1';

    IN_Start   <= '0';
    IN_Data(0) <= x"12000000";
    IN_Data(1) <= x"2200080d";
    IN_Data(2) <= x"3200080b";
    IN_Data(3) <= x"42000809";
    IN_Data(4) <= x"52000005";
    IN_Data(5) <= x"62000003";
    IN_Data(6) <= x"72000001";
    IN_Data(7) <= x"82000000";
--    wait until Clk = '0'; wait for 0.5 ns;
    wait until Clk = '1';

    IN_Start   <= '0';
    IN_Data(0) <= x"13000000";
    IN_Data(1) <= x"23000000";
    IN_Data(2) <= x"3300071a";
    IN_Data(3) <= x"43000718";
    IN_Data(4) <= x"53000000";
    IN_Data(5) <= x"63000000";
    IN_Data(6) <= x"73000000";
    IN_Data(7) <= x"83000000";
--    wait until Clk = '0'; wait for 0.5 ns;
    wait until Clk = '1';

    IN_Start   <= '0';
    IN_Data(0) <= x"14000000";
    IN_Data(1) <= x"24000000";
    IN_Data(2) <= x"34000000";
    IN_Data(3) <= x"44000629";
    IN_Data(4) <= x"54000000";
    IN_Data(5) <= x"64000000";
    IN_Data(6) <= x"74000000";
    IN_Data(7) <= x"84000000";
--    wait until Clk = '0'; wait for 0.5 ns;
    wait until Clk = '1';

    IN_Start   <= '0';
    IN_Data(0) <= x"10000000";
    IN_Data(1) <= x"20000000";
    IN_Data(2) <= x"30000000";
    IN_Data(3) <= x"40000000";
    IN_Data(4) <= x"50000000";
    IN_Data(5) <= x"60000000";
    IN_Data(6) <= x"70000000";
    IN_Data(7) <= x"80000000";
-------------------------------------------------------------------------------



    wait for 200 ns;
    
  end process WaveGen_Proc;

  

end std;

-------------------------------------------------------------------------------

--configuration TopSortingModule_tb_std_cfg of TopSortingModule_tb is
--  for std
--  end for;
--end TopSortingModule_tb_std_cfg;

-------------------------------------------------------------------------------
