----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/10/2016 11:38:56 AM
-- Design Name: 
-- Module Name: delay_mux - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
LIBRARY xil_defaultlib;
--use xil_defaultlib.synch_type.all;

entity delay_mux is
  Port (
     delay_cnt : in Std_logic_vector (3 downto 0 );
     mux_delay : out Std_logic_vector (3 downto 0 )
     );
     
end delay_mux;

architecture Behavioral of delay_mux is

begin
         process (delay_cnt)
          begin 
             case delay_cnt is
               when "0001" => mux_delay <="0001";
               when "0010" => mux_delay <="0001";
               when "0011" => mux_delay <="0100";
               when "0100" => mux_delay <="0110";
               when "0101" => mux_delay <="0001";
               when others => mux_delay <="0001";
            end case;
         end process;   
end Behavioral;
