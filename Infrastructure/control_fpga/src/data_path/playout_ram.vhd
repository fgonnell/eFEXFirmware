
-- RAM reflector
--
-- Dave Sankey, August 2019

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity playout_ram is
  generic (
 	ADDR_WIDTH  : integer :=   12	-- width of ram address bus
 	);
  port (
	clk					: in std_logic;
-- RAM signals
    RAM_data			: out std_logic_vector (63 DOWNTO 0);
    RAM_addr			: in std_logic_vector(ADDR_WIDTH - 1 downto 0);
    RAM_din				: in std_logic_vector (63 DOWNTO 0);
    RAM_we				: in STD_LOGIC
  );
end playout_ram;

architecture simple of playout_ram is

signal cached_value : std_logic_vector (63 DOWNTO 0) := (Others => '0');

begin

ram: Process(clk)
  Variable octet_high, octet_low: std_logic_vector(7 downto 0) := (Others => '0');
  Begin
    If rising_edge(clk) then
      octet_low := RAM_addr(6 downto 0) & '0';
      octet_high := RAM_addr(6 downto 0) & '1';
      RAM_data <= octet_high & octet_high & octet_high & octet_high & octet_low & octet_low & octet_low & octet_low
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      if RAM_we = '1' then
        cached_value <= RAM_din
-- pragma translate_off
        after 2 ns
-- pragma translate_on
        ;
      end if;
    end if;
  End process ram;

end simple;
