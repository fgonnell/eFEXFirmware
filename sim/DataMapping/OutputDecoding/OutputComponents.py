"""
Created on Apr 23 2020
@author: Alexios Stampekis
Module containing a function that decodes a line where a word value appears
AND a function that defines the Input pattern to compare with.
The Input pattern follows the "protocol" of the Input Generator scripts, and 
is stored in a 3D (BC-MGT-WORD) numpy array for efficient comparison (access to
elements)
"""

import numpy as np

def takeFirstEl(elem):
    """
    Just an auxiliary function. Key function for sorting list.
    It makes sure we access the 0th element
    of an element (in RealMap list where this funct.
    is used, this el. is a tuple) in a list.  
    """
    return elem[0]

def takeSecondEl(elem):
    """
    Auxiliary function. Key function for sorting list.
    Makes sure we access the element 1 of a 
    list element. Used to sort list by value of
    2nd element of their components (for us tuples)
    """
    return elem[1]

def decoder(i, line, val):
    """
    We decode a line of the output_file.txt.
    There is a hex val in the line, and we 
    want its position (supercell)    
    """
    #BC=int(i/61)
    wflag=0
    tt=str(i%61).zfill(2)
    pos=line.find(val)  
    
    point=line.index(val)+1
    if val in line.strip()[point:]:
        print('# ** Warning! In line', i, '. Same words found!')
        wflag+=1

    sc='s'               #supercell initialization
    if pos<=3:
        sc='0'
    elif pos<=20:
        sc='1'
        for j in range(2, 6):
            if pos<=j*4:    
                sc=sc+str(j-1)
                break
    elif pos<=37:
        sc='2'
        for j in range(6, 10):
            if pos<=j*4:
                sc=sc+str(j-5)
                break
    elif pos<=42:
        sc='3'
    else:
        sc='H'
    
    return (tt+sc, wflag)


def InputArray(version):
    """
    Here we define a 3D numpy array that stores all the information from the input,
    meaning the BC-MGT-WORD info (not the crc or quality of the BC though!).
    We follow the definitions from InputsGenPattern*.py for a specific data pattern.
    Version must be: 1, 2, 3 only! (for now!).
    We call the array InW (abbreviation for InputWords).
    """
    
    if version==1:
        
        Nbc = 1280
        InW = np.zeros( (Nbc, 64, 20), dtype=np.uint16 )  # arguments are BCs, MGTs, WORDs
        print ("# Input Array has", InW.ndim, "dimensions: BCs-MGTs-WORDS, with shape", InW.shape, "\n")
        
        for i in range (64):
            for j in range(0+i*20, 20*(i+1)):
                InW[j, i, j-i*20]=1            #I suppose that user will take care not to use the same word value more than once per BC!
        
        #InW[0, 0, 1]=1                        #play with providing duplicates! use for test only!
        #InW[1280, 63, 19]=1                   #duplicate the last BC? No need to, since we are implying BCreal counter
        return InW

        
    if version==2:
        
        Nbc = 64
        InW = np.zeros( (Nbc, 64, 20), dtype=np.uint16 )
        print ("# Input Array has", InW.ndim, "dimensions: BCs-MGTs-WORDS, with shape", InW.shape, "\n")
        
        for i in range(64):  #we fill mgt_n with words 1,2...20 for bc=n
            for j in range(20):
                InW[i, i, j]=j+1
        #InW[0,0,1]=1             #play with providing duplicates! use for test only!
        return InW            
        
    
    if version==3:
        
        Nbc = 40
        InW = np.zeros( (Nbc, 64, 20), dtype=np.uint16 )
        print ("# Input Array has", InW.ndim,"dimensions: BCs-MGTs-WORDS, with shape", InW.shape, "\n")
        
        InW[20, 0, 14]=1
        #InW[20, 1, 12]=1   #test warning about duplicate words in a single BC
        
        return InW
    
    
    if version>3:
        print("# * Warning: no input set!!")
        
