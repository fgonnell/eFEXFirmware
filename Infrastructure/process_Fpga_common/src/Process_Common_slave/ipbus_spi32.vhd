-- ipbus interface to SPI engine for FLASH memory and PLL chips on FTM 

-- Designer: Richard Staley
-- Date: 18th May. 2017

-- module: ipbus_spi32.vhd

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;
use work.spi.all;
use ipbus_lib.ipbus_reg_types.all;

entity ipbus_spi32 is
	generic(
      BYTE_SPI:  boolean := FALSE;
      -- SPI i/o granularity. Default is 32bits for PLL. Set TRUE for FLASH.
      ADDR_WIDTH: natural 
      -- = Width of whole interface. Includes control reg, input and output RAM.
      );
port (
		ipbus_clk: in std_logic;
		reset: in std_logic;
		ipb_in: in ipb_wbus;
		ipb_out: out ipb_rbus;
		spi_in    : in spi_mi;
		spi_out   : out spi_mo;
		selreg      : out  std_logic_vector(1 downto 0)
	 );
	 
attribute keep_hierarchy : string;
attribute keep_hierarchy of ipbus_spi32 : entity is "yes";

end ipbus_spi32 ;

architecture rtl of ipbus_spi32 is


signal ram_ptr : std_logic_vector(ADDR_WIDTH - 3 downto 0);-- An index for both in and out ram
signal ram_write: std_logic;

signal outgoing_data, incoming_data: std_logic_vector(31 downto 0);

constant  DIV_BITS: natural := 2;  

signal spi_clk: std_logic := '0';
signal clk_en: std_logic := '0';

constant NSLV: positive := 4;
signal ipbw: ipb_wbus_array(NSLV-1 downto 0);
signal ipbr, ipbr_d: ipb_rbus_array(NSLV-1 downto 0);

signal spi_ctrl: ipb_reg_v(3 downto 0);
signal spi_stat: ipb_reg_v(0 downto 0);


signal transfer_count : std_logic_vector( ADDR_WIDTH  downto 0); 
    -- = number of bytes if BYTE_SPI=TRUE else = number of 32-bit words

signal do_spi,run_spi,busy	:  std_logic;


--attribute PRESERVE_SIGNAL : boolean;
--attribute PRESERVE_SIGNAL of spi_clk: signal is true;

begin

block_decode: entity work.ipbus_fabric_branch
    generic map(
    NSLV => NSLV,
    DECODE_BASE => ADDR_WIDTH -2
    )
	port map(
        ipb_in => ipb_in,
        ipb_out => ipb_out,
        ipb_to_slaves => ipbw,
        ipb_from_slaves => ipbr
    );

clock_div:	process(reset, ipbus_clk)
	variable div : std_logic_vector(DIV_BITS downto 1);
	begin
	if reset = '1' then
		div:=(others => '0');
	elsif rising_edge(ipbus_clk) then
		spi_clk <= div(DIV_BITS);
		div:=div+1;
	end if;
	end process;


spi_control: entity ipbus_lib.ipbus_ctrlreg_v
        generic map(
			N_CTRL => 4,
            N_STAT => 1
            )
        port map(
            clk => ipbus_clk,
            reset => reset,
            ipbus_in => ipbw(0),
            ipbus_out => ipbr(0),
            d => spi_stat,
            q => spi_ctrl
        );
    
        selreg   <= spi_ctrl(0)(1 downto 0);              -- base address + 0
        transfer_count <= spi_ctrl(2)((ADDR_WIDTH )  downto 0); -- 2
        do_spi     <= spi_ctrl(3)(0);                                     -- 3
        
        spi_stat(0) <= X"0000000" & "000" & busy;                        -- 4


-- arbitration register with key, lock and watchdog timer
arbitration: entity work.ipbus_watchdog
	generic map(
    TIMER_WIDTH => 20
   )
    port map(
        ipbus_clk => ipbus_clk,
        reset => reset,
        ipbus_in => ipbw(1),
        ipbus_out => ipbr(1)
     );


-- dpram shares address space with control, arbitration and the other dpram, hence the '-2' width
spi_dpram_out: entity work.ipbus_dpram_flash -- to SPI
	generic map(
		ADDR_WIDTH => ADDR_WIDTH -2
	   )
	port map(
		clk => ipbus_clk,
		rst => reset,
		ipb_in => ipbw(2),
		ipb_out => ipbr(2),
		rclk => spi_clk,
		we => '0',--ram_write,
		d => (others => '0'),--incoming_data,
		q => outgoing_data, 
		addr => ram_ptr--pointer_addr
	);

spi_dpram_in: entity work.ipbus_dpram_flash -- from SPI
        generic map(
            ADDR_WIDTH => ADDR_WIDTH -2
           )
        port map(
            clk => ipbus_clk,
            rst => reset,
            ipb_in => ipbw(3),
            ipb_out => ipbr(3),
            rclk => spi_clk,
            we => ram_write,
            d => incoming_data,
            q => open,--outgoing_data, 
            addr => ram_ptr--pointer_addr
        );

synch: entity work.command_sync
    port map (	
        ipb_clk     => ipbus_clk,
        out_clk     => spi_clk,
		reset       => reset,
		req         => do_spi, -- on 0 to 1 transition
		ack         => busy,
		kick        => run_spi -- active until busy arrives
	 );

spi_engine: entity  work.spi32_8_control
	generic map(
        ADDR_WIDTH => ADDR_WIDTH -1,
        BYTE_SPI => BYTE_SPI
        )
    port map (
      spi_clk => spi_clk,
      reset => reset,
      outgoing_data => outgoing_data,
      incoming_data => incoming_data,
      run_spi => run_spi,
      busy => busy,
      transfer_count  => transfer_count,
      ram_ptr  => ram_ptr,
      ram_write => ram_write,-- grab data returning from slave
      cs_n => spi_out.le,
      clk_en => clk_en,
      mosi => spi_out.mosi,
      miso => spi_in.miso
    );

--    A clean version of clko <= clk_en and spi_clk;
gen_clock: ENTITY work.clock_pulse
   PORT map( 
      CLK_I => spi_clk ,
      RESET =>  reset,
      Enable =>  clk_en, 
      pulse => spi_out.clk
   );

-- incoming block located directly after outgoing block. ram_write used as pointer msb 
--	pointer_addr <= ram_write & ram_ptr;

end rtl;

