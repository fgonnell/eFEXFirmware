--! @file
--! @brief Top feature-extracting algorithm module
--! @details
--! The output TOBS are sent in 5 clock cycles. Only eFEX handlig edge region
--! really need to send out 5 TOBs per BC, most of the mouldes just need to
--! send 4 TOBs. This is handled by the input position register. If bit 1 is '1',
--! then theeFEX is an edge module, in this case bit 0 specifies whether the
--! efex is at the left edge (low eta) bit 0 = '0' or at right edge (high eta)
--! bit 0 = '1'. Bit 0 is otherwise ignored.
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
use work.DataTypes.all;
use work.AlgoDataTypes.all;

--! @copydoc TopAlgoModule.vhd
entity TopAlgoModule is
  port (CLK200 : in std_logic;

        IN_Load : in std_logic;
        IN_Data : in TriggerTowerMatrix;

        --control ports
        OUT_ParameterRAMaddress : out std_logic_vector(2 downto 0);

        --configuration ports
        --!Thresholds for specific condition taken from parameter RAM
        IN_eg_ParWs            : in AlgoParameters(2 downto 0);
        --!Thresholds for specific condition taken from parameter RAM
        IN_eg_ParREta          : in AlgoParameters(2 downto 0);
        --!Thresholds for specific condition taken from parameter RAM
        IN_eg_ParHadron        : in AlgoParameters(2 downto 0);
        --! Control register for eg algorithm
        IN_eg_Control          : in AlgoRegister;
        --! Energy threshold for TOB production;
        IN_eg_Energy_threshold : in DataWord;
        --! Energy threshold for Reta condition;
        IN_eg_Reta_threshold   : in DataWord;
        --! Energy threshold for Ws condition;
        IN_eg_Ws_threshold     : in DataWord;
        --! Energy threshold for Hadronic condition;
        IN_eg_Had_threshold    : in DataWord;



        --! Status register for eg algorithm
        OUT_eg_Status : out AlgoRegister;

        --!Thresholds for specific condition taken from parameter RAM
        IN_tau_ParJet : in AlgoParameters(2 downto 0);

        IN_tau_Control : in AlgoRegister;
        --! Control register for tau algorithm
        --! Energy threshold for TOB production;

        IN_tau_Energy_threshold : in DataWord;
        --! Energy threshold for Jet condition;
        IN_tau_Jet_threshold    : in DataWord;

        OUT_tau_Status : out AlgoRegister;

        --! Register for position of eFEX module, energy lineraisation and TOB production
        --! may differ for different positions of the modules
        IN_glob_Position : in  AlgoRegister := (others => '0');
        IN_glob_Control  : in  AlgoRegister := (others => '0');
        OUT_glob_Status  : out AlgoRegister;

        -- Output sync ports
        OUT_TOB_Start   : out std_logic;
        OUT_TOB_Counter : out std_logic_vector(2 downto 0);

        -- out ports eg
        OUT_eg_TOB : out TriggerObjects_eg(OUTPUT_TOBS-1 downto 0);

        -- out ports tau
        OUT_tau_TOB : out TriggerObjects_tau(OUTPUT_TOBS-1 downto 0));

end TopAlgoModule;

--! @copydoc TopAlgoModule.vhd
architecture Behavioral of TopAlgoModule is

  signal OutEgTOB : TriggerObjects_eg(OUTPUT_TOBS-1 downto 0);

  signal OutTauTOB : TriggerObjects_tau(OUTPUT_TOBS-1 downto 0);

-- Data Shift Register
  signal SR_InData             : TriggerTowerMatrix;
  signal SR_Load               : std_logic;
  signal SR_LeftRight, SR_Edge : std_logic;
  signal SR_OutData            : TriggerTowers(29 downto 0);


-- e/gamma
  signal eg_ParWs     : AlgoParameters(2 downto 0);
  signal eg_ParReta   : AlgoParameters(2 downto 0);
  signal eg_ParHadron : AlgoParameters(2 downto 0);
  signal eg_Control   : AlgoRegister;
  signal eg_Status    : AlgoRegister;
  --                                        AlgoCores  Towers in AC
  signal eg_Data      : TriggerTowersArray(7 downto 0)(8 downto 0);
  signal eg_TOB       : TriggerObjects_eg(7 downto 0);

  -- tau
  signal tau_ParJet : AlgoParameters(2 downto 0);

  signal tau_Control : AlgoRegister;
  signal tau_Status  : AlgoRegister;
  --                                         AlgoCores  Towers in AC
  signal tau_Data    : TriggerTowersArray(7 downto 0)(8 downto 0);
  signal tau_TOB     : TriggerObjects_tau(7 downto 0);

  signal RAMCounter    : std_logic_vector(2 downto 0) := (others => '0');
  signal InputCounter  : std_logic_vector(2 downto 0) := (others => '0');
  signal OutputCounter : std_logic_vector(2 downto 0) := (others => '0');
  signal OutputValid   : std_logic;

begin
  OUT_eg_Status   <= x"a000e100";
  OUT_tau_Status  <= x"a0007a00";
  OUT_glob_Status <= x"a0008100";

  -- Parameters connection
  eg_ParWs     <= IN_eg_ParWs;
  eg_ParReta   <= IN_eg_ParReta;
  eg_ParHadron <= IN_eg_ParHadron;
  eg_Control   <= IN_eg_Control;
  tau_ParJet   <= IN_tau_ParJet;

  tau_Control <= IN_tau_Control;

-- Boolean to specify if eFEX FPGA is on the edge i.e. handles 5 eta columns - or not.
  SR_Edge      <= IN_glob_Position(1);
-- Boolean to specify if eFEX FPGA is on the left edge (small eta) or right edge (high eta). Useless if the previous one is 0.
  SR_LeftRight <= IN_glob_Position(0);


  COUNTERS : process (clk200)
  begin
    if rising_edge(clk200) then
      if IN_Load = '1' then
        -- reset values will determine the offesets
        InputCounter  <= "001";  --1 should always be one, it is used only as REFERENCE
        RAMCounter    <= "001";         --1
        OutputCounter <= "011";         --3
      else
        -- increment
        if InputCounter = "100" then
          InputCounter <= "000";
        else
          InputCounter <= std_logic_vector(unsigned(InputCounter) + 1);
        end if;

        if RAMCounter = "100" then
          RAMCounter <= "000";
        else
          RAMCounter <= std_logic_vector(unsigned(RAMCounter) + 1);
        end if;

        if OutputCounter = "100" then
          OutputCounter <= "000";
        else
          OutputCounter <= std_logic_vector(unsigned(OutputCounter) + 1);
        end if;
      end if;
    end if;
  end process;

  OUTPUT_SIGNALS_PROCESS : process (clk200)
  begin
    if rising_edge(clk200) then
      if OutputCounter = "100" then
        OUT_TOB_Start <= '1';
      else
        OUT_TOB_Start <= '0';
      end if;

      -- TOBs on the first 4 clock cycles are valid, while the 5th is valid only if eFEX is on eta edge
      if OutputCounter = "011" and SR_Edge = '0' then
        OutputValid <= '0';
      else
        OutputValid <= '1';
      end if;

    end if;
  end process;

  OUT_TOB_Counter         <= OutputCounter;
  OUT_ParameterRAMaddress <= RAMCounter;

  SR_InData <= IN_Data;
  SR_Load   <= IN_Load;

  DATA_SHIFT_REGISTER : entity work.AlgoShiftRegister
    port map (
      CLK200       => clk200,
      IN_Load      => SR_Load,
      IN_Data      => SR_INData,
      IN_Edge      => SR_Edge,
      IN_LeftRight => SR_LeftRight,
      OUT_Data     => SR_OutData);


  ALGO_GENERATION : for i in 0 to 7 generate
-- eg mapping
    eg_Data(i) <= (6 => SR_OutData(i+02), 7 => SR_OutData(i+12), 8 => SR_OutData(i+22),
                   3 => SR_OutData(i+01), 4 => SR_OutData(i+11), 5 => SR_OutData(i+21),
                   0 => SR_OutData(i+00), 1 => SR_OutData(i+10), 2 => SR_OutData(i+20));

    AGLO_CORE_EG : entity work.AlgoCore_eg
      port map (
        CLK200              => clk200,
        IN_ParWs            => eg_ParWs,
        IN_ParReta          => eg_ParReta,
        IN_ParHadron        => eg_ParHadron,
        IN_Control          => eg_Control,
        IN_Energy_threshold => IN_eg_Energy_threshold,
        IN_Reta_threshold   => IN_eg_Reta_threshold,
        IN_Ws_threshold     => IN_eg_Ws_threshold,
        IN_Had_threshold    => IN_eg_Had_threshold,
        OUT_Status          => eg_Status,
        IN_Data             => eg_Data(i),
        OUT_TOB             => eg_TOB(i));

-- tau mapping
    tau_Data(i) <= (6 => SR_OutData(i+02), 7 => SR_OutData(i+12), 8 => SR_OutData(i+22),
                    3 => SR_OutData(i+01), 4 => SR_OutData(i+11), 5 => SR_OutData(i+21),
                    0 => SR_OutData(i+00), 1 => SR_OutData(i+10), 2 => SR_OutData(i+20));

-- tau core (temporary: eg)
    AGLO_CORE_TAU : entity work.AlgoCore_tau
      port map (
        CLK200              => clk200,
        IN_ParJet           => tau_ParJet,
        IN_Control          => tau_Control,
        IN_Energy_threshold => IN_tau_Energy_threshold,
        IN_Jet_threshold    => IN_tau_Jet_threshold,
        OUT_Status          => tau_Status,
        IN_Data             => tau_Data(i),
        OUT_TOB             => tau_TOB(i));
  end generate ALGO_GENERATION;


-- Invalidate TOBs at eta=4 if the eFEX is not on edge
  EG_OUT_FOR : for i in 0 to OUT_eg_TOB'high generate
    OUT_eg_TOB(i).Position <= eg_TOB(i).Position;
    OUT_eg_TOB(i).Core     <= eg_TOB(i).Core when OutputValid = '1' else invalidate_core(eg_TOB(i).Core);
  end generate;

  TAU_OUT_FOR : for i in 0 to OUT_tau_TOB'high generate
    OUT_tau_TOB(i).Position <= tau_TOB(i).position;
    OUT_tau_TOB(i).Core     <= tau_TOB(i).Core when OutputValid = '1' else invalidate_core(tau_TOB(i).Core);
  end generate;

end Behavioral;
