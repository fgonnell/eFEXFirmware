-- fwft_register registered FWFT block
--  Dave Sankey August 2020
-- Originally based on http://www.billauer.co.il/reg_fifo.html...

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
 
entity fwft_register is
	GENERIC(
		DATA_WIDTH: positive := 64
	);
	Port ( 
		clk			: in std_logic;
		rst_clk		: in std_logic;
-- Input signals
		in_data     : IN std_logic_vector(DATA_WIDTH-1 DOWNTO 0);
		in_valid    : IN std_logic;
		in_ready	: OUT std_logic;
-- Output signals
		out_data    : OUT std_logic_vector(DATA_WIDTH-1 DOWNTO 0);
		out_valid   : OUT std_logic;
		out_ready	: IN std_logic
	);
end fwft_register;
 
architecture Behavioral of fwft_register is

Signal prefetched_data, middle_data: std_logic_vector(DATA_WIDTH-1 downto 0);
Signal in_ready_sig: std_logic;
 
begin

	in_ready <= in_ready_sig and in_valid;

	register_process: process (clk)
	Variable load_prefetch, load_middle, prefetch_middle, prefetch_valid, middle_valid, dout_valid, input_ready, ending: std_logic;
	begin
		if rising_edge(clk) then
			if rst_clk = '1' then
				out_data <= (Others => '0');
				dout_valid := '0';
				middle_data <= (Others => '0');
				middle_valid := '0';
				prefetched_data <= (Others => '0');
				prefetch_valid := '0';
				input_ready := '0';
				ending := '0';
			else
--- Output register
				if ((out_ready = '1') or (dout_valid = '0')) then
					if (middle_valid = '1') then
						middle_valid := '0';
						dout_valid := '1';
						out_data <= middle_data
						-- pragma translate_off
						after 2 ns
						-- pragma translate_on
						;
					else
						dout_valid := '0';
					end if;
				end if;
				out_valid <= dout_valid
				-- pragma translate_off
				after 2 ns
				-- pragma translate_on
				;
-- In ready logic preserving an IFG!
				if ((dout_valid = '1') and (in_valid = '0')) then
					ending := '1';
				elsif (dout_valid = '0') then
					ending := '0';
				end if;
				if ((middle_valid = '1') or (ending = '1')) then
					input_ready := '0';
				else
					input_ready := '1';
				end if;
				in_ready_sig <= input_ready
				-- pragma translate_off
				after 2 ns
				-- pragma translate_on
				;
--- Buffer logic
				if ((middle_valid = '0') and (prefetch_valid = '1')) then
					prefetch_middle := '1';
					load_middle := '0';
					if ((in_valid = '1') and (in_ready_sig = '1')) then
						load_prefetch := '1';
					else
						load_prefetch := '0';
					end if;
				elsif ((in_valid = '1') and (in_ready_sig = '1')) then
					prefetch_middle := '0';
					if (middle_valid = '1') then
						load_middle := '0';
						load_prefetch := '1';
					else
						load_middle := '1';
						load_prefetch := '0';
					end if;
				else
					prefetch_middle := '0';
					load_middle := '0';
					load_prefetch := '0';
				end if;
-- Move internal data
				if (prefetch_middle = '1') then
					middle_valid := '1';
					prefetch_valid := '0';
					middle_data <= prefetched_data
					-- pragma translate_off
					after 2 ns
					-- pragma translate_on
					;
				elsif (load_middle = '1') then
					middle_valid := '1';
					prefetch_valid := '0';
					middle_data <= in_data
					-- pragma translate_off
					after 2 ns
					-- pragma translate_on
					;					
				end if;
				if (load_prefetch = '1') then
					prefetch_valid := '1';
					prefetched_data <= in_data
					-- pragma translate_off
					after 2 ns
					-- pragma translate_on
					;
				end if;
			end if;
		end if;
	end process register_process;
	
end Behavioral;