----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/27/2016 11:08:50 AM
-- Design Name: 
-- Module Name: ipbus_xadc_drp - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use ieee.numeric_std.all;
USE ieee.math_real.all;

library ipbus_lib;
use ipbus_lib.ipbus.all;

entity ipbus_xadc_drp is
   generic(NUMREG: natural := 12);
	port(
		ipb_clk: in STD_LOGIC;
		reset: in STD_LOGIC;
		ipbus_in: in ipb_wbus;
		ipbus_out: out ipb_rbus
		
	);
	
end ipbus_xadc_drp;

architecture rtl of ipbus_xadc_drp is

constant SEL_BITS	: integer := integer(ceil(log2(real(NUMREG))));
type xadc_reg_array is array( 0 to NUMREG-1) of std_logic_vector(15 downto 0);
signal adc_conv: xadc_reg_array := (others => (others => '0'));	
signal sel: integer range 0 to NUMREG-1;
signal addr: std_logic_vector(SEL_BITS-1 downto 0);
signal ack: std_logic := '0';
	

   

begin

    addr <= ipbus_in.ipb_addr(SEL_BITS-1 downto 0);
    sel <= to_integer(unsigned(addr));
    
----------------------------------
-- IPbus interface 

	process(ipb_clk)
	begin
		if rising_edge(ipb_clk) then
			ack <= ipbus_in.ipb_strobe and not ack;
		end if;
	end process;

    ipbus_out.ipb_ack <= ack;
    ipbus_out.ipb_err <= '0';

-- infer the data selection mux

	ipbus_out.ipb_rdata <= x"0000" & adc_conv(sel);
	

-- instantiate xadc conversion registers


   adc_inst : entity work.xadc_eFEX
    port map(
     DCLK                => ipb_clk, 
     RESET               => RESET,
     VAUXP               => "0000", -- unused during simulation
     VAUXN               => "0000", -- unused during simulation
     VP                  => '0', -- unused during simulation
     VN                  => '0', -- unused during simulation
 
     MEASURED_TEMP          => adc_conv(0),   
     MEASURED_TEMP_MIN      => adc_conv(1),   
     MEASURED_TEMP_MAX      => adc_conv(2),
     MEASURED_VCCINT        => adc_conv(3),
     MEASURED_VCCINT_MIN    => adc_conv(4),
     MEASURED_VCCINT_MAX    => adc_conv(5),
     MEASURED_VCCAUX        => adc_conv(6),
     MEASURED_VCCAUX_MIN    => adc_conv(7),
     MEASURED_VCCAUX_MAX    => adc_conv(8),
     MEASURED_VCCBRAM       => adc_conv(9),
     MEASURED_VCCBRAM_MIN   => adc_conv(10),
     MEASURED_VCCBRAM_MAX   => adc_conv(11),     
     ALM                    => open,-- Alarms               
     CHANNEL             => open,
     OT                  => open,-- Over Temperature Alarm
     EOC                 => open,-- End of conversion
     EOS                 => open -- End of sequence
);

end rtl;