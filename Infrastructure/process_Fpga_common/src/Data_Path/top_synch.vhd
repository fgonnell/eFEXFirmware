----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/14/2016 03:00:29 PM
-- Design Name: 
-- Module Name: top_synch - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;





LIBRARY xil_defaultlib;
use work.EfexDataFormats.all;
--use work.synch_type.all;

entity top_synch is
   
Port (
      rx_clk280               : in  std_logic;    -- rx clock of the mgt                
      TTC_clk                  : IN  std_logic;    -- ttc clk of 40MHz                   
      reset                     : in  std_logic;
      enable_mgt           : in  std_logic;    --- if high enable the mgt rx register
      MGT_Commadet    : in  std_logic;    -- comma detected for incoming data
      reg_sel                  : in  std_logic_vector(3 downto 0);  -- setting the first stage mux
      mux_sel                 : in  std_logic_vector(3 downto 0);  -- setting BC mux
      start                      : in  std_logic;    -- start pulse for the calibration to start
      rx_resetdone         : in     std_logic;
      reg224_latch         : out  std_logic;
      delay_latch            : out  std_logic;
      ttc_pipe                 : out  std_logic;
      delay_num              : out  std_logic_vector (3 downto 0); -- first stage dealy counter value
      bcn_synch              : out std_logic;
      data_out_reg224    : out std_logic_vector(223  downto 0); -- data before mux 
      disp_notable_error  : out std_logic_vector( 1 downto 0);
      crc_error                 : out std_logic;
      data_out                 : out std_logic_vector(223  downto 0);  -- data after mux
      data_in                   : in  std_logic_vector(31 downto 0);
      disperr_error          : in std_logic;
      notable_error         : in std_logic;
      crc_error_40            : in std_logic

  );  
  
end top_synch;

architecture Behavioral of top_synch is
SIGNAL latch_enable,temp0,temp1,Reg_enable,commdet_delay : std_logic;
signal delay_cnt, mux_cntrl,data_delay : std_logic_vector (3 downto 0);
signal data_out_int, dataout: std_logic_vector (226 downto 0);
signal temp2,ttc_redge, ttc_20M : std_logic;
signal bcn,reg_sel_i : std_logic_vector (4 downto 0);
signal data_out_int_i :std_logic_vector( 225 downto 0);


begin

ttc_pipe                <= temp1;
delay_num            <= data_delay;
delay_latch           <= reg_enable;
bcn                       <= dataout(196 downto 192); --MGT_to_SuperCells('0' & dataout).BCID(4 downto 0);  --dataout(196 downto 192);
data_out                <= dataout( 223 downto 0); -- data output after mux
disp_notable_error <= dataout( 225 downto 224);
crc_error                <= dataout( 226)     ;    
 
data_out_reg224 <= data_out_int (223 downto 0); -- data output before 
--data_out_reg224 <= dataout (223 downto 0);  -- ensure 

reg_sel_i <= '0' & reg_sel;


--  This process generates pulse if bcn = 11111
synch_bcn:process ( TTC_clk)
          begin
              if TTC_clk' event and TTC_clk ='1' then
               if (bcn = "11111") then   -- check if bcbn is equal 31
                   bcn_synch <= '1';  -- generate single pulse of 40 MHz
                else  
                  bcn_synch <= '0';   --- not generate 
               end if;    
               
           end if;
        end process;       




shift_register: entity work.SRLC32E_226 
   port map (
      clk      => ttc_clk,
      srl_en   => enable_mgt,
	  address  => reg_sel_i,
	  data_in  => data_out_int,  -- before second stage MUX
	  data_out => dataout        -- after second stage MUX
	  );
   data_out_int <= crc_error_40 & data_out_int_i(225 downto 0);
                     
-- Capture the incoming TTC 40M clk by dtype flipflop 
dtype: entity work.d_type
          port map (
                clk   => ttc_clk,
                 q     => ttc_20M              
                
               ); 



--  Two register synchorization  for the ttc clock in to rx clock of 280M Hz
pipe_ttc_clk:  process (rx_clk280)

                begin
                    if rx_clk280' event and rx_clk280 ='1' then
                          temp0<= ttc_20M;
                          temp1<= temp0;
                          temp2<= temp1;
                          ttc_redge <= temp1 xor temp2  after 1 ns;
                    end if;
                            
                end process; 
                
 -- First stage timing auto calibration  state machine that will generate delay count
state_machine: entity work.tac_sm
 
       PORT MAP (
          clk_280M     => rx_clk280,
          MGT_COMMADET => MGT_COMMADET,
          RESET        => reset,
          rx_resetdone => rx_resetdone, -- wait rx  reset done  to go high
          TTC_CLK_edge => ttc_redge,
          start        => start,  --  kick start pulse for the state machine to count the diffrence edges 
          Reg_enable   =>Reg_enable,
          Mux_value    => Delay_cnt 
       );       
       
       
     
       
 -- Delay count register that holds the number of delay that was counted by the state machine      
 
reg_delay_cnt:  process (rx_clk280)
      
       begin
         if rx_clk280' event and rx_clk280 ='1' then
           if reg_enable ='1' then -- time difference between the ttc edge and 280Mhz is ready
             data_delay <= delay_cnt; -- read the edge difference value between two clocks
           end if;
         end if;  
       end process;   
       
       
synch_1: entity work.synch_stage_1
           
           PORT MAP (
             clk_280     => rx_clk280,
             reset       => reset,
             enable_mgt  => enable_mgt,
             mux_cntrl   => mux_sel, -- first stage mux setting 
             latch_enable => latch_enable,
             MGT_Commadet => MGT_Commadet, -- mgt comma detect input from the rx
             commdet_delay => commdet_delay,
             data_in     => data_in, -- in coming rx_data 32 bits
             data_out    => data_out_int_i,  --outgoing data of 226 bits that goes to the fibre mapping
             disperr_error => disperr_error, -- disperr error is added in the synch in order to go with the data
             notable_error => notable_error  -- notable error is added in the synch in order to go with the data

             
           );      
       
       
 
 -- Generates latch enable for the output data of 224 bits (7 x 32 bits) 
latch: entity work.latch_enable
            Port MAP(
              CLK_280M     => rx_clk280,
              MGT_COMMADET => commdet_delay,
              latch_enable   => latch_enable -- latches when 7  32bis of data are ready
       
              );
 

 
reg224_latch <= latch_enable;          

end Behavioral;
