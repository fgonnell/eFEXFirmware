--! @file
--! @brief FSM to write only XTOBs (tau & e/g) data to Link Output FIFO for process FPGA 3 and 4
--! @details 
--! \verbatim
--! This module creates a complete XTOB event together with Header and Trailer and writes the entire event into Link Output FIFO.
--! The Link Output FIFO is then controlled by FIFO_to_MGT_FSM state machine to transfer data to MGT
--!
--! Process FPGA 1 processes all the e/g TOBs for all 4 Process FPGAs and
--! Process FPGA 2 processes all the tau TOBs for all 4 Process FPGAs.
--! Process FPGA 3 and 4 only construct events with their local XTOBs.
--!
--! This FSM also handles Header and Trailer construction.
--!
--! In order to create full TOB/XTOB event (frame) all parallel data from TOBs and XTOBs de-randomisation FIFOs must be read in turn,
--! In multi-slice readout, all parallel data from TOB and XTOB de-randomisation FIFOs must be read twice or more.
--!
--!    When the occupancy of de-randomisation TOB/XTOB Data FIFO or TTC FIFO reaches its prog FULL occupancy level,
--!    a Safe Mode Flag is set which is used to create Safe Mode XTOB events and empty the XTOB Data FIFO & TTC FIFO.
--!    These Safe Mode events consists of 2 Header words, and one Trailer word. 
--!    The payload consists of two words, a ZERO word together with a sub-trailer word for the slice.
--!    Multi-slice readout contains a number of these double words, equal to the number of slices to be readout.
--!
--! 1. Readout operation for Process FPGAs 3 & 4, create  Events which consists of only Valid e/g and tau XTOBs..
--!    a- XTOB Event in Normal Operation:
--!       Read 1 Slice - Takes 92 ticks of 280MHz clock to create one XTOB Event with 1 Slice Readout.
--!    b- XTOB Event in Normal Operation:
--!       Read 2 Slice - Takes 180 ticks of 280MHz clock to create one XTOB Event with 2 Slice Readout.
--!    c- XTOB Event in Normal Operation:
--!       Read 3 Slice - Takes 267 ticks of 280MHz clock to create one XTOB Event with 3 Slice Readout.
--!    d- XTOB Event in Normal Operation:
--!       Read 4 Slice - Takes 354 ticks of 280MHz clock to create one XTOB Event with 4 Slice Readout.
--!    e- XTOB Event in SAFE Mode Operation:
--!       Read 0 Slice - Takes 8 ticks of 280MHz clock to create one SAFE Mode XTOB Event.
--!
--! The output of this FSM is:
--!     32-bit data word
--!     1-bit data is CHAR
--!     1-bit valid which is the write enable to Link Ouput FIFO
--! \endverbatim
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;
use TOB_rdout_lib.data_type_pkg.all;

--! @copydoc fsm_XTOBs_to_muxPISO.vhd
entity fsm_XTOBs_to_muxPISO is
    Port ( 
       RST                        : in std_logic ;
       -- test signal delete after test
       tst_fsm_cntr               : out std_logic_vector (31 downto 0) ;
       --! TOB Readout FIFO reset Pulse by software command ORed with SYS_RST
       TOB_FIFO_sw_rst          : in std_logic ;       
       --! FPGA Hardware Address
       hw_addr                    : in STD_LOGIC_VECTOR(1 downto 0) ;   
       --! sorted TOBs 209b = 7b+4b+6b+192b  TOB_BCN+ERR+valid+TOBs
       rdout_T_TOB_209b_in        : in STD_LOGIC_VECTOR(208 downto 0);  
       --! sorted TOBs valid signal
       valid_T_TOB_in             : in std_logic ;
       --! TOB Type 0 = em, 1 = tau                        
       TOB_type_in                : in STD_LOGIC;
       --! XTOBs e/g {array 8 of [(5*64b) + 5b valid]}
       XTOBs_eg_in                : in array_8_of_332b;  -- XTOBs {array 8 of [(5*64b) + 5b valid] + XTOB_BCN}
       --! XTOBs e/g valid signal 
       valid_XTOBs_eg_in          : in std_logic ;
       --! XTOBs tau {array 8 of [(5*64b) + 5b valid]}
       XTOBs_tau_in               : in array_8_of_332b;  -- XTOBs {array 8 of [(5*64b) + 5b valid] + XTOB_BCN} 
       --! XTOBs tau valid signal
       valid_XTOBs_tau_in         : in std_logic ;
       --! 280MHz clock input signal
       clk_280M_in                : in STD_LOGIC;
       --! Input empty flag of all TOB FIFOs TTC, TOB & XTOB FIFOs 
       TOB_FIFO_empty_in          : in std_logic ;   
       --! Input prog Full flag of all TOB FIFOs TTC, TOB & XTOB FIFOs
       TOB_prog_full_flag_in      : in std_logic ;   
       --! Ready signal from control FPGA to TX data    
       ctrl_ready_in              : in STD_LOGIC;
       --! number of DRPAM locations to read for multi-slice (1 to 5)
       DPR_locations_to_rd        : in STD_LOGIC_VECTOR (2 downto 0);       
       --!  Readout Expected: RAW data readout    
       rdout_expected             : in STD_LOGIC; 
       --! Bunch Crossing ID from BCN counter
       FIFO_BCN_in                : in STD_LOGIC_VECTOR (11 downto 0);       
       --! L1A ID from TTC input
       FIFO_L1A_ID                : in STD_LOGIC_VECTOR (23 downto 0);       
       --! Extended L1A ID from TTC input
       FIFO_L1A_ID_EXT            : in STD_LOGIC_VECTOR (7 downto 0);       
       --!  Link Output FIFO partial FULL flag to receive RAW calorimeter data
       LO_FIFO_prog_full_in        : in  std_logic ; 
       --!  Link Output FIFO data count
       LO_FIFO_data_count_in      : in  STD_LOGIC_VECTOR (12 downto 0)  ;
       --! enable frame counter to count up by 1                        
       frame_cntr_en              : out STD_LOGIC;
       --! read enable signal to BCN & L1A_ID FIFOs
       BCN_FIFO_rd_en_out         : out STD_LOGIC;
       --! read enable signal to all TOB/XTOB FIFOs
       TOB_rdout_fifo_rd_en_out   : out STD_LOGIC;
       --! sorted TOBsXTOBs data valid signal to Link_outpout_FIFO
       TOB_out_valid              : out STD_LOGIC;
       --! sorted TOBsXTOBs data is CHAR signal to Link_outpout_FIFO
       TOB_out_is_char            : out STD_LOGIC;
       --! sorted TOBsXTOBs 32b data to Link_outpout_FIFO
       TOBs_out                   : out STD_LOGIC_VECTOR (31 downto 0)
   );
   
end fsm_XTOBs_to_muxPISO;

--! @copydoc fsm_XTOBs_to_muxPISO.vhd
architecture Behavioral of fsm_XTOBs_to_muxPISO is

--! @details 
--! \verbatim
--! CHAR constants are defined in data_type_pkg.vhd
--! for reference only
--!    constant ch_idle    : std_logic_vector(7 downto 0)  := X"BC"  ;    -- idle char is K28.5
--!    constant ch_sop1    : std_logic_vector(7 downto 0)  := X"3C"  ;    -- TOB/XTOB star of packet char is K28.1
--!    constant ch_sop2    : std_logic_vector(7 downto 0)  := X"7C"  ;    -- CALO DATA star of packet char is K28.3
--!    constant ch_eop     : std_logic_vector(7 downto 0)  := X"DC"  ;    -- end  of packet char is K28.6
--! \endverbatim
    
    signal  TOB_out_valid_i              : std_logic;
    signal  TOB_rdout_fifo_rd_en_i       : std_logic ;
    signal  corrective_trailer           : std_logic ;

    signal  ctrl_ready_in_i, BCN_FIFO_rd_en_i : std_logic  ;
    
    signal  slice_count_i                : unsigned (2 downto 0);
    signal  link_err_flg_in_i            : STD_LOGIC_VECTOR(3  downto 0);
    signal  TOB_BCN_in_i                 : STD_LOGIC_VECTOR(7 downto 0);
    signal  FIFO_BCN_in_i                : STD_LOGIC_VECTOR(11 downto 0);
    signal  XTOB_eg_BCN_in_i             : std_logic_vector (7 downto 0);
    signal  XTOB_tau_BCN_in_i            : std_logic_vector (7 downto 0);
    signal  FIFO_L1A_ID_i                : STD_LOGIC_VECTOR(23 downto 0);
    signal  FIFO_L1A_ID_EXT_i            : STD_LOGIC_VECTOR(7  downto 0);

    signal  TOBs_out_i                   : STD_LOGIC_VECTOR(31 downto 0);

    signal  TOB_out_is_char_i      : std_logic ;
    signal  write_in_LO_FIFO_i	   : std_logic  ;

    signal  CLK_280M_i             : std_logic ;
    signal  RST_i                  : std_logic ;

    signal  cntr_rst_i             : std_logic  ;
    signal  TOB_cntr_rst           : std_logic  ;
    signal  T_TOB_eg_cntr_en_i     : std_logic  ;
--    signal  T_TOB_eg_cntr_i        : std_logic_vector(3 downto 0);
--    signal  T_TOB_tau_cntr_i       : std_logic_vector(3 downto 0);

    signal  XTOB_eg_cntr_en_i      : std_logic  ;
    signal  XTOB_eg_cntr_i         : std_logic_vector(6 downto 0);
    signal  XTOB_eg_cntr_1dly      : std_logic_vector(6 downto 0);
    signal  XTOB_tau_cntr_en_i     : std_logic ;
    signal  XTOB_tau_cntr_i        : std_logic_vector(6 downto 0);
    signal  XTOB_tau_cntr_1dly     : std_logic_vector(6 downto 0);
    
    signal  trailer_cntr_en        : std_logic ;
    signal  payld_cntr_rst         : std_logic ;
    signal  payld_cntr_rst_i       : std_logic ;
    signal  payld_cntr_en          : std_logic ;
    signal  TOB_payld_cntr_i       : std_logic_vector(11 downto 0);
    signal  TOB_payld_cntr_1dly    : std_logic_vector(11 downto 0);
    
    signal  frame_cntr_en_i        : std_logic ;
    signal  slice_no_txed          : std_logic_vector(2 downto 0);  -- slice number transmitted
 
    signal  T_TOB_eg_i           : std_logic_vector(191 downto 0);
    signal  T_TOB_eg_valid_i     : std_logic_vector(5 downto 0);
    signal  valid_T_TOB_in_i     : std_logic ;
    signal  XTOB_eg_i            : array_8_of_320b;  -- XTOBs e/g {array 8 of [(5*64b) + 5b valid]};
    signal  XTOB_eg_valid_i      : array_8_of_5b ; 	-- std_logic_vector(39 downto 0);
    signal  XTOB_tau_i           : array_8_of_320b;  -- XTOBs tau {array 8 of [(5*64b) + 5b valid]};
    signal  XTOB_tau_valid_i     : array_8_of_5b;	-- std_logic_vector(39 downto 0);

    signal  safe_mode_i         : std_logic ;
    signal  miss_sop_i          : std_logic ;   -- signal to miss SOP for multi-slice readout
    signal  wr_en_i             : std_logic ;   -- enable write into link output fifo when not pfull 
    
    TYPE STATE_TYPE IS (
       idle, wait1, wait2, wait3, wait4,
       err_sop, err_eop,
       norm_sop1, norm_sop2, norm_tlr, norm_eop,
       rd_fifo, sub_trl_1, sub_trl_2,
       rdout_T_TOB_eg, rdout_XTOB_eg_1, rdout_XTOB_eg_2, rdout_XTOB_tau_1, rdout_XTOB_tau_2
       );
  
    SIGNAL current_state : STATE_TYPE;
    signal i, j         : integer range 0 to 15;       -- used to index TOB valid flag 
    signal m_s_count    : integer range 0 to 7;       -- used to count number of multi slice data 1 to 5

--    ####### attributes  ########
    attribute keep       : string ;
    attribute max_fanout : integer;
	
    attribute keep of       j : signal is "true" ;
    attribute max_fanout of j : signal is 30;
    attribute keep of       i : signal is "true" ;
    attribute max_fanout of i : signal is 30;
    attribute keep of       TOBs_out_i : signal is "true" ;
    attribute max_fanout of TOBs_out_i : signal is 30;
--    #######################################
    
begin

    -- input ports
    corrective_trailer   <= '0';    -- set corrective_trailer to ZERO
    CLK_280M_i           <= CLK_280M_in ;
    RST_i                <= RST ;
    ctrl_ready_in_i      <= ctrl_ready_in ;
    
    -- output ports
    TOB_rdout_fifo_rd_en_out  <= TOB_rdout_fifo_rd_en_i  ;
    TOBs_out                  <= TOBs_out_i ;
    TOB_out_valid             <= TOB_out_valid_i ;
    TOB_out_is_char           <= TOB_out_is_char_i ;
    frame_cntr_en             <= frame_cntr_en_i ;
    tst_fsm_cntr              <= (others => '0') ;  -- was tst_fsm_cntr_i ;
    BCN_FIFO_rd_en_out        <= BCN_FIFO_rd_en_i ; 

    TOB_cntr_rst <= cntr_rst_i OR RST_i;

-- clk data input to remove timing error
U1_clk_280_proc : Process (CLK_280M_i)
    begin
        if rising_edge (CLK_280M_i) then
            FIFO_BCN_in_i        <= FIFO_BCN_in ;   -- 12b BCN ID from FIFO
            FIFO_L1A_ID_i        <= FIFO_L1A_ID ;   -- 24b L1A ID from FIFO
            FIFO_L1A_ID_EXT_i    <= FIFO_L1A_ID_EXT ;   -- 8b L1A ID Extended from FIFO
        
            T_TOB_eg_i           <= (others => '0') ;     -- 6*32=192b TOB data
            T_TOB_eg_valid_i     <= (others => '0') ;   -- 6b  Valid flags
            link_err_flg_in_i    <= (others => '0') ;   -- 4 b Error
            TOB_BCN_in_i         <= (others => '0') ;   -- 7b TOB_BCN
            valid_T_TOB_in_i     <= '0' ;    -- sorted TOB valid
            XTOB_eg_i(0)           <= XTOBs_eg_in(0)(319 downto 0);
            XTOB_eg_i(1)           <= XTOBs_eg_in(1)(319 downto 0);
            XTOB_eg_i(2)           <= XTOBs_eg_in(2)(319 downto 0);
            XTOB_eg_i(3)           <= XTOBs_eg_in(3)(319 downto 0);
            XTOB_eg_i(4)           <= XTOBs_eg_in(4)(319 downto 0);
            XTOB_eg_i(5)           <= XTOBs_eg_in(5)(319 downto 0);
            XTOB_eg_i(6)           <= XTOBs_eg_in(6)(319 downto 0);
            XTOB_eg_i(7)           <= XTOBs_eg_in(7)(319 downto 0);
                                    
            XTOB_eg_valid_i(0)     <= XTOBs_eg_in(0)(324 downto 320);
            XTOB_eg_valid_i(1)     <= XTOBs_eg_in(1)(324 downto 320);
            XTOB_eg_valid_i(2)     <= XTOBs_eg_in(2)(324 downto 320);
            XTOB_eg_valid_i(3)     <= XTOBs_eg_in(3)(324 downto 320);
            XTOB_eg_valid_i(4)     <= XTOBs_eg_in(4)(324 downto 320);
            XTOB_eg_valid_i(5)     <= XTOBs_eg_in(5)(324 downto 320);
            XTOB_eg_valid_i(6)     <= XTOBs_eg_in(6)(324 downto 320);
            XTOB_eg_valid_i(7)     <= XTOBs_eg_in(7)(324 downto 320);
            
            XTOB_eg_BCN_in_i       <= '0' & XTOBs_eg_in(0)(331 downto 325);
         
            XTOB_tau_i(0)           <= XTOBs_tau_in(0)(319 downto 0);
            XTOB_tau_i(1)           <= XTOBs_tau_in(1)(319 downto 0);
            XTOB_tau_i(2)           <= XTOBs_tau_in(2)(319 downto 0);
            XTOB_tau_i(3)           <= XTOBs_tau_in(3)(319 downto 0);
            XTOB_tau_i(4)           <= XTOBs_tau_in(4)(319 downto 0);
            XTOB_tau_i(5)           <= XTOBs_tau_in(5)(319 downto 0);
            XTOB_tau_i(6)           <= XTOBs_tau_in(6)(319 downto 0);
            XTOB_tau_i(7)           <= XTOBs_tau_in(7)(319 downto 0);
                                    
            XTOB_tau_valid_i(0)     <= XTOBs_tau_in(0)(324 downto 320);
            XTOB_tau_valid_i(1)     <= XTOBs_tau_in(1)(324 downto 320);
            XTOB_tau_valid_i(2)     <= XTOBs_tau_in(2)(324 downto 320);
            XTOB_tau_valid_i(3)     <= XTOBs_tau_in(3)(324 downto 320);
            XTOB_tau_valid_i(4)     <= XTOBs_tau_in(4)(324 downto 320);
            XTOB_tau_valid_i(5)     <= XTOBs_tau_in(5)(324 downto 320);
            XTOB_tau_valid_i(6)     <= XTOBs_tau_in(6)(324 downto 320);
            XTOB_tau_valid_i(7)     <= XTOBs_tau_in(7)(324 downto 320);
            
            XTOB_tau_BCN_in_i       <= '0' & XTOBs_tau_in(0)(331 downto 325);
            
            XTOB_eg_cntr_1dly   <= XTOB_eg_cntr_i ;
            XTOB_tau_cntr_1dly  <= XTOB_tau_cntr_i ;
            TOB_payld_cntr_1dly <= TOB_payld_cntr_i ;
        end if;
    end process;
           
--! @details
--! \verbatim
--! XTOB_eg_cntr counter, counts the total number of valid e/g XTOBs.  
--! \endverbatim
U2_XTOB_eg_cntr : entity TOB_rdout_lib.cntr_generic
    generic map(
        width  =>  7
        )
    Port map ( 
       CE   =>  XTOB_eg_cntr_en_i ,
       CLK  =>  CLK_280M_i ,
       RST  =>  TOB_cntr_rst,
       Q    =>  XTOB_eg_cntr_i      -- count every valid XTOB e/g
       );
       
--! @details
--! \verbatim
--! XTOB_tau_cntr counter, counts the total number of valid tau XTOBs.  
--! \endverbatim
U2_XTOB_tau_cntr : entity TOB_rdout_lib.cntr_generic
    generic map(
        width  =>  7
        )
    Port map ( 
       CE   =>  XTOB_tau_cntr_en_i ,
       CLK  =>  CLK_280M_i ,
       RST  =>  TOB_cntr_rst,
       Q    =>  XTOB_tau_cntr_i     -- count every valid XTOB tau
       );
                             
    payld_cntr_rst <= RST_i OR payld_cntr_rst_i;
    payld_cntr_en  <= XTOB_eg_cntr_en_i OR XTOB_tau_cntr_en_i OR trailer_cntr_en;
    
    
--! @details
--! \verbatim
--! TOB_payld_length counter, counts the total number of payload data inserted between the Header and Trailer of a TOB/XTOB Event.  
--! \endverbatim
U2_TOB_payld_length : entity TOB_rdout_lib.cntr_generic
    generic map(
        width  =>  12
        )
    Port map ( 
       CE   =>  payld_cntr_en ,     -- count every valid data word (32b words)
       CLK  =>  CLK_280M_i ,
       RST  =>  payld_cntr_rst,
       Q    =>  TOB_payld_cntr_i     -- count every valid data word (divide by 2 to show no. of 64b words)
       );

--! @details
--! \verbatim
--! This process disables writing to Link Output FIFO when the LO_FULL reaches a specific occupancy level of 0X1FF0.  
--! \endverbatim
U3A_proc1 : process (CLK_280M_i)
begin
    if rising_edge (CLK_280M_i) then 
       if (unsigned(LO_FIFO_data_count_in) < X"1FF0") then  -- 8128 leaves enough space for a Safe Mode event
           -- if LO_FIFO is nearly FULL then stop reading from de-randomistion FIFO
           write_in_LO_FIFO_i <= '1';
       else
          write_in_LO_FIFO_i <= '0' ;
       end if;
    end if;
  end process;

     
     wr_en_i <= write_in_LO_FIFO_i ;    -- enable write into link output fifo when not pfull

U4_rd_fsm : process (CLK_280M_i)
    begin
        if CLK_280M_i'event and CLK_280M_i = '1' then
            if ( TOB_FIFO_sw_rst = '1' )then        -- signal is RST OR TOB_FIFO_sw_rst
                current_state           <= idle ;
                TOBs_out_i                <= (others => '0') ;
            else    
                CASE current_state is
                    when idle =>
--                        tst_fsm_cntr_i            <= X"00000001";
                        slice_count_i        <= unsigned (DPR_locations_to_rd) ; -- multi-slice read register 1 to 5
                        miss_sop_i                <= '0' ;    -- do not miss SOP
                        payld_cntr_rst_i          <= '0' ;
                        frame_cntr_en_i           <= '0' ;
                        trailer_cntr_en           <= '0' ; 
                        BCN_FIFO_rd_en_i          <= '0' ; 
                        TOB_rdout_fifo_rd_en_i    <= '0' ;
                        XTOB_eg_cntr_en_i         <= '0' ;      
                        XTOB_tau_cntr_en_i        <= '0' ;      
                        TOB_out_valid_i           <= '0' ;
                        cntr_rst_i                <= '0' ;
                        TOB_out_is_char_i         <= '0' ;
                        m_s_count                 <=  0 ;
                        safe_mode_i               <= '0' ;
                        i <= 0 ;
						j <= 0 ;
                        slice_no_txed             <= "000";  -- initially set to 1
                        current_state             <= idle ;
                        
                        if (write_in_LO_FIFO_i = '1' )  then -- if LO_FIFO is not full, less than X"1FF0"
                            if (TOB_FIFO_empty_in = '0') then   -- if TOB FIFOs are not empty
                                -- if LO_FIFO is partial FULL AND TOB_FIFO_prog_full NOT set, then pause
                                if (LO_FIFO_prog_full_in = '1' AND TOB_prog_full_flag_in = '0')  then 
                                    current_state        <= idle ;
                                else
                                -- if LO_FIFO is partial FULL AND TOB_FIFO is partial FULL, then SAFE MODE, empty FIFOs
                                    if (TOB_prog_full_flag_in = '1' AND TOB_prog_full_flag_in = '1')  then 
                                        safe_mode_i           <= '1' ;    -- enable safe mode flag
                                        BCN_FIFO_rd_en_i      <= '1' ;      -- read data from BCN fifo
                                        TOB_rdout_fifo_rd_en_i  <= '1';           -- read data from fifos
                                       current_state   	  <=  norm_sop1   ;
                                    else    -- read data
                                        BCN_FIFO_rd_en_i    <= '1' ;      -- read data from BCN fifo
                                        TOB_rdout_fifo_rd_en_i  <= '1';           -- read data from fifos
                                        current_state   	<=  rd_fifo   ;
                                    end if;
                                end if;
                             end if;
                         end if;


--                        -- if fifo is not empty, read fifos
--                        if (BCN_FIFO_empty_in = '0' ) then 
--                            current_state           <= rd_fifo ;
--                            TOB_rdout_fifo_rd_en_i  <= '1';           -- read data from fifos
--                            BCN_FIFO_rd_en_i        <= '1' ;
--                        else
--                            -- if no data available wait
--                            current_state <= idle ;
--                       end if;
                    when rd_fifo =>       -- wait 1 clk for fifo output
--                        tst_fsm_cntr_i      <= X"00000002";
                        trailer_cntr_en     <= '0' ; -- stop counter counting coming here from sub_tlr_2
                        cntr_rst_i          <= '0' ;    -- reset the TOB counters
                        TOB_rdout_fifo_rd_en_i    <= '0' ;
                        BCN_FIFO_rd_en_i          <= '0' ;
                        TOB_out_valid_i           <= '0' ;      -- do not save in FIFO
                        if valid_XTOBs_eg_in = '1' then         -- if XTOs are valid, write to shift register.
                            if miss_sop_i = '0'  then     -- insert SOP
                                current_state   <= norm_sop1 ;
                            else                         -- miss SOP
                                current_state   <= rdout_XTOB_eg_1 ;  
                           end if;
                        else
                          current_state   <= rd_fifo ;  -- stay until FIFO read
                        end if;
                           
                    when norm_sop1 =>            -- normal data header SOP 1
--                        tst_fsm_cntr_i      <= X"00000003";
                        -- XTOB_tau_bcn 8b + XTOB_eg_BCN 4b + BCN 12b & K28.1
                        TOB_rdout_fifo_rd_en_i  <= '0' ;
                        BCN_FIFO_rd_en_i        <= '0' ;
                        TOBs_out_i              <= XTOB_tau_BCN_in_i & XTOB_eg_BCN_in_i(3 downto 0) & FIFO_BCN_in_i & ch_sop1 ;   
                        TOB_out_is_char_i       <= '1';
                        TOB_out_valid_i         <= '1' ;      -- save in FIFO
                        current_state           <= norm_sop2 ;
                           
                    when norm_sop2 =>            -- normal data header SOP 2
--                        tst_fsm_cntr_i      <= X"00000004";
                        TOBs_out_i              <= FIFO_L1A_ID_EXT_i & FIFO_L1A_ID_i ;  -- L1A_ID_EXT + L1A ID
                        TOB_out_is_char_i       <= '0';
                        TOB_out_valid_i         <= '1' ;
--                        m_s_count    <= m_s_count + 1 ;      -- used to count multi slice readout
                        if ( safe_mode_i = '1' ) then  -- if in SAFE MODE or Read on Error mode
                            current_state         <= sub_trl_1 ;
                        else    -- normal read operation
                            current_state         <= rdout_XTOB_eg_1 ;  
                        end if;
                            
                    when rdout_XTOB_eg_1 =>         -- normal X_TOB_eg rd out 80 clks
--                        tst_fsm_cntr_i      <= X"00000006";
                        XTOB_eg_cntr_en_i   <= XTOB_eg_valid_i(j)(i) AND wr_en_i;
                        TOB_out_valid_i     <= XTOB_eg_valid_i(j)(i) AND wr_en_i;    -- on valid save in FIFO
                        TOBs_out_i          <= XTOB_eg_i(j)((((i*64)+32)-1) downto (i*64));
                        current_state        <= rdout_XTOB_eg_2 ;

                    when rdout_XTOB_eg_2 =>         -- normal X_TOB_eg rd out 80 clks
--                        tst_fsm_cntr_i      <= X"00000007";
                        XTOB_eg_cntr_en_i   <= XTOB_eg_valid_i(j)(i) AND wr_en_i;
                        TOB_out_valid_i     <= XTOB_eg_valid_i(j)(i) AND wr_en_i;    -- on valid save in FIFO
                        TOBs_out_i          <= XTOB_eg_i(j)((((i*64)+64)-1) downto (i*64)+32);
                        if (i = 4) then    -- 10*32b XTOB from (5 * 64b XTOB)
                           i <= 0 ;
						   if (j = 7) then
						      j <= 0 ;
						      current_state        <= rdout_XTOB_tau_1 ;
							else
							   j <= j + 1 ; 
							   current_state        <= rdout_XTOB_eg_1 ;
							end if;
                        else
                           current_state        <= rdout_XTOB_eg_1 ;
                           i <= i + 1 ;
                        end if;
                        
                    when rdout_XTOB_tau_1 =>         -- normal X_TOB_eg rd out 80 clks
--						tst_fsm_cntr_i      <= X"00000008";
						XTOB_eg_cntr_en_i   <= '0';    -- to prevent a 1 on last data to persist
						XTOB_tau_cntr_en_i  <= XTOB_tau_valid_i(j)(i) AND wr_en_i;
						TOB_out_valid_i     <= XTOB_tau_valid_i(j)(i) AND wr_en_i;    -- on valid save in FIFO
						TOBs_out_i          <= XTOB_tau_i(j)((((i*64)+32)-1) downto (i*64));
						current_state        <= rdout_XTOB_tau_2 ;
    
					when rdout_XTOB_tau_2 =>         -- normal X_TOB_eg rd out 80 clks
--						tst_fsm_cntr_i      <= X"00000009";
						XTOB_tau_cntr_en_i  <= XTOB_tau_valid_i(j)(i) AND wr_en_i;
						TOB_out_valid_i     <= XTOB_tau_valid_i(j)(i) AND wr_en_i;    -- on valid save in FIFO
						TOBs_out_i          <= XTOB_tau_i(j)((((i*64)+64)-1) downto (i*64)+32);
                        if (i = 4) then    -- 10*32b XTOB from (5 * 64b XTOB)
                           i <= 0 ;
						   if (j = 7) then
						      j <= 0 ;
						      current_state        <= wait1 ;
							else
							   j <= j + 1 ; 
							   current_state        <= rdout_XTOB_tau_1 ;
							end if;
						else
						   current_state        <= rdout_XTOB_tau_1 ;
						   i <= i + 1 ;
						end if;
                           
                    when wait1 =>       -- wait for delayed counter to be valid in sub_trl_1 
--                        tst_fsm_cntr_i      <= X"0000000C";
                        XTOB_tau_cntr_en_i  <= '0' ;    -- to prevent a 1 on last data to persist
                        TOB_out_valid_i     <= '0' ;    -- do not save in FIFO
--                        TOBs_out_i          <=  X"00000000" ;
                        current_state       <= wait2 ;   -- wait for counter to count up.

                    when wait2 =>       -- wait for delayed counter to be valid in sub_trl_1 
--                        tst_fsm_cntr_i      <= X"0000000C";
                        TOB_out_valid_i     <= '0' ;    -- do not save in FIFO
--                        TOBs_out_i          <=  X"00000000" ;
                        current_state       <= sub_trl_1 ;   -- wait for counter to count up.

                    when sub_trl_1 =>            -- ZERO data trailer
--                        tst_fsm_cntr_i      <= X"0000000A";
                        TOB_rdout_fifo_rd_en_i  <= '0';         -- stop read from FIFO
                        slice_no_txed           <= std_logic_vector(to_unsigned(m_s_count,3));  --  1st slice is ZERO
                        m_s_count               <= m_s_count + 1 ;      -- increment multi slice readout count 
                        TOB_out_valid_i         <= '0' ;    -- do not save in FIFO
                        if TOB_payld_cntr_1dly(0) = '0' then       -- no. of 32b words even so send a ZERO word
                            TOB_out_is_char_i       <= '0';
                            TOB_out_valid_i         <= '1' ;
                            TOBs_out_i              <=  X"00000000" ;
                            trailer_cntr_en         <= '1' ;    -- increment the payload counter to count the ZERO word
                        end if;
                        current_state           <= sub_trl_2 ;
                        
                    when sub_trl_2 =>            -- normal data trailer
--                        tst_fsm_cntr_i      <= X"0000000B";
                        trailer_cntr_en         <= '1' ;    -- increment the payload counter to count the trailer.
                        cntr_rst_i              <= '1' ;    -- reset the TOB counters

                        -- no. of 32b words is odd so just send trailer which makes even
                        TOB_out_is_char_i       <= '0';
                        TOB_out_valid_i         <= '1' ;
                        -- 3b err + 7b 0 + 2b FPGA no + 1b TSM + 3b Slice + 6b 64b Tau XTOB  + 6b 64b e/g XTOB + 3b 32b TOB cntr + 1b TOB type
                        TOBs_out_i              <=  link_err_flg_in_i(3 downto 0) & "000000" & hw_addr & safe_mode_i & slice_no_txed & 
                                                    XTOB_tau_cntr_1dly(6 downto 1) & XTOB_eg_cntr_1dly(6 downto 1) & "000" & TOB_type_in;
                        i <= 0 ;
                        if m_s_count = slice_count_i then
                            safe_mode_i           <= '0' ;    -- clear safe mode flag
                            current_state           <= wait3 ;
                        else    -- if not in Safe Mode, normal read
                            if safe_mode_i = '0' then 
                                miss_sop_i              <= '1' ;        -- miss SOP for multi-slice readout
                                current_state           <= rd_fifo ;    -- was idle (just in case fifo is empty ie lost sync with data in
                                TOB_rdout_fifo_rd_en_i  <= '1';         -- read next slice from FIFO
--                            m_s_count    <= m_s_count + 1 ;         -- used to count multi slice readout
                            else    -- if in Safe Mode, empty buffers
                                miss_sop_i              <= '1' ;        -- miss SOP for multi-slice readout
                                current_state           <= sub_trl_1 ;    -- empty next slice in Safe Mode
                                TOB_rdout_fifo_rd_en_i  <= '1';         -- read next slice from FIFO
                            end if;
                        end if;

                    when wait3 =>        
--                        tst_fsm_cntr_i      <= X"0000000C";
                        trailer_cntr_en     <= '0' ; 
                        m_s_count           <=  0 ;
                        cntr_rst_i          <= '0' ; 
                        TOB_out_valid_i     <= '0' ;
                        current_state       <= wait4 ;   -- wait for counter to count up.

                    when wait4 =>        
--                        tst_fsm_cntr_i      <= X"0000000C";
                        trailer_cntr_en     <= '0' ; 
                        m_s_count           <=  0 ;
                        cntr_rst_i          <= '0' ; 
                        TOB_out_valid_i     <= '0' ;
                        current_state       <= norm_eop ;   -- extra delay to remove timing error.

                    when norm_eop =>        -- normal end of packet
--                        tst_fsm_cntr_i      <= X"0000000D";
                        frame_cntr_en_i     <= '1' ;    -- inclrease frame counter by 1
                        payld_cntr_rst_i    <= '1' ;    -- reset the pay loadcounters
                        TOB_out_is_char_i   <= '1';
                        TOBs_out_i          <= X"000" & TOB_payld_cntr_1dly(11 downto 0) & ch_eop ; -- pay load cntr is divided by 2 to show number of 64b owrds
                        TOB_out_valid_i     <= '1' ;
                        i <= 0 ;
                        current_state       <= idle ;
                        
                    when others =>
                        NULL;
                end case;
            end if;
        END IF;           
    end process;
end Behavioral;
