----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 19.07.2018 16:58:33
-- Design Name: 
-- Module Name: test_tx_flow - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library infrastructure_lib;
use infrastructure_lib.all;
use infrastructure_lib.synch_type.all;

entity test_tx_flow is
--  Port ( );
end test_tx_flow;

architecture Behavioral of test_tx_flow is
-- clock
constant clk_prd_280:time :=  3.57 ns ;
  -- component generics
--	constant ERROR_BITS: natural := 32;
 -- component ports	
signal reset,iclk280,clk280:std_logic;    
--signal tob_data           :std_logic_vector(31 downto 0):=x"ffffffff";  

 
--signal rx_data      :  mgt_data;

--signal inject_error :  std_logic := '0';
--signal clear_error,comma_detect_ILA  :  std_logic := '0';
--signal error_count  :  unsigned(ERROR_BITS-1 downto 0);
--signal error_counter :std_logic_vector(191 downto 0 );
--signal txdatai_0  : std_logic_vector(33 downto 0 );
--signal txclk  : std_logic_vector(0 downto 0 ) ;

signal txdatai_0   : std_logic_vector(33 downto 0 );
signal txdatai_1   : std_logic_vector(33 downto 0 );
signal txdatai_2   : std_logic_vector(33 downto 0 );
signal txdatai_3   : std_logic_vector(33 downto 0 );
signal txdatai_4   : std_logic_vector(33 downto 0 );
signal txdatai_5   : std_logic_vector(33 downto 0 );
signal txdatai_6   : std_logic_vector(33 downto 0 );
signal txdatai_7   : std_logic_vector(33 downto 0 );
signal txdatai_8   : std_logic_vector(33 downto 0 );
signal txdatai_9   : std_logic_vector(33 downto 0 );
signal txdatai_10  : std_logic_vector(33 downto 0 );
signal txdatai_11  : std_logic_vector(33 downto 0 );

signal topo_data    : std_logic_vector(31 downto 0 );
signal sorted_valid : std_logic := '0';
signal sorted_sync  : std_logic := '0';
signal bcn          : std_logic_vector(3 downto 0 );



begin 


--txclk(0) <= clk280;



test_bench:  entity infrastructure_lib.testbench_phase

    Port map (           
        reset           => reset , -- reset         
        sorted_tob      => topo_data,   
        valid           => sorted_valid,
        synch           => sorted_sync, 
        bcn             => bcn         
        
     );

----------------------------------------  
         -- Clock genation of the 2800MHz 
------------------------------------------------  
          
       clock_gen_280: process 
        
            begin 
             --wait for 2 ns;
     clock_loop:  loop    -- This is an infinite loop  
              iclk280<= '1';               
               wait for clk_prd_280/2 ; 
               iclk280 <= '0' ;                
               wait for clk_prd_280/2 ; 
          end loop clock_loop;      
             end process clock_gen_280 ; 
             clk280 <= iclk280 ;      
            
    
 
tx_phase_adjust: entity infrastructure_lib.efex_topo_tx 
      generic map (NCOUNTERS => 12)
         port map(
              clk280       => clk280    ,   --! clock 280MHz 
              reset        => reset,                
              tx_clk       => clk280 & clk280 & clk280 & clk280 & clk280 & clk280 & clk280 & clk280 & clk280 & clk280 & clk280 & clk280,
              rst          => reset,
              bcn          => bcn , --"1111",
              sorted_sync  => sorted_sync, --'1' ,--! Output sync, high on the first clock cycle of the BC 
              tob_data     => topo_data,   --x"12345678" ,  --! Algorithm external TOB data structure, defined in AlgoDataTypes.vhd 
              sorted_valid => sorted_valid,-- '1' , --! Output data valid, high when correspondent output data are valid
              tx_datai_0   => txdatai_0,   --!txdata of quad 111 mgt0
              tx_datai_1   => txdatai_1   , --!txdata of quad 111 mgt1
              tx_datai_2   => txdatai_2   , --!txdata of quad 111 mgt2
              tx_datai_3   => txdatai_3   , --!txdata of quad 111 mgt3
              tx_datai_4   => txdatai_4   , --!txdata of quad 112 mgt0
              tx_datai_5   => txdatai_5   , --!txdata of quad 112 mgt1
              tx_datai_6   => txdatai_6   , --!txdata of quad 112 mgt2
              tx_datai_7   => txdatai_7   , --!txdata of quad 112 mgt3
              tx_datai_8   => txdatai_8   , --!txdata of quad 211 mgt0
              tx_datai_9   => txdatai_9   , --!txdata of quad 211 mgt1
              tx_datai_10  => txdatai_10  , --!txdata of quad 211 mgt2
              tx_datai_11  => txdatai_11    --!txdata of quad 211 mgt3


 );





end Behavioral;
