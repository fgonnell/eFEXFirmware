-- 
-- 2017/04/26: re-synchronises command to output clock using req/ack handshake

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity command_sync is
    port (	
    ipb_clk 	        : in std_logic;
    out_clk 	        : in std_logic; -- derived from ipb_clk and slower
    reset		        : in std_logic;	
    req                 : in std_logic; -- positive edge sensitive
    ack                 : in std_logic;
    kick    		    : out std_logic := '0' -- kick downstream state machine
	 );
end command_sync ;

architecture rtl of command_sync is

signal do_req          : std_logic := '0';

type command_state is 		( idle, request, done  );
signal sequencer 		: command_state := idle;

begin

-------------------------------------------------------------------------------
-- state machine transitions
-------------------------------------------------------------------------------

new_command: process(ipb_clk, reset, sequencer, req, ack)
    begin
    if falling_edge(ipb_clk) then -- otherwise do_req transitions would coincide with out_clk
			do_req <= '0';
        if reset = '1' then
            sequencer <= idle; 
        else         
            
        case sequencer is 	
            when idle =>
                if (req = '1' ) then 
                    sequencer <= request; 
                end if;
            when request => 
					do_req <= '1';
                if (ack = '1' ) then 
                    sequencer <= done; 
                end if;
            when done =>
                if (req = '0' ) then 
                    sequencer <= idle; 
                end if;
            when others => 
                    sequencer <= idle; 
        end case;
    end if;
    end if;
    end process new_command;


command_sync: process(out_clk, reset)
    begin
    if rising_edge(out_clk) then 
       kick <= do_req;
        end if;         
    end process command_sync;

end;


