set_property PACKAGE_PIN BD41 [get_ports {txp_OUT[76]}]
set_property PACKAGE_PIN BD42 [get_ports {txn_OUT[76]}]
set_property PACKAGE_PIN BB41 [get_ports {txp_OUT[77]}]
set_property PACKAGE_PIN BB42 [get_ports {txn_OUT[77]}]

# Unconnected tx MGTs
set_property IOSTANDARD LVCMOS18 [get_ports {tx?_OUT[4?] tx?_OUT[50] tx?_OUT[51] tx?_OUT[60] tx?_OUT[61] tx?_OUT[62] tx?_OUT[63]}]

set_property PACKAGE_PIN E23 [get_ports {txp_OUT[40]}]
set_property PACKAGE_PIN E24 [get_ports {txn_OUT[40]}]
set_property PACKAGE_PIN G23 [get_ports {txp_OUT[41]}]
set_property PACKAGE_PIN F23 [get_ports {txn_OUT[41]}]
set_property PACKAGE_PIN F24 [get_ports {txp_OUT[42]}]
set_property PACKAGE_PIN F25 [get_ports {txn_OUT[42]}]
set_property PACKAGE_PIN H23 [get_ports {txp_OUT[43]}]
set_property PACKAGE_PIN H24 [get_ports {txn_OUT[43]}]

set_property PACKAGE_PIN G25 [get_ports {txp_OUT[44]}]
set_property PACKAGE_PIN G26 [get_ports {txn_OUT[44]}]
set_property PACKAGE_PIN J24 [get_ports {txp_OUT[45]}]
set_property PACKAGE_PIN H25 [get_ports {txn_OUT[45]}]
set_property PACKAGE_PIN J25 [get_ports {txp_OUT[46]}]
set_property PACKAGE_PIN J26 [get_ports {txn_OUT[46]}]
set_property PACKAGE_PIN K23 [get_ports {txp_OUT[47]}]
set_property PACKAGE_PIN K24 [get_ports {txn_OUT[47]}]

set_property PACKAGE_PIN L24 [get_ports {txp_OUT[48]}]
set_property PACKAGE_PIN L25 [get_ports {txn_OUT[48]}]
set_property PACKAGE_PIN L26 [get_ports {txp_OUT[49]}]
set_property PACKAGE_PIN K26 [get_ports {txn_OUT[49]}]
set_property PACKAGE_PIN N23 [get_ports {txp_OUT[50]}]
set_property PACKAGE_PIN N24 [get_ports {txn_OUT[50]}]
set_property PACKAGE_PIN N25 [get_ports {txp_OUT[51]}]
set_property PACKAGE_PIN M25 [get_ports {txn_OUT[51]}]

set_property PACKAGE_PIN M23 [get_ports {txp_OUT[60]}]
set_property PACKAGE_PIN L23 [get_ports {txn_OUT[60]}]
set_property PACKAGE_PIN T26 [get_ports {txp_OUT[61]}]
set_property PACKAGE_PIN R26 [get_ports {txn_OUT[61]}]
set_property PACKAGE_PIN P25 [get_ports {txp_OUT[62]}]
set_property PACKAGE_PIN P26 [get_ports {txn_OUT[62]}]
set_property PACKAGE_PIN U25 [get_ports {txp_OUT[63]}]
set_property PACKAGE_PIN U26 [get_ports {txn_OUT[63]}]

### MGT asynchronous groups
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[0]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[0]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[0]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[0]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[0]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[0]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[0]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[0]*gt3_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[1]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[1]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[1]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[1]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[1]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[1]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[1]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[1]*gt3_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[2]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[2]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[2]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[2]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[2]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[2]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[2]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[2]*gt3_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[3]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[3]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[3]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[3]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[3]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[3]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[3]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[3]*gt3_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[4]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[4]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[4]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[4]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[4]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[4]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[4]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[4]*gt3_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[5]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[5]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[5]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[5]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[5]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[5]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[5]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[5]*gt3_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[6]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[6]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[6]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[6]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[6]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[6]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[6]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[6]*gt3_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[7]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[7]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[7]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[7]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[7]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[7]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[7]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[7]*gt3_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[8]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[8]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[8]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[8]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[8]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[8]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[8]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[8]*gt3_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[9]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[9]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[9]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[9]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[9]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[9]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[9]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[9]*gt3_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[10]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[10]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[10]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[10]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[10]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[10]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[10]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[10]*gt3_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[11]*gt0_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[11]*gt0_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[11]*gt1_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[11]*gt1_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[11]*gt2_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[11]*gt2_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[11]*gt3_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[11]*gt3_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[12]*gt0_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[12]*gt0_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[12]*gt1_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[12]*gt1_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[12]*gt2_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[12]*gt2_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[12]*gt3_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[12]*gt3_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[13]*gt0_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[13]*gt0_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[13]*gt1_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[13]*gt1_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[13]*gt2_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[13]*gt2_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[13]*gt3_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[13]*gt3_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[14]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[14]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[14]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[14]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[14]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[14]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[14]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[14]*gt3_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[15]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[15]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[15]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[15]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[15]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[15]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[15]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[15]*gt3_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[16]*gt0_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[16]*gt0_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[16]*gt1_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[16]*gt1_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[16]*gt2_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[16]*gt2_min_latency*RXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[16]*gt3_min_latency*TXOUTCLK}]]
#set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[16]*gt3_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[17]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[17]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[17]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[17]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[17]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[17]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[17]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[17]*gt3_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[18]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[18]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[18]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[18]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[18]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[18]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[18]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[18]*gt3_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[19]*gt0_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[19]*gt0_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[19]*gt1_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[19]*gt1_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[19]*gt2_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[19]*gt2_min_latency*RXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[19]*gt3_min_latency*TXOUTCLK}]]
set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*MGT_GEN[19]*gt3_min_latency*RXOUTCLK}]]

