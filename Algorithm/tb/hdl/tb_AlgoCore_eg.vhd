library IEEE;
use IEEE.STD_LOGIC_1164.all;

use IEEE.NUMERIC_STD.all;

library algolib;
use algolib.DataTypes.all;
use algolib.AlgoDataTypes.all;

entity tb_AlgoCore_eg is
--  Port ( );
end tb_AlgoCore_eg;

architecture TestBench of tb_AlgoCore_eg is
-- clocks
  signal clk40  : std_logic;
  signal clk160 : std_logic;
  signal clk200 : std_logic;
  signal rst    : std_logic;

-- data signals
  signal TowerData  : TriggerTowers(8 downto 0);
  signal DataReady  : std_logic;
  signal input_data : TriggerTowers(8 downto 0);
  signal output_tob : TriggerObject;

  --signal std_output_tob : std_logic_vector (63 downto 0);

-- parameters and registers
  signal ParD3     : AlgoParameters(2 downto 0);
  signal ParCEta   : AlgoParameters(2 downto 0);
  signal ParHadron : AlgoParameters(2 downto 0);
  signal Control   : AlgoRegister;
  signal Status    : AlgoRegister;

begin

  input_data <= TowerData;
  ParD3      <= (0 => x"01", 1 => x"02", 2 => x"03");
  ParCEta    <= (0 => x"01", 1 => x"02", 2 => x"03");
  ParHadron  <= (0 => x"01", 1 => x"02", 2 => x"03");
  Control    <= x"00000000";

  --std_output_tob <= to_LogicVector2(to_AlgoXTriggerObject(output_tob));


  TESTER : entity work.FileReaderSingle
    port map (
      DATA_OUT   => TowerData,
      DATA_READY => DataReady,
      CLK40      => clk40,
      CLK160     => clk160,
      CLK200     => clk200,
      RESET      => rst);

  EG_ALGO : entity algolib.AlgoCore_eg
    port map (
      CLK200       => clk200,
      IN_ParD3     => ParD3,
      IN_ParCEta   => ParCEta,
      IN_ParHadron => ParHadron,
      IN_Control   => Control,
      OUT_Status   => Status,
      IN_Data      => input_data,
      OUT_TOB      => output_tob);

  EG_TOB_FileWriter : entity work.FileWriterSingle
    generic map (
      filename   => "../../TOB_single_eg.txt")
    port map (
      CLK     => clk200,
      RST     => rst,
      TOB_IN => output_tob,
      WE      => '1');


end TestBench;
