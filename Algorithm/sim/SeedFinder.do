onerror {resume}
quietly virtual signal -install /tb_topalgo_eg { /tb_topalgo_eg/output_tob(3 downto 0)} Is_Max
quietly virtual signal -install /tb_topalgo_eg { /tb_topalgo_eg/output_tob(7 downto 4)} Up_not_down
quietly virtual signal -install /tb_topalgo_eg { /tb_topalgo_eg/output_tob(9 downto 8)} Seed
quietly WaveActivateNextPane {} 0
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/clk40
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/clk160
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/clk200
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/rst
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/enable
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/TowerData
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/DataReady
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/input_data
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/output_tob
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/clk40
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/clk160
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/clk200
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/rst
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/enable
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/TowerData
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/DataReady
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/input_data
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/output_tob
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/clk40
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/clk160
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/clk200
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/rst
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/enable
add wave -noupdate -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(8) -radix hexadecimal} {/tb_topalgo_eg/TowerData(7) -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(7).Layer0 -radix hexadecimal} {/tb_topalgo_eg/TowerData(7).Layer1 -radix hexadecimal} {/tb_topalgo_eg/TowerData(7).Layer2 -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(7).Layer2(3) -radix hexadecimal} {/tb_topalgo_eg/TowerData(7).Layer2(2) -radix hexadecimal} {/tb_topalgo_eg/TowerData(7).Layer2(1) -radix hexadecimal} {/tb_topalgo_eg/TowerData(7).Layer2(0) -radix hexadecimal}}} {/tb_topalgo_eg/TowerData(7).Layer3 -radix hexadecimal} {/tb_topalgo_eg/TowerData(7).Hadron -radix hexadecimal}}} {/tb_topalgo_eg/TowerData(6) -radix hexadecimal} {/tb_topalgo_eg/TowerData(5) -radix hexadecimal} {/tb_topalgo_eg/TowerData(4) -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(4).Layer0 -radix hexadecimal} {/tb_topalgo_eg/TowerData(4).Layer1 -radix hexadecimal} {/tb_topalgo_eg/TowerData(4).Layer2 -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(4).Layer2(3) -radix hexadecimal} {/tb_topalgo_eg/TowerData(4).Layer2(2) -radix hexadecimal} {/tb_topalgo_eg/TowerData(4).Layer2(1) -radix hexadecimal} {/tb_topalgo_eg/TowerData(4).Layer2(0) -radix hexadecimal}}} {/tb_topalgo_eg/TowerData(4).Layer3 -radix hexadecimal} {/tb_topalgo_eg/TowerData(4).Hadron -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(4).Hadron(0) -radix hexadecimal}}}}} {/tb_topalgo_eg/TowerData(3) -radix hexadecimal} {/tb_topalgo_eg/TowerData(2) -radix hexadecimal} {/tb_topalgo_eg/TowerData(1) -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(1).Layer0 -radix hexadecimal} {/tb_topalgo_eg/TowerData(1).Layer1 -radix hexadecimal} {/tb_topalgo_eg/TowerData(1).Layer2 -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(1).Layer2(3) -radix hexadecimal} {/tb_topalgo_eg/TowerData(1).Layer2(2) -radix hexadecimal} {/tb_topalgo_eg/TowerData(1).Layer2(1) -radix hexadecimal} {/tb_topalgo_eg/TowerData(1).Layer2(0) -radix hexadecimal}}} {/tb_topalgo_eg/TowerData(1).Layer3 -radix hexadecimal} {/tb_topalgo_eg/TowerData(1).Hadron -radix hexadecimal}}} {/tb_topalgo_eg/TowerData(0) -radix hexadecimal}} -radixshowbase 0 -subitemconfig {/tb_topalgo_eg/TowerData(8) {-height 15 -radix hexadecimal -radixshowbase 1} /tb_topalgo_eg/TowerData(7) {-height 15 -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(7).Layer0 -radix hexadecimal} {/tb_topalgo_eg/TowerData(7).Layer1 -radix hexadecimal} {/tb_topalgo_eg/TowerData(7).Layer2 -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(7).Layer2(3) -radix hexadecimal} {/tb_topalgo_eg/TowerData(7).Layer2(2) -radix hexadecimal} {/tb_topalgo_eg/TowerData(7).Layer2(1) -radix hexadecimal} {/tb_topalgo_eg/TowerData(7).Layer2(0) -radix hexadecimal}}} {/tb_topalgo_eg/TowerData(7).Layer3 -radix hexadecimal} {/tb_topalgo_eg/TowerData(7).Hadron -radix hexadecimal}} -radixshowbase 1 -expand} /tb_topalgo_eg/TowerData(7).Layer0 {-height 15 -radix hexadecimal -radixshowbase 0} /tb_topalgo_eg/TowerData(7).Layer1 {-height 15 -radix hexadecimal -radixshowbase 0} /tb_topalgo_eg/TowerData(7).Layer2 {-height 15 -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(7).Layer2(3) -radix hexadecimal} {/tb_topalgo_eg/TowerData(7).Layer2(2) -radix hexadecimal} {/tb_topalgo_eg/TowerData(7).Layer2(1) -radix hexadecimal} {/tb_topalgo_eg/TowerData(7).Layer2(0) -radix hexadecimal}} -radixshowbase 0 -expand} /tb_topalgo_eg/TowerData(7).Layer2(3) {-height 15 -radix hexadecimal -radixshowbase 0} /tb_topalgo_eg/TowerData(7).Layer2(2) {-height 15 -radix hexadecimal -radixshowbase 0} /tb_topalgo_eg/TowerData(7).Layer2(1) {-height 15 -radix hexadecimal -radixshowbase 0} /tb_topalgo_eg/TowerData(7).Layer2(0) {-height 15 -radix hexadecimal -radixshowbase 0} /tb_topalgo_eg/TowerData(7).Layer3 {-height 15 -radix hexadecimal -radixshowbase 0} /tb_topalgo_eg/TowerData(7).Hadron {-height 15 -radix hexadecimal -radixshowbase 1} /tb_topalgo_eg/TowerData(6) {-height 15 -radix hexadecimal -radixshowbase 1} /tb_topalgo_eg/TowerData(5) {-height 15 -radix hexadecimal -radixshowbase 1} /tb_topalgo_eg/TowerData(4) {-height 15 -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(4).Layer0 -radix hexadecimal} {/tb_topalgo_eg/TowerData(4).Layer1 -radix hexadecimal} {/tb_topalgo_eg/TowerData(4).Layer2 -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(4).Layer2(3) -radix hexadecimal} {/tb_topalgo_eg/TowerData(4).Layer2(2) -radix hexadecimal} {/tb_topalgo_eg/TowerData(4).Layer2(1) -radix hexadecimal} {/tb_topalgo_eg/TowerData(4).Layer2(0) -radix hexadecimal}}} {/tb_topalgo_eg/TowerData(4).Layer3 -radix hexadecimal} {/tb_topalgo_eg/TowerData(4).Hadron -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(4).Hadron(0) -radix hexadecimal}}}} -radixshowbase 1 -expand} /tb_topalgo_eg/TowerData(4).Layer0 {-height 15 -radix hexadecimal -radixshowbase 1} /tb_topalgo_eg/TowerData(4).Layer1 {-height 15 -radix hexadecimal -radixshowbase 1} /tb_topalgo_eg/TowerData(4).Layer2 {-height 15 -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(4).Layer2(3) -radix hexadecimal} {/tb_topalgo_eg/TowerData(4).Layer2(2) -radix hexadecimal} {/tb_topalgo_eg/TowerData(4).Layer2(1) -radix hexadecimal} {/tb_topalgo_eg/TowerData(4).Layer2(0) -radix hexadecimal}} -radixshowbase 0 -expand} /tb_topalgo_eg/TowerData(4).Layer2(3) {-height 15 -radix hexadecimal -radixshowbase 0} /tb_topalgo_eg/TowerData(4).Layer2(2) {-height 15 -radix hexadecimal -radixshowbase 0} /tb_topalgo_eg/TowerData(4).Layer2(1) {-height 15 -radix hexadecimal -radixshowbase 0} /tb_topalgo_eg/TowerData(4).Layer2(0) {-height 15 -radix hexadecimal -radixshowbase 0} /tb_topalgo_eg/TowerData(4).Layer3 {-height 15 -radix hexadecimal -radixshowbase 1} /tb_topalgo_eg/TowerData(4).Hadron {-height 15 -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(4).Hadron(0) -radix hexadecimal}} -radixshowbase 1 -expand} /tb_topalgo_eg/TowerData(4).Hadron(0) {-height 15 -radix hexadecimal -radixshowbase 1} /tb_topalgo_eg/TowerData(3) {-height 15 -radix hexadecimal -radixshowbase 1} /tb_topalgo_eg/TowerData(2) {-height 15 -radix hexadecimal -radixshowbase 1} /tb_topalgo_eg/TowerData(1) {-height 15 -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(1).Layer0 -radix hexadecimal} {/tb_topalgo_eg/TowerData(1).Layer1 -radix hexadecimal} {/tb_topalgo_eg/TowerData(1).Layer2 -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(1).Layer2(3) -radix hexadecimal} {/tb_topalgo_eg/TowerData(1).Layer2(2) -radix hexadecimal} {/tb_topalgo_eg/TowerData(1).Layer2(1) -radix hexadecimal} {/tb_topalgo_eg/TowerData(1).Layer2(0) -radix hexadecimal}}} {/tb_topalgo_eg/TowerData(1).Layer3 -radix hexadecimal} {/tb_topalgo_eg/TowerData(1).Hadron -radix hexadecimal}} -radixshowbase 1 -expand} /tb_topalgo_eg/TowerData(1).Layer0 {-height 15 -radix hexadecimal -radixshowbase 1} /tb_topalgo_eg/TowerData(1).Layer1 {-height 15 -radix hexadecimal -radixshowbase 1} /tb_topalgo_eg/TowerData(1).Layer2 {-height 15 -radix hexadecimal -childformat {{/tb_topalgo_eg/TowerData(1).Layer2(3) -radix hexadecimal} {/tb_topalgo_eg/TowerData(1).Layer2(2) -radix hexadecimal} {/tb_topalgo_eg/TowerData(1).Layer2(1) -radix hexadecimal} {/tb_topalgo_eg/TowerData(1).Layer2(0) -radix hexadecimal}} -radixshowbase 0 -expand} /tb_topalgo_eg/TowerData(1).Layer2(3) {-height 15 -radix hexadecimal -radixshowbase 0} /tb_topalgo_eg/TowerData(1).Layer2(2) {-height 15 -radix hexadecimal -radixshowbase 0} /tb_topalgo_eg/TowerData(1).Layer2(1) {-height 15 -radix hexadecimal -radixshowbase 0} /tb_topalgo_eg/TowerData(1).Layer2(0) {-height 15 -radix hexadecimal -radixshowbase 0} /tb_topalgo_eg/TowerData(1).Layer3 {-height 15 -radix hexadecimal -radixshowbase 1} /tb_topalgo_eg/TowerData(1).Hadron {-height 15 -radix hexadecimal -radixshowbase 1} /tb_topalgo_eg/TowerData(0) {-height 15 -radix hexadecimal -radixshowbase 1}} /tb_topalgo_eg/TowerData
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/DataReady
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/input_data
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/output_tob
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/Is_Max
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/Up_not_down
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/Seed
add wave -noupdate -divider SeedFinder
add wave -noupdate -expand -group SeedFinder -radixshowbase 0 /tb_topalgo_eg/EG_ALGO/SEED_FINDER/CLK
add wave -noupdate -expand -group SeedFinder -radix hexadecimal -radixshowbase 0 /tb_topalgo_eg/EG_ALGO/SEED_FINDER/RESET
add wave -noupdate -expand -group SeedFinder -radixshowbase 0 /tb_topalgo_eg/EG_ALGO/SEED_FINDER/ENABLE
add wave -noupdate -expand -group SeedFinder -radixshowbase 0 /tb_topalgo_eg/EG_ALGO/SEED_FINDER/IN_Data
add wave -noupdate -expand -group SeedFinder -radixshowbase 0 /tb_topalgo_eg/EG_ALGO/SEED_FINDER/IN_DataUp
add wave -noupdate -expand -group SeedFinder -radixshowbase 0 /tb_topalgo_eg/EG_ALGO/SEED_FINDER/IN_DataDown
add wave -noupdate -expand -group SeedFinder -radixshowbase 0 /tb_topalgo_eg/EG_ALGO/SEED_FINDER/Result
add wave -noupdate -expand -group SeedFinder -radixshowbase 0 /tb_topalgo_eg/EG_ALGO/SEED_FINDER/IsMaxMax
add wave -noupdate -expand -group SeedFinder -radixshowbase 0 /tb_topalgo_eg/EG_ALGO/SEED_FINDER/UpOrDown
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/EG_ALGO/SEED_FINDER/OUT_UpNotDown
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/EG_ALGO/SEED_FINDER/OUT_Seed
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/EG_ALGO/SEED_FINDER/OUT_IsLocalMax
add wave -noupdate -radixshowbase 0 /tb_topalgo_eg/EG_ALGO/SEED_FINDER/OUT_IsMax
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {164808 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 213
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix -1
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 625
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {432551 ps}
