--! @file
--! @brief Little Seed Finder for the tau algorithm
--! @details 
--! The little seed finder module finds the center of the tau cluster using the 4 SC in layer2 of the central tower
--! Up (7) and down(1) SC in layer 2 are also used to ifne the Up/Down phi asymmetry.
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
use work.DataTypes.all;

--! @copydoc LittleSeedFinder.vhd
entity LittleSeedFinder is
  port (
    CLK           : in  std_logic;
    IN_Data       : in  DataWords(3 downto 0);
    IN_DataUp     : in  DataWords(3 downto 0);
    IN_DataDown   : in  DataWords(3 downto 0);
    OUT_UpNotDown : out std_logic;  --1 => up, 0 => down
    OUT_Seed      : out std_logic_vector(1 downto 0)
    );

end LittleSeedFinder;

architecture Behavioral of LittleSeedFinder is

  signal Seed     : std_logic_vector(1 downto 0);
  signal UpOrDown : std_logic_vector(3 downto 0);

--! @copydoc LittleSeedFinder.vhd
begin

  process (CLK)
  begin
    if rising_edge(CLK) then
      -- first clock cycle         
      if (IN_Data(3) > IN_Data(2)) and (IN_Data(3) > IN_Data(1)) and (IN_Data(3) > IN_Data(0)) then
        Seed <= "11";
      elsif (IN_Data(2) > IN_Data(1)) and (IN_Data(2) > IN_Data(0)) then
        Seed <= "10";
      elsif (IN_Data(1) > IN_Data(0)) and (not (IN_Data(1) < IN_Data(3))) then
        Seed <= "01";
      else
        Seed <= "00";
      end if;

      UpOrDown(0) <= '0' when IN_DataDown(0) > IN_DataUp(0) else '1';
      UpOrDown(1) <= '0' when IN_DataDown(1) > IN_DataUp(1) else '1';
      UpOrDown(2) <= '0' when IN_DataDown(2) > IN_DataUp(2) else '1';
      UpOrDown(3) <= '0' when IN_DataDown(3) > IN_DataUp(3) else '1';

      -- second clock cycle
      OUT_UpNotDown <= UpOrDown(to_integer(unsigned(Seed)));
      OUT_Seed      <= Seed;
    end if;
  end process;

end Behavioral;
