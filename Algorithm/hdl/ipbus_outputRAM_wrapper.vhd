--! @file
--! @brief IPBus wrapper for output spy/playback RAM for Algorithm module
--! @details
--! This RAM contains algorithm output data, i.e. the Trigger Objects or TOBs
--! having a width of 32 bits.
--! It can be used to spy or for playback.
--! It can contain 16*5 TOBs, so TOBs produced during 16 BCs can be stored or played back.
--!
--! The internal memory, AlgoOutputRAM is a real dual port RAM Xilinx IP.
--! The Algorithm uses the RAM port b having a width of 256 bit, i.e. 8 *
--! 32-bit TOBs. In fact, the Algorithm can produce a maximum of 8 TOBs per BC.
--!
--! Ipbus uses AlgoOutputRAM port a having a width of 32 bits.
--! The depth of the RAM is in this case 16*5 = 80
--! The ipbus RAM contains data as follows:
--! | BC  |bits 0-31 |
--! |-----|----------|
--! |BC 1 |   TOB0   |
--! |BC 1 |   TOB1   |
--! |BC 1 |   TOB2   |
--! |BC 1 |   TOB3   |
--! |BC 1 |   TOB4   |
--! |BC 2 |   TOB0   |
--! |BC 2 |   TOB1   |
--! |BC 2 |   TOB2   |
--! |BC 2 |   TOB3   |
--! |BC 2 |   TOB4   |
--! |BC...|   TOB0   |
--! |BC...|   TOB1   |
--! |BC...|   TOB2   |
--! |BC...|   TOB3   |
--! |BC...|   TOB4   |
--! |BC 16|   TOB0   |
--! |BC 16|   TOB1   |
--! |BC 16|   TOB2   |
--! |BC 16|   TOB3   |
--! |BC 16|   TOB4   |
--!
--! @author Francesco Gonnella

--! Use standard libraries
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
--! Use ipbus libarary
library ipbus_lib;

--! Use ipbus data-types
use ipbus_lib.ipbus.all;
--! Use internal data-types
use work.DataTypes.all;
--! Use external data-types
use work.AlgoDataTypes.all;

--! @copydoc ipbus_outputRAM_wrapper.vhd
entity ipbus_outputRAM_wrapper is
  port(
    clk_ipb : in  std_logic;
    rst     : in  std_logic;
    ipb_in  : in  ipb_wbus;
    ipb_out : out ipb_rbus;
    AlgoIn  : in  AlgoOutput;

    BCIDIn    : in BCID_t;
    SpyBCIDIn : in std_logic := '0';
    
    rclk    : in  std_logic;
    Sync    : in  std_logic;
    we      : in  std_logic := '0';
    AlgoOut : out AlgoTriggerObjects(7 downto 0)
    );

end ipbus_outputRAM_wrapper;

--! @copydoc ipbus_outputRAM_wrapper.vhd
architecture rtl of ipbus_outputRAM_wrapper is

  component AlgoOutputRAM
    port (
      clka  : in  std_logic;
      ena   : in  std_logic;
      wea   : in  std_logic_vector(0 downto 0);
      addra : in  std_logic_vector(9 downto 0);
      dina  : in  std_logic_vector(31 downto 0);
      douta : out std_logic_vector(31 downto 0);
      clkb  : in  std_logic;
      enb   : in  std_logic;
      web   : in  std_logic_vector(0 downto 0);
      addrb : in  std_logic_vector(6 downto 0); --! Address space to contain 16 BCs*5TOBS
      dinb  : in  std_logic_vector(255 downto 0);
      doutb : out std_logic_vector(255 downto 0)
      );
  end component;


  -- algorithm signals
  signal din                          : std_logic_vector(255 downto 0);
  signal q                            : std_logic_vector(255 downto 0);
  signal counter                      : std_logic_vector(6 downto 0) := "0000000";  --16 BCs*5TOBs
  signal BC_counter                   : std_logic_vector(3 downto 0) := "0000";  --16 BCs*5TOBs
  signal S1                           : std_logic                    := '0';
  signal AlgoOut3, AlgoOut2, AlgoOut1 : AlgoTriggerObjects(7 downto 0);


  --ipbus signals
  signal ack          : std_logic;
  signal ack2         : std_logic;
  signal ipbus_write  : std_logic_vector(0 downto 0);
  signal write_enable : std_logic_vector(0 downto 0);

begin
  IPBUS_RAM : process(clk_ipb)
  begin
    if rising_edge(clk_ipb) then
      if ipb_in.ipb_strobe = '1' and ipb_in.ipb_write = '1' then
        ipbus_write(0) <= '1';
      else
        ipbus_write(0) <= '0';
      end if;
      ack2 <= ipb_in.ipb_strobe and (not ack2) and (not ack);
      ack  <= ack2;

    end if;
  end process;

  ipb_out.ipb_ack <= ack;
  ipb_out.ipb_err <= '0';
  write_enable(0) <= we;

--! Synchronize the counter with RAM, accounting for latency
  COUNTER_PROC : process(rclk)
  begin
    if rising_edge(rclk) then
      S1 <= Sync;
      if S1 = '1' then
        BC_counter <= std_logic_vector(unsigned(BC_counter) + 1);
      end if;

      if BC_Counter = "0000" and S1 = '1' then
        counter <= (others => '0');
      else
        counter <= std_logic_vector(unsigned(counter) + 1);
      end if;

      -- Output is sync'ed with next BC
      AlgoOut  <= AlgoOut1;
      AlgoOut1 <= AlgoOut2;
      AlgoOut2 <= AlgoOut3;
    end if;
  end process;

  --! Xilinx IP output RAM
  ALGO_OUTPUT_RAM : AlgoOutputRAM
    port map (
      clka  => clk_ipb,
      ena   => ipb_in.ipb_strobe,
      wea   => ipbus_write,
      addra => ipb_in.ipb_addr(9 downto 0),
      dina  => ipb_in.ipb_wdata,
      douta => ipb_out.ipb_rdata,
      clkb  => rclk,
      enb   => '1',
      web   => write_enable,
      addrb => counter,
      dinb  => din,
      doutb => q
      );

  -- Conversion between algorithm data-types and std_logic_vectors 
  AlgoOut3 <= to_AlgoTriggerObjects(to_AlgoOutput(q));

  din      <= to_LogicVector(AlgoIn) when spyBCIDIn = '0'  else to_LogicVector(AlgoIn)(din'high - 32 downto 0) & x"f" & "0" & BCIDIn & x"00fff";

end rtl;
