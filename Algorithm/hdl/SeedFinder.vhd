--! @file
--! @brief Seed Finder for the electromagnetic algorithm
--! @details
--! The seed finder mosule finds the center of the em cluster by comparing the energy values of the cells
--! @author Francesco Gonnella
--! @author Antonio Costa

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
use work.DataTypes.all;

--! @copydoc SeedFinder.vhd
entity SeedFinder is
  port (
    CLK            : in  std_logic;
    IN_Data        : in  DataWords(5 downto 0);
    IN_DataUp      : in  DataWords(5 downto 0);
    IN_DataDown    : in  DataWords(5 downto 0);
    OUT_UpNotDown  : out std_logic;  --1 => up, 0 => down
    OUT_Seed       : out std_logic_vector(1 downto 0);
    OUT_IsLocalMax : out std_logic;
    OUT_IsMax      : out std_logic);
end SeedFinder;

architecture Behavioral of SeedFinder is

  signal Result               : std_logic_vector(2 downto 0) := (others => '0');
  signal UpOrDown, UpOrDown_d : std_logic_vector(3 downto 0) := (others => '0');
  signal alpha, beta, gamma   : std_logic                    := '0';
  signal pos                  : std_logic_vector(3 downto 0) := "0000";

--! @copydoc SeedFinder.vhd
begin

  OUT_IsMax <= '1';

  process (CLK)
  begin
    if rising_edge(CLK) then
      -- first clock cycle         
      pos(0) <= '1' when (IN_Data(1) > IN_Data(0)) and not (IN_Data(2) > IN_Data(1)) and (IN_Data(1) > IN_DataUp(0)) and (IN_Data(1) > IN_DataUp(1)) and (IN_Data(1) > IN_DataUp(2)) and not (IN_DataDown(0) > IN_Data(1)) and not (IN_DataDown(1) > IN_Data(1)) and not (IN_DataDown(2) > IN_Data(1)) else '0';

      pos(1) <= '1' when (IN_Data(2) > IN_Data(1)) and not (IN_Data(3) > IN_Data(2)) and (IN_Data(2) > IN_DataUp(1)) and (IN_Data(2) > IN_DataUp(2)) and (IN_Data(2) > IN_DataUp(3)) and not (IN_DataDown(1) > IN_Data(2)) and not (IN_DataDown(2) > IN_Data(2)) and not (IN_DataDown(3) > IN_Data(2)) else '0';

      pos(2) <= '1' when (IN_Data(3) > IN_Data(2)) and not (IN_Data(4) > IN_Data(3)) and (IN_Data(3) > IN_DataUp(2)) and (IN_Data(3) > IN_DataUp(3)) and (IN_Data(3) > IN_DataUp(4)) and not (IN_DataDown(2) > IN_Data(3)) and not (IN_DataDown(3) > IN_Data(3)) and not (IN_DataDown(4) > IN_Data(3)) else '0';

      pos(3) <= '1' when (IN_Data(4) > IN_Data(3)) and not (IN_Data(5) > IN_Data(4)) and (IN_Data(4) > IN_DataUp(3)) and (IN_Data(4) > IN_DataUp(4)) and (IN_Data(4) > IN_DataUp(5)) and not (IN_DataDown(3) > IN_Data(4)) and not (IN_DataDown(4) > IN_Data(4)) and not (IN_DataDown(5) > IN_Data(4)) else '0';


      alpha <= '0' when (IN_Data(3) > IN_Data(1)) else '1'; -- '1' when pos(2) <= pos(0)
      beta  <= '0' when (IN_Data(4) > IN_Data(1)) else '1'; -- '1' when pos(3) <= pos(0)
      gamma <= '0' when (IN_Data(4) > IN_Data(2)) else '1'; -- '1' when pos(3) <= pos(1)

      UpOrDown(0) <= '0' when IN_DataDown(1) > IN_DataUp(1) else '1';
      UpOrDown(1) <= '0' when IN_DataDown(2) > IN_DataUp(2) else '1';
      UpOrDown(2) <= '0' when IN_DataDown(3) > IN_DataUp(3) else '1';
      UpOrDown(3) <= '0' when IN_DataDown(4) > IN_DataUp(4) else '1';

      -- second clock cycle
      Result <= "111" when (pos = "1000") or (pos(3) = '1' and pos(0) = '1' and beta = '0') or (pos(3) = '1' and pos(1) = '1' and gamma = '0') else
                "110" when (pos = "0100") or (pos(2) = '1' and pos(0) = '1' and alpha = '0') else
                "101" when (pos = "0010") or (pos(1) = '1' and pos(3) = '1' and gamma = '1') else
                "100" when (pos = "0001") or (pos(0) = '1' and pos(3) = '1' and beta = '1') or (pos(0) = '1' and pos(2) = '1' and alpha = '1') else
                "000";
      UpOrDown_d <= UpOrDown;



    end if;
  end process;

  OUT_IsLocalMax <= Result(2);
  -- up/down is evaluated in parallel for each of the 4 cells
  -- the correct one is picked on the basis of the seed, i.e. Result(1 downto 0)
  OUT_UpNotDown  <= UpOrDown_d(to_integer(unsigned(Result(1 downto 0))));
  OUT_Seed       <= Result(1 downto 0);

end Behavioral;
