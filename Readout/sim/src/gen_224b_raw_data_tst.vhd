----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/07/2017 12:56:22 PM
-- Design Name: 
-- Module Name: gen_32b_TOBs_tst - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use std.textio.all;
use ieee.std_logic_textio.all;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;
use TOB_rdout_lib.data_type_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity gen_224b_raw_data_tst is
generic
(
    WORDS_IN_BRAM : integer    :=   519;
    FILE_NAME :     string     :=   "FILE_NAME.DAT"
);    
port
(
    -- User Interface
    TX_DATA_OUT             : out   std_logic_vector(226 downto 0);
    RAW_data_out            : out   RAW_data_227_type;  -- array 49 x 227b input frames
    TXCTRL_OUT              : out   std_logic_vector(3 downto 0); 
    -- System Interface
    USER_CLK                : in    std_logic;      
    SYSTEM_RESET            : in    std_logic
);
end gen_224b_raw_data_tst;

architecture Behavioral of gen_224b_raw_data_tst is

    attribute DowngradeIPIdentifiedWarnings: string;
    attribute DowngradeIPIdentifiedWarnings of Behavioral : architecture is "yes";
--***********************************Parameter Declarations********************

    constant DLY : time := 0 ns;

--********************************* Wire Declarations************************** 

--    signal  tx_ctrl_i               :   std_logic_vector(7 downto 0);
--    signal  tx_data_bram_i          :   std_logic_vector(63 downto 0);
    signal  tx_data_ram_r           :   std_logic_vector(235 downto 0);
    signal  tied_to_ground_vec_i    :   std_logic_vector(31 downto 0);
    signal  tied_to_ground_i        :   std_logic;
    signal  tied_to_vcc_i           :   std_logic;
    signal  TX_DATA_OUT_i           :   std_logic_vector(226 downto 0);
    signal  RAW_data_out_i          :   RAW_data_227_type;  -- array 49 x 224b input frames

--***************************Internal signalister Declarations******************** 

    signal  read_counter_i          :   unsigned(8 downto 0);    
    signal  read_counter_conv       :   std_logic_vector(8 downto 0);    
    signal  system_reset_r          :   std_logic;
    signal  system_reset_r2         :   std_logic;
    attribute keep: string;
    attribute ASYNC_REG                     : string;
    attribute ASYNC_REG of system_reset_r   : signal is "TRUE";
    attribute ASYNC_REG of system_reset_r2  : signal is "TRUE";
    attribute keep of system_reset_r        : signal is "true";
    attribute keep of system_reset_r2       : signal is "true";

--*********************************User Defined Attribute*****************************

    type RomType is array(0 to 514) of std_logic_vector(235 downto 0);

    impure function InitRomFromFile (RomFileName : in string) return RomType is

         FILE RomFile : text open read_mode is RomFileName;
         variable RomFileLine : line;
         variable ROM : RomType;
    begin
         for i in RomType'range loop
           readline (RomFile, RomFileLine);
           hread (RomFileLine, ROM(i));
         end loop;
         return ROM;
    end function;

    -- signal ROM : RomType := InitRomFromFile("RAW_data_rom_init_tx_tst.dat");
    signal ROM : RomType := InitRomFromFile(FILE_NAME);

--*********************************Main Body of Code***************************
begin

    tied_to_ground_vec_i    <=   (others=>'0');
    tied_to_ground_i        <=   '0';
    tied_to_vcc_i           <=   '1';

    --___________ synchronizing the async reset for ease of timing simulation ________
    process( USER_CLK )
    begin
        if(USER_CLK'event and USER_CLK = '1') then
            system_reset_r <= SYSTEM_RESET after DLY; 
            system_reset_r2 <= system_reset_r after DLY; 
        end if;
    end process;

    --__________________________ Counter to read from BRAM ____________________    
    
    process( USER_CLK )
    begin
        if(USER_CLK'event and USER_CLK = '1') then
            if((system_reset_r2='1') or (read_counter_i = (WORDS_IN_BRAM-1)))then
                read_counter_i <= (others => '0') after DLY;
            else
                read_counter_i <= read_counter_i + 1 after DLY;
            end if;
        end if;
    end process;

    -- Assign TX_DATA_OUT to BRAM output

    process( USER_CLK )
    begin
        if(USER_CLK'event and USER_CLK = '1') then
            if(system_reset_r2='1') then
                TX_DATA_OUT_i      <= (others => '0') after DLY;
            else
                TX_DATA_OUT_i      <= (tx_data_ram_r(234 downto 8))  after DLY; 
            end if;
        end if;
    end process;

    -- Assign TXCTRL_OUT to BRAM output
    process( USER_CLK )
    begin
        if(USER_CLK'event and USER_CLK = '1') then
            if(system_reset_r2='1') then
                TXCTRL_OUT    <= (others => '0') after DLY;
            else
                TXCTRL_OUT    <= (tx_data_ram_r(3 downto 0)) after DLY; 
            end if;
        end if;
    end process;


    --______________________________ BRAM Inference Logic_______________________    

--    tx_data_bram_i      <= tx_data_ram_r(79 downto 16);
--    tx_ctrl_i           <= tx_data_ram_r(15 downto 8);

    read_counter_conv   <= std_logic_vector(read_counter_i);
    
    process (USER_CLK)
    begin
       if(USER_CLK'event and USER_CLK='1') then
          tx_data_ram_r <= ROM(conv_integer(read_counter_conv)) after DLY;
       end if;
    end process;
    
    

    -- array 49 x 224b input frames = 14784b
    gen_raw_data : for j in 0 to 48 generate
    
        RAW_data_out_i(j)  <=  TX_DATA_OUT_i ;
        
    end generate gen_raw_data;
        

    TX_DATA_OUT  <= TX_DATA_OUT_i after 50 ps;
    RAW_data_out <= RAW_data_out_i after 50 ps;


end Behavioral;
