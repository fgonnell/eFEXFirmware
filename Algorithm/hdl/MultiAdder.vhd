--! @file
--! @brief Multiple Adder: adds many input words in cascade
--! @details
--! This multi-sage adder works in cascade, adding 2^N numbers together, where
--! N is the number of stages.
--! The minimum latency of the block is N clock cycles (being N the number of stages)
--! A delay might be added to synchronise with outher adders, multipliers, etc..
--! The total latency is L = N + D clock cycles, where N is the number of stages and D is
--! the delay.
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
use work.DataTypes.all;

--! @copydoc MultiAdder.vhd
entity MultiAdder is
  generic (stage : integer := 4;
           delay : integer := 0
           );

  port (
    CLK          : in  std_logic;  --! 200 MHz clock
    IN_Words     : in  DataWords((2**stage)-1 downto 0);
    OUT_Overflow : out std_logic;
    OUT_Word     : out DataWord
    );
end MultiAdder;

--! @copydoc MultiAdder.vhd
architecture Behavioral of MultiAdder is
  signal connector       : DataWords((2**(stage+1))-2 downto 0);
  signal carry           : std_logic_vector((2**(stage+1))-2 downto 0);
  signal DelayedOut      : DataWords(delay downto 0)        := (others => (others => '0'));
  signal DelayedOverflow : std_logic_vector(delay downto 0) := (others => '0');

begin

  stage_gen : for s in 0 to (stage - 1) generate
    adder_gen : for i in 0 to ((2**s) - 1) generate
      ADD : entity work.Adder port map (
        CLK       => CLK,
        IN_Carry  => carry(2*(2**s + i) downto 2*(2**s + i) - 1),
        OUT_Carry => carry((2**s) - 1 + i),
        IN_Words  => connector(2*(2**s + i) downto 2*(2**s + i) - 1),
        OUT_Word  => connector((2**s) - 1 + i)
        );
    end generate adder_gen;
  end generate stage_gen;


  connector((2**(stage+1) - 2) downto ((2**stage) - 1)) <= IN_Words;
  carry((2**(stage+1) - 2) downto ((2**stage) - 1))     <= (others => '0');

  dalay_proc : process (CLK)
  begin
    if rising_edge(CLK) then
      DelayedOut(DelayedOut'high)                      <= connector(0);
      DelayedOverflow(DelayedOverflow'high)            <= carry(0);
      DelayedOverflow(DelayedOverflow'high-1 downto 0) <= DelayedOverflow(DelayedOverflow'high downto 1);
      DelayedOut(DelayedOut'high-1 downto 0)           <= DelayedOut(DelayedOut'high downto 1);
    end if;

  end process;

  OUT_Word     <= DelayedOut(0);
  OUT_Overflow <= DelayedOverflow(0);

end Behavioral;
