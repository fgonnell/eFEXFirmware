# eFEX Firmware

## Description:
This is the eFEX board firmware. It is based on 3 main libraries:
* Infrastructure
* Algorithm
* Readout

This project uses [Hog](http://cern.ch/hog).

## Instructions:
After cloning the repository (with --recursive), just run:

    ./CreateProject.sh process_fpga.1

or give no argument to get a list of all the projects in the repository.

The project will be created in the `Projects` directory.
