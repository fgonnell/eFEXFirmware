--
-- Richard Staley, 2018/07/18

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
library infrastructure_lib;
use infrastructure_lib.all;
use infrastructure_lib.synch_type.all;
--! @copydoc mgt_tx_error_detector.vhd
entity mgt_error_detector is
	generic(
		ERROR_BITS: natural := 32
		);
	port(
		clock:   in std_logic;
		rx_data_in: in mgt_data;
		inject_error: in std_logic := '0';
		clear_error: in std_logic;
		error_count: out unsigned(ERROR_BITS-1 downto 0)
	);
	
end mgt_error_detector;

architecture rtl of mgt_error_detector is

    signal rx_in :  mgt_data;
    signal next_value :  unsigned(31 downto 0) := (others => '0');
    signal rx_data :  unsigned(31 downto 0) := (others => '0');
    signal error_counter : unsigned(ERROR_BITS-1 downto 0) := (others => '0');
    
    signal comma_detected : boolean := FALSE;
    signal clear_counter : std_logic := '0';
    signal error_found : std_logic := '0';
    signal fake_error : std_logic := '0';
    
    constant DELAY : time := 0.5 ns;

begin

-- retime inputs to mgt clock
input_register:process(clock, rx_data_in, clear_error) 
	begin
		if rising_edge(clock) then
			clear_counter <= clear_error;
			fake_error <= inject_error;
			rx_in <= rx_data_in;
		end if;
	end process;

    comma_detected <= TRUE when rx_in.data(7 downto 0) = x"BC" and rx_in.ctrl = x"1" else FALSE;

    rx_data <= unsigned(rx_in.data);
					
count_value: process(clock, comma_detected, next_value) 
	begin
		if rising_edge(clock) then
			if comma_detected  then
				next_value  <= x"55555555";
			else
				next_value  <= not next_value;
			end if;
		end if;
	end process;

	error_found <= '1' when (rx_data /= next_value) and not comma_detected else '0';
	
count_error: process(clock, error_found, fake_error, clear_counter) 
	begin
		if rising_edge(clock) then
			if clear_counter = '1' then
				error_counter  <=  (others => '0');
			else if (error_found = '1' or fake_error = '1') then
					error_counter  <= error_counter + 1;
				end if;
			end if;
		end if;
	end process;

	
	--error_count <= std_logic_vector(error_counter);
	error_count <= error_counter;
	

end rtl;
