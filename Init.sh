#!/bin/bash
echo [Init] Initialising repository...
OLD_DIR=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "${DIR}"

echo [Init] Initialising ipbus submodule...
git submodule init
echo [Init] Updating ipbus submodule...
git submodule update

./Hog/Init.sh

cd "${OLD_DIR}"
