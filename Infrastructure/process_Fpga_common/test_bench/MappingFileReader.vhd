
library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;


-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library std;
use std.textio.all;             --TEXT is here


library algolib;
use algolib.DataTypes.all;
use algolib.AlgoDataTypes.all;
use algolib.Utility.all;

entity MappingFileReader is
  generic(FILENAME   : string := "../../../../../../sim/TopAlgoModule/BigTowers.txt";
          RESET_TIME : time   := 48 ns;
          DEAD_TIME  : time   := 2.5 us);
  port(
    DATA_OUT   : out std_logic_vector(31 downto 0);
    COMMA_OUT  : out std_logic;
    DATA_READY : out std_logic;
    CLK40      : out std_logic;
    CLK160     : out std_logic;
    CLK200     : out std_logic;
    CLK280     : out std_logic;
    LOAD       : out std_logic;
    RESET      : out std_logic);
end MappingFileReader;

--
architecture std of MappingFileReader is
  constant PERIOD : time := 25 ns;  -- 40 MHz;
  constant OFFSET : time := 1 ns;   -- delay between data and clock (must be less then half the period of the fastes clock 200MHz  T/2 = 2.5 ns)

  file stimulus   : text open read_mode is FILENAME;
  signal clock40  : std_logic;
  signal load_i   : std_logic;
  signal clock160 : std_logic;
  signal clock200 : std_logic;
  signal clock280 : std_logic;
  signal DataOut  : std_logic_vector(31 downto 0);
  signal CommaOut : std_logic_vector(0 downto 0);

begin

  CLK40  <= clock40;
  CLK160 <= clock160;
  CLK200 <= clock200;
  CLK280 <= clock280;
  LOAD   <= load_i;

  Clock40Generator : process
  begin
    clock40 <= '1';
    wait for 0.5*PERIOD;
    clock40 <= '0';
    wait for 0.5*PERIOD;
  end process;

  LoadGenerator : process
  begin
    load_i <= '1';
    wait for 0.1*PERIOD;
    load_i <= '0';
    wait for 0.8*PERIOD;
    load_i <= '1';
    wait for 0.1*PERIOD;
  end process;

  Clock160Generator : process
  begin
    clock160 <= '1';
    wait for 0.5*(PERIOD/4);
    clock160 <= '0';
    wait for 0.5*(PERIOD/4);
  end process;

  Clock200Generator : process
  begin
    clock200 <= '1';
    wait for 0.5*(PERIOD/5);
    clock200 <= '0';
    wait for 0.5*(PERIOD/5);
  end process;

  Clock280Generator : process
  begin
    clock280 <= '1';
    wait for 0.5*(PERIOD/7);
    clock280 <= '0';
    wait for 0.5*(PERIOD/7);
  end process;



  stim_proc : process
    variable l       : line;
    variable space   : string(1 downto 1);
    variable s_data  : string(8 downto 1);
    variable s_comma : string(1 downto 1);
  begin
    DataOut    <= (others => '0');
    DATA_READY <= '0';
    RESET      <= '1';
    wait for RESET_TIME;
    reset      <= '0';
    wait for DEAD_TIME;
    DataOut    <= (others => '0');
    DATA_READY <= '0';

    wait for 9 * PERIOD;

    wait until clock280 = '0';
    wait for (0.5*PERIOD/7 - OFFSET);

    while not endfile(stimulus) loop
      readline(stimulus, l);
      read(l, s_data);
      read(l, space);
      read(l, s_comma);
      CommaOut   <= to_std_logic_vector(s_comma);
      DataOut    <= to_std_logic_vector(str_hex_to_bin(s_data));
      DATA_READY <= '1';

      wait until clock280 = '0';
      wait for (0.5*PERIOD/7 - OFFSET);
    end loop;

    CommaOut   <= "0";
    DataOut    <= (others => '0');
    DATA_READY <= '0';
    wait for 1000 us;

  end process;

  COMMA_OUT <= CommaOut(0);
  DATA_OUT  <= DataOut;
end std;
