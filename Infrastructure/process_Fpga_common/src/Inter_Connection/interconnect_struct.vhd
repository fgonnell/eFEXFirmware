
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;


ENTITY interconnect IS
   PORT( 
      mac_clk         : IN     std_logic;
      master_rx_data  : IN     std_logic_vector (9 DOWNTO 0);
      rst_macclk      : IN     std_logic;
      tx_data         : IN     std_logic_vector (8 DOWNTO 0);
      master_rx_err   : OUT    std_logic;
      process_tx_data : OUT    std_logic_vector (9 DOWNTO 0);
      rx_data         : OUT    std_logic_vector (8 DOWNTO 0)
   );



END interconnect ;


ARCHITECTURE struct OF interconnect IS
signal master_rx_data_int :std_logic_vector ( 9 downto 0);
signal process_tx_data_int:std_logic_vector ( 9 downto 0);
  
   

BEGIN
 process (mac_clk)
   begin 
--        if rst_macclk ='1' then
--           master_rx_data_int <= (others => '0');
--           process_tx_data    <= (others => '0');
         if mac_clk' event and mac_clk ='1' then
               master_rx_data_int <= master_rx_data;
               process_tx_data <= process_tx_data_int;
         end if;
         end process;      

   -- Instance port mappings.
   U_1 : entity work.parity_checker
      PORT MAP (
         Clk         => mac_clk,
         Reset       => rst_macclk,
         Data_in     => master_rx_data_int,
         Data_out    => rx_data,
         even_parity => master_rx_err
      );
   U_0 : entity work.parity_gen
      GENERIC MAP (
         width => 9
      )
      PORT MAP (
         Reset       => rst_macclk,
         CLK         => mac_clk,
         Data_in     => tx_data,
         data_Parity => process_tx_data_int
      );

END struct;
