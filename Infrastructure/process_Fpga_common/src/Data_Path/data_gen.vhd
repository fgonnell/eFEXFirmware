----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/22/2017 06:28:40 PM
-- Design Name: 
-- Module Name: data_gen - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

--entity data_gen is
--    Port ( clk : in STD_LOGIC;
--           txdata_in : out STD_LOGIC_VECTOR (31 downto 0);
--           txcharisk : out STD_LOGIC_VECTOR (3 downto 0);
--           reset : in STD_LOGIC);
--end data_gen;

--architecture Behavioral of data_gen is
----signal cntr : unsigned(31 downto 0);

--begin

--data_gen: process(clk,RESET)
--  begin
--      if RESET = '1' then
--         cntr <= (others =>'0');
--         txcharisk <= (others =>'0');
--      elsif clk' event and clk = '1' then
--            cntr <= cntr +1;
--            txdata_in   <= std_logic_vector(cntr);
--            txcharisk   <= "0000";
--          if cntr = 511 then
--             txdata_in <= x"000000BC";
--             cntr <=  (others =>'0');
--             txcharisk <= "0001";
--          end if;
--      end if;    
-- end process;  




 entity data_gen is
    Port ( clk          : in std_logic;
           txdata_in    : out std_logic_vector(31 downto 0);
           txcharisk    : out std_logic_vector(3 downto 0);
           ttc          : in std_logic;
           enable       : in std_logic;
           rx_resetdone : in std_logic;
           reset        : in std_logic
           
           );
end data_gen;




architecture Behavioral of data_gen is
signal ttc_int,reg0,reg1,reg2,ttc_20m: std_logic;
signal cntr : unsigned(31 downto 0);

TYPE STATE_TYPE IS (
      idle,
      ttc_0,
      ttc_1,
      wait_1,
      gen_bc,
      gen_data,
      rst_done,
      en

   );
 
   
 -- Declare current and next state signals
     SIGNAL current_state : STATE_TYPE;
     SIGNAL next_state : STATE_TYPE;
  
     -- Declare any pre-registered internal signals
     SIGNAL txcharisk_cld : std_logic_vector (3 DOWNTO 0);
     SIGNAL txdata_in_cld : std_logic_vector (31 DOWNTO 0);
  
  BEGIN
  

-- Capture the incoming TTC 40M clk by dtype flipflop 
dtype: entity work.d_type
          port map (
                clk   => ttc,
                q     => ttc_20M              
                
               ); 
  
  
ttc_pip: process(clk)
  begin
     
     if clk' event and clk ='1' then
        reg0 <= ttc_20M;
        reg1 <= reg0; 
        reg2 <= reg1;
        ttc_int <= reg2 xor reg1  ;
      end if;
   end process;       
  
 
    



   
      -----------------------------------------------------------------
      clocked_proc : PROCESS ( 
         clk
       
      )
      -----------------------------------------------------------------
      BEGIN
          IF (clk'EVENT AND clk = '1') THEN 
            IF (reset = '1') THEN
               current_state <= idle;
               -- Default Reset Values
               txcharisk_cld <= (others => '0');
               txdata_in_cld <= (others => '0');
               cntr <= (others => '0');
            ELSE
               current_state <= next_state;
               txcharisk_cld <= (others => '0');
               txdata_in_cld <= (others => '0');
               cntr <= (others => '0');
      
             -- Combined Actions
                      CASE current_state IS
                         WHEN idle => 
                            cntr <= (others=> '0') ;
                         WHEN gen_bc => 
                            txdata_in_cld <= x"000000bc";
                            txcharisk_cld <= "0001";
                         WHEN gen_data => 
                            txdata_in_cld <= std_logic_vector(cntr);
                            txcharisk_cld <= "0000";
                            cntr  <= cntr+1;
                            IF (cntr =509) THEN 
                               cntr <= (others =>'0');
                            END IF;
                         WHEN OTHERS =>
                            NULL;
                      END CASE;
                   END IF;
                   END IF;
                END PROCESS clocked_proc;
              
                -----------------------------------------------------------------
                nextstate_proc : PROCESS ( 
                   cntr,
                   current_state,
                   enable,
                   rx_resetdone,
                   ttc_int
                )
                -----------------------------------------------------------------
                BEGIN
                   CASE current_state IS
                      WHEN idle => 
                         IF (enable ='1') THEN 
                            next_state <= rst_done;
                         ELSE
                            next_state <= idle;
                         END IF;
                      WHEN ttc_0 => 
                         IF (ttc_int='0') THEN 
                            next_state <= wait_1;
                         ELSE
                            next_state <= ttc_0;
                         END IF;
                      WHEN ttc_1 => 
                         IF (ttc_int ='1') THEN 
                            next_state <= gen_bc;
                         ELSE
                            next_state <= ttc_1;
                         END IF;
                      WHEN gen_bc => 
                         next_state <= gen_data;
                      WHEN gen_data => 
                         IF (cntr =509) THEN 
                            next_state <= gen_bc;
                         ELSIF (enable ='0') THEN 
                            next_state <= idle;
                         ELSE
                            next_state <= gen_data;
                         END IF;
                      WHEN wait_1 => 
                         IF (ttc_int ='1') THEN 
                            next_state <= gen_bc;
                         ELSE
                            next_state <= wait_1;
                         END IF;
                      WHEN en => 
                         IF (ttc_int='1') THEN 
                            next_state <= ttc_0;
                         ELSIF (ttc_int='0') THEN 
                            next_state <= ttc_1;
                         ELSE
                            next_state <= en;
                         END IF;
                      WHEN rst_done => 
                         IF (rx_resetdone ='1') THEN 
                            next_state <= en;
                         ELSE
                            next_state <= rst_done;
                         END IF;
                      WHEN OTHERS =>
                         next_state <= idle;
                   END CASE;
                END PROCESS nextstate_proc;
              
                -- Concurrent Statements
                -- Clocked output assignments
                txcharisk <= txcharisk_cld;
                txdata_in <= txdata_in_cld;
end Behavioral;
