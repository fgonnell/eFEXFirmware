--! @file
--! @brief Daly for data word format
--! @details
--! SR that delays the input data word by as many clock cycle as specified in
--the delay generic
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
use work.DataTypes.all;

--! @copydoc Delay.vhd
entity Delay is
  generic (delay : integer := 1
           );

  port (
    CLK      : in  std_logic;
    IN_Word  : in  DataWord;
    OUT_Word : out DataWord
    );
end Delay;

--! @copydoc AlgoCore_eg.vhd
architecture Behavioral of Delay is

  signal DelayedWord : DataWords(delay downto 0) := (others => (others => '0'));

begin

  dalay_proc : process (CLK)
  begin
    if rising_edge(CLK) then
      DelayedWord(DelayedWord'high)            <= IN_Word;
      DelayedWord(DelayedWord'high-1 downto 0) <= DelayedWord(DelayedWord'high downto 1);
    end if;
  end process;

  OUT_Word <= DelayedWord(0);
end Behavioral;
