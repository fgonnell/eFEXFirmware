--! @file
--! @brief FSM to write TOB/XTOB data to stage 1 FIFO for process FPGA
--! @details 
--! This module creates a complete TOB/XTOB interim FIFO stage before writing into Link Output FIFO
--! The Link Output FIFO is then controlled by FIFO_to_MGT_FSM state machine to transfer data to MGT
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;
use TOB_rdout_lib.data_type_pkg.all;

--! @copydoc fsm_TOBs_to_3_FIFO.vhd
entity fsm_TOBs_to_3_FIFO is
    Port ( 
       clk_280M_in                : in STD_LOGIC;
       --! TOB Readout FIFO reset Pulse by software command ORed with SYS_RST
       TOB_FIFO_sw_rst            : in std_logic ;       
       --! FPGA Hardware Address
       hw_addr                    : in STD_LOGIC_VECTOR(1 downto 0) ;   
       --! TOB & XTOB input FIFO empty flag
       TOB_empty_flag_in          : in std_logic ;   
       --! number of DRPAM locations to read for multi-slice (1 to 5)
       DPR_locations_to_rd        : in STD_LOGIC_VECTOR (2 downto 0);       
       --!  Readout Expected: RAW data readout    
       rdout_expected             : in STD_LOGIC;
        
       --! read enable signal to read BCN & L1A_ID from Fist Stage FIFO
       FIFO_1_BCN_rd_en_out         : out STD_LOGIC;
       --! BCN, Ext L1A & L1A ID from previous FIFO -- BCN + EXT_L1A_ID + L1A_ID  44b = 12b + 8b + 24b
       FIFO_1_BCN_in              : in STD_LOGIC_VECTOR (43 downto 0);        
       --! BCN, Ext L1A & L1A ID from previous FIFO valid signal
       FIFO_1_BCN_valid_in        : in STD_LOGIC; 

       --! read enable signal to BCN & L1A_ID FIFO_1
       FIFO_1_BCN_rd_en_in         : in STD_LOGIC;
       --! BCN, Ext L1A & L1A ID to Link Output FIFO -- BCN + EXT_L1A_ID + L1A_ID  44b = 12b + 8b + 24b
       FIFO_1_BCN_out              : out STD_LOGIC_VECTOR (43 downto 0);        
       --! BCN, Ext L1A & L1A ID to Link Output FIFO valid signal
       FIFO_1_BCN_valid_out        : out STD_LOGIC; 

-- ###################################################################################
       --! read enable signal to all TOBs and XTOB FIFOs
       TOB_rdout_fifo_rd_en_out   : out STD_LOGIC;
       
       --! sorted TOBs valid signal, also used for XTOBs
       valid_T_TOB_in             : in std_logic ;
       --! sorted TOBs 242b = 7b+4b+7b+224b  TOB_BCN+ERR+valid+T_TOB
       rdout_T_TOB_209b_in        : in STD_LOGIC_VECTOR(208 downto 0);  
       --! XTOBs e/g {array 8 of [(5*64b) + 5b valid]}
       XTOBs_eg_in                : in array_8_of_332b;  -- XTOBs {array 8 of [(5*64b) + 5b valid] + XTOB_BCN}
       --! XTOBs tau {array 8 of [(5*64b) + 5b valid]}
       XTOBs_tau_in               : in array_8_of_332b;  -- XTOBs {array 8 of [(5*64b) + 5b valid] + XTOB_BCN} 
-- ###################################################################################
      --! read signal for FIFO_1 TOBs to Link_outpout_FIFO
       FIFO_1_TOBs_rd_en_in     : in STD_LOGIC;
      --! sorted TOBs data valid signal to Link_outpout_FIFO
       FIFO_1_TOBs_valid_out    : out STD_LOGIC;
       --! sorted TOBs data is CHAR signal to Link_outpout_FIFO
       FIFO_1_TOBs_out          : out STD_LOGIC_VECTOR (32 downto 0);

      --! read signal for FIFO_1 XTOBs e/gto Link_outpout_FIFO
       FIFO_1_XTOBs_eg_rd_en_in     : in STD_LOGIC;
       --! XTOBs e/g data valid signal to Link_outpout_FIFO
       FIFO_1_XTOBs_eg_TOB_valid_out      : out STD_LOGIC;
       --! XTOBs e/g data is CHAR signal to Link_outpout_FIFO
       FIFO_1_XTOBs_eg_out               : out STD_LOGIC_VECTOR (32 downto 0);
       
      --! read signal for FIFO_1 XTOBs tau to Link_outpout_FIFO
       FIFO_1_XTOBs_tau_rd_en_in     : in STD_LOGIC;
       --! XTOBs tau data valid signal to Link_outpout_FIFO
       FIFO_1_XTOBs_tau_TOB_valid_out     : out STD_LOGIC;
       --! XTOBs tau data is CHAR signal to Link_outpout_FIFO
       FIFO_1_XTOBs_tau_out              : out STD_LOGIC_VECTOR (32 downto 0);
       
       --! from Link_outpout_FIFO full fifo threshold assert
       FIFO_1_TOBs_full_thresh_assert   : in  STD_LOGIC_VECTOR (8 downto 0);
       --! from Link_outpout_FIFO full fifo threshold negate
       FIFO_1_TOBs_full_thresh_negate   : in  STD_LOGIC_VECTOR (8 downto 0);
       --! from Link_outpout_FIFO full fifo threshold assert
       FIFO_1_full_thresh_assert   : in  STD_LOGIC_VECTOR (12 downto 0);
       --! from Link_outpout_FIFO full fifo threshold negate
       FIFO_1_full_thresh_negate   : in  STD_LOGIC_VECTOR (12 downto 0)
   );

   
end fsm_TOBs_to_3_FIFO;

--! @copydoc fsm_TOBs_to_3_FIFO.vhd
architecture Behavioral of fsm_TOBs_to_3_FIFO is
    
    signal  TOB_tmp_valid_i              : std_logic;
    signal  TOB_rdout_fifo_rd_en_i       : std_logic ;

    signal  BCN_FIFO_rd_en_i : std_logic  ;
    
    signal  slice_count_i                : unsigned (2 downto 0);
    signal  link_err_flg_in_i            : STD_LOGIC_VECTOR(3  downto 0);
    signal  TOB_BCN_in_i                 : STD_LOGIC_VECTOR(7 downto 0);
    signal  XTOB_eg_BCN_in_i             : std_logic_vector (7 downto 0);
    signal  XTOB_tau_BCN_in_i            : std_logic_vector (7 downto 0);

    signal  TOBs_tmp_i                   : STD_LOGIC_VECTOR(31 downto 0);

    signal  TOB_tmp_is_char_i      : std_logic ;

    signal  CLK_280M_i             : std_logic ;
--    signal  RST_i                  : std_logic ;

--    signal  cntr_rst_i             : std_logic  ;
    signal  TOB_cntr_rst           : std_logic  ;
    signal  T_TOB_cntr_i           : std_logic_vector(3 downto 0);
    signal  T_TOB_cntr_1dly        : std_logic_vector(3 downto 0);

    signal  XTOB_eg_cntr_en_i      : std_logic  ;
    signal  XTOB_eg_cntr_i         : std_logic_vector(6 downto 0);
    signal  XTOB_eg_cntr_1dly      : std_logic_vector(6 downto 0);
    signal  XTOB_tau_cntr_en_i     : std_logic ;
    signal  XTOB_tau_cntr_i        : std_logic_vector(6 downto 0);
    signal  XTOB_tau_cntr_1dly     : std_logic_vector(6 downto 0);
    
    signal  trailer_cntr_en        : std_logic ;
    signal  payld_cntr_rst         : std_logic ;
--    signal  payld_cntr_rst_i       : std_logic ;
    signal  payld_cntr_en          : std_logic ;
    signal  TOB_payld_cntr_i       : std_logic_vector(11 downto 0);
    signal  TOB_payld_cntr_1dly    : std_logic_vector(11 downto 0);
    
    signal  slice_no_txed          : std_logic_vector(2 downto 0);  -- slice number transmitted
    
    signal  FIFO_1_BCN_out_1, FIFO_1_BCN_out_2  : std_logic_vector(35 downto 0);
    signal  XTOBs_eg_tmp_i, XTOBs_tau_tmp_i  : std_logic_vector(32 downto 0);
    signal  XTOBs_eg_tmp_valid_i, XTOBs_tau_tmp_valid_i        : std_logic ;
 
    signal  T_TOB_eg_i           : std_logic_vector(191 downto 0);
    signal  T_TOB_eg_valid_i     : std_logic_vector(5 downto 0);
    signal  valid_T_TOB_in_i     : std_logic ;
    
    signal  XTOB_eg_i            : array_8_of_320b;  -- XTOBs e/g {array 8 of [(5*64b) + 5b valid]};
    signal  XTOB_eg_valid_i      : array_8_of_5b ; 	 -- std_logic_vector(39 downto 0);
    signal  XTOB_tau_i           : array_8_of_320b;  -- XTOBs tau {array 8 of [(5*64b) + 5b valid]};
    signal  XTOB_tau_valid_i     : array_8_of_5b;	-- std_logic_vector(39 downto 0);
    
    TYPE STATE_TYPE IS (
       idle, wait1, wait2, wait3, wait4,
       err_sop, err_eop,
       norm_tlr, norm_eop,
       rd_fifo, sub_trl_1, sub_trl_2,
       rdout_TOBs_1, rdout_TOBs_2,
       rdout_XTOB_eg_1, rdout_XTOB_eg_2, rdout_XTOB_eg_3,
       rdout_XTOB_tau_1, rdout_XTOB_tau_2, rdout_XTOB_tau_3
       );
  
    SIGNAL current_state : STATE_TYPE;
    signal i, j         : integer range 0 to 15;       -- used to index TOB valid flag 

--    ####### attributes  ########
    attribute keep       : string ;
    attribute max_fanout : integer;
	
    attribute keep of       j : signal is "true" ;
    attribute max_fanout of j : signal is 60;
    attribute keep of       i : signal is "true" ;
    attribute max_fanout of i : signal is 60;
    attribute keep of       TOBs_tmp_i : signal is "true" ;
    attribute max_fanout of TOBs_tmp_i : signal is 60;
--    #######################################
    
begin

    -- input ports
    CLK_280M_i           <= CLK_280M_in ;
--    RST_i                <= RST ;
    slice_count_i        <= unsigned (DPR_locations_to_rd) ; -- multi-slice read register 1 to 5
    
    -- output ports
    TOB_rdout_fifo_rd_en_out  <= TOB_rdout_fifo_rd_en_i  ;
    FIFO_1_BCN_rd_en_out      <= BCN_FIFO_rd_en_i; -- read BCN FIFO into BCN FIFO_1
    

-- clk data input to remove timing error
U1_clk_280_proc : Process (CLK_280M_i)
    begin
        if rising_edge (CLK_280M_i) then
            T_TOB_eg_i           <= rdout_T_TOB_209b_in(191 downto 0) ;     -- 6*32=192b TOB data
            T_TOB_eg_valid_i     <= rdout_T_TOB_209b_in(197 downto 192) ;   -- 6b  Valid flags
            link_err_flg_in_i    <= rdout_T_TOB_209b_in(201 downto 198) ;   -- 4 b Error
            TOB_BCN_in_i         <= '0' & rdout_T_TOB_209b_in(208 downto 202) ;   -- 7b TOB_BCN
            valid_T_TOB_in_i     <= valid_T_TOB_in ;    -- sorted TOB valid from previous FIFO
            
            XTOB_eg_i(0)           <= XTOBs_eg_in(0)(319 downto 0);
            XTOB_eg_i(1)           <= XTOBs_eg_in(1)(319 downto 0);
            XTOB_eg_i(2)           <= XTOBs_eg_in(2)(319 downto 0);
            XTOB_eg_i(3)           <= XTOBs_eg_in(3)(319 downto 0);
            XTOB_eg_i(4)           <= XTOBs_eg_in(4)(319 downto 0);
            XTOB_eg_i(5)           <= XTOBs_eg_in(5)(319 downto 0);
            XTOB_eg_i(6)           <= XTOBs_eg_in(6)(319 downto 0);
            XTOB_eg_i(7)           <= XTOBs_eg_in(7)(319 downto 0);
                                    
            XTOB_eg_valid_i(0)     <= XTOBs_eg_in(0)(324 downto 320);
            XTOB_eg_valid_i(1)     <= XTOBs_eg_in(1)(324 downto 320);
            XTOB_eg_valid_i(2)     <= XTOBs_eg_in(2)(324 downto 320);
            XTOB_eg_valid_i(3)     <= XTOBs_eg_in(3)(324 downto 320);
            XTOB_eg_valid_i(4)     <= XTOBs_eg_in(4)(324 downto 320);
            XTOB_eg_valid_i(5)     <= XTOBs_eg_in(5)(324 downto 320);
            XTOB_eg_valid_i(6)     <= XTOBs_eg_in(6)(324 downto 320);
            XTOB_eg_valid_i(7)     <= XTOBs_eg_in(7)(324 downto 320);
            
            XTOB_eg_BCN_in_i       <= '0' & XTOBs_eg_in(0)(331 downto 325);
         
            XTOB_tau_i(0)           <= XTOBs_tau_in(0)(319 downto 0);
            XTOB_tau_i(1)           <= XTOBs_tau_in(1)(319 downto 0);
            XTOB_tau_i(2)           <= XTOBs_tau_in(2)(319 downto 0);
            XTOB_tau_i(3)           <= XTOBs_tau_in(3)(319 downto 0);
            XTOB_tau_i(4)           <= XTOBs_tau_in(4)(319 downto 0);
            XTOB_tau_i(5)           <= XTOBs_tau_in(5)(319 downto 0);
            XTOB_tau_i(6)           <= XTOBs_tau_in(6)(319 downto 0);
            XTOB_tau_i(7)           <= XTOBs_tau_in(7)(319 downto 0);
                                    
            XTOB_tau_valid_i(0)     <= XTOBs_tau_in(0)(324 downto 320);
            XTOB_tau_valid_i(1)     <= XTOBs_tau_in(1)(324 downto 320);
            XTOB_tau_valid_i(2)     <= XTOBs_tau_in(2)(324 downto 320);
            XTOB_tau_valid_i(3)     <= XTOBs_tau_in(3)(324 downto 320);
            XTOB_tau_valid_i(4)     <= XTOBs_tau_in(4)(324 downto 320);
            XTOB_tau_valid_i(5)     <= XTOBs_tau_in(5)(324 downto 320);
            XTOB_tau_valid_i(6)     <= XTOBs_tau_in(6)(324 downto 320);
            XTOB_tau_valid_i(7)     <= XTOBs_tau_in(7)(324 downto 320);
            
            XTOB_tau_BCN_in_i       <= '0' & XTOBs_tau_in(0)(331 downto 325);
            
        end if;
    end process;
                             
--    payld_cntr_rst <= RST_i OR payld_cntr_rst_i;
    

U4_rd_fsm : process (CLK_280M_i)
    begin
        if CLK_280M_i'event and CLK_280M_i = '1' then
            if ( TOB_FIFO_sw_rst = '1' )then        -- signal is RST OR TOB_FIFO_sw_rst
                current_state           <= idle ;
            else    
                CASE current_state is
                    when idle =>
                        BCN_FIFO_rd_en_i          <= '0' ; 
                        TOB_rdout_fifo_rd_en_i    <= '0' ;
                        TOB_tmp_valid_i           <= '0' ;
                        XTOBs_eg_tmp_valid_i      <= '0' ;
                        XTOBs_tau_tmp_valid_i     <= '0' ;
                        i <= 0 ;
						j <= 0 ;
                        slice_no_txed             <= "000";  -- initially set to 1
                        current_state             <= idle ;
                        -- if fifo is not empty, read fifos
                        if (TOB_empty_flag_in = '0') then 
                            current_state           <= rd_fifo ;
                            TOB_rdout_fifo_rd_en_i  <= '1';           -- read data from fifos
                            BCN_FIFO_rd_en_i        <= '1' ;
                        else
                            -- if no data available wait
                            current_state <= idle ;
                       end if;
                       
                    when rd_fifo =>       -- wait 1 clk for fifo output
                        TOB_rdout_fifo_rd_en_i    <= '0' ;
                        BCN_FIFO_rd_en_i          <= '0' ;
                        TOB_tmp_valid_i           <= '0' ;      -- do not save in FIFO
                        if valid_T_TOB_in_i = '1' then         -- if Topo tobs are valid, write to shift register.
                          if hw_addr(1) = '1' then   -- if F3 or F4 then miss T_TOB readout
                              current_state         <= rdout_XTOB_eg_1 ;  
                            else                     -- else do T_TOB readout
                              current_state         <= rdout_TOBs_1 ;
                          end if;
                        else
                          current_state   <= rd_fifo ;  -- stay until FIFO read
                        end if;
                           
                    when rdout_TOBs_1 =>  -- 8b TOB_BCN + 4 b Error
                        TOBs_tmp_i(31 downto 0) <= X"00000" & TOB_BCN_in_i & link_err_flg_in_i ;
                        TOBs_tmp_i(32)          <= '1' ;
                        TOB_tmp_valid_i   <= '1';    -- on valid save in FIFO
                        current_state     <= rdout_TOBs_2 ;

                    when rdout_TOBs_2 =>         -- normal T_TOB_eg rd out 7 clks
                        TOBs_tmp_i(31 downto 0)  <= T_TOB_eg_i((((i*32)+32)-1) downto (i*32)); -- save TOBs
                        TOBs_tmp_i(32)       <= T_TOB_eg_valid_i(i) ;    -- write in FIFO bit 33
                        TOB_tmp_valid_i   <= '1';    -- on valid save in FIFO
                         if (i = 5) then        -- 6 T TOBs are read out, 7th is ignored
                           i <= 0 ;
                           current_state   <= rdout_XTOB_eg_1 ;
                           TOB_tmp_valid_i   <= '0';   -- disable TOBs write into FIFO 1
                        else
                           current_state   <= rdout_TOBs_1 ;
                           i <= i + 1 ;
                        end if;

                    when rdout_XTOB_eg_1 =>         -- normal X_TOB_eg BCN value
--                        TOB_tmp_valid_i   <= '0';   -- disable TOBs write into FIFO 1
                        XTOBs_eg_tmp_valid_i     <= '1' ;    -- write in FIFO bit 33
                        XTOBs_eg_tmp_i(31 downto 0) <= X"000000" & XTOB_eg_BCN_in_i  ;
                        XTOBs_eg_tmp_i(32) <= '1'  ;
                        current_state           <= rdout_XTOB_eg_2 ;

                    when rdout_XTOB_eg_2 =>         -- normal X_TOB_eg rd out 80 clks
                        XTOBs_eg_tmp_valid_i     <= '1' ;    -- write in FIFO 
                        XTOBs_eg_tmp_i(31 downto 0)  <= XTOB_eg_i(j)((((i*64)+32)-1) downto (i*64));
                        XTOBs_eg_tmp_i(32)           <= XTOB_eg_valid_i(j)(i) ;    -- write in FIFO bit 33
                        current_state           <= rdout_XTOB_eg_3 ;

                    when rdout_XTOB_eg_3 =>         -- normal X_TOB_eg rd out 80 clks
                        XTOBs_eg_tmp_valid_i     <= '1' ;    -- write in FIFO bit 33
                        XTOBs_eg_tmp_i(31 downto 0)  <= XTOB_eg_i(j)((((i*64)+32)-1) downto (i*64));
                        XTOBs_eg_tmp_i(32)           <= XTOB_eg_valid_i(j)(i) ;    -- write in FIFO bit 33
                        if (i = 4) then    -- 10*32b XTOB from (5 * 64b XTOB)
                           i <= 0 ;
						   if (j = 7) then
						      j <= 0 ;
						      current_state        <= rdout_XTOB_tau_1 ;
						      XTOBs_eg_tmp_valid_i <= '0' ;    -- disable XTOBs e/g write into FIFO
							else
							   j <= j + 1 ; 
							   current_state        <= rdout_XTOB_eg_2 ;
							end if;
                        else
                           current_state        <= rdout_XTOB_eg_2 ;
                           i <= i + 1 ;
                        end if;
                        
                    when rdout_XTOB_tau_1 =>         -- normal X_TOB_eg rd out 80 clks
--                        XTOBs_eg_tmp_valid_i     <= '0' ;    -- disable XTOBs e/g write into FIFO
                        XTOBs_tau_tmp_valid_i    <= '1' ;    -- write in FIFO bit 33
						XTOBs_tau_tmp_i(31 downto 0)          <= X"000000" & XTOB_tau_BCN_in_i  ;
						XTOBs_tau_tmp_i(32)           <= XTOB_tau_valid_i(j)(i) ;    -- write in FIFO bit 33
						current_state        <= rdout_XTOB_tau_2 ;
    
                    when rdout_XTOB_tau_2 =>         -- normal X_TOB_eg rd out 80 clks
                        XTOBs_tau_tmp_valid_i    <= '1' ;    -- write in FIFO bit 33
						XTOBs_tau_tmp_i(31 downto 0)          <= XTOB_tau_i(j)((((i*64)+32)-1) downto (i*64));
						XTOBs_tau_tmp_i(32)           <= XTOB_tau_valid_i(j)(i) ;    -- write in FIFO bit 33
						current_state        <= rdout_XTOB_tau_3 ;
						
					when rdout_XTOB_tau_3 =>         -- normal X_TOB_eg rd out 80 clks
                        XTOBs_tau_tmp_valid_i    <= '1' ;    -- write in FIFO bit 33
						XTOBs_tau_tmp_i(31 downto 0)          <= XTOB_tau_i(j)((((i*64)+32)-1) downto (i*64));
						XTOBs_tau_tmp_i(32)           <= XTOB_tau_valid_i(j)(i) ;    -- write in FIFO bit 33
                        if (i = 4) then    -- 10*32b XTOB from (5 * 64b XTOB)
                           i <= 0 ;
						   if (j = 7) then
						      j <= 0 ;
						      current_state        <= idle ;
						      XTOBs_tau_tmp_valid_i     <= '0' ;    -- disable XTOBs tau write into FIFO
							else
							   j <= j + 1 ; 
							   current_state        <= rdout_XTOB_tau_2 ;
							end if;
						else
						   current_state        <= rdout_XTOB_tau_2 ;
						   i <= i + 1 ;
						end if;
                        
                    when others => NULL;
                end case;
            end if;
        END IF;           
    end process;

    FIFO_1_BCN_in_tmp1 <= X"0000000" & FIFO_1_BCN_in(43 downto 36);
    
U5a_FIFO_1_BCN_L1A : FIFO_36b_512   -- BCN + EXT_L1A_ID + L1A_ID  44b = 12b + 8b + 24b
    PORT MAP (
      srst          => TOB_FIFO_sw_rst,   -- sys RST OR RAW_FIFO_sw_rst
      clk           => CLK_280M_i,
      din           => FIFO_1_BCN_in(35 downto 0) , -- 36b of 44b (BCN + EXT_L1A_ID + L1A_ID)
      wr_en         => FIFO_1_BCN_valid_in,
      rd_en         => FIFO_1_BCN_rd_en_in,
      dout          => FIFO_1_BCN_out_1,     -- 36b of 44b (BCN + EXT_L1A_ID + L1A_ID) 
      valid         => FIFO_1_BCN_valid_out ,
      prog_full_thresh_assert => FIFO_1_TOBs_full_thresh_assert,
      prog_full_thresh_negate => FIFO_1_TOBs_full_thresh_negate,
      prog_full     => open,
      full          => open,
      empty         => open
    );

U5b_FIFO_1_BCN_L1A : FIFO_36b_512   -- BCN + EXT_L1A_ID + L1A_ID  44b = 12b + 8b + 24b
    PORT MAP (
      srst          => TOB_FIFO_sw_rst,   -- sys RST OR RAW_FIFO_sw_rst
      clk           => CLK_280M_i,
      din           => FIFO_1_BCN_in_tmp1 , -- 8b of 44b (BCN + EXT_L1A_ID + L1A_ID)
      wr_en         => FIFO_1_BCN_valid_in,
      rd_en         => FIFO_1_BCN_rd_en_in,
      dout          => FIFO_1_BCN_out_2,     -- 8b of 44b (BCN + EXT_L1A_ID + L1A_ID) 
      valid         => FIFO_1_BCN_valid_out ,
      prog_full_thresh_assert => FIFO_1_TOBs_full_thresh_assert,
      prog_full_thresh_negate => FIFO_1_TOBs_full_thresh_negate,
      prog_full     => open,
      full          => open,
      empty         => open
    );

    FIFO_1_BCN_out <= FIFO_1_BCN_out_2(7 downto 0) & FIFO_1_BCN_out_1;    -- 8b + 36b = 44b

U6_FIFO_1_TOBs : FIFO_33b_8192         -- the flags from this FIFO is used
    PORT MAP (
        srst        => TOB_FIFO_sw_rst,   -- signal is RST OR TOB_FIFO_sw_rst
        clk         => CLK_280M_i,
        din         => TOBs_tmp_i,
        wr_en       => TOB_tmp_valid_i ,
        rd_en       => FIFO_1_TOBs_rd_en_in,        
        dout        => FIFO_1_TOBs_out,
        valid       => FIFO_1_TOBs_valid_out,
        full        => open,  -- FIFO_1_TOBs_full,
        empty       => open,
        prog_full   => open,
        prog_full_thresh_assert     => FIFO_1_full_thresh_assert ,
        prog_full_thresh_negate     => FIFO_1_full_thresh_negate ,
--        rd_data_count => Link_output_FIFO_rd_data_count ,    -- occupancy of TOB output link MGT FIFO
        data_count => open      -- occupancy of TOB output link MGT FIFO
    );

U7_FIFO_1_XTOBs_eg : FIFO_33b_8192         -- the flags from this FIFO is used
    PORT MAP (
        srst        => TOB_FIFO_sw_rst,   -- signal is RST OR TOB_FIFO_sw_rst
        clk         => CLK_280M_i,
        din         => XTOBs_eg_tmp_i,
        wr_en       => XTOBs_eg_tmp_valid_i,
        rd_en       => FIFO_1_XTOBs_eg_rd_en_in,        
        dout        => FIFO_1_XTOBs_eg_out,
        valid       => FIFO_1_XTOBs_eg_TOB_valid_out,
        full        => open,  -- FIFO_1_XTOBs_eg_full,
        empty       => open,
        prog_full   => open,
        prog_full_thresh_assert     => FIFO_1_full_thresh_assert ,
        prog_full_thresh_negate     => FIFO_1_full_thresh_negate ,
--        rd_data_count => Link_output_FIFO_rd_data_count ,    -- occupancy of TOB output link MGT FIFO
        data_count => open      -- occupancy of TOB output link MGT FIFO
    );

U8_FIFO_1_XTOBs_tau : FIFO_33b_8192         -- the flags from this FIFO is used
    PORT MAP (
        srst        => TOB_FIFO_sw_rst,   -- signal is RST OR TOB_FIFO_sw_rst
        clk         => CLK_280M_i,
        din         => XTOBs_tau_tmp_i,
        wr_en       => XTOBs_tau_tmp_valid_i,
        rd_en       => FIFO_1_XTOBs_tau_rd_en_in,        
        dout        => FIFO_1_XTOBs_tau_out,
        valid       => FIFO_1_XTOBs_tau_TOB_valid_out,
        full        => open,
        empty       => open,
        prog_full   => open,
        prog_full_thresh_assert     => FIFO_1_full_thresh_assert ,
        prog_full_thresh_negate     => FIFO_1_full_thresh_negate ,
--        rd_data_count => Link_output_FIFO_rd_data_count ,    -- occupancy of TOB output link MGT FIFO
        data_count => open      -- occupancy of TOB output link MGT FIFO
    );


end Behavioral;
