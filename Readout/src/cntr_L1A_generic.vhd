--! @file
--! @brief 24 bit Counter for L1A process FPGA
--! @details 
--! This module is a 24 bit Counterfor L1A ID
--! The counter counts when it receives either a TTC L1A or a Software L1A.  
--! This counter is reset by either TTC ECR or Software L1A Reset.  
--! Software L1A Reset is selected when bit 1 of resister rdout_pulse_reg is set to 1.  
--! The rdout_pulse_reg register is pulsed, and after 1 clock cycle the contents are reset to all Zeros.  
--! @author Saeed Taghavi

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.all;

--! @copydoc cntr_L1A_generic.vhd
entity cntr_L1A_generic is
    Port ( 
       CE   : in STD_LOGIC;
       CLK  : in STD_LOGIC;
       RST  : in STD_LOGIC;
       Q    : out STD_LOGIC_VECTOR (23 downto 0)
       );
end cntr_L1A_generic;

--! @copydoc cntr_L1A_generic.vhd
architecture Behavioral of cntr_L1A_generic is

	signal temp : unsigned (23 downto 0):= X"000000"  ;
--	signal terminal_cnt : unsigned (23 downto 0):= (others => '1') ;

begin
process (CLK)
	-- variable temp : unsigned (23 downto 0):= (others => '0') ;
	-- variable terminal_cnt : unsigned (23 downto 0):= (others => '1') ;
	begin
	if CLK'event AND CLK = '1' then
     	if RST = '1' then           -- this is a synchronous reset 
      		temp <= X"000000" ;              -- set counter to 1.
			-- temp := to_unsigned(1, testSignal'24);
      		-- terminal_cnt <= (others => '1');   -- set terminal count value given.
		else
		  if CE = '1' then
             temp <= temp + 1;			          -- else increment the value by one 
	      end if;
		end if;
	end if;
	end process;
	Q <= std_logic_vector(temp) ;	 -- push out new value

end Behavioral;
