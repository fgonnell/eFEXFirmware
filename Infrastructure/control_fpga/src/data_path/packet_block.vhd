LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library unisim;
use unisim.VComponents.all;

LIBRARY ipbus_lib;
USE ipbus_lib.ipbus.all;
LIBRARY infrastructure_lib;
use infrastructure_lib.packet_mux_type.all;

ENTITY packet_block IS
	GENERIC(
		NTTC_clients: positive := 1;
		NProcessorFPGA: positive := 1;	-- first TOB then Input Data for each FPGA in turn
		NAurora_links: positive := 1;
		DATA_RAM_ADDR_WIDTH: positive := 12;
		INPUT_RAM_ADDR_WIDTH: positive := 11
	);
	PORT (
-- clocks
		clk40			: in std_logic;
		clk_mgt_bus		: in packet_sl_array(NProcessorFPGA*2 - 1 downto 0);
		clk_320			: in std_logic;
		clk_ipb			: in std_logic;
		rst_ipb			: in std_logic;
		ECR_320			: in std_logic;
-- eFEX number
		eFEX_number		: in std_logic_vector(7 downto 0);
-- TTC FIFO data
		rst_ttc			: in std_logic;
		ttc_wr_en		: in std_logic;
		ttc_din			: in std_logic_vector(43 downto 0);
-- data from MGT
		data_from_mgt_bus	: in mgt_data_array(NProcessorFPGA*2 - 1 downto 0);		-- first TOB then Input Data for each FPGA in turn
		char_is_k_bus		: in packet_sl_array(NProcessorFPGA*2 - 1 downto 0);	-- first TOB then Input Data for each FPGA in turn
		error_from_mgt_bus	: in packet_sl_array(NProcessorFPGA*2 - 1 downto 0);	-- first TOB then Input Data for each FPGA in turn
-- data to Aurora
		payload_data_bus	: out packet_data_array(NAurora_links - 1 downto 0);
		payload_valid_bus	: out packet_sl_array(NAurora_links - 1 downto 0);
		payload_last_bus	: out packet_sl_array(NAurora_links - 1 downto 0);
		tready_data_bus		: in packet_sl_array(NAurora_links - 1 downto 0);
-- kludge for packet enable
		packet_enable_vld	: in std_logic := '0';
		packet_enable		: in std_logic_vector(NProcessorFPGA*2 downto 0) := (Others => '0')
	);
END ENTITY packet_block;

Architecture rtl of packet_block is

COMPONENT ila_0
	PORT (
		clk : IN STD_LOGIC;
		probe0 : IN STD_LOGIC_VECTOR(67 DOWNTO 0)
	);
END COMPONENT ila_0;

COMPONENT fifo_40M_160M is
	PORT (
		rst : IN STD_LOGIC;
		wr_clk : IN STD_LOGIC;
		rd_clk : IN STD_LOGIC;
		din : IN STD_LOGIC_VECTOR(43 DOWNTO 0);
		wr_en : IN STD_LOGIC;
		rd_en : IN STD_LOGIC;
		dout : OUT STD_LOGIC_VECTOR(43 DOWNTO 0);
		full : OUT STD_LOGIC;
		empty : OUT STD_LOGIC
	);
END COMPONENT fifo_40M_160M;

Component efex_packet_builder is
	port (
		clk					: in std_logic;
		rst_clk				: in std_logic;
-- FIFO signals
		packet_data         : IN std_logic_vector (63 DOWNTO 0) ;
		packet_valid        : IN std_logic;
		packet_hdr          : IN std_logic;
		packet_data_end     : IN std_logic;
		packet_ready		: OUT std_logic;
-- towards Aurora readout
		payload_data        : OUT std_logic_vector (63 DOWNTO 0) ;
		payload_valid       : OUT std_logic;
		payload_last        : OUT std_logic;
		tready_data         : IN std_logic
	);
end Component efex_packet_builder;

Component efex_packet_mux is
	generic(NSRC: positive);
	port (
		clk					: in std_logic;
		rst_clk				: in std_logic;
		packet_mux_enabled  : IN packet_sl_array(NSRC-1 downto 0);
-- Input signals
		packet_mux_data     : IN packet_data_array(NSRC-1 downto 0);
		packet_mux_valid    : IN packet_sl_array(NSRC-1 downto 0);
		packet_mux_hdr      : IN packet_sl_array(NSRC-1 downto 0);
		packet_mux_data_end : IN packet_sl_array(NSRC-1 downto 0);
		packet_mux_ready	: OUT packet_sl_array(NSRC-1 downto 0);
-- FIFO signals
		packet_data         : OUT std_logic_vector (63 DOWNTO 0) ;
		packet_valid        : OUT std_logic;
		packet_hdr          : OUT std_logic;
		packet_data_end     : OUT std_logic;
		packet_ready		: IN std_logic
	);
end Component efex_packet_mux;

Component fwft_register is
	GENERIC(
		DATA_WIDTH: positive := 64
	);
	Port ( 
		clk			: in std_logic;
		rst_clk		: in std_logic;
-- Input signals
		in_data     : IN std_logic_vector(DATA_WIDTH-1 DOWNTO 0);
		in_valid    : IN std_logic;
		in_ready	: OUT std_logic;
-- Output signals
		out_data    : OUT std_logic_vector(DATA_WIDTH-1 DOWNTO 0);
		out_valid   : OUT std_logic;
		out_ready	: IN std_logic
	);
end Component fwft_register;

Component mgt_buffer IS
	GENERIC(
		RAM_ADDR_WIDTH		: positive := 12;
		INPUT_FPGA_NO		: std_logic_vector(1 downto 0) := "00";
		DATA_FORMAT_VERSION	: std_logic_vector (2 DOWNTO 0) := "001"
	);
	port (
-- Static signals
		eFEX_number			: IN std_logic_vector(7 downto 0);
		Stream_Type			: IN std_logic_vector (1 DOWNTO 0) := "00"; -- "00" TOB, "10" Input Data, "11" Debug
-- data from MGT
		clk_mgt				: in std_logic;
		data_from_mgt		: in std_logic_vector(31 downto 0);
		char_is_k			: in std_logic;
		error_from_mgt		: in std_logic;
		free_space			: out std_logic_vector(RAM_ADDR_WIDTH downto 0);
		fifo_full			: OUT std_logic;
-- data to packet builder
		clk_320				: in std_logic;
		rst_320				: in std_logic;
		packet_data			: OUT std_logic_vector (63 DOWNTO 0);
		packet_valid		: OUT std_logic;
		packet_hdr			: OUT std_logic;
		packet_data_end		: OUT std_logic;
		packet_ready		: IN std_logic;
-- interface to IPBus
		clk_ipb				: in std_logic;
		rst_ipb				: in std_logic;
		ipb_in				: in ipb_wbus;
		ipb_out				: out ipb_rbus
	);
END Component mgt_buffer;

Component rom_packet is
	generic (
		DATA_WIDTH			: integer :=   64;	-- DATA bus width
		ROM_ADDR_WIDTH		: integer :=   5;	-- width of rom address bus
		ROM_LENGTH			: integer :=   32;	-- number of words to read from ROM
		FORMAT_VERSION		: std_logic_vector (2 DOWNTO 0) := "001";
		STREAM_TYPE			: std_logic_vector (1 DOWNTO 0) := "00" -- "00" TOB, "10" Input Data, "11" Debug
	);
	port (
		clk					: in std_logic;
		rst_clk				: in std_logic;
-- Status signals
		eFEX_number			: IN std_logic_vector(7 downto 0);
-- FIFO signals
		ttc_rd_en			: OUT STD_LOGIC;
		ttc_dout			: IN STD_LOGIC_VECTOR(71 DOWNTO 0);
		ttc_fifo_empty		: IN STD_LOGIC;
-- towards packet_builder readout
		packet_data         : OUT std_logic_vector (63 DOWNTO 0) ;
		packet_valid        : OUT std_logic;
		packet_hdr          : OUT std_logic;
		packet_data_end     : OUT std_logic;
		packet_ready		: IN std_logic
	);
end Component rom_packet;

    constant FPGA_mapping		: STD_LOGIC_VECTOR (7 downto 0) := x"9C" ; -- HW address to processor number

	SIGNAL packet_data     		: std_logic_vector(65 DOWNTO 0);
	SIGNAL packet_ready    		: std_logic;
	SIGNAL packet_valid    		: std_logic;

	SIGNAL packet_builder_data 	: std_logic_vector(65 DOWNTO 0);
	SIGNAL packet_builder_ready	: std_logic;
	SIGNAL packet_builder_valid	: std_logic;

	SIGNAL ttc_rd_en			: std_logic;
	signal ttc_delay_d			: std_logic_vector(43 DOWNTO 0);
	signal ttc_delay_q			: std_logic_vector(43 DOWNTO 0);
	signal ttc_delay_rd			: std_logic;
	signal ttc_delay_wr			: std_logic;
	signal q_i					: std_logic_vector(3 DOWNTO 0);
	SIGNAL ttc_dout				: std_logic_vector(71 DOWNTO 0);
	SIGNAL ttc_fifo_empty		: std_logic;

	SIGNAL rst_320_sig			: std_logic;

-- Extra source for local packet and ordering is Input Data and TOBs each ordered by Processor Number
	SIGNAL packet_enable_bus						: packet_sl_array(NProcessorFPGA*2 downto 0) := (Others => '1');
	SIGNAL packet_data_bus, mux_data_bus		  	: packet_data_array(NProcessorFPGA*2 downto 0);
	SIGNAL packet_data_end_bus, mux_data_end_bus	: packet_sl_array(NProcessorFPGA*2 downto 0);
	SIGNAL packet_hdr_bus, mux_hdr_bus   			: packet_sl_array(NProcessorFPGA*2 downto 0);
	SIGNAL packet_ready_bus, mux_ready_bus			: packet_sl_array(NProcessorFPGA*2 downto 0);
	SIGNAL packet_valid_bus, mux_valid_bus		 	: packet_sl_array(NProcessorFPGA*2 downto 0);

	SIGNAL packet_enable_reg		: std_logic_vector(NProcessorFPGA*2 downto 0) := (Others => '1');
	signal packet_enable_vld_reg	: std_logic := '0';

	signal mgt_ttc_we_bus		: packet_sl_array(NProcessorFPGA*2-1 downto 0);

	attribute ASYNC_REG: string;
	attribute ASYNC_REG of packet_enable_reg: signal is "TRUE";
	attribute ASYNC_REG of packet_enable_vld_reg: signal is "TRUE";

BEGIN

	ttc_dout(71 downto 44) <= (Others => '0');

TOB_sources: for i in 0 to NProcessorFPGA-1 generate
	MGT_object : mgt_buffer
		GENERIC MAP (
			RAM_ADDR_WIDTH		=> DATA_RAM_ADDR_WIDTH,
			INPUT_FPGA_NO		=> FPGA_mapping(i*2+1 downto i*2),
			DATA_FORMAT_VERSION	=> "001"
		)
		port map (
-- Static signals
			eFEX_number			=> eFEX_number,
			Stream_Type			=> "00",
-- data from MGT
			clk_mgt				=> clk_mgt_bus(i*2),
			data_from_mgt	 	=> data_from_mgt_bus(i*2),
			char_is_k		 	=> char_is_k_bus(i*2),
			error_from_mgt	 	=> error_from_mgt_bus(i*2),
			free_space			=> OPEN,
			fifo_full			=> mgt_ttc_we_bus(i*2),
-- data to packet builder
			clk_320				=> clk_320,
			rst_320				=> rst_320_sig,
			packet_data			=> packet_data_bus(to_integer(unsigned(FPGA_mapping(i*2+1 downto i*2)))),
			packet_valid		=> packet_valid_bus(to_integer(unsigned(FPGA_mapping(i*2+1 downto i*2)))),
			packet_hdr			=> packet_hdr_bus(to_integer(unsigned(FPGA_mapping(i*2+1 downto i*2)))),
			packet_data_end		=> packet_data_end_bus(to_integer(unsigned(FPGA_mapping(i*2+1 downto i*2)))),
			packet_ready		=> packet_ready_bus(to_integer(unsigned(FPGA_mapping(i*2+1 downto i*2)))),
-- interface to IPBus
			clk_ipb				=> clk_ipb,
			rst_ipb				=> rst_ipb,
			ipb_in				=> IPB_WBUS_NULL,
			ipb_out				=> OPEN
		);
	End generate TOB_sources;

Bulk_sources: for i in 0 to NProcessorFPGA-1 generate
	MGT_object : mgt_buffer
		GENERIC MAP (
			RAM_ADDR_WIDTH		=> INPUT_RAM_ADDR_WIDTH,
			INPUT_FPGA_NO		=> FPGA_mapping(i*2+1 downto i*2),
			DATA_FORMAT_VERSION	=> "001"
		)
		port map (
-- Static signals
			eFEX_number			=> eFEX_number,
			Stream_Type			=> "10",
-- data from MGT
			clk_mgt				=> clk_mgt_bus(i*2+1),
			data_from_mgt	 	=> data_from_mgt_bus(i*2+1),
			char_is_k		 	=> char_is_k_bus(i*2+1),
			error_from_mgt	 	=> error_from_mgt_bus(i*2+1),
			free_space			=> OPEN,
			fifo_full			=> mgt_ttc_we_bus(i*2+1),
-- data to packet builder
			clk_320				=> clk_320,
			rst_320				=> rst_320_sig,
			packet_data			=> packet_data_bus(NProcessorFPGA+to_integer(unsigned(FPGA_mapping(i*2+1 downto i*2)))),
			packet_valid		=> packet_valid_bus(NProcessorFPGA+to_integer(unsigned(FPGA_mapping(i*2+1 downto i*2)))),
			packet_hdr			=> packet_hdr_bus(NProcessorFPGA+to_integer(unsigned(FPGA_mapping(i*2+1 downto i*2)))),
			packet_data_end		=> packet_data_end_bus(NProcessorFPGA+to_integer(unsigned(FPGA_mapping(i*2+1 downto i*2)))),
			packet_ready		=> packet_ready_bus(NProcessorFPGA+to_integer(unsigned(FPGA_mapping(i*2+1 downto i*2)))),
-- interface to IPBus
			clk_ipb				=> clk_ipb,
			rst_ipb				=> rst_ipb,
			ipb_in				=> IPB_WBUS_NULL,
			ipb_out				=> OPEN
		);
	End generate Bulk_sources;

q_i(0) <= ttc_wr_en;

ttc_delay_strobe : for i in 0 to 2 generate
	SRLC32E_delay_strobe : SRLC32E
    	generic map (
        	INIT => X"00000000")
        port map (
			Q   => Open,
			Q31 => q_i(i+1),                -- SRL cascaded data output
			A   => (Others => '1'),         -- Select input
			CE  => '1',                     -- Clock enable input
			CLK => CLK40,                   -- Clock input
			D   => q_i(i)                   -- SRL data input
			);
	end generate ttc_delay_strobe;

ttc_fifo_delay: fifo_40M_160M
	port map (
		rst		=> rst_ttc,
		wr_clk	=> clk40,
		rd_clk	=> clk40,
		din		=> ttc_din,
		wr_en	=> ttc_wr_en,
		rd_en	=> q_i(3),
		dout	=> ttc_delay_q,
		full	=> OPEN,
		empty	=> Open
	);

ttc_fifo_320: fifo_40M_160M
	port map (
		rst		=> rst_ttc,
		wr_clk	=> clk40,
		rd_clk	=> clk_320,
		din		=> ttc_delay_d,
		wr_en	=> ttc_delay_wr,
		rd_en	=> ttc_rd_en,
		dout	=> ttc_dout(43 downto 0),
		full	=> OPEN,
		empty	=> ttc_fifo_empty
	);

ttc_fifo_connect: process(clk40)
	Begin
		if rising_edge(clk40) then
			ttc_delay_rd <= q_i(3)
-- pragma translate_off
			after 2 ns
-- pragma translate_on
			;
			ttc_delay_wr <= ttc_delay_rd
-- pragma translate_off
			after 2 ns
-- pragma translate_on
			;
			ttc_delay_d <= ttc_delay_q
-- pragma translate_off
			after 2 ns
-- pragma translate_on
			;
		end if;
	End process ttc_fifo_connect;

local_packet: rom_packet
	GENERIC MAP (
		DATA_WIDTH     => 64,         -- DATA bus width
		ROM_ADDR_WIDTH => 5,          -- width of rom address bus
		ROM_LENGTH     => 32,         -- number of words to read from ROM
		FORMAT_VERSION => "001",
		STREAM_TYPE    => "11"
	)
	PORT MAP (
		clk            => clk_320,
		rst_clk        => rst_320_sig,
-- Status signals
		eFEX_number    => eFEX_number,
-- FIFO signals
		ttc_rd_en      => ttc_rd_en,
		ttc_dout       => ttc_dout,
		ttc_fifo_empty => ttc_fifo_empty,
-- towards packet_builder readout
		packet_data		=> packet_data_bus(NProcessorFPGA*2),
		packet_valid	=> packet_valid_bus(NProcessorFPGA*2),
		packet_hdr		=> packet_hdr_bus(NProcessorFPGA*2),
		packet_data_end	=> packet_data_end_bus(NProcessorFPGA*2),
		packet_ready	=> packet_ready_bus(NProcessorFPGA*2)
	);

MUX_register_block: For i in 0 to NProcessorFPGA*2 generate
	register_object : fwft_register
		GENERIC MAP (
			DATA_WIDTH     => 66         -- DATA bus width + 2
		)
		port map (
			clk			=> clk_320,
			rst_clk		=> rst_320_sig,
-- Input signals
			in_data(65) => packet_hdr_bus(i),
			in_data(64) => packet_data_end_bus(i),
			in_data(63 downto 0) => packet_data_bus(i),
			in_valid    => packet_valid_bus(i),
			in_ready	=> packet_ready_bus(i),
-- Output signals
			out_data(65) => mux_hdr_bus(i),
			out_data(64) => mux_data_end_bus(i),
			out_data(63 downto 0) => mux_data_bus(i),
			out_valid   => mux_valid_bus(i),
			out_ready	=> mux_ready_bus(i)
		);
	End generate MUX_register_block;

Packet_MUX : efex_packet_mux
	GENERIC MAP (
		NSRC     => NProcessorFPGA*2+1
	)
	PORT MAP (
		clk            		=> clk_320,
		rst_clk         	=> rst_320_sig,
		packet_mux_enabled  => packet_enable_bus,
		packet_mux_data     => mux_data_bus,
		packet_mux_valid    => mux_valid_bus,
		packet_mux_hdr      => mux_hdr_bus,
		packet_mux_data_end => mux_data_end_bus,
		packet_mux_ready	=> mux_ready_bus,
		packet_data 		=> packet_data(63 downto 0),
		packet_valid		=> packet_valid,
		packet_hdr			=> packet_data(65),
		packet_data_end	 	=> packet_data(64),
		packet_ready  		=> packet_ready
	);

Packet_Builder_register : fwft_register
	GENERIC MAP (
		DATA_WIDTH     => 66         -- DATA bus width + 2
	)
	port map (
		clk			=> clk_320,
		rst_clk		=> rst_320_sig,
-- Input signals
		in_data     => packet_data,
		in_valid    => packet_valid,
		in_ready	=> packet_ready,
-- Output signals
		out_data    => packet_builder_data,
		out_valid   => packet_builder_valid,
		out_ready	=> packet_builder_ready
	);

Packet_Builder : efex_packet_builder
	PORT MAP (
		clk             => clk_320,
		rst_clk         => rst_320_sig,
		packet_data     => packet_builder_data(63 downto 0),
		packet_valid    => packet_builder_valid,
		packet_hdr      => packet_builder_data(65),
		packet_data_end => packet_builder_data(64),
		packet_ready    => packet_builder_ready,
		payload_data    => payload_data_bus(0),
		payload_valid   => payload_valid_bus(0),
		payload_last    => payload_last_bus(0),
		tready_data     => tready_data_bus(0)
	);

Packet_enable_capture_block: process(clk_320)
	Variable stretch: std_logic_vector(15 downto 0) := (Others => '0');
	begin
		if rising_edge(clk_320) then
			if packet_enable_vld_reg = '1' then
				stretch := stretch(14 downto 0) & '1';
			else
				stretch := (Others => '0');
			end if;
			if stretch(15) = '1' then
				For i in 0 to NProcessorFPGA*2 loop
					packet_enable_bus(i) <= packet_enable_reg(i)
-- pragma translate_off
					after 2 ns
-- pragma translate_on
					;
				end loop;
			end if;
		end if;
	end process Packet_enable_capture_block;

Packet_enable_reg_block: process(clk_320)
	begin
		if rising_edge(clk_320) then
			packet_enable_vld_reg <= packet_enable_vld
-- pragma translate_off
			after 2 ns
-- pragma translate_on
			;
			packet_enable_reg <= packet_enable
-- pragma translate_off
			after 2 ns
-- pragma translate_on
			;
		end if;
	end process Packet_enable_reg_block;

Reset_block: process(clk_320)
	Variable stretch: std_logic_vector(7 downto 0) := (Others => '1');
	Variable seen_l1a: std_logic := '0';
	begin
-- Keep reset high until we see first L1A in ttc_fifo...
-- and reset on ECR
		if rising_edge(clk_320) then
			if ttc_fifo_empty = '0' then
				seen_l1a := '1';
			end if;
			if (seen_l1a = '0') or (ECR_320 = '1') then
				stretch := (Others => '1');
			end if;
			rst_320_sig <= stretch(7)
-- pragma translate_off
			after 2 ns
-- pragma translate_on
			;
			stretch := stretch(6 downto 0) & "0";
		end if;
	end process Reset_block;

END Architecture rtl;
