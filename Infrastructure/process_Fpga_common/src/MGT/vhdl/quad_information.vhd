----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/03/2017 03:01:27 PM
-- Design Name: 
-- Module Name: quad_information - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;
use work.ipbus_decode_mgt_registers.ALL;

entity quad_information is
generic (addr_width :natural :=2);    
 Port ( 
        ipb_clk             : in std_logic;                     
        ipb_rst             : in std_logic;                     
        ipb_in              : in ipb_wbus;                      
        ipb_out             : out ipb_rbus;                     
        loopback            : out std_logic_vector(2 downto 0);
        bc_reg_sel          : out std_logic_vector(15 downto 0);
        mux_sel             : out std_logic_vector(15 downto 0);        
        softreset_tx        : out std_logic;                    
        softreset_rx        : out std_logic;                    
        error_counter_reset : out std_logic; 
        mgt_enable          : out std_logic_vector (3 downto 0);      
        qpll_lock           : in std_logic;                     
        qpll_refclklost     : in std_logic                     
                            
 
 
       );
end quad_information;

architecture Behavioral of quad_information is
  
type reg_array is array(2**addr_width-1 downto 0) of std_logic_vector(31 downto 0);
signal reg: reg_array;                       
signal sel:integer;     
signal ack: std_logic;  
signal status :std_logic_vector(31 downto 0); 

signal q:  STD_LOGIC_VECTOR(2**addr_width*32-1 downto 0);
signal enable_gt3,enable_gt2,enable_gt1,enable_gt0:std_logic;
signal loopback_int   :std_logic_vector(2 downto 0);

begin
 
mgt_enable <=  enable_gt3 & enable_gt2 & enable_gt1 & enable_gt0;

status <= x"0000000" & "00" & qpll_refclklost  & qpll_lock ;   --- concatenating  the pll  status signals
                                                          
sel <= to_integer(unsigned(ipb_in.ipb_addr(addr_width-1 downto 0))) when addr_width > 0 else 0;
                                                          
 process(ipb_clk,ipb_rst )        
 
  
         begin
         
         if rising_edge(ipb_clk) then
             if ipb_rst='1' then
                 reg <= (others=>(others=>'0'));
             elsif ipb_in.ipb_strobe='1' and ipb_in.ipb_write='1' then -- write ipbus is selected 
                 reg(sel) <= ipb_in.ipb_wdata;                         -- write the selected register
             end if;
 
            if sel = 3 then                                     -- if address is 3 read the status register that checks the status of the PLL 
               ipb_out.ipb_rdata <= status;
               ack <= ipb_in.ipb_strobe and not ack;
               else
             ipb_out.ipb_rdata <= reg(sel);                     -- read selected register 
             ack <= ipb_in.ipb_strobe and not ack;
             end if ;
           end if;
                                    
      end process;
      
                     
       q_gen: for i in 2**addr_width-1 downto 0 generate        -- create number of register of addr_width
                 q((i+1)*32-1 downto i*32) <= reg(i);
             end generate;
 
         loopback            <= q(2 downto 0); -- set loopback
         enable_gt0          <= q(4);         -- enable for gt0  
         enable_gt1          <= q(5);         -- enable for gt1  
         enable_gt2          <= q(6);         -- enable for gt2  
         enable_gt3          <= q(7);         -- enable for gt3  
         
         bc_reg_sel          <= q(47 downto 32);
         mux_sel             <= q(63 downto 48);                
         
         softreset_tx        <= q(64);          -- set softreset mgts in the of tx
         softreset_rx        <= q(65);          --set softeset mgts in the rx side                                                
         error_counter_reset <= q(66);         -- set reset error counter of the mgt 

          
          
          
         ipb_out.ipb_ack <= ack;                            
         ipb_out.ipb_err <= '0'; 
                                  

end Behavioral;
