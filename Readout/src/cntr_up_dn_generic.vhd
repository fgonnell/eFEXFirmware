--! @file
--! @brief Up/Down counter
--! @details
--! \verbatim 
--! This is a generic Up/Down counter.
--! The frame counter is incremented when EN and UP signals are asserted,
--! and decremented when  EN and DOWN signals are asserted.
--! If Read and Write happen on the same clock cycle, the ouptut of counter does not change.
--! \endverbatim 
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.all;

--! @copydoc cntr_up_dn_generic.vhd
entity cntr_up_dn_generic is
    generic (
        width  : integer := 16
        );
    Port ( 
       --! Enable signal input
       CE   : in STD_LOGIC;
       --! Clock signal input
       CLK  : in STD_LOGIC;
       --! Reset signal input
       RST  : in STD_LOGIC;
       --! Count UP signal input
       UP   : in STD_LOGIC;
       --! Count DOWN signal input
       DOWN : in STD_LOGIC;
       --! Counter Output signal 
       Q    : out STD_LOGIC_VECTOR (width-1 downto 0)
       );
end cntr_up_dn_generic;

--! @copydoc cntr_up_dn_generic.vhd
architecture Behavioral of cntr_up_dn_generic is

	signal temp : unsigned (width-1 downto 0) ;

begin
process (CLK)
	begin
	if CLK'event AND CLK = '1' then
     	if RST = '1' then             -- this is a synchronous reset 
      		temp <= (others => '0');              -- set counter to offset value given.
		else
			if UP = '1' AND DOWN = '0'then
					temp <= temp + 1;			          -- else increment the value by one 
			else
				if UP = '0' AND DOWN = '1' then 
					if temp > 0 then	     -- if count > 0
						temp <= temp - 1;		-- else decrement the value by one 
				    end if;
				else
				    temp <= temp;	-- do nothing
				end if;
			end if;
		end if;
	end if;
	end process;
	Q <= std_logic_vector(temp) ;	 -- push out new value

end Behavioral;
