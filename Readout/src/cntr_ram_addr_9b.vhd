--! @file
--! @brief 9b counter
--! @details 
--! \verbatim
--! This is a 9 bit counter with offset.
--! It is used to generate the read/write address for the DPRAM of the TOB Sorting block
--! \endverbatim
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.all;

--! @copydoc cntr_ram_addr_9b.vhd
entity cntr_ram_addr_9b is
--    generic (
--    T_count       : in STD_LOGIC_VECTOR (8 downto 0);
--    ld_count      : in STD_LOGIC_VECTOR (8 downto 0)
--    );

    Port ( 
       CE     : in STD_LOGIC;
       CLK    : in STD_LOGIC;
       RST    : in STD_LOGIC;
       Q      : out STD_LOGIC_VECTOR (8 downto 0)
       );
    end cntr_ram_addr_9b;

--! @copydoc cntr_ram_addr_9b.vhd
architecture Behavioral of cntr_ram_addr_9b is

begin
process (CLK)
	variable temp  : unsigned (8 downto 0) ;
--	variable terminal_cnt : unsigned (8 downto 0):= (others => '1') ;
	begin

	if CLK'event AND CLK = '1' then
     	if RST = '1' then           -- this is a synchronous reset
      	   temp := (others => '0') ;          -- set counter to zero
		else 
		   if CE = '1' then
		      temp := temp + 1;
		   end if;
		 end if;
	end if;
	Q <= std_logic_vector(temp) ;	 -- push out new value
	end process;


end Behavioral;
