library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- 2018/02/07: implementation timer constants

package watchdog is

    constant TIMEBASE:        natural := 31250 ;-- = 1 millisecond
    constant DEFAULT_TIMEOUT: natural := 250 ;-- = 0.25 seconds
	 
end watchdog;

