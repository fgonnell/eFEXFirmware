----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/06/2017 01:36:04 PM
-- Design Name: 
-- Module Name: SIPO_unit_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;
use IEEE.numeric_std.ALL;

library TOB_rdout_sim;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SIPO_unit_tb is
--    data_in          : in STD_LOGIC_VECTOR (511 downto 0);   -- TOBs 64b * 8          
--    data_valid_in    : in STD_LOGIC_VECTOR (7 downto 0);     -- 8b TOB valid signal 
--    data_sync_in     : in STD_LOGIC;
--    clk_in_280M      : in STD_LOGIC;
--    data_sync_out    : out STD_LOGIC;
--    TOB_valid_out    : out STD_LOGIC_VECTOR (39 downto 0);   -- TOBs valid (8b * 5) = 40b
--    data_out         : out STD_LOGIC_VECTOR (2559 downto 0)  -- TOBs (64b * 8) * 5 = 2560b
end SIPO_unit_tb;

architecture Behavioral of SIPO_unit_tb is

    
    signal sys_rst : std_logic := '0';
    signal clk_40M : std_logic := '0';
    constant clk_40_period : time := 25 ns;
    signal clk_200M : std_logic := '0';
    constant clk_200_period : time := 5 ns;
    signal TX_DATA_OUT_i : std_logic_vector(31 downto 0) ;
    signal TXCTRL_OUT_i  : std_logic_vector(3 downto 0) ;
    signal SIPO_sync_out_i : std_logic ; 
    signal SIPO_TOBs_out_i : std_logic_vector(1279 downto 0) ;  
begin

    rst_proc : process
    begin
        sys_rst <= '1' ;
        wait for clk_40_period * 4 ;
        sys_rst <= '0' ;
        wait;
    end process;

    clk_40M_proc : process
    begin
         clk_40M <= '0';
         wait for clk_40_period/2;  --for 0.5 ns signal is '0'.
         clk_40M <= '1';
         wait for clk_40_period/2;  --for next 0.5 ns signal is '1'.
    end process;

    clk_200M_proc : process
    begin
         clk_200M <= '0';
         wait for clk_200_period/2;  --for 0.5 ns signal is '0'.
         clk_200M <= '1';
         wait for clk_200_period/2;  --for next 0.5 ns signal is '1'.
    end process;


U0 : entity TOB_rdout_sim.gen_32b_TOBs_tst 
    generic map
    (
        WORDS_IN_BRAM =>   721
        FILE_NAME     => "TOBs_rom_init_tx_tst.dat"
    )   
    port map
    (
        -- User Interface
        TX_DATA_OUT     => TX_DATA_OUT_i ,
        TXCTRL_OUT      => TXCTRL_OUT_i  ,
        -- System Interface
        USER_CLK        => clk_200M ,      
        SYSTEM_RESET    => sys_rst  
    );
    

    -- TOB_eg_Valid_flg_in_i <= '0' & rand_out_280M(6 downto 0) ;    -- 8 bit assignment for TOB e/g valid

U1 : entity TOB_rdout_lib.SIPO_unit  -- convert 5x256b TOBs into one 1280b word for Circular DPR
    Port map ( 
        data_in          =>  TX_DATA_OUT_i & TX_DATA_OUT_i & TX_DATA_OUT_i & TX_DATA_OUT_i & TX_DATA_OUT_i & TX_DATA_OUT_i & TX_DATA_OUT_i & TX_DATA_OUT_i, -- 256b data (8 x 32) 
        data_valid_in
        data_sync_in     =>  TXCTRL_OUT_i(0), -- only bit 0 contains a simulation sync signal
        clk_in_200M      =>  clk_200M,
        data_sync_out    =>  SIPO_sync_out_i,
        TOB_valid_out
        data_out         =>  SIPO_TOBs_out_i
        );
end Behavioral;


