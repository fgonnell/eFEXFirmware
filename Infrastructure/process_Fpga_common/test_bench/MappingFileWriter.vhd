----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

use IEEE.NUMERIC_STD.all;

library std;
use std.textio.all;             --TEXT, FILE is here


library algolib;
use algolib.DataTypes.all;
use algolib.AlgoDataTypes.all;
use algolib.Utility.all;


entity MappingFileWriter is
  generic(
    sync_delimiter : string := "---";
    filename       : string := "out_file.txt");
  port (
    CLK     : in std_logic;
    RST     : in std_logic := '0';
    DATA_IN : in Algoinput;
    WE      : in std_logic
    );
end MappingFileWriter;

--
architecture Behavioral of MappingFileWriter is

  file outfile   : text open write_mode is filename;
  signal cnt     : integer := 0;
  signal syn_cnt : integer := 0;
begin


  process(clk)

  begin
    if RST = '1' then
      print(outfile, "RESET");
    elsif rising_edge(clk) then
      cnt <= cnt + 1;
      if WE = '1' then
        data_out_for_col : for j in 0 to 5 loop
          data_out_for_row : for i in 0 to 9 loop
            print(outfile, hstr(DATA_IN(j)(i).Layer0(0)) & "  " &
                  hstr(DATA_IN(j)(i).Layer1(3)) & " " & hstr(DATA_IN(j)(i).Layer1(2)) & " " & hstr(DATA_IN(j)(i).Layer1(1)) & " " & hstr(DATA_IN(j)(i).Layer1(0)) & "  "
                  & hstr(DATA_IN(j)(i).Layer2(3)) & " " & hstr(DATA_IN(j)(i).Layer2(2)) & " " & hstr(DATA_IN(j)(i).Layer2(1)) & " " & hstr(DATA_IN(j)(i).Layer2(0)) & "  "
                  & hstr(DATA_IN(j)(i).Layer3(0)) &
                  "  " & hstr(DATA_IN(j)(i).Hadron(0)));
          end loop data_out_for_row;
        end loop data_out_for_col;
        print(outfile, "   ");
      end if;

    end if;

  end process;
end Behavioral;

