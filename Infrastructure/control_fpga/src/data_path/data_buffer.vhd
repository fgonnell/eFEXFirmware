----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.11.2018 10:55:28
-- Design Name: 
-- Module Name: data_buffer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
LIBRARY infrastructure_lib;
USE infrastructure_lib.all;

entity data_buffer is
  Port ( 
      rx_clk   : in std_logic;
      clk160   : in std_logic;
      reset    : in std_logic;
      pe       : in std_logic;
      rxdata   : in std_logic_vector(31 downto 0);
      data_buff: out std_logic_vector(71 downto 0); 
      valid    : out std_logic; 
      empty    : OUT STD_LOGIC;  
      rd_en    : in std_logic     
      );
end data_buffer;

architecture Behavioral of data_buffer is

COMPONENT input_fifo
  PORT (
    rst          : IN STD_LOGIC;
    wr_clk       : IN STD_LOGIC;
    rd_clk       : IN STD_LOGIC;
    din          : IN STD_LOGIC_VECTOR(35 DOWNTO 0);
    wr_en        : IN STD_LOGIC;
    rd_en        : IN STD_LOGIC;
    dout         : OUT STD_LOGIC_VECTOR(71 DOWNTO 0);
    full         : OUT STD_LOGIC;
    almost_full  : OUT STD_LOGIC;
    empty        : OUT STD_LOGIC;
    almost_empty : OUT STD_LOGIC;
    valid        : OUT STD_LOGIC;
    wr_rst_busy  : OUT STD_LOGIC;
    rd_rst_busy  : OUT STD_LOGIC
    
  );
END COMPONENT;


signal rxdata_i :  std_logic_vector(31 downto 0);
signal wr_en_i:std_logic;
signal full_i,almost_full_i,empty_i, wr_rst_busy_i,rd_rst_busy_i,almost_empty_i,rd_en_i:std_logic;
signal data_i:std_logic_vector(35 downto 0);
signal wr_rst_busy,rd_rst_busy:std_logic;
begin
   
empty  <= empty_i;

reg: process(rx_clk)
      begin 
       if rx_clk ' event and rx_clk ='1' then
          rxdata_i <= rxdata;
        end if;
        end process;
-----------------------------------------------------------------------        
--- state machine that writes the data and stataus signals to the fifo
--------------------------------------------------------------------------        
sm_buffer: entity infrastructure_lib.sm_buff 
           port map ( 
              almost_full   => almost_full_i,
              pe            => pe,
              rxclk         => rx_clk,
              rxdata        => rxdata_i,
              ctrl_data     => rxdata(7 downto 0),
              data_fifo     => data_i,
              reset         => reset  ,
              wr_fifo       => wr_en_i
           );      
             
        
---------------------------------------------------------------------------
--- fifo that capture the incoming data 
-----------------------------------------------------------------------------
fifo_buff : input_fifo
           PORT MAP (
             rst         => reset,
             wr_clk      => rx_clk,
             rd_clk      => clk160,
             din         => data_i ,
             wr_en       => wr_en_i,
             rd_en       => rd_en_i,
             dout        => data_buff,
             full        => full_i,
             almost_full => almost_full_i,
             empty       => empty_i,
             almost_empty => almost_empty_i,
             valid        => valid,           
             wr_rst_busy   => wr_rst_busy,
             rd_rst_busy   => rd_rst_busy 
           );      

rd_en_i <= rd_en and not empty_i ;


end Behavioral;
