-- TB_source AXI version
--
-- Dave Sankey, May 2019

Library STD;
Use std.textio.all;

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;

LIBRARY ipbus_lib;
USE ipbus_lib.ipbus.all;
LIBRARY infrastructure_lib;
use infrastructure_lib.packet_mux_type.all;

ENTITY Aurora_TB IS
  GENERIC(
	NProcessor_FPGA: positive := 1;
	NAurora_output: positive := 1;
	RAM_ADDR_WIDTH: positive := 12
  );
END ENTITY Aurora_TB;

Architecture rtl of Aurora_TB is

Component TB_source is
  GENERIC(
    Fabric_clk: time := 4 ns;
	NTTC: positive
  );
  PORT(
	clk					: out std_logic;
	rst_clk				: out std_logic;
	ipbclk				: out std_logic;
	rst_ipbclk			: out std_logic;
	mgtclk				: out std_logic;
	rst_mgtclk			: out std_logic;
	ttc_clk				: out std_logic;
	ttc_l1a				: out std_logic;
	ttc_info			: out std_logic_vector(43 downto 0);
-- Status signals
	eFEX_number			: out std_logic_vector(7 downto 0);
-- TTC FIFO signals
-- Format of FIFO is
-- fifo_dout(71 downto 56): starting 64 bit address of payload data in RAM
-- fifo_dout(55 downto 48): number of 64 bit words for payload data excluding trailer
-- fifo_dout(47 downto 44): local error flags excluding header mismatch and processor timeout
-- fifo_dout(43 downto 12): extended L1ID
-- fifo_dout(11 downto 0):  BCN
    ttc_rd_en			: in packet_sl_array(NTTC-1 downto 0);
    ttc_dout			: out packet_metadata_array(NTTC-1 downto 0);
    ttc_fifo_empty		: out packet_sl_array(NTTC-1 downto 0);
-- towards Aurora readout
	payload_data        : in std_logic_vector (63 DOWNTO 0) ;
	payload_valid       : in std_logic;
	payload_last        : in std_logic;
	tready_data         : out std_logic
  );
END Component TB_source;

Component packet_block IS
	GENERIC(
		NTTC_clients: positive := 1;
		NProcessorFPGA: positive := 1;	-- first TOB then Input Data for each FPGA in turn
		NAurora_links: positive := 1;
		DATA_RAM_ADDR_WIDTH: positive := 12;
		INPUT_RAM_ADDR_WIDTH: positive := 11
	);
	PORT (
-- clocks
		clk40			: in std_logic;
		clk_mgt_bus		: in packet_sl_array(NProcessorFPGA*2 - 1 downto 0);
		clk_320			: in std_logic;
		clk_ipb			: in std_logic;
		rst_ipb			: in std_logic;
		ECR_320			: in std_logic;
-- eFEX number
		eFEX_number		: in std_logic_vector(7 downto 0);
-- TTC FIFO data
		rst_ttc			: in std_logic;
		ttc_wr_en		: in std_logic;
		ttc_din			: in std_logic_vector(43 downto 0);
-- data from MGT
		data_from_mgt_bus	: in mgt_data_array(NProcessorFPGA*2 - 1 downto 0);		-- first TOB then Input Data for each FPGA in turn
		char_is_k_bus		: in packet_sl_array(NProcessorFPGA*2 - 1 downto 0);	-- first TOB then Input Data for each FPGA in turn
		error_from_mgt_bus	: in packet_sl_array(NProcessorFPGA*2 - 1 downto 0);	-- first TOB then Input Data for each FPGA in turn
-- data to Aurora
		payload_data_bus	: out packet_data_array(NAurora_links - 1 downto 0);
		payload_valid_bus	: out packet_sl_array(NAurora_links - 1 downto 0);
		payload_last_bus	: out packet_sl_array(NAurora_links - 1 downto 0);
		tready_data_bus		: in packet_sl_array(NAurora_links - 1 downto 0)
	);
END Component packet_block;

Component TB_MGT_emulator is
  	GENERIC(
		SoF_char: std_logic_vector(7 downto 0) := x"3C" -- 3C = TOB, 7C = Input Data
    );
	port(
		clk_mgt			: IN STD_LOGIC;
-- TTC FIFO signals
-- Format of FIFO is
-- fifo_dout(71 downto 56): N/A
-- fifo_dout(55 downto 48): number of 64 bit words for payload data
-- fifo_dout(47 downto 44): N/A
-- fifo_dout(43 downto 12): extended L1ID
-- fifo_dout(11 downto 0):  BCN
	    fifo_rd_en		: OUT STD_LOGIC;
    	fifo_dout		: IN STD_LOGIC_VECTOR(71 DOWNTO 0);
    	fifo_empty		: IN STD_LOGIC;
-- data from MGT
		data_from_mgt	: out std_logic_vector(31 downto 0);
		char_is_k		: out std_logic;
		error_from_mgt	: out std_logic
	);
end Component TB_MGT_emulator;

	SIGNAL clk, ipbclk, mgtclk   : std_logic;
	SIGNAL rst_clk, rst_ipbclk, rst_mgtclk : std_logic;
	SIGNAL eFEX_number     		: std_logic_vector(7 DOWNTO 0);

	SIGNAL ttc_clk				: std_logic;
	SIGNAL ttc_l1a				: std_logic;
	SIGNAL ttc_info				: std_logic_vector(43 downto 0);

	SIGNAL clk_mgt_bus			: packet_sl_array(NProcessor_FPGA*2 - 1 downto 0);

	SIGNAL ttc_dout_bus     	: packet_metadata_array(NProcessor_FPGA*2 - 1 downto 0);
	SIGNAL ttc_fifo_empty_bus	: packet_sl_array(NProcessor_FPGA*2 - 1 downto 0);
	SIGNAL ttc_rd_en_bus    	: packet_sl_array(NProcessor_FPGA*2 - 1 downto 0);

	SIGNAL mgt_data_bus  	   	: mgt_data_array(NProcessor_FPGA*2 - 1 downto 0);
	SIGNAL mgt_char_is_k_bus	: packet_sl_array(NProcessor_FPGA*2 - 1 downto 0);
	SIGNAL mgt_error_bus     	: packet_sl_array(NProcessor_FPGA*2 - 1 downto 0);

	SIGNAL payload_data_bus		: packet_data_array(NAurora_output - 1 downto 0);
	SIGNAL payload_valid_bus	: packet_sl_array(NAurora_output - 1 downto 0);
	SIGNAL payload_last_bus		: packet_sl_array(NAurora_output - 1 downto 0);
	SIGNAL tready_data_bus		: packet_sl_array(NAurora_output - 1 downto 0);


BEGIN

-- Instance port mappings.
Source : TB_source
	GENERIC MAP (
		Fabric_clk => 4 ns,
		NTTC => NProcessor_FPGA*2
	)
	PORT MAP (
		clk				=> clk,
		rst_clk			=> rst_clk,
		ipbclk		    => ipbclk,
		rst_ipbclk	    => rst_ipbclk,
		mgtclk		    => mgtclk,
		rst_mgtclk		=> rst_mgtclk,
		ttc_clk			=> ttc_clk,
		ttc_l1a			=> ttc_l1a,
		ttc_info		=> ttc_info,
		eFEX_number		=> eFEX_number,
		ttc_rd_en		=> ttc_rd_en_bus,
		ttc_dout		=> ttc_dout_bus,
		ttc_fifo_empty	=> ttc_fifo_empty_bus,
		payload_data	=> payload_data_bus(0),
		payload_valid	=> payload_valid_bus(0),
		payload_last	=> payload_last_bus(0),
		tready_data		=> tready_data_bus(0)
	);

Packet_engine: packet_block
	GENERIC MAP (
		NTTC_clients		=> 1,
		NProcessorFPGA		=> NProcessor_FPGA,
		NAurora_links		=> NAurora_output,
		DATA_RAM_ADDR_WIDTH	=> RAM_ADDR_WIDTH,
		INPUT_RAM_ADDR_WIDTH=> RAM_ADDR_WIDTH-1
	)
	PORT MAP (
-- clocks
		clk40				=> ttc_clk,
		clk_mgt_bus			=> clk_mgt_bus,
		clk_320				=> clk,
		clk_ipb				=> ipbclk,
		rst_ipb				=> rst_ipbclk,
		ECR_320				=> '0',
-- eFEX number
		eFEX_number			=> eFEX_number,
-- TTC FIFO data
		rst_ttc				=> rst_ipbclk,
		ttc_wr_en			=> ttc_l1a,
		ttc_din				=> ttc_info,
-- data from MGT
		data_from_mgt_bus	=> mgt_data_bus,
		char_is_k_bus		=> mgt_char_is_k_bus,
		error_from_mgt_bus	=> mgt_error_bus,
-- data to Aurora
		payload_data_bus	=> payload_data_bus,
		payload_valid_bus	=> payload_valid_bus,
		payload_last_bus	=> payload_last_bus,
		tready_data_bus		=> tready_data_bus
	);

MGT_Sources: for i in 0 to NProcessor_FPGA*2-1 generate

	clk_mgt_bus(i) <= mgtclk;

	MGT_emulator : TB_MGT_emulator
		GENERIC MAP (
			SoF_char		 => std_logic_vector(to_unsigned(i, 2)) & "11" & x"C" -- 3C = TOB, 7C = Input Data
		)
		PORT MAP (
			clk_mgt			=> clk_mgt_bus(i),
-- TTC FIFO signals
-- Format of FIFO is
-- fifo_dout(71 downto 56): N/A
-- fifo_dout(55 downto 48): number of 64 bit words for packet data
-- fifo_dout(47 downto 44): N/A
-- fifo_dout(43 downto 12): extended L1ID
-- fifo_dout(11 downto 0):  BCN
			fifo_rd_en      => ttc_rd_en_bus(i),
			fifo_dout       => ttc_dout_bus(i),
			fifo_empty      => ttc_fifo_empty_bus(i),
-- data from MGT
			data_from_mgt	=> mgt_data_bus(i),
			char_is_k		=> mgt_char_is_k_bus(i),
			error_from_mgt	=> mgt_error_bus(i)
		);

  End generate MGT_Sources;

END Architecture rtl;
