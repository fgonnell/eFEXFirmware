--! @file
--! @brief Clock generating module
--! @details
--! This module contains the clock wizard modules used to generate all the clocks in the design.
--! @author Mohammed Siyad
--! @author Francesco Gonnella


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

--! @copydoc clk_resources.vhd
entity clk_resources is
  port (
    gt_clk_p   : in  std_logic;
    gt_clk_n   : in  std_logic;
    TTC_clk_p  : in  std_logic;
    TTC_clk_n  : in  std_logic;
    clk160     : out std_logic;
    clk200     : out std_logic;
    clk40      : out std_logic;
    clk280     : out std_logic;
    rst_macclk : out std_logic;
    onehz      : out std_logic;  -- this goes to the led_clk of the configure block 
    reset      : in  std_logic; -- to reset the mmcm of the 280M fabric clock
    rst_ipb    : out std_logic;
    load       : out std_logic;  -- goes to the top level of the algo block
    mac_clk    : out std_logic;
    locked_160m : out std_logic;    -- use this as this gets its clock from 40Mz signal
    ipb_clk    : out std_logic
    );
end clk_resources;

--! @copydoc clk_resources.vhd
architecture Behavioral of clk_resources is

  component clk_wiz_0
    port
      (mac_clk   : out std_logic;
       ipb_clk   : out std_logic;
       reset     : in  std_logic;
       locked    : out std_logic;
       clk_in1_p : in  std_logic;
       clk_in1_n : in  std_logic
       );
  end component;

  component clk_wiz_1
    port
      (clk40  : in  std_logic;
       clk160 : out std_logic;
       reset  : in  std_logic;
       locked : out std_logic
       );
  end component;


  component ClockWizard
    port
      (clk200    : out std_logic;
       load      : out std_logic;
       reset     : in  std_logic;
       clk40     : out std_logic;
       clk280    : out std_logic;
       locked    : out std_logic;
       clk_in1_p : in  std_logic;
       clk_in1_n : in  std_logic
       );
  end component;


  signal clk40_int, mac_clk_int, ipb_clk_int, d17, d17_d : std_logic;
  signal rst_ipb_ctrl, rsto_eth, rst, srst, rst_125      : std_logic            := '1';
  signal rctr                                            : unsigned(3 downto 0) := "0000";

  signal nuke, nuke_i, nuke_d, nuke_d2, eth_done, eth_locked, locked, locked_40m : std_logic := '0';

begin

  ipb_clk <= ipb_clk_int;
  mac_clk <= mac_clk_int;

  Inputclk40M : ClockWizard
    port map (

      -- Clock in ports
      clk_in1_p => TTC_clk_p,
      clk_in1_n => TTC_clk_n,
      -- Clock out ports
      clk200    => clk200,
      clk40     => clk40_int,
      clk280    => clk280,
      reset     => reset,
      load      => load,
      -- Status and control signals
      locked    => locked_40m
      );

  clk_gen : clk_wiz_0
    port map (
      clk_in1_p => gt_clk_p,
      clk_in1_n => gt_clk_n,
      mac_clk   => mac_clk_int,  -- 125MHz
      ipb_clk   => ipb_clk_int,  -- 31.25MHz
      reset     => '0',
      locked    => locked
      );

  clk160_gen : clk_wiz_1
    port map (

      -- Clock out ports
      clk160 => clk160,
      -- Status and control signals
      reset  => '0',
      locked => locked_160m,
      -- Clock in ports
      clk40  => clk40_int
      );

  clk40 <= clk40_int;

  clkdiv : entity work.ipbus_clock_div port map(
    clk => mac_clk_int,
    d17 => d17,
    d28 => onehz
    );

  process(mac_clk_int)
  begin
    if rising_edge(mac_clk_int) then
      d17_d <= d17;
      if d17 = '1' and d17_d = '0' then
        rst     <= nuke_d2 or not locked;
        nuke_d  <= nuke_i;      -- ~1ms time bomb (allows return packet to be sent)
        nuke_d2 <= nuke_d;
      --eth_done <= (eth_done or eth_locked) and not rst;
      --rsto_eth <= rst; -- delayed reset for ethernet block to avoid startup issues
      end if;
    end if;
  end process;

  srst <= '1' when rctr /= "0000" else '0';

  process(ipb_clk_int)
  begin
    if rising_edge(ipb_clk_int) then
      rst_ipb <= rst;
      nuke_i  <= nuke;
      if srst = '1' then        -- or  hard_rst = '1' then
        rctr <= rctr + 1;
      end if;
    end if;
  end process;

  process(mac_clk_int)
  begin
    if rising_edge(mac_clk_int) then
      rst_125 <= rst;           --or not eth_done;
    end if;
  end process;

  rst_macclk <= rst_125;

end Behavioral;
