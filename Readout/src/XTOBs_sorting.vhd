--! @file
--! @brief Top of Sorting XTOBs for process FPGA
--! @details 
--! \verbatim
--! This module interfaces with 8 ALGO Blocks, receives 8 x 64-bit XTOBs together with 8 valid signals in series,
--! The module consists of an SIPO unit, together with an FSM for handling input data, Circular DPRAM and de-randomisation FIFO.
--! 
--! The SIPO unit sorts input XTOB data into a 8 streams of 320b = 5 x 64-bit parallel word and 5 bit valid word to store in Circular DPRAM.
--! The SIPO data of 325b is saved every Bunch Crossing tick to enable multi-slice readout.
--!
--!
--! The 325-b XTOB data is transferred from Circular DPRAM into derandomisation FIFO upon arraival of L1A.
--!
--! Only Process FPGA 1 and 2 transmit e/g and tau events to TOPO, and the off-line Readout.
--! Process FPGA 3 and 4 do not transmit to TOPO instead they send their TOBs to Process FPGA 1 and 2, 
--! and transmit XTOB only events to the Control FPGA.
--!
--! \endverbatim 
--! @author Saeed Taghavi


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

library TOB_rdout_lib;
use TOB_rdout_lib.data_type_pkg.all;
use TOB_rdout_lib.TOB_rdout_ip_pkg.all;

library algolib;
use algolib.AlgoDataTypes.all;

--! @copydoc XTOBs_sorting.vhd
entity XTOBs_sorting is
    Generic
        ( 
    --! Integer used to distinguish different FPGAs having a slightly different firmware
    FPGA_NUMBER : integer
        ) ; 
    Port ( 
       --! L1A signal input 
        L1A_in            : in STD_LOGIC;
       --! TOB Readout FIFO reset Pulse by software command OR SYS_RST
       XTOB_FIFO_sw_rst    : in std_logic ;       
       --! array 8 x 64b words XTOBs 
       XTOB_512b_in        : in AlgoXOutput;        -- array 8 x 64b words XTOBs   
       --! 8b XTOB has valid data 
       XTOB_Valid_flg_in   : in STD_LOGIC_VECTOR (7 downto 0);     
       --! XTOB sync signal 
       XTOB_sync_in        : in STD_LOGIC;
       --! sorted XTOB BCN with delay through ALGO/sorting block
       ALGO_XTOB_BCN_in         : in std_logic_vector(6 downto 0);
       --! latency pre-load for XTOB DRPAM write address
       pre_ld_wr_addr_XTOB   : in STD_LOGIC_VECTOR (8 downto 0);
       --! number of multi-slice locations to read from DPRAM
       DPR_locations_to_rd   : in STD_LOGIC_VECTOR (2 downto 0); 
       --! read XTOB data into Shift Registers
       FIFO_rd_en            : in STD_LOGIC;                        
       --! 200Mhz input signal    
       clk_200M_in           : in STD_LOGIC;
       --! 280Mhz input signal  
       clk_280M_in           : in STD_LOGIC;
       --! Threshold to assert XTOB FIFO prog full flag
       XTOB_FIFO_pFULL_THRESH_assert : in STD_LOGIC_VECTOR (8 downto 0);
       --! Threshold to negate XTOB FIFO prog full flag
       XTOB_FIFO_pFULL_THRESH_negate : in STD_LOGIC_VECTOR (8 downto 0);
       --! XTOBs output of fifo {array 8 of [(5*34b) + 5b valid] + 7b BCN}
       FIFO_XTOB_data_out       : out array_8_of_182b;     
       --! occupancy of XTOB derandomisation FIFO
       XTOB_FIFO_data_count     : out STD_LOGIC_VECTOR (8 downto 0); 
       --! XTOBs FIFO data valid signal       
       XTOB_FIFO_valid          : out STD_LOGIC;
       --! XTOBs FIFO  full flag
       XTOB_FIFO_full           : out STD_LOGIC;
       --! XTOBs FIFO empty flag
       XTOB_FIFO_empty          : out STD_LOGIC;
       --! XTOBs FIFO prog full flag
       XTOB_FIFO_prog_full      : out STD_LOGIC
       );
    end XTOBs_sorting;

--! @copydoc XTOBs_sorting.vhd
architecture RTL of XTOBs_sorting is

--***********************************Parameter Declarations********************
    constant DLY : time := 0 ns;
--************************** Register Declarations ****************************
    
    -- XTOB signals
    signal  TOB_valid_out_i      : array_8_of_5b;   -- TOBs valid (8 * 5b) 
--    signal  SIPO_sync_vec_i      : STD_LOGIC_VECTOR (0 downto 0);                 
    signal  SIPO_TOB_out_i       : array_8_of_170b;  -- array 8 of XTOBs 5* 34b = 170b
    signal  XTOB_sync_out_i      : STD_LOGIC_VECTOR (7 downto 0);                 

    signal  DPR_XTOBs_in_i       : array_8_of_182b;  -- array 8 of XTOBs (64b) * 5 + 5b valid + XTOB_BCN (7b)
    signal  DPR_XTOBs_out_i      : array_8_of_182b;  -- array 8 of XTOBs (64b) * 5 + 5b valid + XTOB_BCN (7b)
    signal  FIFO_XTOB_data_out_i : array_8_of_182b;  -- array 8 of XTOBs (64b) * 5 + 5b valid + XTOB_BCN (7b)
    signal  XTOB_FIFO_data_count_i : array_8_of_9b;  -- array 8 of XTOBs FIFO count (9b) * 5 
    
    signal  DPR_XTOB_rd_addr_i     : STD_LOGIC_VECTOR (8 downto 0);
    signal  DPR_XTOB_wr_addr_i     : STD_LOGIC_VECTOR (8 downto 0);
    signal  DRP_XTOB_rd_en_i       : STD_LOGIC;                 
    signal  FIFO_XTOB_wr_en_i      : STD_LOGIC;                 
    signal  FIFO_XTOB_rd_en_i      : STD_LOGIC;                 
    signal  XTOB_FIFO_valid_i      : STD_LOGIC_VECTOR (7 downto 0);
    signal  XTOB_FIFO_valid_tmp    : STD_LOGIC;
    signal  XTOB_FIFO_full_i       : STD_LOGIC_VECTOR (7 downto 0);
    signal  XTOB_FIFO_full_tmp     : STD_LOGIC;
    signal  XTOB_FIFO_empty_i      : STD_LOGIC_VECTOR (7 downto 0);
    signal  XTOB_FIFO_empty_tmp    : STD_LOGIC;
    signal  XTOB_FIFO_prog_full_i  : STD_LOGIC_VECTOR (7 downto 0);
    signal  XTOB_FIFO_prog_full_tmp  : STD_LOGIC;
    signal  ALGO_XTOB_BCN_out_i    : std_logic_vector (6 downto 0); -- sorted XTOB BCN with delay through ALGO/sorting block
    signal  FIFO_wr_en_tmp         : std_logic ;
    

    signal  tied_to_vcc_i        : std_logic;

--  ####### Mark  signals  ########
--attribute mark_debug : string ;
-- attribute mark_debug of signal_name : signal is "true" ;
    attribute keep       : string ;
    attribute max_fanout : integer;
    attribute keep of       XTOB_sync_in : signal is "true" ;
    attribute max_fanout of XTOB_sync_in : signal is 20;
    attribute keep of       XTOB_FIFO_prog_full_i : signal is "true" ;
    attribute max_fanout of XTOB_FIFO_prog_full_i : signal is 20;

--   #######################################

begin

    FIFO_XTOB_rd_en_i <= FIFO_rd_en ;
    
    -- input signals

    tied_to_vcc_i   <=   '1';

    XTOB_FIFO_data_count <= XTOB_FIFO_data_count_i(0) ;
    
-- this FSM only control TOB data wr to DPRAM & FIFO
--! @details 
--! \verbatim
--! This state machine is responsible for reading 198-bit data (6 x 32-bit TOBs and 6 valid flags) from Circular DPRAM
--! and storing them into de-randomisation FIFO when the L1A signal is received.
--! It also controls multi-slice readout between 1 to 5 slices in sequence.
--! \endverbatim 
U1_TOBs_wr_FSM :  entity TOB_rdout_lib.fsm_TOB_wr_to_FIFO
    PORT map (
        CLK_IN                 => clk_200M_in ,
        TOB_FIFO_sw_rst        => XTOB_FIFO_sw_rst,    -- RST OR TOB_FIFO_sw_rst
        L1A_in                 => L1A_in ,
        -- this is sync signal which enables WR addr to increase 
        -- can be either TOB_eg_sync_in or TOB_tau_sync_in as they are identical
        SIPO_sync_in           => XTOB_sync_in ,  
        pre_ld_wr_addr         => pre_ld_wr_addr_XTOB ,        -- i/p
        DPR_locations_to_rd    => DPR_locations_to_rd ,   -- number of DRP locations to read 1 to 5
        DPR_rd_addr            => DPR_XTOB_rd_addr_i ,
        DPR_wr_addr            => DPR_XTOB_wr_addr_i ,
        DRP_rd_en              => DRP_XTOB_rd_en_i ,
        FIFO_wr_en             => FIFO_XTOB_wr_en_i
       );

       -- prevent wr to FIFO if pFULL = 1 - use 3rd to be in the middle of ALGO Blocks
       FIFO_wr_en_tmp <= FIFO_XTOB_wr_en_i AND (NOT XTOB_FIFO_prog_full_i(3)) ;

--! @details
--! \verbatim
--! The GEN_XTOB_RAM loop, generates 8 copies firmware blocks within it,these are:
--!     SIPO unit - to convert serial data from 5 x 64b words to a 320b parallel word with 5-bit valid flags.
--!     Dual Port RAM - scrolling memory to store data on every bunch crossing
--!         This is circular Dual Port RAM. It stores 325-bit words per Bunch number.
--!         The Read and Write address have the correct offset in order to read the actual XTOB data
--!         associated with the BCN at the time of arrival of L1A
--!    Derandomisation FIFO - to store the correct data word upon receiving an L1A   
--!         This is Derandomisation FIFO. It stores 325-bit words per L1A, which are read out of Circular memory.
--! \endverbatim
GEN_XTOB_RAM : for i in 0 to 7 GENERATE       

    U2_XTOBs_eg :  entity TOB_rdout_lib.SIPO_unit    -- convert 5 x array of 34b XTOBs into one 170b words for Circular DPRAMs
        Port map ( 
            -- 64b XTOBs from 1 ALGO block - 5 in series only bits 47:14 have data, rest are ZERO
            XTOB_in           =>  XTOB_512b_in(i)(47 downto 14),
            XTOB_valid_in     =>  XTOB_Valid_flg_in(i),     -- 1B valid signal - 5 in series
            XTOB_sync_in      =>  XTOB_sync_in,             -- 1b sync in - at start of valid XTOBs
            ALGO_XTOB_BCN_in  =>  ALGO_XTOB_BCN_in,   
            clk_in            =>  clk_200M_in,
            XTOB_sync_out     =>  XTOB_sync_out_i(i),      -- 1b sync out - at start of valid XTOBs
            XTOB_valid_out    =>  TOB_valid_out_i(i) ,     -- TOBs valid 5b from 1 ALGO block
            XTOB_out          =>  SIPO_TOB_out_i(i),       -- XTOBs 5 * 34b = 170b out
            ALGO_XTOB_BCN_out =>  ALGO_XTOB_BCN_out_i
            );
    
--        XTOBs (5*34b) + 5b valid + 7b XTOB_BCN
        DPR_XTOBs_in_i(i)      <= ALGO_XTOB_BCN_out_i & TOB_valid_out_i(i) & SIPO_TOB_out_i(i) ;
--        SIPO_sync_vec_i(0)     <= XTOB_sync_out_i ;   -- active once every 5 clks
    
    U3_XTOB_DRP : DPR_182b_512     -- XTOBs (5*34b) + 5b valid + 7b XTOB_BCN for 1 ALGO block into DPRAM
        PORT map (
            clka   => clk_200M_in  ,      -- runs at 280M but writes 1 in 7 clks
            ena    => tied_to_vcc_i  ,      -- 
            wea    => stdv(XTOB_sync_out_i(i)) ,    -- was SIPO_sync_vec_i -- this is wr en using input sync sig
            addra  => DPR_XTOB_wr_addr_i    ,
            dina   => DPR_XTOBs_in_i(i)  ,     -- this is XTOBs (5*34b) + 5b valid + 7b XTOB_BCN for 1 algo block 
            clkb   => clk_200M_in  ,
            enb    => DRP_XTOB_rd_en_i  ,      -- clk/rst/addr en for DPRAM port B
            addrb  => DPR_XTOB_rd_addr_i  ,
            doutb  => DPR_XTOBs_out_i(i)       -- XTOBs (5*34b) + 5b valid + 7b XTOB_BCN for 1 algo block 
            );
    
    U5_XTOBs_FIFO : FIFO_182b_512     -- XXTOBs (5*34b) + 5b valid + 7b XTOB_BCN for 1 algo block into FIFO
        PORT MAP (
            rst         => XTOB_FIFO_sw_rst, -- RST OR TOB_FIFO_sw_rst
            wr_clk      => clk_200M_in,
            rd_clk      => clk_280M_in,
            din         => DPR_XTOBs_out_i(i) , --this is XTOBs (5*34b) + 5b valid + 7b XTOB_BCN for 1 algo block 
            wr_en       => FIFO_wr_en_tmp,      -- prevent wr to FIFO if prog-FULL = 1
            rd_en       => FIFO_XTOB_rd_en_i,
            dout        => FIFO_XTOB_data_out_i(i)  ,   -- sorted XTOBs output of fifo XTOBs (5*34b) + 5b valid + 7b XTOB_BCN
            prog_full_thresh_assert     => XTOB_FIFO_pFULL_THRESH_assert ,
            prog_full_thresh_negate     => XTOB_FIFO_pFULL_THRESH_negate ,
            wr_data_count  => XTOB_FIFO_data_count_i(i) ,             -- occupancy of XTOB FIFO
            full        => XTOB_FIFO_full_i(i)      ,
            empty       => XTOB_FIFO_empty_i(i)     ,
            valid       => XTOB_FIFO_valid_i(i)     ,
            prog_full   => XTOB_FIFO_prog_full_i(i) 
            );

    END GENERATE GEN_XTOB_RAM ;


-- If Process FPGA 2 & 4, then assign the XTOB FIFO 6
-- If Process FPGA 1 & 3, then assign the XTOB FIFO 4
U6_gen : 
    if FPGA_NUMBER = 2 generate
        XTOB_FIFO_valid_tmp       <= XTOB_FIFO_valid_i(6)   ;
        XTOB_FIFO_full_tmp        <= XTOB_FIFO_full_i(6)    ;
        XTOB_FIFO_empty_tmp       <= XTOB_FIFO_empty_i(6)   ;
        XTOB_FIFO_prog_full_tmp   <= XTOB_FIFO_prog_full_i(6) ; 

    elsif FPGA_NUMBER = 4 generate
        XTOB_FIFO_valid_tmp       <= XTOB_FIFO_valid_i(4)   ;
        XTOB_FIFO_full_tmp        <= XTOB_FIFO_full_i(4)    ;
        XTOB_FIFO_empty_tmp       <= XTOB_FIFO_empty_i(4)   ;
        XTOB_FIFO_prog_full_tmp   <= XTOB_FIFO_prog_full_i(4) ;
    else generate
        XTOB_FIFO_valid_tmp       <= XTOB_FIFO_valid_i(2)   ;
        XTOB_FIFO_full_tmp        <= XTOB_FIFO_full_i(2)    ;
        XTOB_FIFO_empty_tmp       <= XTOB_FIFO_empty_i(2)   ;
        XTOB_FIFO_prog_full_tmp   <= XTOB_FIFO_prog_full_i(2) ;
     

    end generate U6_gen;



-- register output of FIFO to remove timing violations - use ALGO Block 4 to be middle of FPGA
U6_XTOB_clk_proc : Process (clk_280M_in)
begin
    if rising_edge (clk_280M_in) then
        FIFO_XTOB_data_out    <= FIFO_XTOB_data_out_i ;
        XTOB_FIFO_valid       <= XTOB_FIFO_valid_tmp   ;
        XTOB_FIFO_full        <= XTOB_FIFO_full_tmp    ;
        XTOB_FIFO_empty       <= XTOB_FIFO_empty_tmp   ;
        XTOB_FIFO_prog_full   <= XTOB_FIFO_prog_full_tmp ; 
    end if;
end process;

        
end RTL;
