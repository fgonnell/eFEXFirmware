{ head:{
   text:'Time diagram',
  tick:-2},
  signal: [
    {name: 'clk 40', wave:  'p....', period : 5, phase: 6},
 
  {name: 'data in', wave: '04222', data: "data0 data1 data2 data3", period : 5, phase : 6, node: "...."},
  {name: 'start', wave: '01...', period : 5, phase : 6, node: ".s..."},
    {},
    {name: 'clock 200', wave: 'p........................', period : 1, phase: 6},    
    {name: 'counter',      wave: '00....55550..............', data: '3 2 1 0',period : 1, phase: 6, node :"......c..0"},      
  	{name: 'write address', wave: '04222', data: "0x0 0x1 0x2 0x3", period : 5, phase : 6, node: ".aw."},
    {name: 'read address', wave: '00........4....2....2....', data: '0x0 0x1 0x2 0x3',period : 1, phase: 6, node : "..........b"},      
    {name: 'shift register', wave: '00........544442222222222', data: 'load shift shift shift shift load shift shift shift shift load shift shift shift shift ',period : 1, phase: 6, node : "..........r"},      
    
], 
    edge: [
    's~->c','0~->r', '0~->w', 'a~->b'
  ],
  config: { hscale: 2}
  }
