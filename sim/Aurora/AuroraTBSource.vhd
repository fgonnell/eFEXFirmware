-- TB_source AXI version
--
-- Dave Sankey, August 2019

Library STD;
Use std.textio.all;
LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;

LIBRARY infrastructure_lib;
use infrastructure_lib.packet_mux_type.all;

ENTITY TB_source IS
  GENERIC(
    Fabric_clk: time := 4 ns;
	NTTC: positive
  );
  PORT(
	clk					: out std_logic;
	rst_clk				: out std_logic;
	ipbclk				: out std_logic;
	rst_ipbclk			: out std_logic;
	mgtclk				: out std_logic;
	rst_mgtclk			: out std_logic;
	ttc_clk				: out std_logic;
	ttc_l1a				: out std_logic;
	ttc_info			: out std_logic_vector(43 downto 0);
-- Status signals
	eFEX_number			: out std_logic_vector(7 downto 0);
-- TTC FIFO signals
-- Format of FIFO is
-- fifo_dout(71 downto 56): starting 64 bit address of payload data in RAM
-- fifo_dout(55 downto 48): number of 64 bit words for payload data excluding trailer
-- fifo_dout(47 downto 44): local error flags excluding header mismatch and processor timeout
-- fifo_dout(43 downto 12): extended L1ID
-- fifo_dout(11 downto 0):  BCN
    ttc_rd_en			: in packet_sl_array(NTTC-1 downto 0);
    ttc_dout			: out packet_metadata_array(NTTC-1 downto 0);
    ttc_fifo_empty		: out packet_sl_array(NTTC-1 downto 0);
-- towards Aurora readout
	payload_data        : in std_logic_vector (63 DOWNTO 0) ;
	payload_valid       : in std_logic;
	payload_last        : in std_logic;
	tready_data         : out std_logic
  );
END ENTITY TB_source;

Architecture rtl of TB_source is

  signal clk_i, rst_clk_i, ttc_clk_i, mgt_clk_i, l1a_i, tready_sig: std_logic := '0';
  signal bcn_cntr: std_logic_vector(11 downto 0) := (Others => '0');
  signal l1id_cntr: std_logic_vector(31 downto 0) := (Others => '0');
  signal rndm_i: std_logic_vector(4 downto 0) := (Others => '0');

Begin

  clk <= clk_i;
  rst_clk <= rst_clk_i;
  mgtclk <= mgt_clk_i;
  ipbclk <= ttc_clk_i; -- Bit of a kludge as it's 40 MHz rather than 33, but good enough!
  ttc_clk <= ttc_clk_i;
  eFEX_number <= x"03";
  tready_data <= tready_sig;

Clock:  Process
  Begin
    rst_clk_i <= '1';
    clk_i <= '0', '1' after Fabric_clk/2;
    wait for Fabric_clk;
    clk_i <= '0', '1' after Fabric_clk/2;
    wait for Fabric_clk;
    rst_clk_i <= '0';
    Loop
      clk_i <= '0', '1' after Fabric_clk/2;
      wait for Fabric_clk;
    End loop; -- forever
  End process Clock;

MGT_Clock: Process(clk_i)
  Variable pause: std_logic := '1';
  Begin
    If rising_edge(clk_i) then
      if pause = '1' then
        mgt_clk_i <= not mgt_clk_i after Fabric_clk/2;
        rst_mgtclk <= rst_clk_i after Fabric_clk/2; -- Bit of a kludge, but good enough!
        pause := '0';
      else
        pause := '1';
      end if;
    end if;
  End process MGT_Clock;

TTC_Clock: Process(clk_i)
  Variable pause: unsigned(2 downto 0) := (others => '1');
  Begin
    If rising_edge(clk_i) then
      if pause = 7 then
        ttc_clk_i <= not ttc_clk_i after Fabric_clk/2;
        rst_ipbclk <= rst_clk_i after Fabric_clk/2; -- Bit of a kludge, but good enough!
        pause := (others => '0');
      else
        pause := pause + 1;
      end if;
    end if;
  End process TTC_Clock;

BCN: Process(ttc_clk_i)
  Variable bcn_count: unsigned(11 downto 0) := (Others => '0');
  Begin
    If rising_edge(ttc_clk_i) then
      if bcn_count = 3563 then
        bcn_count := (others => '0');
      else
        bcn_count := bcn_count + 1;
      end if;
      bcn_cntr <= std_logic_vector(bcn_count) after Fabric_clk/2;
    end if;
  End process BCN;

L1A: Process(ttc_clk_i)
  Variable counter: unsigned(4 downto 0) := (Others => '0');
  Variable L1A_interval: unsigned(4 downto 0) := "01000";
  Variable l1a_bit: std_logic := '0';
  Begin
    If rising_edge(ttc_clk_i) then
      if counter = L1A_interval then
        l1a_bit := '1';
        counter := (Others => '0');
        L1A_interval := unsigned(rndm_i);
      else
        l1a_bit := '0';
        counter := counter + 1;
      end if;
      l1a_i <= l1a_bit after Fabric_clk/2;
    end if;
  End process L1A;

L1ID: Process(ttc_clk_i)
  Variable l1id_count: unsigned(31 downto 0) := (Others => '0');
  Begin
    If rising_edge(ttc_clk_i) then
      if l1a_i = '1' then
        l1id_count := l1id_count + 1;
      end if;
      l1id_cntr <= std_logic_vector(l1id_count) after Fabric_clk/2;
    end if;
  End process L1ID;

TTC_Info_block:  Process(ttc_clk_i)
  Begin
    If rising_edge(ttc_clk_i) then
      if l1a_i = '1' then
        ttc_l1a <= '1' after Fabric_clk/2;
        ttc_info <= l1id_cntr & bcn_cntr after Fabric_clk/2;
      else
        ttc_l1a <= '0' after Fabric_clk/2;
        ttc_info <= (Others => '0') after Fabric_clk/2;
      end if;
    end if;
  End process TTC_Info_block;

Random: process(ttc_clk_i)
-- xorshift rng based on http://b2d-f9r.blogspot.co.uk/2010/08/16-bit-xorshift-rng-now-with-more.html
-- using triplet 5, 3, 1 (next 5, 7, 4)
  variable x: std_logic_vector(15 downto 0) := x"DEAD";
  variable y: std_logic_vector(15 downto 0) := x"BEEF";
  variable t : std_logic_vector(15 downto 0) := x"2019";
  begin
    if rising_edge(ttc_clk_i) then
      t := x xor (x(10 downto 0) & "00000");
	  x := y;
	  y := (y xor ("0" & y(15 downto 1))) xor (t xor ("000" & t(15 downto 3)));
      rndm_i <= y(4 downto 0) after Fabric_clk/2;
    end if;
  end process Random;

TTC_FIFO: for i in 0 to NTTC-1 generate

TTC_FIFO_process: process(ttc_clk_i, clk_i)
  Variable TTC_ram: packet_metadata_array(15 DOWNTO 0) := (Others => (Others => '0'));
  Variable fifo_count: Integer := 0;
  Variable RAM_pointer, RAM_words: unsigned(7 downto 0) := (Others => '0');
  Variable pause: unsigned(2 downto 0) := (others => '1');
  begin
    if rising_edge(clk_i) then
      if fifo_count > 0 then
        ttc_fifo_empty(i) <= '0' after Fabric_clk/2;
      else
        ttc_fifo_empty(i) <= '1' after Fabric_clk/2;
      End If;
	  if ttc_rd_en(i) = '1' and fifo_count > 0 and pause = 7 then -- kludge for MGT_Clock domain...
        ttc_dout(i) <= TTC_ram(0) after Fabric_clk/2;
	    TTC_ram(14 downto 0) := TTC_ram(15 downto 1);
	    TTC_ram(15) := (Others => '0');
        fifo_count := fifo_count - 1;
        pause := (Others => '0');
      elsif pause = 7 then -- stretch ttc_dout for MGT_Clock domain...
        ttc_dout(i) <= (Others => '0') after Fabric_clk/2;
      else
        pause := pause + 1;
      end if;
    end if;
    if rising_edge(ttc_clk_i) then
      if l1a_i = '1' and fifo_count < 16 then
        case l1id_cntr(1 downto 0) is
          when "00" =>
            RAM_words := x"10";
          when "01" =>
            RAM_words := x"01";
          when "10" =>
            RAM_words := x"0F";
          when Others =>
            RAM_words := (Others => '0');
        end case;
        TTC_ram(fifo_count) := l1id_cntr(7 downto 0) & std_logic_vector(RAM_pointer) & std_logic_vector(RAM_words) & x"0" & l1id_cntr & bcn_cntr;
        RAM_pointer := RAM_pointer + RAM_words + 1;
        fifo_count := fifo_count + 1;
      end if;
    end if;
  end process TTC_FIFO_process;

  End generate TTC_FIFO;

TReady: process(clk_i)
  Variable mode: integer range 0 to 3 := 0;
  Variable tick: std_logic := '0';
  Variable pause: unsigned(1 downto 0) := (others => '0');
  Variable high, low :std_logic;
  Begin
    If rising_edge(clk_i) then
      case mode is
        when 1 =>
          high := payload_valid;
          low := '0';
        when 2 =>
          high := '1';
          low := '0';
        when 3 =>
          high := '0';
          low := '1';
        when Others =>
          high := '1';
          low := '1';
      end case;
      if payload_last = '1' and tready_sig = '1' then
        if tick = '0' then
          tick := '1';
        elsif mode = 3 then
          tick := '0';
          mode := 0;
        else
          tick := '0';
          mode := mode + 1;
        end if;
      end if;
      if pause = 3 then
        tready_sig <= high after Fabric_clk/2;
        pause := (Others => '0');
      else
        tready_sig <= low after Fabric_clk/2;
        pause := pause + 1;
      end if;
    end if;
  End process TReady;

End Architecture rtl;
