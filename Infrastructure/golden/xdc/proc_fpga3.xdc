###### This is XDC file for Golden processing FPGA3 ###########

###### Ethernet RefClk (125MHz)
set_property PACKAGE_PIN K23 [get_ports gt_clk125_p]
set_property IOSTANDARD LVDS [get_ports  gt_clk125_p]
set_property PACKAGE_PIN K24 [get_ports gt_clk125_n]
set_property IOSTANDARD LVDS [get_ports  gt_clk125_n]
set_property DIFF_TERM TRUE [get_ports gt_clk125_p]
set_property DIFF_TERM TRUE [get_ports gt_clk125_n]

###### FPGA3_GCLK1_40M08
set_property IOSTANDARD LVDS [get_ports ttc_clk_p]
set_property IOSTANDARD LVDS [get_ports ttc_clk_n]
set_property PACKAGE_PIN J27 [get_ports ttc_clk_n]
set_property PACKAGE_PIN K27 [get_ports ttc_clk_p]
set_property DIFF_TERM TRUE [get_ports ttc_clk_p]
set_property DIFF_TERM TRUE [get_ports ttc_clk_n]

###### MAC clock
create_generated_clock -name mac_clk [get_pins clk_wiz_0/clk_gen/inst/mmcm_adv_inst/CLKOUT0]
set_clock_groups -asynchronous -group [get_clocks mac_clk]

create_generated_clock -name flash_spi_clk -source [get_pins slaves/spi_flash/spi_clk_reg/C] -divide_by 2 [get_pins slaves/spi_flash/spi_clk_reg/Q]

###### SPI Flash Pins
set_property PACKAGE_PIN BA29 [get_ports flash_csn]
set_property IOSTANDARD LVCMOS18 [get_ports flash_csn]

set_property PACKAGE_PIN BC27 [get_ports flash_mosi]
set_property IOSTANDARD LVCMOS18 [get_ports flash_mosi]

set_property PACKAGE_PIN BD27 [get_ports flash_miso]
set_property IOSTANDARD LVCMOS18 [get_ports flash_miso]

###### LEDs   #########################
set_property IOSTANDARD LVCMOS18 [get_ports flash_led]
set_property PACKAGE_PIN AP29 [get_ports flash_led]

###### F3 Geographyical address
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_geo_addr[*]}]
set_property PACKAGE_PIN T24 [get_ports  {fpga_geo_addr[0]}]
set_property PACKAGE_PIN T25 [get_ports  {fpga_geo_addr[1]}]

###### Hardware address   Pins
set_property IOSTANDARD LVCMOS18 [get_ports {hardware_addr[*]}]
set_property PACKAGE_PIN AM27 [get_ports {hardware_addr[0]}]
set_property PACKAGE_PIN AK29 [get_ports {hardware_addr[1]}]
set_property PACKAGE_PIN AR28 [get_ports {hardware_addr[2]}]
set_property PACKAGE_PIN AN28 [get_ports {hardware_addr[3]}]
set_property PACKAGE_PIN AK28 [get_ports {hardware_addr[4]}]
set_property PACKAGE_PIN AN30 [get_ports {hardware_addr[5]}]
set_property PACKAGE_PIN AM30 [get_ports {hardware_addr[6]}]
set_property PACKAGE_PIN AN29 [get_ports {hardware_addr[7]}]

###### Tx and Rx signal betweeen FPGA3 and Control FPGA ##########
set_property IOSTANDARD LVCMOS18 [get_ports {master_tx_data[*]}]

set_property PACKAGE_PIN B23 [get_ports {master_tx_data[0]}]
set_property PACKAGE_PIN C25 [get_ports {master_tx_data[1]}]
set_property PACKAGE_PIN C23 [get_ports {master_tx_data[2]}]
set_property PACKAGE_PIN A24 [get_ports {master_tx_data[3]}]
set_property PACKAGE_PIN D24 [get_ports {master_tx_data[4]}]
set_property PACKAGE_PIN E23 [get_ports {master_tx_data[5]}]
set_property PACKAGE_PIN G23 [get_ports {master_tx_data[6]}]
set_property PACKAGE_PIN F24 [get_ports {master_tx_data[7]}]
set_property PACKAGE_PIN H23 [get_ports {master_tx_data[8]}]
set_property PACKAGE_PIN G25 [get_ports {master_tx_data[9]}]

set_property IOSTANDARD LVCMOS18 [get_ports master_tx_pause]
set_property PACKAGE_PIN J24 [get_ports master_tx_pause]

set_property IOSTANDARD LVCMOS18 [get_ports {master_rx_data[*]}]

set_property PACKAGE_PIN A23 [get_ports {master_rx_data[0]}]
set_property PACKAGE_PIN B25 [get_ports {master_rx_data[1]}]
set_property PACKAGE_PIN C24 [get_ports {master_rx_data[2]}]
set_property PACKAGE_PIN A25 [get_ports {master_rx_data[3]}]
set_property PACKAGE_PIN D25 [get_ports {master_rx_data[4]}]
set_property PACKAGE_PIN E24 [get_ports {master_rx_data[5]}]
set_property PACKAGE_PIN F23 [get_ports {master_rx_data[6]}]
set_property PACKAGE_PIN F25 [get_ports {master_rx_data[7]}]
set_property PACKAGE_PIN H24 [get_ports {master_rx_data[8]}]
set_property PACKAGE_PIN G26 [get_ports {master_rx_data[9]}]

set_property IOB TRUE [get_ports {master_rx_data[*]}]
set_property IOB TRUE [get_ports {master_tx_data[*]}]
set_property IOB TRUE [get_ports master_tx_pause]
