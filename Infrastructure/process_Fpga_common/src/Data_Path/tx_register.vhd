----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.08.2018 16:06:05
-- Design Name: 
-- Module Name: tx_register - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;



entity tx_register is
 Port (
       tx_clk  : in std_logic;
       data_in : in std_logic_vector( 33 downto 0 );
       data_out : out std_logic_vector( 33 downto 0 )
       );
end tx_register;

architecture Behavioral of tx_register is

begin
      process (tx_clk)
        begin
           if tx_clk' event and tx_clk ='1' then
                 data_out<= data_in;
       end  if;
      end process;
      


end Behavioral;
