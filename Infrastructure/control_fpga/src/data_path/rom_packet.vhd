
-- First version of packet engine...
--
-- Listens for L1A in FIFO then builds a fragment to Aurora containing the contents of the playout ROM
--
-- Dave Sankey, May 2019

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rom_packet is
  generic (
	DATA_WIDTH			: integer :=   64;	-- DATA bus width
 	ROM_ADDR_WIDTH		: integer :=   5;	-- width of rom address bus
 	ROM_LENGTH			: integer :=   32;	-- number of words to read from ROM
 	FORMAT_VERSION		: std_logic_vector (2 DOWNTO 0) := "001";
 	STREAM_TYPE			: std_logic_vector (1 DOWNTO 0) := "00" -- "00" TOB, "10" Input Data, "11" Debug
 	);
  port (
	clk					: in std_logic;
	rst_clk				: in std_logic;
-- Status signals
	eFEX_number			: IN std_logic_vector(7 downto 0);
-- FIFO signals
    ttc_rd_en			: OUT STD_LOGIC;
    ttc_dout			: IN STD_LOGIC_VECTOR(71 DOWNTO 0);
    ttc_fifo_empty		: IN STD_LOGIC;
-- towards packet_builder readout
	packet_data         : OUT std_logic_vector (63 DOWNTO 0) ;
	packet_valid        : OUT std_logic;
   	packet_hdr          : OUT std_logic;
	packet_data_end     : OUT std_logic;
	packet_ready		: IN std_logic
  );
end rom_packet;

architecture rom of rom_packet is

  TYPE STATE_TYPE IS (
    idle,
    start_rom,
    wait_l1id,
    capture_l1id,
	do_hdr_crc,
	wait_hdr_crc,
    send_l1id,
    send_payload,
    send_last,
    build_trailer,
	do_trailer_crc,
	wait_trailer_crc,
    send_trailer,
    wait_end,
    wait_idle
  );

  SIGNAL Stream_ID: std_logic_vector(7 DOWNTO 0);

  signal addr_sig: std_logic_vector(10 downto 0);
  signal first_sig, valid_sig, last_sig, read_l1id_sig, ready_sig, force_ready_sig: std_logic;
  signal set_addr_sig: std_logic;
  signal data_sig: std_logic_vector(63 DOWNTO 0);
  signal state_sig: STATE_TYPE;
-- ROM signals
  signal ROM_addr: std_logic_vector(ROM_ADDR_WIDTH-1 downto 0);
  signal ROM_data: std_logic_vector(DATA_WIDTH-1 downto 0);

begin

  Stream_ID <= x"01" when (STREAM_TYPE = "00") else STREAM_TYPE & "00" & eFEX_number(3 downto 0);
  ttc_rd_en <= read_l1id_sig;
  ROM_addr <= addr_sig(ROM_ADDR_WIDTH-1 downto 0);
  packet_hdr <= first_sig;
  packet_data <= data_sig;
  packet_valid <= valid_sig;
  packet_data_end <= last_sig;
  ready_sig <= packet_ready and valid_sig;

send_data:  process(clk)
  variable data_int, next_data, next_data_buf: std_logic_vector(63 DOWNTO 0) := (others => '0');
  variable ready_buf, first_data, first_int, last_buf, last_data, last_int: std_logic := '0';
  variable payload_length: unsigned(10 downto 0) := (others => '0');
  begin
    if rising_edge(clk) then
      last_buf := '0';
      first_data := '0';
      case state_sig is
        when send_l1id =>
          next_data_buf := ttc_dout(43 downto 12) & FORMAT_VERSION & "000000000" & ttc_dout(11 downto 0) & Stream_ID;
          first_data := '1';
          payload_length := (Others => '0');
        when send_trailer =>
          next_data_buf :=  x"00000000010" & eFEX_number & std_logic_vector(payload_length) & "0";
        when send_payload =>
		  next_data_buf := ROM_data;
        when send_last =>
		  next_data_buf := ROM_data;
		  last_buf := '1';
        when Others =>
		  next_data_buf := (Others => '0');
      end case;
      if ready_buf = '1' or force_ready_sig = '1' then
        next_data := next_data_buf;
        last_data := last_buf;
      end if;
      if ready_sig = '1' or force_ready_sig = '1' then
        ready_buf := '1';
        data_int := next_data;
        first_int := first_data;
        last_int := last_data;
        if ready_sig = '1' then  -- only count payload words!
	      payload_length := payload_length + 1;
	    end if;
      else
        ready_buf := '0';
      end if;
      data_sig <= data_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      first_sig <= first_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      last_sig <= last_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process send_data;

ROM: Process(clk)
  Variable octet_high, octet_low: std_logic_vector(7 downto 0) := (Others => '0');
  Begin
    If rising_edge(clk) then
      octet_low(5 downto 1) := ROM_addr;
      octet_high(5 downto 1) := ROM_addr;
      octet_high(0) := '1';
      ROM_data <= octet_high & octet_high & octet_high & octet_high & octet_low & octet_low & octet_low & octet_low
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  End process ROM;

counter:  process(clk)
  variable addr_int, next_addr: unsigned(10 downto 0) := (others => '0');
  begin
    if rising_edge(clk) then
      if set_addr_sig = '1' then
        addr_int := (Others => '0');
      elsif ready_sig = '1' or force_ready_sig = '1' then
        addr_int := next_addr;
      end if;
      addr_sig <= std_logic_vector(addr_int)
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      next_addr := addr_int + 1;
    end if;
  end process counter;

state_machine:  process(clk)
  variable set_addr_int: std_logic := '0';
  variable valid_int, read_l1id_int, force_ready_int: std_logic := '0';
  variable next_state: STATE_TYPE := idle;
  variable end_addr_int: unsigned(10 downto 0) := (others => '0');
  begin
    if rising_edge(clk) then
      if rst_clk = '1' then
        next_state := idle;
      end if;
	  valid_int := '0';
	  force_ready_int := '0';
	  set_addr_int := '0';
	  read_l1id_int := '0';
      case state_sig is
        when start_rom =>
	 	  end_addr_int := to_unsigned(ROM_LENGTH, 11) - 1;
		  set_addr_int := '1';
		  read_l1id_int := '1';
		  next_state := wait_l1id;
        when wait_l1id =>
		  force_ready_int := '1';
		  next_state := send_l1id;
        when send_l1id =>
		  valid_int := '1';
		  next_state := send_payload;
        when send_payload =>
	      valid_int := '1';
		  if unsigned(addr_sig) = end_addr_int and ready_sig = '1' then
            next_state := send_last;
	 	  end if;
        when send_last =>
	      valid_int := '1';
	      if ready_sig = '1' then
            next_state := send_trailer;
          end if;
        when send_trailer =>
	      valid_int := '1';
	      if ready_sig = '1' then
            next_state := wait_end;
          end if;
        when wait_end =>
	      if ready_sig = '1' then
            next_state := wait_idle;
          else
            valid_int := '1';
          end if;
        when wait_idle =>
	      if packet_ready = '0' then
            next_state := idle;
          end if;
        when others =>  -- idle
		  set_addr_int := '0';
		  read_l1id_int := '0';
		  if (ttc_fifo_empty = '0') and (rst_clk = '0') then
	    	next_state := start_rom;
		  end if;
      end case;
      state_sig <= next_state
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      force_ready_sig <= force_ready_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      set_addr_sig <= set_addr_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      read_l1id_sig <= read_l1id_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      valid_sig <= valid_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process state_machine;

end rom;
