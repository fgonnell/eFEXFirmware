# eFEX Algorithm firmware

**Author:** Francesco Gonnella

## Description:
This is the feature extracting algorithm module and the TOB merging module for the eFEX board for Atlas L1Calo upgrade.
There are to top units:

*IPBusTopAlgoModule*	(For the feature extracting module)
*IPBusTopSortingModule* (For the TOB-sorting/merging module)

*IPBusTopAlgoModule* should be used in all the 4 eFEX FPGAs to produce TOBS.
*IPBusTopSortingModule* has genercis to be customised. An instance of it should be made  (with 8 inputs) to sort TOBs locally in all the 4 FPGAs. Another instance (with 4 input ports) should be used as merging module.


## Instructions:

### To create the Algorithm firmware Vivado project:

1) Define environment variable:

*VIVADO_ALGOLIB_PATH*: path to the directory containing this git repository (the path in which this README.md is).

*VIVADO_IPBUS_PATH*: path to the directory containing the IPBUS SVN repository: http://svn.cern.ch/guest/cactus/branches/tsw_ipbus/repo_reorganisation_april2015/cactusupgrades

*VIVADO_MODELSIM_PATH*: path to the Xilinx compiled library for modelsim. If you don't have a compiled library yet, set it to an empty folder, you will need to comiple Modelsime library from within Vivado to run a Modelsim simulation. To do so, enter in the Vivado Tcl console:
    
    compile_simlib -simulator questa -directory <path of compiled library results>
   
If you prefer the graphical interface, you can open the Vivado tools in GUI mode.
Select "Tools" > "Compile Simulation Libraries" to open the dialog box. Use the dialog box to compile the libraries in the sdesired path.

*VIVADO_PATH*: path to the directory containing Vivado executable, needed only if Vivado executable is not in the PATH variable.

2) Run the CreateProject.sh script in the script directory to create the Vivado project.


### To add the algorithm Library (algolib) to an existing Vivado project:

1) Define environment variables *VIVADO_ALGOLIB_PATH* and *VIVADO_IPBUS_PATH* as described above.

2) Open your Vivado project.

3) From Vivado Tcl console run the eFEX-TriggerAlgo.tcl script by entering:

    source -notrace <path to this repository>/tcl/eFEX-FirmwareAlgo.tcl
    
If you prefer, you can skip point 1) and, before point 3), define the variable from within Vivado by entering into the Tcl console:

    set ipbus_path <path to ipbus repository>
    set algolib_path <path to this repository>