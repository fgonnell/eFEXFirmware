-- This is the top level of the Slaves Ipbus  of the Control FPGA.
-- The ipbus slaves live in this entity - modify according to requirements
-- Ports can be added to give ipbus slaves access to the chip top level.

-- Mohammed Siyad October 2015

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library ipbus_lib;
use ipbus_lib.ipbus.all;
use work.spi.all;
LIBRARY infrastructure_lib;
USE infrastructure_lib.all;
use infrastructure_lib.ipbus_decode_L1CaloEfexControlGolden.all;
--use work.ipbus_decode_efex_cntrl_registers.ALL;
entity slave_cntrl_fpga is
	port(
		ipb_clk    : in std_logic;
		ipb_rst    : in std_logic;
		ipb_in     : in ipb_wbus;
		ipb_out    : out ipb_rbus;
		
	-- module id signals
	    hw_id      : in std_logic_vector(31 downto 0); 
     	hw_position: in std_logic_vector(31 downto 0);
        fpga_id    : in std_logic_vector(31 downto 0);
        fw_rev     : in std_logic_vector(31 downto 0);
        fw_tag     : in std_logic_vector(31 downto 0);
        datafmt_rev: in std_logic_vector(31 downto 0);
	--- module status signals
      	status   : in std_logic_vector(31 downto 0); 
		
	--- module control signals	
	    control_reg : out std_logic_vector(31 downto 0);
	--- xadc signals
	    
    ---	reconfigure signal
        reconfig_reg	 : out std_logic_vector(31 downto 0);
	---	i2c bus 0 signals
	    scl_i_0      : in std_logic;  
	    scl_o_0      : out std_logic;
	    scl_enb_0    : out std_logic;
		sda_o_0    : out std_logic;
		sda_i_0    : in std_logic;
		sda_enb_0    : out std_logic;

	---	pll signals
		pll_miso   : in std_logic;
        pll_le     : out std_logic;
        pll_clko   : out std_logic;
        pll_mosi   : out std_logic;
        pll_select : out std_logic_vector(1 downto 0);
	--- flash signals	
		flash_miso : in std_logic;
		--select_slv: out std_logic_vector(3 downto 0);
        flash_le   : out std_logic;
        flash_clko : out std_logic;
        flash_mosi : out std_logic

	);

end slave_cntrl_fpga;

architecture rtl of slave_cntrl_fpga is


	 
--	constant sel_width:positive := 4;
	signal ipbw: ipb_wbus_array(N_SLAVES-1 downto 0);
	signal ipbr, ipbr_d: ipb_rbus_array(N_SLAVES-1 downto 0);
	signal ctrl_pulse_reg : std_logic_vector(63 downto 0);
	signal nc_0,nc_1,nc_2 : std_logic_vector(63 downto 0);
	signal pll_spi_in,flash_spi_in :spi_mi; 
	signal pll_spi_out, flash_spi_out :spi_mo;
	signal flash_select : std_logic_vector(1 downto 0) := (others => '0');
	--signal select_slv_int: std_logic_vector(sel_width-1 downto 0);
	
Begin
	

control_reg <= ctrl_pulse_reg (31 downto 0);


--- spi pll assignment
pll_spi_in.miso <= pll_miso;
	      
pll_le   <= pll_spi_out.le;
pll_mosi <= pll_spi_out.mosi;
pll_clko <= pll_spi_out.clk;

--- spi flash pin assignment
flash_spi_in.miso <=flash_miso;

flash_mosi <= flash_spi_out.mosi;
flash_le   <= flash_spi_out.le;
flash_clko <= flash_spi_out.clk;

--select_slv <= select_slv_int;


fabric: entity ipbus_lib.ipbus_fabric_sel 
    generic map(NSLV => N_SLAVES,
	             SEL_WIDTH => ipbus_sel_width)
          port map(
             ipb_in => ipb_in,
             ipb_out => ipb_out,
             sel => ipbus_sel_L1CaloEfexControlGolden(ipb_in.ipb_addr),
             ipb_to_slaves => ipbw,
             ipb_from_slaves => ipbr
           ); 




Board_ID: entity work.module_information
        port map(
         ipb_clk   => ipb_clk,
		 reset     => ipb_rst,
		 ipbus_in  => ipbw(0),
		 ipbus_out => ipbr(0),
         HW_ID      => hw_id,
         HW_Position => hw_Position,
         FPGA_ID    => fpga_id, 
         FW_Rev     => fw_rev, 
         FW_Tag     => fw_tag,   
         DataFmt_Rev  => datafmt_rev
        );

module_status: entity work.module_status  --
                          
                     port map(
                         
                         ipbus_in  => ipbw(1),
                         ipbus_out => ipbr(1),
                         status => status
                                                   
                                    
                     );

module_control: entity work.ipbus_reg --- 
                      generic map (
                         addr_width => 1
                         
                       )
                       port map(
                               clk       => ipb_clk,
                               reset     => ipb_rst,
                               ipbus_in  => ipbw(2),
                               ipbus_out => ipbr(2),
                                q        => ctrl_pulse_reg  
                            );        

xadc:     entity work.ipbus_xadc_drp -- accessing the xadc of the FPGA
                                       
                           port map(
                              ipb_clk     => ipb_clk,
                               reset       => ipb_rst,
                               ipbus_in    => ipbw(3),
                               ipbus_out   => ipbr(3)
                                                             
                                 );
				 
				 
reconfig: entity work.ipbus_ctrlreg -- when select will reconfigure the fpga
              generic map (
		            ctrl_addr_width => 0,
		            stat_addr_width => 0
	              )
		        port map(
			        clk => ipb_clk,
			        reset => ipb_rst,
			        ipbus_in => ipbw(4),
			       ipbus_out => ipbr(4),
			       d => nc_0(31 downto 0),
			       q => reconfig_reg
		 );		 
		 
i2c_0: entity work.ipbus_i2c_master 
	    generic map (addr_width     => 3  -- slave that allow to access i2c devices that connect to i2c bus 0
	    ) 
	    port map(
		   clk     => ipb_clk,
		   rst     => ipb_rst,
		   ipb_in  => ipbw(5),
		   ipb_out => ipbr(5),
		   scl_i   => scl_i_0 ,
		   scl_o   => scl_o_0 ,
		   scl_enb => scl_enb_0 ,
		   sda_o   => sda_o_0,
		   sda_i   => sda_i_0,
		   sda_enb => sda_enb_0
	    );
	
spi_pll: entity work.ipbus_spi32 -- Slave that used to configure the PLL and read its status
        generic map(
    BYTE_SPI => FALSE,
    ADDR_WIDTH => 5
     )
    port map(
        ipbus_clk => ipb_clk,
		reset     => ipb_rst,
		ipb_in    => ipbw(6),
		ipb_out   => ipbr(6),
		spi_in    => pll_spi_in,
		spi_out   => pll_spi_out,
		selreg    => pll_select
        
      	 );
	 
spi_flash: entity work.ipbus_spi32 --acess to SPI fLSH for write/read it
	generic map(
    BYTE_SPI => TRUE,
    ADDR_WIDTH => 8
    )
    port map(
        ipbus_clk => ipb_clk,
		reset     => ipb_rst,
		ipb_in    => ipbw(7),
		ipb_out   => ipbr(7),
		spi_in    => flash_spi_in,
        spi_out   => flash_spi_out,
        selreg    => flash_select
                
	 );

RAM: entity ipbus_lib.ipbus_ram -- internal ram for testing the iPbus transections.
	generic map(
       ADDR_WIDTH => 10
    )
    port map(
        clk       => ipb_clk,
		reset     => ipb_rst,
		ipbus_in  => ipbw(8),
		ipbus_out => ipbr(8)
		                
	 );


end rtl;
