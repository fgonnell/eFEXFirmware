
-- First version of Input Data packet engine...
--
-- Listens for data in FIFO then sends a fragment to packet builder containing the specified contents from the RAM
--
-- Dave Sankey, August 2019

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity data_packet is
  generic (
 	ADDR_WIDTH  : integer :=   16;	-- width of ram address bus (maximum value is 16)
 	FORMAT_VERSION: std_logic_vector (2 DOWNTO 0) := "001"
 	);
  port (
	clk					: in std_logic;
	rst_clk				: in std_logic;
-- Status signals
	eFEX_number			: IN std_logic_vector(7 downto 0);
 	Stream_ID			: IN std_logic_vector (7 DOWNTO 0) := x"81";
-- FIFO signals
-- Format of FIFO is
-- fifo_dout(71 downto 56): starting 64 bit address of payload data in RAM
-- fifo_dout(55 downto 48): number of 64 bit words for payload data excluding trailer
-- fifo_dout(47 downto 44): local error flags excluding header mismatch and processor timeout
-- fifo_dout(43 downto 12): extended L1ID
-- fifo_dout(11 downto 0):  BCN
    fifo_rd_en			: OUT STD_LOGIC;
    fifo_dout			: IN STD_LOGIC_VECTOR(71 DOWNTO 0);
    fifo_empty			: IN STD_LOGIC;
-- RAM signals
    RAM_data			: IN std_logic_vector (63 DOWNTO 0);
    RAM_addr			: OUT std_logic_vector(ADDR_WIDTH - 1 downto 0);
    RAM_din				: OUT std_logic_vector (63 DOWNTO 0);
    RAM_we				: OUT STD_LOGIC;
-- towards packet_builder readout
	packet_data         : OUT std_logic_vector (63 DOWNTO 0);
	packet_valid        : OUT std_logic;
   	packet_hdr          : OUT std_logic;
	packet_data_end     : OUT std_logic;
	packet_ready		: IN std_logic
  );
end data_packet;

architecture ram of data_packet is

  TYPE STATE_TYPE IS (
    initial,
    idle,
	read_fifo,
    wait_fifo,
    capture_fifo,
	wait_ram,
    send_l1id,
    send_payload,
    send_last,
    send_trailer,
    wait_end,
    wait_idle
  );

  signal addr_sig, start_addr_sig: std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal first_sig, valid_sig, last_sig, read_fifo_sig, ready_sig, force_ready_sig: std_logic;
  signal set_addr_sig, WE_sig, empty_sig: std_logic;
  signal data_sig: std_logic_vector(63 DOWNTO 0);
  signal state_sig: STATE_TYPE := initial;

begin
  fifo_rd_en <= read_fifo_sig;
  RAM_addr <= addr_sig when WE_sig = '0' else (Others => '0');
  RAM_we <= WE_sig;  -- addr_sig has overrun a couple of counts when WE_sig is enabled, but close enough!
  RAM_din(63 downto ADDR_WIDTH) <= (Others => '0');
  RAM_din(ADDR_WIDTH-1 downto 0) <= addr_sig;
  packet_hdr <= first_sig;
  packet_data <= data_sig;
  packet_valid <= valid_sig;
  packet_data_end <= last_sig;
  ready_sig <= packet_ready and valid_sig;

send_data:  process(clk)
  variable data_int, next_data, next_data_buf: std_logic_vector(63 DOWNTO 0) := (others => '0');
  variable ready_buf, first_data, first_int, last_buf, last_data, last_int: std_logic := '0';
  variable error_flags: std_logic_vector(5 downto 0) := (others => '0');
  variable payload_length: unsigned(7 downto 0) := (others => '0');
  begin
    if rising_edge(clk) then
      last_buf := '0';
      first_data := '0';
      case state_sig is
        when capture_fifo =>
          next_data_buf := fifo_dout(43 downto 12) & FORMAT_VERSION & "000000000" & fifo_dout(11 downto 0) & Stream_ID;
          error_flags := fifo_dout(47 downto 44) & "00";
          payload_length := unsigned(fifo_dout(55 downto 48)); -- maximum packet size is ~60 channels of input data each 4 64-bit words...
        when wait_ram => -- preserve next_data_buf...
        when send_l1id => -- this is when force_ready_sig = '1' for logic to drive signals...
          first_data := '1';
          last_buf := empty_sig;
        when send_payload =>
		  next_data_buf := RAM_data;
        when send_last =>
		  next_data_buf := RAM_data;
		  last_buf := '1';
        when send_trailer =>
          next_data_buf := x"000000" & "00" & error_flags & x"010" & eFEX_number & "000" & std_logic_vector(payload_length) & "0";
        when Others =>
		  next_data_buf := (Others => '0');
      end case;
      if ready_buf = '1' or force_ready_sig = '1' then
        next_data := next_data_buf;
        last_data := last_buf;
      end if;
      if ready_sig = '1' or force_ready_sig = '1' then
        ready_buf := '1';
        data_int := next_data;
        first_int := first_data;
        last_int := last_data;
      else
        ready_buf := '0';
      end if;
      data_sig <= data_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      first_sig <= first_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      last_sig <= last_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process send_data;

counter:  process(clk)
  variable addr_int, next_addr: unsigned(ADDR_WIDTH-1 downto 0) := (others => '0');
  constant max_addr: unsigned(ADDR_WIDTH-1 downto 0) := (Others => '1');
  begin
    if rising_edge(clk) then
      if set_addr_sig = '1' then
        addr_int := unsigned(start_addr_sig);
      elsif ready_sig = '1' or force_ready_sig = '1' then
        addr_int := next_addr;
      end if;
      if addr_int = max_addr then -- wrap around to 1 rather than 0!
      	next_addr := (Others => '0');
      	next_addr(0) := '1';
      else
      	next_addr := addr_int + 1;
      end if;
      addr_sig <= std_logic_vector(addr_int)
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process counter;

state_machine:  process(clk)
  variable set_addr_int, WE_int, empty_int, single_int: std_logic := '0';
  variable valid_int, read_fifo_int, force_ready_int: std_logic := '0';
  variable next_state: STATE_TYPE := initial;
  variable start_addr_int, end_addr_int: unsigned(ADDR_WIDTH downto 0) := (Others => '0');
  begin
    if rising_edge(clk) then
      if rst_clk = '1' then
        next_state := initial;
      end if;
	  valid_int := '0';
	  force_ready_int := '0';
	  set_addr_int := '0';
	  read_fifo_int := '0';
	  WE_int := '0';
	  start_addr_int := (Others => '0');
      case state_sig is
        when initial =>
		  read_fifo_int := '1'; -- flush out metadata FIFO at start!
		  if rst_clk = '0' then
			next_state := idle;
		  end if;
        when read_fifo =>
		  read_fifo_int := '1';
		  next_state := wait_fifo;
        when wait_fifo =>
		  next_state := capture_fifo;
        when capture_fifo =>
	 	  start_addr_int := unsigned("0" & fifo_dout(55+ADDR_WIDTH downto 56));
	 	  end_addr_int := start_addr_int + unsigned(fifo_dout(55 downto 48));
		  set_addr_int := '1';
		  if fifo_dout(55 downto 49) = "0000000" then
		    if fifo_dout(48) = '0' then
		  	  empty_int := '1';
		  	  single_int := '0';
		  	else
		  	  empty_int := '0';
		  	  single_int := '1';
		  	end if;
		  end if;
		  next_state := wait_ram;
        when wait_ram =>
          if end_addr_int(ADDR_WIDTH) = '1' then  --correct for wraparound in RAM
        	end_addr_int(ADDR_WIDTH) := '0';
          else
        	end_addr_int := end_addr_int - 1;
          end if;
		  force_ready_int := '1';
		  next_state := send_l1id;
        when send_l1id =>
		  valid_int := '1';
		  if empty_int = '1' then
		  	next_state := send_trailer;
		  elsif single_int = '1' then
		  	next_state := send_last;
		  else
		  	next_state := send_payload;
		  end if;
        when send_payload =>
	      valid_int := '1';
		  if (unsigned('0' & addr_sig) = end_addr_int) and ready_sig = '1' then
            next_state := send_last;
	 	  end if;
        when send_last =>
	      valid_int := '1';
	      if ready_sig = '1' then
            next_state := send_trailer;
          end if;
        when send_trailer =>
	      valid_int := '1';
	      if ready_sig = '1' then
            WE_int := '1';
            next_state := wait_end;
          end if;
        when wait_end =>
	      if ready_sig = '1' then
            next_state := wait_idle;
          else
            valid_int := '1';
          end if;
        when wait_idle =>
	      if packet_ready = '0' then
            next_state := idle;
          end if;
        when others =>  -- idle
		  empty_int := '0';
		  single_int := '0';
	  	  end_addr_int := (Others => '0');
		  if fifo_empty = '0' then
	    	next_state := read_fifo;
		  end if;
      end case;
      state_sig <= next_state
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      force_ready_sig <= force_ready_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      set_addr_sig <= set_addr_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      start_addr_sig <= std_logic_vector(start_addr_int(ADDR_WIDTH - 1 downto 0))
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      read_fifo_sig <= read_fifo_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      empty_sig <= empty_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      valid_sig <= valid_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      WE_sig <= WE_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process state_machine;

end ram;
