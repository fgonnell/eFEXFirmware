--! @file
--! @brief Top of fsm_TOB_wr_to_FIFO for process FPGA
--! @details 
--! \verbatim
--! This state machine is responsible for reading TOBs/XTObs and valid flags from Circular DPRAM
--! and storing them into de-randomisation FIFO when the L1A signal is received.
--! 
--! It also controls multi-slice readout between 1 to 5 slices in sequence.
--! \endverbatim 
--! 
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;
use TOB_rdout_lib.data_type_pkg.all;

--! @copydoc fsm_TOB_wr_to_FIFO.vhd
entity fsm_TOB_wr_to_FIFO is
    Port ( 
       --! Clock input signal  
       CLK_IN                : in STD_LOGIC;
       --! TOB Readout FIFO reset Pulse by software command
       TOB_FIFO_sw_rst          : in std_logic ;       
       --! L1A signal input 
       L1A_in                : in STD_LOGIC;
       --! SIPO TOBs synch signal input 
       SIPO_sync_in          : in STD_LOGIC;
       --! latency pre-load for DRPAM write address
       pre_ld_wr_addr        : in STD_LOGIC_VECTOR (8 downto 0);
       --! number of multi-slice locations to read from DPRAM
       DPR_locations_to_rd   : in STD_LOGIC_VECTOR (2 downto 0); 
       --! DPRAM read address
       DPR_rd_addr           : out STD_LOGIC_VECTOR (8 downto 0);
       --! DPRAM write address
       DPR_wr_addr           : out STD_LOGIC_VECTOR (8 downto 0);
       --! DPRAM read enable
       DRP_rd_en             : out STD_LOGIC ;
       --! enable sorted TOBs write into derandomisatiion FIFO
       FIFO_wr_en            : out STD_LOGIC
       );  
end fsm_TOB_wr_to_FIFO;

--! @copydoc fsm_TOB_wr_to_FIFO.vhd
architecture Behavioral of fsm_TOB_wr_to_FIFO is

    signal  SIPO_sync_in_i          : std_logic ;
    signal  FIFO_wr_en_i            : std_logic := '0' ;
    signal  FIFO_wr_en_tmp          : std_logic := '0' ;
    signal  DRP_rd_en_i             : std_logic ;
    signal  DPR_rd_addr_i           : STD_LOGIC_VECTOR (8 downto 0);
    signal  DPR_rd_addr_tmp         : unsigned (8 downto 0);
    signal  DPR_wr_addr_i           : STD_LOGIC_VECTOR (8 downto 0);
--    signal  pre_ld_wr_addr_i        : STD_LOGIC_VECTOR (8 downto 0);
    signal  slice_count_i           : unsigned (2 downto 0);

--  ####### Mark signals  ########
    attribute keep       : string ;
    attribute max_fanout : integer;
    attribute keep of       DPR_wr_addr_i : signal is "true" ;
    attribute max_fanout of DPR_wr_addr_i : signal is 40;
    attribute keep of       DPR_rd_addr_i : signal is "true" ;
    attribute max_fanout of DPR_rd_addr_i : signal is 40;
    attribute keep of       FIFO_wr_en_i  : signal is "true" ;
    attribute max_fanout of FIFO_wr_en_i  : signal is 40;
    attribute keep of       DRP_rd_en_i   : signal is "true" ;
    attribute max_fanout of DRP_rd_en_i   : signal is 40;

--   #######################################
 
    TYPE STATE_TYPE IS (
       idle,
       ser_1,
       ser_2,
       ser_3,
       ser_4
       ) ;
  
    SIGNAL current_state : STATE_TYPE;
    signal count : integer range 0 to 7;

begin
    -- input ports
    SIPO_sync_in_i       <= SIPO_sync_in ;
    slice_count_i        <= unsigned (DPR_locations_to_rd) ;
    
    -- output ports
    DRP_rd_en       <= DRP_rd_en_i ;
    DPR_rd_addr     <= STD_LOGIC_VECTOR (DPR_rd_addr_tmp) ;
    FIFO_wr_en      <= FIFO_wr_en_tmp ;
    DPR_wr_addr     <= DPR_wr_addr_i ;
    FIFO_wr_en_tmp  <= FIFO_wr_en_i ; 

--! @details
--! \verbatim
--! This process generates the Circular DPRAM Write address by adding the pre-load offset with Circular DPRAM Read address.
--! It operates with the 280 MHz clock but increments once every 7 clock to read TOBs.
--! It operates with the 200 MHz clock but increments once every 5 clock to read XTOBs.
--! \endverbatim 
U0_clk_proc : process (CLK_IN)
    begin
        if CLK_IN'event and CLK_IN = '1' then
            -- write address = read_addr + offset
            DPR_wr_addr_i     <= std_logic_vector ( unsigned(pre_ld_wr_addr) + unsigned(DPR_rd_addr_i) )  ;

--            FIFO_wr_en_tmp <= FIFO_wr_en_i ;    -- 1 clk delay so data is available at ouput of DPRAM
        end if;
    end process;

--! @details
--! \verbatim
--! This counter generates the Circular DPRAM Read address.
--! It increments once when SIPO_sync_in signal is active.
--! \endverbatim 
U2_rd_addr : entity TOB_rdout_lib.cntr_ram_addr_9b
    Port map (                                        
    -- as words are placed together in parallel,
    -- only one increment is needed every 1 start bit (40MHz)                                       
       CE       => SIPO_sync_in_i   ,
       CLK      => CLK_IN ,
       RST      => TOB_FIFO_sw_rst ,    -- RST OR TOB_FIFO_sw_rst
       Q        => DPR_rd_addr_i 
       );                                         

-- 
--! @details
--! \verbatim
--! This FSM transfers TOB data from DPRAM into de-randomising FIFO (buffer).
--! \endverbatim 
U3_rd_XTOB_fsm : process (CLK_IN)
         
    begin
        if CLK_IN'event and CLK_IN = '1' then
            if ( TOB_FIFO_sw_rst = '1' )then -- RST OR TOB_FIFO_sw_rst
                current_state   <= idle ;
                FIFO_wr_en_i    <= '0' ;
                DRP_rd_en_i     <= '0' ;
                count <= 0 ;
                DPR_rd_addr_tmp <= (others => '0');
            else    
                CASE current_state is
                    when idle => 
                        if L1A_in = '1' then   -- on L1A
                            DPR_rd_addr_tmp <= unsigned(DPR_rd_addr_i);
                            DRP_rd_en_i     <= '1' ;        -- read 1st location
                            FIFO_wr_en_i    <= '0';         -- delay by 1 clk so data is on o/p reg of DPRAM
                            current_state   <= ser_1 ;
                            count <= count + 1 ; 
                        else
                            current_state <= idle ;
                        end if;
                    when ser_1 =>
                        -- if  slice_count_i = 1 then     -- default is at least one location, max is 5
                        if  slice_count_i = count then     -- default is at least one location, max is 5
                            current_state <= ser_4 ;    
                            DRP_rd_en_i     <= '1' ;    -- extra enable cycle for reg at o/p of DPRAM
                            FIFO_wr_en_i    <= '1';     -- wr enable into FIFO
                            count <= count + 1 ;
                        else
                            current_state <= ser_1 ;    -- was ser_2
                            DPR_rd_addr_tmp <= DPR_rd_addr_tmp + 1 ;
                            DRP_rd_en_i     <= '1' ;        -- read next location
                            FIFO_wr_en_i    <= '1';
                            count <= count + 1 ;
                        end if;

                    when ser_4 => 
                        if count = 6 then 
                            current_state <= idle ;
                            DRP_rd_en_i   <= '0' ;
                            FIFO_wr_en_i  <= '0' ;
                            count <= 0 ;
                        else
                            current_state <= ser_4;
                            DRP_rd_en_i   <= '0' ;
                            FIFO_wr_en_i  <= '0' ;
                            count <= count + 1 ;
                        end if;
                    when others =>
                        NULL;
                end case;
            end if;
        END IF;                
    end process;

end Behavioral;

