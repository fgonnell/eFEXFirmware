library IEEE;
use IEEE.STD_LOGIC_1164.all;

package mgt_type is 


--- rxresetdone
type mgt_rxresetdone_path is 
     record 
          gt0_rxresetdone           : std_logic;
          gt1_rxresetdone           : std_logic;
          gt2_rxresetdone           : std_logic;
          gt3_rxresetdone           : std_logic;  
    end record;
type  mgt_rxresetdone_array is
       array (natural range <> ) of mgt_rxresetdone_path;

---- txresetdone
type mgt_txresetdone_path is 
     record 
          gt0_txresetdone           : std_logic;
          gt1_txresetdone           : std_logic;
          gt2_txresetdone           : std_logic;
          gt3_txresetdone           : std_logic;  
    end record;
type  mgt_txresetdone_array is
       array (natural range <> ) of mgt_txresetdone_path;
       
       
-----txbufstatus record
type mgt_txbufstatus_path is 
     record 
          gt0_txbufstatus           : std_logic_vector(1 downto 0);
          gt1_txbufstatus           : std_logic_vector(1 downto 0);
          gt2_txbufstatus           : std_logic_vector(1 downto 0);
          gt3_txbufstatus           : std_logic_vector(1 downto 0);
    end record;
type  mgt_txbufstatus_array is
       array (natural range <> ) of mgt_txbufstatus_path;
       
 ---tx power down record     
 type mgt_txpd_path is 
      record 
           gt0_txpd         : std_logic_vector(1 downto 0);
           gt1_txpd         : std_logic_vector(1 downto 0);
           gt2_txpd         : std_logic_vector(1 downto 0);
           gt3_txpd         : std_logic_vector(1 downto 0);
     end record;
 type  mgt_txpd_array is
        array (natural range <> ) of mgt_txpd_path;
 
---rx power down record     
type mgt_rxpd_path is 
     record 
           gt0_rxpd         : std_logic_vector(1 downto 0);
           gt1_rxpd         : std_logic_vector(1 downto 0);
           gt2_rxpd         : std_logic_vector(1 downto 0);
           gt3_rxpd         : std_logic_vector(1 downto 0);
      end record;
type  mgt_rxpd_array is
       array (natural range <> ) of mgt_rxpd_path;
         






------txchrisk record

type mgt_txcharisk_path is 
     record 
          gt0_txcharisk      : std_logic_vector(3 downto 0);
          gt1_txcharisk      : std_logic_vector(3 downto 0);
          gt2_txcharisk      : std_logic_vector(3 downto 0);
          gt3_txcharisk      : std_logic_vector(3 downto 0);  
    end record;

constant ZERO_MGT_TXCHARISK : mgt_txcharisk_path := (
  gt0_txcharisk =>  (others =>  '0'),
  gt1_txcharisk =>  (others =>  '0'),
  gt2_txcharisk =>  (others =>  '0'),
  gt3_txcharisk =>  (others =>  '0')
  );


type  mgt_txcharisk_array is
       array (natural range <> ) of mgt_txcharisk_path;




--- txdata record 
type mgt_txdata is
           
              record  
                  gt0_txdata_in                         : std_logic_vector(31 downto 0);
                  gt1_txdata_in                         : std_logic_vector(31 downto 0); 
                  gt2_txdata_in                         : std_logic_vector(31 downto 0);
                  gt3_txdata_in                         : std_logic_vector(31 downto 0);                                            
              end record;  
                  
constant ZERO_MGT_TXDATA : mgt_txdata := (
  gt0_txdata_in =>  (others =>  '0'),
  gt1_txdata_in =>  (others =>  '0'),
  gt2_txdata_in =>  (others =>  '0'),
  gt3_txdata_in =>  (others =>  '0')
  );

type mgt_txdata_array is 
              array (natural range <>) of mgt_txdata;  





---- gt_rxcommadet
  
  type mgt_rxcommadet_path is 
     record 
          gt0_rxcommadet           : std_logic;
          gt1_rxcommadet           : std_logic;
          gt2_rxcommadet           : std_logic;
          gt3_rxcommadet           : std_logic;  
    end record;
type  mgt_rxcommadet_array is
       array (natural range <> ) of mgt_rxcommadet_path;
       
       
---- rxbyteisaligned record
 type mgt_rxbyteisaligned_path is 
     record 
          gt0_rxbyteisaligned          : std_logic;
          gt1_rxbyteisaligned          : std_logic;
          gt2_rxbyteisaligned          : std_logic;
          gt3_rxbyteisaligned          : std_logic;  
    end record;
type  mgt_rxbyteisaligned_array is
       array (natural range <> ) of mgt_rxbyteisaligned_path;    
       
---- rxbyterealign record
        type mgt_rxbyterealign_path is 
            record 
                 gt0_rxbyterealign          : std_logic;
                 gt1_rxbyterealign          : std_logic;
                 gt2_rxbyterealign          : std_logic;
                 gt3_rxbyterealign          : std_logic;  
           end record;
       type  mgt_rxbyterealign_array is
              array (natural range <> ) of mgt_rxbyterealign_path;           
         
       
  
---loopback record

type mgt_loopback_path is 
     record 
          gt0_loopback_in                         : std_logic_vector(2 downto 0);
         -- gt1_loopback_in                         : std_logic_vector(2 downto 0);
         -- gt2_loopback_in                         : std_logic_vector(2 downto 0);
         -- gt3_loopback_in                         : std_logic_vector(2 downto 0);  
    end record;
type  mgt_loopback_array is
       array (natural range <> ) of mgt_loopback_path;
       
--------------- rxccharisk record

type mgt_rxcharisk_path is 
         record
           gt0_rxcharisk_out                       : std_logic_vector(3 downto 0);
           gt1_rxcharisk_out                       : std_logic_vector(3 downto 0);
           gt2_rxcharisk_out                       : std_logic_vector(3 downto 0);
           gt3_rxcharisk_out                       : std_logic_vector(3 downto 0);
            
     end record ;


type mgt_rxchris_out_array is 
     array (natural range <> ) of mgt_rxcharisk_path; 
     
type mgt_rxcharisk_array is           
                         array (natural range <>) of mgt_rxcharisk_path;
                         

-------------------rxchriscomma record
type mgt_rxchariskcomm_path is 
         record
           gt0_rxchariscomma_out                   : std_logic_vector(3 downto 0);
           gt1_rxchariscomma_out                   : std_logic_vector(3 downto 0);
           gt2_rxchariscomma_out                   : std_logic_vector(3 downto 0);
           gt3_rxchariscomma_out                   : std_logic_vector(3 downto 0);
           
            
     end record ;


type mgt_rxchariscomm_out_array is 
     array (natural range <> ) of mgt_rxchariskcomm_path; 
     
type mgt_rxchariskcomm_array is           
                             array (natural range <>) of mgt_rxchariskcomm_path;
                             
                             
 --- rxnotintable record                            
                             
  type mgt_rxnotintable_path is 
          record
            gt0_rxnotintable                   : std_logic_vector(3 downto 0);
            gt1_rxnotintable                   : std_logic_vector(3 downto 0);
            gt2_rxnotintable                   : std_logic_vector(3 downto 0);
            gt3_rxnotintable                   : std_logic_vector(3 downto 0);
            
             
      end record ;
 
 
 type mgt_rxnotintable_out_array is 
      array (natural range <> ) of mgt_rxnotintable_path; 
      
 type mgt_rxnotintable_array is           
                              array (natural range <>) of mgt_rxnotintable_path;                           
                             
                             
--- rxnotintable record                            
                                                           
 type mgt_rxdisperr_path is 
         record
           gt0_rxdisperr                 : std_logic_vector(3 downto 0);
           gt1_rxdisperr                 : std_logic_vector(3 downto 0);
           gt2_rxdisperr                 : std_logic_vector(3 downto 0);
           gt3_rxdisperr                 : std_logic_vector(3 downto 0);                                         
                                           
          end record ;
                               
                               
type mgt_rxdisperr_out_array is 
                               array (natural range <> ) of mgt_rxdisperr_path; 
                                    
  type mgt_rxdisperr_array is           
                             array (natural range <>) of mgt_rxdisperr_path;                               
                             


----------- mgt_tx_record 
        
      
type mgt_tx_data is
           
              record 
                  TXP_OUT             :   std_logic_vector(3 downto 0); 
                  TXN_OUT             :   std_logic_vector(3 downto 0);                           
              end record;  
                  
type mgt_tx_array is 
                  array (natural range <>) of mgt_tx_data;   


---------mgt_rx_record

type mgt_rx_data is
           
              record  
                  RXP_IN              :   std_logic_vector(3 downto 0);
                  RXN_IN              :   std_logic_vector(3 downto 0);                                           
              end record;  
                  
type mgt_rx_array is 
              array (natural range <>) of mgt_rx_data;   

--------------- mgt_rxdataout record 

type mgt_rxdata_out is
           
              record  
                  gt0_rxdata_out                          : std_logic_vector(31 downto 0);
                  gt1_rxdata_out                          : std_logic_vector(31 downto 0); 
                  gt2_rxdata_out                          : std_logic_vector(31 downto 0);
                  gt3_rxdata_out                          : std_logic_vector(31 downto 0);                                            
              end record;  

constant ZERO_MGT_RXDATA : mgt_rxdata_out := (
  gt0_rxdata_out =>  (others =>  '0'),
  gt1_rxdata_out =>  (others =>  '0'),
  gt2_rxdata_out =>  (others =>  '0'),
  gt3_rxdata_out =>  (others =>  '0')
  );


                  
type mgt_rxdata_array is 
              array (natural range <>) of mgt_rxdata_out;  
-------------------------------------------------------------              


--- synch data in

type synch_datain is
           
              record  
                  data_in                            : std_logic_vector(31 downto 0);
                                                             
              end record;  
                  
type synch_datain_array is 
              array (natural range <>) of synch_datain; 


--type mgt_qplllock is
           
--              record  
--                 GT0_QPLLLOCK_OUT                         : std_logic;
                                                            
--              end record;  
                  
--type mgt_QPLLLOCK_array is 
--              array (natural range <>) of mgt_QPLLLOCK;  
                        
-------------------------------
--type mgt_QPLLLOST is
           
--              record  
--                 GT0_QPLLREFCLKLOST_OUT                        : std_logic;
                                                            
--              end record;  
                  
--type mgt_QPLLREFCLKLOST_array is 
--              array (natural range <>) of mgt_QPLLLOST;  

end mgt_type ;
