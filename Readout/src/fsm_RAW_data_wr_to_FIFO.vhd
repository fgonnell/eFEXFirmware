----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/18/2017 02:10:27 PM
-- Design Name: 
-- Module Name: fsm_RAW_data_wr_to_FIFO - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fsm_RAW_data_wr_to_FIFO is
    Port ( 
       CLK_200M              : in STD_LOGIC;
       CLK_40M               : in STD_LOGIC;
       RST                   : in STD_LOGIC;
       L1A_in                : in STD_LOGIC;
       SIPO_sync_in          : in STD_LOGIC;
       pre_ld_wr_adrr        : in STD_LOGIC_VECTOR (8 downto 0);
       DPR_rd_addr           : out STD_LOGIC_VECTOR (8 downto 0);
       DPR_wr_addr           : out STD_LOGIC_VECTOR (8 downto 0);
       DRP_rd_en             : out STD_LOGIC ;
       FIFO_wr_en            : out STD_LOGIC       -- en wr into FIFO
       );  end fsm_RAW_data_wr_to_FIFO;

architecture Behavioral of fsm_RAW_data_wr_to_FIFO is

    signal  CLK_200M_i              : std_logic ;
    signal  CLK_40M_i               : std_logic ;
    signal  RST_i                   : std_logic ;
    signal  L1A_in_i                : std_logic ;
    signal  SIPO_sync_in_i          : std_logic ;
    signal  FIFO_wr_en_i            : std_logic := '0' ;
    signal  FIFO_wr_en_tmp          : std_logic := '0' ;
    signal  DRP_rd_en_i             : std_logic ;
    signal  DPR_rd_addr_i           : STD_LOGIC_VECTOR (8 downto 0);
    signal  DPR_rd_addr_tmp         : unsigned (8 downto 0);
    signal  DPR_wr_addr_i           : STD_LOGIC_VECTOR (8 downto 0);
    signal  pre_ld_wr_adrr_i        : STD_LOGIC_VECTOR (8 downto 0);
    signal  data_count               : unsigned (1 downto 0);
 
    TYPE STATE_TYPE IS (
       idle,
       ser_1,
       ser_2,
       ser_3,
       ser_4
       );
  
    SIGNAL current_state : STATE_TYPE;    signal count : integer range 0 to 5;
    
begin

    CLK_200M_i           <= CLK_200M ;
    CLK_40M_i            <= CLK_40M ;
    RST_i                <= RST ;
    L1A_in_i             <= L1A_in ;
    SIPO_sync_in_i       <= SIPO_sync_in ;
    pre_ld_wr_adrr_i        <= pre_ld_wr_adrr ;    
    
    DRP_rd_en       <= DRP_rd_en_i ;
    DPR_rd_addr     <= STD_LOGIC_VECTOR (DPR_rd_addr_tmp) ;
    FIFO_wr_en      <= FIFO_wr_en_tmp ;
    DPR_wr_addr     <= DPR_wr_addr_i ;

U0_clk_proc : process (CLK_200M_i)
    begin
        if CLK_200M_i'event and CLK_200M_i = '1' then
            FIFO_wr_en_tmp <= FIFO_wr_en_i ;    -- 1 clk delay so data is available at ouput od DPRAM
        end if;
    end process;
    
U1_wr_addr : entity work.cntr_ld_9b
    Port map (                                        
       CE       => SIPO_sync_in_i   , -- this was '1'
       CLK      => CLK_200M_i ,
       RST      => RST_i ,
       LD       => RST_i ,      -- this needs to be fixed to work with load signal
       LD_VAL   => pre_ld_wr_adrr_i,
       Q        => DPR_wr_addr_i 
       );                                         

U2_rd_addr : entity work.cntr_ld_9b
    Port map (                                        
       CE       => SIPO_sync_in_i   ,
       CLK      => CLK_200M_i ,
       RST      => RST_i ,
       LD       => RST_i ,      -- this needs to be fixed to work with load signal
       LD_VAL   => "000000000",
       Q        => DPR_rd_addr_i 
       );                                         

U3_rd_fsm : process (CLK_200M_i)
         
    begin
        if CLK_200M_i'event and CLK_200M_i = '1' then
            if RST_i = '1' then
                FIFO_wr_en_i    <= '0' ;
                DRP_rd_en_i     <= '0' ;
                DPR_rd_addr_tmp <= (OTHERS => '0') ;
                current_state <= idle ;
            else    
                CASE current_state is
                    when idle => 
                        FIFO_wr_en_i    <= '0' ;
                        DRP_rd_en_i     <= '0' ;
                        count <= 0 ;
                        DPR_rd_addr_tmp <= unsigned(DPR_rd_addr_i);
                        if L1A_in_i = '1' then   -- on L1A
                            DPR_rd_addr_tmp <= unsigned(DPR_rd_addr_i);
                            DRP_rd_en_i     <= '1' ;
                            FIFO_wr_en_i    <= '1';
                            count <= count + 1 ; 
                            current_state <= ser_1 ;
                        else
                            current_state <= idle ;
                        end if;
                    when ser_1 =>
--                        DPR_rd_addr_tmp <= unsigned(DPR_rd_addr_i); address already loaded in idle
--                        DPR_rd_addr_tmp <= DPR_rd_addr_tmp + 1 ;
                        if  data_count = 1 then     -- default is at least one location, max is 3
                            current_state <= ser_3 ;
                            count <= count + 1 ;
                            DRP_rd_en_i     <= '0' ;
                            FIFO_wr_en_i    <= '0';
                        else
                            current_state <= ser_2 ;
                            DPR_rd_addr_tmp <= DPR_rd_addr_tmp + 1 ;
                            DRP_rd_en_i     <= '1' ;
                            FIFO_wr_en_i    <= '1';
                            count <= count + 1 ; 
                        end if;
                    when ser_2 => 
                        if  data_count = 2 then     -- default is at least one location, max is 3
                            current_state <= ser_3 ;
                            count <= count + 1 ;
                            DRP_rd_en_i     <= '0' ;
                            FIFO_wr_en_i    <= '0';
                        else
                            current_state <= ser_3 ;
                            DPR_rd_addr_tmp <= DPR_rd_addr_tmp + 1 ;
                            DRP_rd_en_i     <= '1' ;
                            FIFO_wr_en_i    <= '1';
                            count <= count + 1 ; 
                        end if;
                    when ser_3 => 
                        if count = 4 then 
                            current_state <= idle ;
                            DRP_rd_en_i   <= '0' ;
                            FIFO_wr_en_i  <= '0' ;
                            count <= 0 ;
                        else
                            current_state <= ser_3;
                            DRP_rd_en_i   <= '0' ;
                            FIFO_wr_en_i  <= '0' ;
                            count <= count + 1 ;
                        end if;
                    when others =>
                        NULL;
                end case;
            end if;
        END IF;                
        
    end process;


end Behavioral;
