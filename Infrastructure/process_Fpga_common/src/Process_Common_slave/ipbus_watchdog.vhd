
-- Ipbus Lock bit with Timer. Provides for indivisable access for IPBus slaves
-- when used with Lock + Watchdog Timer aware software

-- R.Staley, February 2018

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;
use ipbus_lib.ipbus_reg_types.all;

-- TIMEBASE and DEFAULT_TIMEOUT defined here. 
-- Implementation and simulation use separate files with different values
use work.watchdog.all;

entity ipbus_watchdog is
	generic(
  TIMER_WIDTH: natural := 10
  -- = Width in bits of watchdog timer
    );    
	port(
		ipbus_clk: in STD_LOGIC;
		reset: in STD_LOGIC;
		ipbus_in: in ipb_wbus;
		ipbus_out: out ipb_rbus
	);
	
end ipbus_watchdog;

architecture rtl of ipbus_watchdog is

   signal addr: natural range 0 to 7;
   signal ack: std_logic := '0';

   signal timeout_value: std_logic_vector(TIMER_WIDTH-1 downto 0) := std_logic_vector(to_unsigned(DEFAULT_TIMEOUT,TIMER_WIDTH));
   constant timer_padding: std_logic_vector(31 downto TIMER_WIDTH) := (others => '0');
    
   signal access_key:   unsigned(29 downto 0) := (others => '0');
   signal lock, previous_lock: std_logic := '0';
   signal poslock, neglock: std_logic := '0';

   signal tick1ms: std_logic;

   signal wr_timer, wr_error: std_logic := '0';
   signal timer_running, previous_timer_running: std_logic := '0';
   signal timeout, t_error:std_logic := '0';

begin

-------------------------------------------------------------------------------
-- IPBus Control register
-- Addr(0) = Arbitration register = 30 bit Key & Lock & Timeout_error
-- Addr(1) = Programmable timeout delay in ms ticks. Default is 1 second.
--
-- Timer starts when Lock goes True ie '0' -> '1'. Timer refreshed when Timeout value (re)written.
-- Watchdog = '1' indicates the timer is running.
-- A timeout clears the Lock bit , sets T_error and sets the timeout delay register back to a default value.
-- The Key value is incremented whenever Lock goes False ie '1' -> '0'.


   addr <= to_integer(unsigned(ipbus_in.ipb_addr(2 downto 0)));

transitions: process(ipbus_clk, lock, previous_lock)
       begin
           if rising_edge(ipbus_clk) then
               previous_lock <= lock;
               poslock <= lock and not previous_lock; -- lock being applied
               neglock <= not lock and previous_lock; -- lock being removed
               previous_timer_running <= timer_running;
               timeout <= not timer_running and previous_timer_running; -- a single pulse on timer reaching zero
           end if;
       end process;
   
key_op:	process(ipbus_clk, access_key, lock, previous_lock)
       begin
           if rising_edge(ipbus_clk) then
               if reset = '1' then 
                   access_key <= (others => '0');
               elsif neglock = '1' then 
                   access_key <= access_key + 1; 
               end if;
           end if;
       end process;

-- Control register
writeop:	process(ipbus_clk,addr,ipbus_in.ipb_wdata,ipbus_in.ipb_strobe,ipbus_in.ipb_write,ack)
              begin
                  if rising_edge(ipbus_clk) then
                      if ( reset = '1' or timeout = '1' ) then 
                          timeout_value <= std_logic_vector(to_unsigned(DEFAULT_TIMEOUT,TIMER_WIDTH));
                          lock <= '0';
                      elsif (ipbus_in.ipb_strobe='1' and ipbus_in.ipb_write = '1') then
                          if addr = 0 then lock             <= ipbus_in.ipb_wdata(1); end if;
                          if addr = 1 then timeout_value    <= ipbus_in.ipb_wdata(TIMER_WIDTH-1 downto 0); end if;
                      end if;
                      ack <= ipbus_in.ipb_strobe and not ack;
                  end if;
              end process;

timeout_flag: process(ipbus_clk,timeout,lock,ipbus_in.ipb_wdata(0))
                begin
                  if rising_edge(ipbus_clk) then
                      if timeout = '1'  then 
                            t_error <= lock;-- a timeout with lock is an error
                      elsif wr_error = '1' then
                            t_error <= ipbus_in.ipb_wdata(0);-- clear error if needbe
                      end if;
                  end if;
       end process;



pulseop:	process(ipbus_clk,addr,ipbus_in.ipb_wdata,ipbus_in.ipb_strobe,ipbus_in.ipb_write)
	begin
		if rising_edge(ipbus_clk) then
				wr_timer <= '0'; wr_error <= '0';
			     if (ipbus_in.ipb_strobe='1' and ipbus_in.ipb_write = '1' and  addr = 0) then
                    wr_error <= '1';
                end if;
			     if (ipbus_in.ipb_strobe='1' and ipbus_in.ipb_write = '1' and  addr = 1) then
                    wr_timer <= '1';
                end if;
		end if;
	end process;

 	ipbus_out.ipb_ack <= ack;
	ipbus_out.ipb_err <= '0';

-- Status register
	ipbus_out.ipb_rdata <=  std_logic_vector(access_key) & lock & t_error      when addr = 0 else
	                        timer_padding & timeout_value                      when addr = 1 else
                            x"00000000";


-- generate 1ms clock
ticker: process(reset, ipbus_clk)
	variable cnt : natural range 0 to 32767 := 0;
	begin
        if rising_edge(ipbus_clk) then -- 32ns clock
            if cnt < TIMEBASE then	    
                cnt:= cnt+1;
                tick1ms <= '0';
            else
                cnt:= 0;
                tick1ms <= '1';
            end if;
	    end if;
	end process;


watchdog_timer:	process(ipbus_clk, tick1ms)
	variable timer : natural := 0;
	begin
		if rising_edge(ipbus_clk) then
			if (wr_timer = '1' or poslock = '1' ) then 
				timer := to_integer(unsigned(timeout_value));
                timer_running <= '1';
			else -- count down to zero and stop
                if tick1ms = '1' then
                    if timer = 0 then
                        timer := 0;
                        timer_running <= '0';
                    else
                        timer := timer - 1;
                        timer_running <= '1';
                    end if;
			    end if;
            end if;
		end if;
	end process;

end rtl;
