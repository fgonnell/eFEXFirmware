-- First version of mapping function from geographic location to IP address...
--
-- Dave Sankey, May 2019

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity unique_address is
  port (
    mac_clk         : in std_logic;
    rst_macclk      : in std_logic;
    hardware_addr   : in std_logic_vector (11 DOWNTO 0);
    serial_number	: in std_logic_vector (5 DOWNTO 0) := (others => '0');
    mac_addr        : out std_logic_vector(47 DOWNTO 0);
    ip_addr         : out std_logic_vector(31 DOWNTO 0);
    enable          : out std_logic
  );
end unique_address;

architecture efex of unique_address is
begin

parse_address:  process(mac_clk)
  type eFEX_mapping_array is array(15 downto 0) of std_logic_vector(3 downto 0);
  constant eFEX_mapping: eFEX_mapping_array := (x"C", x"B", x"0", x"A", x"1", x"9", x"2", x"8", x"3", x"7", x"4", x"6", x"5", x"C", x"C", x"C"); -- Logical Slot Address to eFEX number
  variable enable_int: std_logic := '0';
  variable mac_addr_int: std_logic_vector(47 downto 0) := (others => '0');
  variable ip_addr_int: std_logic_vector(31 downto 0) := (others => '0');
  variable offset: unsigned(5 downto 0) := (others => '0');
  variable efex_number: std_logic_vector(5 downto 0);
  begin
    if rising_edge(mac_clk) then
      if (rst_macclk = '1') then
        enable_int := '0';
      else
        enable_int := '1';
        efex_number := "00" & eFEX_mapping(to_integer(unsigned(hardware_addr(3 downto 0))));
        case hardware_addr(11 downto 8) is
          when x"3" => -- STF4
            mac_addr_int := X"80D336003C0" & efex_number(3 downto 0);
            offset := unsigned(efex_number) + 23;
            ip_addr_int := X"0A0B1E" & "00" & std_logic_vector(offset);
          when x"C" => -- RAL
            mac_addr_int := X"80D336003C1" & efex_number(3 downto 0);
            ip_addr_int := X"C0A80C0" & efex_number(3 downto 0);
        when Others =>
          mac_addr_int := X"80D336003C" & "11" & serial_number;
          ip_addr_int := X"C0A80F" & "00" & serial_number;
        end case;
      end if;
      enable <= enable_int
-- pragma translate_off
      after 4 ns
-- pragma translate_on
      ;
      mac_addr <= mac_addr_int
-- pragma translate_off
      after 4 ns
-- pragma translate_on
      ;
      ip_addr <= ip_addr_int
-- pragma translate_off
      after 4 ns
-- pragma translate_on
      ;
    end if;
  end process parse_address;

end efex;
