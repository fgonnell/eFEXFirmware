--! @file
--! @brief 32b register, read and write cable
--! @details 
--! \verbatim
--! This is 32b register, which is read and write cable through the IPBus.
--! \endverbatim
--! @author Saeed Taghavi

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library ipbus_lib;
use ieee.numeric_std.all;
use ipbus_lib.ipbus.all;

--! @copydoc ipbus_reg_rd_wr.vhd
entity ipbus_reg_rd_wr is
	generic(addr_width: natural := 0);
	port(
	    --! IPBus Clock input
		ipb_clk : in std_logic;
		--! IPBus Reset input
		ipb_rst : in std_logic;
		--! IPBus input bus going from master to slaves
		ipbus_in: in ipb_wbus;
		--! IPBus output bus going from slaves to master
		ipbus_out: out ipb_rbus;
		--! 32-bit output of the register which can be read/written via IPBus 
		q: out STD_LOGIC_VECTOR(2**addr_width*32-1 downto 0)
	);
	
end ipbus_reg_rd_wr;

--! @copydoc ipbus_reg_rd_wr.vhd
architecture rtl of ipbus_reg_rd_wr is

	type reg_array is array(2**addr_width-1 downto 0) of std_logic_vector(31 downto 0);
	signal reg: reg_array;
	signal sel: integer range 0 to 2 ** ADDR_WIDTH - 1 := 0;
	signal ack: std_logic;

begin

	sel <= to_integer(unsigned(ipbus_in.ipb_addr(addr_width - 1 downto 0))) when addr_width > 0 else 0;

	process(ipb_clk)
	begin
		if rising_edge(ipb_clk) then
			if ipb_rst = '1' then
				reg <= (others=>(others=>'0'));
			else
			     if ipbus_in.ipb_strobe='1' and ipbus_in.ipb_write='1' then
				    reg(sel) <= ipbus_in.ipb_wdata;
			     end if;
            end if;
			ipbus_out.ipb_rdata <= reg(sel);
			ack <= ipbus_in.ipb_strobe and not ack;

		end if;
	end process;
	
	ipbus_out.ipb_ack <= ack;
	ipbus_out.ipb_err <= '0';
	
	q <= reg(sel);

end rtl;
