--! @file
--! @brief eFEX Fibre mapping module
--! @details
--! This is eFEX Fibre mapping module
--! @author Ian Brawn
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;


library algolib;
use algolib.AlgoDataTypes.all;
use algolib.DataTypes.all;

library ipbus_lib;
use ipbus_lib.ipbus.all;
library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.all;
use TOB_rdout_lib.data_type_pkg.all;

library work;
use work.synch_type.all;
use work.EfexDataFormats.all;
use work.mgt_type.all;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.all;

--! @copydoc data_path_block.vhd
entity data_path_block is
  generic(n_channels  : natural := 64;
          FPGA_NUMBER : integer );
  port (
    clk200          : in  std_logic;
    rx_clk280       : in  std_logic_vector(n_channels-1 downto 0);
    clk280          : in  std_logic;
    reset           : in  std_logic;
    ttc_clk         : in  std_logic;
    in_load         : in  std_logic;  --! clk 40MHz, 20% duty cycle,  -36 deg pahse
    --IPBus connection
    ipb_clk         : in  std_logic;
    ipb_rst         : in  std_logic;
    --- Algo ipbus connection
    ipb_in_algo     : in  ipb_wbus;
    ipb_out_algo    : out ipb_rbus;
    -- Sorting ipbus connection
    ipb_in_sorting  : in  ipb_wbus;
    ipb_out_sorting : out ipb_rbus;
    sel_bcn_or_bc_cnt    : in std_logic;
    pseudo_orbit    : out std_logic;
    BC_Reg_sel      : in  std_logic_vector(255 downto 0);   -- 16-b BC MUX select for 16 Quads
    mux_sel         : in  std_logic_vector(255 downto 0);   -- 16-b 1st stage MUX select for 16 Quads
    enable_mgt      : in  std_logic_vector(n_channels -1 downto 0);
    rx_resetdone    : in  std_logic_vector(n_channels -1 downto 0);
    --bc alignment            
    start_pulse_rst : out std_logic;
    bc_cntr_0       : out std_logic_vector(111 downto 0);
    bc_cntr_1       : out std_logic_vector(111 downto 0);
    bc_cntr_2       : out std_logic_vector(111 downto 0);
    bc_cntr_3       : out std_logic_vector(111 downto 0);
    bc_mux_cntr_0   : out std_logic_vector(111 downto 0);
    bc_mux_cntr_1   : out std_logic_vector(111 downto 0);
    bc_mux_cntr_2   : out std_logic_vector(111 downto 0);
    bc_mux_cntr_3   : out std_logic_vector(111 downto 0);
    bcn_synch       : out std_logic_vector(63 downto 0);
    crc_error_chan  : out std_logic_vector(63 downto 0);

    RAW_data     : out RAW_data_227_type;
    -- out ports eg
    OUT_eg_Valid : out std_logic_vector(OUTPUT_TOBS-1 downto 0);
    OUT_eg_Sync  : out std_logic;

    OUT_sorted_eg_TOB   : out AlgoTriggerObject;
    OUT_sorted_eg_Sync  : out std_logic;
    OUT_sorted_eg_Valid : out std_logic;

    -- out ports tau
    OUT_tau_Valid : out std_logic_vector(OUTPUT_TOBS-1 downto 0);
    OUT_tau_Sync  : out std_logic;
    OUT_eg_XTOB   : out AlgoXOutput;
    OUT_tau_XTOB  : out AlgoXOutput;

    OUT_XTOB_BCID : out std_logic_vector(6 downto 0);

    OUT_sorted_tau_TOB   : out AlgoTriggerObject;
    OUT_sorted_tau_Sync  : out std_logic;
    OUT_sorted_tau_Valid : out std_logic;
    OUT_TOB_BCID         : out std_logic_vector(6 downto 0);
    --- debug signals for synch
    delay_latch          : out std_logic_vector(n_channels-1 downto 0);  -- for debuging 
    delay_num            : out std_logic_vector(255 downto 0);  -- for debuging 
    Reg224_latch         : out std_logic_vector(n_channels-1 downto 0);  -- for debuging 
    ttc_pipe             : out std_logic_vector(n_channels-1 downto 0);  -- for debuging 
    data_readout_0       : out std_logic_vector(223 downto 0);
    data_readout_1       : out std_logic_vector(223 downto 0);
    data_readout_2       : out std_logic_vector(223 downto 0);
    data_readout_3       : out std_logic_vector(223 downto 0);
    start                : in  std_logic;
    --playback  
    kchar                : in  std_logic_vector (63 downto 0);
    sel_data_in          : in  std_logic;
    ram_data_mgt0        : in  std_logic_vector (511 downto 0);
    ram_data_mgt1        : in  std_logic_vector (511 downto 0);
    ram_data_mgt2        : in  std_logic_vector (511 downto 0);
    ram_data_mgt3        : in  std_logic_vector (511 downto 0);
    -- MGT signals
    MGT_Commadet         : in std_logic_vector(n_channels-1 downto 0);
    MGT_Data             : in mgt_rxdata_array (15 downto 0);
   -- error_handling
    disperr_error        : in std_logic_vector(n_channels-1 downto 0);    
    notable_error        : in std_logic_vector(n_channels-1 downto 0)
     
    );

end data_path_block;

--! @copydoc data_path_block.vhd
architecture Behavioral of data_path_block is
  signal algo_in : AlgoInput;

  signal eg_Sync, eg_XSync     : std_logic;
  signal tau_Sync, tau_XSync   : std_logic;
  signal eg_Valid, eg_XValid   : std_logic_vector(OUTPUT_TOBS-1 downto 0);
  signal tau_Valid, tau_XValid : std_logic_vector(OUTPUT_TOBS-1 downto 0);

  signal eg_TOB  : AlgoOutput;
  signal tau_TOB : AlgoOutput;

  signal eg_XTOB      : AlgoXOutput;
  signal tau_XTOB     : AlgoXOutput;
  signal eFEXPosition : std_logic_vector(31 downto 0);  --! Geographic position of eFEX Module, for the mapping logic

  signal mgt_data_int, ram_data_int                 : mgt_data_in;
  signal BCID_int                                   : BCID_array;
  signal sorted_TOB_BCID_i, XTOB_BCID_i, TOB_BCID_i : BCID_t;
  signal BCID_int_ila                               : std_logic_vector(31 downto 0);
  signal TOB_BCID_ila                               : std_logic_vector(31 downto 0);
  signal XTOB_BCID_ila                              : std_logic_vector(31 downto 0);

--  ####### Mark signals  ########
  attribute keep                      : string;
  attribute max_fanout                : integer;
  attribute keep of sel_data_in       : signal is "true";
  attribute max_fanout of sel_data_in : signal is 30;
--   #######################################

begin

-----------------------------------------------------------------
-- data alignment block 
------------------------------------------------------------------
  --- data from the rx of the mgt

  mgt_data_int <= (00 => MGT_Data(00).gt0_rxdata_out, 01 => MGT_Data(00).gt1_rxdata_out, 02 => MGT_Data(00).gt2_rxdata_out, 03 => MGT_Data(00).gt3_rxdata_out,
                   04 => MGT_Data(01).gt0_rxdata_out, 05 => MGT_Data(01).gt1_rxdata_out, 06 => MGT_Data(01).gt2_rxdata_out, 07 => MGT_Data(01).gt3_rxdata_out,
                   08 => MGT_Data(02).gt0_rxdata_out, 09 => MGT_Data(02).gt1_rxdata_out, 10 => MGT_Data(02).gt2_rxdata_out, 11 => MGT_Data(02).gt3_rxdata_out,
                   12 => MGT_Data(03).gt0_rxdata_out, 13 => MGT_Data(03).gt1_rxdata_out, 14 => MGT_Data(03).gt2_rxdata_out, 15 => MGT_Data(03).gt3_rxdata_out,
                   16 => MGT_Data(04).gt0_rxdata_out, 17 => MGT_Data(04).gt1_rxdata_out, 18 => MGT_Data(04).gt2_rxdata_out, 19 => MGT_Data(04).gt3_rxdata_out,
                   20 => MGT_Data(05).gt0_rxdata_out, 21 => MGT_Data(05).gt1_rxdata_out, 22 => MGT_Data(05).gt2_rxdata_out, 23 => MGT_Data(05).gt3_rxdata_out,
                   24 => MGT_Data(06).gt0_rxdata_out, 25 => MGT_Data(06).gt1_rxdata_out, 26 => MGT_Data(06).gt2_rxdata_out, 27 => MGT_Data(06).gt3_rxdata_out,
                   28 => MGT_Data(07).gt0_rxdata_out, 29 => MGT_Data(07).gt1_rxdata_out, 30 => MGT_Data(07).gt2_rxdata_out, 31 => MGT_Data(07).gt3_rxdata_out,
                   32 => MGT_Data(08).gt0_rxdata_out, 33 => MGT_Data(08).gt1_rxdata_out, 34 => MGT_Data(08).gt2_rxdata_out, 35 => MGT_Data(08).gt3_rxdata_out,
                   36 => MGT_Data(09).gt0_rxdata_out, 37 => MGT_Data(09).gt1_rxdata_out, 38 => MGT_Data(09).gt2_rxdata_out, 39 => MGT_Data(09).gt3_rxdata_out,
                   40 => MGT_Data(10).gt0_rxdata_out, 41 => MGT_Data(10).gt1_rxdata_out, 42 => MGT_Data(10).gt2_rxdata_out, 43 => MGT_Data(10).gt3_rxdata_out,
                   44 => MGT_Data(11).gt0_rxdata_out, 45 => MGT_Data(11).gt1_rxdata_out, 46 => MGT_Data(11).gt2_rxdata_out, 47 => MGT_Data(11).gt3_rxdata_out,
                   48 => MGT_Data(12).gt0_rxdata_out, 49 => MGT_Data(12).gt1_rxdata_out, 50 => MGT_Data(12).gt2_rxdata_out, 51 => MGT_Data(12).gt3_rxdata_out,
                   52 => MGT_Data(13).gt0_rxdata_out, 53 => MGT_Data(13).gt1_rxdata_out, 54 => MGT_Data(13).gt2_rxdata_out, 55 => MGT_Data(13).gt3_rxdata_out,
                   56 => MGT_Data(14).gt0_rxdata_out, 57 => MGT_Data(14).gt1_rxdata_out, 58 => MGT_Data(14).gt2_rxdata_out, 59 => MGT_Data(14).gt3_rxdata_out,
                   60 => MGT_Data(15).gt0_rxdata_out, 61 => MGT_Data(15).gt1_rxdata_out, 62 => MGT_Data(15).gt2_rxdata_out, 63 => MGT_Data(15).gt3_rxdata_out);

  --- data from the playback ram                  
  ram_data_int <= (00 => ram_data_mgt0(31 downto 0), 01 => ram_data_mgt1(31 downto 0), 02 => ram_data_mgt2(31 downto 0), 03 => ram_data_mgt3(31 downto 0),
                   04 => ram_data_mgt0(63 downto 32), 05 => ram_data_mgt1(63 downto 32), 06 => ram_data_mgt2(63 downto 32), 07 => ram_data_mgt3(63 downto 32),
                   08 => ram_data_mgt0(95 downto 64), 09 => ram_data_mgt1(95 downto 64), 10 => ram_data_mgt2(95 downto 64), 11 => ram_data_mgt3(95 downto 64),
                   12 => ram_data_mgt0(127 downto 96), 13 => ram_data_mgt1(127 downto 96), 14 => ram_data_mgt2(127 downto 96), 15 => ram_data_mgt3(127 downto 96),
                   16 => ram_data_mgt0(159 downto 128), 17 => ram_data_mgt1(159 downto 128), 18 => ram_data_mgt2(159 downto 128), 19 => ram_data_mgt3(159 downto 128),
                   20 => ram_data_mgt0(191 downto 160), 21 => ram_data_mgt1(191 downto 160), 22 => ram_data_mgt2(191 downto 160), 23 => ram_data_mgt3(191 downto 160),
                   24 => ram_data_mgt0(223 downto 192), 25 => ram_data_mgt1(223 downto 192), 26 => ram_data_mgt2(223 downto 192), 27 => ram_data_mgt3(223 downto 192),
                   28 => ram_data_mgt0(255 downto 224), 29 => ram_data_mgt1(255 downto 224), 30 => ram_data_mgt2(255 downto 224), 31 => ram_data_mgt3(255 downto 224),
                   32 => ram_data_mgt0(287 downto 256), 33 => ram_data_mgt1(287 downto 256), 34 => ram_data_mgt2(287 downto 256), 35 => ram_data_mgt3(287 downto 256),
                   36 => ram_data_mgt0(319 downto 288), 37 => ram_data_mgt1(319 downto 288), 38 => ram_data_mgt2(319 downto 288), 39 => ram_data_mgt3(319 downto 288),
                   40 => ram_data_mgt0(351 downto 320), 41 => ram_data_mgt1(351 downto 320), 42 => ram_data_mgt2(351 downto 320), 43 => ram_data_mgt3(351 downto 320),
                   44 => ram_data_mgt0(383 downto 352), 45 => ram_data_mgt1(383 downto 352), 46 => ram_data_mgt2(383 downto 352), 47 => ram_data_mgt3(383 downto 352),
                   48 => ram_data_mgt0(415 downto 384), 49 => ram_data_mgt1(415 downto 384), 50 => ram_data_mgt2(415 downto 384), 51 => ram_data_mgt3(415 downto 384),
                   52 => ram_data_mgt0(447 downto 416), 53 => ram_data_mgt1(447 downto 416), 54 => ram_data_mgt2(447 downto 416), 55 => ram_data_mgt3(447 downto 416),
                   56 => ram_data_mgt0(479 downto 448), 57 => ram_data_mgt1(479 downto 448), 58 => ram_data_mgt2(479 downto 448), 59 => ram_data_mgt3(479 downto 448),
                   60 => ram_data_mgt0(511 downto 480), 61 => ram_data_mgt1(511 downto 480), 62 => ram_data_mgt2(511 downto 480), 63 => ram_data_mgt3(511 downto 480));











  data_alignment_block : entity work.data_alignment
    generic map(FPGA_NUMBER => FPGA_NUMBER)
    port map(
      TTC_clk         => TTC_clk,       -- 40MHz clock
      clk280          => clk280,        -- generated 280MHz clock
      rx_clk280       => rx_clk280,     -- rx_clks
      MGT_Commadet    => MGT_Commadet,
      data_readout_0  => data_readout_0,
      data_readout_1  => data_readout_1,
      data_readout_2  => data_readout_2,
      data_readout_3  => data_readout_3,
      pseudo_orbit    => pseudo_orbit,
      start           => start,
      start_pulse_rst => start_pulse_rst,
      rx_resetdone    => rx_resetdone,
      reset           => reset,
      bc_cntr_0       => bc_cntr_0,
      bc_cntr_1       => bc_cntr_1,
      bc_cntr_2       => bc_cntr_2,
      bc_cntr_3       => bc_cntr_3,
      bc_mux_cntr_0   => bc_mux_cntr_0,
      bc_mux_cntr_1   => bc_mux_cntr_1,
      bc_mux_cntr_2   => bc_mux_cntr_2,
      bc_mux_cntr_3   => bc_mux_cntr_3,
      bcn_synch       => bcn_synch,
      crc_error_chan  => crc_error_chan,
      delay_latch     => delay_latch,   --for debuging
      delay_num       => delay_num,     -- for debuging
      Reg224_latch    => Reg224_latch,  -- for debuging
      ttc_pipe        => ttc_pipe,      -- for debuging
      enable_mgt      => enable_mgt,
      BC_Reg_sel      => BC_Reg_sel,
      mux_sel         => mux_sel,
      sel_bcn_or_bc_cnt => sel_bcn_or_bc_cnt,
      BCID            => BCID_int,
      DataToAlg       => Algo_in,
      RAW_data        => RAW_data,      -- raw data of 49 x 227  
      eFEXPosition    => eFEXPosition,
      sel_data_in     => sel_data_in,
      kchar           => kchar,
      ram_data        => ram_data_int,
      MGT_Data        => mgt_data_int,
      disperr_error   => disperr_error,
      notable_error   => notable_error
      
      );

--------------------------------------------------------------
-- algorithm block
--------------------------------------------------------------
  algorithm_block : entity algolib.IPBusTopAlgoModule
    generic map(FPGA => FPGA_NUMBER)
    port map(
      CLk200  => CLk200,
      CLK280  => CLk280,
      In_load => IN_Load,
      ipb_clk => ipb_clk,
      ipb_rst => ipb_rst,
      ipb_in  => ipb_in_algo,
      ipb_out => ipb_out_algo,

      --algorithm data
      IN_BCID => BCID_int,
      IN_Data => algo_in,

      -- out general ports
      OUT_eFEXPosition => eFEXPosition,
      OUT_BCID_TOB     => TOB_BCID_i,

      --out ports eg
      OUT_eg_Sync   => eg_Sync,
      OUT_eg_Valid  => eg_Valid,
      OUT_eg_TOB    => eg_TOB,
      -- out ports tau
      OUT_tau_Sync  => tau_Sync,
      OUT_tau_Valid => tau_Valid,
      OUT_tau_TOB   => tau_TOB,

      OUT_eg_Valid_XTOB  => eg_XValid,
      OUT_eg_Sync_XTOB   => eg_XSync,
      OUT_tau_Valid_XTOB => tau_XValid,
      OUT_tau_Sync_XTOB  => tau_XSync,
      OUT_BCID_XTOB      => XTOB_BCID_i,
      OUT_eg_XTOB        => eg_XTOB,
      OUT_tau_XTOB       => tau_XTOB
      );

  OUT_eg_Sync  <= eg_XSync;
  OUT_eg_Valid <= eg_XValid;
  OUT_eg_XTOB  <= eg_XTOB;

  OUT_tau_Sync  <= tau_XSync;
  OUT_tau_Valid <= tau_XValid;
  OUT_tau_XTOB  <= tau_XTOB;


  Sorting_Module : entity algolib.IPBusTopSortingModule
    port map (
      CLK => CLk280,

      IN_eg_Data  => eg_TOB,
      IN_tau_Data => tau_TOB,
      IN_Sync     => eg_Sync,
      IN_BCID     => TOB_BCID_i,

      ipb_clk => ipb_clk,
      ipb_rst => ipb_rst,
      ipb_in  => ipb_in_sorting,
      ipb_out => ipb_out_sorting,

      OUT_BCID     => sorted_TOB_BCID_i,
      OUT_eg_Sync  => OUT_sorted_eg_Sync,
      OUT_eg_Valid => OUT_sorted_eg_Valid,
      OUT_eg_TOB   => OUT_sorted_eg_TOB,

      OUT_tau_Sync  => OUT_sorted_tau_Sync,
      OUT_tau_Valid => OUT_sorted_tau_Valid,
      OUT_tau_TOB   => OUT_sorted_tau_TOB);


  OUT_XTOB_BCID <= XTOB_BCID_i;
  OUT_TOB_BCID  <= sorted_TOB_BCID_i;

  BCID_int_ila  <= X"000000" & '0' & BCID_int(0);
  TOB_BCID_ila  <= X"000000" & '0' & Sorted_TOB_BCID_i;
  XTOB_BCID_ila <= X"000000" & '0' & XTOB_BCID_i;

--U1_TOB_Delay_ila : ila_ipbus_fabric_rd_wr
--PORT MAP (
--      clk    =>  CLk280,     
--      probe0 =>  BCID_int_ila,       -- 32b    
--      probe1 =>  OUT_TOB_BCID_ila,  -- 32b 
--      probe2 =>  (others => '0'),   -- 1b
--      probe3 =>  (others => '0'),           -- 1b
--      probe4 =>  (others => '0') ,   -- 1b
--      probe5 =>  OUT_XTOB_BCID_ila,  -- 32b 
--      probe6 =>  (others => '0'),        -- 32b
--      probe7 =>   (others => '0'),             -- 1b
--      probe8 =>   (others => '0') ,       -- 1b
--    probe9 =>   (others => '0')              -- 1b
--);  


end Behavioral;
