----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/06/2017 03:57:42 PM
-- Design Name: 
-- Module Name: mgt_rx_gen_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--library mgt_lib;
--use mgt_lib.mgt_type.all;
library work;
use work.mgt_type.all;

entity mgt_rx_gen_top is 
 generic( num_quad_rx: natural := 12   ); 
 
   Port ( 
   TTC_CLK                     : IN    std_logic;
   MGT_CLK_GTREFCLK_PAD_N_IN   : IN    std_logic_vector(num_quad_rx-1 downto 0);                 
   MGT_CLK_GTREFCLK_PAD_P_IN   : IN    std_logic_vector(num_quad_rx-1 downto 0);
   mgt_RXUSRCLK_OUT            : out   std_logic_vector(4*num_quad_rx -1 downto 0);
   mgt_SOFT_RESET_RX_IN        : in    std_logic_vector(num_quad_rx-1 downto 0);
   -- data                    
   RXN_IN                      : IN   mgt_rx_array(num_quad_rx-1 downto 0);
   RXP_IN                      : IN   mgt_rx_array(num_quad_rx-1 downto 0);
   rxdata_quad_array           : out  mgt_rxdata_array (num_quad_rx-1 downto 0); 
     -- status and monitoring  
   mgt_DATA_VALID_IN           : in   std_logic_vector(4*num_quad_rx-1 downto 0);   
   mgt_RX_FSM_RESET_DONE       : out  std_logic_vector(4*num_quad_rx-1 downto 0);
   rxbyteisaligned_quad_array  : out  mgt_rxbyteisaligned_array(num_quad_rx-1 downto 0);  
   rxresetdone_quad_array      : out  mgt_rxresetdone_array   (num_quad_rx-1 downto 0);
   
   --loopback_quad_array         : in   mgt_loopback_array(num_quad_rx-1  downto 0); 
   rxchariscomma_quad_array    : out  mgt_rxchariskcomm_array (num_quad_rx-1 downto 0);
   rxcharisk_quad_array        : out  mgt_rxcharisk_array (num_quad_rx-1 downto 0);
   
   rxbyterealign_quad_array    : out  mgt_rxbyterealign_array (num_quad_rx -1 downto 0);
   rxcommadet_quad_array       : out  mgt_rxcommadet_array    (num_quad_rx -1 downto 0);
   rxdisperr_quad_array        : out  mgt_rxdisperr_array     (num_quad_rx -1 downto 0);
   rxnotintable_quad_array     : out  mgt_rxnotintable_array  (num_quad_rx -1 downto 0);
   mgt_QPLLREFCLKLOST_OUT      : out  std_logic_vector (num_quad_rx-1 downto 0) ;
   mgt_QPLLLOCK_OUT            : out  std_logic_vector(num_quad_rx-1 downto 0)
   
   );
end mgt_rx_gen_top;

architecture Behavioral of mgt_rx_gen_top is



signal loopback_quad_array : mgt_loopback_array ( num_quad_rx -1 downto 0);



       


begin




MGT_GEN: for i in 0 to num_quad_rx-1
-- This part will generate 12 quads, 4*12 = 48 mgts. 
generate

mgt_quad_Rx:  entity work.min_latency_1quad_11g2_Rx_wrapper

 
                                    
   Port map ( 
   
       
       SOFT_RESET_RX_IN                       => mgt_SOFT_RESET_RX_IN(i),
       RXN_IN                                 => RXN_IN(i).RXN_IN ,       
       RXP_IN                                 => RXP_IN(i).RXP_IN,       
       Q3_CLK0_GTREFCLK_PAD_N_IN              => MGT_CLK_GTREFCLK_PAD_N_IN(i) ,
       Q3_CLK0_GTREFCLK_PAD_P_IN              => MGT_CLK_GTREFCLK_PAD_P_IN(i) ,        
       GT0_RX_FSM_RESET_DONE_OUT              => mgt_RX_FSM_RESET_DONE(i+3*i) ,        
       GT1_RX_FSM_RESET_DONE_OUT              => mgt_RX_FSM_RESET_DONE(i+1+3*i),       
       GT2_RX_FSM_RESET_DONE_OUT              => mgt_RX_FSM_RESET_DONE(i+2+3*i),       
       GT3_RX_FSM_RESET_DONE_OUT              => mgt_RX_FSM_RESET_DONE(i+3+3*i),      
       GT0_RXUSRCLK_OUT                       => MGT_RXUSRCLK_OUT(i+3*i),        
       GT1_RXUSRCLK_OUT                       => MGT_RXUSRCLK_OUT(i+1+3*i) ,      
       GT2_RXUSRCLK_OUT                       => MGT_RXUSRCLK_OUT(i+2+3*i),         
       GT3_RXUSRCLK_OUT                       => MGT_RXUSRCLK_OUT(i+3+3*i),  
   
       --_________________________________________________________________________
       --GT0  (X0Y0)
       --____________________________CHANNEL PORTS________________________________
       
       gt0_loopback_in                         => loopback_quad_array(i).gt0_loopback_in,   --mgt_loopback_in (i+3*i).gt0_loopback_in,      
       gt0_rxdata_out                          => rxdata_quad_array (i).gt0_rxdata_out,
       gt0_rxdisperr_out                       => rxdisperr_quad_array(i).gt0_rxdisperr,        
       gt0_rxnotintable_out                    => rxnotintable_quad_array(i).gt0_rxnotintable,  
       gt0_rxbyterealign_out                   => rxbyterealign_quad_array(i).gt0_rxbyterealign,
       gt0_rxcommadet_out                      => rxcommadet_quad_array(i).gt0_rxcommadet,      
       
       gt0_rxbyteisaligned_out                 => rxbyteisaligned_quad_array(i).gt0_rxbyteisaligned,        
       gt0_rxchariscomma_out                   => rxchariscomma_quad_array(i).gt0_rxchariscomma_out, 
       gt0_rxcharisk_out                       => rxcharisk_quad_array(i).gt0_rxcharisk_out,        
       gt0_rxresetdone_out                     => rxresetdone_quad_array(i).gt0_rxresetdone,   
          
       --GT1  (X0Y1)
       --____________________________CHANNEL PORTS________________________________
       gt1_loopback_in                         => loopback_quad_array(i).gt0_loopback_in,          
       gt1_rxdata_out                          => rxdata_quad_array (i).gt1_rxdata_out,
       gt1_rxdisperr_out                       => rxdisperr_quad_array(i).gt1_rxdisperr,        
       gt1_rxnotintable_out                    => rxnotintable_quad_array(i).gt1_rxnotintable,  
       gt1_rxbyterealign_out                   => rxbyterealign_quad_array(i).gt1_rxbyterealign,
       gt1_rxcommadet_out                      => rxcommadet_quad_array(i).gt1_rxcommadet,    
             
       gt1_rxbyteisaligned_out                 => rxbyteisaligned_quad_array(i).gt1_rxbyteisaligned,                           
       gt1_rxchariscomma_out                   => rxchariscomma_quad_array(i).gt1_rxchariscomma_out,
       gt1_rxcharisk_out                       => rxcharisk_quad_array(i).gt1_rxcharisk_out,         
       gt1_rxresetdone_out                     => rxresetdone_quad_array(i).gt1_rxresetdone,                        
                
       --GT2  (X0Y2)
       --____________________________CHANNEL PORTS________________________________
       gt2_loopback_in                         => loopback_quad_array(i).gt0_loopback_in,         
       gt2_rxdata_out                          => rxdata_quad_array (i).gt2_rxdata_out,       
       gt2_rxdisperr_out                       => rxdisperr_quad_array(i).gt2_rxdisperr,    
       gt2_rxnotintable_out                    => rxnotintable_quad_array(i).gt2_rxnotintable, 
       gt2_rxbyterealign_out                   => rxbyterealign_quad_array(i).gt2_rxbyterealign,
       gt2_rxcommadet_out                      => rxcommadet_quad_array(i).gt2_rxcommadet,     
       
       gt2_rxbyteisaligned_out                 => rxbyteisaligned_quad_array(i).gt2_rxbyteisaligned,                             
       gt2_rxchariscomma_out                   => rxchariscomma_quad_array(i).gt2_rxchariscomma_out,
       gt2_rxcharisk_out                       => rxcharisk_quad_array(i).gt2_rxcharisk_out,         
       gt2_rxresetdone_out                     =>rxresetdone_quad_array(i).gt2_rxresetdone,                    
             
   
       --GT3  (X0Y3)
       --____________________________CHANNEL PORTS________________________________
       gt3_loopback_in                         => loopback_quad_array(i).gt0_loopback_in,         
       gt3_rxdata_out                          => rxdata_quad_array (i).gt3_rxdata_out,
       gt3_rxdisperr_out                       => rxdisperr_quad_array(i).gt3_rxdisperr,    
       gt3_rxnotintable_out                    => rxnotintable_quad_array(i).gt3_rxnotintable, 
       gt3_rxbyterealign_out                   => rxbyterealign_quad_array(i).gt3_rxbyterealign,
       gt3_rxcommadet_out                      => rxcommadet_quad_array(i).gt3_rxcommadet,  
       
       gt3_rxbyteisaligned_out                 => rxbyteisaligned_quad_array(i).gt3_rxbyteisaligned,                           
       gt3_rxchariscomma_out                   => rxchariscomma_quad_array(i).gt3_rxchariscomma_out,
       gt3_rxcharisk_out                       => rxcharisk_quad_array(i).gt3_rxcharisk_out,         
       gt3_rxresetdone_out                     => rxresetdone_quad_array(i).gt3_rxresetdone,                       
               
   
       --____________________________COMMON PORTS________________________________
       GT0_QPLLLOCK_OUT                       => mgt_QPLLLOCK_OUT(i), --.GT0_QPLLLOCK_OUT  , 
       GT0_QPLLREFCLKLOST_OUT                 => mgt_QPLLREFCLKLOST_OUT(i), --.GT0_QPLLREFCLKLOST_OUT,
       sysclk_in                              => TTC_CLK
   
       );
    
  end generate MGT_GEN; 






end Behavioral;
