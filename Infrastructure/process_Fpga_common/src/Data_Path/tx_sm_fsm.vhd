
--  This tx state machine recives sorted TOB output data and transimits to the Topo
--  If valid is low it transimits k character of bc.
--  The seven word will be k charcter of "3c" for synchronisation 

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

 entity tx_sm is
   PORT (
      clk        : IN     std_logic ;
      reset      : IN     std_logic ;
      sorted_TOB : IN     std_logic_vector (31 downto 0);
      valid      : IN     std_logic ;
      tx_data    : OUT    std_logic_vector (31 downto 0);
      txcharisk  : OUT    std_logic_vector (3 downto 0)
   );
   end  tx_sm;
Architecture fsm OF tx_sm IS

   -- Architecture Declarations
   signal cntr :unsigned (2 downto 0);

   Type STATE_TYPE IS (
      idle,
      tx,
      s1,
      s2
   );
 
   -- Declare current and next state signals
   signal current_state : STATE_TYPE;
   signal next_state : STATE_TYPE;

   -- Declare any pre-registered internal signals
   signal tx_data_cld : std_logic_vector (31 downto 0);
   signal txcharisk_cld : std_logic_vector (3 downto 0);

begin

   -----------------------------------------------------------------
   clocked_proc : process ( 
      clk,
      reset
   )
   -----------------------------------------------------------------
   begin
      if (reset = '1') then
         current_state <= idle;
         -- Default Reset Values
         tx_data_cld <= (others => '0');
         txcharisk_cld <= (others => '0');
         cntr <= (others => '0');
      ELSif (clk'EVENT AND clk = '1') then
         current_state <= next_state;

         -- Combined Actions
         case current_state IS
            when idle => 
               if (valid ='1') then 
                  tx_data_cld <= sorted_TOB;
                  cntr <= cntr +1;
                  txcharisk_cld <= "0000";
               end if;
            when tx => 
               tx_data_cld <= sorted_TOB;
               cntr <= cntr +1;
               txcharisk_cld <= "0000";
               if (valid ='0') then 
                  tx_data_cld<= x"000000bc";
                  txcharisk_cld<= "0001";
               end if;
            when s1 => 
               cntr <= (others => '0');
               if (valid ='1') then 
                  tx_data_cld <= sorted_TOB;
                  cntr <= cntr +1;
                  txcharisk_cld <= "0000";
               end if;
            when s2 => 
               cntr<= cntr +1;
               if (cntr = 6) then 
                  tx_data_cld <= x"0000003c";
                  txcharisk_cld <= "0001";
               end if;
            when OTHERS =>
               NULL;
         end case;
      end if;
   end process clocked_proc;
 
   -----------------------------------------------------------------
   nextstate_proc : process ( 
      cntr,
      current_state,
      valid
   )
   -----------------------------------------------------------------
   begin
      case current_state IS
         when idle => 
            if (valid ='1') then 
               next_state <= tx;
            else
               next_state <= idle;
            end if;
         when tx => 
            if (valid ='0') then 
               next_state <= s2;
            ELSif (cntr = 6) then 
               next_state <= s1;
            else
               next_state <= tx;
            end if;
         when s1 => 
            if (valid ='1') then 
               next_state <= tx;
            else
               next_state <= s1;
            end if;
         when s2 => 
            if (cntr = 6) then 
               next_state <= s1;
            else
               next_state <= s2;
            end if;
         when OTHERS =>
            next_state <= idle;
      end case;
   end process nextstate_proc;
 
   
   -- Clocked output assignments
   tx_data <= tx_data_cld;
   txcharisk <= txcharisk_cld;
end ARCHITECTURE fsm;
