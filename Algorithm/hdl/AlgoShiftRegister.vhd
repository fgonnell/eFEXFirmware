-------------------------------------------------------
--! @file 
--! @brief Algorithm input-data shift register
--! @details
--! Here is a detailed description of the input shift register.
--! This module handles the data distribution to the 8 ACs within 5 clock cycles.
--! All the input data are loaded in parallel into a shift register at the
--! beginning of every BC. Then data are shifted at 200 MHz at every rising
--! edge of the clock.
--!
--! The algorithm logic is connected only to the first 3 locations of the
--! shift  register, so that the same data that served as environment (location
--! 2) is then used as core (location 1) and finally
--! as environment again (location 0) before being discarded.
--!
--! On every 200MHz clock cycle, data “moves” in the Eta direction, so each of the 8 ACs handles
--! always the same Phi but a value of Eta 0.1 higher on each of the 5 clock cycles.
--!
--! The shift register has the capability to handle peripherical regions. This
--! is done by filling with dummy doata (zeroes) the regions for which data is
--! not provided by the calorimeter, i.e. the first or last Eta column.
--!
--! In order to do this this module needs to know if the eFEX board is located
--! on the right border (high Eta values) or on the left Border (low Eta values).
--! This is done through the LeftRight port.
--! - If the value of LeftRight port is '0' the eFEX board is locatd on the left so the
--! first column in the shift register will be filled with dummy data.
--! - If the value of LeftRight port is '1' the eFEX board is locatd on the right so the
--! last column in the shift register will be filled with dummy data.
--! If the eFEX module is not located in a border region, it is recomended to
--! set this value to 1 and ignore the TOBs produced during the last 200 MHz
--! clock cycle
--!
--! @image html ShiftRegister.png "A representation of the shift register" width=10cm
--! In the picture above you can see a graphical representation of the input
--! data shift register. On the top, a graphical view of the trigger towers is
--! shown. Orange towers are core and green are environment. As depicted by the
--! black borders, the core towers are also used as environment.
--! The blue column at the far right represents dummy data.
--!
--! On the bottom, the concept from the point of view of the shift register is
--! shown. YOu can see that on the first clock cycle the SR is loaded and the
--! it is shifted for 4 more clock cycles. The algorithm logic is connected to
--! the first 3 locations of the SR, represented in colour.
--!
--! Here is a table of input Tower numbering
--! |ST00|ST01|ST02|ST03|ST04|ST05|
--! |----|----|----|----|----|----|
--! | 09 | 19 | 29 | 39 | 49 | 59 |
--! | 08 | 18 | 28 | 38 | 48 | 58 |
--! | 07 | 17 | 27 | 37 | 47 | 57 |
--! | 06 | 16 | 26 | 36 | 46 | 56 |
--! | 05 | 15 | 25 | 35 | 45 | 55 |
--! | 04 | 14 | 24 | 34 | 44 | 54 |
--! | 03 | 13 | 23 | 33 | 43 | 53 |
--! | 02 | 12 | 22 | 32 | 42 | 52 |
--! | 01 | 11 | 21 | 31 | 41 | 51 |
--! | 00 | 10 | 20 | 30 | 40 | 50 |
--! 
--! @author Francesco Gonnella
-------------------------------------------------------

--! Use standard library
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;


library work;
--! Use internal algorithm data types and functions
use work.DataTypes.all;
--! Use external algorithm data types and functions
use work.AlgoDataTypes.all;

--!@copydoc AlgoShiftRegister.vhd
entity AlgoShiftRegister is
  port (CLK200 : in std_logic;  --!200 MHz clock

        IN_Load      : in  std_logic;  --! In load signal, synch with data arrival
        IN_Data      : in  TriggerTowerMatrix;  --! All TT input data defined in DataTypes.vhd
        IN_Edge      : in  std_logic;  --! Signal indicating if eFEX is on edge position
        IN_LeftRight : in  std_logic;  --! Signal indicating if eFEX module position
        OUT_Data     : out TriggerTowers(3*INPUT_ROWS-1 downto 0));  --! Output data to be fed to algorithm logic (3 columns)
end AlgoShiftRegister;

--!@copydoc AlgoShiftRegister.vhd
architecture Behavioral of AlgoShiftRegister is

  --!@brief  The whole memory blocks of the shift resgiste
  --!@details The memory blocks of the shift register are more than the input
  --! data. There is one additional column to be filled with dummy data (all zeroes).
  signal ShiftTowers : TriggerTowersArray(INPUT_COLUMNS downto 0)(INPUT_ROWS-1 downto 0);
  signal LeftRight   : std_logic;

--  ####### attributes  ########
 attribute keep       : string ;
 attribute max_fanout : integer;
 attribute keep of       ShiftTowers : signal is "true" ;
 attribute max_fanout of ShiftTowers : signal is 30;

--   #######################################
begin

  OUT_Data <= (INPUT_ROWS-1 downto 0              => ShiftTowers(0)(INPUT_ROWS-1 downto 0),
               2*INPUT_ROWS-1 downto INPUT_ROWS   => ShiftTowers(1)(INPUT_ROWS-1 downto 0),
               3*INPUT_ROWS-1 downto 2*INPUT_ROWS => ShiftTowers(2)(INPUT_ROWS-1 downto 0));

  -- When eFEX is not on edge, posistion is always considered to be left
  LeftRight <= '0' when IN_edge = '0' else IN_LeftRight;

  --!@brief Shift register process
  --!@details As a function of the LeftRioght signal, when IN_Load = '1' the
  --! fisrt or the last column of ShifTowers is loaded with zeroes
  LOADER_SHIFTER : process (CLK200)
  begin
    if rising_edge(CLK200) then
      if IN_Load = '1' then
        case LeftRight is
          when '0' =>
            -- left, put zeroes in the last (high eta) column
            ShiftTowers(INPUT_COLUMNS)            <= (others => ZERO_TRIGGER_TOWER);
            ShiftTowers(INPUT_COLUMNS-1 downto 0) <= IN_Data;

          when others =>
            -- right, put zeroes in the first (small eta) column            
            ShiftTowers(INPUT_COLUMNS downto 1) <= IN_Data;
            ShiftTowers(0)                      <= (others => ZERO_TRIGGER_TOWER);
        end case;

      else
        -- shift data
        ShiftTowers(INPUT_COLUMNS)            <= (others => ZERO_TRIGGER_TOWER);
        ShiftTowers(INPUT_COLUMNS-1 downto 0) <= ShiftTowers(INPUT_COLUMNS downto 1);
      end if;
    end if;
  end process;

end Behavioral;
