----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.08.2018 16:15:43
-- Design Name: 
-- Module Name: mgt_tx_error_detector - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library infrastructure_lib;
use infrastructure_lib.all;
use infrastructure_lib.synch_type.all;

entity mgt_tx_error_detector is

generic(NCOUNTERS: natural := 12 );

	port(
	
		tx_clk      : in std_logic_vector( NCOUNTERS-1 downto 0);  --!  mgt tx clocks
		rx_data_in  : in mgt_data_array ( NCOUNTERS-1 downto 0);                           --! input data to the error detector. It is  from the phase mux output
		inject_error: in std_logic_vector( NCOUNTERS-1 downto 0):= (others=> '0'); --! for testing 
		clear_error : in std_logic ; --_vector( NCOUNTERS-1 downto 0); --! clearing error counters
		error_counters : out std_logic_vector(NCOUNTERS*16-1 downto 0)  --! high when found error in the data
		
		);
		
end mgt_tx_error_detector;

architecture Behavioral of mgt_tx_error_detector is

type counter_array is array( 0 to NCOUNTERS-1) of unsigned(15 downto 0);
signal counters, error_count: counter_array := (others => (others => '0'));

begin




gen_error_detector: for i in 0 to NCOUNTERS-1 --! generate ncounters number of mgt error dectectors using generate statement

generate 

 error_counters( i+15+15*i downto i+15*i) <= std_logic_vector(counters(i) ); --! asign error counters (16 bit counter)
 
 mgt_error_detect :entity infrastructure_lib.mgt_error_detector
               generic map( 
         ERROR_BITS => 16 ) -- (16 bit counter)
port map  (
		clock        => tx_clk(i),  --! tx clks
		rx_data_in   => rx_data_in(i), --! incoming data 
		inject_error => inject_error(i),
		clear_error  => clear_error,
		error_count  => counters(i) --! set high when found error inthe data  
				
		);
		
end generate  gen_error_detector;


end Behavioral;
