onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /data_alignment_tb/clk280
add wave -noupdate /data_alignment_tb/rx_clk280
add wave -noupdate /data_alignment_tb/MGT_Commadet
add wave -noupdate /data_alignment_tb/MGT_data
add wave -noupdate /data_alignment_tb/DataToAlg
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {5433927702 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 631
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {5368219670 fs} {5499635734 fs}
