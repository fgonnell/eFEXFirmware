--
-- VHDL Architecture moh_test.parity_checker.spec
--
-- Created:
--          by - mjs59.UNKNOWN (te7gromit)
--          at - 19:07:03 11/04/15
--
-- 
--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;


ENTITY parity_checker IS
  PORT (
       Clk        :IN std_logic;
       Reset      :IN std_logic;
       Data_in    :IN std_logic_vector(9 downto 0);
       Data_out   :OUT std_logic_vector(8 downto 0);
       even_parity :OUT std_logic
     );
END ENTITY parity_checker;

--
ARCHITECTURE spec OF parity_checker IS
  
BEGIN
  parity_check: process(clk)
  variable parity: std_logic := '0';
  
                Begin
                    
                    if clk' event and clk ='1' then
                            parity := '0';
                          for i in 0 to 9 loop                         
                           if data_in(i) = '1' then                          
                              parity := not parity;                              
                           end if;                        
                           end loop;  
                                              
                      even_parity<=parity;                  
                      Data_out <= data_in(8 downto 0);
                    end if;
                      
                end process;   
END ARCHITECTURE spec;

