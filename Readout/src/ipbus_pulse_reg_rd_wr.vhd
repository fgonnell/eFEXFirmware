--! @file
--! @brief Pulsed 32b register read and write with automatic reset
--! @details 
--! This is 32b register, which is read/written through the IPBus.
--! This is a Pulse Register and automatically resets to ZERO after one clock cycle. 
--! @author Saeed Taghavi

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library ipbus_lib;
use ieee.numeric_std.all;
use ipbus_lib.ipbus.all;

--! @copydoc ipbus_pulse_reg_rd_wr.vhd
entity ipbus_pulse_reg_rd_wr is
	generic(addr_width: natural := 0);
port(
    --! IPBus Clock input
    ipb_clk : in std_logic;
    --! IPBus Reset input
    ipb_rst : in std_logic;
    --! IPBus input bus going from master to slaves
    ipbus_in: in ipb_wbus;
    --! IPBus output bus going from slaves to master
    ipbus_out: out ipb_rbus;
    --! 32-bit output of the register which can be read/written via IPBus
    q: out STD_LOGIC_VECTOR(2**addr_width*32-1 downto 0)
);
end ipbus_pulse_reg_rd_wr;

--! @copydoc ipbus_pulse_reg_rd_wr.vhd
architecture Behavioral of ipbus_pulse_reg_rd_wr is

	type reg_array is array(2**addr_width-1 downto 0) of std_logic_vector(31 downto 0);
    signal reg      : reg_array;
    signal sel      : integer range 0 to 2 ** ADDR_WIDTH - 1 := 0;
    signal ack      : std_logic;
    signal ack_tmp1  : std_logic;
    signal ack_tmp2  : std_logic;
    signal ack_tmp3  : std_logic;
    signal ack_tmp4  : std_logic;
    signal rst_tmp  : std_logic := '0';
    signal ipb_wdata_tmp  : std_logic_vector (31 downto 0) := (others=>'0');

begin

    sel <= to_integer(unsigned(ipbus_in.ipb_addr(addr_width - 1 downto 0))) when addr_width > 0 else 0;

U1 : process(ipb_clk)
begin
    if rising_edge(ipb_clk) then
        if ipb_rst = '1' OR rst_tmp = '1' then
            reg <= (others=> (others=>'0'));
            ack      <= '0' ;
            ack_tmp4 <= '0' ;
        else
             if ipbus_in.ipb_strobe='1' and ipbus_in.ipb_write='1' then
                reg(sel) <= ipbus_in.ipb_wdata;
             end if;
        end if;
        ipbus_out.ipb_rdata <= reg(sel);
        ack <= ipbus_in.ipb_strobe and not ack;
        
        -- reset the register after the pulse has been registered
        ack_tmp1 <= ack ;    -- delay by 1 clock
        ack_tmp2 <= ack_tmp1 ;    -- delay by 1 clock
        ack_tmp3 <= ack_tmp2 ;    -- delay by 1 clock
        ack_tmp4 <= ack_tmp3 ;    -- delay by 1 clock
        
        if ack_tmp4 = '1' then   -- upon acknowledge reset the register
            rst_tmp <= '1' ;
        else
             rst_tmp <= '0' ;
        end if;
    end if;
end process;

ipbus_out.ipb_ack <= ack;
ipbus_out.ipb_err <= '0';

q <= reg(sel);

-- reset the register after the pulse has been registered
--U2 : process(ipb_clk)
--begin
--    if rising_edge(ipb_clk) then
--        if ipb_wdata_tmp = X"00000000" then
--            rst_tmp <= '0' ;
--        else
--             rst_tmp <= '1' ;
--        end if;

--    end if;
--end process;



end Behavioral;
