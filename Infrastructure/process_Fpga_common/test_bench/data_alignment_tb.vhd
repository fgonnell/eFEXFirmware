-------------------------------------------------------------------------------
-- Title      : Testbench for design "data_alignment"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : data_alignment_tb.vhd
-- Author     : Francesco Gonnella  <fg@epldt072.ph.bham.ac.uk>
-- Company    : 
-- Created    : 2019-09-03
-- Last update: 2020-03-03
-- Platform   : 
-- Standard   : VHDL'08, Math Packages
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2019 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2019-09-03  1.0      fg      Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library std;
use std.textio.all;             --TEXT is here

library algolib;
use algolib.AlgoDataTypes.all;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.all;
use TOB_rdout_lib.data_type_pkg.all;

library infrastructure_lib;
use infrastructure_lib.synch_type.all;
use infrastructure_lib.EfexDataFormats.all;


----------------------------------------------------------------------------------------------------------------------------------------------------------------

entity data_alignment_tb is

end entity data_alignment_tb;

----------------------------------------------------------------------------------------------------------------------------------------------------------------

architecture test of data_alignment_tb is

  -- component generics
  constant n_channels  : natural := N_MGT;

  -- component ports

  signal clk280          : std_logic;
  signal rx_clk280       : std_logic_vector(n_channels-1 downto 0);
  signal MGT_Commadet    : std_logic_vector(n_channels-1 downto 0);
  signal enable_mgt      : std_logic_vector(n_channels-1 downto 0) := (others => '1');
  signal BC_Reg_sel      : std_logic_vector(255 downto 0) := (others => '0');
  signal mux_sel         : std_logic_vector(255 downto 0) := (others => '0');
  signal pseudo_orbit    : std_logic := '0';
  signal start           : std_logic := '0';
  signal start_pulse_rst : std_logic := '0';
  signal rx_resetdone    : std_logic_vector(n_channels-1 downto 0) := (others => '1');
  signal reset           : std_logic := '0';
  signal DataToAlg       : Algoinput;
  signal BCID            : BCID_array;
  signal RAW_data        : RAW_data_224_type;
  signal bc_cntr_0       : std_logic_vector(79 downto 0):= (others => '0');
  signal bc_cntr_1       : std_logic_vector(79 downto 0):= (others => '0');
  signal bc_cntr_2       : std_logic_vector(79 downto 0):= (others => '0');
  signal bc_cntr_3       : std_logic_vector(79 downto 0):= (others => '0');
  signal bc_mux_cntr_0   : std_logic_vector(79 downto 0):= (others => '0');
  signal bc_mux_cntr_1   : std_logic_vector(79 downto 0):= (others => '0');
  signal bc_mux_cntr_2   : std_logic_vector(79 downto 0):= (others => '0');
  signal bc_mux_cntr_3   : std_logic_vector(79 downto 0):= (others => '0');
  signal bcn_synch       : std_logic_vector(63 downto 0):= (others => '0');
  signal crc_error_chan  : std_logic_vector(63 downto 0):= (others => '0');
  signal data_readout_0  : std_logic_vector(223 downto 0);
  signal data_readout_1  : std_logic_vector(223 downto 0);
  signal data_readout_2  : std_logic_vector(223 downto 0);
  signal data_readout_3  : std_logic_vector(223 downto 0):= (others => '0');
  signal delay_num       : std_logic_vector(255 downto 0):= (others => '0');
  signal Reg224_latch    : std_logic_vector(n_channels-1 downto 0):= (others => '0');
  signal ttc_pipe        : std_logic_vector(n_channels-1 downto 0):= (others => '0');
  signal delay_latch     : std_logic_vector(n_channels-1 downto 0):= (others => '0');
  signal eFEXPosition    : std_logic_vector(31 downto 0) := x"00000000";
  signal MGT_data, ram_data : mgt_data_in;


  signal DATA_OUT   : std_logic_vector(31 downto 0);
  signal DATA_READY : std_logic;
  signal dataready  : std_logic_vector(31 downto 0) := (others => '0');
  signal CLK40      : std_logic;
  signal CLK160     : std_logic;
  signal CLK200     : std_logic;
  signal LOAD       : std_logic;
  

  signal RST  : std_logic := '0';
  signal WE   : std_logic;


-- clock
  signal Clk : std_logic := '1';

begin  -- architecture test

  input_for : for i in 0 to 63 generate
    FR : entity work.MappingFileReader
      generic map (
        FILENAME => "../../../../../../sim/DataMapping/MGT" & integer'image(i) & ".txt")
      port map (
        DATA_OUT   => mgt_data(i),
        COMMA_OUT => MGT_commadet(i),
        DATA_READY => DATA_READY,
        CLK40      => CLK40,
        CLK160     => CLK160,
        CLK200     => CLK200,
        CLK280     => CLK280,
        LOAD       => LOAD,
        RESET      => RESET);
  end generate input_for;

 clk_for: for i in 0 to rx_clk280'high generate
 rx_clk280(i) <= clk280;
 
 end generate;

-- component instantiation
  DUT : entity infrastructure_lib.data_alignment
    generic map (
      n_channels  => 64,
      FPGA_NUMBER => 1)
    port map (
      TTC_clk         => clk40,
      clk280          => clk280,
      rx_clk280       => rx_clk280,
      MGT_Commadet    => MGT_Commadet,
      enable_mgt      => enable_mgt,
      BC_Reg_sel      => BC_Reg_sel,
      mux_sel         => mux_sel,
      pseudo_orbit    => pseudo_orbit,
      start           => reset,
      start_pulse_rst => reset,
      rx_resetdone    => rx_resetdone,
      reset           => reset,
      DataToAlg       => DataToAlg,
      BCID            => BCID,
      RAW_data        => RAW_data,
      bc_cntr_0       => bc_cntr_0,
      bc_cntr_1       => bc_cntr_1,
      bc_cntr_2       => bc_cntr_2,
      bc_cntr_3       => bc_cntr_3,
      bc_mux_cntr_0   => bc_mux_cntr_0,
      bc_mux_cntr_1   => bc_mux_cntr_1,
      bc_mux_cntr_2   => bc_mux_cntr_2,
      bc_mux_cntr_3   => bc_mux_cntr_3,
      bcn_synch       => bcn_synch,
      crc_error_chan  => crc_error_chan,
      data_readout_0  => data_readout_0,
      data_readout_1  => data_readout_1,
      data_readout_2  => data_readout_2,
      data_readout_3  => data_readout_3,
      delay_num       => delay_num,
      Reg224_latch    => Reg224_latch,
      ttc_pipe        => ttc_pipe,
      delay_latch     => delay_latch,
      eFEXPosition    => eFEXPosition,
      sel_data_in     => '0',
      kchar => (others => '0'),
      ram_data => (others => (others => '0')),
      MGT_data        => MGT_data);


  data_read_proc : process (clk280)
  begin
  if rising_edge(clk280) then
    dataready(0) <= DATA_READY;
    dataready(31 downto 1) <= dataready(30 downto 0);
  end if;
  
  end process;

  FW : entity work.MappingFileWriter
    generic map (
      filename       => "../../output_file.txt")
    port map (
      CLK     => clk40,
      RST     => reset,
      DATA_IN => DataToAlg,
      WE      => dataready(14) 
      );

-- clock generation
  Clk <= not Clk after 10 ns;

  -- waveform generation
  WaveGen_Proc : process
  begin
    -- insert signal assignments here

    wait until Clk = '1';
  end process WaveGen_Proc;







end architecture test;

----------------------------------------------------------------------------------------------------------------------------------------------------------------

configuration data_alignment_tb_test_cfg of data_alignment_tb is
  for test
  end for;
end data_alignment_tb_test_cfg;

----------------------------------------------------------------------------------------------------------------------------------------------------------------
