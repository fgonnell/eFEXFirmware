--! @file
--! @brief Serial sorter module
--! @details 
--! 
--! IN_Clear should be '1' togheter with the first valid input data word
--! @author Francesco Gonnella

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
use work.DataTypes.all;
use work.AlgoDataTypes.all;

--! @copydoc SerialSorter.vhd
entity SerialSorter is
  port (
    clk       : in  std_logic;
    clk_out   : in  std_logic;
    IN_Load   : in  std_logic;
    IN_Clear  : in  std_logic;
    IN_Data   : in  AlgoTriggerObject;
    OUT_Start : out std_logic;
    OUT_Data  : out AlgoTriggerObject
    );

end entity SerialSorter;


--! @copydoc SerialSorter.vhd
architecture str of SerialSorter is
  -----------------------------------------------------------------------------
  -- Internal signal declarations
  -----------------------------------------------------------------------------
  signal serial     : AlgoTriggerObjects(5 downto 0);
  signal OutputData : AlgoTriggerObjects(4 downto 0);
  signal Inhibit    : std_logic_vector(5 downto 0);
  signal SyncData   : AlgoTriggerObjects(4 downto 0);

begin  -- architecture str

  -----------------------------------------------------------------------------
  -- Component instantiations
  -----------------------------------------------------------------------------
  SortingCells : for i in 0 to 4 generate
    serial(0)  <= ZERO_ALGO_TRIGGER_OBJECT;
    Inhibit(0) <= '0';

    SORT_CELL : entity work.SortingCell
      port map (
        clk         => clk,
        IN_clear    => IN_Clear,
        IN_Previous => serial(i),
        OUT_Next    => serial(i+1),
        IN_Parallel => IN_Data,
        IN_Inhibit  => Inhibit(i),
        OUT_Inhibit => Inhibit(i+1));
  end generate SortingCells;

  -- Just before the sorter gets cleared saves the data in the SyncData buffer
  Sync_proc : process (clk)
  begin
    if rising_edge(clk) then
      if IN_Clear = '1' then
        SyncData <= serial(5 downto 1);
      else
        SyncData <= SyncData;
      end if;
    end if;
  end process Sync_proc;

  -- When IN_Load comes, in the 280 domain, transers the data from SyncData to Output data
  Output_proc : process (clk_out)
  begin  -- process Output_proc
    if rising_edge(clk_out) then
      if IN_Load = '1' then
        OUT_Start  <= '1';
        OutputData <= SyncData;
      else
        OUT_Start                   <= '0';
        OutputData(OutputData'high) <= ZERO_ALGO_TRIGGER_OBJECT;
        for i in OutputData'low to OutputData'high-1 loop
          OutputData(i) <= OutputData(i+1);
        end loop;  -- i
      end if;
    end if;
  end process Output_proc;

  OUT_Data <= OutputData(0);

end architecture str;

-------------------------------------------------------------------------------
