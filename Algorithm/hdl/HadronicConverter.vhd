--! @file
--! @brief Energy converter module
--! @details 
--! 
--! Thie module converts energy value from the linear 10-bit with .500 GeV LSB
--! to linear 16-bit with .025 GeV LSB
--! This is done adding 2 copies of the input signal, one right shifted by 2 and one right shifted by 4
--! 
--! @author Francesco Gonnella

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
use work.DataTypes.all;
use work.AlgoDataTypes.all;

--! @copydoc HadronicConverter.vhd
entity HadronicConverter is
  port (
    clk      : in  std_logic;
    IN_Data  : in  std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
    OUT_Data : out std_logic_vector(DATA_WIDTH-1 downto 0)
    );
end entity HadronicConverter;


--! @copydoc HadronicConverter.vhd
architecture str of HadronicConverter is
  -----------------------------------------------------------------------------
  -- Internal signal declarations
  -----------------------------------------------------------------------------
  signal Data2, Data4 : std_logic_vector(DATA_WIDTH-1 downto 0);
  signal Saturated    : std_logic;

begin  -- architecture str
  Sync_proc : process (clk)
  begin
    if rising_edge(clk) then
      Saturated <= '1' when IN_Data = "1111111111" else '0';
      Data2     <= '0'&'0'&'0'&'0'& IN_Data &'0'&'0';
      Data4     <= '0'&'0'& IN_Data &'0'&'0'&'0'&'0';

      OUT_Data <= std_logic_vector(unsigned(Data2) + unsigned(Data4)) when Saturated = '0' else
                  "1111111111111111";
    end if;
  end process Sync_proc;
end architecture str;

-------------------------------------------------------------------------------
