onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider Clocks
add wave -noupdate /raw_data_rdout_tb/clk_40M_ld
add wave -noupdate /raw_data_rdout_tb/clk_40M
add wave -noupdate /raw_data_rdout_tb/U2_clk_tst/clk_160M
add wave -noupdate /raw_data_rdout_tb/U2_clk_tst/clk_200M
add wave -noupdate /raw_data_rdout_tb/U2_clk_tst/clk_280M
add wave -noupdate /raw_data_rdout_tb/U2_clk_tst/reset
add wave -noupdate /raw_data_rdout_tb/U2_clk_tst/locked
add wave -noupdate -divider RAW_data_array
add wave -noupdate /raw_data_rdout_tb/U3_RAW_data/RAW_data_out
add wave -noupdate /raw_data_rdout_tb/U3_RAW_data/TXCTRL_OUT
add wave -noupdate -divider {Slave block}
add wave -noupdate /raw_data_rdout_tb/slave_2/ipb_rst
add wave -noupdate /raw_data_rdout_tb/slave_2/ipb_clk
add wave -noupdate /raw_data_rdout_tb/slave_2/IPb_in
add wave -noupdate /raw_data_rdout_tb/slave_2/IPb_out
add wave -noupdate /raw_data_rdout_tb/slave_2/RAW_FIFO_FULL_THRESH_ASSERT
add wave -noupdate /raw_data_rdout_tb/slave_2/RAW_FIFO_FULL_THRESH_NEGATE
add wave -noupdate /raw_data_rdout_tb/slave_2/BCN_FIFO_full_thresh_assert
add wave -noupdate /raw_data_rdout_tb/slave_2/BCN_FIFO_full_thresh_negate
add wave -noupdate /raw_data_rdout_tb/slave_2/Link_output_FIFO_FULL_THRESH_ASSERT
add wave -noupdate /raw_data_rdout_tb/slave_2/Link_output_FIFO_FULL_THRESH_NEGATE
add wave -noupdate /raw_data_rdout_tb/slave_2/Link_output_FIFO_rd_data_count
add wave -noupdate /raw_data_rdout_tb/slave_2/Link_output_FIFO_wr_data_count
add wave -noupdate /raw_data_rdout_tb/slave_2/RAW_frame_count
add wave -noupdate /raw_data_rdout_tb/slave_2/RDOUT_PULSE_REG
add wave -noupdate /raw_data_rdout_tb/slave_2/SPY_mem_wr_addr
add wave -noupdate /raw_data_rdout_tb/slave_2/SPY_mem_rd_addr_en
add wave -noupdate /raw_data_rdout_tb/slave_2/SPY_mem_rd_addr
add wave -noupdate /raw_data_rdout_tb/slave_2/SPY_mem_rd_data
add wave -noupdate /raw_data_rdout_tb/slave_2/ipbw
add wave -noupdate /raw_data_rdout_tb/slave_2/ipbr
add wave -noupdate /raw_data_rdout_tb/slave_2/ipbr_d
add wave -noupdate /raw_data_rdout_tb/slave_2/RAW_FIFO_FULL_THRESH_ASSERT_i
add wave -noupdate /raw_data_rdout_tb/slave_2/RAW_FIFO_FULL_THRESH_NEGATE_i
add wave -noupdate /raw_data_rdout_tb/slave_2/BCN_FIFO_full_thresh_assert_i
add wave -noupdate /raw_data_rdout_tb/slave_2/BCN_FIFO_full_thresh_negate_i
add wave -noupdate /raw_data_rdout_tb/slave_2/Link_output_FIFO_FULL_THRESH_ASSERT_i
add wave -noupdate /raw_data_rdout_tb/slave_2/Link_output_FIFO_FULL_THRESH_NEGATE_i
add wave -noupdate /raw_data_rdout_tb/slave_2/RAW_Link_output_FIFO_rd_data_count_i
add wave -noupdate /raw_data_rdout_tb/slave_2/RAW_Link_output_FIFO_wr_data_count_i
add wave -noupdate /raw_data_rdout_tb/slave_2/RDOUT_PULSE_REG_i
add wave -noupdate /raw_data_rdout_tb/slave_2/SPY_mem_wr_addr_i
add wave -noupdate -divider {XTOBs TOBs Out}
add wave -noupdate -divider {Sorted TOBs Out}
add wave -noupdate -divider Modules
add wave -noupdate -divider -height 50 Algorithm
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 5} {3075100000 fs} 0}
quietly wave cursor active 0
configure wave -namecolwidth 213
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix -1
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 fs} {21 us}
