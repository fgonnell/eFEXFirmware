--! @file
--! @brief RAW calorimeter data readout slave registers
--! @details 
--! This module provides IPBus access for all Read Only and Read/Write reigsters withing the RAW calorimeter readout block.
--! @author Saeed Taghavi

library IEEE;
use IEEE.STD_LOGIC_1164.all;

library ipbus_lib;
use ipbus_lib.ipbus.all;

library TOB_rdout_lib;
use TOB_rdout_lib.data_type_pkg.all;
use TOB_rdout_lib.ipbus_decode_efex_raw_readout.all;  -- RAW calorimeter readout package

--! @copydoc slave_RAW_readout.vhd
entity slave_RAW_readout is
  port (
    --! IPBus Reset input
    ipb_rst : in  std_logic;
    --! IPBus Clock input
    ipb_clk : in  std_logic;
    --! IPBus input bus going from master to slaves
    IPb_in  : in  ipb_wbus;  -- The signals going from master to slaves
    --! IPBus output bus going from slaves to master
    IPb_out : out ipb_rbus;  -- The signals going from slaves to master

    --! Derandomisation FIFO partial full flag assert threshold
    RAW_FIFO_pFULL_THRESH_ASSERT : out std_logic_vector(8 downto 0);
    --! 36b derandomisation FIFO partial full flag negate threshold
    RAW_FIFO_pFULL_THRESH_NEGATE : out std_logic_vector(8 downto 0);

    --! BCN FIFO partial full flag assert threshold
    BCN_FIFO_pFULL_THRESH_assert : out std_logic_vector(8 downto 0);
    --! BCN FIFO partial full flag negate threshold
    BCN_FIFO_pFULL_THRESH_negate : out std_logic_vector(8 downto 0);

    --! Link output FIFO (before MGT) partial full flag assert threshold
    Link_output_FIFO_pFULL_THRESH_ASSERT : out std_logic_vector (12 downto 0);
    --! Link output FIFO (before MGT) partial full flag negate threshold
    Link_output_FIFO_pFULL_THRESH_NEGATE : out std_logic_vector (12 downto 0);
    --! Link output FIFO (before MGT) occupancy data count
    Link_output_FIFO_rd_data_count       : in  std_logic_vector (12 downto 0);
    --! numer of frames in the link output FIFO to be transmitted to MGT & Control FPGA
    RAW_frame_count                      : in  std_logic_vector (31 downto 0);

    --! Derandomisation FIFO full flag assert threshold
    RAW_FIFO_FULL_THRESH_ASSERT : out std_logic_vector(8 downto 0);
    --! 36b derandomisation FIFO full flag negate threshold
    RAW_FIFO_FULL_THRESH_NEGATE : out std_logic_vector(8 downto 0);
    --! Derandomisation FIFO FIFO occupancy data count
    RAW_FIFO_data_count         : in  std_logic_vector(8 downto 0);

    --! Read only register containing all Empty pFull and Full of RAW data block       
    RAW_data_FIFO_flags : in  std_logic_vector (8 downto 0);
    --! The write address offset pre load for RAW data Circular DRPAM 
    RAW_WR_ADDR_OFFSET  : out std_logic_vector (8 downto 0);
    --! RAW SPY Memory write address (read only register)               
    SPY_RAW_mem_wr_addr : in  std_logic_vector (10 downto 0);
    --! IPBus signal going to RAW SPY DPRAM 
    ipbus_out_raw_dpram : out ipb_wbus;  -- signal going to RAW SPY DPRAM
    --! IPBus signal coming from RAW SPY DPRAM
    ipbus_in_raw_dpram  : in  ipb_rbus;   -- signal coming from RAW SPY DPRAM
    --! 54-b error flags from the Error Flag FIFO to IPBUS register
    link_error_flags        : in STD_LOGIC_VECTOR (53 downto 0) 

    );    
end slave_RAW_readout;

--! @copydoc slave_RAW_readout.vhd
architecture Behavioral of slave_RAW_readout is
  signal ipbw         : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal ipbr, ipbr_d : ipb_rbus_array(N_SLAVES-1 downto 0);

  signal RAW_FIFO_pFULL_THRESH_ASSERT_i : std_logic_vector (31 downto 0);
  signal RAW_FIFO_pFULL_THRESH_NEGATE_i : std_logic_vector (31 downto 0);

  signal BCN_FIFO_pFULL_THRESH_assert_i : std_logic_vector (31 downto 0);
  signal BCN_FIFO_pFULL_THRESH_negate_i : std_logic_vector (31 downto 0);

  signal Link_output_FIFO_pFULL_THRESH_ASSERT_i : std_logic_vector (31 downto 0);
  signal Link_output_FIFO_pFULL_THRESH_NEGATE_i : std_logic_vector (31 downto 0);

  signal RAW_FIFO_FULL_THRESH_ASSERT_i : std_logic_vector (31 downto 0);
  signal RAW_FIFO_FULL_THRESH_NEGATE_i : std_logic_vector (31 downto 0);

  signal RAW_Link_output_FIFO_rd_data_count_i : std_logic_vector (31 downto 0);
  signal RAW_data_FIFO_flags_i                : std_logic_vector (31 downto 0);
  signal RAW_FIFO_data_count_i                : std_logic_vector (31 downto 0);
  signal SPY_mem_wr_addr_i                    : std_logic_vector (31 downto 0);
  signal RAW_WR_ADDR_OFFSET_REG_i             : std_logic_vector (31 downto 0);
  signal link_error_flags_1             : std_logic_vector (31 downto 0);
  signal link_error_flags_2             : std_logic_vector (31 downto 0);
  
begin
  -- input signals
  RAW_Link_output_FIFO_rd_data_count_i <= X"0000" & "000" & Link_output_FIFO_rd_data_count;  -- 12 bits
  RAW_FIFO_data_count_i                <= X"00000" & "000" & RAW_FIFO_data_count;  -- 9 bits
  RAW_data_FIFO_flags_i                <= X"00000" & "000" & RAW_data_FIFO_flags;  -- 9 bits

  SPY_mem_wr_addr_i <= X"00000" & "0" & SPY_RAW_mem_wr_addr;

  -- output signals
  RAW_FIFO_pFULL_THRESH_ASSERT <= RAW_FIFO_pFULL_THRESH_ASSERT_i(8 downto 0);
  RAW_FIFO_pFULL_THRESH_NEGATE <= RAW_FIFO_pFULL_THRESH_NEGATE_i(8 downto 0);

  BCN_FIFO_pFULL_THRESH_assert <= BCN_FIFO_pFULL_THRESH_assert_i(8 downto 0);
  BCN_FIFO_pFULL_THRESH_negate <= BCN_FIFO_pFULL_THRESH_negate_i(8 downto 0);

  Link_output_FIFO_pFULL_THRESH_ASSERT <= Link_output_FIFO_pFULL_THRESH_ASSERT_i(12 downto 0);
  Link_output_FIFO_pFULL_THRESH_NEGATE <= Link_output_FIFO_pFULL_THRESH_NEGATE_i(12 downto 0);

  RAW_FIFO_FULL_THRESH_ASSERT <= RAW_FIFO_FULL_THRESH_ASSERT_i(8 downto 0);
  RAW_FIFO_FULL_THRESH_NEGATE <= RAW_FIFO_FULL_THRESH_NEGATE_i(8 downto 0);

  RAW_WR_ADDR_OFFSET <= RAW_WR_ADDR_OFFSET_REG_i(8 downto 0);
  
  link_error_flags_1 <= link_error_flags(31 downto 0);
  link_error_flags_2 <= "0000000000" & link_error_flags(53 downto 32);

---- access to RAW DPRAM spy memeory
  ipbus_out_raw_dpram           <= ipbw(N_SLV_CALO_DATA_SPY_MEM);  -- signal going to RAW SPY DPRAM
  ipbr(N_SLV_CALO_DATA_SPY_MEM) <= ipbus_in_raw_dpram;  -- signal coming from RAW SPY DPRAM

--! @details
--! \verbatim
--! The IPBus bus fabric, which also has address select logic and data multiplexers.
--! This version selects the addressed slave depending on the state of incoming control lines.
--! \endverbatim
  RAW_rdout_fabric : entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV      => N_SLAVES,
                SEL_WIDTH => ipbus_sel_width)
    port map(
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      sel             => ipbus_sel_efex_raw_readout(ipb_in.ipb_addr),
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      ); 

--! @details 
--! \verbatim
--! This is one 32b register, which is read and write cable through the IPBus.
--! \endverbatim
  U1_RAW_FIFO_pFULL_THRESH_ASSERT : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_RAW_FIFO_PROG_FULL_THRESH_ASSERT),
      ipbus_out => ipbr(N_SLV_RAW_FIFO_PROG_FULL_THRESH_ASSERT),
      d         => (others => (others => '0')),
      q(0)      => RAW_FIFO_pFULL_THRESH_ASSERT_i,  -- read/write reg
      stb       => open); 

  U2_RAW_FIFO_pFULL_THRESH_NEGATE : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_RAW_FIFO_PROG_FULL_THRESH_NEGATE),
      ipbus_out => ipbr(N_SLV_RAW_FIFO_PROG_FULL_THRESH_NEGATE),
      d         => (others => (others => '0')),
      q(0)      => RAW_FIFO_pFULL_THRESH_NEGATE_i,  -- read/write reg
      stb       => open); 

  U5_BCN_FIFO_pFULL_THRESH_assert : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_BCN_FIFO_PROG_FULL_THRESH_ASSERT),
      ipbus_out => ipbr(N_SLV_BCN_FIFO_PROG_FULL_THRESH_ASSERT),
      d         => (others => (others => '0')),
      q(0)      => BCN_FIFO_pFULL_THRESH_assert_i,  -- read/write reg
      stb       => open); 

  U6_BCN_FIFO_pFULL_THRESH_negate : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_BCN_FIFO_PROG_FULL_THRESH_NEGATE),
      ipbus_out => ipbr(N_SLV_BCN_FIFO_PROG_FULL_THRESH_NEGATE),
      d         => (others => (others => '0')),
      q(0)      => BCN_FIFO_pFULL_THRESH_negate_i,  -- read/write reg
      stb       => open); 

  U7_Link_output_FIFO_pFULL_THRESH_ASSERT : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_LINK_OUTPUT_FIFO_PROG_FULL_THRESH_ASSERT),
      ipbus_out => ipbr(N_SLV_LINK_OUTPUT_FIFO_PROG_FULL_THRESH_ASSERT),
      d         => (others => (others => '0')),
      q(0)      => Link_output_FIFO_pFULL_THRESH_ASSERT_i,  -- read/write reg
      stb       => open); 

  U8_Link_output_FIFO_pFULL_THRESH_NEGATE : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_LINK_OUTPUT_FIFO_PROG_FULL_THRESH_NEGATE),
      ipbus_out => ipbr(N_SLV_LINK_OUTPUT_FIFO_PROG_FULL_THRESH_NEGATE),
      d         => (others => (others => '0')),
      q(0)      => Link_output_FIFO_pFULL_THRESH_NEGATE_i,  -- read/write reg
      stb       => open); 

--! @details 
--! \verbatim
--! This is one 32b register, which is read only cable through the IPBus.
--! \endverbatim
  U9_Link_output_FIFO_RD_DATA_COUNT : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 0, N_STAT => 1)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_LINK_OUTPUT_FIFO_RD_DATA_COUNT),
      ipbus_out => ipbr(N_SLV_LINK_OUTPUT_FIFO_RD_DATA_COUNT),
      d(0)      => RAW_Link_output_FIFO_rd_data_count_i,  -- read only reg
      q         => open,
      stb       => open);

  U9A_RAW_FIFO_FULL_THRESH_ASSERT : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_RAW_FIFO_FULL_THRESH_ASSERT),
      ipbus_out => ipbr(N_SLV_RAW_FIFO_FULL_THRESH_ASSERT),
      q(0)      => RAW_FIFO_FULL_THRESH_ASSERT_i,  -- read/write reg
      d         => (others => (others => '0')),
      stb       => open);

  U9B_RAW_FIFO_FULL_THRESH_NEGATE : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_RAW_FIFO_FULL_THRESH_NEGATE),
      ipbus_out => ipbr(N_SLV_RAW_FIFO_FULL_THRESH_NEGATE),
      q(0)      => RAW_FIFO_FULL_THRESH_NEGATE_i,  -- read/write reg
      d         => (others => (others => '0')),
      stb       => open);

  U9C_RAW_FIFO_data_count : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 0, N_STAT => 1)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_RAW_FIFO_DATA_COUNT),
      ipbus_out => ipbr(N_SLV_RAW_FIFO_DATA_COUNT),
      d(0)      => RAW_FIFO_data_count_i,  -- read only reg
      q         => open,
      stb       => open);

  U11_RAW_frame_count : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 0, N_STAT => 1)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_RAW_FRAME_COUNT),
      ipbus_out => ipbr(N_SLV_RAW_FRAME_COUNT),
      d(0)      => RAW_frame_count,     -- read only reg
      q         => open,
      stb       => open);

  U12_RAW_data_FIFO_flags : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 0, N_STAT => 1)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_RAW_DATA_FIFO_FLAGS),
      ipbus_out => ipbr(N_SLV_RAW_DATA_FIFO_FLAGS),
      d(0)      => RAW_data_FIFO_flags_i,  -- read only reg
      q         => open,
      stb       => open);

  U13_SPY_mem_wr_addr : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 0, N_STAT => 1)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_SPY_MEM_WR_ADDR),
      ipbus_out => ipbr(N_SLV_SPY_MEM_WR_ADDR),
      d(0)      => SPY_mem_wr_addr_i,   -- read only reg
      q         => open,
      stb       => open);

  U15_RAW_WR_ADDR_OFFSET_REG : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 1, N_STAT => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_RAW_WR_ADDR_OFFSET_REG),
      ipbus_out => ipbr(N_SLV_RAW_WR_ADDR_OFFSET_REG),
      q(0)      => RAW_WR_ADDR_OFFSET_REG_i,  -- read/write reg
      d         => (others => (others => '0')),
      stb       => open);

  U16_FIFO_LINK_ERRORS : entity ipbus_lib.ipbus_ctrlreg_v
    generic map(N_CTRL => 0, N_STAT => 2)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_FIFO_LINK_ERRORS),
      ipbus_out => ipbr(N_SLV_FIFO_LINK_ERRORS),
      d(0)      => link_error_flags_1,   -- read only reg
      d(1)      => link_error_flags_2,   -- read only reg
      q         => open,
      stb       => open);


end Behavioral;
