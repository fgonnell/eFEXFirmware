----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.02.2019 13:14:50
-- Design Name: 
-- Module Name: readout_cfpga - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;




entity readout_cfpga is
 Port ( 
       clk                 : IN  std_logic; 
	   reset               : IN  std_logic;
	   ---aurora 
       aurora_tready       : IN  std_logic;
	   axi4_m_data         : OUT std_logic_vector (63 DOWNTO 0) ;
       axi4_m_tlast        : OUT std_logic;
       axi4_m_tvalid       : OUT std_logic;
	   --readout  
       first               : IN std_logic;           
       payload_data        : IN std_logic_vector (63 DOWNTO 0) ;
       payload_valid       : IN std_logic;
	   payload_last        : IN std_logic;
	   tready_data         : OUT std_logic
       );
       
end readout_cfpga;

architecture Behavioral of readout_cfpga is
signal payload_valid_i,run_crc,rdy_rega,rdy_regb,crc_rdy, payload_last_i :std_logic;
signal payload_data_crc,rega,regb:std_logic_vector(63 downto 0) ;
signal trailer_data : std_logic_vector(63 downto 0):= x"0000000000000000" ;
signal crc_out :std_logic_vector(19 downto 0);
signal crc_reset,reset_i  :std_logic;

 
begin
 
cfpga_sm: entity work.readout_cfpga_sm

   Port Map( 
      aurora_tready       => aurora_tready ,
      clk                 => clk           ,
      payload_trailer_crc => trailer_data,
      first               => first ,
      payload_data        => payload_data ,
      payload_last        => payload_last ,
      payload_len         => x"0020"  ,
      payload_valid       => payload_valid_i,
      reset               => reset         ,
      axi4_m_data         => axi4_m_data   ,
      axi4_m_tlast        => axi4_m_tlast  ,
      axi4_m_tvalid       => axi4_m_tvalid ,
      tready_data         => tready_data,
      crc_reset           => reset_i 
   );



---- crc generator

crc:entity work.CRC20 
--   generic( 
--     Nbits               => 64, 
--     CRC_Width           => 20, 
--     G_Poly              => "0x8349f", 
--     G_InitVal           => x"fffff" 
--     ); 
   Port Map( 
     CRC   =>  crc_out, 
     Calc  =>  run_crc ,
     Clk   =>  clk,
     DIn   =>  payload_data_crc ,
     Reset =>  crc_reset
     );

crc_reset <= reset or reset_i ;
payload_data_crc <= payload_data(31 downto 0) & payload_data(63 downto 32); ---swapping the 32-bit words for crc 64 bits
run_crc          <= payload_valid_i and not first;                 -- enable the crc generator
payload_valid_i    <= payload_valid and not  payload_last; -- payload_valid not included the last data of the trailer


-------------------------------------------------------------------------------
capture_trailer: process( clk)
              begin
                                         
              if clk' event and clk ='1' then
                 if payload_last ='1'  then    -- indicates the trailer                 
                    rega     <= payload_data;    --capture the trailer data before adding crc bits               
               end if;
              end if;  
             end process;  
 -----------------------------------------------------------------------------             
 process( clk)
      begin
                                 
      if clk' event and clk ='1' then
          payload_last_i <=payload_last  ; -- delaying the last. this may not needed in the dave design but used ed rom design           
      end if;  
     end process;        
       
-----------------------------------------------------------------------------             
 delay: process( clk)           --- capturing the crc out on the right time
          begin          
           if clk' event and clk ='1' then  
              rdy_rega <= payload_last;               
              --rdy_regb <= rdy_rega;
              crc_rdy <=  payload_last_i; --rdy_rega;
            end if;
          end process;  
 
 --------------------------------------------------------------------------- 
 traliar_crc_gen:  process( clk)      --generating trailer with crc to  be transmitted
                   begin 
                     if clk' event and clk ='1' then
                       if crc_rdy ='1'  then    
                         trailer_data <=  crc_out & rega (43 downto 0); 
                       end if;    
                    end if; 
                 end process;   
 
end Behavioral;
