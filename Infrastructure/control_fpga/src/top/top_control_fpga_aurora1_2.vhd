--! @file
--! @brief Top of the control FPGA
--! @details 
--! Top module of eFEX control FPGA
--! @author Mohammed Siyad

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
LIBRARY ipbus_lib;
USE ipbus_lib.ipbus.all;
LIBRARY unisim;
USE unisim.VComponents.all;
LIBRARY infrastructure_lib;
USE infrastructure_lib.all;
use infrastructure_lib.mgt_type.all;
use infrastructure_lib.ipbus_decode_L1CaloEfexControl.all;

--! @copydoc top_control_fpga.vhd
ENTITY top_control_fpga IS
  GENERIC (
    -- Repository and synthesis
    --! bit 31 is official, bit 30:16 reserved, bit 15:0 AWE attempt number
    OFFICIAL         : std_logic_vector(31 downto 0) := x"00000000"; 
    --! Date format DDMMYYYY in decimal
    GLOBAL_DATE    : std_logic_vector(31 downto 0) := x"00000000"; 
    --! Time format  00HHMMSS in decimal
    GLOBAL_TIME    : std_logic_vector(31 downto 0) := x"00000000";
    --! Short 7-digit git SHA of the repository
    GLOBAL_SHA    : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the repository (format: MMmmcccc in hex)
    GLOBAL_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the XMLs
    XML_HASH   : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the XMLs
    XML_VERSION   : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the top folder: list file, xdcs, XMLs, tcl file and this file
    TOP_SHA   : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the top folder, see TOP_SHA
    TOP_VER   : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the Hog submodule
    HOG_SHA : std_logic_vector(31 downto 0) := x"00000000";

    --! Version of infrastructure library (format: MMmmcccc in hex)
    INFRASTRUCTURE_LIB_VER : std_logic_vector(31 downto 0) := x"00000000";
    --! Short 7-digit git SHA
    INFRASTRUCTURE_LIB_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Short 7-digit git SHA of the ipbus submodule
    IPBUS_LIB_SHA : std_logic_vector(31 downto 0) := x"00000000"
    );


   PORT( 
      sysclk_p                 : in std_logic;
      sysclk_n                 : in std_logic;        
      gmii_gtx_clk             : out std_logic;
      gmii_tx_en               : out std_logic;
      gmii_tx_er               : out std_logic;
      gmii_txd                 : out std_logic_vector(7 downto 0);
      gmii_rx_clk              : in std_logic;
      gmii_rx_dv               : in std_logic;
      gmii_rx_er               : in std_logic;
      gmii_rxd                 : in std_logic_vector(7 downto 0);
      phy_rstb                 : out std_logic;
      clk_40_n                 : in  std_logic;
      clk_40_p                 : in  std_logic;
      pseudo_orbit_p           : in  std_logic;
      pseudo_orbit_n           : in  std_logic;
      pseudo_orbit_fpga1_p     : out std_logic;
      pseudo_orbit_fpga1_n     : out std_logic;
      pseudo_orbit_fpga2_p     : out std_logic;
      pseudo_orbit_fpga2_n     : out std_logic;
      pseudo_orbit_fpga3_p     : out std_logic;
      pseudo_orbit_fpga3_n     : out std_logic;
      pseudo_orbit_fpga4_p     : out std_logic;
      pseudo_orbit_fpga4_n     : out std_logic;
      
      ----mgt
      Q_CLK_GTREFCLK_PAD_N_IN  : in std_logic_vector(1 downto 0);
      Q_CLK_GTREFCLK_PAD_P_IN  : in std_logic_vector(1 downto 0);
      rxp_IN                   : in std_logic_vector(5 downto 0);
      rxn_IN                   : in std_logic_vector(5 downto 0); 
      txp_OUT                  : out std_logic_vector(3 downto 0); 
      txn_OUT                  : out std_logic_vector(3 downto 0);
      --------------------I2C connections
      i2c_scl_0  : inout std_logic;    
      i2c_sda_0  : inout std_logic;
      i2c_scl_1  : inout std_logic;    
      i2c_sda_1  : inout std_logic;
      i2c_rst_0  : out   std_logic;
      i2c_rst_1  : out   std_logic;
      ------Pll connections-----
      pll_miso   : in  std_logic;
      pll_le_1   : out std_logic;
      pll_le_2   : out std_logic;
      pll_le_3   : out std_logic;
      pll_clko   : out std_logic;
      pll_mosi   : out std_logic;    
      pll_lock_1 : in  std_logic;
      pll_lock_2 : in  std_logic;
      pll_lock_3 : in  std_logic;
      -------------Flash--------------    
	  flash_csn        : out std_logic;
      flash_mosi       : out std_logic;
      flash_miso       : in  std_logic;
      flash_hold       : out std_logic;
      flash_wp         : out std_logic;
      f1_flash_sel     : out std_logic;
      f2_flash_sel     : out std_logic;
      f3_flash_sel     : out std_logic;
      f4_flash_sel     : out std_logic;
      f5_flash_disable : out std_logic;     
      flash_addr       : out std_logic_vector (2 DOWNTO 0); 
       
      -----------------------------
      xtal_ttc_clk_sel: out STD_LOGIC;
      SYNC_B_CDCE     : out STD_LOGIC;
      POWERDN_B_CDCE  : out STD_LOGIC;      
      tx_disable      : out STD_LOGIC; 
 -- Signals coming to contrl FPGA

      master_tx_data1  : in  std_logic_vector (9 DOWNTO 0);
      master_tx_data2  : in  std_logic_vector (9 DOWNTO 0);
      master_tx_data3  : in  std_logic_vector (9 DOWNTO 0);
      master_tx_data4  : in  std_logic_vector (9 DOWNTO 0);

--- signals going out of cntrl FPGA

      master_rx_data1  :  out std_logic_vector (9 DOWNTO 0);
      master_rx_data2  :  out std_logic_vector (9 DOWNTO 0);
      master_rx_data3  :  out std_logic_vector (9 DOWNTO 0);
      master_rx_data4  :  out std_logic_vector (9 DOWNTO 0);
      master_tx_pause1 :  out std_logic;
      master_tx_pause2 :  out std_logic;
      master_tx_pause3 :  out std_logic;
      master_tx_pause4 :  out std_logic;
      hardware_addr    :  in  std_logic_vector(11 downto 0);
      leds             :  out std_logic_vector (1 DOWNTO 0);
      ttc_L1A_p        :  out std_logic_vector (3 DOWNTO 0);
      ttc_L1A_n        :  out std_logic_vector (3 DOWNTO 0);
      ttc_BCR_p        :  out std_logic_vector (3 DOWNTO 0);
      ttc_BCR_n        :  out std_logic_vector (3 DOWNTO 0);
      ttc_ECR_p        :  out std_logic_vector (3 DOWNTO 0);
      ttc_ECR_n        :  out std_logic_vector (3 DOWNTO 0);
      ttc_pr_rdout_p   :  out std_logic_vector (3 DOWNTO 0);
      ttc_pr_rdout_n   :  out std_logic_vector (3 DOWNTO 0);
      --- aurora hub1 links
     -- aurora_txp       :  out std_logic_vector (3 DOWNTO 0);
     -- aurora_txn       :  out std_logic_vector (3 DOWNTO 0);
      aurora_refclk1_p :  in  std_logic;
      aurora_refclk1_n :  in  std_logic;      
      --- aurora hub2 links
      aurora_hub2_txp       :  out std_logic_vector (3 DOWNTO 0);
      aurora_hub2_txn       :  out std_logic_vector (3 DOWNTO 0);
      aurora_hub2_refclk1_p :  in  std_logic;
      aurora_hub2_refclk1_n :  in  std_logic;
      -- control rdy signals
      cntrl_raw_rdy_out   : out  std_logic_vector (3 DOWNTO 0);
      cntrl_tob_rdy_out   : out  std_logic_vector (3 DOWNTO 0)
   );


END top_control_fpga ;

--! @copydoc top_control_fpga.vhd
ARCHITECTURE spec OF top_control_fpga IS
------------------- chipscope for debuging 
 COMPONENT ila_0
 
 PORT (
 	clk : IN STD_LOGIC;
 


 	probe0 : IN STD_LOGIC_VECTOR( 257 DOWNTO 0)
 );
 END COMPONENT  ;
 
 
component clk_ttc
port
 (-- Clock in ports
  -- Clock out ports
  clk40          : out    std_logic;
  clk280          : out    std_logic;
  clk160          : out    std_logic;
  -- Status and control signals
  locked            : out    std_logic;
  clk_in1_p         : in     std_logic;
  clk_in1_n         : in     std_logic
 );
end component;
component clk_wiz_0
port
 (-- Clock in ports
  clkfb_in          : in     std_logic;
  -- Clock out ports
  clk_out1          : out    std_logic;
  clkfb_out         : out    std_logic;
  -- Status and control signals
  reset             : in     std_logic;
  locked            : out    std_logic;
  clk_in1           : in     std_logic
 );
end component;

   -- Architecture declarations
   -- ipbus signals
   signal clk125_fr,clk200,clk280,clk160,clko_p40,ipb_clk,mac_clk,locked,clk_locked,eth_locked,clk40  : std_logic;   
   signal rst_125, rst_ipb, rst_eth, rst_macclk,sys_rst,soft_rst,rsto_eth,enable_pll_rst: std_logic;
   signal pseudo_orbit:std_logic;
   signal mac_rx_data   : std_logic_vector(7 DOWNTO 0);
   signal mac_rx_error,mac_rx_valid,mac_rx_last : std_logic;     
   signal mac_tx_data     : std_logic_vector(7 DOWNTO 0);
   signal mac_tx_error, mac_tx_last,mac_tx_ready,mac_tx_valid : std_logic;    
   signal onehz,nuke ,rst_ipb_ctrl: std_logic;
   signal ipb_master_out : ipb_wbus;
   signal ipb_master_in : ipb_rbus;
   signal ipbw: ipb_wbus_array(N_SLAVES-1 downto 0);
   signal ipbr, ipbr_d: ipb_rbus_array(N_SLAVES-1 downto 0);
   signal ipb_in  : ipb_rbus;
   signal ipb_out : ipb_wbus;  
   signal mac_addr        : std_logic_vector(47 downto 0);
   signal ip_addr         : std_logic_vector(31 downto 0); 
   signal master_rx_data1_int,master_rx_data2_int,master_rx_data3_int,master_rx_data4_int  : std_logic_vector(8 DOWNTO 0); 
   signal master_tx_data1_int,master_tx_data2_int,master_tx_data3_int,master_tx_data4_int  : std_logic_vector(8 DOWNTO 0);
   signal flash_clk,flash_led,scl_enb_0,sda_enb_0,scl_enb_1,sda_enb_1,scl_o_0,sda_o_0,scl_o_1,sda_o_1: std_logic;
   signal hw_id, hw_position, fpga_id, fw_rev,fw_tag,datafmt_rev,status: std_logic_vector(31 downto 0);
   signal master_tx_err1,master_tx_err2 ,master_tx_err3 ,master_tx_err4  : std_logic;
   -----------------------------------------------------------------------------------------------------
   signal control_reg, pulse_reg, reconfig_reg: std_logic_vector (31 downto 0);   
   signal clkfb_in,gmii_rx_clk_int,clkfb_out :std_logic;
   signal select_pll,select_flash :std_logic_vector (1 downto 0);                 
   signal pll_en, flash_csn_int,trigger_reconfig,reconfig,en_reset:std_logic;
   signal data_reg0,data_reg1,reg128_latch:std_logic;
   signal flash_addr_int:std_logic_vector(2 downto 0);
   signal flash_mosi_int,f5_flash_disable_int,f5_flash_csn_int:std_logic;
   signal f1_flash_sel_int,f2_flash_sel_int,f3_flash_sel_int,f4_flash_sel_int:std_logic;
   signal probe :std_logic_vector(12 downto 0);   
   signal master_link_down1,master_link_down2,master_link_down3, master_link_down4,start :std_logic;
   -- mgt signals                                                                      
   signal topo_data_fpga1,topo_data_fpga2,topo_data_fpga3,topo_data_fpga4,rod_busy,ttc_data_hub2: std_logic_vector(31 downto 0);
   signal raw_data_fpga1, raw_data_fpga2, raw_data_fpga3, raw_data_fpga4,ttc_rx_data,rx_er_count: std_logic_vector(31 downto 0);
   signal ttc_sync_data,ttc_sync_data_i,data_out_reg128,readout_ctrl,readout_ctrl_i,ttc_hub2_syn : std_logic_vector(127 downto 0);
   signal mgt_enable               : std_logic_vector(3 downto 0);
   signal bc_reg_sel, mux_sel: std_logic_vector(15 downto 0); 
   signal delay_cntr_0,delay_cntr_1 : std_logic_vector(3 downto 0); 
   signal rx_resetdone_mgt114 : std_logic_vector (1 downto 0);
   signal mgt_commadret     : std_logic_vector (5 downto 0);
   signal RXN_IN_i,RXP_IN_i,rx_clk160:std_logic_vector(7 downto 0);
   signal ttc_BCR,ttc_L1A ,ttc_ECR,ttc_pr_rdout, encode_114,rx_disperr_114  : std_logic_vector(3 downto 0); 
   signal probe0 : std_logic_vector( 257 DOWNTO 0);
   signal sof_topo_fpga1,sof_topo_fpga2,sof_topo_fpga3,sof_topo_fpga4     : std_logic;
   signal sof_raw_fpga1, sof_raw_fpga2, sof_raw_fpga3, sof_raw_fpga4  : std_logic;
   signal rx_resetdone,tx_resetdone,tx_fsm_resetdone,rx_fsm_resetdone,rx_realign,rx_byteisaligned:std_logic_vector(7 downto 0); 
   signal loopback :std_logic_vector (2 downto 0);
   signal rx_disperr,encode_error:std_logic_vector(31 downto 0);
   signal qpll_lock,qpll_refclklost,softreset_tx, softreset_rx : std_logic_vector(1 downto 0);
   signal tx_bufstatus :std_logic_vector(15 downto 0);
   signal gmii_rx_er_i,reset,readout_ctrl_latch,link_reset,link_reset_hub2:std_logic;
 --signal ipbw: ipb_wbus_array(N_SLAVES-1 downto 0);
 --signal ipbr, ipbr_d: ipb_rbus_array(N_SLAVES-1 downto 0);
 
---Aurora Hub1 signal declarations 
signal s_axi_tx_tdata:std_logic_vector (63 downto 0);
signal s_axi_tx_tvalid,s_axi_tx_tready,s_axi_tx_tlast:std_logic; 
signal s_axi_tx_tkeep :std_logic_vector (7 downto 0); 
signal s_axi_ufc_tx_req,s_axi_ufc_tx_ack:std_logic; 
signal s_axi_ufc_tx_ms:std_logic_vector (2 downto 0); 
signal aurora_status:std_logic_vector(31 downto 0); 
signal tx_lane_up :std_logic_vector (3 downto 0);
signal tx_channel_up,tx_hard_err, tx_lock,pll_not_locked,aurora_tx_resetdone:std_logic; 
signal aurora_user_clk,sys_reset_out,init_clk_out:std_logic;

---Aurora hub2 signal declarations
signal s_axi_tx_tdata_hub2:std_logic_vector (63 downto 0);                                   
signal s_axi_tx_tvalid_hub2,s_axi_tx_tready_hub2,s_axi_tx_tlast_hub2:std_logic;                        
signal s_axi_tx_tkeep_hub2 :std_logic_vector (7 downto 0);                                   
signal s_axi_ufc_tx_req_hub2,s_axi_ufc_tx_ack_hub2:std_logic;                                     
signal s_axi_ufc_tx_ms_hub2:std_logic_vector (2 downto 0);                                   
signal aurora_status_hub2:std_logic_vector(31 downto 0);                                     
signal tx_lane_up_hub2 :std_logic_vector (3 downto 0);                                       
signal tx_channel_up_hub2,tx_hard_err_hub2, tx_lock_hub2,pll_not_locked_hub2,aurora_tx_resetdone_hub2:std_logic; 
signal aurora_user_clk_hub2,sys_reset_out_hub2,init_clk_out_hub2:std_logic;
signal hub2_gt_refclk1   :std_logic;

signal ttc_L1A_hub2 ,ttc_BCR_hub2,ttc_ECR_hub2 ,ttc_pr_rdout_hub2 :std_logic_vector( 3 downto 0); 

signal payload_data:std_logic_vector(63 downto 0) ;
signal payload_valid,payload_last,tready_data, first :std_logic;

signal raw_fpga1, raw_fpga2, topo_fpga1,topo_fpga2:std_logic_vector(71 downto 0);
signal rd_raw_fpga1,rd_raw_fpga2,rd_topo_fpga1,rd_topo_fpga2:std_logic;
signal valid_raw_fpga1,valid_raw_fpga2,valid_topo_fpga1,valid_topo_fpga2:std_logic;
signal fifo_empty,valid  :std_logic;
---------------------------------------------------------------------------------------------------


   
BEGIN

cntrl_raw_rdy_out <= "1111";
cntrl_tob_rdy_out <= "1111";
ttc_hub2_syn <= readout_ctrl;

ctrl_scope : ila_0
PORT MAP (
	clk       => rx_clk160(0), --clk160,
    probe0(0) =>  mgt_commadret(0),  -- in coming pseudo orbit from FPGA1
    probe0(4 downto 1) => mgt_enable, 
	probe0(5) => reg128_latch,
    probe0(6) => start,
    probe0(7) => mgt_commadret(1),
    probe0(8) => mgt_commadret(4),
    probe0(12 downto 9) => rx_disperr(3 downto 0), --bc_reg_sel(3 downto 0),
    probe0(16 downto 13)=> rx_disperr(7 downto 4), --(3 downto 0),
    probe0(20 downto 17)=> encode_error(3 downto 0),
    probe0(21) => rx_resetdone(4),
    probe0(25 downto 22) => rx_disperr(19 downto 16),   -- 35 32
    probe0(29 downto 26) => encode_error(19 downto 16),
    probe0(61 downto 30)  => ttc_data_hub2, -- rod_busy, --ttc_rx_data ,       -- in coming ttc information data  
    probe0(189 downto 62) => ttc_hub2_syn, --readout_ctrl, --ttc_sync_data,    -- ttc_information synch goes to process fpgas ttc_rx_data                                  
    probe0(221 downto 190)  => topo_data_fpga1,
    --probe0(223 downto 222)  => softreset_rx ,  --topo_data_fpga2 --mgt_txdata(0).gt0_txdata_in 
    probe0(253 downto 222)  => raw_data_fpga1,--topo_data_fpga2 --mgt_txdata(0).gt0_txdata_in 
    probe0(257 downto 254)  => encode_error(7 downto 4)
 );   
-----------------------------------------------------------------------------
-------lvds
--------------------------------------------------------------------------              
f1_to_f5:IBUFDS       -- referrence  pseudo orbit signal from fpga1 
    port map(       
         i  => pseudo_orbit_p ,       
         ib => pseudo_orbit_n ,           
         o  => pseudo_orbit       
              );       
             
          
f5_to_f1:OBUFDS       
       port map(       
       o  =>  pseudo_orbit_fpga1_p, --! pseudo_orbit going to fpga1        
       ob =>  pseudo_orbit_fpga1_n ,           
       i  =>  pseudo_orbit       
             );       
             
f5_to_f2:OBUFDS       
       port map(       
         o  =>  pseudo_orbit_fpga2_p,      --! pseudo_orbit going to fpga2  
         ob =>  pseudo_orbit_fpga2_n ,           
         i  =>  pseudo_orbit       
          );                    
 f5_to_f3:OBUFDS       
        port map(       
            o  =>  pseudo_orbit_fpga3_p,      --! pseudo_orbit going to fpga3  
            ob =>  pseudo_orbit_fpga3_n ,           
            i  =>  pseudo_orbit       
                    );                    
 f5_to_f4:OBUFDS       
        port map(       
          o  =>  pseudo_orbit_fpga4_p,      --! pseudo_orbit going to fpga4 
          ob =>  pseudo_orbit_fpga4_n ,           
          i  =>  pseudo_orbit       
         );                                               

ttc_bcr_gen: for i in 0 to 3 generate
     bcr: OBUFDS       
        port map(       
          o  =>  ttc_BCR_p(i),      --! ttc bcr fanout to the process fpgas
          ob =>  ttc_BCR_n(i) ,           
          i  =>  ttc_BCR(i)       
         );           

     end generate;

ttc_L1A_gen: for i in 0 to 3 generate
     L1A: OBUFDS       
        port map(       
          o  =>  ttc_L1A_p(i),      --! ttc L1A fanout to the process fpgas
          ob =>  ttc_L1A_n(i) ,           
          i  =>  ttc_L1A(i)       
         );           

     end generate;


ttc_ecr_gen: for i in 0 to 3 generate
     ecr: OBUFDS       
        port map(       
          o  =>  ttc_ECR_p(i),      --! ttc ecr fanout to the process fpgas
          ob =>  ttc_ECR_n(i) ,           
          i  =>  ttc_ECR(i)       
         );           

         end generate;

ttc_pr_rdout_gen: for i in 0 to 3 generate
     pr_rd: OBUFDS       
        port map(       
          o  =>  ttc_pr_rdout_p(i),      --! ttc privilage read out fanout to the process fpgas
          ob =>  ttc_pr_rdout_n(i) ,           
          i  =>  ttc_pr_rdout(i)       
         );           

         end generate;

-------------------------------------------------------------
-- i2c bus settings
------------------------------------------------------------
i2c_scl_0 <= scl_o_0 when (scl_enb_0 = '0') else 'Z';
i2c_sda_0 <= sda_o_0 when (sda_enb_0 = '0') else 'Z';
i2c_scl_1 <= scl_o_1 when (scl_enb_1 = '0') else 'Z';
i2c_sda_1 <= sda_o_1 when (sda_enb_1 = '0') else 'Z';
i2c_rst_0 <= '1'; -- This will be assigned active low reset signal 
i2c_rst_1 <= '1'; --This will be assigned active low reset signal 
--------------------------------------------------------------------
phy_rstb <= not rst_ipb ;
 
leds(0)   <= clk_locked  and onehz;   
leds(1)   <= pll_lock_1 or pll_lock_2 or pll_lock_3 ;

mac_addr <= X"424c31433" & hardware_addr; --  424c3143 & 4 bits module type & 4 bits shelf address & 8 bits slot address ---Careful here, arbitrary addresses do not always work
ip_addr  <= X"c0a80"& hardware_addr(11 downto 8) & "0000" & hardware_addr( 3 downto 0) ;  -- 192.168. shelf(0-1).slot(0-16)

flash_csn <= f5_flash_csn_int;   
tx_disable <= '0' ;   

  -- control fpga
  --  moddule status assignement
  
  hw_id       <= x"00000efe";
  fpga_id     <= x"00000005";
  hw_position <= x"000000" & hardware_addr( 7 downto 0);
  fw_rev      <= x"000000" & sof_raw_fpga4 & sof_raw_fpga3 & sof_raw_fpga2 & sof_raw_fpga1 & sof_topo_fpga4 & sof_topo_fpga3 & sof_topo_fpga2 & sof_topo_fpga1;
  fw_tag      <= topo_data_fpga1; --GLOBAL_TIME;
  datafmt_rev <= rx_er_count;
  status      <= x"0000000"  & '0' & pll_lock_3 & pll_lock_2 & pll_lock_1 ;
      
      
trigger_reconfig <= reconfig  and clk_locked; -- if active it will reconfigure the FPGA5       
reconfig <= reconfig_reg(30);-- if active it will reconfigure the spi flash.      
xtal_ttc_clk_sel   <= control_reg(0);  -- If low it selects local crystal clcok of 40.08MHz. If high it selects  LHC TTC clock
enable_pll_rst     <= control_reg(6); -- This will generate active low reset pulse that will synchronous the PLLs     
f5_flash_disable <=  '0';
POWERDN_B_CDCE <= not  control_reg(2); --'1';
--SYNC_B_CDCE    <= '1';
flash_hold <= '1'; -- This will make the spi flash not to go in hold mode
flash_wp   <= '1'; -- this will aviod the spi flash to go in write protect mode 
flash_addr       <="000";--flash_addr_int;
f1_flash_sel     <= '0';--f1_flash_sel_int;
f2_flash_sel     <= '0';--f2_flash_sel_int;
f3_flash_sel     <= '0';--f3_flash_sel_int;
f4_flash_sel     <= '0';--f4_flash_sel_int;
flash_mosi       <= flash_mosi_int;
start            <=  control_reg(1); -- will start the synchronisation of incoming ttc information data with 40 MHz



--global_ctrl_fabric:entity ipbus_lib.ipbus_fabric_sel
--    generic map(NSLV => N_SLAVES,
--	             SEL_WIDTH => ipbus_sel_width)
--          port map(
--             ipb_in  => ipb_master_out,
--             ipb_out => ipb_master_in,
--             sel     => ipbus_sel_L1CaloEfexControl(ipb_out.ipb_addr),
--             ipb_to_slaves => ipbw,
--             ipb_from_slaves => ipbr
--           ); 
            
 
---------------------------------------------------------------------------------
 ----- common id registers slave
 ---------------------------------------------------------------------------------------
           
--common_reg: entity infrastructure_lib.common_id_registers
--         --generic map(addr_width => 3)    
--          port map (
--               ipb_clk      => ipb_clk,  
--               ipb_rst      => rst_ipb,  
--               ipb_in       => ipbw(N_SLV_COMMON_IDVERSION),
--               ipb_out      => ipbr(N_SLV_COMMON_IDVERSION),
--               Module_ID    => x"00000efe",
--               xml_version  => XML_VERSION,
--               xml_Gitsha   => XML_HASH,   
--               fw_version   => GLOBAL_VER,
--               fw_Gitsha    => GLOBAL_SHA,
--               build_date   => GLOBAL_DATE ,
--               build_time   => GLOBAL_TIME 
--                           );
                                             

--- this is for making sure the gmii_rx_clk goes through a mmc 
 clk_rx : clk_wiz_0
           port map ( 
        
           -- Clock in ports
           clk_in1 => gmii_rx_clk,
           clkfb_in => clkfb_in,
          -- Clock out ports  
           clk_out1 => gmii_rx_clk_int,
           clkfb_out => clkfb_out,
          -- Status and control signals                
           reset => '0',
           locked => open           
         );
         
          BUFG_inst : BUFG
           port map (
              O => clkfb_in, -- 1-bit output: Clock output
              I => clkfb_out -- 1-bit input: Clock input
           );
 
 
 
 
   U_0 : entity infrastructure_lib.top_udp_config_FPGA 
      PORT MAP (
         ip_addr          => ip_addr,
         ipb_clk          => ipb_clk,
         ipb_in           => ipb_master_in,
         mac_addr         => mac_addr,
         mac_clk          => mac_clk,
         mac_rx_data      => mac_rx_data,
         mac_rx_error     => mac_rx_error,
         mac_rx_last      => mac_rx_last,
         mac_rx_valid     => mac_rx_valid,
         mac_tx_ready     => mac_tx_ready,
         master_tx_data1  => master_tx_data1_int, 
         master_tx_data2  => master_tx_data2_int,
         master_tx_data3  => master_tx_data3_int,
         master_tx_data4  => master_tx_data4_int,
         master_tx_err1   => master_tx_err1,
         master_tx_err2   => master_tx_err2,
         master_tx_err3   => master_tx_err3,
         master_tx_err4   => master_tx_err4,
         rst_ipb          => rst_ipb_ctrl,
         rst_macclk       => rst_macclk,
         ipb_out          => ipb_master_out,
         mac_tx_data      => mac_tx_data,
         mac_tx_error     => mac_tx_error,
         mac_tx_last      => mac_tx_last,
         mac_tx_valid     => mac_tx_valid,
         master_rx_data1  => master_rx_data1_int,
         master_rx_data2  => master_rx_data2_int,
         master_rx_data3  => master_rx_data3_int,
         master_rx_data4  => master_rx_data4_int,
         master_tx_pause1 => master_tx_pause1,
         master_tx_pause2 => master_tx_pause2,
         master_tx_pause3 => master_tx_pause3,
         master_tx_pause4 => master_tx_pause4,
         master_link_down1=> '0' , --master_link_down1,
         master_link_down2=> '0' , --master_link_down2,
         master_link_down3=> '0' , --master_link_down3,
         master_link_down4=> '0'  --master_link_down4
      );
	  
	  
	------- -- Interconnection between control FPGA and Process FPGA1------------------ 

   U_1 : entity infrastructure_lib.interconnect
      PORT MAP (
         mac_clk         => mac_clk,
         master_rx_data  => master_tx_data1, -- coming from FPGA1
         rst_macclk      => rst_macclk,
         tx_data         => master_rx_data1_int, 
         master_rx_err   => master_tx_err1,  -- is set high if data has parity error
         process_tx_data => master_rx_data1, -- goes to FPGA1
         rx_data         => master_tx_data1_int -- goes to top_udp_config block
      );  
	  
	------- -- Interconnection between control FPGA and Process FPGA2------------------       
   U_2 : entity infrastructure_lib.interconnect
      PORT MAP (
         mac_clk         => mac_clk,
         master_rx_data  => master_tx_data2,  -- coming from FPGA2
         rst_macclk      => rst_macclk,
         tx_data         => master_rx_data2_int,
         master_rx_err   => master_tx_err2,  -- is set high if data has parity error
         process_tx_data => master_rx_data2,  -- goes to FPGA2
         rx_data         => master_tx_data2_int -- goes to top_udp_config block
      );

------- -- Interconnection between control FPGA and Process FPGA3------------------   
    
   U_3 : entity infrastructure_lib.interconnect
      PORT MAP (
         mac_clk         => mac_clk,
         master_rx_data  => master_tx_data3,   -- coming from FPGA3
         rst_macclk      => rst_macclk,
         tx_data         => master_rx_data3_int,
         master_rx_err   => master_tx_err3,   -- is set high if data has parity error
         process_tx_data => master_rx_data3,  -- goes to FPGA3
         rx_data         => master_tx_data3_int  -- goes to top_udp_config block
      );
 ------- -- Interconnection between control FPGA and Process FPGA4------------------      
      
   U_4 : entity infrastructure_lib.interconnect
      PORT MAP (
         mac_clk         => mac_clk,
         master_rx_data  => master_tx_data4,   -- coming from FPGA4
         rst_macclk      => rst_macclk,
         tx_data         => master_rx_data4_int,
         master_rx_err   => master_tx_err4, -- is set high if data has parity error
         process_tx_data => master_rx_data4, -- goes to FPGA4
         rx_data         => master_tx_data4_int  -- goes to top_udp_config block
      );   
	  
	--	DCM clock generation for internal bus, ethernet 
	clocks: entity infrastructure_lib.clocks_7s_extphy
            port map(
                sysclk_p      => sysclk_p,
                sysclk_n      => sysclk_n,
                clko_125      => mac_clk, 
                clko_200      => clk200,
                clko_ipb      => ipb_clk,
                locked        => clk_locked,
                nuke          => nuke,
                soft_rst      => soft_rst,
                rsto_125      => rst_macclk,
                rsto_ipb      => rst_ipb,
                rsto_ipb_ctrl => rst_ipb_ctrl,
                onehz         => onehz
            );
	
	
	

-- --- Ethernet MAC core and PHY interface   
   eth: entity infrastructure_lib.eth_7s_gmii
		port map(
			clk125       => mac_clk,
			clk200       => clk200,
			rst          => rst_macclk,
			gmii_gtx_clk => gmii_gtx_clk,
			gmii_txd     => gmii_txd,
			gmii_tx_en   => gmii_tx_en,
			gmii_tx_er   => gmii_tx_er,
			gmii_rx_clk  => gmii_rx_clk_int,
			gmii_rxd     => gmii_rxd,
			gmii_rx_dv   => gmii_rx_dv,
			gmii_rx_er   => gmii_rx_er,
			tx_data      => mac_tx_data,
			tx_valid     => mac_tx_valid,
			tx_last      => mac_tx_last,
			tx_error     => mac_tx_error,
			tx_ready     => mac_tx_ready,
			rx_data      => mac_rx_data,
			rx_valid     => mac_rx_valid,
			rx_last      => mac_rx_last,
			rx_error     => mac_rx_error
			
		);
  
--  process(gmii_rx_clk_int)
--  begin
--        if gmii_rx_clk_int' event and gmii_rx_clk_int ='1' then
--           gmii_rx_er_i <= gmii_rx_er;
--         end if;
--  end process;
                
--rx_er_cntr: entity infrastructure_lib.counter 
--                Port map(  
--                     clk       => gmii_rx_clk_int, 
--                     resetdone => '1',
--                     enable    => gmii_rx_er_i ,  --- rx errors
--                     count     => rx_er_count,
--                     reset     =>  control_reg(3) 
                    
--                      );          
--  -----------------------------------
 --- control fpga mgts. 
 ---------------------------------------------------------------
 
 RXN_IN_i <= "00" & RXN_IN ;
 RXP_IN_i <= "00" & RXP_IN ;
 
   MGT_TX_RX : entity infrastructure_lib.top_mgt_cfpga
       port map (
     
           clk40                    =>  clk40, 
           start                    =>  start,
           clk160                   => clk160,
           rx_clk160                =>  rx_clk160,                               
           Q_CLK_GTREFCLK_PAD_N_IN  =>  Q_CLK_GTREFCLK_PAD_N_IN , -- (1 downto 0), 
           Q_CLK_GTREFCLK_PAD_P_IN  =>  Q_CLK_GTREFCLK_PAD_P_IN, -- (1 downto 0), 
           RXN_IN                   =>  RXN_IN_i, 
           RXP_IN                   =>  RXP_IN_i, 
           TXN_OUT                  =>  TXN_OUT, 
           TXP_OUT                  =>  TXP_OUT, 
           rx_resetdone             => rx_resetdone    ,
           rx_fsm_resetdone         => rx_fsm_resetdone,
           rx_byteisaligned         => rx_byteisaligned,
           tx_resetdone             => tx_resetdone    ,
           tx_fsm_resetdone         => tx_fsm_resetdone,
           tx_bufstatus             => tx_bufstatus    ,
           rx_realign               => rx_realign      ,
           rx_disperr               => rx_disperr      ,
           encode_error             => encode_error,
           mgt_commadret            => mgt_commadret,
           loopback                 => loopback,             
           mgt_SOFT_RESET_TX_IN     => softreset_tx ,
           mgt_SOFT_RESET_RX_IN     => softreset_rx,             
           topo_data_fpga1          =>  topo_data_fpga1 , -- topo data from fpga1 through mgt 115 gt_rx0
           topo_data_fpga2          =>  topo_data_fpga2 , -- topo data from fpga2 through mgt 115 gt_rx2
           --topo_data_fpga3          =>  topo_data_fpga3 , -- topo data from fpga3 through mgt 116 gt_rx0
           --topo_data_fpga4          =>  topo_data_fpga4 , -- topo data from fpga4 through mgt 116 gt_rx2
           raw_data_fpga1           =>  raw_data_fpga1  , -- raw data from fpga1 through mgt 115 gt_rx1 
           raw_data_fpga2           =>  raw_data_fpga2  , -- raw data from fpga2 through mgt 115 gt_rx3 
           --raw_data_fpga3           =>  raw_data_fpga3  , -- raw data from fpga3 through mgt 116 gt_rx1 
           --raw_data_fpga4           =>  raw_data_fpga4  , -- raw data from fpga4 through mgt 116 gt_rx3
           ttc_rx_data              =>  ttc_rx_data  ,     -- ttc information data from hub1 through mgt114 gt_rx0
           rod_busy                 =>  ttc_data_hub2 --rod_busy           -- rod busy data from hub2 through mgt114 gt_rx1
           
            
         );


-----------------------------------------------------------------------------------
--- data buffer  raw data fpga 1
--buffer_0:entity infrastructure_lib.data_buffer
--  Port map  ( 
--      rx_clk   => rx_clk160(0) ,
--      clk160   => clk160 ,
--      reset    => reset, 
--      pe       => '0', --pe,
--      rxdata   => raw_data_fpga1,
--      data_buff=> raw_fpga1,
--      rd_en    => rd_raw_fpga1,
--      empty    => fifo_empty,
--      valid    => valid_raw_fpga1
--      );
      

      
      
      
      
      
      
      
      
      
----- data buffer  topo data fpga 1
--buffer_1:entity infrastructure_lib.data_buffer
--  Port map  ( 
--      rx_clk   => rx_clk160(1) ,
--      clk160   => clk160 ,
--      reset    => reset, 
--      pe       => '0', --pe,
--      rxdata   => topo_data_fpga1,
--      data_buff=> topo_fpga1,
--      rd_en    => rd_topo_fpga1,
--      valid    => valid_topo_fpga1
--      );
            
----- data buffer  raw data fpga 1
--buffer_2:entity infrastructure_lib.data_buffer
--  Port map  ( 
--      rx_clk   => rx_clk160(2) ,
--      clk160   => clk160 ,
--      reset    => reset, 
--      pe       => '0', --pe,
--      rxdata   => raw_data_fpga2,
--      data_buff=> raw_fpga2,
--      rd_en    => rd_raw_fpga2,
--      valid    => valid_raw_fpga2
--      );
            
----- data buffer  topo data fpga 1
--buffer_3:entity infrastructure_lib.data_buffer
--  Port map  ( 
--      rx_clk   => rx_clk160(3) ,
--      clk160   => clk160 ,
--      reset    => reset, 
--      pe       => '0', --pe,
--      rxdata   => topo_data_fpga2,
--      data_buff=> topo_fpga2,
--      rd_en    => rd_topo_fpga2,
--      valid    => valid_topo_fpga2
--      );          
       
  
  
  
--------------------------------------------------------------------------------
-- place holder
----------------------------------------------------------------------------------------
--topofpga1:process (rx_clk160(0))
--       begin
       
--       if rx_clk160(0)' event and rx_clk160(0) ='1' then
--          if topo_data_fpga1(7 downto 0) = x"3c" then --k28.1
--             sof_topo_fpga1 <= '1' ;
--          end if;
--          end if;
--          end process;           
           

--rawfpga1:process (rx_clk160(1))
--       begin       
--       if rx_clk160(1)' event and rx_clk160(1) ='1' then
--          if raw_data_fpga1(7 downto 0) = x"3c" then --k28.1
--              sof_raw_fpga1 <= '1' ;
--           end if;
--           end if; 
--           end process;  

--topofpga2:process (rx_clk160(2))
--       begin
       
--       if rx_clk160(2)' event and rx_clk160(2) ='1' then
--          if topo_data_fpga2(7 downto 0) = x"3c" then --k28.1
--             sof_topo_fpga2 <= '1' ;
--          end if;
--          end if;
--          end process;           
           

--rawfpga2:process (rx_clk160(3))
--       begin       
--       if rx_clk160(3)' event and rx_clk160(3) ='1' then
--          if raw_data_fpga2(7 downto 0) = x"3c" then --k28.1
--              sof_raw_fpga2 <= '1' ;
--           end if;
--           end if; 
--           end process;  

--topofpga3:process (rx_clk160(4))
--       begin
       
--       if rx_clk160(4)' event and rx_clk160(4) ='1' then
--          if topo_data_fpga3(7 downto 0) = x"3c" then --k28.1
--             sof_topo_fpga3 <= '1' ;
--          end if;
--          end if;
--          end process;           
           

--rawfpga3:process (rx_clk160(5))
--       begin       
--       if rx_clk160(5)' event and rx_clk160(5) ='1' then
--          if raw_data_fpga3(7 downto 0) = x"3c" then --k28.1
--              sof_raw_fpga3 <= '1' ;
--           end if;
--           end if; 
--           end process;  

--topofpga4:process (rx_clk160(6))
--       begin
       
--       if rx_clk160(6)' event and rx_clk160(6) ='1' then
--          if topo_data_fpga4(7 downto 0) = x"3c" then --k28.1
--             sof_topo_fpga4 <= '1' ;
--          end if;
--          end if;
--          end process;           
           

--rawfpga4:process (rx_clk160(7))
--       begin       
--       if rx_clk160(7)' event and rx_clk160(7) ='1' then
--          if raw_data_fpga4(7 downto 0) = x"3c" then --k28.1
--              sof_raw_fpga4 <= '1' ;
--           end if;
--           end if; 
--           end process;  




-------------------------------------------------------------------------
--- synchtonisation block
----------------------------------------------------------------------           
           
synch_ttc_combined: entity infrastructure_lib.top_cntrl_synch                                                                   
                                                                                           
port map (                                                                                     
      rx_clk160        => rx_clk160(4),   --8  -- rx clock of the mgt114
      TTC_clk          => clk40,            -- ttc clock of 40 MHz
      reset            => reset,
      enable_mgt       => mgt_enable(0),    -- enaable of the incoming rx data 
      MGT_Commadet     => mgt_commadret(4), --8 -- comma detected for incoming data
      reg_sel          => bc_reg_sel(3 downto 0),       -- setting BC mux              --
      mux_sel          => mux_sel(3 downto 0),          -- setting the first stage mux
      delay_num        => delay_cntr_0,    -- delay count of first stage    
      start            => start,            -- start pulse for the calibration to start
      rx_resetdone     => rx_resetdone(0),                                                   
      reg128_latch     => reg128_latch,                                                     
      --data_out_reg128  => ttc_sync_data,    -- ttc_information synch goes to process fpgasdata_out_reg128,                        
      data_out         => ttc_sync_data_i,    -- ttc_information synch goes to process fpgasdata_out_reg128, ---data_out_reg128,                             
      data_in          => ttc_rx_data        -- in coming ttc information data                        
  );                                                                                                  
           
     process(clk40)
     
       begin     
            if clk40' event and clk40 ='1' then
                ttc_sync_data <= ttc_sync_data_i;
               end if;
           end process;    
                 
         
           
 --- ttc information signal selection
ttc_L1A       <=   ttc_sync_data(16) & ttc_sync_data(16) & ttc_sync_data(16) & ttc_sync_data(16) ;   -- L1 accept to indicate when an event has been accepted  
ttc_BCR       <=   ttc_sync_data(17) & ttc_sync_data(17) & ttc_sync_data(17) & ttc_sync_data(17) ;   -- bunch counter reset. used to reset local bc counters    
ttc_ECR       <=   ttc_sync_data(18) & ttc_sync_data(18) & ttc_sync_data(18) & ttc_sync_data(18) ;   -- event counter reset    
ttc_pr_rdout  <=   ttc_sync_data(19) & ttc_sync_data(19) & ttc_sync_data(19) & ttc_sync_data(19) ;   -- indicate full data readout has been requested  
link_reset    <=   ttc_sync_data(96);    
   
---------------------------------------------------------------------------------------------------------------------------------------------
----readout_ctrl synchronisation
--------------------------------------------------------------------------------------------      


synch_readout_ctrl: entity infrastructure_lib.top_cntrl_synch                                                                   
                                                                                           
port map (                                                                                     
      rx_clk160        => rx_clk160(5),    --9 -- rx clock of the mgt114
      TTC_clk          => clk40,            -- ttc clock of 40 MHz
      reset            => reset,
      enable_mgt       => mgt_enable(1),    -- enaable of the incoming rx data 
      MGT_Commadet     => mgt_commadret(5), --9 comma detected for incoming data
      reg_sel          => bc_reg_sel(7 downto 4),       -- setting BC mux              --
      mux_sel          => mux_sel(7 downto 4),          -- setting the first stage mux
      delay_num        => delay_cntr_1,    -- delay count of first stage    
      start            => start,            -- start pulse for the calibration to start
      rx_resetdone     => rx_resetdone(1),                                                   
      reg128_latch     => readout_ctrl_latch,                                                     
      --data_out_reg128  => ttc_sync_data,    -- ttc_information synch goes to process fpgasdata_out_reg128,                        
      data_out         => readout_ctrl_i,    -- ttc_information synch goes to process fpgasdata_out_reg128, ---data_out_reg128,                             
      data_in          =>  ttc_data_hub2 --rod_busy        -- in coming ttc information data                        
  );                                                        
      
      
      process(clk40)
       
         begin     
              if clk40' event and clk40 ='1' then
                  readout_ctrl <= readout_ctrl_i;
                 end if;
             end process;    
             
   --- ttc information signal selection
    ttc_L1A_hub2       <=   readout_ctrl(16) & readout_ctrl(16) & readout_ctrl(16) & readout_ctrl(16) ;   -- L1 accept to indicate when an event has been accepted  
    ttc_BCR_hub2       <=   readout_ctrl(17) & readout_ctrl(17) & readout_ctrl(17) & readout_ctrl(17) ;   -- bunch counter reset. used to reset local bc counters    
    ttc_ECR_hub2       <=   readout_ctrl(18) & readout_ctrl(18) & readout_ctrl(18) & readout_ctrl(18) ;   -- event counter reset    
    ttc_pr_rdout_hub2  <=   readout_ctrl(19) & readout_ctrl(19) & readout_ctrl(19) & readout_ctrl(19) ;   -- indicate full data readout has been requested  
    link_reset_hub2    <=   readout_ctrl(96); 
    
    
               
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
 --aurora design
--------------------------------------------------------------------------------------------------------------------------------------------------------------------- top_aurora
-- top_aurora:entity infrastructure_lib.efex_aurora 
                                                  
--  port map (                                                                
--         -- TX Stream Interface                                         
--          s_axi_tx_tdata      =>  s_axi_tx_tdata ,  
--          s_axi_tx_tvalid     =>  s_axi_tx_tvalid,  
--          s_axi_tx_tready     =>  s_axi_tx_tready,  
--          s_axi_tx_tkeep      =>  s_axi_tx_tkeep ,  
--          s_axi_tx_tlast      =>  s_axi_tx_tlast ,  
--         -- User Flow Control TX Interface                              
--          s_axi_ufc_tx_req    =>  s_axi_ufc_tx_req,    
--          s_axi_ufc_tx_ms     =>  s_axi_ufc_tx_ms ,   
--          s_axi_ufc_tx_ack    =>  s_axi_ufc_tx_ack,   
--         -- V7 Serial I/O             
--          txp                 =>  aurora_txp,       
--          txn                 =>  aurora_txn,       
--         -- GT Reference Clock Interface     
                                    
--          gt_refclk1_p        =>  aurora_refclk1_p,                          
--          gt_refclk1_n        =>  aurora_refclk1_n,                         
--         -- Error Detection Interface                                   
--          tx_hard_err         =>  tx_hard_err,                         
--         -- Status                                                      
--          tx_channel_up       =>  tx_channel_up,         
--          tx_lane_up          =>  tx_lane_up,            
--         -- System Interface                                            
--          user_clk_out        =>  aurora_user_clk  ,                        
--          sys_reset_out       =>  sys_reset_out  ,                        
--          tx_lock             =>  tx_lock        ,                        
--          init_clk            =>  mac_clk      ,                        
--          init_clk_out        =>  init_clk_out   ,                       
--          pll_not_locked      =>  pll_not_locked ,                        
--          tx_resetdone        =>  aurora_tx_resetdone   ,                        
--          link_reset          =>  link_reset                             
--         );                                                               
                                                                        
---- aurora_status <=  x"00000" &  "000" & aurora_tx_resetdone  & tx_lock & pll_not_locked & tx_hard_err & tx_channel_up & tx_lane_up; 
 
-- playout_memory : entity infrastructure_lib.playout_rom
----      generic
----   (
----    DATA_WIDTH  : integer :=   64;      -- DATA bus width
----    ADDR_WIDTH  : integer :=   5;       -- width of rom address bus
----    GAP_WIDTH   : integer :=   8;       -- width of gap counter ex; 5 makes gap of 32 cycles 
----    REM_WIDTH   : integer :=   1        -- REM bus width
----   );  
    
--    Port Map ( 
--           clock                        => aurora_user_clk,
--           reset                        => sys_reset_out,
--           AXI4_M_TDATA                 => s_axi_tx_tdata ,
--           AXI4_M_TKEEP                 => s_axi_tx_tkeep,
--           AXI4_M_TVALID                => s_axi_tx_tvalid,
--           AXI4_M_TLAST                 => s_axi_tx_tlast,
--           AXI4_M_TREADY                => s_axi_tx_tready
   
--       ); 
       
       
--------------------------------------------------------------------------------------------------------------------------------------------
-- Aurora hub2 connections    
--------------------------------------------------------------------------------------------------------------------------------------
top_aurora_hub2:entity infrastructure_lib.aurora_hub2 
                                                  
  port map (                                                                
         -- TX Stream Interface                                         
          s_axi_tx_tdata      =>  s_axi_tx_tdata_hub2 ,  
          s_axi_tx_tvalid     =>  s_axi_tx_tvalid_hub2,  
          s_axi_tx_tready     =>  s_axi_tx_tready_hub2,  
          s_axi_tx_tkeep      =>  s_axi_tx_tkeep_hub2 ,  
          s_axi_tx_tlast      =>  s_axi_tx_tlast_hub2 ,  
         -- User Flow Control TX Interface                              
          s_axi_ufc_tx_req    =>  s_axi_ufc_tx_req_hub2,    
          s_axi_ufc_tx_ms     =>  s_axi_ufc_tx_ms_hub2 ,   
          s_axi_ufc_tx_ack    =>  s_axi_ufc_tx_ack_hub2,   
         -- V7 Serial I/O             
          txp                 =>  aurora_hub2_txp,       
          txn                 =>  aurora_hub2_txn,       
         -- GT Reference Clock Interface      
                                  
          gt_refclk1_p        =>  aurora_refclk1_p,                          
          gt_refclk1_n        =>  aurora_refclk1_n,                        
         -- Error Detection Interface                                   
          tx_hard_err         =>  tx_hard_err_hub2,                         
         -- Status                                                      
          tx_channel_up       =>  tx_channel_up_hub2,         
          tx_lane_up          =>  tx_lane_up_hub2,            
         -- System Interface                                            
          user_clk_out        =>  aurora_user_clk_hub2  ,                        
          sys_reset_out       =>  sys_reset_out_hub2  ,                        
          tx_lock             =>  tx_lock_hub2        ,                        
          init_clk            =>  mac_clk      ,                        
          init_clk_out        =>  init_clk_out_hub2   ,                       
          pll_not_locked      =>  pll_not_locked_hub2 ,                        
          tx_resetdone        =>  aurora_tx_resetdone_hub2   ,                        
          link_reset          =>  link_reset_hub2                             
         );                                                               
                                                                        
 aurora_status <=  x"00000" & "000" & aurora_tx_resetdone_hub2  & tx_lock_hub2  & pll_not_locked_hub2  & tx_hard_err_hub2  & tx_channel_up_hub2  & tx_lane_up_hub2 ; 
 
-- playout_memory_hub2 : entity infrastructure_lib.playout_rom
----      generic
----   (
----    DATA_WIDTH  : integer :=   64;      -- DATA bus width
----    ADDR_WIDTH  : integer :=   5;       -- width of rom address bus
----    GAP_WIDTH   : integer :=   8;       -- width of gap counter ex; 5 makes gap of 32 cycles 
----    REM_WIDTH   : integer :=   1        -- REM bus width
----   );  
    
--    Port Map ( 
--           clock                        => aurora_user_clk_hub2,
--           reset                        => sys_reset_out_hub2,
--           AXI4_M_TDATA                 => payload_data  ,
--           AXI4_M_TKEEP                 => s_axi_tx_tkeep_hub2 ,
--           AXI4_M_TVALID                => payload_valid,
--           AXI4_M_TLAST                 => payload_last  ,
--           AXI4_M_TREADY                => tready_data ,
--           first                        => first 
   
--       );    
       
  
  buffer_0:entity infrastructure_lib.data_buffer
         Port map  ( 
             rx_clk   => rx_clk160(0) ,
             clk160   => clk160 ,
             reset    => reset, 
             pe       => '0', --pe,
             rxdata   => raw_data_fpga1,
             data_buff=> raw_fpga1,
             rd_en    => rd_raw_fpga1,
             empty    => fifo_empty,
             valid    => valid_raw_fpga1
             );
             
 rdout:entity infrastructure_lib.rdout_data_process 
             port map( 
                  clk           =>  clk160,
                  fifo_empty    =>  fifo_empty,
                  reset         =>  reset,
                  rxdata_fifo   =>  raw_fpga1(63 downto 0 ),
                  tready_data   =>  tready_data     ,
                  valid         =>  valid            ,
                  aurora_tready =>  open  ,
                  first         =>  first            ,
                  payload_data  =>  payload_data     ,
                  payload_last  =>  payload_last     ,
                  payload_valid =>  payload_valid    ,
                  rd_fifo       =>  rd_raw_fpga1         
                );     
         
 crc_cfpa_readout: entity work.readout_cfpga
       Port map( 
              clk                 =>   clk160, --aurora_user_clk_hub2,    
              reset               =>   sys_reset_out_hub2,                   
              ---aurora                              
              aurora_tready       =>   s_axi_tx_tready_hub2       ,
              axi4_m_data         =>   s_axi_tx_tdata_hub2  ,
              axi4_m_tlast        =>   s_axi_tx_tlast_hub2  ,
              axi4_m_tvalid       =>   s_axi_tx_tvalid_hub2 ,
              --readout           =>                      
              first               =>  first,
              payload_data        =>  payload_data,  
              payload_valid       =>  payload_valid ,
              payload_last        =>  payload_last ,
              tready_data         =>  tready_data  
              
              );
 
 
 
 
 
 
 
 
 
 
 
 

 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
       -- ipbus slaves live in the entity below, and can expose top-level ports
-- The ipbus fabric is instantiated within.
--------------------------------------------------------------------------------------------------------
slaves: entity infrastructure_lib.slaves 
	  port map(
		ipb_clk      => ipb_clk,
		ipb_rst      => rst_ipb,
		ipb_in       => ipb_master_out, ---ipbw(N_SLV_COMMON_CONTROL),
		ipb_out      => ipb_master_in,  ---ipbr(N_SLV_COMMON_CONTROL),
		hw_id        => hw_id, 
		hw_position  => hw_position ,
        fpga_id      => fpga_id ,
        fw_rev       => fw_rev,
        fw_tag       => fw_tag,
        datafmt_rev  => datafmt_rev,
        status       => status, 
		control_reg  => control_reg,
		pulse_reg    => pulse_reg ,
		reconfig_reg => reconfig_reg,
		scl_i_0      => i2c_scl_0,
		scl_o_0      => scl_o_0,
		scl_enb_0    => scl_enb_0,		
		sda_i_0      => i2c_sda_0,
		sda_o_0      => sda_o_0,
		sda_enb_0    => sda_enb_0,					
		scl_i_1      => i2c_scl_1,
        scl_o_1      => scl_o_1,
        scl_enb_1    => scl_enb_1,        
        sda_i_1      => i2c_sda_1,
        sda_o_1      => sda_o_1,       
        sda_enb_1    => sda_enb_1,        
		pll_clko     => pll_clko ,
		pll_le       => pll_en ,
		pll_miso     => pll_miso,
		pll_mosi     =>  pll_mosi,
		pll_select   => select_pll,
		flash_select => select_flash,
		flash_miso   => flash_miso,
        flash_le     => flash_csn_int,
        flash_clko   => flash_clk,
        flash_mosi   => flash_mosi_int,
        -------mgt slaves
        clk160        => rx_clk160, 
        loopback      => loopback,  
        mux_sel       =>  mux_sel,  
        delay_cntr_0  =>  delay_cntr_0,   
        delay_cntr_1  =>  delay_cntr_1 ,  
        softreset_tx  =>  softreset_tx,
        softreset_rx  =>  softreset_rx, 
        mgt_enable    =>  mgt_enable, 
        qpll_lock     =>   "11", --111
        qpll_refclklost  => "00",--000
        rx_resetdone     => rx_resetdone, 
        rx_fsm_resetdone => rx_fsm_resetdone,
        rx_byteisaligned => rx_byteisaligned,     
        tx_resetdone     => tx_resetdone,        
        tx_fsm_resetdone => tx_fsm_resetdone,
        tx_bufstatus     => (others=> '0'),
        rx_realign       => rx_realign ,   
        rx_disperr       => rx_disperr ,    
        encode_error     => encode_error,
        aurora_status    => aurora_status 
                                                    
        
        
        
        
	);

----------------------------------------------------
--- pll select and flash
------------------------------------------------------

pll_sel: entity infrastructure_lib.pll_selector
          port map
          (
             sel     => select_pll(1 downto 0),
             pll_en_1 => pll_le_1,
             pll_en_2 => pll_le_2,
             pll_en_3 => pll_le_3,
             pll_en   =>  pll_en 
              
            );               
  
 flash_sel  : entity infrastructure_lib.flash_selector
                     port map
                     (
                        sel         => select_flash ,                      
                        f5_flash_cs =>  f5_flash_csn_int,
                        f2_flash_cs =>  f2_flash_sel_int,
                        f3_flash_cs =>  f3_flash_sel_int,
                        f4_flash_cs =>  f4_flash_sel_int,
                        flash_cs   => flash_csn_int
                        
                        
                        ) ;
  
      
   
   cclk_o: entity infrastructure_lib.startup
   port map(
      flash_cclk => flash_clk
      );

------------------------------------------------------------
---- generate config pulse
--------------------------------------------------------------
configure: entity infrastructure_lib.self_configure
    port map(
    clk => ipb_clk,--icap_clk,
    led_clk => onehz,
    reset => rst_ipb,
    trigger => trigger_reconfig,
    indicator => flash_led
    );
-------------------------------------------------------    
--- clock generation of 40M,160M and 280M from the TTC 
------------------------------------------------------
ttc_clk : clk_ttc
   port map ( 
  -- Clock out ports  
   clk40 => clk40,
   clk280 => clk280,
   clk160 => clk160,
  -- Status and control signals                
   locked => locked,
   -- Clock in ports
   clk_in1_p => clk_40_p,
   clk_in1_n => clk_40_n
);
reset <= not locked;

reset_pll: entity infrastructure_lib.nreset_pll
     port map(
         clk40            => clk40 , 
         enable_pll_rst   => enable_pll_rst ,
         rst_ipb          => rst_ipb,
         SYNC_B_CDCE      =>  SYNC_B_CDCE
         
         );
                       



END Spec;
