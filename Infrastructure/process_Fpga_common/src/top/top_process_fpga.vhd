--! @file
--! @brief Top of the process FPGA
--! @details
--! \verbatim
--! Top module of eFEX process FPGA
--!
--! TOB Type Definition:
--!    TOB Type = 0 for FPGA 1 to transmit e/g TOBs
--!    TOB Type = 1 for FPGA 2 to transmit tau TOBs
--!
--! \endverbatim
--! @author Mohammed Syiad
--! @author Francesco Gonnella
--! @author Saeed Taghavi

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
library unisim;
use unisim.VComponents.all;

library algolib;
use algolib.AlgoDataTypes.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;

library infrastructure_lib;
use infrastructure_lib.all;
use infrastructure_lib.ipbus_decode_L1CaloEfexProcessor.all;
use infrastructure_lib.synch_type.all;
use infrastructure_lib.EfexDataFormats.all;
use infrastructure_lib.mgt_type.all;
use infrastructure_lib.ProcessorFPGAPackage.all;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.all;
use tob_rdout_lib.data_type_pkg.all;
use tob_rdout_lib.tob_rdout_comp_pkg.all;



--! @copydoc top_process_fpga.vhd
entity top_process_fpga is
  generic(
    --! Integer used to distinguish different FPGAs having a slightly different firmware
    FLAVOUR     : integer                       := 0;
    --! Date format DDMMYYYY in decimal
    GLOBAL_DATE : std_logic_vector(31 downto 0) := x"00000000";
    --! Time format  00HHMMSS in decimal
    GLOBAL_TIME : std_logic_vector(31 downto 0) := x"00000000";
    --! Short 7-digit git SHA of the repository
    GLOBAL_SHA  : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the repository (format: MMmmcccc in hex)
    GLOBAL_VER  : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the tcl file
    TOP_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the top folder, see TOP_SHA
    TOP_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the tcl file
    CON_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the top folder, see TOP_SHA
    CON_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the XMLs
    XML_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the XMLs

    XML_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the Hog submodule
    HOG_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of Hog
    HOG_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git sha
    ALGOLIB_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of algolib library (format: MMmmcccc in hex)
    ALGOLIB_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA
    INFRASTRUCTURE_LIB_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of infrastructure library (format: MMmmcccc in hex)
    INFRASTRUCTURE_LIB_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA
    TOB_RDOUT_LIB_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the readout library (format: MMmmcccc in hex)
    TOB_RDOUT_LIB_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the ipbus submodule
    IPBUS_LIB_SHA : std_logic_vector(31 downto 0) := x"00000000";

    -- Firmware parameters
    n_channels : natural := 64);

  port (
    --------------- top_ipbus_fpga signals
    gt_clk125_p             : in  std_logic;                       --! 125MHz ipbus clock
    gt_clk125_n             : in  std_logic;                       --! 125MHz ipbus clock
    hardware_addr           : in  std_logic_vector (7 downto 0);
    master_rx_data          : in  std_logic_vector (9 downto 0);   --! ipbus interconnections signals
    master_tx_pause         : in  std_logic;                       --! ipbus interconnections signals
    master_tx_data          : out std_logic_vector (9 downto 0);   --! ipbus interconnections signals
    flash_csn               : out std_logic;
    flash_mosi              : out std_logic;
    flash_miso              : in  std_logic;
    flash_led               : out std_logic;
--    flash_wp                : out std_logic;
--    flash_hold              : out std_logic;
    ttc_clk_p               : in  std_logic;                       -- !ttc clock 40Mhz
    ttc_clk_n               : in  std_logic;                       -- !ttc clock 40Mhz
    q_clk_gtrefclk_pad_n_in : in  std_logic_vector (19 downto 0);  --! mgts reference clocks
    q_clk_gtrefclk_pad_p_in : in  std_logic_vector (19 downto 0);  --! mgts reference clocks
    rxn_IN                  : in  std_logic_vector (79 downto 0);  --! mgt rx side inputs
    rxp_IN                  : in  std_logic_vector (79 downto 0);  --! mgt rx side inputs
    txn_OUT                 : out std_logic_vector (77 downto 0);  --! mgts tx side outputs
    txp_OUT                 : out std_logic_vector (77 downto 0);  --! mgts tx side outputs
    fpga_geo_addr           : in  std_logic_vector (1 downto 0);   --! geographical address of the fpga
    -- inputs from other FPGAs
    data_from_fpga_A_p      : in  std_logic_vector(32 downto 0);   --! merging data from another fpga
    data_from_fpga_A_n      : in  std_logic_vector(32 downto 0);   --! merging data from another fpga
    data_from_fpga_B_p      : in  std_logic_vector(32 downto 0);   --! merging data from another fpga
    data_from_fpga_B_n      : in  std_logic_vector(32 downto 0);   --! merging data from another fpga
    data_from_fpga_C_p      : in  std_logic_vector(32 downto 0);   --! merging data from another fpga
    data_from_fpga_C_n      : in  std_logic_vector(32 downto 0);   --! merging data from another fpga

    -- outputs to other FPGAs
    data_to_fpga_X_p : out std_logic_vector(32 downto 0);  --! merging data from this fpga to another fpga
    data_to_fpga_X_n : out std_logic_vector(32 downto 0);  --! merging data from this fpga to another fpga
    data_to_fpga_Y_p : out std_logic_vector(32 downto 0);  --! merging data from this fpga to another fpga
    data_to_fpga_Y_n : out std_logic_vector(32 downto 0);  --! merging data from this fpga to another fpga                                                                               --

    ttc_inform_p : in std_logic_vector(3 downto 0);  --! ttc information that has L1A,BCR and ECR
    ttc_inform_n : in std_logic_vector(3 downto 0);  --! ttc information that has L1A,BCR and ECR
    ECRID        : in std_logic_vector(7 downto 0);  --! ttc information ECR ID

    ctrl_RAW_ready_in : in std_logic;  --! Ready signal from control FPGA to receive RAW calorimeter data
    ctrl_TOB_ready_in : in std_logic   --! Ready signal from control FPGA to receive TOBs data
    );


end top_process_fpga;

--! @copydoc top_process_fpga.vhd 
architecture Behavioral of top_process_fpga is

------------------------------------------------

  component io_delay
    generic
      (                         -- width of the data for the system
        SYS_W : integer := 16;
        -- width of the data for the device
        DEV_W : integer := 16);
    port
      (
        -- From the system into the device
        data_in_from_pins_p : in  std_logic_vector(SYS_W-1 downto 0);
        data_in_from_pins_n : in  std_logic_vector(SYS_W-1 downto 0);
        data_in_to_device   : out std_logic_vector(DEV_W-1 downto 0);

-- Input, Output delay control signals
        delay_clk         : in  std_logic;
        in_delay_reset    : in  std_logic;                              -- Active high synchronous reset for input delay
        in_delay_data_ce  : in  std_logic_vector(SYS_W -1 downto 0);    -- Enable signal for delay 
        in_delay_data_inc : in  std_logic_vector(SYS_W -1 downto 0);    -- Delay increment (high), decrement (low) signal
        in_delay_tap_in   : in  std_logic_vector(5*SYS_W -1 downto 0);  -- Dynamically loadable delay tap value for input delay
        in_delay_tap_out  : out std_logic_vector(5*SYS_W -1 downto 0);  -- Delay tap value for monitoring input delay
        delay_locked      : out std_logic;                              -- Locked signal from IDELAYCTRL
        ref_clock         : in  std_logic;                              -- Reference Clock for IDELAYCTRL. Has to come from BUFG.

-- Clock and reset signals
        clk_in   : in std_logic;   -- Fast clock from PLL/MMCM 
        io_reset : in std_logic);  -- Reset signal for IO circuit
  end component;

  component io_delay2
    generic
      (                         -- width of the data for the system
        SYS_W : integer := 1;
        -- width of the data for the device
        DEV_W : integer := 1);
    port
      (
        -- From the system into the device
        data_in_from_pins_p : in  std_logic_vector(SYS_W-1 downto 0);
        data_in_from_pins_n : in  std_logic_vector(SYS_W-1 downto 0);
        data_in_to_device   : out std_logic_vector(DEV_W-1 downto 0);

-- Input, Output delay control signals
        delay_clk         : in  std_logic;
        in_delay_reset    : in  std_logic;                              -- Active high synchronous reset for input delay
        in_delay_data_ce  : in  std_logic_vector(SYS_W -1 downto 0);    -- Enable signal for delay 
        in_delay_data_inc : in  std_logic_vector(SYS_W -1 downto 0);    -- Delay increment (high), decrement (low) signal
        in_delay_tap_in   : in  std_logic_vector(5*SYS_W -1 downto 0);  -- Dynamically loadable delay tap value for input delay
        in_delay_tap_out  : out std_logic_vector(5*SYS_W -1 downto 0);  -- Delay tap value for monitoring input delay
        delay_locked      : out std_logic;                              -- Locked signal from IDELAYCTRL
        ref_clock         : in  std_logic;                              -- Reference Clock for IDELAYCTRL. Has to come from BUFG.

-- Clock and reset signals
        clk_in   : in std_logic;   -- Fast clock from PLL/MMCM 
        io_reset : in std_logic);  -- Reset signal for IO circuit
  end component;


  signal MGT_QUAD_ENABLE   : std_logic_vector(19 downto 0) := x"fffff";
  signal MGT_USE_OTHER_CLK : std_logic_vector(19 downto 0) := x"00000";
  signal MGT_TX_POWER      : std_logic_vector(79 downto 0) := (others => '1');
  signal MGT_RX_POWER      : std_logic_vector(79 downto 0) := (others => '1');

-----ipb signal declaration
  signal clk200, load, ipb_clk, mac_clk, clk40, clk160, clk280, clk280_90 : std_logic;
  signal onehz, ipb_rst, rst_macclk, rst_ipb, start, reset, locked_160m   : std_logic;
  signal algo_ipb_in                                                      : ipb_rbus;  -- this goes to ip_out slave
  signal algo_ipb_out                                                     : ipb_wbus;  -- this goes to ip_in slave

  signal ipbw    : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal ipbr    : ipb_rbus_array(N_SLAVES-1 downto 0);
  signal ipb_in  : ipb_rbus;
  signal ipb_out : ipb_wbus;

-----------------mgt signal declaration
  signal MGT_Commadet_int, rxdata, clk280_int : std_logic_vector (79 downto 0);  -- was using n_channels


--------------- inter FPGA, control and spi flash signal declaration---------------------------------------------
  signal flash_clk : std_logic;

  signal Module_ID, fpga_id, hw_position, fw_rev, fw_tag, datafmt_rev, status : std_logic_vector(31 downto 0);
  signal trigger_reconfig, reconfig                                           : std_logic;

  signal control_reg, reconfig_reg              : std_logic_vector (31 downto 0);
  signal master_rx_data_int, master_tx_data_int : std_logic_vector (9 downto 0);
  signal master_tx_pause_int                    : std_logic;

  signal data_from_fpga_A, data_from_fpga_B, data_from_fpga_C : std_logic_vector(32 downto 0);



------- agorithm signal declaration
  signal eg_tob_0, eg_tob_1, eg_tob_2, eg_tob_3, eg_tob_4, eg_tob_5, eg_tob_6, eg_tob_7         : std_logic_vector(31 downto 0);
  signal OUT_tau_XTOB, OUT_eg_XTOB                                                              : AlgoXOutput;        -- array 8 x 32b words
  signal sorted_tau_TOB, sorted_eg_TOB                                                          : AlgoTriggerObject;  -- 32b word
  signal OUT_eg_Sync, OUT_tau_Sync                                                              : std_logic;
  signal OUT_eg_Valid, OUT_tau_Valid                                                            : std_logic_vector(OUTPUT_TOBS-1 downto 0);
  signal sorted_eg_Valid, sorted_tau_start, sorted_eg_start                                     : std_logic;
  signal tau_tob_0, tau_tob_1, tau_tob_2, tau_tob_3, tau_tob_4, tau_tob_5, tau_tob_6, tau_tob_7 : std_logic_vector(31 downto 0);

  -- merger signlas. Defaults values are needed in case fpgas have no merging module
  signal data_merge_1, data_merge_2, data_merge_3, data_merge_4 : std_logic_vector(32 downto 0);
  signal data_merge                                             : AlgoTriggerObjects(3 downto 0) := (others => (others => '0'));
  signal sorted_merged_Start, sorted_merged_Valid               : std_logic                      := '0';
  signal sorted_merged_TOB                                      : AlgoTriggerObject              := (others => '0');

--- internal rx_data declaration

--------------mgt signal declaration
  signal MGT_CLK_GTREFCLK_PAD_N_IN, MGT_CLK_GTREFCLK_PAD_P_IN, mgt_RXUSRCLK_OUT, rx_resetdone : std_logic_vector(79 downto 0);
  signal mgt_SOFT_RESET_TX_IN, mgt_SOFT_RESET_RX_IN                                           : std_logic_vector(19 downto 0);
  signal tx_resetdone, mgt_TXUSRCLK_OUT                                                       : std_logic_vector(79 downto 0);
  signal mgt_sel_tx_clk                                                                       : std_logic_vector(11 downto 0);

  signal MGT_RXN_IN, MGT_RXP_IN : mgt_rx_array (19 downto 0);

  signal MGT_TXN_IN, MGT_TXP_IN : mgt_tx_array(19 downto 0);
  signal rxdata_quad_array      : mgt_rxdata_array (19 downto 0);
  signal mgt_txdata             : mgt_txdata_array(19 downto 0);

  signal mgt_DATA_VALID_IN : std_logic_vector(79 downto 0);
  signal mgt_loopback_in   : mgt_loopback_array (19 downto 0);

  signal mgt_rxchariscomma : mgt_rxchariskcomm_array(19 downto 0);
  signal mgt_rxcharisk     : mgt_rxcharisk_array(19 downto 0);

  signal mgt_rxdisperr    : mgt_rxdisperr_array(79 downto 0);
  signal mgt_rxnotintable : mgt_rxnotintable_array(79 downto 0);
  
  signal disperr_error_i,notable_error_i: std_logic_vector(79 downto 0);
  --signal mgt_rx_fsm_resetdone                      : std_logic_vector(n_channels-1 downto 0);
  signal mgt_rx_fsm_resetdone                      : std_logic_vector(79 downto 0);
  signal mgt_tx_fsm_resetdone, qpll_fsm_reset_done : std_logic_vector(79 downto 0);

  signal mgt_QPLLLOCK_OUT, mgt_QPLLREFCLKLOST_OUT : std_logic_vector(19 downto 0);

  signal gt_rxpd : mgt_rxpd_array(19 downto 0);
  signal gt_txpd : mgt_txpd_array(19 downto 0);

  signal mgt_commdet                     : std_logic_vector(0 downto 0);
  signal tx_bufstatus                    : std_logic_vector (159 downto 0);
  signal rx_realign, error_counter_reset : std_logic_vector (79 downto 0);

  signal mgt_loopback_reg    : std_logic_vector (59 downto 0);
  signal mgt_txbufstatus     : mgt_txbufstatus_array(19 downto 0);
  signal mgt_rxcommadet      : mgt_rxcommadet_array (19 downto 0);
  signal mgt_rxbyterealign   : mgt_rxbyterealign_array(19 downto 0);
  signal mgt_rx_resetdone    : mgt_rxresetdone_array (19 downto 0);
  signal mgt_rxbyteisaligned : mgt_rxbyteisaligned_array (19 downto 0);
  signal mgt_tx_resetdone    : mgt_txresetdone_array (19 downto 0);
  signal mgt_txcharisk       : mgt_txcharisk_array (19 downto 0);

  signal rx_disperr_reg, encode_error_reg, mgt_DATA_VALID_IN_reg, rxbyteisaligned, rxdata_out : std_logic_vector (79 downto 0);
  signal rx_disperr, txcharisk, encode_error, mgt_rxcharisk_reg                               : std_logic_vector (319 downto 0);
  signal data_readout_0, data_readout_1, data_readout_2, data_readout_3                       : std_logic_vector(223 downto 0);

  signal bcn_cntr                                                                                                               : std_logic_vector(11 downto 0);
  signal enable_mgt, bcn_synch, crc_error_chan                                                                                  : std_logic_vector(79 downto 0);
  signal sorted_tau_Valid                                                                                                       : std_logic;
  signal start_pulse_rst                                                                                                        : std_logic;
  signal BC_Reg_sel, mux_sel                                                                                                    : std_logic_vector(319 downto 0);
  -- debug signals
  signal reg224_latch, ttc_pipe, delay_latch                                                                                    : std_logic_vector(63 downto 0);
  signal delay_num                                                                                                              : std_logic_vector(319 downto 0);
  signal reg224_latch_0, reg224_latch_1, reg224_latch_2, reg224_latch_3, rx_realign_0, rx_realign_1, rx_realign_2, rx_realign_3 : std_logic;
  signal delay_latch_0, delay_latch_1, delay_latch_2, delay_latch_3, rx_resetdone_quad111                                       : std_logic;
  signal delay_num_0, delay_num_1, delay_num_2, delay_num_3, rx_disperr_0, rx_disperr_1, rx_disperr_2, rx_disperr_3             : std_logic_vector(3 downto 0);
  signal encode_error_0, encode_error_1, encode_error_2, encode_error_3                                                         : std_logic_vector(3 downto 0);
  signal bc_cntr_0, bc_cntr_1, bc_cntr_2, bc_cntr_3, bc_mux_cntr_0, bc_mux_cntr_1, bc_mux_cntr_2, bc_mux_cntr_3                 : std_logic_vector(139 downto 0);

--  signal resetbc_cntr : std_logic;
  signal bcn_ref : std_logic_vector(4 downto 0);

  signal pseudo_orbit                                                                                                                         : std_logic;
  -- data paths tx phase calibration
  signal comma_0, comma_1, comma_2, comma_3, data_eq0, data_eq1, data_eq2, data_eq3                                                           : std_logic;
  signal data_out_0, data_out_1, data_out_2, data_out_3, phase_mux_i                                                                          : std_logic_vector(31 downto 0);
  signal test_data                                                                                                                            : std_logic_vector(33 downto 0);
  signal rx_data_0, rx_data_1, rx_data_2, rx_data_3, rx_data_4, rx_data_5, rx_data_6, rx_data_7                                               : mgt_data;
  signal txdatai_0, txdatai_1, txdatai_2, txdatai_3, txdatai_4, txdatai_5, txdatai_6, txdatai_7, txdatai_8, txdatai_9, txdatai_10, txdatai_11 : std_logic_vector (33 downto 0);
  signal phase_mux                                                                                                                            : std_logic_vector(319 downto 0);
  signal sorted_eg_TOB_i_gt0, sorted_eg_TOB_i_gt1, sorted_eg_TOB_i_gt2, sorted_eg_TOB_i_gt3                                                   : std_logic_vector (33 downto 0);
  signal sorted_eg_TOB_i_gt4, sorted_eg_TOB_i_gt5, sorted_eg_TOB_i_gt6, sorted_eg_TOB_i_gt7                                                   : std_logic_vector (33 downto 0);
  signal error_count_0, error_count_1, error_count_2, error_count_3, error_count_i, error_count_i_0                                           : unsigned(31 downto 0);
  signal error_count_4, error_count_5, error_count_6, error_count_7                                                                           : unsigned(31 downto 0);
  signal clear_error, comma_detect_ILA                                                                                                        : std_logic;
  signal error_counter                                                                                                                        : std_logic_vector(1279 downto 0);

  attribute PRESERVE_SIGNAL                                                                                                           : boolean;
  signal Q210_SOFT_RESET_TX_IN_i, Q210_gt0_txresetdone_i, Q210_GT0_TX_FSM_RESET_DONE_i, Q210_GT0_RX_FSM_RESET_DONE_i                  : std_logic;
  signal Q210_GT1_TX_FSM_RESET_DONE_i, Q210_GT1_RX_FSM_RESET_DONE_i, Q210_gt1_txresetdone_i, Q210_gt0_cplllock_i, Q210_gt1_cplllock_i : std_logic;
  signal Q210_gt0_txcharisk, Q210_gt1_txcharisk, mgt210_tx_bufstatus_i                                                                : std_logic_vector(3 downto 0);
  signal gt0_cpllfbclklost_i, gt1_cpllfbclklost_i                                                                                     : std_logic;
  signal mgt210_tx_resetdone_i, mgt210_tx_fsm_resetdone_i, mgt210_txclk_i                                                             : std_logic_vector (1 downto 0);

  signal T_TOB_32b_in_i                                             : AlgoTriggerObject;  -- 32b owrd
  signal T_TOB_sync_in_i                                            : std_logic;
  signal T_TOB_wr_in_i                                              : std_logic;
  signal TOB_BCN_sync_out_i,TOB_BCN_sync_tau_i,TOB_BCN_sync_eg_i   : std_logic;
  signal data_merge_BCN_A_i, data_merge_BCN_B_i, data_merge_BCN_C_i : std_logic;

  signal data_to_X_i, data_to_Y_i : std_logic_vector(32 downto 0);

--- Readout signals

  signal arr_raw_data_in_i                   : RAW_data_227_type;
  signal TOPO_TOB_out_char                   : std_logic;
  signal TOPO_TOB_out                        : std_logic_vector(31 downto 0);
  signal RAW_Data_out_char                   : std_logic;
  signal RAW_Data_out                        : std_logic_vector(31 downto 0);
  signal sorted_eg_TOB_1, sorted_tau_TOB_1   : std_logic_vector(32 downto 0);
  signal tob_eg_in                           : std_logic_vector(511 downto 0);
  signal tob_tau_in                          : std_logic_vector(511 downto 0);
  signal TOB_TXOUTCLK_i, RAW_TXOUTCLK_i      : std_logic;
  signal ttc_inform                          : std_logic_vector (3 downto 0);
  signal ECRID_i                             : std_logic_vector (7 downto 0);
  signal L1A_i, BCR_i, ECR_i, TTC_read_all_i : std_logic;
  signal privilege_read_i                    : std_logic;   -- privilege read
  signal ctrl_RAW_ready_i                    : std_logic;
  signal ctrl_TOB_ready_i                    : std_logic;
--  signal TTC_L1A_ID_EXT_in_i                 : std_logic_vector (7 downto 0);
  --! TOB type, 0 = electrom, 1 = tau
  signal TOB_type_i                          : std_logic;
  signal sorted_TOB_BCN_i, OUT_TOB_BCN_i     : std_logic_vector (6 downto 0);  -- sorted TOB BC_ID with delay through ALGO/sorting block
  signal merged_TOB_BCN_i                    : std_logic_vector (6 downto 0);  -- merged TOB BC_ID with delay through ALGO/merging block
  signal OUT_XTOB_BCN_i                      : std_logic_vector (6 downto 0);  -- sorted XTOB BC_ID with delay through ALGO/sorting block


----------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MGT selector signals
------------------------------------------------------------------------------------------------------------------------------------

  signal mgt_sel_BC_Reg_sel     : std_logic_vector(255 downto 0);
  signal mgt_sel_mux_sel        : std_logic_vector(255 downto 0);
  signal mgt_sel_delay_num      : std_logic_vector(255 downto 0);
  signal mgt_sel_bc_cntr_0      : std_logic_vector(111 downto 0);
  signal mgt_sel_bc_cntr_1      : std_logic_vector(111 downto 0);
  signal mgt_sel_bc_cntr_2      : std_logic_vector(111 downto 0);
  signal mgt_sel_bc_cntr_3      : std_logic_vector(111 downto 0);
  signal mgt_sel_bc_mux_cntr_0  : std_logic_vector(111 downto 0);
  signal mgt_sel_bc_mux_cntr_1  : std_logic_vector(111 downto 0);
  signal mgt_sel_bc_mux_cntr_2  : std_logic_vector(111 downto 0);
  signal mgt_sel_bc_mux_cntr_3  : std_logic_vector(111 downto 0);
  signal mgt_sel_RXUSRCLK_OUT   : std_logic_vector(63 downto 0);
  signal mgt_sel_enable_mgt     : std_logic_vector(63 downto 0);
  signal mgt_sel_bcn_synch      : std_logic_vector(63 downto 0);
  signal mgt_sel_crc_error_chan : std_logic_vector(63 downto 0);
  signal mgt_sel_disperr_error   : std_logic_vector(63 downto 0);
  signal mgt_sel_notable_error     : std_logic_vector(63 downto 0);  
  signal mgt_sel_rx_resetdone   : std_logic_vector(63 downto 0);
  signal mgt_sel_Commadet       : std_logic_vector(63 downto 0);
  signal mgt_sel_Data           : mgt_rxdata_array (15 downto 0);

  -- playback ram signals
  signal kchar_mgt     : std_logic_vector(79 downto 0);
  signal rxdata_mgt0   : std_logic_vector(639 downto 0);
  signal ram_data_mgt0 : std_logic_vector(639 downto 0);
  signal rxdata_mgt1   : std_logic_vector(639 downto 0);
  signal ram_data_mgt1 : std_logic_vector(639 downto 0);
  signal rxdata_mgt2   : std_logic_vector(639 downto 0);
  signal ram_data_mgt2 : std_logic_vector(639 downto 0);
  signal rxdata_mgt3   : std_logic_vector(639 downto 0);
  signal ram_data_mgt3 : std_logic_vector(639 downto 0);

  signal mgt_sel_rxdata_mgt0 : std_logic_vector(511 downto 0);
  signal mgt_sel_rxdata_mgt1 : std_logic_vector(511 downto 0);
  signal mgt_sel_rxdata_mgt2 : std_logic_vector(511 downto 0);
  signal mgt_sel_rxdata_mgt3 : std_logic_vector(511 downto 0);

  signal mgt_sel_ram_data_mgt0 : std_logic_vector(511 downto 0);
  signal mgt_sel_ram_data_mgt1 : std_logic_vector(511 downto 0);
  signal mgt_sel_ram_data_mgt2 : std_logic_vector(511 downto 0);
  signal mgt_sel_ram_data_mgt3 : std_logic_vector(511 downto 0);

  signal mgt_sel_kchar : std_logic_vector(63 downto 0);
  signal fpga_number   : integer;

  signal sorted_Start_sel, sorted_Start_sel_1, sorted_Start_sel_i : std_logic                      := '0';
  signal sorted_Valid_sel, sorted_Valid_sel_1, sorted_Valid_sel_i : std_logic                      := '0';
  signal sorted_TOB_sel                                           : std_logic_vector (31 downto 0) := (others => '0');
  signal TOB_BCN_sync_reg_i                                       : std_logic_vector (31 downto 0) := (others => '0');
  signal sorted_synch_int                : std_logic;
  signal sel_bcn_or_bc_cnt_i             : std_logic;
  signal tob_bc_reg, tob_bc_status       : std_logic_vector (31 downto 0);
  signal TOB_BCN_sync_internal  : std_logic;
  signal dummy1, dummy2, dummy3 : std_logic_vector(74 downto 0);
  signal tob_delay_reg, tob_delay_status : std_logic_vector (31 downto 0);


--  ####### Mark signals  ########
  attribute keep                    : string;
  attribute max_fanout              : integer;
  attribute keep of data_merge      : signal is "true";
  attribute keep of sorted_eg_TOB   : signal is "true";
  attribute keep of sorted_tau_TOB  : signal is "true";
  attribute keep of sorted_eg_TOB_1 : signal is "true";
-- attribute keep of       sorted_tau_TOB_2 : signal is "true" ;

--   #######################################

begin
  fpga_number <= F_FPGA_NUMBER(FLAVOUR);

  reset             <= not locked_160m;
  mgt_DATA_VALID_IN <= (others => '1');
  clear_error       <= not control_reg(2);
  sel_bcn_or_bc_cnt_i <=  control_reg(31);  -- selects between BC_MUX value and real data BC value

--  TTC_L1A_ID_EXT_in_i <= (others => '0');  -- connect to TTC input from cFPGA
  TTC_read_all_i      <= '0';              -- connect to TTC input from cFPGA (previledge read)

  ----------debug signal assignment---------------------------------------------
  reg224_latch_0 <= reg224_latch(0);
  reg224_latch_1 <= reg224_latch(1);
  reg224_latch_2 <= reg224_latch(2);
  reg224_latch_3 <= reg224_latch(3);

  delay_latch_0 <= delay_latch(0);
  delay_latch_1 <= delay_latch(1);
  delay_latch_2 <= delay_latch(2);
  delay_latch_3 <= delay_latch(3);

  delay_num_0 <= delay_num (3 downto 0);
  delay_num_1 <= delay_num (7 downto 4);
  delay_num_2 <= delay_num (11 downto 8);
  delay_num_3 <= delay_num (15 downto 12);

  rx_realign_0 <= rx_realign(0);
  rx_realign_1 <= rx_realign(1);
  rx_realign_2 <= rx_realign(2);
  rx_realign_3 <= rx_realign(3);

  rx_disperr_0 <= rx_disperr(3 downto 0);
  rx_disperr_1 <= rx_disperr(7 downto 4);
  rx_disperr_2 <= rx_disperr(11 downto 8);
  rx_disperr_3 <= rx_disperr(15 downto 12);

  encode_error_0 <= encode_error (3 downto 0);
  encode_error_1 <= encode_error (7 downto 4);
  encode_error_2 <= encode_error (11 downto 8);
  encode_error_3 <= encode_error (15 downto 12);

  ---------------------------------------------------------------------

  ---- Power control of the MGT
  -- All the rx side of the mgt are enabled 64 of them
  -- "00" = power on   and "11" = power down
  ---------------------------------------------------------------------

  rx_pwr_on_gen : for i in 0 to 19
  generate
    gt_rxpd(i).gt0_rxpd <= not F_MGT_RX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+0) & not F_MGT_RX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+0);
    gt_rxpd(i).gt1_rxpd <= not F_MGT_RX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+1) & not F_MGT_RX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+1);
    gt_rxpd(i).gt2_rxpd <= not F_MGT_RX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+2) & not F_MGT_RX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+2);
    gt_rxpd(i).gt3_rxpd <= not F_MGT_RX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+3) & not F_MGT_RX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+3);
    gt_txpd(i).gt0_txpd <= not F_MGT_TX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+0) & not F_MGT_TX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+0);
    gt_txpd(i).gt1_txpd <= not F_MGT_TX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+1) & not F_MGT_TX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+1);
    gt_txpd(i).gt2_txpd <= not F_MGT_TX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+2) & not F_MGT_TX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+2);
    gt_txpd(i).gt3_txpd <= not F_MGT_TX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+3) & not F_MGT_TX_POWER(F_FPGA_NUMBER(FLAVOUR))(i*4+3);
  end generate;

---------------------------------------------------------------------------------------------------------------------------------

--  moddule status assignement
  Module_ID <= (31           => '1',                                        -- user (1) / golden image (0)
                30 downto 28 => std_logic_vector(to_unsigned(FLAVOUR, 3)),  -- FPGA flavour, 0 for control FPGA
                27 downto 26 => fpga_geo_addr,
                25 downto 20 => "000000",                                   -- space for control serial number
                19 downto 16 => "0000",                                     -- shelf address
                15 downto 12 => hardware_addr(3 downto 0),                  -- slot address
                11 downto 0  => X"efe"                                      -- module ID
                );

  status <= x"0000" & delay_num_3 & delay_num_2 & delay_num_1 & delay_num_0;

  start            <= control_reg(0);
  trigger_reconfig <= reconfig;          -- and locked;
  reconfig         <= reconfig_reg(30);  -- if active it will reconfigure the spi flash.

  U_1 : entity infrastructure_lib.proc_fpgas
    generic map (IPBUSPORT => F_IPBUS_PORT_N(F_FPGA_NUMBER(FLAVOUR)))
    port map (
      ipb_clk         => ipb_clk,
      mac_clk         => mac_clk,
      rst_ipb         => rst_ipb,
      rst_macclk      => rst_macclk,
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      master_rx_data  => master_rx_data,
      master_tx_pause => master_tx_pause_int,
      master_tx_data  => master_tx_data
      );

 process(mac_clk)
  begin
    if rising_edge(mac_clk) then
      master_tx_pause_int <= master_tx_pause;
    end if;
  end process;

------------------------------------------------------------------------------------------------------
-- global decoder at the top level.it implements the top xml
-----------------------------------------------------------------------------------------------------

  global_fabric : entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV      => N_SLAVES,  -- defined in ipbus_decode_top_address_table
                SEL_WIDTH => ipbus_sel_width)
    port map(
      ipb_in          => ipb_out,
      ipb_out         => ipb_in,
      sel             => ipbus_sel_L1CaloEfexProcessor(ipb_out.ipb_addr),
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      );

---------------------------------------------------------------------------------
----- common id registers slave
---------------------------------------------------------------------------------------

  common_ID : entity infrastructure_lib.common_id_registers
    port map (
      ipb_clk     => ipb_clk,
      ipb_rst     => rst_ipb,
      ipb_in      => ipbw(N_SLV_COMMON_ID_VERSION),
      ipb_out     => ipbr(N_SLV_COMMON_ID_VERSION),
      Module_ID   => Module_ID,
      xml_version => XML_VER,
      xml_Gitsha  => XML_SHA,
      build_date  => GLOBAL_DATE,
      build_time  => GLOBAL_TIME,
      fw_version  => GLOBAL_VER,
      fw_Gitsha   => GLOBAL_SHA
      );

-------------------------------------------------------------------------------------------------------------------
---library versions slave
--------------------------------------------------------------------------------------------------------------------

  library_reg : entity infrastructure_lib.lib_registers
    port map (
      ipb_clk             => ipb_clk,
      ipb_rst             => rst_ipb,
      ipb_in              => ipbw(N_SLV_EFEX_LIB_VERSION),
      ipb_out             => ipbr(N_SLV_EFEX_LIB_VERSION),
      constraints_version => CON_VER,
      constraints_gitsha  => CON_SHA,
      hog_gitsha          => HOG_SHA,
      hog_version         => HOG_VER,
      top_version         => TOP_VER,
      top_gitsha          => TOP_SHA,
      infra_version       => INFRASTRUCTURE_LIB_VER,
      infra_gitsha        => INFRASTRUCTURE_LIB_SHA,
      algo_version        => ALGOLIB_VER,
      algo_gitsha         => ALGOLIB_SHA,
      readout_version     => TOB_RDOUT_LIB_VER,
      readout_gitsha      => TOB_RDOUT_LIB_SHA,
      ipbus_gitsha        => IPBUS_LIB_SHA
      );

  ----------------------------------------------------------------------------------------------------------------------
  -- ipbus slaves live in the entity below, and can expose top-level ports and the ipbus fabric is instantiated with in.
  ----------------------------------------------------------------------------------------------------------------------
  slaves : entity infrastructure_lib.slaves
    generic map (FPGA_FLAVOUR => FLAVOUR )
    port map(
      ipb_clk          => ipb_clk,
      ipb_rst          => rst_ipb,
      ipb_in           => ipbw(N_SLV_COMMON_INFRA),
      ipb_out          => ipbr(N_SLV_COMMON_INFRA),
      status           => status,
      control_reg      => control_reg,
      tob_delay_reg    => tob_delay_reg,
      tob_delay_status => tob_delay_status,
      tob_bc_reg       => tob_bc_reg,
      tob_bc_status    => tob_bc_status,
      TOB_BCN_sych_reg => TOB_BCN_sync_reg_i,
      reconfig_reg     => reconfig_reg,
      flash_select     => open,
      flash_miso       => flash_miso,
      flash_le         => flash_csn,
      flash_clko       => flash_clk,
      flash_mosi       => flash_mosi
      );

-------------------------------------------------------------
-- reconfiguring the spi flash and FPGA logic
-------------------------------------------------------------
  cclk_o : entity infrastructure_lib.startup
    port map(
      flash_cclk => flash_clk

      );


  configure : entity infrastructure_lib.self_configure
    port map(
      clk       => ipb_clk,     --icap_clk,
      led_clk   => onehz,
      reset     => rst_ipb,
      trigger   => trigger_reconfig,
      indicator => flash_led
      );


--------------------------------------------------------------------------
---  impelemtation of all the clocks required for the design
---------------------------------------------------------------------
  clock_resources : entity infrastructure_lib.clk_resources

    port map (
      gt_clk_p    => gt_clk125_p,     -- input clock of 125MHz for ipbus
      gt_clk_n    => gt_clk125_n,     -- clock 125MHz for ipbus
      TTC_clk_p   => ttc_clk_p,       -- TTC clock of 40MHz
      TTC_clk_n   => ttc_clk_n,       -- TTC clock of 40MHz
      ipb_clk     => ipb_clk,         -- clock 31.25MHz for ipbus
      mac_clk     => mac_clk,         -- clock 125MHz for ipbus
      clk280      => clk280,
      clk200      => clk200,          -- clock 200MHz for algo
      clk40       => clk40,           -- clock of 40MHz
      clk160      => clk160,          -- clock of 160MHz for readout
      reset       => control_reg(3),  -- to reset the mmcm of the 280M fabric clock
      onehz       => onehz,
      rst_macclk  => rst_macclk,      -- internal reset for the ipbus logic
      rst_ipb     => rst_ipb,
      locked_160m => locked_160m,     -- lock signal for 160MH clock
      load        => load             --clk 40MHz, 20% duty cycle,  -36 deg phase
      );

-- This selects sorted TOBs for FPGA 1 OR 2
  clk_proc : process (clk280)
  begin
    if clk280'event and clk280 = '1' then
      T_TOB_32b_in_i  <= sorted_merged_TOB;    -- 32b
      T_TOB_sync_in_i <= sorted_merged_Start;  -- 1b
      T_TOB_wr_in_i   <= sorted_merged_Valid;  -- 1b
      OUT_TOB_BCN_i   <= merged_TOB_BCN_i;     -- 1b
      TOB_type_i      <= '0';                  -- TOB Type 0 = em
    end if;
  end process;

  ----------------------------------------------------
  -- ttc information signals
  ----------------------------------------------------
  input_f5_to_f1 : for i in 0 to 3 generate

    f5_to_f1 : IBUFDS
      port map(
        i  => ttc_inform_p (i),
        ib => ttc_inform_n (i),
        o  => ttc_inform(i)
        );
  end generate;


  synch_L1A_in : process (clk40)  -- synch L1A_in with 40MHz
  begin
    if clk40'event and clk40 = '0' then   -- sample on FALLING EDGE clock
      L1A_i <= ttc_inform(0);   -- this is  L1A from TTC
      BCR_i <= ttc_inform(1);   -- this is  BCR from TTC
      ECR_i <= ttc_inform(2);   -- this is  ECR from TTC
      privilege_read_i <= ttc_inform(3);   -- this is  Privilege Read from TTC
      ECRID_i <= ECRID ;        -- this is  ECRID from TTC
    end if;
  end process;

-----------------------------------------------------------------------------
--- TOB  read out
------------------------------------------------------------------------------

  Readout_block : entity TOB_rdout_lib.Readout_logic_top
    generic map (FPGA_NUMBER => F_FPGA_NUMBER(FLAVOUR))
    port map(
      RST                   => reset,
      hw_addr               => fpga_geo_addr,                -- FPGA Hardware Address
      ipb_rst               => rst_ipb,
      ipb_clk               => ipb_clk,
      IPb_in                => ipbw(N_SLV_EFEX_READOUT),     -- The signals going from master to slaves
      IPb_out               => ipbr(N_SLV_EFEX_READOUT),     -- The signals going from slaves to master
      clk_200M_in           => clk200,
      clk_280M_in           => clk280,
      clk_160M_in           => clk160,
      clk_40M_in            => clk40,
      TOB_TXOUTCLK          => TOB_TXOUTCLK_i,
      RAW_TXOUTCLK          => RAW_TXOUTCLK_i,
      -- eXtended TOB data readout signals
      XTOB_eg_in            => OUT_eg_XTOB,                  -- array 8 * 64b of XTOBs e/g
      XTOB_eg_Valid_flg_in  => OUT_eg_Valid,                 -- 8b XTOB e/g has valid data
      XTOB_eg_sync_in       => OUT_eg_Sync,                  -- XTOB e/g  sync signal
      XTOB_tau_in           => OUT_tau_XTOB,                 -- array 8 * 64b of XTOBs tau
      XTOB_tau_Valid_flg_in => OUT_tau_Valid,                -- 8b XTOB tau has valid data
      XTOB_tau_sync_in      => OUT_tau_Sync,                 -- XTOB tau  sync signal
      OUT_XTOB_BCN          => OUT_XTOB_BCN_i,               -- sorted XTOB BC_ID with delay through ALGO/sorting block
      -- sorted TOB data readout signals
      -- F1 reads e/g TOBs and F2 reads tau TOBs
      -- so same firmware in both FPGAs, use hw addr to differentiate
      T_TOB_32b_in          => T_TOB_32b_in_i,               -- sorted TOBs 32b  7 in series
      T_TOB_sync_in         => T_TOB_sync_in_i,              -- sorted TOB start signal
      T_TOB_valid_in        => T_TOB_wr_in_i,                -- sorted TOB write signal
      TOB_type_in           => TOB_type_i,                   -- TOB Type 0 = em, 1 = tau
      OUT_TOB_BCN           => OUT_TOB_BCN_i,                -- sorted TOB BC_ID with delay through ALGO/sorting block
      -- L1A signal input
      L1A_in                => L1A_i,
      -- ECR signal input
      ECR_in                => ECR_i,
      -- BCR signal input
      BCR_in                => BCR_i,
      TTC_read_all_in       => privilege_read_i,             -- privilege read in
      local_BCN_out         => bcn_cntr,                     -- BCN generated in eFEX
      TTC_L1A_ID_EXT_in     => ECRID_i,
      ctrl_TOB_ready_in     => ctrl_TOB_ready_in,            -- back pressure from cntl FPGA
      ctrl_RAW_ready_in     => ctrl_RAW_ready_in,            -- back pressure from cntl FPGA
      TOB_out_is_char       => TOPO_TOB_out_char,            -- o/p
      TOB_out               => TOPO_TOB_out,                 -- o/p sorted TOPO TOBs 32b out to MGT
      --! RAW input data readout signals
--      link_error_flags_in   => (others => (others => '0')),  -- i/p array 49 x 4 bit per link
      RAW_data_in           => arr_raw_data_in_i,            -- i/p array 49 x 227b input frames
      RAW_out_is_char       => RAW_Data_out_char,            -- o/p RAW data valid 32b out to MGT
      RAW_data_out          => RAW_Data_out                  -- o/p RAW data 32b out to MGT
      );

  --------------------------------------------------------------------------
  -- Top TOB merging and sorting Module
  -----------------------------------------------------------------------------------
  inter_FPGA_io : for i in 0 to 32 generate

    this_to_X : OBUFDS
      port map(
        o  => data_to_fpga_X_p(i),
        ob => data_to_fpga_X_n(i),
        i  => data_to_X_i(i)
        );

    this_to_Y : OBUFDS
      port map(
        o  => data_to_fpga_Y_p(i),
        ob => data_to_fpga_Y_n(i),
        i  => data_to_Y_i(i)
        );

  end generate inter_FPGA_io;

  data_to_X_i <= (others => '0') when FLAVOUR = 1 else  -- pFPGA 2, 3, 4 send  sorted_eg_TOB to pFPGA 1, Bus X
                 sorted_eg_TOB_1;

  data_to_Y_i <= (others => '0') when FLAVOUR = 2 else  -- pFPGA 1, 3, 4 send  sorted_tau_TOB to pFPGA 2, Bus Y
                 sorted_tau_TOB_1;


  ------------------------------------------------------------               
  io_reg_0 : process(clk280)
  begin
    if clk280' event and clk280 = '1' then
        if TOB_BCN_sync_reg_i(31) = '1' then
           sorted_eg_TOB_1 <= TOB_BCN_sync_eg_i & X"000000" & '0' & sorted_TOB_BCN_i;
        else 
           sorted_eg_TOB_1 <= TOB_BCN_sync_eg_i & sorted_eg_TOB;
        end if;
    end if;
  end process;

  io_reg_1 : process(clk280)
  begin
    if clk280' event and clk280 = '1' then
        if TOB_BCN_sync_reg_i(31) = '1' then
           sorted_tau_TOB_1 <= TOB_BCN_sync_tau_i & X"000000" & '0' & sorted_TOB_BCN_i;
        else
           sorted_tau_TOB_1 <= TOB_BCN_sync_tau_i & sorted_tau_TOB;
        end if;
    end if;
  end process;

-- Create TOB synchronisation signal
  TOB_BCN_sync : process (clk280)
  begin
    if clk280'event and clk280 = '1' then
      if (sorted_TOB_BCN_i = TOB_BCN_sync_reg_i(6 downto 0)) and (sorted_eg_start = '1') then
        TOB_BCN_sync_eg_i <= '1';
      else
        TOB_BCN_sync_eg_i <= '0';
      end if;

      if (sorted_TOB_BCN_i = TOB_BCN_sync_reg_i(6 downto 0)) and (sorted_tau_start = '1') then
        TOB_BCN_sync_tau_i <= '1';
      else
        TOB_BCN_sync_tau_i <= '0';
      end if;

    end if;
  end process;

----------------------------------------------------  

--  ILA_merging_2 : ila_ipbus_fabric_rd_wr
--    port map (
--      clk                  => clk280,
--      probe0               => sorted_eg_TOB_1(31 downto 0),  --36b 
--      probe1               => sorted_tau_TOB_1(31 downto 0),  --36b 
--      probe2(0)            => sorted_eg_TOB_1(32),  -- 1b 
--      probe3(0)            => sorted_tau_TOB_1(32),  -- 1b 
--      probe4(0)            => L1A_i,            -- 1b   
--      probe5               => x"0000000" & "000"&sorted_eg_Start,  --36b   
--      probe6               => (others => '0'),  --32b
--      probe7(0)            => BCR_i,            -- 1b
--      probe8(0)            => TOB_BCN_sync_eg_i,  -- 1b
--      probe9(0)            => TOB_BCN_sync_tau_i             -- 1b
--      );

-------------------------------------------------------------------------------------------
------Data Path  Block
------------------------------------------------------------------------------------------
  data_path_Module : entity infrastructure_lib.data_path_block
    generic map (FPGA_NUMBER => F_FPGA_NUMBER(FLAVOUR))
    port map(
      clk200              => clk200,                -- clk200 genearte fromm the ttc clock of 40MHz
      rx_clk280           => mgt_sel_RXUSRCLK_OUT,  -- Generated rx clocks of 280MHz from the mgt pll.
      clk280              => clk280,                -- clk280 genearte fromm the ttc clock of 40MHz
      reset               => reset,                 -- reset that is active when the 40MHz MMCM is not locked.
      ttc_clk             => clk40,                 -- clk40 genearte fromm the ttc clock of 40MHz
      in_Load             => load,                  --load generated by the 40MHz as 40Hz with duty cycle of 20%
      enable_mgt          => mgt_sel_enable_mgt,    -- enable for the rx_data register before the top_synch block.
      start               => start,
      start_pulse_rst     => start_pulse_rst,
      --IPBus connection
      ipb_clk             => ipb_clk,
      ipb_rst             => rst_ipb,
      ipb_in_algo         => ipbw(N_SLV_EFEX_ALGORITHM),
      ipb_out_algo        => ipbr(N_SLV_EFEX_ALGORITHM),
      ipb_in_sorting      => ipbw(N_SLV_EFEX_SORTING),
      ipb_out_sorting     => ipbr(N_SLV_EFEX_SORTING),
      sel_bcn_or_bc_cnt   => sel_bcn_or_bc_cnt_i ,
      pseudo_orbit        => pseudo_orbit,
      RAW_data            => arr_raw_data_in_i,     -- change to array of 49 x 227b data words
      BC_Reg_sel          => mgt_sel_BC_Reg_sel,
      mux_sel             => mgt_sel_mux_sel,
      delay_num           => mgt_sel_delay_num,
      bc_cntr_0           => mgt_sel_bc_cntr_0,
      bc_cntr_1           => mgt_sel_bc_cntr_1,
      bc_cntr_2           => mgt_sel_bc_cntr_2,
      bc_cntr_3           => mgt_sel_bc_cntr_3,
      bc_mux_cntr_0       => mgt_sel_bc_mux_cntr_0,
      bc_mux_cntr_1       => mgt_sel_bc_mux_cntr_1,
      bc_mux_cntr_2       => mgt_sel_bc_mux_cntr_2,
      bc_mux_cntr_3       => mgt_sel_bc_mux_cntr_3,
      bcn_synch           => mgt_sel_bcn_synch,
      crc_error_chan      => mgt_sel_crc_error_chan,
      rx_resetdone        => mgt_sel_rx_resetdone,
      --debug signal for synch
      data_readout_0      => data_readout_0,
      data_readout_1      => data_readout_1,
      data_readout_2      => data_readout_2,
      data_readout_3      => data_readout_3,
      Reg224_latch        => Reg224_latch,
      ttc_pipe            => ttc_pipe,
      delay_latch         => delay_latch,
      -- out ports eg
      OUT_eg_Sync         => OUT_eg_Sync,
      OUT_eg_Valid        => OUT_eg_Valid,
      OUT_eg_XTOB         => OUT_eg_XTOB,
      OUT_XTOB_BCID       => OUT_XTOB_BCN_i,
      --
      OUT_sorted_eg_TOB   => sorted_eg_TOB,         -- 32b
      OUT_sorted_eg_Sync  => sorted_eg_Start,       -- 1b
      OUT_sorted_eg_Valid => sorted_eg_Valid,       -- 1b
      OUT_TOB_BCID        => sorted_TOB_BCN_i,

      -- out ports tau
      OUT_tau_Sync  => OUT_tau_Sync,
      OUT_tau_Valid => OUT_tau_Valid,
      OUT_tau_XTOB  => OUT_tau_XTOB,

      OUT_sorted_tau_TOB   => sorted_tau_TOB,    -- 32b 
      OUT_sorted_tau_Sync  => sorted_tau_start,  -- 1b  
      OUT_sorted_tau_Valid => sorted_tau_Valid,  -- 1b  

      -- playback ram
      kchar         => mgt_sel_kchar,
      sel_data_in   => control_reg(4),
      ram_data_mgt0 => mgt_sel_ram_data_mgt0,
      ram_data_mgt1 => mgt_sel_ram_data_mgt1,
      ram_data_mgt2 => mgt_sel_ram_data_mgt2,
      ram_data_mgt3 => mgt_sel_ram_data_mgt3,

      ---- MGT signals rx_data and mgt_commadet signals of all rx channells
      MGT_Commadet  => mgt_sel_Commadet,
      MGT_Data      => mgt_sel_Data,
      disperr_error  => mgt_sel_disperr_error,
      notable_error  =>mgt_sel_notable_error
      );


  MGT_SELECTOR_RX (
    fpga_number      => fpga_number,
    in_BC_Reg_sel    => BC_Reg_sel,
    in_mux_sel       => mux_sel,
    in_delay_num     => delay_num,
    in_bc_cntr_0     => bc_cntr_0,
    in_bc_cntr_1     => bc_cntr_1,
    in_bc_cntr_2     => bc_cntr_2,
    in_bc_cntr_3     => bc_cntr_3,
    in_bc_mux_cntr_0 => bc_mux_cntr_0,
    in_bc_mux_cntr_1 => bc_mux_cntr_1,
    in_bc_mux_cntr_2 => bc_mux_cntr_2,
    in_bc_mux_cntr_3 => bc_mux_cntr_3,

    in_rxdata_mgt0 => rxdata_mgt0,
    in_rxdata_mgt1 => rxdata_mgt1,
    in_rxdata_mgt2 => rxdata_mgt2,
    in_rxdata_mgt3 => rxdata_mgt3,

    in_ram_data_mgt0 => ram_data_mgt0,
    in_ram_data_mgt1 => ram_data_mgt1,
    in_ram_data_mgt2 => ram_data_mgt2,
    in_ram_data_mgt3 => ram_data_mgt3,

    in_kchar => kchar_mgt,

    in_mgt_RXUSRCLK_OUT => mgt_RXUSRCLK_OUT,
    in_enable_mgt       => enable_mgt,
    in_bcn_synch        => bcn_synch,
    in_crc_error_chan   => crc_error_chan,
    in_disperr_error     =>    disperr_error_i,
    in_notable_error     =>    notable_error_i,

    in_rx_resetdone => rx_resetdone,

    out_BC_Reg_sel       => mgt_sel_BC_Reg_sel,
    out_mux_sel          => mgt_sel_mux_sel,
    out_delay_num        => mgt_sel_delay_num,
    out_bc_cntr_0        => mgt_sel_bc_cntr_0,
    out_bc_cntr_1        => mgt_sel_bc_cntr_1,
    out_bc_cntr_2        => mgt_sel_bc_cntr_2,
    out_bc_cntr_3        => mgt_sel_bc_cntr_3,
    out_bc_mux_cntr_0    => mgt_sel_bc_mux_cntr_0,
    out_bc_mux_cntr_1    => mgt_sel_bc_mux_cntr_1,
    out_bc_mux_cntr_2    => mgt_sel_bc_mux_cntr_2,
    out_bc_mux_cntr_3    => mgt_sel_bc_mux_cntr_3,
    out_mgt_RXUSRCLK_OUT => mgt_sel_RXUSRCLK_OUT,
    out_enable_mgt       => mgt_sel_enable_mgt,
    out_bcn_synch        => mgt_sel_bcn_synch,
    out_crc_error_chan   => mgt_sel_crc_error_chan,
    out_disperr_error   => mgt_sel_disperr_error,
    out_notable_error  => mgt_sel_notable_error,

    out_rxdata_mgt0 => mgt_sel_rxdata_mgt0,
    out_rxdata_mgt1 => mgt_sel_rxdata_mgt1,
    out_rxdata_mgt2 => mgt_sel_rxdata_mgt2,
    out_rxdata_mgt3 => mgt_sel_rxdata_mgt3,

    out_ram_data_mgt0 => mgt_sel_ram_data_mgt0,
    out_ram_data_mgt1 => mgt_sel_ram_data_mgt1,
    out_ram_data_mgt2 => mgt_sel_ram_data_mgt2,
    out_ram_data_mgt3 => mgt_sel_ram_data_mgt3,

    out_kchar        => mgt_sel_kchar,
    out_rx_resetdone => mgt_sel_rx_resetdone,

    in_MGT_Commadet => MGT_Commadet_int,
    in_MGT_Data     => rxdata_quad_array,

    out_MGT_Commadet => mgt_sel_Commadet,
    out_MGT_Data     => mgt_sel_Data);


  MGT_SELECTOR_TX (

    fpga_number    => fpga_number,
    in_txdata_0    => txdatai_0,
    in_txdata_1    => txdatai_1,
    in_txdata_2    => txdatai_2,
    in_txdata_3    => txdatai_3,
    in_txdata_4    => txdatai_4,
    in_txdata_5    => txdatai_5,
    in_txdata_6    => txdatai_6,
    in_txdata_7    => txdatai_7,
    in_txdata_8    => txdatai_8,
    in_txdata_9    => txdatai_9,
    in_txdata_10   => txdatai_10,
    in_txdata_11   => txdatai_11,
    in_mgt_usr_clk => mgt_TXUSRCLK_OUT,

    in_topo_k    => TOPO_TOB_out_char,
    in_raw_k     => RAW_Data_out_char,
    in_topo_data => TOPO_TOB_out,
    in_raw_data  => RAW_Data_out,

    out_topo_tob_clk => TOB_TXOUTCLK_i,
    out_topo_raw_clk => RAW_TXOUTCLK_i,

    out_txcharisk_quad_array => mgt_txcharisk,
    out_txdata_quad_array    => mgt_txdata,
    out_mgt_usr_clk          => mgt_sel_tx_clk
    );

-------------------------------------------------------------------------------------------------------------------------------
  -- This will use generate statement   to assgin the MGT_Commadet_int, mgt_loopback_in,mgt_rxcharisk_reg,rx_realign,rx_disperr,encode_error
  -- It will generate the status signals of 16  mgts of tx_rx 
--------------------------------------------------------------------------------------------------------------------------------             
  mgt_gen : for i in 0 to 19    --loop

  generate

    MGT_Commadet_int(0+4*i downto 0+4*i) <= mgt_rxchariscomma(i).gt0_rxchariscomma_out(0 downto 0);  -- asginment of the rxchriscomma
    MGT_Commadet_int(1+4*i downto 1+4*i) <= mgt_rxchariscomma(i).gt1_rxchariscomma_out(0 downto 0);  -- asginment of the rxchriscomma
    MGT_Commadet_int(2+4*i downto 2+4*i) <= mgt_rxchariscomma(i).gt2_rxchariscomma_out(0 downto 0);  -- asginment of the rxchriscomma
    MGT_Commadet_int(3+4*i downto 3+4*i) <= mgt_rxchariscomma(i).gt3_rxchariscomma_out(0 downto 0);  -- asginment of the rxchriscomma

    rx_realign(0+4*i) <= mgt_rxbyterealign(i).gt0_rxbyterealign;  -- asignment of rxbyterealign
    rx_realign(1+4*i) <= mgt_rxbyterealign(i).gt1_rxbyterealign;  -- asignment of rxbyterealign
    rx_realign(2+4*i) <= mgt_rxbyterealign(i).gt2_rxbyterealign;  -- asignment of rxbyterealign
    rx_realign(3+4*i) <= mgt_rxbyterealign(i).gt3_rxbyterealign;  -- asignment of rxbyterealign

    rx_resetdone(0+4*i) <= mgt_rx_resetdone(i).gt0_rxresetdone;  -- asignment of rx_resetdone                     
    rx_resetdone(1+4*i) <= mgt_rx_resetdone(i).gt1_rxresetdone;  -- asignment of rx_resetdone
    rx_resetdone(2+4*i) <= mgt_rx_resetdone(i).gt2_rxresetdone;  -- asignment of rx_resetdone
    rx_resetdone(3+4*i) <= mgt_rx_resetdone(i).gt3_rxresetdone;  -- asignment of rx_resetdone

    rxbyteisaligned(0+4*i) <= mgt_rxbyteisaligned(i).gt0_rxbyteisaligned;
    rxbyteisaligned(1+4*i) <= mgt_rxbyteisaligned(i).gt1_rxbyteisaligned;
    rxbyteisaligned(2+4*i) <= mgt_rxbyteisaligned(i).gt2_rxbyteisaligned;
    rxbyteisaligned(3+4*i) <= mgt_rxbyteisaligned(i).gt3_rxbyteisaligned;

    mgt_rxcharisk_reg(3+16*i downto 16*i)     <= mgt_rxcharisk(i).gt0_rxcharisk_out;
    mgt_rxcharisk_reg(7+16*i downto 4+16*i)   <= mgt_rxcharisk(i).gt1_rxcharisk_out;
    mgt_rxcharisk_reg(11+16*i downto 8+16*i)  <= mgt_rxcharisk(i).gt2_rxcharisk_out;
    mgt_rxcharisk_reg(15+16*i downto 12+16*i) <= mgt_rxcharisk(i).gt3_rxcharisk_out;


    rx_disperr(3+16*i downto 16*i)     <= mgt_rxdisperr(i).gt0_rxdisperr;
    rx_disperr(7+16*i downto 4+16*i)   <= mgt_rxdisperr(i).gt1_rxdisperr;
    rx_disperr(11+16*i downto 8+16*i)  <= mgt_rxdisperr(i).gt2_rxdisperr;
    rx_disperr(15+16*i downto 12+16*i) <= mgt_rxdisperr(i).gt3_rxdisperr;

    encode_error(3+16*i downto 16*i)     <= mgt_rxnotintable(i).gt0_rxnotintable;  -- asignment of 10b/8b encoding error- 
    encode_error(7+16*i downto 4+16*i)   <= mgt_rxnotintable(i).gt1_rxnotintable;  -- asignment of 10b/8b encoding error
    encode_error(11+16*i downto 8+16*i)  <= mgt_rxnotintable(i).gt2_rxnotintable;  -- asignment of 10b/8b encoding error
    encode_error(15+16*i downto 12+16*i) <= mgt_rxnotintable(i).gt3_rxnotintable;  -- asignment of 10b/8b encoding error

    MGT_RXN_IN(i).RXN_IN <= RXN_IN(3+4*i downto 4*i);  -- rx input asignment
    MGT_RXP_IN(i).RXP_IN <= RXP_IN(3+4*i downto 4*i);  -- rx input asignment

    mgt_DATA_VALID_IN(i) <= mgt_DATA_VALID_IN_reg(i);

  end generate mgt_gen;

------------------------------------------------------------------------------------------------------------------
  -- This will use generate statament in order to assign tx side signals and control loopbackof the MGT_TX quads               
------------------------------------------------------------------------------------------------------------------  

  internal_mgt_tx_gen : for i in 0 to 19
  generate
    tx_resetdone(0+4*i) <= mgt_tx_resetdone(i).gt0_txresetdone;
    tx_resetdone(1+4*i) <= mgt_tx_resetdone(i).gt1_txresetdone;
    tx_resetdone(2+4*i) <= mgt_tx_resetdone(i).gt2_txresetdone;
    tx_resetdone(3+4*i) <= mgt_tx_resetdone(i).gt3_txresetdone;

    txcharisk(03+16*i downto 00+16*i) <= mgt_txcharisk(i).gt0_txcharisk;
    txcharisk(07+16*i downto 04+16*i) <= mgt_txcharisk(i).gt1_txcharisk;
    txcharisk(11+16*i downto 08+16*i) <= mgt_txcharisk(i).gt2_txcharisk;
    txcharisk(15+16*i downto 12+16*i) <= mgt_txcharisk(i).gt3_txcharisk;

    mgt_loopback_in(i).gt0_loopback_in <= mgt_loopback_reg(2+3*i downto 3*i);
  end generate;

  external_mgt_tx_gen_1 : for i in 0 to F_MGT_USE_OTHER_CLK_N(F_FPGA_NUMBER(FLAVOUR))-1
  generate
    TXN_OUT(3+4*i downto 4*i) <= MGT_TXN_IN(i).TXN_OUT;
    TXP_OUT(3+4*i downto 4*i) <= MGT_TXP_IN(i).TXP_OUT;
  end generate;

  TXN_OUT(77 downto 76) <= MGT_TXN_IN(F_MGT_USE_OTHER_CLK_N(F_FPGA_NUMBER(FLAVOUR))).TXN_OUT(1 downto 0);
  TXP_OUT(77 downto 76) <= MGT_TXP_IN(F_MGT_USE_OTHER_CLK_N(F_FPGA_NUMBER(FLAVOUR))).TXP_OUT(1 downto 0);

  external_mgt_tx_gen_2 : for i in F_MGT_USE_OTHER_CLK_N(F_FPGA_NUMBER(FLAVOUR))+1 to 19
  generate
    TXN_OUT(3+4*(i-1) downto 4*(i-1)) <= MGT_TXN_IN(i).TXN_OUT;
    TXP_OUT(3+4*(i-1) downto 4*(i-1)) <= MGT_TXP_IN(i).TXP_OUT;
  end generate;

---------------------------------------------------------------------------------------
---- signal assignments of the 20 mgts tx_rx
--------------------------------------------------------------------------------------
  MGT_TX_RX : entity infrastructure_lib.MGT_4_quad_gen
    generic map(QUAD_ENABLE => F_MGT_QUAD_ENABLE(F_FPGA_NUMBER(FLAVOUR)))
    port map (
      clk280                     => clk280,
      TTC_CLK                    => clk40,
      MGT_CLK_GTREFCLK_PAD_N_IN  => Q_CLK_GTREFCLK_PAD_N_IN,
      MGT_CLK_GTREFCLK_PAD_P_IN  => Q_CLK_GTREFCLK_PAD_P_IN,
      mgt_TXUSRCLK_OUT           => mgt_TXUSRCLK_OUT,
      mgt_RXUSRCLK_OUT           => mgt_RXUSRCLK_OUT,
      mgt_SOFT_RESET_TX_IN       => mgt_SOFT_RESET_TX_IN,
      mgt_SOFT_RESET_RX_IN       => mgt_SOFT_RESET_RX_IN,
      -- data                    
      RXN_IN                     => MGT_RXN_IN,
      RXP_IN                     => MGT_RXP_IN,
      TXN_IN                     => MGT_TXN_IN,
      TXP_IN                     => MGT_TXP_IN,
      rxdata_quad_array          => rxdata_quad_array,
      txdata_quad_array          => mgt_txdata,
      -- status and monitoring
      gt_rxpd_array              => gt_rxpd,
      gt_txpd_array              => gt_txpd,
      mgt_DATA_VALID_IN          => mgt_DATA_VALID_IN,
      mgt_TX_FSM_RESET_DONE      => mgt_tx_fsm_resetdone,
      mgt_RX_FSM_RESET_DONE      => mgt_rx_fsm_resetdone,
      rxbyteisaligned_quad_array => mgt_rxbyteisaligned,
      rxresetdone_quad_array     => mgt_rx_resetdone,
      txresetdone_quad_array     => mgt_tx_resetdone,
      loopback_quad_array        => mgt_loopback_in,
      rxchariscomma_quad_array   => mgt_rxchariscomma,
      rxcharisk_quad_array       => mgt_rxcharisk,
      txcharisk_quad_array       => mgt_txcharisk,
      txbufstatus_quad_array     => mgt_txbufstatus,
      rxbyterealign_quad_array   => mgt_rxbyterealign,
      rxcommadet_quad_array      => mgt_rxcommadet,
      rxdisperr_quad_array       => mgt_rxdisperr(19 downto 0),  --what about the other bits?
      rxnotintable_quad_array    => mgt_rxnotintable(19 downto 0),
      mgt_QPLLREFCLKLOST_OUT     => mgt_QPLLREFCLKLOST_OUT,
      mgt_QPLLLOCK_OUT           => mgt_QPLLLOCK_OUT
      );



-----------------------------------------------------------------------------------
--- MGT  tx_rx ipbus connections
------------------------------------------------------------------------------------
  MGT_ipb : entity infrastructure_lib.mgt_slaves
    generic map (
      MGT_QUAD_ENABLE   => F_MGT_QUAD_ENABLE(F_FPGA_NUMBER(FLAVOUR)),
      MGT_USE_OTHER_CLK => F_MGT_USE_OTHER_CLK(F_FPGA_NUMBER(FLAVOUR)),
      MGT_TX_POWER      => F_MGT_TX_POWER(F_FPGA_NUMBER(FLAVOUR)),
      MGT_RX_POWER      => F_MGT_RX_POWER(F_FPGA_NUMBER(FLAVOUR))
      )
    port map (
      clk280_rx        => mgt_RXUSRCLK_OUT,
      clk280_tx        => mgt_TXUSRCLK_OUT,
      BCR_in           => BCR_i,
      clk40            => clk40,
      ipb_clk          => ipb_clk,
      ipb_rst          => rst_ipb,
      ipb_in           => ipbw(N_SLV_EFEX_MGT_TOP),
      ipb_out          => ipbr(N_SLV_EFEX_MGT_TOP),
      --mgt quad          
      bc_cntr_0        => bc_cntr_0,
      bc_cntr_1        => bc_cntr_1,
      bc_cntr_2        => bc_cntr_2,
      bc_cntr_3        => bc_cntr_3,
      bc_mux_cntr_0    => bc_mux_cntr_0,
      bc_mux_cntr_1    => bc_mux_cntr_1,
      bc_mux_cntr_2    => bc_mux_cntr_2,
      bc_mux_cntr_3    => bc_mux_cntr_3,
      delay_cntr       => delay_num,
      mgt_enable       => enable_mgt,
      phase_mux        => phase_mux,
      error_counter    => error_counter,
      loopback         => mgt_loopback_reg,
      bc_reg_sel       => bc_reg_sel,
      mux_sel          => mux_sel,
      softreset_tx     => mgt_SOFT_RESET_TX_IN,
      softreset_rx     => mgt_SOFT_RESET_RX_IN,
      qpll_lock        => mgt_QPLLLOCK_OUT,
      qpll_refclklost  => mgt_QPLLREFCLKLOST_OUT,
      rx_resetdone     => rx_resetdone,
      rx_fsm_resetdone => mgt_rx_fsm_resetdone,
      rx_byteisaligned => rxbyteisaligned,
      crc_error_chan   => crc_error_chan,
      tx_resetdone     => tx_resetdone,
      tx_fsm_resetdone => mgt_tx_fsm_resetdone,
      tx_bufstatus     => tx_bufstatus,
      rx_realign       => rx_realign,
      kchar_mgt        => kchar_mgt,
      rxdata_mgt0      => rxdata_mgt0,
      ram_data_mgt0    => ram_data_mgt0,
      rxdata_mgt1      => rxdata_mgt1,
      ram_data_mgt1    => ram_data_mgt1,
      rxdata_mgt2      => rxdata_mgt2,
      ram_data_mgt2    => ram_data_mgt2,
      rxdata_mgt3      => rxdata_mgt3,
      ram_data_mgt3    => ram_data_mgt3,
      rx_disperr       => rx_disperr,   --(63 downto 0),
      encode_error     => encode_error,  --(63 downto 0)
      
      disperr_error   => disperr_error_i,
      notable_error   => notable_error_i
       
      );

--  ILA_disperr_error : ila_ipbus_fabric_rd_wr
--    port map (
--      clk                  => clk280,
--      probe0               => disperr_error_i(35 downto 0),  --36b 
--      probe1               => disperr_error_i(71 downto 36),  --36b 
--      probe2(0)            => L1A_i,  -- 1b
--      probe3(0)            => '0',  -- 1b
--      probe4(0)            => '0',  -- 1b
--      probe5(7 downto 0)   => disperr_error_i(79 downto 72),  --36b 
--      probe5(35 downto 8)  => (others => '0'),  --36b 
--      probe6               => (others => '0'),        --36b
--      probe7(0)            => '0',            -- 1b
--      probe8(0)            => BCR_i,  -- 1b
--      probe9(0)            => '0'   -- 1b
--      );

--  ILA_notable_error : ila_ipbus_fabric_rd_wr
--    port map (
--      clk                  => clk280,
--      probe0               => notable_error_i(35 downto 0),  --36b 
--      probe1               => notable_error_i(71 downto 36),  --36b 
--      probe2(0)            => L1A_i,  -- 1b
--      probe3(0)            => '0',  -- 1b
--      probe4(0)            => '0',  -- 1b
--      probe5(7 downto 0)   => notable_error_i(79 downto 72),  --36b 
--      probe5(35 downto 8)  => (others => '0'),  --36b 
--      probe6               => (others => '0'),        --36b
--      probe7(0)            => '0',            -- 1b
--      probe8(0)            => BCR_i,  -- 1b
--      probe9(0)            => '0'   -- 1b
--      );


------------------------------------------------------------------
--global merge signals
  GLOBAL_MERGE : if FLAVOUR = 1 or FLAVOUR = 2 generate

    TOB_BCN_sync_out_i <= TOB_BCN_sync_eg_i when FLAVOUR =1 else TOB_BCN_sync_tau_i;   
    sorted_Start_sel <= sorted_eg_Start when FLAVOUR = 1 else sorted_tau_Start;
    sorted_TOB_sel   <= sorted_eg_TOB   when FLAVOUR = 1 else sorted_tau_TOB;
    sorted_Valid_sel <= sorted_eg_Valid when FLAVOUR = 1 else sorted_tau_Valid;


    
    proc_clk_280 : process (clk280)
    begin
      if rising_edge (clk280) then
        data_merge_1       <= '0' & sorted_TOB_sel;
        data_merge_2       <= data_from_fpga_A;
        data_merge_3       <= data_from_fpga_B;
        data_merge_4       <= data_from_fpga_C;
        sorted_Valid_sel_1 <= sorted_Valid_sel;  -- delay by 1 clk to match merged data
        sorted_Start_sel_1 <= sorted_Start_sel;  -- delay by 1 clk to match merged data

        data_merge(0) <= data_merge_1(31 downto 0);  -- pipeline 1 tick
        data_merge(1) <= data_merge_2(31 downto 0);  -- pipeline 1 tick
        data_merge(2) <= data_merge_3(31 downto 0);  -- pipeline 1 tick
        data_merge(3) <= data_merge_4(31 downto 0);  -- pipeline 1 tick

        data_merge_BCN_A_i <= data_merge_2(32);  -- pipeline 1 tick
        data_merge_BCN_B_i <= data_merge_3(32);  -- pipeline 1 tick
        data_merge_BCN_C_i <= data_merge_4(32);  -- pipeline 1 tick

        sorted_Valid_sel_i <= sorted_Valid_sel_1;  -- pipeline 1 tick
        sorted_Start_sel_i <= sorted_Start_sel_1;  -- pipeline 1 tick
      end if;
    end process;

    Merging_Module : entity algolib.IPBusTopMergingModule  -- merging module of TOBs
      port map (
        CLK => clk280,

        IN_Data     => data_merge,          --! merge data from fpga2, fpga3 and fpga4
        IN_BCN_sync => TOB_BCN_sync_out_i&data_merge_BCN_C_i&data_merge_BCN_B_i&data_merge_BCN_A_i,
        IN_Sync     => sorted_Start_sel_i,  --! start eg from fpga1

        IN_local_BCN => sorted_TOB_BCN_i,
        OUT_merged_BCN => merged_TOB_BCN_i,
        
        --IPBus connection
        ipb_clk   => ipb_clk,
        ipb_rst   => rst_ipb,
        ipb_in    => ipbw(N_SLV_EFEX_MERGING),  --! ipbus connection
        ipb_out   => ipbr(N_SLV_EFEX_MERGING),  --! ipbus connection
        OUT_Sync  => sorted_merged_Start,       --!sorted and merged start
        OUT_Valid => sorted_merged_Valid,       --! sorted and merged valid
        OUT_TOB   => sorted_merged_TOB          --! sorted and merged Tob data
        );

    --------------------------------------------------------------------------------------
    ---- tob data phase adjusting with tx clock
    ----------------------------------------------------------------------------------------
    tx_phase_adjust : entity infrastructure_lib.efex_topo_tx  --top_tx_alignment
      generic map (NCOUNTERS => 12)
      port map(
        clk280           => clk280,
        reset            => control_reg(5),  --! clock 280MHz
        tx_clk           => mgt_sel_tx_clk,
        rst              => reset,
        bcn              => merged_TOB_BCN_i(3 downto 0),     -- bcn from the merging block
        sorted_sync      => T_TOB_sync_in_i,                  --sorted_merged_Start,          --! Output sync, high on the first clock cycle of the BC
        sorted_synch_int => sorted_synch_int,
        tob_data         => T_TOB_32b_in_i,  --sorted_merged_TOB,            --! Algorithm external TOB data structure, defined in AlgoDataTypes.vhd
        sorted_valid     => T_TOB_wr_in_i,   --sorted_merged_Valid,          --! Output data valid, high when correspondent output data are valid
        tx_datai_0       => txdatai_0,       --!txdata of quad 111 mgt0
        tx_datai_1       => txdatai_1,       --!txdata of quad 111 mgt1
        tx_datai_2       => txdatai_2,       --!txdata of quad 111 mgt2
        tx_datai_3       => txdatai_3,       --!txdata of quad 111 mgt3
        tx_datai_4       => txdatai_4,       --!txdata of quad 112 mgt0
        tx_datai_5       => txdatai_5,       --!txdata of quad 112 mgt1
        tx_datai_6       => txdatai_6,       --!txdata of quad 112 mgt2
        tx_datai_7       => txdatai_7,       --!txdata of quad 112 mgt3
        tx_datai_8       => txdatai_8,       --!txdata of quad 211 mgt0
        tx_datai_9       => txdatai_9,       --!txdata of quad 211 mgt1
        tx_datai_10      => txdatai_10,      --!txdata of quad 211 mgt2
        tx_datai_11      => txdatai_11       --!txdata of quad 211 mgt3
        );


  IO_DELAY_A1 : io_delay
    port map
    (
      data_in_from_pins_p => data_from_fpga_A_p(15 downto 0),
      data_in_from_pins_n => data_from_fpga_A_n(15 downto 0),
      data_in_to_device   => data_from_fpga_A(15 downto 0),

      delay_clk                     => clk280,
      in_delay_reset                => tob_delay_reg(24),
      in_delay_data_ce              => (others => '0'),
      in_delay_data_inc             => (others => '0'),
      in_delay_tap_in               => tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&
      tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&
      tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&
      tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0),
      in_delay_tap_out(4 downto 0)  => tob_delay_status(4 downto 0),
      in_delay_tap_out(79 downto 5) => dummy1,
      delay_locked                  => tob_delay_status(24),
      ref_clock                     => clk200,
      clk_in                        => clk280,
      io_reset                      => tob_delay_reg(28)
      );
  IO_DELAY_A2 : io_delay
    port map
    (
      data_in_from_pins_p => data_from_fpga_A_p(31 downto 16),
      data_in_from_pins_n => data_from_fpga_A_n(31 downto 16),
      data_in_to_device   => data_from_fpga_A(31 downto 16),

      delay_clk         => clk280,
      in_delay_reset    => tob_delay_reg(24),
      in_delay_data_ce  => (others => '0'),
      in_delay_data_inc => (others => '0'),
      in_delay_tap_in   => tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&
      tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&
      tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&
      tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0)&tob_delay_reg(4 downto 0),
      in_delay_tap_out  => open,

      delay_locked => tob_delay_status(25),
      ref_clock    => clk200,
      clk_in       => clk280,
      io_reset     => tob_delay_reg(28)
      );

-----B
  IO_DELAY_B1 : io_delay
    port map
    (
      data_in_from_pins_p => data_from_fpga_B_p(15 downto 0),
      data_in_from_pins_n => data_from_fpga_B_n(15 downto 0),
      data_in_to_device   => data_from_fpga_B(15 downto 0),

      delay_clk                     => clk280,
      in_delay_reset                => tob_delay_reg(24),
      in_delay_data_ce              => (others => '0'),
      in_delay_data_inc             => (others => '0'),
      in_delay_tap_in               => tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&
      tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&
      tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&
      tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8),
      in_delay_tap_out(4 downto 0)  => tob_delay_status(12 downto 8),
      in_delay_tap_out(79 downto 5) => dummy2,
      delay_locked                  => tob_delay_status(26),
      ref_clock                     => clk200,
      clk_in                        => clk280,
      io_reset                      => tob_delay_reg(28)
      );
  IO_DELAY_B2 : io_delay
    port map
    (
      data_in_from_pins_p => data_from_fpga_B_p(31 downto 16),
      data_in_from_pins_n => data_from_fpga_B_n(31 downto 16),
      data_in_to_device   => data_from_fpga_B(31 downto 16),

      delay_clk         => clk280,
      in_delay_reset    => tob_delay_reg(24),
      in_delay_data_ce  => (others => '0'),
      in_delay_data_inc => (others => '0'),
      in_delay_tap_in   => tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&
      tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&
      tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&
      tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8)&tob_delay_reg(12 downto 8),
      in_delay_tap_out  => open,

      delay_locked => tob_delay_status(27),
      ref_clock    => clk200,
      clk_in       => clk280,
      io_reset     => tob_delay_reg(28)
      );

-----C
  IO_DELAY_C1 : io_delay
    port map
    (
      data_in_from_pins_p => data_from_fpga_C_p(15 downto 0),
      data_in_from_pins_n => data_from_fpga_C_n(15 downto 0),
      data_in_to_device   => data_from_fpga_C(15 downto 0),

      delay_clk                     => clk280,
      in_delay_reset                => tob_delay_reg(24),
      in_delay_data_ce              => (others => '0'),
      in_delay_data_inc             => (others => '0'),
      in_delay_tap_in               => tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&
      tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&
      tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&
      tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16),
      in_delay_tap_out(4 downto 0)  => tob_delay_status(20 downto 16),
      in_delay_tap_out(79 downto 5) => dummy3,

      delay_locked => tob_delay_status(28),
      ref_clock    => clk200,
      clk_in       => clk280,
      io_reset     => tob_delay_reg(28)
      );

  IO_DELAY_C2 : io_delay
    port map
    (
      data_in_from_pins_p => data_from_fpga_C_p(31 downto 16),
      data_in_from_pins_n => data_from_fpga_C_n(31 downto 16),
      data_in_to_device   => data_from_fpga_C(31 downto 16),

      delay_clk         => clk280,
      in_delay_reset    => tob_delay_reg(24),
      in_delay_data_ce  => (others => '0'),
      in_delay_data_inc => (others => '0'),
      in_delay_tap_in   => tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&
      tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&
      tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&
      tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16)&tob_delay_reg(20 downto 16),
      in_delay_tap_out  => open,

      delay_locked => tob_delay_status(29),
      ref_clock    => clk200,
      clk_in       => clk280,
      io_reset     => tob_delay_reg(28)
      );

  IO_DELAY_BC_A : io_delay2
    port map
    (
      data_in_from_pins_p(0) => data_from_fpga_A_p(32),
      data_in_from_pins_n(0) => data_from_fpga_A_n(32),
      data_in_to_device(0)   => data_from_fpga_A(32),

      delay_clk         => clk280,
      in_delay_reset    => tob_bc_reg(24),
      in_delay_data_ce  => (others => '0'),
      in_delay_data_inc => (others => '0'),
      in_delay_tap_in   => tob_bc_reg(4 downto 0),
      in_delay_tap_out  => tob_bc_status(4 downto 0),
      delay_locked      => tob_bc_status(24),
      ref_clock         => clk200,
      clk_in            => clk280,
      io_reset          => tob_delay_reg(28)
      );

  IO_DELAY_BC_B : io_delay2
    port map
    (
      data_in_from_pins_p(0) => data_from_fpga_B_p(32),
      data_in_from_pins_n(0) => data_from_fpga_B_n(32),
      data_in_to_device(0)   => data_from_fpga_B(32),

      delay_clk         => clk280,
      in_delay_reset    => tob_bc_reg(25),
      in_delay_data_ce  => (others => '0'),
      in_delay_data_inc => (others => '0'),
      in_delay_tap_in   => tob_bc_reg(12 downto 8),
      in_delay_tap_out  => tob_bc_status(12 downto 8),
      delay_locked      => tob_bc_status(25),
      ref_clock         => clk200,
      clk_in            => clk280,
      io_reset          => tob_delay_reg(28)
      );

  IO_DELAY_BC_C : io_delay2
    port map
    (
      data_in_from_pins_p(0) => data_from_fpga_C_p(32),
      data_in_from_pins_n(0) => data_from_fpga_C_n(32),
      data_in_to_device(0)   => data_from_fpga_C(32),

      delay_clk         => clk280,
      in_delay_reset    => tob_bc_reg(26),
      in_delay_data_ce  => (others => '0'),
      in_delay_data_inc => (others => '0'),
      in_delay_tap_in   => tob_bc_reg(20 downto 16),
      in_delay_tap_out  => tob_bc_status(20 downto 16),
      delay_locked      => tob_bc_status(26),
      ref_clock         => clk200,
      clk_in            => clk280,
      io_reset          => tob_delay_reg(28)
      );

  end generate GLOBAL_MERGE;
------------------------------------------------------------------------

--  resetbc_cntr <= mgt_SOFT_RESET_RX_IN(0) or mgt_SOFT_RESET_RX_IN(0);

--  ILA_bcn_l1A_id : ila_ipbus_fabric_rd_wr
--    port map (
--      clk                  => clk40,
--      probe0               => (others => '0'),  --36b 
--      probe1               => (others => '0'),  --36b 
--      probe2(0)            => L1A_i,            -- 1b
--      probe3               => (others => '0'),  -- 1b
--      probe4               => (others => '0'),  -- 1b
--      probe5               => (others => '0'),  --36b 
--      probe6(11 downto 0)  => bcn_cntr,         --36b
--      probe6(31 downto 12) => (others => '0'),  --36b
--      probe7(0)            => BCR_i,            -- 1b
--      probe8               => (others => '0'),         -- 1b
--      probe9(0)            => ECR_i             -- 1b
--      );


--  scope : ila_0
--    port map (
--      clk                    => clk280,
--      probe0(31 downto 0)    => TOPO_TOB_out,
--      probe0(63 downto 32)   => RAW_Data_out,
--      probe0(64)              => RAW_Data_out_char,
--      probe0(65)             => TOPO_TOB_out_char
--    );


--  scope : ila_0
--    port map (
--      clk                    => clk280,
--      probe0(31 downto 0)    => txdatai_0(31 downto 0),
--      probe0(63 downto 32)   => T_TOB_32b_in_i, 
--      probe0(64)             => txdatai_0(32),
--      probe0(65)             => T_TOB_sync_in_i,
--      probe0 (66)            => T_TOB_wr_in_i,
--      probe0 (70 downto 67)  => sorted_TOB_BCN_i(3 downto 0),
--      probe0(71)             => sorted_synch_int  

--      );

end Behavioral;
