--! @file
--! @brief MGT ipbus control
--! @details 
--! Module to connect all the MGTs to the ipbus registers
--! @author Mohammed Syiad
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
library work;
library ipbus_lib;
use ipbus_lib.ipbus.all;
use work.ipbus_decode_efex_mgt_top.all;

--!@copydoc mgt_slaves.vhd
entity mgt_slaves is
  generic (
    MGT_QUAD_ENABLE   : std_logic_vector(19 downto 0) := x"00000";
    MGT_USE_OTHER_CLK : std_logic_vector(19 downto 0) := x"00000";
    MGT_TX_POWER      : std_logic_vector(79 downto 0) := (others => '0');
    MGT_RX_POWER      : std_logic_vector(79 downto 0) := (others => '0')
    );
  port (
    clk280_tx   : in  std_logic_vector(79 downto 0);
    clk280_rx   : in  std_logic_vector(79 downto 0);
    clk40    : in  std_logic;
    BCR_in   : in  std_logic;
    ipb_clk  : in  std_logic;
    ipb_rst  : in  std_logic;
    ipb_in   : in  ipb_wbus;
    ipb_out  : out ipb_rbus;
    --mgt quad
    loopback : out std_logic_vector(59 downto 0);  -- 3 bits per quad

    softreset_tx    : out std_logic_vector(19 downto 0);
    softreset_rx    : out std_logic_vector(19 downto 0);
    mgt_enable      : out std_logic_vector(79 downto 0);
    qpll_lock       : in  std_logic_vector(19 downto 0);
    qpll_refclklost : in  std_logic_vector(19 downto 0);
    phase_mux       : out std_logic_vector(319 downto 0);
    -- error counter
    error_counter   : in  std_logic_vector(1279 downto 0);  --was only 191, only
                                                            --first were
                                                            --connected (32 * 16 bits)
    -- bc couners
    bc_cntr_0       : in  std_logic_vector(139 downto 0);
    bc_cntr_1       : in  std_logic_vector(139 downto 0);
    bc_cntr_2       : in  std_logic_vector(139 downto 0);
    bc_cntr_3       : in  std_logic_vector(139 downto 0);
    bc_mux_cntr_0   : in  std_logic_vector(139 downto 0);
    bc_mux_cntr_1   : in  std_logic_vector(139 downto 0);
    bc_mux_cntr_2   : in  std_logic_vector(139 downto 0);
    bc_mux_cntr_3   : in  std_logic_vector(139 downto 0);

    --gt  channel
    delay_cntr       : in  std_logic_vector (319 downto 0);
    BC_Reg_sel       : out std_logic_vector (319 downto 0);
    mux_sel          : out std_logic_vector (319 downto 0);
    rx_resetdone     : in  std_logic_vector (79 downto 0);
    rx_fsm_resetdone : in  std_logic_vector (79 downto 0);
    rx_byteisaligned : in  std_logic_vector (79 downto 0);
    crc_error_chan   : in  std_logic_vector (79 downto 0);
    tx_resetdone     : in  std_logic_vector (79 downto 0);
    tx_fsm_resetdone : in  std_logic_vector (79 downto 0);
    tx_bufstatus     : in  std_logic_vector (159 downto 0);
    rx_realign       : in  std_logic_vector (79 downto 0);
    rx_disperr       : in  std_logic_vector (319 downto 0);
    encode_error     : in  std_logic_vector (319 downto 0);
    
    kchar_mgt         : out std_logic_vector (79 downto 0);    
 --   rxdata_mgt       : in  std_logic_vector (2687 downto 0);    
 --   ram_data_mgt     : out std_logic_vector (2687 downto 0) 
    rxdata_mgt0       : in  std_logic_vector (639 downto 0);  
    rxdata_mgt1       : in  std_logic_vector (639 downto 0);      
    rxdata_mgt2       : in  std_logic_vector (639 downto 0);                                             
    rxdata_mgt3       : in  std_logic_vector (639 downto 0);
    ram_data_mgt0     : out std_logic_vector (639 downto 0); 
    ram_data_mgt1     : out std_logic_vector (639 downto 0); 
    ram_data_mgt2     : out std_logic_vector (639 downto 0);     
    ram_data_mgt3     : out std_logic_vector (639 downto 0);
    
    disperr_error     : out std_logic_vector (79 downto 0);
    notable_error     : out std_logic_vector (79 downto 0)

    );

end mgt_slaves;

--!@copydoc mgt_slaves.vhd
architecture Behavioral of mgt_slaves is

--constant NSLV: positive := 16; 
  signal ipbw         : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal ipbr, ipbr_d : ipb_rbus_array(N_SLAVES-1 downto 0);
  signal clock        : std_logic_vector(79 downto 0);

  -- ipbus number assignment, here we link the constants defined in the IPbus address map with the array used in the for generate
  -- we should try and keep this the same for every FPGA, but if that it not possible we can define it in a different file and
  -- make it FPGA dependent

  type MGT_IPBUS_NUMBER is array(19 downto 0) of integer;
  constant mgt_n : MGT_IPBUS_NUMBER := (N_SLV_MGT_219, N_SLV_MGT_218, N_SLV_MGT_217, N_SLV_MGT_216, N_SLV_MGT_215,
                                        N_SLV_MGT_214, N_SLV_MGT_213, N_SLV_MGT_212, N_SLV_MGT_211, N_SLV_MGT_210,
                                        N_SLV_MGT_119, N_SLV_MGT_118, N_SLV_MGT_117, N_SLV_MGT_116, N_SLV_MGT_115,
                                        N_SLV_MGT_114, N_SLV_MGT_113, N_SLV_MGT_112, N_SLV_MGT_111, N_SLV_MGT_110);

begin

  mgt_fabric : entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV      => N_SLAVES,  --defined in ipbus_decode_mgt_slvs
                SEL_WIDTH => ipbus_sel_width)
    port map(
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      sel             => ipbus_sel_efex_mgt_top(ipb_in.ipb_addr),
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      );

  QUAD_FOR : for i in 0 to 19 generate
    
    -- Use rx clock in case the MGT is using the other clock and so it is transmitter only
    -- Maybe this should be done using the rx power instead as the new board will not have any slow MGT
    clock(i*4+3 downto i*4) <= Clk280_rx(i*4+3 downto i*4) when MGT_USE_OTHER_CLK(i) = '0' else
                               Clk280_tx(i*4+3 downto i*4);
    
    quad : entity work.mgt_quad_slaves
      generic map(ENABLE => MGT_QUAD_ENABLE(i)
                  )
      port map (
        clk280           => clock(i*4+3 downto i*4),
        clk40            => clk40,
        BCR_in           => BCR_in,
        ipb_clk          => ipb_clk,
        ipb_rst          => ipb_rst,
        ipb_in           => ipbw(mgt_n(i)),
        ipb_out          => ipbr(mgt_n(i)),
        tx_pd            => MGT_TX_POWER(i*4+3 downto i*4),      --79
        rx_pd            => MGT_RX_POWER(i*4+3 downto i*4),      --79
        loopback         => loopback(i*3+2 downto i*3),          --59
        error_counter    => error_counter(i*64+63 downto i*64),  --1279
        bc_reg_sel       => bc_reg_sel(i*16+15 downto i*16),     --319
        mux_sel          => mux_sel(i*16+15 downto i*16),        --319
        bc_cntr_0        => bc_cntr_0(i*7+6 downto i*7),         --99
        bc_cntr_1        => bc_cntr_1(i*7+6 downto i*7),         --99
        bc_cntr_2        => bc_cntr_2(i*7+6 downto i*7),         --99
        bc_cntr_3        => bc_cntr_3(i*7+6 downto i*7),         --99
        bc_mux_cntr_0    => bc_mux_cntr_0(i*7+6 downto i*7),     --99
        bc_mux_cntr_1    => bc_mux_cntr_1(i*7+6 downto i*7),     --99
        bc_mux_cntr_2    => bc_mux_cntr_2(i*7+6 downto i*7),     --99
        bc_mux_cntr_3    => bc_mux_cntr_3(i*7+6 downto i*7),     --99
        delay_cntr_0     => delay_cntr(i*16+3 downto i*16+0),    --319
        delay_cntr_1     => delay_cntr(i*16+7 downto i*16+4),    --319
        delay_cntr_2     => delay_cntr(i*16+11 downto i*16+8),   --319
        delay_cntr_3     => delay_cntr(i*16+15 downto i*16+12),  --319
        softreset_tx     => softreset_tx(i),                     --19
        softreset_rx     => softreset_rx(i),                     --19
        mgt_enable       => mgt_enable (i*4+3 downto i*4),       --79
        phase_mux        => phase_mux(i*16+15 downto i*16),      --319
        qpll_lock        => qpll_lock(i),                        --19
        qpll_refclklost  => qpll_refclklost(i),                  --19
        rx_resetdone     => rx_resetdone(i*4+3 downto i*4),      --79
        rx_fsm_resetdone => rx_fsm_resetdone(i*4+3 downto i*4),  --79
        rx_byteisaligned => rx_byteisaligned(i*4+3 downto i*4),  --79
        tx_resetdone     => tx_resetdone(i*4+3 downto i*4),      --79
        tx_fsm_resetdone => tx_fsm_resetdone(i*4+3 downto i*4),  --79
        tx_bufstatus     => tx_bufstatus(i*8+7 downto i*8),
        rx_realign       => rx_realign(i*4+3 downto i*4),        --79
        crc_error        => crc_error_chan(i*4+3 downto i*4),    --79
        rx_disperr       => rx_disperr(i*16+15 downto i*16),     --319
        encode_error     => encode_error(i*16+15 downto i*16),   --319
       
        kchar_gt         => kchar_mgt   (i*4+3 downto i*4),       --79

        rxdata_gt0       => rxdata_mgt0 (i*32+31 downto i*32),   --639   
        rxdata_gt1       => rxdata_mgt1 (i*32+31 downto i*32),   --639
        rxdata_gt2       => rxdata_mgt2 (i*32+31 downto i*32),   --639   
        rxdata_gt3       => rxdata_mgt3 (i*32+31 downto i*32),   --639  
                                                                
        ram_data_gt0     => ram_data_mgt0(i*32+31 downto i*32),  --639
        ram_data_gt1     => ram_data_mgt1(i*32+31 downto i*32),  --639  
        ram_data_gt2     => ram_data_mgt2(i*32+31 downto i*32),  --639     
        ram_data_gt3     => ram_data_mgt3(i*32+31 downto i*32),  --639
        
        disperr_error    => disperr_error (i*4+3 downto i*4),
        notable_error    => notable_error (i*4+3 downto i*4)
        );
  end generate;



end Behavioral;
