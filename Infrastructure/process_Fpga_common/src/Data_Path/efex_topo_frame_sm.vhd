LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

ENTITY efex_topo_frame_sm IS
   PORT( 
      bcn        : IN     std_logic_vector (3 DOWNTO 0);
      clk        : IN     std_logic;
      crc_start  : out    std_logic;
      crc        : IN     std_logic_vector (8 DOWNTO 0);
      reset      : IN     std_logic;
      tobo_data  : IN     std_logic_vector (31 DOWNTO 0);
      tobo_data_reg : IN     std_logic_vector (31 DOWNTO 0);
      tobo_sync  : IN     std_logic;
      tobo_valid : IN     std_logic;
      crc_data   : OUT    std_logic_vector (31 DOWNTO 0);
      tx_ctrl    : OUT    std_logic;
      tx_data    : OUT    std_logic_vector (31 DOWNTO 0)
      
   );



END ENTITY efex_topo_frame_sm ;
 
ARCHITECTURE fsm OF efex_topo_frame_sm IS
 
   -- Architecture Declarations
   signal cntr : unsigned(3 downto 0);
      constant fexid  : std_logic_vector(1 downto 0):="01";
      constant fexfld : std_logic_vector(8 downto 0):= (others => '0');
  TYPE STATE_TYPE IS (
      idle,
      empty,
      trialer_0,
      kchar,
      empty_1,
      str,
      wait0,
      wait1,
      empty_data,
      fin,
      wait_2,
      wait_3,
      trialer_1,
      fin1
   );
 
   -- Declare current and next state signals
   SIGNAL current_state : STATE_TYPE;

BEGIN

   -----------------------------------------------------------------
   clocked_proc : PROCESS ( 
      clk,
      reset
   )
   -----------------------------------------------------------------
   BEGIN
      IF (reset = '1') THEN
         current_state <= idle;
      ELSIF (clk'EVENT AND clk = '1') THEN

         -- Combined Actions
         CASE current_state IS
            WHEN idle => 
               cntr <= (others => '0');
               tx_data <= x"00000000";
               crc_data<= (others => '0');
               tx_ctrl <= '0';
               crc_start<= '0';
               IF ((tobo_sync = '1' AND tobo_valid = '0')) THEN 
                  crc_start <= '1';
                  current_state <= empty;
               ELSIF (tobo_valid ='1') THEN 
                  crc_start <= '1' ;
                  current_state <= str;
               ELSE
                  current_state <= idle;
               END IF;
            WHEN empty => 
               cntr<= cntr+1;
               tx_data <= x"00000000";
               tx_ctrl <= '0';
               crc_data<= x"00000000";
               crc_start <='0';
               IF ((cntr = 4)) THEN 
                  current_state <= kchar;
               ELSE
                  current_state <= empty;
               END IF;
            WHEN trialer_0 => 
               cntr <= cntr +1 ;
               crc_start <= '0';
               tx_data <= crc & fexfld & fexid & bcn & x"bc";
               tx_ctrl <= '1' ;
               crc_data <= (others => '0');
               current_state <= empty;
            WHEN kchar => 
               cntr <= (others => '0') ;
               crc_data <= x"00" &'0'& fexfld & fexid & bcn & x"00" ;
               tx_data <= x"00000000" ;
               tx_ctrl <= '0' ;
               current_state <= empty_1;
            WHEN empty_1 => 
               cntr <= (others => '0') ;
               crc_data<= tobo_data; 
               tx_data <= x"00000000" ;
               tx_ctrl <= '0' ;
               crc_start <= '1';
               IF (tobo_valid = '0') THEN 
                  current_state <= trialer_0;
               ELSIF (tobo_valid = '1') THEN 
                  current_state <= trialer_1;
               ELSE
                  current_state <= empty_1;
               END IF;
            WHEN str => 
               crc_start <= '0';
               crc_data<= tobo_data; 
               tx_data <= tobo_data_reg ;
               tx_ctrl <= '0' ;
               cntr <= cntr +1;
               IF (tobo_valid ='0' and cntr = 4) THEN 
                  current_state <= wait1;
               ELSIF (tobo_valid ='0') THEN 
                  current_state <= empty_data;
               ELSIF (cntr = 4) THEN 
                  current_state <= wait0;
               ELSE
                  current_state <= str;
               END IF;
            WHEN wait0 => 
               tx_data <= tobo_data_reg ;
               crc_data <= x"00" &'0'& fexfld & fexid & bcn & x"00" ;
               cntr <= (others => '0') ;
               tx_ctrl <= '0' ;
               current_state <= wait_3;
            WHEN wait1 => 
               cntr <= (others => '0') ;
               tx_data <= tobo_data_reg ;
               tx_ctrl <= '0' ;
               crc_data <= x"00" &'0'& fexfld & fexid & bcn & x"00" ;
               current_state <= wait_2;
            WHEN empty_data => 
               tx_data <= tobo_data_reg;
               crc_data<= (others => '0') ;
               tx_ctrl <= '0' ;
               cntr <= cntr +1;
               IF (cntr = 4) THEN 
                  current_state <= wait1;
               ELSE
                  current_state <= empty_data;
               END IF;
            WHEN fin => 
               crc_data <= tobo_data ; --                 
               --cntr <= (others => '0') ;
               tx_data <= crc & fexfld & fexid & bcn & x"bc";
               tx_ctrl <= '1' ;
               crc_start <='0';
               cntr <= cntr +1;
               current_state <= str;
            WHEN wait_2 => 
               cntr <= (others => '0') ;
               crc_data <= tobo_data ; --
               tx_data <= tobo_data_reg ;
               tx_ctrl <= '0' ;
               crc_start <= '1';
               IF (tobo_valid = '1') THEN 
                  current_state <= fin;
               ELSIF (tobo_valid = '0') THEN 
                  current_state <= fin1;
               ELSE
                  current_state <= wait_2;
               END IF;
            WHEN wait_3 => 
               cntr <= (others => '0') ;
               crc_data <= tobo_data; --
               tx_data <= tobo_data_reg ;
               tx_ctrl <= '0' ;
               crc_start <= '1';
               IF (tobo_valid ='1') THEN 
                  current_state <= fin;
               ELSIF (tobo_valid ='0') THEN 
                  current_state <= fin1;
               ELSE
                  current_state <= wait_3;
               END IF;
            WHEN trialer_1 => 
               cntr <= cntr +1 ;
               crc_start <= '0';
               tx_data <= crc & fexfld & fexid & bcn & x"bc";
               tx_ctrl <= '1' ;
               crc_data<= tobo_data;
               current_state <= str;
            WHEN fin1 => 
               crc_data <= tobo_data;                 
               cntr <= cntr+1 ;
               tx_data <= crc & fexfld & fexid & bcn & x"bc";
               tx_ctrl <= '1' ;
               crc_start <='0';
               IF (tobo_valid ='0') THEN 
                  crc_data<= (others => '0');
                  current_state <= empty;
               ELSE
                  current_state <= fin1;
               END IF;
            WHEN OTHERS =>
               current_state <= idle;
         END CASE;
      END IF;
   END PROCESS clocked_proc;
 
END ARCHITECTURE fsm;
    
--    TYPE STATE_TYPE IS (
--      idle,
--      empty,
--      trialer_0,
--      kchar,
--      empty_1,
--      str,
--      wait0,
--      wait1,
--      empty_data,
--      fin,
--      wait_2,
--      wait_3,
--      trialer_1
--   );
 
--   -- Declare current and next state signals
--   SIGNAL current_state : STATE_TYPE;

--BEGIN

--   -----------------------------------------------------------------
--   clocked_proc : PROCESS ( 
--      clk,
--      reset
--   )
--   -----------------------------------------------------------------
--   BEGIN
--      IF (reset = '1') THEN
--         current_state <= idle;
--      ELSIF (clk'EVENT AND clk = '1') THEN

--         -- Combined Actions
--         CASE current_state IS
--            WHEN idle => 
--               cntr <= (others => '0');
--               tx_data <= x"00000000";
--               crc_data<= (others => '0');
--               tx_ctrl <= '0';
--               crc_start<= '0';
--               IF ((tobo_sync = '1' AND tobo_valid = '0')) THEN 
--                  crc_start <= '1';
--                  current_state <= empty;
--               ELSIF (tobo_valid ='1') THEN 
--                  crc_start <= '1' ;
--                  current_state <= str;
--               ELSE
--                  current_state <= idle;
--               END IF;
--            WHEN empty => 
--               cntr<= cntr+1;
--               tx_data <= x"00000000";
--               tx_ctrl <= '0';
--               crc_data<= x"00000000";
--               crc_start <='0';
--               IF ((cntr = 4)) THEN 
--                  current_state <= kchar;
--               ELSE
--                  current_state <= empty;
--               END IF;
--            WHEN trialer_0 => 
--               cntr <= cntr +1 ;
--               crc_start <= '0';
--               tx_data <= crc & fexfld & fexid & bcn & x"bc";
--               tx_ctrl <= '1' ;
--               crc_data <= (others => '0');
--               current_state <= empty;
--            WHEN kchar => 
--               cntr <= (others => '0') ;
--               crc_data <= x"00" &'0'& fexfld & fexid & bcn & x"00" ;
--               tx_data <= x"00000000" ;
--               tx_ctrl <= '0' ;
--               current_state <= empty_1;
--            WHEN empty_1 => 
--               cntr <= (others => '0') ;
--               crc_data<= tobo_data; 
--               tx_data <= x"00000000" ;
--               tx_ctrl <= '0' ;
--               crc_start <= '1';
--               IF (tobo_valid = '0') THEN 
--                  current_state <= trialer_0;
--               ELSIF (tobo_valid = '1') THEN 
--                  current_state <= trialer_1;
--               ELSE
--                  current_state <= empty_1;
--               END IF;
--            WHEN str => 
--               crc_start <= '0';
--               crc_data<= tobo_data;
--               tx_data <= tobo_data_reg ;
--               tx_ctrl <= '0' ;
--               cntr <= cntr +1;
--               IF (tobo_valid ='0' and cntr = 4) THEN 
--                  current_state <= wait1;
--               ELSIF (tobo_valid ='0') THEN 
--                  current_state <= empty_data;
--               ELSIF (cntr = 4) THEN 
--                  current_state <= wait0;
--               ELSE
--                  current_state <= str;
--               END IF;
--            WHEN wait0 => 
--               tx_data <= tobo_data_reg ;
--               crc_data <= x"00" &'0'& fexfld & fexid & bcn & x"00" ;
--               cntr <= (others => '0') ;
--               tx_ctrl <= '0' ;
--               current_state <= wait_3;
--            WHEN wait1 => 
--               cntr <= (others => '0') ;
--               tx_data <= tobo_data_reg ;
--               tx_ctrl <= '0' ;
--               crc_data <= x"00" &'0'& fexfld & fexid & bcn & x"00" ;
--               current_state <= wait_2;
--            WHEN empty_data => 
--               tx_data <= tobo_data_reg;
--               crc_data<= (others => '0') ;
--               tx_ctrl <= '0' ;
--               cntr <= cntr +1;
--               IF (cntr = 4) THEN 
--                  current_state <= wait1;
--               ELSE
--                  current_state <= empty_data;
--               END IF;
--            WHEN fin => 
--               crc_data <= tobo_data ;                 
--               --cntr <= (others => '0') ;
--               tx_data <= crc & fexfld & fexid & bcn & x"bc";
--               tx_ctrl <= '1' ;
--               crc_start <='0';
--               cntr <= cntr +1;
--               IF (tobo_valid ='1') THEN 
--                  current_state <= str;
--               ELSIF (tobo_valid ='0') THEN 
--                  crc_data<= (others => '0');
--                  current_state <= empty;
--               ELSE
--                  current_state <= fin;
--               END IF;
--            WHEN wait_2 => 
--               cntr <= (others => '0') ;
--               crc_data <= tobo_data ;
--               tx_data <= tobo_data_reg ;
--               tx_ctrl <= '0' ;
--               crc_start <= '1';
--               current_state <= fin;
--            WHEN wait_3 => 
--               cntr <= (others => '0') ;
--               crc_data <= tobo_data ;
--               tx_data <= tobo_data_reg ;
--               tx_ctrl <= '0' ;
--               crc_start <= '1';
--               current_state <= fin;
--            WHEN trialer_1 => 
--               cntr <= cntr +1 ;
--               crc_start <= '0';
--               tx_data <= crc & fexfld & fexid & bcn & x"bc";
--               tx_ctrl <= '1' ;
--               crc_data<= tobo_data;
--               current_state <= str;
--            WHEN OTHERS =>
--               current_state <= idle;
--         END CASE;
--      END IF;
--   END PROCESS clocked_proc;
 
--END ARCHITECTURE fsm;   
      
      
    