----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/15/2016 11:40:20 AM
-- Design Name: 
-- Module Name: synch_type_pkg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package  synch_type is

 
type data_path is
    record     
      data_in      : std_logic_vector(31 downto 0);
      data_out     : std_logic_vector(223 downto 0);
      data_out_reg224     : std_logic_vector(223 downto 0);
    end record;   

type data_path_array is
    array (natural range <>) of data_path;

	
-- Another data array but with is-K-char control for transceivers
    type mgt_data is
    record
        data: std_logic_vector(31 downto 0);
        ctrl: std_logic_vector( 3 downto 0);
    end record;

    type mgt_data_array is array(natural range <>) of mgt_data;              
                   
end synch_type;
