library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;


-------------------------------------------------------------------------------

entity tx_crc_gen is
 generic(
        REVERSE_BIT_ORDER : boolean := True);
   port (
      clk        : in std_logic;
	  data_in    : in std_logic_vector (31 downto 0) := (others=> '0') ;
      start_txcrc: in std_logic;
      --eof        : in std_logic;
      tx_data    : out std_logic_vector (31 downto 0) := (others=> '0')	
      );  

end tx_crc_gen;

-------------------------------------------------------------------------------

architecture behavioral of tx_crc_gen is

  -- signal declaration
  signal crc_out   : std_logic_vector (8 downto 0) := (others=> '0');
  signal crc23_out : std_logic_vector (8 downto 0) := (others=> '0');  
  signal eof       : std_logic := '0'; 
  
    

begin  -- architecture 


  -- component instantiation
  TX: entity work.osum_crc9d32
    generic map(
      REVERSE_BIT_ORDER =>  REVERSE_BIT_ORDER)
    port map (
      d_in       => data_in,
      crc_start  => start_txcrc,
      clock      => clk,
      crc_out    => crc_out
      );
    
    
crc9d23_i : entity work.osum_crc9d23
    generic map(
        REVERSE_BIT_ORDER => REVERSE_BIT_ORDER)
    port map(
        clock   => clk,
        d_in    => data_in(22 downto 0),
        crc_in  => crc_out,
        crc_out => crc23_out
        );

 -- this combined the crc23 & crc32
  append_crc: entity work.crc_add
    generic map(
      REVERSE_BIT_ORDER => REVERSE_BIT_ORDER)
    port map(
    clock    =>  clk   ,
    crc_en   =>  eof  ,
    crc_in   =>  crc23_out,
    data_in  =>  data_in ,
    data_out =>  tx_data
    );

-- generating end of frame signal
 eof_process: process(clk)
 variable cntr : unsigned(2 downto 0):= "000";
          begin 
           
          if clk' event and clk ='1' then 
              eof  <= '0';
            if 	start_txcrc = '1' then
                cntr := (others => '0') ;
	           else cntr := cntr+1;
             end if ;
            if cntr = 5 then 
               eof  <= '1';	
             end if;
           end if;
         end process;	  
  

end behavioral;

