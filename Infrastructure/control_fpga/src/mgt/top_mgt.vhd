----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.10.2018 10:27:24
-- Design Name: 
-- Module Name: top_mgt - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
LIBRARY ipbus_lib;
USE ipbus_lib.ipbus.all;
LIBRARY infrastructure_lib;
USE infrastructure_lib.all;
use infrastructure_lib.mgt_type.all;
use infrastructure_lib.ipbus_decode_L1CaloEfexControl.all;
--!@copydoc top_mgt.vhd

entity top_mgt is
  Port (        
   clk40                    : in std_logic;                     -- clk40 generatred from ttc clock
   rx_clk160                : out std_logic_vector(9 downto 0); --- Generated rx clocks of 160MHz from the mgt pll.- Generated rx clocks of 160MHz from the mgt pll.
   Q_CLK_GTREFCLK_PAD_N_IN  : in std_logic_vector(2 downto 0); 
   Q_CLK_GTREFCLK_PAD_P_IN  : in std_logic_vector(2 downto 0); 
   RXN_IN                   : in std_logic_vector(11 downto 0); 
   RXP_IN                   : in std_logic_vector(11 downto 0); 
   TXN_OUT                  : out std_logic_vector(11 downto 0);
   TXP_OUT                  : out std_logic_vector(11 downto 0);
   ipb_clk                  : in std_logic;
   ipb_rst                  : in std_logic;
   ipb_in                   : in ipb_wbus;
   ipb_out                  : out ipb_rbus;
   topo_data_fpga1          : out std_logic_vector(31 downto 0);   -- topo tob data from fpga1                                                           
   topo_data_fpga2          : out std_logic_vector(31 downto 0);   -- topo tob data from fpga2
   topo_data_fpga3          : out std_logic_vector(31 downto 0);   -- topo tob data from fpga3
   topo_data_fpga4          : out std_logic_vector(31 downto 0);   -- topo tob data from fpga4
   raw_data_fpga1           : out std_logic_vector(31 downto 0);   -- raw  data from fpga1                                                            
   raw_data_fpga2           : out std_logic_vector(31 downto 0);   -- raw  data from fpga2 
   raw_data_fpga3           : out std_logic_vector(31 downto 0);   -- raw  data from fpga3 
   raw_data_fpga4           : out std_logic_vector(31 downto 0);    -- raw  data from fpga4   
   bc_reg_sel               : out std_logic_vector (7 downto 0);   -- setting the bc alignment mux
   delay_cntr_0             : in  std_logic_vector (3 downto 0);   -- first stage delay counter value of the ttc mgt
   delay_cntr_1             : in  std_logic_vector (3 downto 0);  -- first stage delay counter of the hub mgt 
   mux_sel                  : out std_logic_vector (7 downto 0);  -- first stage mux selector
   mgt_enable               : out std_logic_vector(1 downto 0) ;   -- enable for the incoming rx data of the mgt.
   ttc_rx_data              : out std_logic_vector(31 downto 0);  -- ttc information
   rod_busy                 : out std_logic_vector(31 downto 0)    -- rod busy 
       
  );
end top_mgt;

architecture Behavioral of top_mgt is

signal gt_rxpd                : mgt_rxpd_array(2 downto 0);
signal gt_txpd                : mgt_txpd_array(2 downto 0);
signal mgt_DATA_VALID_IN      : std_logic_vector(11 downto 0);
signal MGT_RXN_in,MGT_RXP_in  : mgt_rx_array(2 downto 0);
signal MGT_TXN_in,MGT_TXP_in  : mgt_tx_array(2 downto 0);
signal mgt_TXUSRCLK_OUT,mgt_RXUSRCLK_OUT,mgt_tx_fsm_resetdone, mgt_rx_fsm_resetdone,MGT_Commadet_int : std_logic_vector(11 downto 0);
signal mgt_SOFT_RESET_TX_IN, mgt_SOFT_RESET_RX_IN,mgt_QPLLLOCK_OUT,mgt_QPLLREFCLKLOST_OUT : std_logic_vector(2 downto 0);
signal rxdata_quad_array      : mgt_rxdata_array (2 downto 0);
signal mgt_txdata             : mgt_txdata_array(2 downto 0);
signal mgt_loopback_reg       : std_logic_vector (14 downto 0);          
signal mgt_txbufstatus        : mgt_txbufstatus_array(2 downto 0);      
signal mgt_rxcommadet         : mgt_rxcommadet_array (2 downto 0);      
signal mgt_rxbyterealign      : mgt_rxbyterealign_array(2 downto 0);    
signal mgt_rx_resetdone       : mgt_rxresetdone_array  (2 downto 0);    
signal mgt_rxbyteisaligned    : mgt_rxbyteisaligned_array (2 downto 0); 
signal mgt_tx_resetdone       : mgt_txresetdone_array   (2 downto 0);   
signal mgt_txcharisk          : mgt_txcharisk_array  (2 downto 0);      
signal mgt_loopback_in        : mgt_loopback_array (2 downto 0);                                                                               
signal mgt_rxchariscomma      : mgt_rxchariskcomm_array(2 downto 0);   
signal mgt_rxcharisk          : mgt_rxcharisk_array(2 downto 0);    
signal mgt_rxdisperr          : mgt_rxdisperr_array(11 downto 0);        
signal mgt_rxnotintable       : mgt_rxnotintable_array(11 downto 0); 
signal rx_realign,rx_resetdone,tx_resetdone,rxbyteisaligned:std_logic_vector(11 downto 0);
signal encode_error,rx_disperr,mgt_rxcharisk_reg,txcharisk : std_logic_vector(47 downto 0);

signal ipbw: ipb_wbus_array(N_SLAVES-1 downto 0);
signal ipbr, ipbr_d: ipb_rbus_array(N_SLAVES-1 downto 0);

begin


  --- powerering the two quad  tx and rx  
         
         tx_pwr_on_gen:for i in 0 to 2 --loop   
        
        generate 
         gt_txpd(i).gt0_txpd <= "00";
         gt_txpd(i).gt1_txpd <= "00";
         gt_txpd(i).gt2_txpd <= "00";
         gt_txpd(i).gt3_txpd <= "00";  
        end generate;
        
  -----------------------------------
  
  -- assign rx data 
  
  topo_data_fpga1 <= rxdata_quad_array(0).gt0_rxdata_out;
  raw_data_fpga1  <= rxdata_quad_array(0).gt1_rxdata_out;
  topo_data_fpga2 <= rxdata_quad_array(0).gt2_rxdata_out;
  raw_data_fpga2  <= rxdata_quad_array(0).gt3_rxdata_out;
  
   topo_data_fpga3 <= rxdata_quad_array(1).gt0_rxdata_out;
   raw_data_fpga3  <= rxdata_quad_array(1).gt1_rxdata_out;
   topo_data_fpga4 <= rxdata_quad_array(1).gt2_rxdata_out;
   raw_data_fpga4  <= rxdata_quad_array(1).gt3_rxdata_out;
   
   ttc_rx_data     <= rxdata_quad_array(2).gt0_rxdata_out;
   rod_busy        <= rxdata_quad_array(2).gt1_rxdata_out;
   
  --------------------------------------------------------
   
  
   
  --- mgt for receiving topo and raw data from the processing fpga1,fpga2,fpga3 and fpga4
  
  MGT_TX_RX : entity infrastructure_lib.MGT_quad_gen
  
     port map (
     
         TTC_CLK                   =>  clk40,                               
         MGT_CLK_GTREFCLK_PAD_N_IN =>  Q_CLK_GTREFCLK_PAD_N_IN (2 downto 0), 
         MGT_CLK_GTREFCLK_PAD_P_IN =>  Q_CLK_GTREFCLK_PAD_P_IN (2 downto 0),                        
         mgt_TXUSRCLK_OUT          =>  mgt_TXUSRCLK_OUT(11 downto 0),        
         mgt_RXUSRCLK_OUT          =>  mgt_RXUSRCLK_OUT(11 downto 0),       
         mgt_SOFT_RESET_TX_IN      =>  mgt_SOFT_RESET_TX_IN(2 downto 0),    
         mgt_SOFT_RESET_RX_IN      =>  mgt_SOFT_RESET_RX_IN(2 downto 0),    
          -- data                    
         RXN_IN                    =>  MGT_RXN_IN(2 downto 0),
         RXP_IN                    =>  MGT_RXP_IN(2 downto 0),
         TXN_IN                    =>  MGT_TXN_IN(2 downto 0),
         TXP_IN                    =>  MGT_TXP_IN(2 downto 0),                       
         rxdata_quad_array         =>  rxdata_quad_array (2 downto 0),
         txdata_quad_array         =>  mgt_txdata(2 downto 0),     
    -- status and monitoring  =>
         gt_rxpd_array             =>  gt_rxpd (2 downto 0), 
         gt_txpd_array             =>  gt_txpd (2 downto 0),       
         mgt_TX_FSM_RESET_DONE     =>  mgt_tx_fsm_resetdone(11 downto 0), 
         mgt_RX_FSM_RESET_DONE     =>  mgt_rx_fsm_resetdone(11 downto 0),
         rxbyteisaligned_quad_array=>  mgt_rxbyteisaligned (2 downto 0), 
         rxresetdone_quad_array    =>  mgt_rx_resetdone (2 downto 0),    
         txresetdone_quad_array    =>  mgt_tx_resetdone (2 downto 0),                
         loopback_quad_array       =>  mgt_loopback_in (2 downto 0),      
         rxchariscomma_quad_array  =>  mgt_rxchariscomma (2 downto 0),   
         rxcharisk_quad_array      =>  mgt_rxcharisk (2 downto 0),       
         txcharisk_quad_array      =>  mgt_txcharisk (2 downto 0),       
         txbufstatus_quad_array    =>  mgt_txbufstatus (2 downto 0),     
         rxbyterealign_quad_array  =>  mgt_rxbyterealign (2 downto 0),   
         rxcommadet_quad_array     =>  mgt_rxcommadet (2 downto 0),      
         rxdisperr_quad_array      =>  mgt_rxdisperr (2 downto 0),                                   
         rxnotintable_quad_array   =>  mgt_rxnotintable (2 downto 0)    
        -- mgt_QPLLREFCLKLOST_OUT    =>  mgt_QPLLREFCLKLOST_OUT (1 downto 0),                                  
        -- mgt_QPLLLOCK_OUT          =>  mgt_QPLLLOCK_OUT(1 downto 0) 
    
    
       );
       
        mgt_gen :    for i in 0 to 2 --loop
        
        generate
      
            MGT_Commadet_int(i+3*i downto i+3*i)     <= mgt_rxchariscomma(i).gt0_rxchariscomma_out(0 downto 0); -- asginment of the rxchriscomma
            MGT_Commadet_int(i+1+3*i downto i+1+3*i) <= mgt_rxchariscomma(i).gt1_rxchariscomma_out(0 downto 0); -- asginment of the rxchriscomma
            MGT_Commadet_int(i+2+3*i downto i+2+3*i) <= mgt_rxchariscomma(i).gt2_rxchariscomma_out(0 downto 0); -- asginment of the rxchriscomma
            MGT_Commadet_int(i+3+3*i downto i+3+3*i) <= mgt_rxchariscomma(i).gt3_rxchariscomma_out(0 downto 0); -- asginment of the rxchriscomma
            
            rx_realign (i+3*i)  <= mgt_rxbyterealign(i).gt0_rxbyterealign;  -- asignment of rxbyterealign
            rx_realign(i+1+3*i) <= mgt_rxbyterealign(i).gt1_rxbyterealign;  -- asignment of rxbyterealign
            rx_realign(i+2+3*i) <= mgt_rxbyterealign(i).gt2_rxbyterealign;  -- asignment of rxbyterealign
            rx_realign(i+3+3*i) <= mgt_rxbyterealign(i).gt3_rxbyterealign;  -- asignment of rxbyterealign
            
            rx_resetdone (i+3*i)  <= mgt_rx_resetdone(i).gt0_rxresetdone;    -- asignment of rx_resetdone                     
            rx_resetdone(i+1+3*i) <= mgt_rx_resetdone(i).gt1_rxresetdone;    -- asignment of rx_resetdone
            rx_resetdone(i+2+3*i)  <= mgt_rx_resetdone(i).gt2_rxresetdone;   -- asignment of rx_resetdone
            rx_resetdone(i+3+3*i)  <= mgt_rx_resetdone(i).gt3_rxresetdone;   -- asignment of rx_resetdone
                              
            rxbyteisaligned (i+3*i)  <= mgt_rxbyteisaligned(i).gt0_rxbyteisaligned;
            rxbyteisaligned(i+1+3*i) <= mgt_rxbyteisaligned(i).gt1_rxbyteisaligned;
            rxbyteisaligned(i+2+3*i) <= mgt_rxbyteisaligned(i).gt2_rxbyteisaligned;
            rxbyteisaligned(i+3+3*i) <= mgt_rxbyteisaligned(i).gt3_rxbyteisaligned;
                            
            mgt_rxcharisk_reg(i+3+15*i downto i+15*i)     <= mgt_rxcharisk(1).gt0_rxcharisk_out;
            mgt_rxcharisk_reg(i+7+15*i downto i+4+15*i)   <= mgt_rxcharisk(1).gt1_rxcharisk_out;
            mgt_rxcharisk_reg(i+11+15*i downto i+8+15*i)  <= mgt_rxcharisk(1).gt2_rxcharisk_out;
            mgt_rxcharisk_reg(i+15+15*i downto i+12+15*i) <= mgt_rxcharisk(1).gt3_rxcharisk_out;
          
      
            rx_disperr(i+3+15*i downto i+15*i)     <= mgt_rxdisperr(i).gt0_rxdisperr;
            rx_disperr(i+7+15*i downto i+4+15*i)   <= mgt_rxdisperr(i).gt1_rxdisperr;
            rx_disperr(i+11+15*i downto i+8+15*i)  <= mgt_rxdisperr(i).gt2_rxdisperr;
            rx_disperr(i+15+15*i downto i+12+15*i) <= mgt_rxdisperr(i).gt3_rxdisperr;
      
            encode_error(i+3+15*i downto i+15*i)     <= mgt_rxnotintable(i).gt0_rxnotintable; -- asignment of 10b/8b encoding error- 
            encode_error(i+7+15*i downto i+4+15*i)   <= mgt_rxnotintable(i).gt1_rxnotintable; -- asignment of 10b/8b encoding error
            encode_error(i+11+15*i downto i+8+15*i)  <= mgt_rxnotintable(i).gt2_rxnotintable; -- asignment of 10b/8b encoding error
            encode_error(i+15+15*i downto i+12+15*i) <= mgt_rxnotintable(i).gt3_rxnotintable; -- asignment of 10b/8b encoding error
      
            MGT_RXN_IN(i).RXN_IN <= RXN_IN(i+3+3*i downto i+3*i);  -- rx input asignment
            MGT_RXP_IN(i).RXP_IN <= RXP_IN(i+3+3*i downto i+3*i);  -- rx input asignment
      
                  
        end generate mgt_gen;
        

       
   -----------------------------------------------------------------------------------------------------------------
          -- This will use generate statament in order to assign tx side signals and control loopback of the MGT_TX quads               
        ------------------------------------------------------------------------------------------------------------------  
        mgt_tx_gen: for i in 0 to 2 
        generate 
        
              TXN_OUT(i+3+3*i downto i+3*i) <= MGT_TXN_IN(i).TXN_OUT;
              TXP_OUT(i+3+3*i downto i+3*i) <= MGT_TXP_IN(i).TXP_OUT;
              tx_resetdone (i+3*i)   <= mgt_tx_resetdone(i).gt0_txresetdone; 
              tx_resetdone(i+1+3*i)  <= mgt_tx_resetdone(i).gt1_txresetdone; 
              tx_resetdone(i+2+3*i)  <= mgt_tx_resetdone(i).gt2_txresetdone; 
              tx_resetdone(i+3+3*i)  <= mgt_tx_resetdone(i).gt3_txresetdone; 
              
              txcharisk(i+3+15*i downto i+15*i)   <= mgt_txcharisk(i).gt0_txcharisk;  
              txcharisk(i+7+15*i downto i+4+15*i)  <= mgt_txcharisk(i).gt1_txcharisk; 
              txcharisk(i+11+15*i downto i+8+15*i) <= mgt_txcharisk(i).gt2_txcharisk; 
              txcharisk(i+15+15*i downto i+12+15*i) <= mgt_txcharisk(i).gt3_txcharisk;
              
              --mgt_loopback_in(i).gt0_loopback_in <= mgt_loopback_reg(i+2+2*i downto i+2*i); 
              
              end generate mgt_tx_gen;    
  
------------------------------------------------------------------------------------------------     
       --- MGT  tx_rx ipbus connections
 ------------------------------------------------------------------------------------------------
MGT_ipb : entity infrastructure_lib.mgt_cntrl_slaves
                  port map (
                    clk280              => mgt_RXUSRCLK_OUT (7 downto 0),
                    ipb_clk             => ipb_clk,
                    ipb_rst             => ipb_rst ,
                    ipb_in              => ipbw(N_SLV_CNTRL_MGT),
                    ipb_out             => ipbr(N_SLV_CNTRL_MGT),  
                    softreset_tx        => mgt_SOFT_RESET_TX_IN,
                    softreset_rx        => mgt_SOFT_RESET_RX_IN,
                    qpll_lock           => mgt_QPLLLOCK_OUT,
                    qpll_refclklost     => mgt_QPLLREFCLKLOST_OUT,
                    rx_resetdone        => rx_resetdone,
                    rx_fsm_resetdone    => mgt_rx_fsm_resetdone,
                    rx_byteisaligned    => rxbyteisaligned,
                    tx_resetdone        => tx_resetdone,
                    tx_fsm_resetdone    => mgt_tx_fsm_resetdone,
                    tx_bufstatus        => (others=> '0'),
                    rx_realign          => rx_realign,
                    rx_disperr          => rx_disperr,   
                    encode_error        => encode_error , 
                    bc_reg_sel          => bc_reg_sel   ,
                    delay_cntr_0        => delay_cntr_0,
                    delay_cntr_1        => delay_cntr_1 ,
                    mux_sel             => mux_sel,
                    mgt_enable          => mgt_enable   
                   
                    
                    
                                           
                        
                    );    
       
       
       
end Behavioral;
