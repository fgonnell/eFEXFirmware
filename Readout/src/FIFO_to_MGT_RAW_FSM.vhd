--! @file
--! @brief FSM to read RAW Event data from Link Output FIFO to MGT for process FPGA
--! @details 
--! \verbatim
--! This FSM reads RAW frames from Link Output FIFO and writes into MGT to transmit to cFPGA.
--! This FSM handles one full frame at a time without pausing.
--! It monitors the RAW_frame_counter to find out if there are Frames waiting to be transmitted to cFPGA.
--! The Frames are only transmitted when cFPGA indicates it is ready to receive data by setting ctrl_RAW_ready signal to 1.
--! If ctrl_RAW_ready signal is set to 0 by cFPGA, this indicates cFPGA is not ready to receive data,
--! in this case events accumulate in Link Output FIFO until occupancy reaches a pre-define prog_FULL level,
--! at this time, Link Output FIFO stops receiving data.
--! The data transmission of MGT is at 11.2 Gbps.
--! \endverbatim
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;
use TOB_rdout_lib.data_type_pkg.all;

--! @copydoc FIFO_to_MGT_RAW_FSM.vhd
entity FIFO_to_MGT_RAW_FSM is
    Port ( 
--        RST                      : in STD_LOGIC;
        --! TOB Readout FIFO reset Pulse by software command ORed with SYS_RST
        TOB_FIFO_sw_rst          : in std_logic ;       
        clk_in_280M              : in STD_LOGIC;
       --! Ready signal from control FPGA to receive RAW data from process FPGA
        ctrl_RAW_ready_in        : in  std_logic ;
        --! Frame counter register
        frame_counter            : in STD_LOGIC_VECTOR (11 downto 0);
        --! RAW from Link Output FIFO
        FIFO_MGT_TOBs            : in STD_LOGIC_VECTOR (32 downto 0);
        --! RAW from Link Output FIFO valid signal
        FIFO_MGT_TOB_valid       : in STD_LOGIC;
        --! Link Output FIFO FULL Flag
        LO_FIFO_full             : in std_logic;
        --! Link Output FIFO Empty Flag
        LO_FIFO_empty            : in std_logic;
        --! Link Output FIFO partial FULL Flag
        LO_FIFO_prog_full        : in std_logic;
        --! Read enalbe singal to Link Output FIFO
        FIFO_MGT_rd_en           : out STD_LOGIC;
        --! Decrement Frame Counter
        frame_counter_dec_en     : out STD_LOGIC;
        --! 32b RAW to connect to output MGT to control FPGA
        RAW_data_out               : out STD_LOGIC_VECTOR (31 downto 0);
        --! RAW is CHAR to connect to output MGT to control FPGA
        RAW_data_is_char            : out STD_LOGIC;
         --! RAW is valid signal to wr to SPY memory
        RAW_data_out_valid          : out STD_LOGIC
       );
end FIFO_to_MGT_RAW_FSM;

--! @copydoc FIFO_to_MGT_RAW_FSM.vhd
architecture Behavioral of FIFO_to_MGT_RAW_FSM is

-- CHAR constants are defined in data_type_pkg.vhd
-- for reference only
--    constant ch_idle    : std_logic_vector(7 downto 0)  := X"BC"  ;    -- idle char is K28.5
--    constant ch_sop1    : std_logic_vector(7 downto 0)  := X"3C"  ;    -- star of packet char 1 is K28.1
--    constant ch_sop2    : std_logic_vector(7 downto 0)  := X"5C"  ;    -- star of packet char 2 is K28.2
--    constant ch_eop     : std_logic_vector(7 downto 0)  := X"DC"  ;    -- end  of packet char is K28.6

--************************** Register Declarations ****************************    
    signal  clk_in_280M_i       : STD_LOGIC;                  
                  
    signal  T_TOB_out_valid_i   : STD_LOGIC;
    signal  TOB_in_is_char_i    : STD_LOGIC;
    signal  TOBs_in_i           : STD_LOGIC_VECTOR (31 downto 0);
    signal  MGT_fifo_rd_en_i    : STD_LOGIC;
    signal  TOBs_out_i, TOBs_in_tmp_1dly                : STD_LOGIC_VECTOR (31 downto 0);
    signal  TOB_out_is_char_i, TOB_in_is_char_tmp_1dly  : STD_LOGIC;

    signal  frame_counter_dec_en_i       : STD_LOGIC;


    TYPE STATE_TYPE IS (
       idle, rd_fifo1, rd_fifo2, tx_data_SOF1, tx_data_SOF2, tx_data1, tx_data2, tx_data_EOF1, tx_data_EOF2, 
       wait1, wait2, wait3, wait4
       );
  
    SIGNAL current_state : STATE_TYPE;

begin
    -- input ports
    clk_in_280M_i        <= clk_in_280M ;

    -- output port
--    RAW_data_out         <= TOBs_out_i;                  -- TOB data out to MGT
--    RAW_data_is_char     <= TOB_out_is_char_i ;          -- TOB data out to MGT is CHAR
--	RAW_data_out_valid   <= T_TOB_out_valid_i ;			 -- TOB data out is valid
    FIFO_MGT_rd_en       <= MGT_fifo_rd_en_i ;
--    frame_counter_dec_en <= frame_counter_dec_en_i ;


U1_clk_proc : process (clk_in_280M_i)   -- to remove timing errors
    begin
        if clk_in_280M_i'event and clk_in_280M_i = '1' then
            -- output ports
            RAW_data_out         <= TOBs_out_i;                  -- TOB data out to MGT
            RAW_data_is_char     <= TOB_out_is_char_i ;          -- TOB data out to MGT is CHAR
            RAW_data_out_valid   <= T_TOB_out_valid_i ;			 -- TOB data out is valid
--            FIFO_MGT_rd_en       <= MGT_fifo_rd_en_i ;
            frame_counter_dec_en <= frame_counter_dec_en_i ;
        end if;
    end process;

U2_clk_proc : process (clk_in_280M_i)   -- to remove timing errors
    begin
        if clk_in_280M_i'event and clk_in_280M_i = '1' then
            if MGT_fifo_rd_en_i = '1' then
                TOB_in_is_char_tmp_1dly  <= FIFO_MGT_TOBs(32);           -- TOB data from FIFO is CHAR
                TOBs_in_tmp_1dly         <= FIFO_MGT_TOBs(31 downto 0);  -- TOB data from FIFO
            end if;
        end if;
    end process;

U4_rd_fsm : process (clk_in_280M_i)
    begin
        if clk_in_280M_i'event and clk_in_280M_i = '1' then
            TOB_in_is_char_i     <= TOB_in_is_char_tmp_1dly;           -- TOB data from FIFO is CHAR
            TOBs_in_i            <= TOBs_in_tmp_1dly(31 downto 0);  -- TOB data from FIFO
            T_TOB_out_valid_i    <= '0';
            if ( TOB_FIFO_sw_rst = '1' )then            -- signal is RST OR TOB_FIFO_sw_rst
                current_state           <= idle ;
            else
    
                CASE current_state is
                    when idle =>
                        T_TOB_out_valid_i       <= '0' ;
                        frame_counter_dec_en_i  <= '0' ;
                        MGT_fifo_rd_en_i        <= '0' ;
                        TOB_out_is_char_i       <= '1';
                        TOBs_out_i              <= X"000000" & ch_idle ; -- K28.5 idle = BC
                        -- if no data available wait 
                        if ctrl_RAW_ready_in = '1' then     -- if CNTL FPGA ready to receive data
                            if (frame_counter > X"000" )  then
                                -- if FRAME fifo is not empty, read DATA fifos
                                current_state     <= rd_fifo1 ;
                           end if;
                        end if;

                    when rd_fifo1 =>
                        T_TOB_out_valid_i   <= '0' ;
                        TOB_out_is_char_i   <= '1';                                                         
                        TOBs_out_i          <= X"000000" & ch_idle ; -- K28.5 idle
                        MGT_fifo_rd_en_i    <= '1';           -- read data from fifos
                        if (TOBs_in_tmp_1dly(7 downto 0) = X"7C" AND TOB_in_is_char_tmp_1dly = '1') then      -- check fist data in order to save the SOF data and char
--                            TOB_out_is_char_i <= TOB_in_is_char_i;
--                            TOBs_out_i        <= TOBs_in_i       ;
                            current_state       <= tx_data_SOF1 ;
                        else
                            -- if fifo is not empty, read fifos
                            current_state <= rd_fifo1 ;
                        end if;
                       
                    when tx_data_SOF1 =>
                        MGT_fifo_rd_en_i  <= '1';           -- read data from fifos
                        TOB_out_is_char_i <= TOB_in_is_char_i;
                        TOBs_out_i        <= TOBs_in_i       ;
                        T_TOB_out_valid_i   <= '1' ;
                        current_state     <= tx_data1 ;
                           
                    when wait1 =>       -- this is a FIFO rd to ensure correct SOF
                        T_TOB_out_valid_i   <= '0' ;
                        MGT_fifo_rd_en_i  <= '1';           -- read data from fifos 
                        TOB_out_is_char_i <= '1';
                        TOBs_out_i        <= X"000000"& ch_idle ; -- K28.5 idle
                        current_state     <= wait2 ;   -- was 

                    when wait2 =>       -- this is a FIFO rd to ensure correct SOF
                        MGT_fifo_rd_en_i  <= '1';           -- read data from fifos
                        TOB_out_is_char_i   <= TOB_in_is_char_i;    
                        TOBs_out_i          <= TOBs_in_i       ;
                        T_TOB_out_valid_i     <= '1' ;
                        current_state     <= tx_data1 ;   -- was 

                    when tx_data1 =>
                        TOB_out_is_char_i   <= TOB_in_is_char_i;    
                        TOBs_out_i          <= TOBs_in_i       ;
                        T_TOB_out_valid_i     <= '1' ;
                        if (TOBs_in_tmp_1dly(7 downto 0) = X"DC" AND TOB_in_is_char_tmp_1dly = '1') then   -- check last data in order to save the SOF data and char
                            frame_counter_dec_en_i  <= '1' ;
                            MGT_fifo_rd_en_i    <= '0';           -- stop read data from fifos
                            current_state       <= tx_data_EOF1 ;
                        else
                            MGT_fifo_rd_en_i  <= '1';           -- read data from fifos
                            -- if fifo is not empty, read fifos
                            current_state <= tx_data1 ;
                        end if;
                        
                    when tx_data_EOF1 =>
                        frame_counter_dec_en_i  <= '0' ;
                        TOB_out_is_char_i   <= TOB_in_is_char_i;    
                        TOBs_out_i          <= TOBs_in_i       ;
                        T_TOB_out_valid_i     <= '1' ;
--                        TOB_out_is_char_i   <= '1';
--                        TOBs_out_i          <= X"000000" & ch_idle ; -- K28.5 idle
                        current_state       <= wait3 ;   -- was wait1
                       
--                    when wait1 =>
--                        T_TOB_out_valid_i     <= '0' ;       
--                        TOB_out_is_char_i   <= '1';
--                        TOBs_out_i          <= X"000000"& ch_idle ; -- K28.5 idle
--                        current_state       <= wait2 ;

                    when wait3 =>
                        T_TOB_out_valid_i     <= '0' ;       
                        TOB_out_is_char_i   <= '1';
                        TOBs_out_i          <= X"000000"& ch_idle ; -- K28.5 idle
                        current_state       <= tx_data_EOF2 ;
						
                    when tx_data_EOF2 =>
                        T_TOB_out_valid_i     <= '0' ;
                        TOB_out_is_char_i   <= '1';
                        TOBs_out_i          <= X"000000" & ch_idle ; -- K28.5 idle
                        current_state       <= tx_data_EOF2 ;
                        if ctrl_RAW_ready_in = '1' then
                            if (frame_counter = X"000" ) then      -- if frame_counter = X"000"
                               if (TOBs_in_i(7 downto 0) = X"DC" AND TOB_in_is_char_i = '1') then   -- no event is in FIFO (end of last event)
                                  current_state       <= idle ;
                               end if;
                            else        -- if frame_counter /= X"000"
                               MGT_fifo_rd_en_i <= '1';
                               if (TOBs_in_i(7 downto 0) = X"7C" AND TOB_in_is_char_i = '1') then     -- more events are in FIFO (start of next event)
                                  current_state <= wait1 ;
                               end if;
                            end if;
                        end if;
                        
                    when others =>
                        NULL;
                end case;
            end if;
        end if;
    end process;

end Behavioral;
