 
----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/8/2018 03:00:29 PM
-- Design Name: 
-- Module Name: top_synch - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;
LIBRARY xil_defaultlib;
--use work.synch_type.all;

entity top_cntrl_synch is
   
Port (
      rx_clk160    : in  std_logic;    -- rx clock of the mgt                
      TTC_clk      : IN  std_logic;    -- ttc clk of 40MHz                   
      reset        : in  std_logic;
      enable_mgt   : in  std_logic;    --- if high enable the mgt rx register
      MGT_Commadet : in  std_logic;    -- comma detected for incoming data
      reg_sel      : in  std_logic_vector(3 downto 0); -- setting BC mux 
      mux_sel      : in  std_logic_vector(3 downto 0);  -- setting the first stage mux
      delay_num    : out std_logic_vector(3 downto 0); -- counted delay on the first stage of the synchronisation
      start        : in  std_logic;    -- start pulse for the calibration to start
      rx_resetdone : in   std_logic;
      reg128_latch : out  std_logic;
      data_out     : out std_logic_vector(127  downto 0);
      data_in      : in  std_logic_vector(31 downto 0)
  );  
 
end top_cntrl_synch;

architecture Behavioral of top_cntrl_synch is
SIGNAL latch_enable,temp0,temp1,Reg_enable,commdet_delay : std_logic;
signal delay_cnt, mux_cntrl,data_delay : std_logic_vector (3 downto 0);
signal data_out_int,data_out_0,data_out_1,data_out_2,data_out_3,data_out_4, dataout: std_logic_vector (127 downto 0);
signal temp2,ttc_redge, ttc_20M : std_logic;
signal bcn : std_logic_vector (4 downto 0);



begin



data_out        <= data_out_int; --dataout;
delay_num      <= data_delay;



                     
-- Capture the incoming TTC 40M clk by dtype flipflop 
dtype: entity work.d_type
          port map (
                clk   => ttc_clk,
                 q     => ttc_20M              
                
               ); 



--  Two register synchorization  for the ttc clock in to rx clock of 160M Hz
pipe_ttc_clk:  process (rx_clk160)

                begin
                     if rx_clk160' event and rx_clk160 ='1' then
                          temp0<= ttc_20M;
                          temp1<= temp0;
                          temp2<= temp1;
                          ttc_redge <= temp1 xor temp2  after 1ns;
                    end if;
                            
                end process; 
                
 -- First stage timing auto calibration  state machine that will generate delay count
state_machine: entity work.tac_sm
 
       PORT MAP (
          clk_280M     => rx_clk160,
          MGT_COMMADET => MGT_COMMADET,
          RESET        => reset,
          rx_resetdone => rx_resetdone, -- wait rx  reset done  to go high
          TTC_CLK_edge => ttc_redge,
          start        => start,  --  kick start pulse for the state machine to count the diffrence edges 
          Reg_enable   => Reg_enable,
          Mux_value    => Delay_cnt 
       );       
       
       
     
       
 -- Delay count register that holds the number of delay that was counted by the state machine      
 
reg_delay_cnt:  process (rx_clk160)
      
       begin
          if rx_clk160' event and rx_clk160 ='1' then
            if reg_enable ='1' then -- time difference between the ttc edge and 280Mhz is ready
             data_delay <= delay_cnt; -- read the edge difference value between two clocks
           end if;
         end if;  
       end process;   
       
       
synch_1: entity work.first_stage_synch
           
           PORT MAP (
             clk160     => rx_clk160,
             reset       => reset,
             enable_mgt  => enable_mgt,
             mux_cntrl   => mux_sel, -- first stage mux setting 
             latch_enable => latch_enable,
             MGT_Commadet => MGT_Commadet, -- mgt comma detect input from the rx
             commdet_delay => commdet_delay,
             data_in     => data_in, -- in coming rx_data 32 bits
             data_out    => data_out_int  --outgoing data of 128 bits that goes to the fibre mapping
             
           );      
       
       
 
 -- Generates latch enable for the output data of 128 bits (4 x 32 bits) 
latch: entity work.ctrl_synch_latch
            Port MAP(
              CLK160     => rx_clk160,
              MGT_COMMADET => commdet_delay,
             latch_enable   => latch_enable -- latches when 4  32 bit of data are ready
       
              );
 

 
reg128_latch <= latch_enable;          

end Behavioral;
