--! @file
--! @brief Input spy/playback RAM for Algorithm block
--! @details 
--! This RAM contains algorithm input data, can be used to spy or for playback.
--! Its depth is 16, so data for 16 BCs can be stored or played back.
--! The total width needed to save input data would be 60 TriggerTowers * 11 Layers *
--! 10 bits = 6600 bits.
--! But, In order to make data more human readable, for every of the 60 TTs (110 bits)
--! a RAM having a width of 128 is used so 18 bits must be padded with zeroes.
--! So the actual total width of the RAM is 7680 bits.
--!
--! The RAM is internally made of 60 128-bit wide RAMs, one for each Trigger Tower.
--! The trigger towers are numbered from 0 to 59 and organized as shown in the
--! following table:
--! |      | Eta0 | Eta1 | Eta2 | Eta3 | Eta4 | Eta5 |
--! |------|------|------|------|------|------|------|
--! | Phi9 |  09  |  19  |  29  |  39  |  49  |  59  | 
--! | Phi8 |  08  |  18  |  28  |  38  |  48  |  58  | 
--! | Phi7 |  07  |  17  |  27  |  37  |  47  |  57  | 
--! | Phi6 |  06  |  16  |  26  |  36  |  46  |  56  | 
--! | Phi5 |  05  |  15  |  25  |  35  |  45  |  55  | 
--! | Phi4 |  04  |  14  |  24  |  34  |  44  |  54  | 
--! | Phi3 |  03  |  13  |  23  |  33  |  43  |  53  | 
--! | Phi2 |  02  |  12  |  22  |  32  |  42  |  52  |
--! | Phi1 |  01  |  11  |  21  |  31  |  41  |  51  |
--! | Phi0 |  00  |  10  |  20  |  30  |  40  |  50  | 
--!
--! Each one of the 60 128-bit RAMs contains data-words of one trigger tower
--! for 15 BCs padded with zeroes as represented in the following table.
--! | BC  |bits 0-9|bits 10-19|bits 20-29|bits 30-31||bits 32-41|bits 42-51|bits 52-61|bits 62-63||bits 64-73|bits 74-83|bits 84-93|bits 94-95||bits 96-105|bits 106-115| bits 116-127 |
--! |-----|--------|----------|----------|----------||----------|----------|----------|----------||----------|----------|----------|----------||-----------|------------|--------------|
--! |BC 1 |     L0 |      L1.0|     L1.1 |     "00" ||     L1.2 |     L1.3 |     L2.0 |     "00" ||     L2.1 |     L2.2 |     L2.3 |     "00" ||       L3  |       HAD  |"000000000000"|
--! |BC 2 |     L0 |      L1.0|     L1.1 |     "00" ||     L1.2 |     L1.3 |     L2.0 |     "00" ||     L2.1 |     L2.2 |     L2.3 |     "00" ||       L3  |       HAD  |"000000000000"|
--! |BC...|     L0 |      L1.0|     L1.1 |     "00" ||     L1.2 |     L1.3 |     L2.0 |     "00" ||     L2.1 |     L2.2 |     L2.3 |     "00" ||       L3  |       HAD  |"000000000000"|
--! |BC 16|     L0 |      L1.0|     L1.1 |     "00" ||     L1.2 |     L1.3 |     L2.0 |     "00" ||     L2.1 |     L2.2 |     L2.3 |     "00" ||       L3  |       HAD  |"000000000000"|
--!
--! Were BC <em>n</em> indicates the bunch crossing number and L0, L1.<em>n</em> , L2., L3,
--! and HAD indicate the energy samples for layer 0 (1 data-word), layer 1 (4
--! data-words), layer 2 (4 data-words), layer 3 (1 word), and hadronic (1 word).
--! All data words are 10-bit wide.
--!
--! From the ipbus side, each of these 128*16 RAM is remapped into a 32*64 RAM.
--! The ipbus RAM contains data as follows:
--! | BC  | bits 0-9 |bits 10-19| bits    20-29   |bits 30-31|
--! |-----|----------|----------|-----------------|----------|
--! |BC 1 |      L0  |     L1.0 |         L1.1    |     "00" |
--! |BC 1 |     L1.2 |     L1.3 |         L2.0    |     "01" |
--! |BC 1 |     L2.1 |     L2.2 |         L2.3    |     "10" |
--! |BC 1 |      L3  |      HAD |     "000" BCID  |     "11" |
--! |     |          |          |                 |          |
--! |BC 2 |      L0  |     L1.0 |         L1.1    |     "00" |
--! |BC 2 |     L1.2 |     L1.3 |         L2.0    |     "01" |
--! |BC 2 |     L2.1 |     L2.2 |         L2.3    |     "10" |
--! |BC 2 |      L3  |      HAD |     "000" BCID  |     "11" |
--! |     |          |          |                 |          |
--! |BC...|      L0  |     L1.0 |         L1.1    |     "00" |
--! |BC...|     L1.2 |     L1.3 |         L2.0    |     "01" |
--! |BC...|     L2.1 |     L2.2 |         L2.3    |     "10" |
--! |BC...|      L3  |      HAD |     "000" BCID  |     "11" |
--! |     |          |          |                 |          |
--! |BC 16|      L0  |     L1.0 |         L1.1    |     "00" |
--! |BC 16|     L1.2 |     L1.3 |         L2.0    |     "01" |
--! |BC 16|     L2.1 |     L2.2 |         L2.3    |     "10" |
--! |BC 16|      L3  |      HAD |     "000" BCID  |     "11" |
--!
--! As you can see in the entity declaration, the data type used to interface to
--! from this RAM is AlgoInput, being an array of the AlgoTower data
--! structure containing all the 10-bit energy samples of the 4 + hadronic layers.
--! The conversion from this data-structure (AlgoTower) to
--! std_logic_vector is performed by means of two functions to_LogicVector() and
--! to_AlgoInput().  All the data structures and the functions are defined in
--! AlgoDataTypes.vhd, containing all the functions and data-types used to
--! interface to/from the Algorithm and Sorting modules.
--!
--! @author Francesco Gonnella

--! Use standard library
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
--! Use ipbus library
library ipbus_lib;

--! Use ipbus data-types
use ipbus_lib.ipbus.all;
--! Use internal data-types
use work.DataTypes.all;
--! Use external data-types
use work.AlgoDataTypes.all;
library infrastructure_lib;
use infrastructure_lib.EfexDataFormats.all;

--! @copydoc ipbus_inputRAM.vhd
entity ipbus_inputRAM is
  port(
    clk_ipb : in  std_logic;
    rst     : in  std_logic;
    ipb_in  : in  ipb_wbus;
    ipb_out : out ipb_rbus;

    rclk    : in  std_logic;
    Load    : in  std_logic;
    BCIDIn  : in  BCID_array;
    AlgoIn  : in  AlgoInput;
    we      : in  std_logic := '0';
    AlgoOut : out AlgoInput;
    BCIDOut : out BCID_array
    );

end ipbus_inputRAM;

--! @copydoc ipbus_inputRAM.vhd
architecture rtl of ipbus_inputRAM is

-- algorithm signals
  signal q                  : std_logic_vector(128*60-1 downto 0);
  signal din                : std_logic_vector(128*60-1 downto 0);
  signal counter            : std_logic_vector(3 downto 0) := "0000";  --16 BCs
  signal L1, L2, L3         : std_logic                    := '0';

-- fabric decode signals
  constant BLOCK_WIDTH : integer := 6;  --log2 of the memory address
  signal ipbw : ipb_wbus_array((INPUT_TOWERS - 1) downto 0);
  signal ipbr : ipb_rbus_array((INPUT_TOWERS - 1) downto 0);

  function disable_ram(i : integer) return std_logic is  --only enable RAM for central towers
  begin
    if (i >= 12 and i <= 17) or
      (i >= 22 and i <= 27) or
      (i >= 32 and i <= 37) or
      (i >= 42 and i <= 47) then
      return '0';
    else
      return '1';
    end if;
  end function;


begin

  fabric_decode : entity work.ipbus_fabric_branch
    generic map(
      NSLV        => INPUT_TOWERS,
      DECODE_BASE => BLOCK_WIDTH
      )
    port map(
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      );

--! Synchronize the counter with RAM, accounting for latency
  COUNTER_PROC : process(rclk)
  begin
    if rising_edge(rclk) then
      L3 <= L2;                 -- to compensate RAM read latency should be set (at least) 2 clock cycles before Load
      L2 <= L1;                 -- so we delay by 3,
      L1 <= Load;
      if L2 = '1' then
        counter <= std_logic_vector(unsigned(counter) + 1);
      else
        counter <= counter;
      end if;
      --d_counter <= counter;
    end if;
  end process;


  RAM_FOR : for i in 0 to INPUT_TOWERS-1 generate
    ALGO_INPUT_RAM_WRAPPER : entity work.ipbus_inputRAM_wrapper
      generic map(DISABLE => disable_ram(i))
      port map (
        clk_ipb => clk_ipb,
        rst     => rst,
        ipb_in  => ipbw(i),
        ipb_out => ipbr(i),

        rclk => rclk,
        din  => din((127+128*i) downto 128*i),
        we   => we and Load, --this should be synchronous with input data
        q    => q((127+128*i) downto 128*i),
        addr => counter);
  end generate;

  -- Conversion between algorithm data-types and std_logic_vectors 
  din     <= to_LogicVector(AlgoIn, BCIDIn);
  AlgoOut <= to_AlgoInput(q);
  BCIDOut <= to_BCID(q);
end rtl;
