-------------------------------------------------------
--! @file
--! @brief Wrapper for the input spy/playback RAM 
--! @details 
--! generalised from: ipbus_dpram
--! @author Francesco Gonnella
-------------------------------------------------------

--! Use standard library
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

--! Use ipbus ibrary 
library ipbus_lib;
use ipbus_lib.ipbus.all;

--! Use internal algorithm data types and functions 
use work.DataTypes.all;

--! @copydoc ipbus_inputRAM_wrapper.vhd
entity ipbus_inputRAM_wrapper is
  generic (DISABLE : std_logic := '0');
  port(
    clk_ipb : in  std_logic;
    rst     : in  std_logic;
    ipb_in  : in  ipb_wbus;
    ipb_out : out ipb_rbus;
    rclk    : in  std_logic;

    din  : in  std_logic_vector(127 downto 0);
    we   : in  std_logic := '0';
    q    : out std_logic_vector(127 downto 0);
    addr : in  std_logic_vector(3 downto 0)  --contains 16 BCs
    );

end ipbus_inputRAM_wrapper;

--! @copydoc ipbus_inputRAM_wrapper.vhd
architecture rtl of ipbus_inputRAM_wrapper is

  component AlgoInputRAM
    port (
      clka  : in  std_logic;
      ena   : in  std_logic;
      wea   : in  std_logic_vector(0 downto 0);
      addra : in  std_logic_vector(5 downto 0);
      dina  : in  std_logic_vector(31 downto 0);
      douta : out std_logic_vector(31 downto 0);
      clkb  : in  std_logic;
      enb   : in  std_logic;
      web   : in  std_logic_vector(0 downto 0);
      addrb : in  std_logic_vector(3 downto 0);
      dinb  : in  std_logic_vector(127 downto 0);
      doutb : out std_logic_vector(127 downto 0)
      );
  end component;

  --ipbus signals
  signal ack, ack2    : std_logic;
  signal ipbus_write  : std_logic_vector(0 downto 0);
  signal write_enable : std_logic_vector(0 downto 0);

  --signal for disabling the RAM
  signal ipb_out_int : std_logic_vector(31 downto 0); 
  signal q_int : std_logic_vector(127 downto 0);

begin
  IPBUS_RAM : process(clk_ipb)
  begin
    if rising_edge(clk_ipb) then
      if ipb_in.ipb_strobe = '1' and ipb_in.ipb_write = '1' then
        ipbus_write(0) <= '1';
      else
        ipbus_write(0) <= '0';
      end if;
      ack2 <= ipb_in.ipb_strobe and (not ack2) and (not ack);
      ack  <= ack2;

    end if;
  end process;

  ipb_out.ipb_ack <= ack;
  ipb_out.ipb_err <= '0';
  write_enable(0) <= we;

  ALGO_INPUT_RAM : AlgoInputRAM
    port map (
      clka  => clk_ipb,
      ena   => ipb_in.ipb_strobe,
      wea   => ipbus_write,
      addra => ipb_in.ipb_addr(5 downto 0),
      dina  => ipb_in.ipb_wdata,
      douta => ipb_out_int, --ipb_out.ipb_rdata,
      clkb  => rclk,
      enb   => '1',
      web   => write_enable,
      addrb => addr,
      dinb  => din,
      doutb => q_int
      );

    q <= q_int when DISABLE = '0' else (others => '0');
    ipb_out.ipb_rdata <= ipb_out_int when DISABLE = '0' else x"d15ab1ed";

end rtl;
