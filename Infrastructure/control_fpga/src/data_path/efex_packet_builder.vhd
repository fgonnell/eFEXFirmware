
-- Universal version of packet engine...
--
-- Takes an incoming packet and calculates Header and Payload CRC en route to Aurora
--
-- Dave Sankey, July 2019

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity efex_packet_builder is
  port (
	clk					: in std_logic;
	rst_clk				: in std_logic;
-- FIFO signals
	packet_data         : IN std_logic_vector (63 DOWNTO 0) ;
	packet_valid        : IN std_logic;
   	packet_hdr          : IN std_logic;
	packet_data_end     : IN std_logic;
	packet_ready		: OUT std_logic;
-- towards Aurora readout
	payload_data        : OUT std_logic_vector (63 DOWNTO 0) ;
	payload_valid       : OUT std_logic;
	payload_last        : OUT std_logic;
	tready_data         : IN std_logic
  );
end efex_packet_builder;

architecture universal of efex_packet_builder is

  TYPE STATE_TYPE IS (
    idle,
    capture_l1id,
	do_hdr_crc,
	wait_hdr_crc,
    send_l1id,
    send_payload,
    build_trailer,
	do_trailer_crc,
	wait_trailer_crc,
    send_trailer,
    wait_end
  );

Component CRC20 is
   generic(
     Nbits              :  positive     := 64;
     CRC_Width          :  positive     := 20;
     G_Poly             : Std_Logic_Vector :=x"8349f";
     G_InitVal          : std_logic_vector :=x"fffff"
     );
   port(
     CRC   : out    std_logic_vector(CRC_Width-1 downto 0);
     Calc  : in     std_logic;
     Clk   : in     std_logic;
     DIn   : in     std_logic_vector(Nbits-1 downto 0);
     Reset : in     std_logic);
end Component CRC20;

  signal valid_sig, last_sig, ready_sig, force_ready_sig, input_active_sig: std_logic;
  signal do_crc9, calc_crc20, reset_crc20, send_crc9, send_crc20: std_logic;
  signal input_hdr_sig, input_valid_sig, input_data_end_sig, prefetch_sig: std_logic;
  signal data_sig, inputdata_sig, crc_word: std_logic_vector(63 DOWNTO 0);
  signal crc9val: std_logic_vector(8 DOWNTO 0);
  signal crc20val: std_logic_vector(19 DOWNTO 0);
  signal state_sig: STATE_TYPE;

begin
  payload_data <= data_sig;
  payload_valid <= valid_sig;
  payload_last <= last_sig;
  ready_sig <= tready_data and valid_sig;

  packet_ready <= ((ready_sig or force_ready_sig) and input_active_sig) or prefetch_sig;

crc9_block : CRC20
  GENERIC MAP (
    Nbits     => 64,
    CRC_Width => 9,
    G_Poly    => "011111011",
    G_InitVal => "111111111"
  )
  PORT MAP (
    CRC   => crc9val,
    Calc  => do_crc9,
    Clk   => clk,
    DIn   => crc_word,
    Reset => do_crc9
  );

crc20_block : CRC20
  GENERIC MAP (
    Nbits     => 64,
    CRC_Width => 20,
    G_Poly    => x"8349f",
    G_InitVal => x"fffff"
  )
  PORT MAP (
    CRC   => crc20val,
    Calc  => calc_crc20,
    Clk   => clk,
    DIn   => crc_word,
    Reset => reset_crc20
  );

buffer_input:  process(clk)
  variable hdr_int, valid_int, data_end_int, hdr_buf, valid_buf, data_end_buf: std_logic := '0';
  variable data_int, data_buf: std_logic_vector(63 DOWNTO 0) := (others => '0');
  variable input_active_int: std_logic := '0';
  begin
    if rising_edge(clk) then
      if rst_clk = '1' then
        valid_int := '0';
        valid_buf := '0';
        input_active_int := '0';
      elsif ((state_sig = idle) and (input_active_int = '0')) or (prefetch_sig = '1') or (ready_sig = '1') or (force_ready_sig = '1') then
        data_int := data_buf;
        data_buf := packet_data;
        valid_int := valid_buf;
        valid_buf := packet_valid;
        hdr_int := hdr_buf;
        hdr_buf := packet_hdr;
        data_end_int := data_end_buf;
        data_end_buf := packet_data_end;
      elsif state_sig = wait_hdr_crc then -- Starting new frame...
        input_active_int := '1';
      end if;
      if (input_active_sig = '1') and (packet_valid = '0') then  -- End of incoming frame...
        input_active_int := '0';
      end if;
      inputdata_sig <= data_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      input_valid_sig <= valid_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      input_hdr_sig <= hdr_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      input_data_end_sig <= data_end_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      input_active_sig <= input_active_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process buffer_input;

build_crc_word:  process(clk)
  variable do_crc9_int, calc_crc20_int, reset_crc20_int, send_crc9_int, send_crc20_int: std_logic := '0';
  variable crc_word_int: std_logic_vector(63 downto 0) := (others => '0');
  begin
    if rising_edge(clk) then
      do_crc9_int := '0';
	  calc_crc20_int := '0';
	  reset_crc20_int := '0';
	  send_crc9_int := '0';
	  send_crc20_int := '0';
      case state_sig is
        when capture_l1id =>
          do_crc9_int := '1';
          crc_word_int := inputdata_sig(31 downto 29) & "000000000" & inputdata_sig(19 downto 0) & inputdata_sig(63 downto 32);
        when do_hdr_crc =>
        when wait_hdr_crc =>
	      send_crc9_int := '1';  -- to match CRC arriving
        when send_l1id =>
	      reset_crc20_int := '1';
        when send_payload =>  -- NB low and high words are swapped for CRC calculation!
          if ready_sig = '1' then
            crc_word_int := inputdata_sig(31 downto 0) & inputdata_sig(63 downto 32);
            calc_crc20_int := '1';
          end if;
        when build_trailer =>  -- NB low and high words are swapped for CRC calculation!
          crc_word_int := inputdata_sig(31 downto 0) & x"00000" & inputdata_sig(43 downto 32) ;
	      calc_crc20_int := ready_sig;
        when do_trailer_crc =>
        when wait_trailer_crc =>
          send_crc20_int := '1';  -- to match CRC arriving
        when Others =>
		  crc_word_int := (Others => '0');
      end case;
      do_crc9 <= do_crc9_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      calc_crc20 <= calc_crc20_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      reset_crc20 <= reset_crc20_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      send_crc9 <= send_crc9_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      send_crc20 <= send_crc20_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      crc_word <= crc_word_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process build_crc_word;

send_data:  process(clk)
  variable data_int, next_data, next_data_buf: std_logic_vector(63 DOWNTO 0) := (others => '0');
  variable ready_buf: std_logic := '0';
  begin
    if rising_edge(clk) then
      if send_crc9 = '1' then  -- NB low and high words were swapped for CRC calculation!
        next_data_buf := crc_word(31 downto 0) & crc_word(63 downto 61) & crc9val & crc_word(51 downto 32);
      elsif send_crc20 = '1' then  -- NB low and high words were swapped for CRC calculation!
        next_data_buf := crc20val & crc_word(11 downto 0) & crc_word(63 downto 32);
      elsif input_valid_sig = '1' then
        next_data_buf := inputdata_sig;
      else
        next_data_buf := (Others => '0');
      end if;
      if ready_buf = '1' or force_ready_sig = '1' then
        next_data := next_data_buf;
      end if;
      if ready_sig = '1' and last_sig = '1' then
		data_int := (Others => '0');
      elsif ready_sig = '1' or force_ready_sig = '1' then
        ready_buf := '1';
        data_int := next_data;
      else
        ready_buf := '0';
      end if;
      data_sig <= data_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process send_data;

state_machine:  process(clk)
  variable got_data_end: std_logic := '0';
  variable valid_int, last_int, prefetch_int, force_ready_int: std_logic := '0';
  variable next_state: STATE_TYPE := idle;
  begin
    if rising_edge(clk) then
      if rst_clk = '1' then
        next_state := idle;
      end if;
	  valid_int := '0';
	  last_int := '0';
      prefetch_int := '0';
	  force_ready_int := '0';
	  if input_data_end_sig = '1' then  -- latch end of incoming payload
	    got_data_end := '1';
	  end if;
      case state_sig is
        when capture_l1id =>
		  prefetch_int := '1';
		  next_state := do_hdr_crc;
        when do_hdr_crc =>
		  next_state := wait_hdr_crc;
        when wait_hdr_crc =>
		  force_ready_int := '1';
		  next_state := send_l1id;
        when send_l1id =>
		  valid_int := '1';
		  if got_data_end = '1' then  -- empty packet...
            next_state := build_trailer;
          else
            next_state := send_payload;
	 	  end if;
        when send_payload =>
	      valid_int := '1';
		  if got_data_end = '1' and ready_sig = '1' then
            next_state := build_trailer;
	 	  end if;
        when build_trailer =>
	      if ready_sig = '1' then
            next_state := do_trailer_crc;
          else
            valid_int := '1';
          end if;
        when do_trailer_crc =>
          next_state := wait_trailer_crc;
        when wait_trailer_crc =>
          next_state := send_trailer;
		  force_ready_int := '1';
        when send_trailer =>
	      valid_int := '1';
	      last_int := '1';
	      next_state := wait_end;
        when wait_end =>
	      if ready_sig = '1' then
            next_state := idle;
          else
            valid_int := '1';
	        last_int := '1';
          end if;
        when others =>  -- idle
		  got_data_end := '0';
		  if (input_hdr_sig = '1') and (input_valid_sig = '1') then
		    prefetch_int := '1';
	    	next_state := capture_l1id;
		  end if;
      end case;
      state_sig <= next_state
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      prefetch_sig <= prefetch_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      force_ready_sig <= force_ready_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      last_sig <= last_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      valid_sig <= valid_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process state_machine;

end universal;
