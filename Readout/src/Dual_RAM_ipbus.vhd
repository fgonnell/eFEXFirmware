--! @file
--! @brief Dual Port RAM with read access to IPBus
--! @details 
--! This module creates a DPRAM with write port connected to firmware and read port connected to IPBus
--! The size of the DPRAM is defined by the ADDR_WIDTH generic
--! @author Saeed Taghavi


library IEEE;
library ipbus_lib;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ipbus_lib.ipbus.all;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;
use TOB_rdout_lib.data_type_pkg.all;

entity Dual_RAM_ipbus is
generic(
		ADDR_WIDTH: natural
	);
	port(
		ipb_clk   : in std_logic;
		clk280    : in std_logic;
		reset     : in std_logic;
		ipbus_in  : in ipb_wbus;
		ipbus_out : out ipb_rbus;
		wr_data   : in std_logic := '0';
		data      : in std_logic_vector(31 downto 0) := (others => '0');
		wr_addr   : in std_logic_vector(ADDR_WIDTH - 1 downto 0)
	);
end Dual_RAM_ipbus;

architecture Behavioral of Dual_RAM_ipbus is
type reg_array is array(2 ** ADDR_WIDTH - 1 downto 0) of std_logic_vector(31 downto 0);
	signal reg: reg_array;
	signal sel,addrsel: integer range 0 to 2 ** ADDR_WIDTH - 1 := 0;
	signal ack: std_logic;
	
	--  #### ILA Signals  ###
	signal spy_wr_en_vec       :  std_logic_vector(0 downto 0);
	signal spy_data_in_vec     :  std_logic_vector(31 downto 0);
	signal spy_wr_addr_vec     :  std_logic_vector(31 downto 0);
	signal spy_rd_en_vec       :  std_logic_vector(0 downto 0);
    signal spy_data_out_vec    :  std_logic_vector(31 downto 0);
    signal spy_rd_addr_vec     :  std_logic_vector(31 downto 0);

begin

	sel <= to_integer(unsigned(ipbus_in.ipb_addr(ADDR_WIDTH - 1 downto 0)));
    addrsel <= to_integer(unsigned(wr_addr(ADDR_WIDTH - 1 downto 0)));
    
	process(clk280) -- writes to the ram 
	begin
		if rising_edge(clk280) then
			if wr_data ='1' then
				reg(addrsel) <= data;
			end if;
		end if;
    end process;
		
		process(ipb_clk)  -- read from ram 
		   begin
		      if rising_edge(ipb_clk) then 	
		          if ipbus_in.ipb_strobe='1' and ipbus_in.ipb_write='0' then
			          ipbus_out.ipb_rdata <= reg(sel);
			       end if;  
			       ack <= ipbus_in.ipb_strobe and not ack;
		      end if;
		  end process;
		  
		  ipbus_out.ipb_ack <= ack;
          ipbus_out.ipb_err <= '0';

--    spy_data_in_vec <= data ;
--    spy_wr_addr_vec <= X"00000" & '0' & wr_addr ;
--    spy_wr_en_vec(0)  <= wr_data ;
    
--U1_ila_spy_mem_wr : ila_ipbus_fabric_rd_wr
--PORT MAP (
--	clk    =>  clk280,     
--	probe0 =>  spy_data_in_vec,       -- 32b    
--	probe1 =>  spy_wr_addr_vec,  -- 32b 
--	probe2 =>  spy_wr_en_vec,   -- 1b
--	probe3 =>  (others => '0'),           -- 1b
--	probe4 =>  (others => '0'),   -- 1b
--	probe5 =>  (others => '0'),  -- 32b 
--	probe6 =>  (others => '0'),        -- 32b
--	probe7 =>   (others => '0'),             -- 1b
--	probe8 =>   (others => '0') ,       -- 1b
--    probe9 =>   (others => '0')              -- 1b
--);  

--    spy_data_out_vec  <= reg(sel);
--    spy_rd_addr_vec   <= ipbus_in.ipb_addr ;
--    spy_rd_en_vec(0)  <= ipbus_in.ipb_strobe and ipbus_in.ipb_write ;

--U2_ila_spy_mem_rd : ila_ipbus_fabric_rd_wr
--PORT MAP (
--	clk    =>  ipb_clk,     
--	probe0 =>  spy_data_out_vec,       -- 32b    
--	probe1 =>  spy_rd_addr_vec,  -- 32b 
--	probe2 =>  spy_rd_en_vec,   -- 1b
--	probe3 =>  (others => '0'),           -- 1b
--	probe4 =>  (others => '0'),   -- 1b
--	probe5 =>  (others => '0'),  -- 32b 
--	probe6 =>  (others => '0'),        -- 32b
--	probe7 =>   (others => '0'),             -- 1b
--	probe8 =>   (others => '0') ,       -- 1b
--    probe9 =>   (others => '0')              -- 1b
--);  

end Behavioral;
