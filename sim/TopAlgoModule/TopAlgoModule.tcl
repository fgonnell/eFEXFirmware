#restart -force
onerror {resume}

proc TOB_decode { parent signal name} {
    quietly virtual signal -install $parent [concat $parent/$signal\(31 downto 30\)] $name\_FPGA
    quietly virtual signal -install $parent [concat $parent/$signal\(29 downto 27\)] $name\_ETA
    quietly virtual signal -install $parent [concat $parent/$signal\(26 downto 24\)] $name\_PHI
    quietly virtual signal -install $parent [concat $parent/$signal\(23 downto 22\)] $name\_Had
    quietly virtual signal -install $parent [concat $parent/$signal\(21 downto 20\)] $name\_f3
    quietly virtual signal -install $parent [concat $parent/$signal\(19 downto 18\)] $name\_Reta
    quietly virtual signal -install $parent [concat $parent/$signal\(14\)]           $name\_Max
    quietly virtual signal -install $parent [concat $parent/$signal\(15\)]           $name\_UpNotDown
    quietly virtual signal -install $parent [concat $parent/$signal\(17 downto 16\)] $name\_Seed
    quietly virtual signal -install $parent [concat $parent/$signal\(11 downto 0\)]  $name\_Energy
    add wave -noupdate -group $name [concat $parent/$name\_FPGA	   ]
    add wave -noupdate -group $name [concat $parent/$name\_ETA	   ]
    add wave -noupdate -group $name [concat $parent/$name\_PHI	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Had	   ]
    add wave -noupdate -group $name [concat $parent/$name\_f3	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Reta	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Max	   ]
    add wave -noupdate -group $name [concat $parent/$name\_UpNotDown]
    add wave -noupdate -group $name [concat $parent/$name\_Seed	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Energy   ]
}

proc XTOB_decode { parent signal name} {
    quietly virtual signal -install $parent [concat $parent/$signal\(31 downto 30\)] $name\_FPGA
    quietly virtual signal -install $parent [concat $parent/$signal\(29 downto 27\)] $name\_ETA
    quietly virtual signal -install $parent [concat $parent/$signal\(26 downto 24\)] $name\_PHI
    quietly virtual signal -install $parent [concat $parent/$signal\(23 downto 22\)] $name\_Had
    quietly virtual signal -install $parent [concat $parent/$signal\(21 downto 20\)] $name\_f3
    quietly virtual signal -install $parent [concat $parent/$signal\(19 downto 18\)] $name\_Reta
    quietly virtual signal -install $parent [concat $parent/$signal\(14\)]           $name\_Max
    quietly virtual signal -install $parent [concat $parent/$signal\(15\)]           $name\_UpNotDown
    quietly virtual signal -install $parent [concat $parent/$signal\(17 downto 16\)] $name\_Seed
    quietly virtual signal -install $parent [concat $parent/$signal\(63 downto 48\)]  $name\_Energy
    add wave -noupdate -group $name [concat $parent/$name\_FPGA	   ]
    add wave -noupdate -group $name [concat $parent/$name\_ETA	   ]
    add wave -noupdate -group $name [concat $parent/$name\_PHI	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Had	   ]
    add wave -noupdate -group $name [concat $parent/$name\_f3	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Reta	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Max	   ]
    add wave -noupdate -group $name [concat $parent/$name\_UpNotDown]
    add wave -noupdate -group $name [concat $parent/$name\_Seed	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Energy   ]
}

quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {Remove signals above this line}
add wave -noupdate /tb_algomodule/clk40_p
add wave -noupdate /tb_algomodule/clk40_n

add wave -noupdate -divider Clocks
add wave -noupdate /tb_algomodule/clk40
add wave -noupdate /tb_algomodule/load
add wave -noupdate /tb_algomodule/clk200
add wave -noupdate /tb_algomodule/clk280
add wave -noupdate /tb_algomodule/locked
add wave -noupdate /tb_algomodule/rst

add wave -noupdate -divider IPBus
add wave -noupdate /tb_algomodule/ipb_rst
add wave -noupdate /tb_algomodule/ipb_clk
add wave -noupdate /tb_algomodule/ipb_in_algo
add wave -noupdate /tb_algomodule/ipb_in_sorting
add wave -noupdate /tb_algomodule/ipb_out_algo
add wave -noupdate /tb_algomodule/ipb_out_sorting

add wave -noupdate -divider {Data In}
add wave -noupdate -color Yellow /tb_algomodule/DataReady
add wave -noupdate -color Orange -format Event /tb_algomodule/AlgoData

add wave -noupdate -divider {XTOBs TOBs Out}
add wave -noupdate -color Yellow /tb_algomodule/eg_Start
add wave -noupdate -color Magenta /tb_algomodule/one_eg_Valid
add wave -noupdate -color Magenta /tb_algomodule/eg_Valid
add wave -noupdate -color Cyan /tb_algomodule/eg_TOB

for {set i 0} {$i < 7} {incr i} {
    TOB_decode /tb_algomodule eg_TOB($i) eg_TOB$i\_d
}


add wave -noupdate -color Cyan /tb_algomodule/eg_XTOB
for {set i 0} {$i < 7} {incr i} {
    XTOB_decode /tb_algomodule eg_XTOB($i) eg_XTOB$i\_d
}

add wave -noupdate -color Yellow /tb_algomodule/tau_Start
add wave -noupdate -color Magenta /tb_algomodule/one_tau_Valid
add wave -noupdate -color Magenta /tb_algomodule/tau_Valid
add wave -noupdate -color Cyan /tb_algomodule/tau_TOB

for {set i 0} {$i < 7} {incr i} {
    TOB_decode /tb_algomodule tau_TOB($i) tau_TOB$i\_d
}

add wave -noupdate -color Cyan /tb_algomodule/tau_XTOB

for {set i 0} {$i < 7} {incr i} {
    XTOB_decode /tb_algomodule tau_XTOB($i) tau_XTOB$i\_d
}

add wave -noupdate -divider {Sorted TOBs Out}
add wave -noupdate -color Yellow /tb_algomodule/eg_Sync_out
add wave -noupdate -color Magenta /tb_algomodule/eg_Valid_out
add wave -noupdate -color Cyan /tb_algomodule/eg_TOB_out
TOB_decode /tb_algomodule eg_TOB_out eg_TOB_d

add wave -noupdate -divider Modules
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/CLK200
add wave -noupdate -color Yellow /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_Load
add wave -noupdate -color Orange /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_Data

add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_eg_ParD3
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_eg_ParCEta
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_eg_ParHadron
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_eg_Control
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_tau_ParD3
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_tau_ParCEta
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_tau_ParHadron
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_tau_Control
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_glob_Position
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_glob_Control

add wave -noupdate  -group LoadGenerator /tb_algomodule/TOPALGO/LOAD_GENERATOR/clk
add wave -noupdate  -group LoadGenerator /tb_algomodule/TOPALGO/LOAD_GENERATOR/IN_Load
add wave -noupdate  -group LoadGenerator /tb_algomodule/TOPALGO/LOAD_GENERATOR/Counter
add wave -noupdate  -group LoadGenerator /tb_algomodule/TOPALGO/LOAD_GENERATOR/LoadR
add wave -noupdate  -group LoadGenerator /tb_algomodule/TOPALGO/LOAD_GENERATOR/LoadF
add wave -noupdate  -group LoadGenerator /tb_algomodule/TOPALGO/LOAD_GENERATOR/Load
add wave -noupdate  -group LoadGenerator /tb_algomodule/TOPALGO/LOAD_GENERATOR/OUT_Counter
add wave -noupdate  -group LoadGenerator /tb_algomodule/TOPALGO/LOAD_GENERATOR/OUT_Load

add wave -noupdate -group IPB_fabric /tb_algomodule/TOPALGO/IPBUS_FABRIC/sel
add wave -noupdate -group IPB_fabric /tb_algomodule/TOPALGO/IPBUS_FABRIC/ipb_in
add wave -noupdate -group IPB_fabric /tb_algomodule/TOPALGO/IPBUS_FABRIC/ipb_out
add wave -noupdate -group IPB_fabric /tb_algomodule/TOPALGO/IPBUS_FABRIC/ipb_to_slaves
add wave -noupdate -group IPB_fabric /tb_algomodule/TOPALGO/IPBUS_FABRIC/ipb_from_slaves
add wave -noupdate -group IPB_fabric /tb_algomodule/TOPALGO/IPBUS_FABRIC/sel_i
add wave -noupdate -group IPB_fabric /tb_algomodule/TOPALGO/IPBUS_FABRIC/ored_ack
add wave -noupdate -group IPB_fabric /tb_algomodule/TOPALGO/IPBUS_FABRIC/ored_err
add wave -noupdate -group IPB_fabric /tb_algomodule/TOPALGO/IPBUS_FABRIC/qstrobe

add wave -noupdate -group IPB_LagoRegisters /tb_algomodule/TOPALGO/IPBUS_ALGO_REGISTERS/clk
add wave -noupdate -group IPB_LagoRegisters /tb_algomodule/TOPALGO/IPBUS_ALGO_REGISTERS/reset
add wave -noupdate -group IPB_LagoRegisters /tb_algomodule/TOPALGO/IPBUS_ALGO_REGISTERS/ipbus_in
add wave -noupdate -group IPB_LagoRegisters /tb_algomodule/TOPALGO/IPBUS_ALGO_REGISTERS/ipbus_out
add wave -noupdate -group IPB_LagoRegisters /tb_algomodule/TOPALGO/IPBUS_ALGO_REGISTERS/d
add wave -noupdate -group IPB_LagoRegisters /tb_algomodule/TOPALGO/IPBUS_ALGO_REGISTERS/q
add wave -noupdate -group IPB_LagoRegisters /tb_algomodule/TOPALGO/IPBUS_ALGO_REGISTERS/qmask
add wave -noupdate -group IPB_LagoRegisters /tb_algomodule/TOPALGO/IPBUS_ALGO_REGISTERS/stb
add wave -noupdate -group IPB_LagoRegisters /tb_algomodule/TOPALGO/IPBUS_ALGO_REGISTERS/sel
add wave -noupdate -group IPB_LagoRegisters /tb_algomodule/TOPALGO/IPBUS_ALGO_REGISTERS/reg
add wave -noupdate -group IPB_LagoRegisters /tb_algomodule/TOPALGO/IPBUS_ALGO_REGISTERS/si
add wave -noupdate -group IPB_LagoRegisters /tb_algomodule/TOPALGO/IPBUS_ALGO_REGISTERS/ri
add wave -noupdate -group IPB_LagoRegisters /tb_algomodule/TOPALGO/IPBUS_ALGO_REGISTERS/stat_cyc
add wave -noupdate -group IPB_LagoRegisters /tb_algomodule/TOPALGO/IPBUS_ALGO_REGISTERS/cw_cyc

add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/clk_ipb
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/rst
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ipb_in
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ipb_out
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/rclk
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/we
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/q
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/addr
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ram_ena
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ram_wea
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ram_addra
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ram_dina
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ram_douta
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ram_clkb
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ram_enb
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ram_web
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ram_addrb
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ram_dinb
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ram_doutb
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ack
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ack2
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/ipbus_write
add wave -noupdate -group RAM_Parameter /tb_algomodule/TOPALGO/IPBUS_ALGO_PARAMETER_RAM/write_enable

add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/clk_ipb
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/rst
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/ipb_in
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/ipb_out
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/rclk
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/Load
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/AlgoIn
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/we
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/AlgoOut
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/q
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/din
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/d_counter
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/counter
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/L1
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/L2
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/L3
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/ipbw
add wave -noupdate -group RAM_Input /tb_algomodule/TOPALGO/IPBUS_INPUT_RAM/ipbr

add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/clk_ipb
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/rst
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/ipb_in
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/ipb_out
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/AlgoIn
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/rclk
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/Sync
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/we
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/AlgoOut
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/din
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/q
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/counter
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/BC_counter
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/S1
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/AlgoOut3
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/AlgoOut2
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/AlgoOut1
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/ack
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/ack2
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/ipbus_write
add wave -noupdate -group RAM_Output-eg /tb_algomodule/TOPALGO/IPBUS_OUTPUT_EG_RAM/write_enable

add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/clk_ipb
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/rst
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/ipb_in
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/ipb_out
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/AlgoIn
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/rclk
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/Sync
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/we
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/AlgoOut
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/din
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/q
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/counter
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/BC_counter
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/S1
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/AlgoOut3
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/AlgoOut2
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/AlgoOut1
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/ack
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/ack2
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/ipbus_write
add wave -noupdate -group RAM_Output-tau /tb_algomodule/TOPALGO/IPBUS_OUTPUT_TAU_RAM/write_enable

##########################################
foreach type {eg tau}  {
    for {set i 0} {$i < 7} {incr i} {
	add wave -noupdate               -group SerialSorters_$type\_$i /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/clk
	add wave -noupdate               -group SerialSorters_$type\_$i /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/clk_out
	add wave -noupdate -color Yellow -group SerialSorters_$type\_$i /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/IN_Load
	add wave -noupdate -color Yellow -group SerialSorters_$type\_$i /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/IN_Clear
	add wave -noupdate -color Orange   -group SerialSorters_$type\_$i /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/IN_Data
	add wave -noupdate               -group SerialSorters_$type\_$i /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/serial
	add wave -noupdate               -group SerialSorters_$type\_$i /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/Inhibit
	add wave -noupdate               -group SerialSorters_$type\_$i /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SyncData
	add wave -noupdate               -group SerialSorters_$type\_$i /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/OutputData

	for {set ii 0} {$ii < 4} {incr ii} {
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/clk
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/IN_Clear
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/IN_Previous
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/IN_Parallel
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/IN_Inhibit
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/StoredWord
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/Inhibit
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/Selector
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/OUT_Inhibit
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/OUT_Next
	}

	for {set ii 0} {$ii < 4} {incr ii} {
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/clk
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/IN_Clear
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/IN_Previous
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/IN_Parallel
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/IN_Inhibit
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/StoredWord
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/Inhibit
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/Selector
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/OUT_Inhibit
	    add wave -noupdate -group SerialSorters_$type\_$i -group Cell$ii /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/SortingCells($ii)/SORT_CELL/OUT_Next
	}


	add wave -noupdate -color Yellow -group SerialSorters_$type\_$i /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/OUT_Start
	add wave -noupdate -color Cyan   -group SerialSorters_$type\_$i /tb_algomodule/TOPALGO/out_tob_for($i)/SerialSorter_$type/OUT_Data
    }
}

################ ALGO ##########################
add wave -noupdate -divider -height 50 Algorithm
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/CLK200
add wave -noupdate -color Yellow /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_Load
add wave -noupdate -color Orange /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_Data
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_eg_ParD3
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_eg_ParCEta
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_eg_ParHadron
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_eg_Control
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_tau_ParD3
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_tau_ParCEta
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_tau_ParHadron
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_tau_Control
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_glob_Position
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_glob_Control

add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/eg_ParD3
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/eg_ParCEta
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/eg_ParHadron
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/eg_Control
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/eg_Status
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/eg_Data
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/eg_TOB
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/tau_ParD3
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/tau_ParCEta
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/tau_ParHadron
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/tau_Control
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/tau_Status
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/tau_Data
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/tau_TOB

# Shift Register
add wave -noupdate -group DataShiftRegister /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/DATA_SHIFT_REGISTER/CLK200
add wave -noupdate -color Yellow -group DataShiftRegister /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/DATA_SHIFT_REGISTER/IN_Load
add wave -noupdate -color Orange -group DataShiftRegister /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/DATA_SHIFT_REGISTER/IN_Data
add wave -noupdate -group DataShiftRegister /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/DATA_SHIFT_REGISTER/IN_Edge
add wave -noupdate -group DataShiftRegister /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/DATA_SHIFT_REGISTER/IN_LeftRight
add wave -noupdate -group DataShiftRegister /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/DATA_SHIFT_REGISTER/ShiftTowers
add wave -noupdate -group DataShiftRegister /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/DATA_SHIFT_REGISTER/LeftRight
add wave -noupdate -color Cyan -group DataShiftRegister /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/DATA_SHIFT_REGISTER/OUT_Data

add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/SR_InData
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/SR_LeftRight
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/SR_Edge
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/SR_OutData
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/RAMCounter

for {set i 0} {$i < 8} {incr i} {
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/CLK200
    add wave -noupdate -color Orange -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IN_Data
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IN_ParD3
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IN_ParCEta
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IN_ParHadron
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IN_Control
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/ParCEta
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/ParD3
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/ParRh

    # Seed Finder
    add wave -noupdate -group AlgoEG$i -group SeedFinder /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_FINDER/CLK
    add wave -noupdate -color Orange -group AlgoEG$i -group SeedFinder /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_FINDER/IN_Data
    add wave -noupdate -color Orange -group AlgoEG$i -group SeedFinder /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_FINDER/IN_DataUp
    add wave -noupdate -color Orange -group AlgoEG$i -group SeedFinder /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_FINDER/IN_DataDown
    add wave -noupdate -group AlgoEG$i -group SeedFinder /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_FINDER/Result
    add wave -noupdate -group AlgoEG$i -group SeedFinder /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_FINDER/IsMaxMax
    add wave -noupdate -group AlgoEG$i -group SeedFinder /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_FINDER/UpOrDown
    add wave -noupdate -color Cyan -group AlgoEG$i -group SeedFinder /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_FINDER/OUT_UpNotDown
    add wave -noupdate -color Cyan -group AlgoEG$i -group SeedFinder /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_FINDER/OUT_Seed
    add wave -noupdate -color Cyan -group AlgoEG$i -group SeedFinder /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_FINDER/OUT_IsLocalMax
    add wave -noupdate -color Cyan -group AlgoEG$i -group SeedFinder /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_FINDER/OUT_IsMax

    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SF_Data
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SF_DataUp
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SF_DataDown
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SF_UpNotDown
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SF_Seed
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SF_IsLocalMax
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SF_IsMax

    # Input Mux
    add wave -noupdate -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/CLK
    add wave -noupdate -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/IN_Seed
    add wave -noupdate -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/IN_UpNotDown
    add wave -noupdate -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/IN_Towers
    add wave -noupdate -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/Selector
    add wave -noupdate -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/ShiftTowers
    add wave -noupdate -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/ShiftTowers2
    add wave -noupdate -color Cyan -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/OUT_EnergyL0
    add wave -noupdate -color Cyan -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/OUT_EnergyL1
    add wave -noupdate -color Cyan -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/OUT_EnergyL2
    add wave -noupdate -color Cyan -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/OUT_EnergyL3
    add wave -noupdate -color Cyan -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/OUT_REtaCoreData
    add wave -noupdate -color Cyan -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/OUT_REtaEnvData
    add wave -noupdate -color Cyan -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/OUT_f3CoreData
    add wave -noupdate -color Cyan -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/OUT_f3EnvDataL2
    add wave -noupdate -color Cyan -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/OUT_f3EnvDataL1
    add wave -noupdate -color Cyan -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/OUT_HadCoreData
    add wave -noupdate -color Cyan -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/OUT_HadEnvDataL1
    add wave -noupdate -color Cyan -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/OUT_HadEnvDataL2
    add wave -noupdate -color Cyan -group AlgoEG$i -group InputMux /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/INPUT_MULTIPLEXER/OUT_HadEnvDataL3

    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IM_Towers
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IM_EnergyL0
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IM_EnergyL1
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IM_EnergyL2
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IM_EnergyL3
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IM_REtaCoreData
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IM_REtaEnvData
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IM_f3CoreData
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IM_f3EnvDataL2
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IM_f3EnvDataL1
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IM_HadEnvDataL1
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IM_HadEnvDataL2
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IM_HadEnvDataL3
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/IM_HadCoreData

    # Adders
    foreach cond {ENERGY RETA_ENV RETA_CORE F3ENV HAD_CORE HAD_ENV}  {
	add wave -noupdate -group AlgoEG$i -group Adder$cond /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MULTI_ADDER_$cond/stage
	add wave -noupdate -group AlgoEG$i -group Adder$cond /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MULTI_ADDER_$cond/delay
	add wave -noupdate -group AlgoEG$i -group Adder$cond /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MULTI_ADDER_$cond/CLK
	add wave -noupdate -group AlgoEG$i -group Adder$cond /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MULTI_ADDER_$cond/IN_Words
	add wave -noupdate -group AlgoEG$i -group Adder$cond /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MULTI_ADDER_$cond/connector
	add wave -noupdate -group AlgoEG$i -group Adder$cond /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MULTI_ADDER_$cond/carry
	add wave -noupdate -group AlgoEG$i -group Adder$cond /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MULTI_ADDER_$cond/DelayedOut
	add wave -noupdate -group AlgoEG$i -group Adder$cond /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MULTI_ADDER_$cond/DelayedOverflow
	add wave -noupdate -color Cyan -group AlgoEG$i -group Adder$cond /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MULTI_ADDER_$cond/OUT_Overflow
	add wave -noupdate -color Cyan -group AlgoEG$i -group Adder$cond /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MULTI_ADDER_$cond/OUT_Word

    }

    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MA_EnergyOverflow
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MA_EnergySum
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MA_REtaEnvOverflow
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MA_REtaEnvSum
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MA_REtaCoreOverflow
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MA_REtaCoreSum
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MA_f3EnvOverflow
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MA_f3EnvSum
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MA_f3CoreOverflow
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MA_f3CoreSum
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MA_HadCoreOverflow
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MA_HadCoreSum
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MA_HadEnvOverflow
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MA_HadEnvSum

    # Multipliers
    foreach cond {RETA F3 HADRON}  {
	add wave -noupdate -group AlgoEG$i -group MultReta /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/$cond\_MULTIPLIER/CLK
	add wave -noupdate -group AlgoEG$i -group MultReta /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/$cond\_MULTIPLIER/IN_Word
	add wave -noupdate -group AlgoEG$i -group MultReta /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/$cond\_MULTIPLIER/IN_parameters
	add wave -noupdate -group AlgoEG$i -group MultReta /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/$cond\_MULTIPLIER/OUT_Overflow
	add wave -noupdate -group AlgoEG$i -group MultReta /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/$cond\_MULTIPLIER/OUT_Words
	add wave -noupdate -group AlgoEG$i -group MultReta /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/$cond\_MULTIPLIER/Param
	add wave -noupdate -group AlgoEG$i -group MultReta /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/$cond\_MULTIPLIER/Data
	add wave -noupdate -group AlgoEG$i -group MultReta /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/$cond\_MULTIPLIER/Overflow
	add wave -noupdate -group AlgoEG$i -group MultReta /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/$cond\_MULTIPLIER/Result_big
	add wave -noupdate -group AlgoEG$i -group MultReta /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/$cond\_MULTIPLIER/Result
    }

    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MU_REtaEnvOverflows
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MU_f3CoreOverflows
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MU_HadCoreOverflows
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MU_REtaEnvMult
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MU_f3CoreMult
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/MU_HadCoreMult

    add wave -noupdate -group AlgoEG$i -group SeedDealy /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_DELAY/delay
    add wave -noupdate -group AlgoEG$i -group SeedDealy /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_DELAY/size
    add wave -noupdate -group AlgoEG$i -group SeedDealy /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_DELAY/CLK
    add wave -noupdate -group AlgoEG$i -group SeedDealy /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_DELAY/IN_Signal
    add wave -noupdate -group AlgoEG$i -group SeedDealy /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_DELAY/IN_Word
    add wave -noupdate -group AlgoEG$i -group SeedDealy /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_DELAY/OUT_Signal
    add wave -noupdate -group AlgoEG$i -group SeedDealy /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_DELAY/OUT_Word
    add wave -noupdate -group AlgoEG$i -group SeedDealy /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_DELAY/DelayedWord
    add wave -noupdate -group AlgoEG$i -group SeedDealy /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/SEED_DELAY/DelayedSignal

    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/DL_SeedFinder
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/DL_UpNotDown
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/DL_Seed
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/DL_IsLocalMax
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/DL_IsMax
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/DL_Overflows
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/DL_REtaEnvOverflow
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/DL_f3CoreOverflow
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/DL_HadCoreOverflow

    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/REtaCondition
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/f3Condition
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/HadCondition
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/TOBEnergy
    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/TOBEnergyOverflow

    add wave -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/OUT_Status
    add wave -color Cyan -noupdate -group AlgoEG$i /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/ALGO_GENERATION($i)/AGLO_CORE_EG/OUT_TOB
}

##########################################

add wave -noupdate -color Magenta /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OutputValid
add wave -noupdate -color Cyan    /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OutEgTOB
add wave -noupdate -color Cyan    /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OutTauTOB

add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_ParameterRAMaddress
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_eg_Status
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_tau_Status
add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_glob_Status

##########################################

add wave -noupdate /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_TOB_Counter
add wave -noupdate -color Yellow /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_TOB_Start
add wave -noupdate -color Cyan /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_eg_TOB
add wave -noupdate -color Cyan /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_tau_TOB

TreeUpdate [SetDefaultTree]
WaveRestoreCursors {Start {3300000 ps} 1} {XTOBs {3389285 ps} 1} {Sorted_TOBs {3417857 ps} 1} {Cursor1 {4000 ns} 0}

quietly wave cursor active 4
configure wave -namecolwidth 213
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix -1
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
