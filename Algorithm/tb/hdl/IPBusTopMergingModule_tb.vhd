library ieee;
use ieee.std_logic_1164.all;

library algolib;
use algolib.DataTypes.all;
use algolib.AlgoDataTypes.all;

library ipbus_lib;
use ipbus_lib.ipbus_reg_types.all;

use ipbus_lib.ipbus.all;
----------------------------------------------------------------------------------------------------------------------------------------------------------------

entity IPBusTopMergingModule_tb is
-- hello
end entity IPBusTopMergingModule_tb;

architecture TestBench of IPBusTopMergingModule_tb is

  -- component ports
  signal CLK       : std_logic := '0';
  signal Data      : AlgoTriggerObjects(3 downto 0);
  signal Sync      : std_logic;
  signal ipb_clk   : std_logic;
  signal ipb_rst   : std_logic;
  signal ipb_in    : ipb_wbus;
  signal ipb_out   : ipb_rbus;
  signal OUT_Sync  : std_logic;
  signal OUT_Valid : std_logic;
  signal OUT_TOB   : AlgoTriggerObject;


begin  -- architecture std

  -- component instantiation
  DUT : entity algolib.IPBusTopMergingModule
    generic map (TEST => true)
    port map (
      CLK       => CLK,
      IN_Data   => Data,
      IN_Sync   => Sync,
      ipb_clk   => ipb_clk,
      ipb_rst   => ipb_rst,
      ipb_in    => ipb_in,
      ipb_out   => ipb_out,
      OUT_Sync  => OUT_Sync,
      OUT_Valid => OUT_Valid,
      OUT_TOB   => OUT_TOB);

  -- clock generation
  Clk <= not Clk after 1.78571429 ns;


  -- waveform generation
  WaveGen_Proc : process
  begin
    -- insert signal assignments here
    Data <= (x"00000000", x"00000000", x"00000000", x"00000000");
    Sync <= '0';
    wait until Clk = '1'; wait until Clk = '1'; wait until Clk = '1';


    Sync <= '1';
    Data <= (x"00000099", x"00000161", x"00000062", x"00000063");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000098", x"00000151", x"00000052", x"00000053");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000097", x"00000141", x"00000042", x"00000043");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000096", x"00000131", x"00000032", x"00000033");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000095", x"00000121", x"00000022", x"00000023");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000094", x"00000111", x"00000012", x"00000013");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000093", x"00000191", x"00000002", x"00000003");
    wait until Clk = '1';


    Sync <= '1';
    Data <= (x"00000060", x"00000261", x"00000062", x"00000063");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000050", x"00000251", x"00000052", x"00000073");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000040", x"00000241", x"00000042", x"00000043");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000030", x"00000231", x"00000032", x"00000033");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000020", x"00000221", x"00000022", x"00000023");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000010", x"00000211", x"00000012", x"00000013");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000000", x"00000201", x"00000002", x"00000003");
    wait until Clk = '1';


    Sync <= '1';
    Data <= (x"00000060", x"00000061", x"00000062", x"00000063");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000050", x"00000051", x"00000052", x"00000053");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000040", x"00000041", x"00000042", x"00000043");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000030", x"00000031", x"00000032", x"00000033");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000020", x"00000021", x"00000022", x"00000023");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000010", x"00000011", x"00000012", x"00000013");
    wait until Clk = '1';

    Sync <= '0';
    Data <= (x"00000000", x"00000001", x"00000002", x"00000003");
    wait until Clk = '1';

  end process WaveGen_Proc;



end architecture TestBench;
