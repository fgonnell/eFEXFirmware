-- Dave Sankey, September 2019
-- Emulate MGT giving counting data

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity TB_CRC_checker is
	port(
		clk				: IN STD_LOGIC;
		rst_clk			: IN STD_LOGIC;
		payload_data	: IN std_logic_vector(63 DOWNTO 0);
		payload_last	: IN std_logic;
		payload_valid	: IN std_logic;
		tready_data		: IN std_logic;
		header_crc_ok	: out std_logic;
		payload_crc_ok	: out std_logic
	);

end TB_CRC_checker;

architecture initial of TB_CRC_checker is

  TYPE INCOMING_STATE_TYPE IS (
    idle,
    capture_l1id,
    capture_payload
  );

  TYPE CHECKING_STATE_TYPE IS (
    idle,
    check_l1id,
    check_payload
  );

Component CRC20 is
   generic(
     Nbits              :  positive     := 64;
     CRC_Width          :  positive     := 20;
     G_Poly             : Std_Logic_Vector :=x"8349f";
     G_InitVal          : std_logic_vector :=x"fffff"
     );
   port(
     CRC   : out    std_logic_vector(CRC_Width-1 downto 0);
     Calc  : in     std_logic;
     Clk   : in     std_logic;
     DIn   : in     std_logic_vector(Nbits-1 downto 0);
     Reset : in     std_logic);
end Component CRC20;

Component dpram32_64 is
	generic(
		ADDR_WIDTH: positive := 12
	);
	port(
		clk32: in std_logic;
		we32: in std_logic := '0';
		d32: in std_logic_vector(31 downto 0);
		q32: out std_logic_vector(31 downto 0);
		addr32: in std_logic_vector(ADDR_WIDTH downto 0);
		clk64: in std_logic;
		we64: in std_logic := '0';
		d64: in std_logic_vector(63 downto 0);
		q64: out std_logic_vector(63 downto 0);
		addr64: in std_logic_vector(ADDR_WIDTH - 1 downto 0)
	);
end Component dpram32_64;

  signal addr32_sig: std_logic_vector(9 downto 0);
  signal addr64_sig: std_logic_vector(8 downto 0);
  signal clk32, rst32, we64_sig, do_crc9_sig, do_crc20_sig: std_logic;
  signal crc_word, data32_sig: std_logic_vector(31 downto 0);

Begin


DPRAM: dpram32_64
	generic map(
		ADDR_WIDTH => 8
	)
	port map (
		clk32	=> clk32,
		we32	=> '0',
		d32		=> (Others => '0'),
		q32		=> data32_sig,
		addr32	=> addr32_sig,
		clk64	=> clk,
		we64	=> we64_sig,
		d64		=> payload_data,
		q64		=> Open,
		addr64	=> addr64_sig
	);

crc9 : CRC20
  GENERIC MAP (
    Nbits     => 32,
    CRC_Width => 9,
    G_Poly    => "011111011",
    G_InitVal => "111111111"
  )
  PORT MAP (
    CRC   => crc9val,
    Calc  => do_crc9,
    Clk   => clk32,
    DIn   => crc_word,
    Reset => reset_crc
  );

crc20 : CRC20
  GENERIC MAP (
    Nbits     => 32,
    CRC_Width => 20,
    G_Poly    => x"8349f",
    G_InitVal => x"fffff"
  )
  PORT MAP (
    CRC   => crc20val,
    Calc  => do_crc20,
    Clk   => clk32,
    DIn   => crc_word,
    Reset => reset_crc
  );

  we64_sig <= '1' when ((payload_valid = '1') and (tready_data = '1')) else '0';

Fast_Clock: Process(clk)
  Begin
    If (rising_edge(clk) or falling_edge(clk)) then -- ensure clk32 is delta time after clk
      clk32 <= '1', '0' after 1 ns;
      rst32 <= rst_clk;
    end if;
  End process Fast_Clock;

capture_data:  process(clk32)
  variable next_addr: unsigned(7 downto 0) := (others => '0');
  variable end_addr: std_logic_vector(9 downto 0) := (others => '0');
  variable do_crc, ram_page: std_logic := '0';
  begin
    if rising_edge(clk32) then
      do_crc := '0';
      if rst32 = '1' then
        next_addr := (Others => '0');
        end_addr := (Others => '0');
        ram_page := '0';
      elsif clk = '1' then -- correct delta time wrt clk...
        if ((payload_valid = '1') and (tready_data)) then
          if payload_last = '1' then
        	do_crc := '1';
            end_addr := ram_page & std_logic_vector(next_addr) & "0";
			next_addr := (Others => '0');
			if ram_page = '0' then
			  ram_page := '1';
			else
			  ram_page := '0';
			end if;
          else
    	    next_addr := next_addr + 1;
		  end if;
		end if;
	  end if;
      addr64_sig <= ram_page & std_logic_vector(next_addr)
-- pragma translate_off
      after 1 ns
-- pragma translate_on
      ;
      end_addr_sig <= end_addr
-- pragma translate_off
      after 1 ns
-- pragma translate_on
      ;
      do_crc_sig <= do_crc
-- pragma translate_off
      after 1 ns
-- pragma translate_on
      ;
    end if;
  end process capture_data;

End architecture initial;
