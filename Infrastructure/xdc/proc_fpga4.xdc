###### This is XDC file for processing FPGA 4 ###########
create_clock -period 8.000  -name gt_clk125_p [get_ports gt_clk125_p]

###### Ethernet RefClk (125MHz)
set_property IOSTANDARD LVDS [get_ports  gt_clk125_p]
set_property PACKAGE_PIN K17 [get_ports gt_clk125_p]
set_property PACKAGE_PIN J16 [get_ports gt_clk125_n]
set_property IOSTANDARD LVDS [get_ports  gt_clk125_n]
set_property DIFF_TERM TRUE [get_ports gt_clk125_p]
set_property DIFF_TERM TRUE [get_ports gt_clk125_n]

create_generated_clock -name mac_clk [get_pins clock_resources/clk_gen/inst/mmcm_adv_inst/CLKOUT0]
create_generated_clock -name ipb_clk [get_pins clock_resources/clk_gen/inst/mmcm_adv_inst/CLKOUT1]

set_clock_groups -asynchronous -group [get_clocks ipb_clk]
set_clock_groups -asynchronous -group [get_clocks mac_clk]

###### creating delay for the processing FPGA
#set_input_delay  -clock mac_clk -min 0.000 [get_ports {master_rx_data[*]}]
#set_input_delay -clock mac_clk -max 2.000 [get_ports {master_rx_data[*]}]
#set_input_delay -clock mac_clk -min 0.000 [get_ports master_tx_pause]
#set_input_delay -clock mac_clk -max 2.000 [get_ports master_tx_pause]
#
#set_output_delay -clock mac_clk -min 4.000 [get_ports {master_tx_data[*]}]
#set_output_delay -clock mac_clk -max -0.500 [get_ports {master_tx_data[*]}]

###### SPI generated clock
create_generated_clock -name flash_spi_clk -source [get_pins slaves/spi_flash/spi_clk_reg/C] -divide_by 2 [get_pins slaves/spi_flash/spi_clk_reg/Q]

###### FPGA4_GCLK1_40M08
set_property IOSTANDARD LVDS [get_ports ttc_clk_p]
set_property IOSTANDARD LVDS [get_ports ttc_clk_n]
set_property PACKAGE_PIN AW14 [get_ports ttc_clk_n]
set_property PACKAGE_PIN AV14 [get_ports ttc_clk_p]
set_property DIFF_TERM TRUE [get_ports ttc_clk_p]
set_property DIFF_TERM TRUE [get_ports ttc_clk_n]

###### SPI Flash Pins
set_property PACKAGE_PIN BA29 [get_ports flash_csn]
set_property IOSTANDARD LVCMOS18 [get_ports flash_csn]
set_property PACKAGE_PIN BC27 [get_ports flash_mosi]
set_property IOSTANDARD LVCMOS18 [get_ports flash_mosi]
set_property PACKAGE_PIN BD27 [get_ports flash_miso]
set_property IOSTANDARD LVCMOS18 [get_ports flash_miso]

###### LEDs   #########################
set_property IOSTANDARD LVCMOS18 [get_ports flash_led]
set_property PACKAGE_PIN AP29  [get_ports flash_led]

###### F4 Geographical address
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_geo_addr[*]}]
set_property PACKAGE_PIN AL28 [get_ports {fpga_geo_addr[0]}]
set_property PACKAGE_PIN AL29 [get_ports {fpga_geo_addr[1]}]

###### Hardware address   Pins
set_property IOSTANDARD LVCMOS18 [get_ports {hardware_addr[*]}]
set_property PACKAGE_PIN AR29 [get_ports {hardware_addr[0]}]
set_property PACKAGE_PIN AM30 [get_ports {hardware_addr[1]}]
set_property PACKAGE_PIN AK27 [get_ports {hardware_addr[2]}]
set_property PACKAGE_PIN AK28 [get_ports {hardware_addr[3]}]
set_property PACKAGE_PIN AN29 [get_ports {hardware_addr[4]}]
set_property PACKAGE_PIN AK29 [get_ports {hardware_addr[5]}]
set_property PACKAGE_PIN AM28 [get_ports {hardware_addr[6]}]
set_property PACKAGE_PIN AJ29 [get_ports {hardware_addr[7]}]

###### Tx and Rx signal between FPGA4 and Control FPGA ##########
set_property IOSTANDARD LVCMOS18 [get_ports {master_tx_data[*]}]

set_property PACKAGE_PIN C18 [get_ports {master_tx_data[0]}]
set_property PACKAGE_PIN B16 [get_ports {master_tx_data[1]}]
set_property PACKAGE_PIN D19 [get_ports {master_tx_data[2]}]
set_property PACKAGE_PIN C17 [get_ports {master_tx_data[3]}]
set_property PACKAGE_PIN A19 [get_ports {master_tx_data[4]}]
set_property PACKAGE_PIN D17 [get_ports {master_tx_data[5]}]
set_property PACKAGE_PIN E17 [get_ports {master_tx_data[6]}]
set_property PACKAGE_PIN G18 [get_ports {master_tx_data[7]}]
set_property PACKAGE_PIN G16 [get_ports {master_tx_data[8]}]
set_property PACKAGE_PIN E19 [get_ports {master_tx_data[9]}]

set_property IOSTANDARD LVCMOS18 [get_ports master_tx_pause]
set_property PACKAGE_PIN J17 [get_ports master_tx_pause]

set_property IOSTANDARD LVCMOS18 [get_ports {master_rx_data[*]}]

set_property PACKAGE_PIN B18 [get_ports {master_rx_data[0]}]
set_property PACKAGE_PIN A16 [get_ports {master_rx_data[1]}]
set_property PACKAGE_PIN C19 [get_ports {master_rx_data[2]}]
set_property PACKAGE_PIN B17 [get_ports {master_rx_data[3]}]
set_property PACKAGE_PIN A18 [get_ports {master_rx_data[4]}]
set_property PACKAGE_PIN D16 [get_ports {master_rx_data[5]}]
set_property PACKAGE_PIN E16 [get_ports {master_rx_data[6]}]
set_property PACKAGE_PIN F18 [get_ports {master_rx_data[7]}]
set_property PACKAGE_PIN F16 [get_ports {master_rx_data[8]}]
set_property PACKAGE_PIN E18 [get_ports {master_rx_data[9]}]

###### master tx and rx
set_property IOB TRUE [get_ports {master_rx_data[*]}]
set_property IOB TRUE [get_ports {master_tx_data[*]}]
set_property IOB TRUE [get_ports master_tx_pause]

###### Control FPGA Ready signal inputs
# F1_F5_RDOUT_0_p pin is used for RAW
# F1_F5_RDOUT_0_n pin is used for TOB
set_property IOSTANDARD LVCMOS18 [get_ports {ctrl_RAW_ready_in}]
set_property IOSTANDARD LVCMOS18 [get_ports {ctrl_TOB_ready_in}]
set_property PACKAGE_PIN C15 [get_ports {ctrl_RAW_ready_in}]
set_property PACKAGE_PIN C14 [get_ports {ctrl_TOB_ready_in}]

###### ECRID from F5 to F4  TTC_5_p TO TTC_8_n
set_property IOSTANDARD LVCMOS18 [get_ports {ECRID[*]}]
set_property IOB TRUE [get_ports {ECRID[*]}]
set_property PACKAGE_PIN AN14 [get_ports {ECRID[0]}]
set_property PACKAGE_PIN AP14 [get_ports {ECRID[1]}]
set_property PACKAGE_PIN AR13 [get_ports {ECRID[2]}]
set_property PACKAGE_PIN AR12 [get_ports {ECRID[3]}]
set_property PACKAGE_PIN AU12 [get_ports {ECRID[4]}]
set_property PACKAGE_PIN AU11 [get_ports {ECRID[5]}]
set_property PACKAGE_PIN AT14 [get_ports {ECRID[6]}]
set_property PACKAGE_PIN AT13 [get_ports {ECRID[7]}]

###### TTC information signals
set_property IOSTANDARD LVDS [get_ports {ttc_inform_n[*]}]
set_property IOSTANDARD LVDS [get_ports {ttc_inform_p[*]}]
set_property DIFF_TERM true  [get_ports {ttc_inform_p[*]}]
set_property DIFF_TERM true  [get_ports {ttc_inform_n[*]}]
set_property PACKAGE_PIN AJ14 [get_ports {ttc_inform_p[0]}]
set_property PACKAGE_PIN AM13 [get_ports {ttc_inform_p[1]}]
set_property PACKAGE_PIN AK13 [get_ports {ttc_inform_p[2]}]
set_property PACKAGE_PIN AN13 [get_ports {ttc_inform_p[3]}]
set_property IOB TRUE [get_ports {ttc_inform_n[*]}]
set_property IOB TRUE [get_ports {ttc_inform_p[*]}]
