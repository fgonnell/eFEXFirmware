----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Creabe Date:
-- Design Name:
-- Module Name:
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: read data from file:
-- this is the data format, spaces matter
--  samples are 4-nibble words in hex
--  samples are in this order:
--      L0.0<3 spaces>L1.0<space>L1.1<space>L1.2<space>L1.3<3 spaces>L2.0<space>L2.1<space>L2.2<space>L2.3<3 spaces>L3.0<3 spaces>HAD.0<end of line>
--  samples of the same layer are separated by 1 space
--  different layes are separated by 3 spaces
--  the moudule needs 9 trigger towers:
--   6 7 8
--   3 4 5 Tower number 4 is the core
--   0 1 2  
-- different data samples - pertaining to different clock cycles - are separated by an empty line (or a line that will be ignored)
-- the file must end with 2 empy lines (or 2 lines that will be ignored)
-- the file can contain an arbitrary number of data samples
-- example with all zeroes:

-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- <empty line>
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- <empty line>
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- 0000   0000 0000 0000 0000   0000 0000 0000 0000   0000   0000
-- <empty line>
-- <empty line>

-- Dependencies: DataTypes package, Utility
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;


-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library std;
use std.textio.all;             --TEXT is here


library algolib;
use algolib.DataTypes.all;
use algolib.AlgoDataTypes.all;
use algolib.Utility.all;

entity FileReader is
  generic(FILENAME   : string := "../../../../../../sim/TopAlgoModule/BigTowers.txt";
          RESET_TIME : time   := 48 ns;
          DEAD_TIME  : time   := 2.5 us);
  port(
    DATA_OUT   : out AlgoInput;
    BCID_OUT   : out BCID_array;
    DATA_READY : out std_logic;
    CLK40      : out std_logic;
    CLK160     : out std_logic;
    CLK200     : out std_logic;
    CLK280     : out std_logic;
    LOAD       : out std_logic;
    RESET      : out std_logic);
end FileReader;

--
architecture std of FileReader is
  constant PERIOD : time := 25 ns;  -- 40 MHz;
  constant OFFSET : time := 1 ns;   -- delay between data and clock (must be less then half the period of the fastes clock 200MHz  T/2 = 2.5 ns)

  file stimulus   : text open read_mode is FILENAME;
  signal clock40  : std_logic;
  signal load_i   : std_logic;
  signal clock160 : std_logic;
  signal clock200 : std_logic;
  signal clock280 : std_logic;
  signal DataOut  : TriggerTowers(59 downto 0);
  signal counter  : integer := 0;


begin

  data_out_for_col : for j in 0 to 5 generate
    data_out_for_row : for i in 0 to 9 generate
      DATA_OUT(j)(i).Layer0(0) <= DataOut(10*j+i).Layer0(0)(INPUT_DATA_WIDTH-1 downto 0);
      DATA_OUT(j)(i).Layer1(3) <= DataOut(10*j+i).Layer1(3)(INPUT_DATA_WIDTH-1 downto 0);
      DATA_OUT(j)(i).Layer1(2) <= DataOut(10*j+i).Layer1(2)(INPUT_DATA_WIDTH-1 downto 0);
      DATA_OUT(j)(i).Layer1(1) <= DataOut(10*j+i).Layer1(1)(INPUT_DATA_WIDTH-1 downto 0);
      DATA_OUT(j)(i).Layer1(0) <= DataOut(10*j+i).Layer1(0)(INPUT_DATA_WIDTH-1 downto 0);
      DATA_OUT(j)(i).Layer2(3) <= DataOut(10*j+i).Layer2(3)(INPUT_DATA_WIDTH-1 downto 0);
      DATA_OUT(j)(i).Layer2(2) <= DataOut(10*j+i).Layer2(2)(INPUT_DATA_WIDTH-1 downto 0);
      DATA_OUT(j)(i).Layer2(1) <= DataOut(10*j+i).Layer2(1)(INPUT_DATA_WIDTH-1 downto 0);
      DATA_OUT(j)(i).Layer2(0) <= DataOut(10*j+i).Layer2(0)(INPUT_DATA_WIDTH-1 downto 0);
      DATA_OUT(j)(i).Layer3(0) <= DataOut(10*j+i).Layer3(0)(INPUT_DATA_WIDTH-1 downto 0);
      DATA_OUT(j)(i).Hadron(0) <= DataOut(10*j+i).Hadron(0)(INPUT_DATA_WIDTH-1 downto 0);
    end generate data_out_for_row;
  end generate data_out_for_col;


  CLK40  <= clock40;
  CLK160 <= clock160;
  CLK200 <= clock200;
  CLK280 <= clock280;
  LOAD   <= load_i;

  Clock40Generator : process
  begin
    clock40 <= '1';
    wait for 0.5*PERIOD;
    clock40 <= '0';
    wait for 0.5*PERIOD;
  end process;

  LoadGenerator : process
  begin
    load_i <= '1';
    wait for 0.1*PERIOD;
    load_i <= '0';
    wait for 0.8*PERIOD;
    load_i <= '1';
    wait for 0.1*PERIOD;
  end process;

  Clock160Generator : process
  begin
    clock160 <= '1';
    wait for 0.5*(PERIOD/4);
    clock160 <= '0';
    wait for 0.5*(PERIOD/4);
  end process;

  Clock200Generator : process
  begin
    clock200 <= '1';
    wait for 0.5*(PERIOD/5);
    clock200 <= '0';
    wait for 0.5*(PERIOD/5);
  end process;

  Clock280Generator : process
  begin
    clock280 <= '1';
    wait for 0.5*(PERIOD/7);
    clock280 <= '0';
    wait for 0.5*(PERIOD/7);
  end process;

  counter_proc : process(clock40)
  begin
    if rising_edge(clock40) then
      counter <= counter + 1;
    end if;
  end process;

  minc : for i in 0 to 59 generate
    BCID_OUT(i) <= std_logic_vector(to_unsigned(counter, BCID_OUT(i)'length));
  end generate;

  stim_proc : process
    variable l            : line;
    variable s            : string(DATA_WIDTH/4 downto 1);
    variable three_spaces : string(3 downto 1);
    variable space        : string(1 downto 1);
    variable n            : integer;
    variable DataString   : string(DATA_WIDTH downto 1);

  begin
    n          := 0;
    DataOut    <= (others => ZERO_TRIGGER_TOWER);
    DATA_READY <= '0';
    RESET      <= '1';
    wait for RESET_TIME;
    reset      <= '0';
    wait for DEAD_TIME;
    DataOut    <= (others => ZERO_TRIGGER_TOWER);
    DATA_READY <= '0';

    wait for 9 * PERIOD;

    wait until clock40 = '0';
    wait for (0.5*PERIOD - OFFSET);

    while not endfile(stimulus) loop
      for i in 0 to DataOut'high loop  -- 0 to 59
        readline(stimulus, l);
        n                    := n+1;
        --layer 0
        read(l, s);
        DataOut(i).Layer0(0) <= to_std_logic_vector(str_hex_to_bin(s));
        --3 spaces
        read(l, three_spaces);
        if (three_spaces /= "   ") then
          report "Syntax error in file!" severity error;
        end if;

        --layer 1
        for j in 0 to 3 loop
          read(l, s);
          DataOut(i).Layer1(j) <= to_std_logic_vector(str_hex_to_bin(s));
          if j < 3 then
            --1 space
            read(l, space);
            if (space /= " ") then
              report "Syntax error in file in layer 1!" severity error;
            end if;
          end if;
        end loop;
        --3 spaces
        read(l, three_spaces);
        if (three_spaces /= "   ") then
          report "Syntax error in file after layer 1!" severity error;
        end if;

        --layer 2
        for j in 0 to 3 loop
          read(l, s);
          DataOut(i).Layer2(j) <= to_std_logic_vector(str_hex_to_bin(s));
          if j < 3 then
            --1 space
            read(l, space);
            if (space /= " ") then
              report "Syntax error in file in layer 2!" severity error;
            end if;
          end if;
        end loop;
        --3 spaces
        read(l, three_spaces);
        if (three_spaces /= "   ") then
          report "Syntax error in file after layer 2!" severity error;
        end if;

        --layer 3
        read(l, s);
        DataOut(i).Layer3(0) <= to_std_logic_vector(str_hex_to_bin(s));
        --3 spaces
        read(l, three_spaces);
        if (three_spaces /= "   ") then
          report "Syntax error in file after layer 2!" severity error;
        end if;

        --hadronic
        read(l, s);
        DataOut(i).Hadron(0) <= to_std_logic_vector(str_hex_to_bin(s));
      end loop;

      readline(stimulus, l);    --Blank line after a set of trigger towers                
      n := n+1;

      DATA_READY <= '1';

      wait until clock40 = '0';
      wait for (0.5*PERIOD - OFFSET);
    end loop;

    DataOut    <= (others => ZERO_TRIGGER_TOWER);
    DATA_READY <= '0';
    wait for 1000 us;
  end process;
end std;

