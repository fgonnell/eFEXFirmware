--! @file
--! @brief eFEX data-types and functions
--! @details
--! This file/package contains all types, constants, and functions used to
--! interface from/to the eFEX.
--! The following table reports the connection between the hardware and the
--! fw channel number. For FPGA1
--! | FW Ch  |  MGT Quad |MiniPOD |   Type   |
--! |--------|-----------|--------|----------|
--! | MGT_00 |    110    |  E03   |   ECal   |
--! | MGT_01 |    110    |  E09   |   ECal   |
--! | MGT_02 |    110    |  E07   |   ECal   |
--! | MGT_03 |    110    |  E01   |   ECal   |
--! | MGT_04 |    111    |  E05   |   ECal   |
--! | MGT_05 |    111    |  E0A   |   ECal   |
--! | MGT_06 |    111    |  E08   |   ECal   |
--! | MGT_07 |    111    |  E04   |   ECal   |
--! | MGT_08 |    112    |  E06   |   ECal   |
--! | MGT_09 |    112    |  E02   |   ECal   |
--! | MGT_10 |    112    |  E13   |   ECal   |
--! | MGT_11 |    112    |  E17   |   ECal   |
--! | MGT_12 |    113    |  E15   |   ECal   |
--! | MGT_13 |    113    |  E19   |   ECal   |
--! | MGT_14 |    113    |  E1A   |   ECal   |
--! | MGT_15 |    113    |  E18   |   ECal   |
--! | MGT_16 |    114    |  E11   |   ECal   |
--! | MGT_17 |    114    |  E16   |   ECal   |
--! | MGT_18 |    114    |  E12   |   ECal   |
--! | MGT_19 |    114    |  E14   |   ECal   |
--! | MGT_20 |    115    |  E22   |   ECal   |
--! | MGT_21 |    115    |  E21   |   ECal   |
--! | MGT_22 |    115    |  E33   |   ECal   |
--! | MGT_23 |    115    |  E25   |   ECal   |
--! | MGT_24 |    116    |  E35   |   ECal   |
--! | MGT_25 |    116    |  E23   |   ECal   |
--! | MGT_26 |    116    |  E32   |   ECal   |
--! | MGT_27 |    116    |  E29   |   ECal   |
--! | MGT_28 |    117    |  E31   |   ECal   |
--! | MGT_29 |    117    |  E27   |   ECal   |
--! | MGT_30 |    117    |  E37   |   ECal   |
--! | MGT_31 |    117    |  E2A   |   ECal   |
--! | MGT_32 |    118    |  E39   |   ECal   |
--! | MGT_33 |    118    |  E24   |   ECal   |
--! | MGT_34 |    118    |  E3A   |   ECal   |
--! | MGT_35 |    118    |  E26   |   ECal   |
--! | MGT_36 |    119    |  E38   |   ECal   |
--! | MGT_37 |    119    |  E36   |   ECal   |
--! | MGT_38 |    119    |  E28   |   ECal   |
--! | MGT_39 |    119    |  E34   |   ECal   |
--! | MGT_40 |    211    |  H09   |   HCal-  |
--! | MGT_41 |    211    |  H07   |   HCal   |
--! | MGT_42 |    211    |  H0B   |   HCal-  |
--! | MGT_43 |    211    |  H0A   |   HCal-  |
--! | MGT_44 |    212    |  H05   |   HCal-  |
--! | MGT_45 |    212    |  H03   |   HCal-  |
--! | MGT_46 |    212    |  H04   |   HCal-  |
--! | MGT_47 |    212    |  H01   |   HCal   |
--! | MGT_48 |    213    |  H08   |   HCal   |
--! | MGT_49 |    213    |  H00   |   HCal   |
--! | MGT_50 |    213    |  H02   |   HCal   |
--! | MGT_51 |    213    |  H06   |   HCal   |
--! | MGT_52 |    214    |        |          |
--! | MGT_53 |    214    |        |          |
--! | MGT_54 |    214    |  H13   |   HCal-  |
--! | MGT_55 |    214    |  H15   |   HCal-  |
--! | MGT_56 |    215    |  H14   |   HCal-  |
--! | MGT_57 |    215    |  H11   |   HCal   |
--! | MGT_58 |    215    |  H10   |   HCal   |
--! | MGT_59 |    215    |  H12   |   HCal   |
--! | MGT_60 |    216    |        |          |
--! | MGT_61 |    216    |        |          |
--! | MGT_62 |    216    |        |          |
--! | MGT_63 |    216    |        |          |
--!
--!
--! For FPGA2
--! | FW Ch  |  MGT Quad |MiniPOD |   Type   |
--! |--------|-----------|--------|----------|
--! | MGT_00 |    110    |  E6A   |   ECal   |
--! | MGT_01 |    110    |  E68   |   ECal   |
--! | MGT_02 |    110    |  E62   |   ECal   |
--! | MGT_03 |    110    |  E64   |   ECal   |
--! | MGT_04 |    111    |  E63   |   ECal   |
--! | MGT_05 |    111    |  E66   |   ECal   |
--! | MGT_06 |    111    |  E61   |   ECal   |
--! | MGT_07 |    111    |  E67   |   ECal   |
--! | MGT_08 |    112    |  E65   |   ECal   |
--! | MGT_09 |    112    |  E69   |   ECal   |
--! | MGT_10 |    112    |  E7A   |   ECal   |
--! | MGT_11 |    112    |  E79   |   ECal   |
--! | MGT_12 |    113    |  E77   |   ECal   |
--! | MGT_13 |    113    |  E75   |   ECal   |
--! | MGT_14 |    113    |  E71   |   ECal   |
--! | MGT_15 |    113    |  E73   |   ECal   |
--! | MGT_16 |    114    |  E76   |   ECal   |
--! | MGT_17 |    114    |  E72   |   ECal   |
--! | MGT_18 |    114    |  E74   |   ECal   |
--! | MGT_19 |    114    |  E78   |   ECal   |
--! | MGT_20 |    115    |  E82   |   ECal   |
--! | MGT_21 |    115    |  E84   |   ECal   |
--! | MGT_22 |    115    |  E81   |   ECal   |
--! | MGT_23 |    115    |  E86   |   ECal   |
--! | MGT_24 |    116    |  E83   |   ECal   |
--! | MGT_25 |    116    |  E85   |   ECal   |
--! | MGT_26 |    116    |  E88   |   ECal   |
--! | MGT_27 |    116    |  E87   |   ECal   |
--! | MGT_28 |    117    |  E89   |   ECal   |
--! | MGT_29 |    117    |  E8A   |   ECal   |
--! | MGT_30 |    117    |  E98   |   ECal   |
--! | MGT_31 |    117    |  E96   |   ECal   |
--! | MGT_32 |    118    |  E92   |   ECal   |
--! | MGT_33 |    118    |  E94   |   ECal   |
--! | MGT_34 |    118    |  E9A   |   ECal   |
--! | MGT_35 |    118    |  E97   |   ECal   |
--! | MGT_36 |    119    |  E99   |   ECal   |
--! | MGT_37 |    119    |  E91   |   ECal   |
--! | MGT_38 |    119    |  E95   |   ECal   |
--! | MGT_39 |    119    |  E93   |   ECal   |
--! | MGT_40 |    213    | SP E40 |   ECal   |
--! | MGT_41 |    213    | SP E4B |   ECal   |
--! | MGT_42 |    213    | SP E50 |   ECal   |
--! | MGT_43 |    213    | SP E5B |   ECal   |
--! | MGT_44 |    214    |  H16   |   HCal   |
--! | MGT_45 |    214    |  H17   |   HCal   |
--! | MGT_46 |    214    |  H18   |   HCal   |
--! | MGT_47 |    214    |  H19   |   HCal-  |
--! | MGT_48 |    215    |  H1A   |   HCal-  |
--! | MGT_49 |    215    |  H1B   |   HCal-  |
--! | MGT_50 |    215    |  SPARE |          |
--! | MGT_51 |    215    |  SPARE |          |
--! | MGT_52 |    217    |  H26   |   HCal   |
--! | MGT_53 |    217    |  H22   |   HCal   |
--! | MGT_54 |    217    |  H24   |   HCal-  |
--! | MGT_55 |    217    |  H28   |   HCal   |
--! | MGT_56 |    218    |  H2A   |   HCal-  |
--! | MGT_57 |    218    |  H20   |   HCal   |
--! | MGT_58 |    218    |  H2B   |   HCal-  |
--! | MGT_59 |    218    |  H27   |   HCal   |
--! | MGT_60 |    219    |  H21   |   HCal   |
--! | MGT_61 |    219    |  H29   |   HCal-  |
--! | MGT_62 |    219    |  H23   |   HCal-  |
--! | MGT_63 |    219    |  H25   |   HCal-  |
--!
--! For FPGA3
--! | FW Ch  |  MGT Quad |MiniPOD |   Type   |
--! |--------|-----------|--------|----------|
--! | MGT_00 |    110    |  H06   |   HCal   |
--! | MGT_01 |    110    |  H0B   |   HCal-  |
--! | MGT_02 |    110    |  E30   |   SPARE- |
--! | MGT_03 |    110    |  E3B   |   SPARE- |
--! | MGT_04 |    111    |  H08   |   HCal   |
--! | MGT_05 |    111    |  H0A   |   HCal-  |
--! | MGT_06 |    111    |  H09   |   HCal-  |
--! | MGT_07 |    111    |  H07   |   HCal   |
--! | MGT_08 |    115    |  E28   |   ECal   |
--! | MGT_09 |    115    |  E26   |   ECal   |
--! | MGT_10 |    115    |  E25   |   ECal   |
--! | MGT_11 |    115    |  E2A   |   ECal   |
--! | MGT_12 |    116    |  E23   |   ECal   |
--! | MGT_13 |    116    |  E22   |   ECal   |
--! | MGT_14 |    116    |  E24   |   ECal   |
--! | MGT_15 |    116    |  E21   |   ECal   |
--! | MGT_16 |    117    |  E29   |   ECal   |
--! | MGT_17 |    117    |  E27   |   ECal   |
--! | MGT_18 |    117    |  E32   |   ECal   |
--! | MGT_19 |    117    |  E31   |   ECal   |
--! | MGT_20 |    118    |  E37   |   ECal   |
--! | MGT_21 |    118    |  E3A   |   ECal   |
--! | MGT_22 |    118    |  E39   |   ECal   |
--! | MGT_23 |    118    |  E38   |   ECal   |
--! | MGT_24 |    119    |  E35   |   ECal   |
--! | MGT_25 |    119    |  E36   |   ECal   |
--! | MGT_26 |    119    |  E33   |   ECal   |
--! | MGT_27 |    119    |  E34   |   ECal   |
--! | MGT_28 |    210    |  H10   |   HCal   |
--! | MGT_29 |    210    |  H14   |   HCal-  |
--! | MGT_30 |    210    |  H19   |   HCal-  |
--! | MGT_31 |    210    |  H18   |   HCal   |
--! | MGT_32 |    211    |  H11   |   HCal   |
--! | MGT_33 |    211    |  H12   |   HCal   |
--! | MGT_34 |    211    |  H13   |   HCal-  |
--! | MGT_35 |    211    |  H15   |   HCal-  |
--! | MGT_36 |    212    |  H16   |   HCal   |
--! | MGT_37 |    212    |  H17   |   HCal   |
--! | MGT_38 |    212    |  H1A   |   HCal-  |
--! | MGT_39 |    212    |  H1B   |   HCal-  |
--! | MGT_40 |    214    |  E6B   |  SPARE-  |
--! | MGT_41 |    214    |  E60   |  SPARE-  |
--! | MGT_42 |    214    |  NC    |    -     |
--! | MGT_43 |    214    |  NC    |    -     |
--! | MGT_44 |    215    |  E55   |   ECal   |
--! | MGT_45 |    215    |  E52   |   ECal   |
--! | MGT_46 |    215    |  E53   |   ECal   |
--! | MGT_47 |    215    |  E57   |   ECal   |
--! | MGT_48 |    216    |  E51   |   ECal   |
--! | MGT_49 |    216    |  E56   |   ECal   |
--! | MGT_50 |    216    |  E54   |   ECal   |
--! | MGT_51 |    216    |  E58   |   ECal   |
--! | MGT_52 |    217    |  E59   |   ECal   |
--! | MGT_53 |    217    |  E45   |   ECal   |
--! | MGT_54 |    217    |  E5A   |   ECal   |
--! | MGT_55 |    217    |  E41   |   ECal   |
--! | MGT_56 |    218    |  E47   |   ECal   |
--! | MGT_57 |    218    |  E4A   |   ECal   |
--! | MGT_58 |    218    |  E43   |   ECal   |
--! | MGT_59 |    218    |  E42   |   ECal   |
--! | MGT_60 |    219    |  E49   |   ECal   |
--! | MGT_61 |    219    |  E44   |   ECal   |
--! | MGT_62 |    219    |  E48   |   ECal   |
--! | MGT_63 |    219    |  E46   |   ECal   |
--!
--! For FPGA4
--! | FW Ch  |  MGT Quad |MiniPOD |   Type   |
--! |--------|-----------|--------|----------|
--! | MGT_00 |    110    |  E6A   |   ECal   |
--! | MGT_01 |    110    |  E68   |   ECal   |
--! | MGT_02 |    110    |  E64   |   ECal   |
--! | MGT_03 |    110    |  E66   |   ECal   |
--! | MGT_04 |    111    |  E62   |   ECal   |
--! | MGT_05 |    111    |  E61   |   ECal   |
--! | MGT_06 |    111    |  E65   |   ECal   |
--! | MGT_07 |    111    |  E63   |   ECal   |
--! | MGT_08 |    115    |  E69   |   ECal   |
--! | MGT_09 |    115    |  E67   |   ECal   |
--! | MGT_10 |    115    |  E7A   |   ECal   |
--! | MGT_11 |    115    |  E78   |   ECal   |
--! | MGT_12 |    116    |  E74   |   ECal   |
--! | MGT_13 |    116    |  E76   |   ECal   |
--! | MGT_14 |    116    |  E72   |   ECal   |
--! | MGT_15 |    116    |  E71   |   ECal   |
--! | MGT_16 |    117    |  E73   |   ECal   |
--! | MGT_17 |    117    |  E75   |   ECal   |
--! | MGT_18 |    117    |  E79   |   ECal   |
--! | MGT_19 |    117    |  E77   |   ECal   |
--! | MGT_20 |    119    |  H20   |   HCal   |
--! | MGT_21 |    119    |  H21   |   HCal   |
--! | MGT_22 |    119    |  H22   |   HCal   |
--! | MGT_23 |    119    |  H23   |   HCal-  |
--! | MGT_24 |    210    |  E48   |   ECal   |
--! | MGT_25 |    210    |  E46   |   ECal   |
--! | MGT_26 |    210    |  E44   |   ECal   |
--! | MGT_27 |    210    |  E49   |   ECal   |
--! | MGT_28 |    211    |  E42   |   ECal   |
--! | MGT_29 |    211    |  E43   |   ECal   |
--! | MGT_30 |    211    |  E47   |   ECal   |
--! | MGT_31 |    211    |  E4A   |   ECal   |
--! | MGT_32 |    212    |  E41   |   ECal   |
--! | MGT_33 |    212    |  E45   |   ECal   |
--! | MGT_34 |    212    |  E56   |   ECal   |
--! | MGT_35 |    212    |  E54   |   ECal   |
--! | MGT_36 |    213    |  E58   |   ECal   |
--! | MGT_37 |    213    |  E5A   |   ECal   |
--! | MGT_38 |    213    |  E59   |   ECal   |
--! | MGT_39 |    213    |  E51   |   ECal   |
--! | MGT_40 |    214    |  E52   |   ECal   |
--! | MGT_41 |    214    |  E53   |   ECal   |
--! | MGT_42 |    214    |  E57   |   ECal   |
--! | MGT_43 |    214    |  E55   |   ECal   |
--! | MGT_44 |    215    |  E40 SP|   ECal-  |
--! | MGT_45 |    215    |  E4B SP|   ECal-  |
--! | MGT_46 |    215    |  E5B SP|   ECal-  |
--! | MGT_47 |    215    |  E50 SP|   ECal-  |
--! | MGT_48 |    216    |  H1A   |   HCal-  |
--! | MGT_49 |    216    |  H19   |   HCal-  |
--! | MGT_50 |    216    |  H17   |   HCal   |
--! | MGT_51 |    216    |  H16   |   HCal   |
--! | MGT_52 |    217    |  H14   |   HCal-  |
--! | MGT_53 |    217    |  H12   |   HCal   |
--! | MGT_54 |    217    |  H1B   |   HCal-  |
--! | MGT_55 |    217    |  H18   |   HCal   |
--! | MGT_56 |    218    |  H11   |   HCal   |
--! | MGT_57 |    218    |  H10   |   HCal   |
--! | MGT_58 |    218    |  H15   |   HCal-  |
--! | MGT_59 |    218    |  H13   |   HCal-  |
--! | MGT_60 |    219    |  H24   |   HCal-  |
--! | MGT_61 |    219    |  H25   |   HCal-  |
--! | MGT_62 |    219    |  E30 SP|   ECal-  |
--! | MGT_63 |    219    |  E3B SP|   ECal-  |

--! For Hadron mapping we assumed that the channel mapping within one fiber is
--! numbered as follows (eta Left to Right and Phi Bottom Upward):
--! |    |    |    |    |
--! |----|----|----|----|
--! | 03 | 07 | 11 | 15 |
--! | 02 | 06 | 10 | 14 |
--! | 01 | 05 | 09 | 13 |
--! | 00 | 04 | 08 | 12 |
--! Fibers are numberd as follows:
--!
--! FPGA1
--! |     |      |     |
--! |-----|------|-----|
--! | H05 | H0B  | H15 |
--! | H04 | H0A  | H14 |
--! | H03 | H09  | H13 |
--! | H02 | H08  | H12 |
--! | H01 | H07  | H11 |
--! | H00 | H06  | H10 |
--!
--! FPGA2
--! |     |      |     |
--! |-----|------|-----|
--! | H1B | H25  | H2B |
--! | H1A | H24  | H2A |
--! | H19 | H23  | H29 |
--! | H18 | H22  | H28 |
--! | H17 | H21  | H27 |
--! | H16 | H20  | H26 |
--!
--! FPGA3
--! |     |      |     |
--! |-----|------|-----|
--! | H0B | H15  | H1B |
--! | H0A | H14  | H1A |
--! | H09 | H13  | H19 |
--! | H08 | H12  | H18 |
--! | H07 | H11  | H17 |
--! | H06 | H10  | H16 |
--!
--! FPGA4
--! |     |      |     |
--! |-----|------|-----|
--! | H15 | H1B  | H25 |
--! | H14 | H1A  | H24 |
--! | H13 | H19  | H23 |
--! | H12 | H18  | H22 |
--! | H11 | H17  | H21 |
--! | H10 | H16  | H20 |
--!
--!
--! Only bottom 3 lines are used in phase 1.
--! @author Ian Brawn
--! @author Mohammed Siyad
--! @author Francesco Gonnella


library IEEE;
use IEEE.STD_LOGIC_1164.all;

use IEEE.NUMERIC_STD.all;
library algolib;
use algolib.AlgoDataTypes.all;
use algolib.DataTypes.all;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.all;
use TOB_rdout_lib.data_type_pkg.all;

--! @copydoc efex_data_formats.vhd
package EfexDataFormats is

  -- Define the number of fibres needed to cover the environment area processed by an eFEX FPGA,
  -- in eta and Phi...

  constant NUM_FIB_Em_ETA : integer := 4;  --! Width  (eta) of Proc FPGA Environment area in Em Fibres
  constant NUM_FIB_Em_PHI : integer := 10;  --! Height (phi) of Proc FPGA Environment area in Em Fibres

  constant NUM_FIB_Had_ETA : integer := 3;  --! Width  (eta) of Proc FPGA Environment area in Had Fibres
  constant NUM_FIB_Had_PHI : integer := 6;  --! Height (phi) of Proc FPGA Environment area in Had Fibres

  constant NUM_FIB_SPARE : integer := 8;  --! No. Spare Fibres

  constant N_MGT : integer := 64;

  type mgt_data_in is array (N_MGT-1 downto 0) of std_logic_vector(31 downto 0);
  type mgt_data_out is array (N_MGT-1 downto 0) of std_logic_vector(226 downto 0);

  -- define types to hold the fibre data

  --! This holds all Em data from a column of fibres in phi for 1 Proc FPGA:
  type FibreColumnEm is array(NUM_FIB_Em_PHI-1 downto 0) of std_logic_vector(226 downto 0);

  --! This holds all data from Em for 1 Proc FPGA:
  type FibreAllEm is array(NUM_FIB_Em_ETA-1 downto 0) of FibreColumnEm;

  --! This holds all Had data from a column of fibres in phi for 1 Proc FPGA:
  type FibreColumnHad is array(NUM_FIB_Had_PHI-1 downto 0) of std_logic_vector(226 downto 0);

  --! This holds all data from Had for 1 Proc FPGA:
  type FibreAllHad is array(NUM_FIB_Had_ETA-1 downto 0) of FibreColumnHad;

  --! This holds all data from the spare fibres:
  type FibreAllSpare is array(NUM_FIB_SPARE-1 downto 0) of std_logic_vector(226 downto 0);

  subtype SuperCell is std_logic_vector(9 downto 0);

  type SuperCellData is array(19 downto 0) of SuperCell;
  type SuperCells is record
    Data    : SuperCellData;
    CRC     : std_logic_vector(8 downto 0);
    BCID    : BCID_t;
    Quality : std_logic_vector(7 downto 0);
  end record;


  function AlgoTowerBuilder
    (
      EmDataIn     : FibreAllEm;
      HadDataIn    : FibreAllHad;
      SpareDataIn  : FibreAllSpare;
      eFEXPosition : std_logic_vector(31 downto 0)
      )
    return Algoinput;

  function MGT_to_SuperCells (MGT : std_logic_vector(226 downto 0)) return SuperCells;

  function to_raw_data(em: FibreAllEm; had: FibreAllHad) return RAW_data_227_type;

  procedure FibreArrayBuilder (
    -- Inputs:
    signal fpga_number : in  integer;
    signal MGT_DATA    : in  mgt_data_out;
    -- Outputs:
    signal EmData      : out FibreAllEm;
    signal HadData     : out FibreAllHad;
    signal SpareData   : out FibreAllSpare
    );


end EfexDataFormats;

--! @copydoc efex_data_formats.vhd
package body EfexDataFormats is

  --! @brief Data re-mapping  data between the MGT and supercell
  --! @details This function re-maps data between the MGT type and supercell type
  --! Note: based on Zhulanov's talk on 27.06.2019
  --! @author F. Gonnella
  function MGT_to_SuperCells (MGT : std_logic_vector(226 downto 0)) return SuperCells is
    variable SC : SuperCells;
  begin
    SC.Quality := MGT(7 downto 0);
    SC.CRC     := MGT(223 downto 215);
    SC.BCID    := MGT(9 downto 8) & MGT(31 downto 30) & MGT(214 downto 212);

    SC.Data(0) := MGT(19 downto 10);
    SC.Data(1) := MGT(29 downto 20);

    SC.Data(2) := MGT(41 downto 32);
    SC.Data(3) := MGT(51 downto 42);
    SC.Data(4) := MGT(61 downto 52);

    SC.Data(5) := MGT(73 downto 64);
    SC.Data(6) := MGT(83 downto 74);
    SC.Data(7) := MGT(93 downto 84);

    SC.Data(8)  := MGT(105 downto 96);
    SC.Data(9)  := MGT(115 downto 106);
    SC.Data(10) := MGT(125 downto 116);

    SC.Data(11) := MGT(137 downto 128);
    SC.Data(12) := MGT(147 downto 138);
    SC.Data(13) := MGT(157 downto 148);

    SC.Data(14) := MGT(169 downto 160);
    SC.Data(15) := MGT(179 downto 170);
    SC.Data(16) := MGT(189 downto 180);

    SC.Data(17) := MGT(201 downto 192);
    SC.Data(18) := MGT(211 downto 202);

    SC.Data(19) := MGT(63 downto 62)&MGT(95 downto 94)&MGT(127 downto 126)&MGT(159 downto 158)&MGT(191 downto 190);
    return SC;
  end;

  --! @brief Data re-mapping between SuperCell and algorithm input
  --! @details
  --! This function re-maps data between the SC type and algorithm input.
  --! The eFEXPoistion input will allow dynamic remapping to account for
  --! geographical eFEX position.
  --!
  --! Position register bits:
  --! - 0 is eFEX on eta edge
  --! - 1 is eFEX on left eta edge
  --! - 2 Eta orientation for left column (1 = ->, 0 = <-)
  --! - 3 Eta orientation for central columns (1 = ->, 0 = <-)
  --! - 4 Eta orientation for right column (1 = ->, 0 = <-)
  --!
  --! For the four eFEX FPGAs in module A (positive eta), B (centre), C
  --! (negative eta), the bit 2 (L), 3(C), and 4(R) configuration should be as
  --! follows:
  --!
  --!  | FPGA  | eFEX A | eFEX B | eFEX C |
  --!  |-------|--------|--------|--------|
  --!  |       | L C R  |  L C R | L C R  |
  --!  |   0   | 1 1 1  |  0 0 0 | 0 0 0  |
  --!  |   1   | 1 1 1  |  0 0 1 | 0 0 0  |
  --!  |   2   | 1 1 1  |  0 1 1 | 0 0 0  |
  --!  |   3   | 1 1 1  |  1 1 1 | 0 0 0  |
  --!
  --! @author I. Brawn
  --! @author F. Gonnella
  function AlgoTowerBuilder
    (
      EmDataIn     : FibreAllEm;
      HadDataIn    : FibreAllHad;
      SpareDataIn  : FibreAllSpare;
      eFEXPosition : std_logic_vector(31 downto 0)
      )
    return Algoinput is

    variable AlgoData                     : AlgoInput;
    variable Position_on_eta_edge         : std_logic;
    variable Position_on_eta_left_edge    : std_logic;
    variable Position_positive_eta_left   : std_logic;
    variable Position_positive_eta_right  : std_logic;
    variable Position_positive_eta_centre : std_logic;
    variable Position_hadronic            : std_logic_vector(2 downto 0);  --! 0 Centre, 1 Near negative eta, 2 Near positive Eta, 3 Far negative eta, 4 Far positive eta



  begin

    if eFEXPosition(EM_POSITION_ETA_ON_EDGE) = '1' then
      Position_on_eta_edge := '1';
      if eFEXPosition(EM_POSITION_ETA_ON_LEFT_EDGE) = '1' then
        Position_on_eta_left_edge := '1';
      else
        Position_on_eta_left_edge := '0';
      end if;
    else
      Position_on_eta_edge      := '0';
      Position_on_eta_left_edge := '0';
    end if;

    Position_positive_eta_left   := eFEXPosition(EM_POSITION_POSITIVE_ETA_LEFT);
    Position_positive_eta_centre := eFEXPosition(EM_POSITION_POSITIVE_ETA_CENTRE);
    Position_positive_eta_right  := eFEXPosition(EM_POSITION_POSITIVE_ETA_RIGHT);
    Position_hadronic            := eFEXPosition(HADRONIC_POSTION_BIT_END downto HADRONIC_POSTION_BIT_START);

    --ECal (2 TTs per MGT)
    for phi in 0 to NUM_FIB_Em_PHI-1 loop
      -- Eta = 0 or 5
      if Position_positive_eta_left = '1' then
        AlgoData(0)(phi).Layer0(0) := MGT_to_SuperCells(EmDataIn(0)(phi)).Data(14);
        for i in 0 to 3 loop
          AlgoData(0)(phi).Layer1(i) := MGT_to_SuperCells(EmDataIn(0)(phi)).Data(i + 15);
          AlgoData(0)(phi).Layer2(i) := MGT_to_SuperCells(EmDataIn(0)(phi)).Data(i + 4);
        end loop;
        AlgoData(0)(phi).Layer3(0) := MGT_to_SuperCells(EmDataIn(0)(phi)).Data(19);
      else
        AlgoData(0)(phi).Layer0(0) := MGT_to_SuperCells(EmDataIn(0)(phi)).Data(8);
        for i in 0 to 3 loop
          AlgoData(0)(phi).Layer1(i) := MGT_to_SuperCells(EmDataIn(0)(phi)).Data(i + 9);
          AlgoData(0)(phi).Layer2(i) := MGT_to_SuperCells(EmDataIn(0)(phi)).Data(i + 0);
        end loop;
        AlgoData(0)(phi).Layer3(0) := MGT_to_SuperCells(EmDataIn(0)(phi)).Data(13);
      end if;

      -- Eta = 1,2,3,4 or 4,3,2,1
      if Position_positive_eta_centre = '1' then
        AlgoData(1)(phi).Layer0(0) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(8);
        AlgoData(2)(phi).Layer0(0) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(14);
        AlgoData(3)(phi).Layer0(0) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(8);
        AlgoData(4)(phi).Layer0(0) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(14);

        for i in 0 to 3 loop
          AlgoData(1)(phi).Layer1(i) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(i + 9);
          AlgoData(1)(phi).Layer2(i) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(i + 0);
          AlgoData(2)(phi).Layer1(i) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(i + 15);
          AlgoData(2)(phi).Layer2(i) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(i + 4);
          AlgoData(3)(phi).Layer1(i) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(i + 9);
          AlgoData(3)(phi).Layer2(i) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(i + 0);
          AlgoData(4)(phi).Layer1(i) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(i + 15);
          AlgoData(4)(phi).Layer2(i) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(i + 4);
        end loop;
        AlgoData(1)(phi).Layer3(0) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(13);
        AlgoData(2)(phi).Layer3(0) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(19);
        AlgoData(3)(phi).Layer3(0) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(13);
        AlgoData(4)(phi).Layer3(0) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(19);

      else
        AlgoData(1)(phi).Layer0(0) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(14);
        AlgoData(2)(phi).Layer0(0) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(8);
        AlgoData(3)(phi).Layer0(0) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(14);
        AlgoData(4)(phi).Layer0(0) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(8);

        for i in 0 to 3 loop
          AlgoData(1)(phi).Layer1(i) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(i + 15);
          AlgoData(1)(phi).Layer2(i) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(i + 4);
          AlgoData(2)(phi).Layer1(i) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(i + 9);
          AlgoData(2)(phi).Layer2(i) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(i + 0);
          AlgoData(3)(phi).Layer1(i) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(i + 15);
          AlgoData(3)(phi).Layer2(i) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(i + 4);
          AlgoData(4)(phi).Layer1(i) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(i + 9);
          AlgoData(4)(phi).Layer2(i) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(i + 0);
        end loop;
        AlgoData(1)(phi).Layer3(0) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(19);
        AlgoData(2)(phi).Layer3(0) := MGT_to_SuperCells(EmDataIn(1)(phi)).Data(13);
        AlgoData(3)(phi).Layer3(0) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(19);
        AlgoData(4)(phi).Layer3(0) := MGT_to_SuperCells(EmDataIn(2)(phi)).Data(13);
      end if;

      -- Eta = 5 or 0
      if Position_positive_eta_right = '1' then
        AlgoData(5)(phi).Layer0(0) := MGT_to_SuperCells(EmDataIn(3)(phi)).Data(8);
        for i in 0 to 3 loop
          AlgoData(5)(phi).Layer1(i) := MGT_to_SuperCells(EmDataIn(3)(phi)).Data(i + 9);
          AlgoData(5)(phi).Layer2(i) := MGT_to_SuperCells(EmDataIn(3)(phi)).Data(i + 0);
        end loop;
        AlgoData(5)(phi).Layer3(0) := MGT_to_SuperCells(EmDataIn(3)(phi)).Data(13);
      else
        AlgoData(5)(phi).Layer0(0) := MGT_to_SuperCells(EmDataIn(3)(phi)).Data(14);
        for i in 0 to 3 loop
          AlgoData(5)(phi).Layer1(i) := MGT_to_SuperCells(EmDataIn(3)(phi)).Data(i + 15);
          AlgoData(5)(phi).Layer2(i) := MGT_to_SuperCells(EmDataIn(3)(phi)).Data(i + 4);
        end loop;
        AlgoData(5)(phi).Layer3(0) := MGT_to_SuperCells(EmDataIn(3)(phi)).Data(19);
      end if;
    end loop;

    --Hadron (16 TTs per MGT of which 9 are used)
    --Position_hadronic =  0 Centre, 1 Near negative eta, 2 Near positive Eta, 3 Far negative eta, 4 Far positive eta

    -- Hadronic centre
    if Position_hadronic = "000" then
      -- Eta = 0
      AlgoData(0)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(07);
      AlgoData(0)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(10);
      AlgoData(0)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(11);
      AlgoData(0)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(06);
      AlgoData(0)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(07);
      AlgoData(0)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(10);
      AlgoData(0)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(11);
      AlgoData(0)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(06);
      AlgoData(0)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(07);
      AlgoData(0)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(10);

      -- Eta = 1
      AlgoData(1)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(01);
      AlgoData(1)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(12);
      AlgoData(1)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(13);
      AlgoData(1)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(00);
      AlgoData(1)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(01);
      AlgoData(1)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(12);
      AlgoData(1)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(13);
      AlgoData(1)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(00);
      AlgoData(1)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(01);
      AlgoData(1)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(12);

      -- Eta = 2
      AlgoData(2)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(03);
      AlgoData(2)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(14);
      AlgoData(2)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(15);
      AlgoData(2)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(02);
      AlgoData(2)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(03);
      AlgoData(2)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(14);
      AlgoData(2)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(15);
      AlgoData(2)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(02);
      AlgoData(2)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(03);
      AlgoData(2)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(14);

      -- Eta = 3
      AlgoData(3)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(05);
      AlgoData(3)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(08);
      AlgoData(3)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(09);
      AlgoData(3)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(04);
      AlgoData(3)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(05);
      AlgoData(3)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(08);
      AlgoData(3)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(09);
      AlgoData(3)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(04);
      AlgoData(3)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(05);
      AlgoData(3)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(08);

      -- Eta = 4
      AlgoData(4)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(07);
      AlgoData(4)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(10);
      AlgoData(4)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(11);
      AlgoData(4)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(06);
      AlgoData(4)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(07);
      AlgoData(4)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(10);
      AlgoData(4)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(11);
      AlgoData(4)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(06);
      AlgoData(4)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(07);
      AlgoData(4)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(10);

      -- Eta = 5
      AlgoData(5)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(01);
      AlgoData(5)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(12);
      AlgoData(5)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(13);
      AlgoData(5)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(00);
      AlgoData(5)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(01);
      AlgoData(5)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(12);
      AlgoData(5)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(13);
      AlgoData(5)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(00);
      AlgoData(5)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(01);
      AlgoData(5)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(12);
    end if;

    -- Hadronic near negative
    if Position_hadronic = "001" then
      -- Eta = 0
      AlgoData(0)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(05);
      AlgoData(0)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(09);
      AlgoData(0)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(13);
      AlgoData(0)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(01);
      AlgoData(0)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(05);
      AlgoData(0)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(09);
      AlgoData(0)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(13);
      AlgoData(0)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(01);
      AlgoData(0)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(05);
      AlgoData(0)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(09);

      -- Eta = 1
      AlgoData(1)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(04);
      AlgoData(1)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(08);
      AlgoData(1)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(12);
      AlgoData(1)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(00);
      AlgoData(1)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(04);
      AlgoData(1)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(08);
      AlgoData(1)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(12);
      AlgoData(1)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(00);
      AlgoData(1)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(04);
      AlgoData(1)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(08);

      -- Eta = 2
      AlgoData(2)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(03);
      AlgoData(2)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(14);
      AlgoData(2)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(15);
      AlgoData(2)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(02);
      AlgoData(2)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(03);
      AlgoData(2)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(14);
      AlgoData(2)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(15);
      AlgoData(2)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(02);
      AlgoData(2)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(03);
      AlgoData(2)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(14);

      -- Eta = 3
      AlgoData(3)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(05);
      AlgoData(3)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(08);
      AlgoData(3)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(09);
      AlgoData(3)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(04);
      AlgoData(3)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(05);
      AlgoData(3)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(08);
      AlgoData(3)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(09);
      AlgoData(3)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(04);
      AlgoData(3)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(05);
      AlgoData(3)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(08);

      -- Eta = 4
      AlgoData(4)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(07);
      AlgoData(4)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(10);
      AlgoData(4)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(11);
      AlgoData(4)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(06);
      AlgoData(4)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(07);
      AlgoData(4)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(10);
      AlgoData(4)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(11);
      AlgoData(4)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(06);
      AlgoData(4)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(07);
      AlgoData(4)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(10);

      -- Eta = 5
      AlgoData(5)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(01);
      AlgoData(5)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(12);
      AlgoData(5)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(13);
      AlgoData(5)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(00);
      AlgoData(5)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(01);
      AlgoData(5)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(12);
      AlgoData(5)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(13);
      AlgoData(5)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(00);
      AlgoData(5)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(01);
      AlgoData(5)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(12);
    end if;

    --Hadronic near positive
    if Position_hadronic = "010" then
      -- Eta = 0
      AlgoData(0)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(07);
      AlgoData(0)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(10);
      AlgoData(0)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(11);
      AlgoData(0)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(06);
      AlgoData(0)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(07);
      AlgoData(0)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(10);
      AlgoData(0)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(11);
      AlgoData(0)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(06);
      AlgoData(0)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(07);
      AlgoData(0)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(10);

      -- Eta = 1
      AlgoData(1)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(01);
      AlgoData(1)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(12);
      AlgoData(1)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(13);
      AlgoData(1)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(00);
      AlgoData(1)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(01);
      AlgoData(1)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(12);
      AlgoData(1)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(13);
      AlgoData(1)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(00);
      AlgoData(1)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(01);
      AlgoData(1)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(12);

      -- Eta = 2
      AlgoData(2)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(03);
      AlgoData(2)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(14);
      AlgoData(2)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(15);
      AlgoData(2)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(02);
      AlgoData(2)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(03);
      AlgoData(2)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(14);
      AlgoData(2)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(15);
      AlgoData(2)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(02);
      AlgoData(2)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(03);
      AlgoData(2)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(14);

      -- Eta = 3
      AlgoData(3)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(05);
      AlgoData(3)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(08);
      AlgoData(3)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(09);
      AlgoData(3)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(04);
      AlgoData(3)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(05);
      AlgoData(3)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(08);
      AlgoData(3)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(09);
      AlgoData(3)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(04);
      AlgoData(3)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(05);
      AlgoData(3)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(08);

      -- Eta = 4
      AlgoData(4)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(04);
      AlgoData(4)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(08);
      AlgoData(4)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(12);
      AlgoData(4)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(00);
      AlgoData(4)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(04);
      AlgoData(4)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(08);
      AlgoData(4)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(12);
      AlgoData(4)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(00);
      AlgoData(4)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(04);
      AlgoData(4)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(08);

      -- Eta = 5
      AlgoData(5)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(05);
      AlgoData(5)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(09);
      AlgoData(5)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(13);
      AlgoData(5)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(01);
      AlgoData(5)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(05);
      AlgoData(5)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(09);
      AlgoData(5)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(13);
      AlgoData(5)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(01);
      AlgoData(5)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(05);
      AlgoData(5)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(09);
    end if;

    -- Hadronic far negative
    if Position_hadronic = "011" then
      -- Eta = 0
      AlgoData(0)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(05);
      AlgoData(0)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(09);
      AlgoData(0)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(13);
      AlgoData(0)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(01);
      AlgoData(0)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(05);
      AlgoData(0)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(09);
      AlgoData(0)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(13);
      AlgoData(0)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(01);
      AlgoData(0)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(05);
      AlgoData(0)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(09);

      -- Eta = 1
      AlgoData(1)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(04);
      AlgoData(1)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(08);
      AlgoData(1)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(0)).Data(12);
      AlgoData(1)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(00);
      AlgoData(1)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(04);
      AlgoData(1)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(08);
      AlgoData(1)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(1)).Data(12);
      AlgoData(1)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(00);
      AlgoData(1)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(04);
      AlgoData(1)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(0)(2)).Data(08);

      -- Eta = 2
      AlgoData(2)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(07);
      AlgoData(2)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(11);
      AlgoData(2)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(15);
      AlgoData(2)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(03);
      AlgoData(2)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(07);
      AlgoData(2)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(11);
      AlgoData(2)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(15);
      AlgoData(2)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(03);
      AlgoData(2)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(07);
      AlgoData(2)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(11);

      -- Eta = 3
      AlgoData(3)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(06);
      AlgoData(3)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(10);
      AlgoData(3)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(14);
      AlgoData(3)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(02);
      AlgoData(3)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(06);
      AlgoData(3)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(10);
      AlgoData(3)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(14);
      AlgoData(3)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(02);
      AlgoData(3)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(06);
      AlgoData(3)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(10);

      -- Eta = 4
      AlgoData(4)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(05);
      AlgoData(4)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(09);
      AlgoData(4)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(13);
      AlgoData(4)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(01);
      AlgoData(4)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(05);
      AlgoData(4)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(09);
      AlgoData(4)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(13);
      AlgoData(4)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(01);
      AlgoData(4)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(05);
      AlgoData(4)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(09);

      -- Eta = 5
      AlgoData(5)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(04);
      AlgoData(5)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(08);
      AlgoData(5)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(12);
      AlgoData(5)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(00);
      AlgoData(5)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(04);
      AlgoData(5)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(08);
      AlgoData(5)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(12);
      AlgoData(5)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(00);
      AlgoData(5)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(04);
      AlgoData(5)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(08);
    end if;

    -- Hadronic far positive
    if Position_hadronic = "100" then
      -- Eta = 0
      AlgoData(0)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(04);
      AlgoData(0)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(08);
      AlgoData(0)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(12);
      AlgoData(0)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(00);
      AlgoData(0)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(04);
      AlgoData(0)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(08);
      AlgoData(0)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(12);
      AlgoData(0)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(00);
      AlgoData(0)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(04);
      AlgoData(0)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(08);

      -- Eta = 1
      AlgoData(1)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(05);
      AlgoData(1)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(09);
      AlgoData(1)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(13);
      AlgoData(1)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(01);
      AlgoData(1)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(05);
      AlgoData(1)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(09);
      AlgoData(1)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(13);
      AlgoData(1)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(01);
      AlgoData(1)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(05);
      AlgoData(1)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(09);

      -- Eta = 2
      AlgoData(2)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(06);
      AlgoData(2)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(10);
      AlgoData(2)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(14);
      AlgoData(2)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(02);
      AlgoData(2)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(06);
      AlgoData(2)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(10);
      AlgoData(2)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(14);
      AlgoData(2)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(02);
      AlgoData(2)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(06);
      AlgoData(2)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(10);

      -- Eta = 3
      AlgoData(3)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(07);
      AlgoData(3)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(11);
      AlgoData(3)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(0)).Data(15);
      AlgoData(3)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(03);
      AlgoData(3)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(07);
      AlgoData(3)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(11);
      AlgoData(3)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(1)).Data(15);
      AlgoData(3)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(03);
      AlgoData(3)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(07);
      AlgoData(3)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(1)(2)).Data(11);

      -- Eta = 4
      AlgoData(4)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(04);
      AlgoData(4)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(08);
      AlgoData(4)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(12);
      AlgoData(4)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(00);
      AlgoData(4)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(04);
      AlgoData(4)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(08);
      AlgoData(4)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(12);
      AlgoData(4)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(00);
      AlgoData(4)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(04);
      AlgoData(4)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(08);

      -- Eta = 5
      AlgoData(5)(0).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(05);
      AlgoData(5)(1).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(09);
      AlgoData(5)(2).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(0)).Data(13);
      AlgoData(5)(3).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(01);
      AlgoData(5)(4).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(05);
      AlgoData(5)(5).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(09);
      AlgoData(5)(6).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(1)).Data(13);
      AlgoData(5)(7).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(01);
      AlgoData(5)(8).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(05);
      AlgoData(5)(9).Hadron(0) := MGT_to_SuperCells(HadDataIn(2)(2)).Data(09);
    end if;

    return AlgoData;
  end AlgoTowerBuilder;


  --! @brief   Map the received MGT data onto the FibreAll*** types.
  --! @details This procedure maps the received MGT data onto the FibreAll*** types.
  --! It is the only place in the logic that knows how this mapping works.
  --! @author M. Syiad
  procedure FibreArrayBuilder
    (
      signal fpga_number : in  integer;
      -- Inputs:
      signal MGT_DATA    : in  mgt_data_out;
      -- Outputs:
      signal EmData      : out FibreAllEm;
      signal HadData     : out FibreAllHad;
      signal SpareData   : out FibreAllSpare
      ) is
    ----------

    variable EmData_int    : FibreAllEm;
    variable HadData_int   : FibreAllHad;
    variable SpareData_int : FibreAllSpare;
  begin

    if fpga_number = 1 then
      EmData_int := (
        (MGT_DATA(34), MGT_DATA(32), MGT_DATA(36), MGT_DATA(30), MGT_DATA(37), MGT_DATA(24), MGT_DATA(39), MGT_DATA(22), MGT_DATA(26), MGT_DATA(28)),
        (MGT_DATA(31), MGT_DATA(27), MGT_DATA(38), MGT_DATA(29), MGT_DATA(35), MGT_DATA(23), MGT_DATA(33), MGT_DATA(25), MGT_DATA(20), MGT_DATA(21)),
        (MGT_DATA(14), MGT_DATA(13), MGT_DATA(15), MGT_DATA(11), MGT_DATA(17), MGT_DATA(12), MGT_DATA(19), MGT_DATA(10), MGT_DATA(18), MGT_DATA(16)),
        (MGT_DATA(05), MGT_DATA(01), MGT_DATA(06), MGT_DATA(02), MGT_DATA(08), MGT_DATA(04), MGT_DATA(07), MGT_DATA(00), MGT_DATA(09), MGT_DATA(03))
        );
      HadData_int := (
        (MGT_DATA(55), MGT_DATA(56), MGT_DATA(54), MGT_DATA(59), MGT_DATA(57), MGT_DATA(58)),
        (MGT_DATA(42), MGT_DATA(43), MGT_DATA(40), MGT_DATA(48), MGT_DATA(41), MGT_DATA(51)),
        (MGT_DATA(44), MGT_DATA(46), MGT_DATA(45), MGT_DATA(50), MGT_DATA(47), MGT_DATA(49))
        );
      SpareData_int := (others => (others => '0'));
        --MGT_DATA(52), MGT_DATA(53), MGT_DATA(60), MGT_DATA(61), MGT_DATA(62), MGT_DATA(63), (others => '0'), (others => '0')
        --);

    elsif fpga_number = 2 then
      EmData_int := (
        (MGT_DATA(34), MGT_DATA(36), MGT_DATA(30), MGT_DATA(35), MGT_DATA(31), MGT_DATA(38), MGT_DATA(33), MGT_DATA(39), MGT_DATA(32), MGT_DATA(37)),
        (MGT_DATA(29), MGT_DATA(28), MGT_DATA(26), MGT_DATA(27), MGT_DATA(23), MGT_DATA(25), MGT_DATA(21), MGT_DATA(24), MGT_DATA(20), MGT_DATA(22)),
        (MGT_DATA(10), MGT_DATA(11), MGT_DATA(19), MGT_DATA(12), MGT_DATA(16), MGT_DATA(13), MGT_DATA(18), MGT_DATA(15), MGT_DATA(17), MGT_DATA(14)),
        (MGT_DATA(00), MGT_DATA(09), MGT_DATA(01), MGT_DATA(07), MGT_DATA(05), MGT_DATA(08), MGT_DATA(03), MGT_DATA(04), MGT_DATA(02), MGT_DATA(06))
        );
      HadData_int := (
        (MGT_DATA(58), MGT_DATA(56), MGT_DATA(61), MGT_DATA(55), MGT_DATA(59), MGT_DATA(52)),
        (MGT_DATA(63), MGT_DATA(54), MGT_DATA(62), MGT_DATA(53), MGT_DATA(60), MGT_DATA(57)),
        (MGT_DATA(49), MGT_DATA(48), MGT_DATA(47), MGT_DATA(46), MGT_DATA(45), MGT_DATA(44))
        );
      SpareData_int := (others => (others => '0'));
        --MGT_DATA(63), MGT_DATA(58), MGT_DATA(51), MGT_DATA(50), MGT_DATA(61), MGT_DATA(56), (others => '0'), (others => '0')
        --);

    elsif fpga_number = 3 then
      EmData_int := (
        (MGT_DATA(54), MGT_DATA(52), MGT_DATA(51), MGT_DATA(47), MGT_DATA(49), MGT_DATA(44), MGT_DATA(50), MGT_DATA(46), MGT_DATA(45), MGT_DATA(48)),
        (MGT_DATA(57), MGT_DATA(60), MGT_DATA(62), MGT_DATA(56), MGT_DATA(63), MGT_DATA(53), MGT_DATA(61), MGT_DATA(58), MGT_DATA(59), MGT_DATA(55)),
        (MGT_DATA(21), MGT_DATA(22), MGT_DATA(23), MGT_DATA(20), MGT_DATA(25), MGT_DATA(24), MGT_DATA(27), MGT_DATA(26), MGT_DATA(18), MGT_DATA(19)),
        (MGT_DATA(11), MGT_DATA(16), MGT_DATA(08), MGT_DATA(17), MGT_DATA(09), MGT_DATA(10), MGT_DATA(14), MGT_DATA(12), MGT_DATA(13), MGT_DATA(15))
        );
      HadData_int := (
        (MGT_DATA(39), MGT_DATA(38), MGT_DATA(30), MGT_DATA(31), MGT_DATA(37), MGT_DATA(36)),
        (MGT_DATA(35), MGT_DATA(29), MGT_DATA(34), MGT_DATA(33), MGT_DATA(32), MGT_DATA(28)),
        (MGT_DATA(01), MGT_DATA(05), MGT_DATA(06), MGT_DATA(04), MGT_DATA(07), MGT_DATA(00))
        );
      SpareData_int := (others => (others => '0'));
        --MGT_DATA(02), MGT_DATA(03), MGT_DATA(40), MGT_DATA(41), MGT_DATA(42), MGT_DATA(43), (others => '0'), (others => '0')
        --);
    
    elsif fpga_number = 4 then
      EmData_int := (
        (MGT_DATA(10), MGT_DATA(18), MGT_DATA(11), MGT_DATA(19), MGT_DATA(13), MGT_DATA(17), MGT_DATA(12), MGT_DATA(16), MGT_DATA(14), MGT_DATA(15)),
        (MGT_DATA(00), MGT_DATA(08), MGT_DATA(01), MGT_DATA(09), MGT_DATA(03), MGT_DATA(06), MGT_DATA(02), MGT_DATA(07), MGT_DATA(04), MGT_DATA(05)),
        (MGT_DATA(37), MGT_DATA(38), MGT_DATA(36), MGT_DATA(42), MGT_DATA(34), MGT_DATA(43), MGT_DATA(35), MGT_DATA(41), MGT_DATA(40), MGT_DATA(39)),
        (MGT_DATA(31), MGT_DATA(27), MGT_DATA(24), MGT_DATA(30), MGT_DATA(25), MGT_DATA(33), MGT_DATA(26), MGT_DATA(29), MGT_DATA(28), MGT_DATA(32))
        );
      HadData_int := (
        (MGT_DATA(61), MGT_DATA(60), MGT_DATA(23), MGT_DATA(22), MGT_DATA(21), MGT_DATA(20)),
        (MGT_DATA(54), MGT_DATA(48), MGT_DATA(49), MGT_DATA(55), MGT_DATA(50), MGT_DATA(51)),
        (MGT_DATA(58), MGT_DATA(52), MGT_DATA(59), MGT_DATA(53), MGT_DATA(56), MGT_DATA(57))
        );
      SpareData_int := (others => (others => '0'));
      --MGT_DATA(63), MGT_DATA(62), MGT_DATA(47), MGT_DATA(46), MGT_DATA(45), MGT_DATA(44), MGT_DATA(21), MGT_DATA(20)
      --);

    end if;

    EmData    <= EmData_int;
    HadData   <= HadData_int;
    SpareData <= SpareData_int;
  end FibreArrayBuilder;

function to_raw_data(em: FibreAllEm; had: FibreAllHad) return RAW_data_227_type is
  variable ret : RAW_data_227_type;
begin
  for eta in 0 to 3 loop
    for phi in 0 to 9 loop
      ret(eta*10+phi) := em(eta)(phi);
    end loop;
  end loop;

  for eta in 0 to 2 loop
    for phi in 0 to 2 loop
      ret(40+eta*3+phi) := had(eta)(phi);
    end loop;
  end loop;

  return ret;

end function;

end EfexDataFormats;
