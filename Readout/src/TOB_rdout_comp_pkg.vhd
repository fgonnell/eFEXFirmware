----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/02/2017 08:41:03 AM
-- Design Name: 
-- Module Name: TOB_readout_pkg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--library TOB_rdout_lib;
--use TOB_rdout_lib.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

package TOB_rdout_comp_pkg is

COMPONENT cntr_ld_9b is
--    generic (
--    T_count       : in STD_LOGIC_VECTOR (8 downto 0);
--    ld_count      : in STD_LOGIC_VECTOR (8 downto 0)
--    );

    Port ( 
       CE   : in STD_LOGIC;
       CLK  : in STD_LOGIC;
       RST  : in STD_LOGIC;
       LD   : in STD_LOGIC;
       LD_VAL : in STD_LOGIC_VECTOR (8 downto 0);
--       TC_VAL : in STD_LOGIC_VECTOR (8 downto 0);
       Q      : out STD_LOGIC_VECTOR (8 downto 0)
       );
END COMPONENT;

COMPONENT cntr_ld_12b is
    generic (
        T_count       : in STD_LOGIC_VECTOR (11 downto 0);
        ld_count      : in STD_LOGIC_VECTOR (11 downto 0)
        );

    Port ( 
       CE   : in STD_LOGIC;
       CLK  : in STD_LOGIC;
       RST  : in STD_LOGIC;
       LD   : in STD_LOGIC;
       Q    : out STD_LOGIC_VECTOR (11 downto 0)
       );
END COMPONENT;

COMPONENT SIPO_unit is
    Port ( data_in : in STD_LOGIC_VECTOR (255 downto 0);
           data_sync_in : in STD_LOGIC;
           clk_in_200M : in STD_LOGIC;
           clk_in_40M : in STD_LOGIC;
           data_sync_out : out STD_LOGIC;
           data_out : out STD_LOGIC_VECTOR (1279 downto 0)
           );
END COMPONENT;

COMPONENT sm_TOB_wr_to_FIFO is
    Port ( 
           CLK_200M              : in STD_LOGIC;
           CLK_40M               : in STD_LOGIC;
           RST                   : in STD_LOGIC;
           L1A_in                : in STD_LOGIC;
           SIPO_sync_in          : in STD_LOGIC;
           pre_ld_wr_adrr        : in STD_LOGIC_VECTOR (8 downto 0);
           DPR_locations_to_rd   : in STD_LOGIC_VECTOR (1 downto 0); -- no. of locations read from DPR
           DPR_rd_addr           : out STD_LOGIC_VECTOR (8 downto 0);
           DPR_wr_addr           : out STD_LOGIC_VECTOR (8 downto 0);
           DRP_rd_en             : out STD_LOGIC ;
           FIFO_wr_en            : out STD_LOGIC       -- en wr into FIFO
           );
END COMPONENT;



end TOB_rdout_comp_pkg;

package body TOB_rdout_comp_pkg is



end TOB_rdout_comp_pkg;
