--! @file
--! @brief Top of TOB sorting module
--! @details 
--! This module is a 3-stage TOB sorter. The input are 8 32-bit TOBs, the
--! output is on 32-bit TOB. The clock frequency is 280 MHz.
--! The OUT_Start signal marks the first of the 7 clock cycles on which the
--! first output TOB is sent out (if any). The TOBs are valid only if the OUT_Write
--! signal is high.
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
use work.DataTypes.all;
use work.AlgoDataTypes.all;

--! @copydoc TopSortingModule.vhd
entity TopSortingModule is
  generic (
    STAGE  : integer := 3;
    N_TOBS : integer := 5
    );
  port (CLK : in std_logic;

        IN_Control : in  AlgoRegister;
        OUT_Status : out AlgoRegister;

        IN_Start : in std_logic;
        IN_Data  : in AlgoTriggerObjects((2**STAGE)-1 downto 0);

        OUT_Start : out std_logic;
        OUT_Write : out std_logic;
        OUT_Data  : out AlgoTriggerObject
        );

end TopSortingModule;
--! @copydoc TopSortingModule.vhd
architecture Behavioral of TopSortingModule is

  signal connector : AlgoTriggerObjects((2**(stage+1))-2 downto 0);
  signal start     : std_logic_vector((2**(stage+1))-2 downto 0);
  signal writ      : std_logic_vector((2**(stage+1))-2 downto 0);

begin
  OUT_Status <= x"a0a0a0a0";

  stage_gen : for s in 0 to (stage - 1) generate

  begin
    ifFirst : if s = (stage -1) generate
    begin
      sorter_gen0 : for i in 0 to ((2**s) - 1) generate
      begin
        PAR_SORTER : entity work.ParallelSorter
          generic map (
            USE_EXTERNAL_WRITE => false,
            N_TOBS_IN          => N_TOBS,
            N_TOBS_OUT         => 7)
          port map (
            clk      => CLK,
            IN_Start => start(2*(2**s + i) downto 2*(2**s + i) - 1),
            IN_Write => writ(2*(2**s + i) downto 2*(2**s + i) - 1),
            IN_Data  => connector(2*(2**s + i) downto 2*(2**s + i) - 1),

            OUT_Start => start((2**s) - 1 + i),
            OUT_Write => writ((2**s) - 1 + i),
            OUT_Data  => connector((2**s) - 1 + i)
            );
      end generate sorter_gen0;
    end generate ifFirst;

    ifAll : if s /= (stage -1) generate
      sorter_gen : for i in 0 to ((2**s) - 1) generate
      begin
        PAR_SORTER : entity work.ParallelSorter
          generic map (
            USE_EXTERNAL_WRITE => true,
            N_TOBS_IN          => 7,
            N_TOBS_OUT         => 7)
          port map (
            clk      => CLK,
            IN_Start => start(2*(2**s + i) downto 2*(2**s + i) - 1),
            IN_Write => writ(2*(2**s + i) downto 2*(2**s + i) - 1),
            IN_Data  => connector(2*(2**s + i) downto 2*(2**s + i) - 1),

            OUT_Start => start((2**s) - 1 + i),
            OUT_Write => writ((2**s) - 1 + i),
            OUT_Data  => connector((2**s) - 1 + i)
            );
      end generate sorter_gen;
    end generate ifAll;

  end generate stage_gen;


  writ((2**(stage+1) - 2) downto (2**stage - 1)) <= (others => '0');

  process(CLK)
  begin
    if rising_edge(CLK) then
      connector((2**(stage+1) - 2) downto (2**stage - 1)) <= IN_Data;
      if IN_Start = '1' then
        start((2**(stage+1) - 2) downto (2**stage - 1)) <= (others => '1');
      else
        start((2**(stage+1) - 2) downto (2**stage - 1)) <= (others => '0');
      end if;
    end if;
  end process;

  process(CLK)
  begin
    if rising_edge(CLK) then
      OUT_Data  <= connector(0);
      OUT_Write <= writ(0);
      OUT_Start <= start(0);
    end if;
  end process;

end Behavioral;
