-------------------------------------------------------
--! @file
--! @brief External data-types and functions
--! @details
--! This file/package contains all types, constants, and functions used to
--! interface from/to the Algorithm and Sorting module.
--!
--! Internal data types and functions are defined in DataTypes.vhd
--!
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

--! @copydoc AlgoDataTypes.vhd
package AlgoDataTypes is

-----------------------------------------------------------------------------
-- eFEX data Parameter
-----------------------------------------------------------------------------

  constant INPUT_DATA_WIDTH : integer := 10;

  constant INPUT_COLUMNS : integer := 6;
  constant INPUT_ROWS    : integer := 10;
  constant INPUT_TOWERS  : integer := INPUT_ROWS*INPUT_COLUMNS;

-------------------------------------------------------------------------------
-- Trigger Tower Parameters
-------------------------------------------------------------------------------

  constant LAYER0 : integer := 1;
  constant LAYER1 : integer := 4;
  constant LAYER2 : integer := 4;
  constant LAYER3 : integer := 1;
  constant HADRON : integer := 1;

  constant LAYERS : integer := LAYER0 + LAYER1 + LAYER2 + LAYER3 + HADRON;

-------------------------------------------------------------------------------
-- Trigger Object Parameters
-------------------------------------------------------------------------------

  constant OUT_XTOB_WIDTH : integer := 64;
  constant OUT_TOB_WIDTH  : integer := 32;
  constant OUTPUT_TOBS    : integer := 8;

-------------------------------------------------------------------------------
-- Useful subtypes, types, constants
-------------------------------------------------------------------------------

  subtype BCID_t is std_logic_vector(6 downto 0);
  type BCID_array is array(59 downto 0) of BCID_t;

  --!External (10 bit) non-linearly encoded data word
  subtype AlgoWord is std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);

  --!External layer 0 array: dimension 1  
  type AlgoLayer0 is array(LAYER0-1 downto 0) of AlgoWord;
  --!External layer 1 array: dimension 4  
  type AlgoLayer1 is array(LAYER1-1 downto 0) of AlgoWord;
  --!External layer 2 array: dimension 4  
  type AlgoLayer2 is array(LAYER2-1 downto 0) of AlgoWord;
  --!External layer 3 array: dimension 1  
  type AlgoLayer3 is array(LAYER3-1 downto 0) of AlgoWord;
  --!External hadronic layer array: dimension 1  
  type AlgoHadron is array(HADRON-1 downto 0) of AlgoWord;

  constant ZERO_ALGO_WORD : AlgoWord := (others => '0');

  --!@brief Data-structure containing energy samples for the 4+hadronic layers
  --!@details
  --!Every data sample is made of 10 bit words: AlgoWord.
  --!
  --! |  Name  | number of words | word width | total bits |
  --! |--------|-----------------|------------|------------|
  --! | layer0 |        1        |     10     |    10      |
  --! | layer1 |        4        |     10     |    40      |
  --! | layer2 |        4        |     10     |    40      |
  --! | layer3 |        1        |     10     |    10      |
  --! |hadronic|        1        |     10     |    10      |  
  type AlgoTower is record
    Layer0 : AlgoLayer0;
    Layer1 : AlgoLayer1;
    Layer2 : AlgoLayer2;
    Layer3 : AlgoLayer3;
    Hadron : AlgoHadron;
  end record;

--! An AlgoTower filled with zeroes  
  constant ZERO_ALGO_TOWER : AlgoTower := (Layer0 => ((others => (others => '0'))),
                                           Layer1 => ((others => (others => '0'))),
                                           Layer2 => ((others => (others => '0'))),
                                           Layer3 => ((others => (others => '0'))),
                                           Hadron => ((others => (others => '0'))));

--!  Algorithm Phi column
  type AlgoColumn is array(INPUT_ROWS-1 downto 0) of AlgoTower;

-------------------------------------------------------------------------------
--!  Algorithm 2D INPUT port
  type Algo2DInput is array(INPUT_COLUMNS-1 downto 0, INPUT_ROWS-1 downto 0) of AlgoTower;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--!  Algorithm INPUT port
  type AlgoInput is array(INPUT_COLUMNS-1 downto 0) of AlgoColumn;
-------------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- OUTPUT
-----------------------------------------------------------------------------
--! Algorithm Extended Trigger Object XTOB
  subtype AlgoXTriggerObject is std_logic_vector(OUT_XTOB_WIDTH-1 downto 0);
--! Algorithm Trigger Object TOB
  subtype AlgoTriggerObject is std_logic_vector(OUT_TOB_WIDTH-1 downto 0);
--! Algorithm OUTPUT port
  type AlgoTriggerObjects is array (natural range <>) of AlgoTriggerObject;
  type AlgoXTriggerObjects is array (natural range <>) of AlgoXTriggerObject;
-------------------------------------------------------------------------------
--! Algorithm OUTPUT port
  type AlgoOutput is array(OUTPUT_TOBS-1 downto 0) of AlgoTriggerObject;
--! Algorithm XOUTPUT port
  type AlgoXOutput is array(OUTPUT_TOBS-1 downto 0) of AlgoXTriggerObject;
-------------------------------------------------------------------------------

  constant ZERO_ALGO_TRIGGER_OBJECT  : AlgoTriggerObject  := (others => '0');
  constant ZERO_ALGO_XTRIGGER_OBJECT : AlgoXTriggerObject := (others => '0');

-------------------------------------------------------------------------------
-- Conversion Functions
-------------------------------------------------------------------------------

  function to_AlgoInput (X   : Algo2DInput) return AlgoInput;
  function to_Algo2DInput (Y : AlgoInput) return Algo2DInput;
  function to_LogicVector (Y : AlgoInput; Z : BCID_array) return std_logic_vector;
  function to_AlgoInput (X   : std_logic_vector) return AlgoInput;
  function to_BCID (X        : std_logic_vector) return BCID_array;

  function to_LogicVector (Y : AlgoOutput) return std_logic_vector;
  function to_AlgoOutput (X  : std_logic_vector) return AlgoOutput;
  function to_AlgoXOutput (X : std_logic_vector) return AlgoXOutput;

  function to_LogicVector (Y        : AlgoTriggerObject) return std_logic_vector;
  function to_LogicVector2 (Y       : AlgoXTriggerObject) return std_logic_vector;
  function to_AlgoTriggerObject (X  : std_logic_vector) return AlgoTriggerObject;
  function to_AlgoTriggerObjects (X : AlgoOutput) return AlgoTriggerObjects;

end package;

--! @copydoc AlgoDataTypes.vhd
package body AlgoDataTypes is

  --! Converts from 2d array to array of arrays
  function to_AlgoInput (X : Algo2DInput) return AlgoInput is
    variable Y : AlgoInput;
  begin
    for i in 0 to (INPUT_COLUMNS-1) loop
      for j in 0 to (INPUT_ROWS-1) loop
        Y(i)(j) := X(i, j);
      end loop;
    end loop;

    return Y;
  end;

  --! Converts from array of arrays to 2d array
  function to_Algo2DInput (Y : AlgoInput) return Algo2DInput is
    variable X : Algo2DInput;
  begin
    for i in 0 to (INPUT_COLUMNS-1) loop
      for j in 0 to (INPUT_ROWS-1) loop
        X(i, j) := Y(i)(j);
      end loop;
    end loop;

    return X;
  end;

--!@brief Converts AlgoInput format into a 128-bit std_logic vector with padding zeroes
--!@details
--! This function is used to write data into the ipbus_inputRAM. The AlgoInput
--! data structure contains 60 Trigger Towers (TT) organised in column 0-5 (Eta) and rows (Phi) 0-9.
--! They are sorted according to the following table:
--! 
--! |      | Eta0 | Eta1 | Eta2 | Eta3 | Eta4 | Eta5 |
--! |------|------|------|------|------|------|------|
--! | Phi9 |  09  |  19  |  29  |  39  |  49  |  59  | 
--! | Phi8 |  08  |  18  |  28  |  38  |  48  |  58  | 
--! | Phi7 |  07  |  17  |  27  |  37  |  47  |  57  | 
--! | Phi6 |  06  |  16  |  26  |  36  |  46  |  56  | 
--! | Phi5 |  05  |  15  |  25  |  35  |  45  |  55  | 
--! | Phi4 |  04  |  14  |  24  |  34  |  44  |  54  | 
--! | Phi3 |  03  |  13  |  23  |  33  |  43  |  53  | 
--! | Phi2 |  02  |  12  |  22  |  32  |  42  |  52  |
--! | Phi1 |  01  |  11  |  21  |  31  |  41  |  51  |
--! | Phi0 |  00  |  10  |  20  |  30  |  40  |  50  | 
--!
--! Every TT data structure is converted into a 128-bit std_logic vector with padding
--! zeroes as follows:
--! |0-9|10-19|20-29|30-31||32-41|42-51|52-61|62-63||64-73|74-83|84-93|94-95||96-105|106-115|  116-123    | 124-127|
--! |---|-----|-----|-----||-----|-----|-----|-----||-----|-----|-----|-----||------|-------|-------------|--------|
--! |L0 | L1.0|L1.1 |"00" ||L1.2 |L1.3 |L2.0 |"01" ||L2.1 |L2.2 |L2.3 |"10" ||  L3  |  HAD  | BCID & "0"  | "0011" |
  function to_LogicVector (Y : AlgoInput; Z : BCID_array) return std_logic_vector is
    variable X : std_logic_vector(128*INPUT_TOWERS-1 downto 0);
  begin
    for c in 0 to (INPUT_COLUMNS-1) loop
      for r in 0 to (INPUT_ROWS-1) loop
        X(127 + 128*(c*INPUT_ROWS + r) downto 128*(c*INPUT_ROWS + r)) := "1100" & "0" & Z(c*INPUT_ROWS + r) & Y(c)(r).Hadron(0) & Y(c)(r).Layer3(0) &
                                                                         "10" & Y(c)(r).Layer2(3) & Y(c)(r).Layer2(2) & Y(c)(r).Layer2(1) &
                                                                         "01" & Y(c)(r).Layer2(0) & Y(c)(r).Layer1(3) & Y(c)(r).Layer1(2) &
                                                                         "00" & Y(c)(r).Layer1(1) & Y(c)(r).Layer1(0) & Y(c)(r).Layer0(0);
      end loop;
    end loop;

    return X;
  end;

--!@brief Converts a 128-bit std_logic vector with padding 0 into AlgoInput format
--!@details
--! This function is used to read data out of the ipbus_inputRAM. This functions
--! ignores padding zeroes added by to_LogicVector() function.
  function to_AlgoInput (X : std_logic_vector) return AlgoInput is
    variable Y : AlgoInput;
  begin
    for c in 0 to (INPUT_COLUMNS-1) loop
      for r in 0 to (INPUT_ROWS-1) loop
        Y(c)(r).Layer0(0) := X(INPUT_DATA_WIDTH-1 + c*INPUT_ROWS*128+INPUT_DATA_WIDTH*00+r*128+0 downto c*INPUT_ROWS*128+INPUT_DATA_WIDTH*00+r*128+0);
        Y(c)(r).Layer1(0) := X(INPUT_DATA_WIDTH-1 + c*INPUT_ROWS*128+INPUT_DATA_WIDTH*01+r*128+0 downto c*INPUT_ROWS*128+INPUT_DATA_WIDTH*01+r*128+0);
        Y(c)(r).Layer1(1) := X(INPUT_DATA_WIDTH-1 + c*INPUT_ROWS*128+INPUT_DATA_WIDTH*02+r*128+0 downto c*INPUT_ROWS*128+INPUT_DATA_WIDTH*02+r*128+0);
        Y(c)(r).Layer1(2) := X(INPUT_DATA_WIDTH-1 + c*INPUT_ROWS*128+INPUT_DATA_WIDTH*03+r*128+2 downto c*INPUT_ROWS*128+INPUT_DATA_WIDTH*03+r*128+2);
        Y(c)(r).Layer1(3) := X(INPUT_DATA_WIDTH-1 + c*INPUT_ROWS*128+INPUT_DATA_WIDTH*04+r*128+2 downto c*INPUT_ROWS*128+INPUT_DATA_WIDTH*04+r*128+2);
        Y(c)(r).Layer2(0) := X(INPUT_DATA_WIDTH-1 + c*INPUT_ROWS*128+INPUT_DATA_WIDTH*05+r*128+2 downto c*INPUT_ROWS*128+INPUT_DATA_WIDTH*05+r*128+2);
        Y(c)(r).Layer2(1) := X(INPUT_DATA_WIDTH-1 + c*INPUT_ROWS*128+INPUT_DATA_WIDTH*06+r*128+4 downto c*INPUT_ROWS*128+INPUT_DATA_WIDTH*06+r*128+4);
        Y(c)(r).Layer2(2) := X(INPUT_DATA_WIDTH-1 + c*INPUT_ROWS*128+INPUT_DATA_WIDTH*07+r*128+4 downto c*INPUT_ROWS*128+INPUT_DATA_WIDTH*07+r*128+4);
        Y(c)(r).Layer2(3) := X(INPUT_DATA_WIDTH-1 + c*INPUT_ROWS*128+INPUT_DATA_WIDTH*08+r*128+4 downto c*INPUT_ROWS*128+INPUT_DATA_WIDTH* 8+r*128+4);
        Y(c)(r).Layer3(0) := X(INPUT_DATA_WIDTH-1 + c*INPUT_ROWS*128+INPUT_DATA_WIDTH*09+r*128+6 downto c*INPUT_ROWS*128+INPUT_DATA_WIDTH* 9+r*128+6);
        Y(c)(r).Hadron(0) := X(INPUT_DATA_WIDTH-1 + c*INPUT_ROWS*128+INPUT_DATA_WIDTH*10+r*128+6 downto c*INPUT_ROWS*128+INPUT_DATA_WIDTH*10+r*128+6);
      end loop;
    end loop;

    return Y;
  end;

  function to_BCID (X : std_logic_vector) return BCID_array is
    variable Y : BCID_array;
  begin
    for c in 0 to (INPUT_COLUMNS-1) loop
      for r in 0 to (INPUT_ROWS-1) loop
        Y(c*INPUT_ROWS + r) := X(BCID_t'high + c*INPUT_ROWS*128+INPUT_DATA_WIDTH*11+r*128+6 downto c*INPUT_ROWS*128+INPUT_DATA_WIDTH*11+r*128+6);
      end loop;
    end loop;
    return Y;
  end;


--! Converts from AlgoOutput (array of AlgoTowers) to std_logic_vector
  function to_LogicVector (Y : AlgoOutput) return std_logic_vector is
    variable X : std_logic_vector(OUTPUT_TOBS*OUT_TOB_WIDTH-1 downto 0);
  begin
    for i in 0 to (OUTPUT_TOBS-1) loop
      X(i*OUT_TOB_WIDTH + OUT_TOB_WIDTH-1 downto i*OUT_TOB_WIDTH) := Y(i);
    end loop;

    return X;
  end;

--! Converts from std_logic_vector to AlgoOutput (array of TOBs)
  function to_AlgoOutput (X : std_logic_vector) return AlgoOutput is
    variable Y : AlgoOutput;
  begin
    for i in 0 to (OUTPUT_TOBS-1) loop
      Y(i) := X(i*OUT_TOB_WIDTH + OUT_TOB_WIDTH-1 downto i*OUT_TOB_WIDTH);
    end loop;

    return Y;
  end;

--! Converts from std_logic_vector to XAlgoOutput (array of XTOBs)
  function to_AlgoXOutput (X : std_logic_vector) return AlgoXOutput is
    variable Y : AlgoXOutput;
  begin
    for i in 0 to (OUTPUT_TOBS-1) loop
      Y(i) := X(i*OUT_TOB_WIDTH + OUT_TOB_WIDTH-1 downto i*OUT_TOB_WIDTH);
    end loop;

    return Y;
  end;

--! Converts from AlgoTriggerObject (external TOB format) to std_logic_vector
  function to_LogicVector (Y : AlgoTriggerObject) return std_logic_vector is
    variable X : std_logic_vector(OUT_TOB_WIDTH-1 downto 0);
  begin
    for i in 0 to (OUT_TOB_WIDTH-1) loop
      X(i) := Y(i);
    end loop;

    return X;
  end;

--! Converts from AlgoXTriggerObject (external TOB format) to std_logic_vector
  function to_LogicVector2 (Y : AlgoXTriggerObject) return std_logic_vector is
    variable X : std_logic_vector(OUT_XTOB_WIDTH-1 downto 0);
  begin
    for i in 0 to (OUT_XTOB_WIDTH-1) loop
      X(i) := Y(i);
    end loop;

    return X;
  end;



--! Converts from std_logic_vector to AlgoTriggerObject (external TOB format)
  function to_AlgoTriggerObject (X : std_logic_vector) return AlgoTriggerObject is
    variable Y : AlgoTriggerObject;
  begin
    for i in 0 to (OUT_TOB_WIDTH-1) loop
      Y(i) := X(i);
    end loop;

    return Y;
  end;

  function to_AlgoTriggerObjects (
    X : AlgoOutput)
    return AlgoTriggerObjects is
    variable Y : AlgoTriggerObjects(OUTPUT_TOBS-1 downto 0);
  begin  -- function to_AlgoTriggerObjects
    for i in 0 to (OUTPUT_TOBS-1) loop
      Y(i) := X(i);
    end loop;
    return Y;
  end function to_AlgoTriggerObjects;



end AlgoDataTypes;
