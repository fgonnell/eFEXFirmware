-- generalised from: ipbus_dpram
-- F. Gonnella, October 2017
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
library ipbus_lib;

use ipbus_lib.ipbus.all;
use work.DataTypes.all;
use work.AlgoDataTypes.all;

entity ipbus_sorting_inputRAM_wrapper is
  port(
    clk_ipb   : in  std_logic;
    rst       : in  std_logic;
    ipb_in    : in  ipb_wbus;
    ipb_out   : out ipb_rbus;
    SortingIn : in  AlgoOutput;
    BCIDIn    : in BCID_t;
    SpyBCIDIn : in std_logic := '0';
    
    rclk       : in  std_logic;         --280
    Sync       : in  std_logic;
    we         : in  std_logic := '0';
    SortingOut : out AlgoOutput
    );

end ipbus_sorting_inputRAM_wrapper;

architecture rtl of ipbus_sorting_inputRAM_wrapper is

  component SortingInputRAM
    port (
      clka  : in  std_logic;
      ena   : in  std_logic;
      wea   : in  std_logic_vector(0 downto 0);
      addra : in  std_logic_vector(9 downto 0);
      dina  : in  std_logic_vector(31 downto 0);
      douta : out std_logic_vector(31 downto 0);
      clkb  : in  std_logic;
      enb   : in  std_logic;
      web   : in  std_logic_vector(0 downto 0);
      addrb : in  std_logic_vector(6 downto 0);  --contains 16 BCs*5TOBS
      dinb  : in  std_logic_vector(255 downto 0);
      doutb : out std_logic_vector(255 downto 0)
      );
  end component;


  -- algorithm signals
  signal dSync      : std_logic_vector(5 downto 0);
  signal din        : std_logic_vector(255 downto 0);
  signal q          : std_logic_vector(255 downto 0);
  signal address    : std_logic_vector(6 downto 0) := "0000000";  --16 BCs*5TOBs
  signal counter    : std_logic_vector(2 downto 0) := "000";   --0-6
  signal BC_counter : std_logic_vector(3 downto 0) := "0000";  --16 BCs*5TOBs
  signal FirstFive  : std_logic                    := '0';

  type   AlgoOutputArray is array (3 downto 0) of AlgoOutput;  -- output shift register
  signal dOutput : AlgoOutputArray;

  --ipbus signals
  signal ack          : std_logic;
  signal ack2         : std_logic;
  signal ipbus_write  : std_logic_vector(0 downto 0);
  signal write_enable : std_logic_vector(0 downto 0);

begin
  IPBUS_RAM : process(clk_ipb)
  begin
    if rising_edge(clk_ipb) then
      if ipb_in.ipb_strobe = '1' and ipb_in.ipb_write = '1' then
        ipbus_write(0) <= '1';
      else
        ipbus_write(0) <= '0';
      end if;
      ack2 <= ipb_in.ipb_strobe and (not ack2) and (not ack);
      ack  <= ack2;

    end if;
  end process;

  ipb_out.ipb_ack <= ack;
  ipb_out.ipb_err <= '0';
  --selects the first 5 ticks of the BCs  
--  FirstFive       <= '0' when unsigned(counter) > 4 else '1';
  FirstFive       <= '0' when counter = "101" or counter = "110" or counter = "111" else '1';

  write_enable(0) <= we and FirstFive;  --don't write on RAM on 6th and 7th tick

  COUNTER_PROC : process(rclk)
  begin
    if rising_edge(rclk) then
      dSync(0)                   <= Sync;
      dSync(dSync'high downto 1) <= dSync(dSync'high-1 downto 0);

      if dSync(5) = '1' then
        BC_counter <= std_logic_vector(unsigned(BC_counter) + 1);
      end if;

      if BC_Counter = "0000" and dSync(5) = '1' then
        counter <= (others => '0');
        address <= (others => '0');
      else
        if counter = "110" then         --counts the 7 ticks in one BC
          counter <= (others => '0');
        else
          counter <= std_logic_vector(unsigned(counter) + 1);
        end if;

        if FirstFive = '1' then
          if address = "1001111" then
            address <= (others => '0');
          else
            address <= std_logic_vector(unsigned(address) + 1);
          end if;
        else
          address <= address;
        end if;

--        -- Output is sync'ed with
        dOutput(0)                     <= to_AlgoOutput(q);
        dOutput(dOutput'high downto 1) <= dOutput(dOutput'high-1 downto 0);
      end if;
    end if;
  end process;

  ALGO_OUTPUT_RAM : SortingInputRAM
    port map (
      clka  => clk_ipb,
      ena   => ipb_in.ipb_strobe,
      wea   => ipbus_write,
      addra => ipb_in.ipb_addr(9 downto 0),
      dina  => ipb_in.ipb_wdata,
      douta => ipb_out.ipb_rdata,
      clkb  => rclk,
      enb   => '1',
      web   => write_enable,
      addrb => address,
      dinb  => din,
      doutb => q
      );

  SortingOut <= dOutput(3);
  din        <= to_LogicVector(SortingIn) when spyBCIDIn = '0'  else to_LogicVector(SortingIn)(din'high - 32 downto 0) & x"f" & "0" & BCIDIn & x"00fff";

end rtl;
