--! @file
--! @brief SIPO Sorting TOBs for process FPGA
--! @details 
--! \verbatim
--! This file sorts the TOBs from 32b * 7 in series into one parallel word of 224b.
--! The valid signals of all TOBs are paralled up into one 7b word.
--! 
--! Only 6 TOBs and 6 valid flags are stored, the 7th TOB data is ignored together with its valid flag.
--! \endverbatim
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;
use TOB_rdout_lib.data_type_pkg.all;

library Infrastructure_lib;

--! @copydoc SIPO_TOPO_TOBs_unit.vhd
entity SIPO_TOPO_TOBs_unit is
    Port ( 
       --! 280Mhz input signal  
        clk_in_280M          : in STD_LOGIC;
        --! 32b sorted TOB in - 7 x 32b words in series
        TOBs_in              : in STD_LOGIC_VECTOR (31 downto 0);
        --! TOB synch signal - indicating the start of the 7 TOB data words.      
        TOBs_sync_in         : in STD_LOGIC;
        --! 1b valid signal - 7 x 1b valid in series
        TOB_valid_flg_in     : in STD_LOGIC;
        --! 4b error flags for 49 links - OR of 49 fibre error flags together.
        link_err_4b_in           : in STD_LOGIC_VECTOR (3 downto 0);    
        --! sorted TOB BCN with delay through ALGO/sorting block
        ALGO_TOB_BCN_in         : in std_logic_vector(6 downto 0);
        --! 6 * 32b sorted TOBs = 192b (ignore 7th TOB)
        TOPO_TOB_out         : out STD_LOGIC_VECTOR (191 downto 0); 
        --! 1b sorted TOBs synch signal output
        TOPO_TOB_sync_out    : out STD_LOGIC;
        --! 6b sorted TOBs valid (ignore 7th TOB)
        TOPO_TOB_valid_out    : out STD_LOGIC_VECTOR (5 downto 0);
        --! output 4b error flags for all 49 links - delayed to match the delay of the TOBs through this block
        link_err_4b_out       : out STD_LOGIC_VECTOR (3 downto 0); 
        --! sorted TOB BCN with delay to match the delay on the TOBs through this block   
        ALGO_TOB_BCN_out      : out std_logic_vector(6 downto 0)
        );
end SIPO_TOPO_TOBs_unit;

--! @copydoc SIPO_TOPO_TOBs_unit.vhd
architecture Behavioral of SIPO_TOPO_TOBs_unit is

    signal  reg_1_tmp        : std_logic_vector(31 downto 0);   -- 32b sorted TOB
    signal  reg_2_tmp        : std_logic_vector(31 downto 0);
    signal  reg_3_tmp        : std_logic_vector(31 downto 0);
    signal  reg_4_tmp        : std_logic_vector(31 downto 0);
    signal  reg_5_tmp        : std_logic_vector(31 downto 0);
    signal  reg_6_tmp        : std_logic_vector(31 downto 0);
    
    signal  sync_1_tmp       : std_logic;
    signal  sync_2_tmp       : std_logic;
    signal  sync_3_tmp       : std_logic;
    signal  sync_4_tmp       : std_logic;
    signal  sync_5_tmp       : std_logic;
    signal  sync_6_tmp       : std_logic;

    signal  wr_1_tmp       : std_logic;
    signal  wr_2_tmp       : std_logic;
    signal  wr_3_tmp       : std_logic;
    signal  wr_4_tmp       : std_logic;
    signal  wr_5_tmp       : std_logic;
    signal  wr_6_tmp       : std_logic;
    
---------   ILA Signals
    
    signal  TOB_valid_flg_in_vec       : STD_LOGIC_VECTOR (0 downto 0) ;
    signal  TOBs_sync_in_vec     : STD_LOGIC_VECTOR (0 downto 0) ;
    signal  sync_7_tmp_vec         : STD_LOGIC_VECTOR (0 downto 0) ;
    signal  wr_7_tmp_vec      : STD_LOGIC_VECTOR (0 downto 0) ;


begin

--! @details
--! \verbatim
--! This module delays the BCN to match the delay of the TOBs Data through this block.
--! \endverbatim 
U3_TOB_BCN_Delay : entity Infrastructure_lib.GeneralDelay
    generic map (
      delay => 5,   --generates delay of 5 + 1 clocks
      size  => 7)
    port map (
      clk      => clk_in_280M,
      data_in  => ALGO_TOB_BCN_in,
      data_out => ALGO_TOB_BCN_out);

--! @details
--! \verbatim
--! This module delays the 4b error flags to match the delay of the TOBs Data through this block.
--! \endverbatim 
U4_TOB_Error_Delay : entity Infrastructure_lib.GeneralDelay
    generic map (
      delay => 5,   --generates delay of 5 + 1 clocks
      size  => 4)
    port map (
      clk      => clk_in_280M,
      data_in  => link_err_4b_in,
      data_out => link_err_4b_out);

U1_proc : process (clk_in_280M)
    begin
        if(clk_in_280M'event and clk_in_280M = '1') then
--            sync_7_tmp <= TOBs_sync_in;       -- register input sync data
            sync_6_tmp <= TOBs_sync_in;      
            sync_5_tmp <= sync_6_tmp;      
            sync_4_tmp <= sync_5_tmp;      
            sync_3_tmp <= sync_4_tmp;
            sync_2_tmp <= sync_3_tmp;
            sync_1_tmp <= sync_2_tmp;
                  
--            reg_7_tmp  <= TOBs_in ;         -- register input data 
            reg_6_tmp <= TOBs_in;       
            reg_5_tmp <= reg_6_tmp;       
            reg_4_tmp <= reg_5_tmp;       
            reg_3_tmp <= reg_4_tmp;       
            reg_2_tmp <= reg_3_tmp;       
            reg_1_tmp <= reg_2_tmp;       
    
--            wr_7_tmp  <= TOB_valid_flg_in ;         -- register input data valid
            wr_6_tmp <= TOB_valid_flg_in;       
            wr_5_tmp <= wr_6_tmp;       
            wr_4_tmp <= wr_5_tmp;       
            wr_3_tmp <= wr_4_tmp;       
            wr_2_tmp <= wr_3_tmp;       
            wr_1_tmp <= wr_2_tmp;       
    
--            link_err_4b_out   <= link_err_4b_in;
            
        end if;
    end process U1_proc;
    
    -- need only 6 TOBs, ignore 7th TOB value
    TOPO_TOB_out        <= reg_6_tmp & reg_5_tmp & reg_4_tmp & reg_3_tmp & reg_2_tmp & reg_1_tmp;
    TOPO_TOB_valid_out  <= wr_6_tmp & wr_5_tmp & wr_4_tmp & wr_3_tmp & wr_2_tmp & wr_1_tmp;
    TOPO_TOB_sync_out   <= sync_1_tmp;


--    TOB_valid_flg_in_vec(0) <= TOB_valid_flg_in ;
--    TOBs_sync_in_vec(0) <= TOBs_sync_in ;
--    sync_7_tmp_vec(0) <= sync_1_tmp ;
--    wr_7_tmp_vec (0)  <= wr_1_tmp ;

--U1_ila_TOB_sorting : ila_ipbus_fabric_rd_wr
--PORT MAP (
--	clk    => clk_in_280M ,
--	probe0 => TOBs_in ,     -- 32b 
--	probe1 => (others => '0' ) ,         -- 32b  
--	probe2 => TOBs_sync_in_vec ,  -- 1b 
--	probe3 => TOB_valid_flg_in_vec,     -- 1b
--	probe4 => (others => '0' ),    -- 1b
--	probe5 => reg_7_tmp,    --32b 
--    probe6 => (others => '0' ),    -- 32b
--    probe7 => sync_7_tmp_vec,   -- 1b
--	probe8 => wr_7_tmp_vec  ,   -- 1b
--    probe9 => (others => '0' )    -- 1b
--);

end Behavioral;
