 
----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/10/2018 10:43:56 AM
-- Design Name: 
-- Module Name: cntrl_synch_latch - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.all;
LIBRARY xil_defaultlib;
--use xil_defaultlib.synch_type.all;

entity ctrl_synch_latch is
  Port ( 
     CLK160     : IN     std_logic;
     MGT_COMMADET : IN     std_logic;
     latch_enable  : OUT    std_logic
     
     );
end ctrl_synch_latch;

architecture Behavioral of ctrl_synch_latch is



signal cntr     : unsigned(3 downto 0);


begin


 process (clk160)
 
      begin
       if CLK160'  event and CLK160  ='1'  then
          latch_enable  <='0';
          cntr <= cntr +1;
            if  (MGT_COMMADET ='1' )then
                cntr <="0010";
            end if;  
         
            if (cntr = 3  and  MGT_COMMADET ='0'  ) then
                latch_enable  <='1';
                cntr <="0000";
            end if;
       end if;
   end process;    
         
         
end Behavioral;
