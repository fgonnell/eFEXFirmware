--! @file
--! @brief MGT quad ipbus control
--! @details 
--! Module to connect the single MGT to ipbus registers
--! @author Mohammed Syiad
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;
use work.ipbus_decode_efex_mgt_quad.all;

--!@copydoc mgt_quad_slaves.vhd
entity mgt_quad_slaves is
  generic(ENABLE : in std_logic := '1'
          );
  port (
    clk280           : in  std_logic_vector(3 downto 0);
    ipb_clk          : in  std_logic;
    ipb_rst          : in  std_logic;
    ipb_in           : in  ipb_wbus;
    ipb_out          : out ipb_rbus;
    loopback         : out std_logic_vector(2 downto 0);
    bc_reg_sel       : out std_logic_vector(15 downto 0);
    mux_sel          : out std_logic_vector(15 downto 0);
    softreset_tx     : out std_logic;
    softreset_rx     : out std_logic;
    qpll_lock        : in  std_logic;
    qpll_refclklost  : in  std_logic;
    tx_pd            : in  std_logic_vector(3 downto 0);
    rx_pd            : in  std_logic_vector(3 downto 0);
    error_counter    : in  std_logic_vector(63 downto 0);
    bc_cntr_0        : in  std_logic_vector(4 downto 0);
    bc_cntr_1        : in  std_logic_vector(4 downto 0);
    bc_cntr_2        : in  std_logic_vector(4 downto 0);
    bc_cntr_3        : in  std_logic_vector(4 downto 0);
    bc_mux_cntr_0    : in  std_logic_vector(4 downto 0);
    bc_mux_cntr_1    : in  std_logic_vector(4 downto 0);
    bc_mux_cntr_2    : in  std_logic_vector(4 downto 0);
    bc_mux_cntr_3    : in  std_logic_vector(4 downto 0);
    delay_cntr_0     : in  std_logic_vector (3 downto 0);
    delay_cntr_1     : in  std_logic_vector (3 downto 0);
    delay_cntr_2     : in  std_logic_vector (3 downto 0);
    delay_cntr_3     : in  std_logic_vector (3 downto 0);
    mgt_enable       : out std_logic_vector(3 downto 0);
    phase_mux        : out std_logic_vector(15 downto 0);
    rx_resetdone     : in  std_logic_vector (3 downto 0);
    rx_fsm_resetdone : in  std_logic_vector (3 downto 0);
    rx_byteisaligned : in  std_logic_vector (3 downto 0);
    tx_resetdone     : in  std_logic_vector (3 downto 0);
    tx_fsm_resetdone : in  std_logic_vector (3 downto 0);
    tx_bufstatus     : in  std_logic_vector (7 downto 0);
    rx_realign       : in  std_logic_vector (3 downto 0);
    rx_disperr       : in  std_logic_vector (15 downto 0);
    encode_error     : in  std_logic_vector (15 downto 0)



    );
end mgt_quad_slaves;

--!@copydoc mgt_quad_slaves.vhd
architecture Behavioral of mgt_quad_slaves is
--constant N_SLAVES: positive := 5; 
  signal ipbw                                                               : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal ipbr, ipbr_d                                                       : ipb_rbus_array(N_SLAVES-1 downto 0);
  signal error_counter_reset                                                : std_logic;
  signal encode_error_int, rx_disperr_int                                   : std_logic_vector (3 downto 0);
  signal softreset_rx_int, softreset_tx_int, error_counter_reset_int        : std_logic;
  signal control_reg, synch_reg, pulse_reg, quad_status, phase_reg          : std_logic_vector (31 downto 0);
  signal pulse_reset, softreset_rx_i, error_counter_reset_i, softreset_tx_i : std_logic;
  signal clr_pulse_reg, cntr_reset                                          : std_logic;

begin
--- quad fabric decoder
  ENABLED_QUAD : if ENABLE = '1' generate
    fabric_quad : entity ipbus_lib.ipbus_fabric_sel
      generic map(NSLV      => N_SLAVES,
                  SEL_WIDTH => ipbus_sel_width)
      port map(
        ipb_in          => ipb_in,
        ipb_out         => ipb_out,
        sel             => ipbus_sel_efex_mgt_quad(ipb_in.ipb_addr),
        ipb_to_slaves   => ipbw,
        ipb_from_slaves => ipbr
        );
  end generate ENABLED_QUAD;

  DISABLED_QUAD : if ENABLE = '0' generate
    fabric_quad : entity ipbus_lib.ipbus_fabric_sel
      generic map(NSLV      => N_SLAVES,
                  SEL_WIDTH => ipbus_sel_width)
      port map(
        ipb_in          => IPB_WBUS_NULL,
        ipb_out         => open,
        sel             => (others => '0'),
        ipb_to_slaves   => open,
        ipb_from_slaves => (others => IPB_RBUS_NULL)
        );
    --ipb_out <= IPB_RBUS_NULL;
    ipb_out <= (x"DEADBEEF", '1', '0');
  end generate DISABLED_QUAD;

  encode_error_int(0) <= encode_error(0) or encode_error(1) or encode_error(2) or encode_error(3);
  encode_error_int(1) <= encode_error(4) or encode_error(5) or encode_error(6) or encode_error(7);
  encode_error_int(2) <= encode_error(8) or encode_error(9) or encode_error(10) or encode_error(11);
  encode_error_int(3) <= encode_error(12) or encode_error(13) or encode_error(14) or encode_error(15);

  rx_disperr_int(0) <= rx_disperr(0) or rx_disperr(1) or rx_disperr(2) or rx_disperr(3);
  rx_disperr_int(1) <= rx_disperr(4) or rx_disperr(5) or rx_disperr(6) or rx_disperr(7);
  rx_disperr_int(2) <= rx_disperr(8) or rx_disperr(9) or rx_disperr(10) or rx_disperr(11);
  rx_disperr_int(3) <= rx_disperr(12) or rx_disperr(13) or rx_disperr(14) or rx_disperr(15);

  loopback                <= control_reg(2 downto 0);  --- loopback signal 
  mgt_enable              <= control_reg(7 downto 4);  -- mgt enable signals 
  bc_reg_sel              <= synch_reg (15 downto 0);
  mux_sel                 <= synch_reg (31 downto 16);
  softreset_tx_int        <= pulse_reg(0);  -- tx reset pulse
  softreset_rx_int        <= pulse_reg(1);  -- rxreset pulse
  error_counter_reset_int <= pulse_reg(2);  -- error counter reset pulse
  quad_status             <= x"0000000" & "00" & qpll_refclklost & qpll_lock;  --- concatenating  the pll  status signals
  softreset_tx            <= softreset_tx_i;
  softreset_rx            <= softreset_rx_i;
  phase_mux               <= phase_reg(15 downto 0);

  softreset_rx_pulse : entity work.led_stretch  -- softresetrx pulse generator 
    port map(
      input  => softreset_rx_int,
      clk    => clk280(0),
      reset  => ipb_rst,
      output => softreset_rx_i
      );

  softreset_tx_pulse : entity work.led_stretch  -- softresettx pulse generator 
    port map(
      input  => softreset_tx_int,
      clk    => clk280(0),
      reset  => ipb_rst,
      output => softreset_tx_i
      );

  error_counter_reset_pulse : entity work.led_stretch  -- pulse generator 
    port map(
      input  => error_counter_reset_int,
      clk    => clk280(0),
      reset  => ipb_rst,
      output => error_counter_reset_i
      );

  cntr_reset    <= error_counter_reset_i or softreset_rx_i;  -- error counter will be reset when rx is reset 
  pulse_reset   <= softreset_rx_i or error_counter_reset_i or softreset_tx_i;
  clr_pulse_reg <= ipb_rst or pulse_reset;  -- reset the pulse register with ipb reset or when it generates one pulse 

  MGT_QUAD_Control : entity work.ipbus_reg  --- control register
    generic map (
      addr_width => 0
      )
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_QUAD_MGT_CONTROL),
      ipbus_out => ipbr(N_SLV_QUAD_MGT_CONTROL),
      q         => control_reg
      );

  MGT_QUAD_Synch : entity work.ipbus_reg  --- synchronise  register         
    generic map (addr_width => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_QUAD_SYNCH_CONTROL),
      ipbus_out => ipbr(N_SLV_QUAD_SYNCH_CONTROL),
      q         => synch_reg
      );

  MGT_QUAD_Pulse : entity work.ipbus_reg  --- pulse signal   register         
    generic map (addr_width => 0)
    port map(
      clk       => ipb_clk,
      reset     => clr_pulse_reg,  -- clear the pulse register after it generates one pulse                     
      ipbus_in  => ipbw(N_SLV_QUAD_MGT_PULSE),
      ipbus_out => ipbr(N_SLV_QUAD_MGT_PULSE),
      q         => pulse_reg
      );

  MGT_QUAD_status : entity work.module_status  -- reads the status registers
    port map(
      ipbus_in  => ipbw(N_SLV_QUAD_MGT_STATUS),
      ipbus_out => ipbr(N_SLV_QUAD_MGT_STATUS),
      status    => quad_status
      );

  MGT_QUAD_PHASE : entity work.ipbus_reg  --- phase shift  register         
    generic map (addr_width => 0)
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_QUAD_PHASE_CONTROL),
      ipbus_out => ipbr(N_SLV_QUAD_PHASE_CONTROL),
      q         => phase_reg
      );


  MGT_GT0 : entity work.gt_information
    generic map (addr_width => 3)
    port map(
      ipb_clk          => ipb_clk,
      clk280           => clk280(0),
      ipb_rst          => ipb_rst,
      reset            => cntr_reset,
      ipb_in           => ipbw(N_SLV_GT0),
      ipb_out          => ipbr(N_SLV_GT0),
      tx_pd            => tx_pd(0),
      rx_pd            => rx_pd(0),
      error_counter    => error_counter(15 downto 0),
      bc_cntr          => bc_cntr_0,
      bc_mux_cntr      => bc_mux_cntr_0,
      delay_cntr       => delay_cntr_0,
      not_intable      => encode_error_int(0),
      rx_resetdone     => rx_resetdone(0),
      rx_fsm_resetdone => rx_fsm_resetdone(0),
      rx_byteisaligned => rx_byteisaligned(0),
      tx_resetdone     => tx_resetdone(0),
      tx_fsm_resetdone => tx_fsm_resetdone(0),
      tx_bufstatus     => tx_bufstatus(1 downto 0),
      rx_realign       => rx_realign(0),
      rx_disperr       => rx_disperr_int(0)
      

      );

  MGT_GT1 : entity work.gt_information
    generic map (addr_width => 3)
    port map(
      ipb_clk          => ipb_clk,
      clk280           => clk280(1),
      ipb_rst          => ipb_rst,
      reset            => cntr_reset,
      ipb_in           => ipbw(N_SLV_GT1),
      ipb_out          => ipbr(N_SLV_GT1),
      tx_pd            => tx_pd(1),
      rx_pd            => rx_pd(1),
      error_counter    => error_counter(31 downto 16),
      bc_cntr          => bc_cntr_1,
      bc_mux_cntr      => bc_mux_cntr_1,
      delay_cntr       => delay_cntr_1,
      not_intable      => encode_error_int(1),
      rx_resetdone     => rx_resetdone(1),
      rx_fsm_resetdone => rx_fsm_resetdone(1),
      rx_byteisaligned => rx_byteisaligned(1),
      tx_resetdone     => tx_resetdone(1),
      tx_fsm_resetdone => tx_fsm_resetdone(1),
      tx_bufstatus     => tx_bufstatus(3 downto 2),
      rx_realign       => rx_realign(1),
      rx_disperr       => rx_disperr_int(1)
     
      );

  MGT_GT2 : entity work.gt_information
    generic map (addr_width => 3)
    port map(
      ipb_clk          => ipb_clk,
      clk280           => clk280(2),
      ipb_rst          => ipb_rst,
      reset            => cntr_reset,
      ipb_in           => ipbw(N_SLV_GT2),
      ipb_out          => ipbr(N_SLV_GT2),
      tx_pd            => tx_pd(2),
      rx_pd            => rx_pd(2),
      error_counter    => error_counter(47 downto 32),
      bc_cntr          => bc_cntr_2,
      bc_mux_cntr      => bc_mux_cntr_2,
      delay_cntr       => delay_cntr_2,
      not_intable      => encode_error_int(2),
      rx_resetdone     => rx_resetdone(2),
      rx_fsm_resetdone => rx_fsm_resetdone(2),
      rx_byteisaligned => rx_byteisaligned(2),
      tx_resetdone     => tx_resetdone(2),
      tx_fsm_resetdone => tx_fsm_resetdone(2),
      tx_bufstatus     => tx_bufstatus(5 downto 4),
      rx_realign       => rx_realign(2),
      rx_disperr       => rx_disperr_int(2)
      
      
      );

  MGT_GT3 : entity work.gt_information
    generic map (addr_width => 3)
    port map(
      ipb_clk          => ipb_clk,
      clk280           => clk280(3),
      ipb_rst          => ipb_rst,
      reset            => cntr_reset,
      ipb_in           => ipbw(N_SLV_GT3),
      ipb_out          => ipbr(N_SLV_GT3),
      tx_pd            => tx_pd(3),
      rx_pd            => rx_pd(3),
      error_counter    => error_counter(63 downto 48),
      bc_cntr          => bc_cntr_3,
      bc_mux_cntr      => bc_mux_cntr_3,
      delay_cntr       => delay_cntr_3,
      not_intable      => encode_error_int(3),
      rx_resetdone     => rx_resetdone(3),
      rx_fsm_resetdone => rx_fsm_resetdone(3),
      rx_byteisaligned => rx_byteisaligned(3),
      tx_resetdone     => tx_resetdone(3),
      tx_fsm_resetdone => tx_fsm_resetdone(3),
      tx_bufstatus     => tx_bufstatus(7 downto 6),
      rx_realign       => rx_realign(3),
      rx_disperr       => rx_disperr_int(3)
      
      );

end Behavioral;
