-- This is the top level of the Slaves Ipbus  of the Control FPGA.
-- The ipbus slaves live in this entity - modify according to requirements
-- Ports can be added to give ipbus slaves access to the chip top level.

-- Mohammed Siyad October 2015

library IEEE;
use IEEE.STD_LOGIC_1164.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;
use work.spi.all;
library infrastructure_lib;
use infrastructure_lib.all;
use infrastructure_lib.mgt_type.all;
use infrastructure_lib.ipbus_decode_efex_cntrl_infrastructure.all;
entity infrastructure_slaves_cntrl is
  port(
    ipb_clk : in  std_logic;
    ipb_rst : in  std_logic;
    ipb_in  : in  ipb_wbus;
    ipb_out : out ipb_rbus;
    --- module status signals
    status  : in  std_logic_vector(31 downto 0);

    --- module control signals  
    control_reg : out std_logic_vector(31 downto 0);
    
    --- ipbus io delay signals
    ipbus_delay_in_ctrl    : out std_logic_vector(31 downto 0);
    ipbus_delay_out_ctrl   : out std_logic_vector(31 downto 0);
    ipbus_delay_in_status  : in std_logic_vector(31 downto 0) := (others => '0');
    ipbus_delay_out_status : in std_logic_vector(31 downto 0) := (others => '0');
    
    --- reconfigure signal
    reconfig_reg : out std_logic_vector(31 downto 0);

    --- i2c bus 0 signals
    scl_i_0    : in  std_logic;
    scl_o_0    : out std_logic;
    scl_enb_0  : out std_logic;
    sda_o_0    : out std_logic;
    sda_i_0    : in  std_logic;
    sda_enb_0  : out std_logic;
    --- pll signals
    pll_miso   : in  std_logic;
    pll_le     : out std_logic;
    pll_clko   : out std_logic;
    pll_mosi   : out std_logic;
    pll_select : out std_logic_vector(1 downto 0);

    --- flash signals   
    flash_miso : in  std_logic;
    flash_le   : out std_logic;
    flash_clko : out std_logic;
    flash_mosi : out std_logic

    );

end infrastructure_slaves_cntrl;

architecture Behavioral of infrastructure_slaves_cntrl is



--      constant sel_width:positive := 4;
  signal ipbw                       : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal ipbr, ipbr_d               : ipb_rbus_array(N_SLAVES-1 downto 0);
  signal ctrl_pulse_reg             : std_logic_vector(31 downto 0);
  signal pll_spi_in, flash_spi_in   : spi_mi;
  signal pll_spi_out, flash_spi_out : spi_mo;
  signal flash_select               : std_logic_vector(1 downto 0) := (others => '0');


begin


--control_reg <= ctrl_pulse_reg ( 31 downto 0);



--- spi pll assignment
  pll_spi_in.miso <= pll_miso;
  pll_le          <= pll_spi_out.le;
  pll_mosi        <= pll_spi_out.mosi;
  pll_clko        <= pll_spi_out.clk;

--- spi flash pin assignment
  flash_spi_in.miso <= flash_miso;
  flash_mosi        <= flash_spi_out.mosi;
  flash_le          <= flash_spi_out.le;
  flash_clko        <= flash_spi_out.clk;




  fabric : entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV      => N_SLAVES,
                SEL_WIDTH => ipbus_sel_width)
    port map(
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      sel             => ipbus_sel_efex_cntrl_infrastructure(ipb_in.ipb_addr),
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      );


  module_control : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 1,              --number of control reg 
      N_STAT => 1)              --number of status reg
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_MODULE_REG),
      ipbus_out => ipbr(N_SLV_MODULE_REG),
      d         => (0 => status),
      q(0)      => (control_reg),
--    qmask     => (others => '0'),     
      stb       => open
      );


  xadc : entity work.ipbus_xadc_drp  -- accessing the xadc of the FPGA
    port map(
      ipb_clk   => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_XADC),
      ipbus_out => ipbr(N_SLV_XADC)

      );

  reconfig : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 1,                 --number of control reg 
      N_STAT => 0)                 --number of status reg
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_RECONFIGURE),
      ipbus_out => ipbr(N_SLV_RECONFIGURE),
      d         => (others =>(others => '0')),
      q(0)      => (reconfig_reg),
      stb       => open
      );

  i2c_0 : entity work.ipbus_i2c_master
    generic map (addr_width => 3  -- slave that allow to access i2c devices that connect to i2c bus 
                 )
    port map(
      clk     => ipb_clk,
      rst     => ipb_rst,
      ipb_in  => ipbw(N_SLV_I2C),
      ipb_out => ipbr(N_SLV_I2C),
      scl_i   => scl_i_0,
      scl_o   => scl_o_0,
      scl_enb => scl_enb_0,
      sda_o   => sda_o_0,
      sda_i   => sda_i_0,
      sda_enb => sda_enb_0
      );


  spi_pll : entity work.ipbus_spi32  -- Slave that used to configure the PLL and read its status
    generic map(
      BYTE_SPI   => false,
      ADDR_WIDTH => 6
      )
    port map(
      ipbus_clk => ipb_clk,
      reset     => ipb_rst,
      ipb_in    => ipbw(N_SLV_PLL_SPI_RAM),
      ipb_out   => ipbr(N_SLV_PLL_SPI_RAM),
      spi_in    => pll_spi_in,
      spi_out   => pll_spi_out,
      selreg    => pll_select

      );

  spi_flash : entity work.ipbus_spi32  --access to SPI FLASH for write/read it
    generic map(
      BYTE_SPI   => true,
      ADDR_WIDTH => 9
      )
    port map(
      ipbus_clk => ipb_clk,
      reset     => ipb_rst,
      ipb_in    => ipbw(N_SLV_FLASH_SPI_RAM),
      ipb_out   => ipbr(N_SLV_FLASH_SPI_RAM),
      spi_in    => flash_spi_in,
      spi_out   => flash_spi_out,
      selreg    => flash_select

      );

  io_delay : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 2,                 --number of control reg 
      N_STAT => 2)                 --number of status reg
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_IPBUS_IO_DELAY),
      ipbus_out => ipbr(N_SLV_IPBUS_IO_DELAY),
      d(0)      => ipbus_delay_in_status,
      d(1)      => ipbus_delay_out_status,     
      ctrl_default(0) => x"1e0fffff", -- 11111 11111 11111 11111
      ctrl_default(1) => x"1e0fffff", -- 11111 11111 11111 11111
      q(0)      => ipbus_delay_in_ctrl,
      q(1)      => ipbus_delay_out_ctrl,      
      stb       => open
      );



  RAM : entity ipbus_lib.ipbus_ram  -- internal ram for testing the iPbus transactions.
    generic map(
      ADDR_WIDTH => 10
      )
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_RAM),
      ipbus_out => ipbr(N_SLV_RAM)
      );

end Behavioral;
