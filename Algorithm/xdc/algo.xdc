### Algorithm
## Rename the automatically generated clock to further constrain them
create_generated_clock -name load [get_pins clock_resources/Inputclk40M/inst/mmcm_adv_inst/CLKOUT1]
create_generated_clock -name clk200 [get_pins clock_resources/Inputclk40M/inst/mmcm_adv_inst/CLKOUT0]
create_generated_clock -name clk280 [get_pins clock_resources/Inputclk40M/inst/mmcm_adv_inst/CLKOUT2]
create_generated_clock -name clk40 [get_pins clock_resources/Inputclk40M/inst/mmcm_adv_inst/CLKOUT3]

create_generated_clock -name clk160 [get_pins clock_resources/clk160_gen/inst/mmcm_adv_inst/CLKOUT0]

## Multicicle constraint for algorithm transition between 200 and 280 match PHASE280
set_multicycle_path -setup -end -from [get_clocks clk200] -to [get_clocks clk280] 2
set_multicycle_path -hold -end -from [get_clocks clk200] -to [get_clocks clk280] 1


## Multicicle constraint for algorithm transition between 40 and 200 has to match the phase PHASE200
set_multicycle_path -setup -end -from [get_clocks clk40] -to [get_clocks clk200] 2
set_multicycle_path -hold -end -from [get_clocks clk40] -to [get_clocks clk200] 1

## Exclude 40 -> 280
set_false_path -from [get_clocks clk40] -to [get_clocks clk280]
