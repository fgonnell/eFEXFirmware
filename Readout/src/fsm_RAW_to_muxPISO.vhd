--! @file
--! @brief FSM to write RAW calolimeter data to Link Output FIFO for process FPGA
--! @details 
--! \verbatim
--! This module creates a complete RAW calolimeter event together with Header and Trailer and writes the entire event into Link Output FIFO.
--! The Link Output FIFO is then controlled by FIFO_to_MGT_FSM state machine to transfer data to MGT.
--! This FSM also handles Header and Trailer construction.
--!
--! In order to create one full RAW event (frame) all 49 fibre channels must be read in turn from channel 0 to channel 48,
--! This proces takes 598 ticks of 280MHz clock, approximately 2135 ns.
--!
--! Under normal operation mode, only MGT channels with error flag set, are added to the RAW data event frame.
--! If there are no errors, then an event is generated with 2 header and 1 trailer words, and the payload is two 32-b words
--! containing the error flags of all 49 MGT channels.
--! In this case generating a RAW Event takes only 15 ticks of 280MHz clock, approximately 53.571 ns.
--!
--! If Privilege Read flag or Read_RAW_all flag is set, then the RAW data for all MGT channels are added to the RAW data event frame.
--! Read_RAW_all flag is bit 1 of Test_Contol_Reg register.
--! The Privilege Read flag is bit 9 of TTC B-channel command.
--! Privilege Read or Read_RAW_all, generate a full event which also takes 598 ticks of 280MHz clock, approximately 2135 ns.
--!
--! Under Safe Mode Operation, the occupancy of TTC FIFO are monitored.
--! If the occupancy reaches a programmable threshold, empty events are generated which consits of 2 header and 1 trailer words.
--! The RAW Event under safe mode operation is 11 ticks of 280MHz clock, approximately 39.29 ns
--!
--! The output of this FSM is:
--!     32-bit data word
--!     1-bit data is CHAR
--!     1-bit valid which is the write enable to Link Ouput FIFO
--! 
--! 
--! CHAR constants are defined in data_type_pkg.vhd
--! for reference only
--!    constant ch_idle    : std_logic_vector(7 downto 0)  := X"BC"  ;    -- idle char is K28.5
--!    constant ch_sop1    : std_logic_vector(7 downto 0)  := X"3C"  ;    -- TOB/XTOB star of packet char is K28.1
--!    constant ch_sop2    : std_logic_vector(7 downto 0)  := X"7C"  ;    -- CALO DATA star of packet char is K28.3
--!    constant ch_eop     : std_logic_vector(7 downto 0)  := X"DC"  ;    -- end  of packet char is K28.6
--! 
--! \endverbatim
    
--! @author Saeed Taghavi

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;
use TOB_rdout_lib.data_type_pkg.all;

--! @copydoc fsm_RAW_to_muxPISO.vhd
entity fsm_RAW_to_muxPISO is
    Port ( 
       RST                        : in std_logic ;
       --! FPGA Hardware Address
       hw_addr                    : in STD_LOGIC_VECTOR(1 downto 0) ;   -- FPGA Hardware Address
       --! raw data 36b = 4b+32b  ERR+RAW
       rdout_RAW_36b_in           : in DPR_RAW_out_36_type;  
       --! RAW data valid signal
       valid_RAW_in               : in t_49_arr_1b;
       --! BCN fifo data 47b = 1b FIFO_RAW_Data_prog_full_i + 1b ORed_err_flags + 1b Privilege Read + 12b BCN_ID_in + 32b L1A_ID_in
       -- 1b FIFO_RAW_Data_prog_full_i + 1b ORed_err_flags + 1b Privilege Read + 12b BCN_ID_in + 8b ECID + 24b L1A_ID_in
       bcn_fifo_47b_in            : in STD_LOGIC_VECTOR(46 downto 0);  
       --! RAW data valid signal
       valid_BCN_in               : in std_logic ;
       --! Link Error FIFO = 1-b request RAW data on error + ORed 4-bit link error + 49-bit channel error map
       FIFO_error_flags_54b       : in STD_LOGIC_VECTOR(53 downto 0);    
       --! fabric 280MHz clock
       clk_280M_in                : in STD_LOGIC;
       --!  RAW Input FIFO partial FULL flag 
       RAW_FIFO_prog_full_in       : in  std_logic ; 
       --!  Link Output FIFO partial FULL flag to receive RAW calorimeter data
       LO_FIFO_prog_full_in        : in  std_logic ; 
       --!  Link Output FIFO FULL flag
       LO_FIFO_full_in             : in  std_logic ;
       --!  Link Output FIFO data count
       LO_FIFO_data_count_in      : in  STD_LOGIC_VECTOR (12 downto 0)  ;
       --! Comp;leted frame counter enable
       frame_cntr_en              : out STD_LOGIC;
       --! readout all raw data links
       raw_rd_all_in        : in STD_LOGIC;                 
       --! read enable signal to all RAW data FIFOs
       RAW_rdout_fifo_rd_en_out   : OUT t_49_arr_1b;
       --! read enable signal to BCN & L1A FIFOs
       BCN_fifo_rd_en_out         : out STD_LOGIC;
       --! FIFO empty flag from BCN L1A FIFO
       BCN_FIFO_empty     : in STD_LOGIC;
       --! FIFO empty flag from link error FIFO
       link_err_FIFO_empty     : in STD_LOGIC;
       --! RAW data valid signal to Link_outpout_FIFO
       RAW_out_valid              : out STD_LOGIC;
       --! RAW data is CHAR signal to Link_outpout_FIFO
       RAW_out_is_char            : out STD_LOGIC;
       --! RAW data 32b to Link_outpout_FIFO
       RAW_out                   : out STD_LOGIC_VECTOR (31 downto 0)
   );
   
end fsm_RAW_to_muxPISO;

--! @copydoc fsm_RAW_to_muxPISO.vhd
architecture Behavioral of fsm_RAW_to_muxPISO is

	type FPGA_mapping_array is array(3 downto 0) of std_logic_vector(1 downto 0);
    constant FPGA_mapping				: FPGA_mapping_array := ("10", "01", "11", "00") ; -- HW address to processor number
    constant LO_almost_full_value       : unsigned := X"1FF0" ;
    signal  RAW_out_valid_i       		: std_logic ;
    signal  RAW_rdout_fifo_rd_en_i		: t_49_arr_1b ;
	signal  BCN_fifo_rd_en_out_i		: std_logic;

    signal  rec_err_flg_4b_i	: STD_LOGIC_VECTOR (3 downto 0) ;  -- recovered error flag for mgt link
    signal  rd_on_err_en_i      : std_logic ;                      -- read on error enable
	signal  BCN_in_i            : STD_LOGIC_VECTOR (11 downto 0) ;
	signal  valid_BCN_i         : STD_LOGIC ;
    signal  L1A_ID_in_i         : STD_LOGIC_VECTOR (23 downto 0) ;
    signal  L1A_ID_EXT_in_i     : STD_LOGIC_VECTOR (7 downto 0) ;

    -- TTC_read_all_in signal input (previledge read)
    signal TTC_read_all_in      : STD_LOGIC;    -- privilege read
    signal FIFO_RAW_Data_prog_full_i      : STD_LOGIC;    -- RAW DATA FIFO is pfull so operate in SAFE MODE
    signal ORed_err_flg_i       : STD_LOGIC;    -- OR of all error flag for normal readout
    signal  RAW_out_i           : STD_LOGIC_VECTOR(31 downto 0) ;

    signal  RAW_out_is_char_i  	: std_logic ;
	
    signal  CLK_280M_i         	: std_logic ;
    signal  RST_i              	: std_logic ;
	
    signal  payld_cntr_rst     	: std_logic ;
    signal  payld_cntr_rst_i   	: std_logic ;
    signal  payld_cntr_en      	: std_logic ;
    signal  raw_payld_cntr_i   	: std_logic_vector(11 downto 0) ;
		
    signal  frame_cntr_en_i    	: std_logic  ;
    signal  write_in_LO_FIFO_i	: std_logic  ;
	
    signal  RAW_in_i           	: DPR_RAW_out_36_type ;  -- array 49 of 36b = err + data;
    signal  RAW_in_tmp         	: DPR_RAW_out_36_type ;  -- array 49 of 36b = err + data;
    signal  RAW_in_valid_i     	: t_49_arr_1b ;		-- 49 valid signals
    signal  RAW_in_valid_tmp   	: t_49_arr_1b ;		-- 49 valid signals
    
    signal  opt_link_num_i      : std_logic_vector(7 downto 0);
    signal  fpga_num_i          : std_logic_vector(1 downto 0);
    signal  raw_slice_num_i     : std_logic_vector(3 downto 0);
    signal  raw_rd_all_in_i     : std_logic ;
    signal  safe_mode_i         : std_logic ;
    signal  BCN_FIFO_empty_i    : std_logic ;
    signal  link_err_FIFO_empty_i    : std_logic ;

--    signal  OR_of_error_map_i   : std_logic_vector(3 downto 0);
    signal  link_err_4b_in_i   : std_logic_vector(3 downto 0);
    signal  channel_error_map_i : STD_LOGIC_VECTOR (48 downto 0);  -- 49-bit channel error map to input into RAW readout

    TYPE STATE_TYPE IS (
       idle, wait_1, wait_2, wait_3, wait_4, wait_5, wait_6, wait_7, wait_8,
       start, pause, hdr_sel, err_sop, err_eop, BCN_wait,
       norm_sop_1, norm_sop_2, norm_tlr, norm_eop,
       rd_bcn_fifo, empty_raw_fifo,
       rdout_raw_1, rdout_raw_2, rdout_raw_3, rdout_raw_4, rdout_raw_5, rdout_raw_6, rdout_raw_7,
	   sub_trl_1, sub_trl_2
       );
  
    SIGNAL current_state : STATE_TYPE;
    signal i    : integer range 0 to 50;       -- used to index 49 FIFOs
    signal ii   : integer range 0 to 8;        -- used to index 7 raw data words from each FIFO
    
----  ####### attributes  ########
    attribute keep       : string ;
    attribute max_fanout : integer;

    attribute keep of       RAW_out_valid_i : signal is "true" ;
    attribute max_fanout of RAW_out_valid_i : signal is 30;
    attribute keep of       rd_on_err_en_i : signal is "true" ;
    attribute max_fanout of rd_on_err_en_i : signal is 30;
    attribute keep of       RAW_in_i : signal is "true" ;
    attribute max_fanout of RAW_in_i : signal is 30;    
    attribute keep of       RAW_rdout_fifo_rd_en_i : signal is "true" ;
    attribute max_fanout of RAW_rdout_fifo_rd_en_i : signal is 30;
    attribute keep of       LO_FIFO_prog_full_in : signal is "true" ;
    attribute max_fanout of LO_FIFO_prog_full_in : signal is 30;
    attribute keep of       RAW_FIFO_prog_full_in : signal is "true" ;
    attribute max_fanout of RAW_FIFO_prog_full_in : signal is 30;
    attribute keep of       raw_rd_all_in_i : signal is "true" ;
    attribute max_fanout of raw_rd_all_in_i : signal is 30;
    attribute keep of       safe_mode_i     : signal is "true" ;
    attribute max_fanout of safe_mode_i     : signal is 30;
    attribute keep of       i 				: signal is "true" ;
    attribute max_fanout of i 				: signal is 30;
    attribute keep of       ii 				: signal is "true" ;
    attribute max_fanout of ii 				: signal is 30;
----   #######################################

begin

    -- input ports
    CLK_280M_i           <= CLK_280M_in ;
    RST_i                <= RST ;
    
	RAW_in_tmp    		    <= rdout_RAW_36b_in;   -- assign input data
    RAW_in_valid_tmp		<= valid_RAW_in;
          
    -- Read all fibres under these 2 conditions
    raw_rd_all_in_i         <= raw_rd_all_in OR TTC_read_all_in;

    -- output ports
    RAW_rdout_fifo_rd_en_out  <= RAW_rdout_fifo_rd_en_i  ;	-- enable FIFO read out
    BCN_fifo_rd_en_out        <= BCN_fifo_rd_en_out_i  ;	-- enable BCN LIA FIFO read out
    
    payld_cntr_rst <= RST_i OR payld_cntr_rst_i;

-- internal signal asssignments


--! @details
--! \verbatim
--! raw_payld_length counter, counts the total number of payload data inserted between the Header and Trailer of a RAW Event.  
--! \endverbatim
U3_raw_payld_length : entity TOB_rdout_lib.cntr_generic
    generic map(
        width  =>  12
        )
    Port map ( 
       CE   =>  payld_cntr_en ,     -- count every valid data word (32b words)
       CLK  =>  CLK_280M_i ,
       RST  =>  payld_cntr_rst,
       Q    =>  raw_payld_cntr_i     -- count every valid data word (divide by 2 to show no. of 64b words)
       );

--! @details
--! \verbatim
--! This process disables writing to Link Output FIFO when the LO_FULL reaches a specific occupancy level of 0X1FF0.  
--! \endverbatim
U3A_proc1 : process (CLK_280M_i)
begin
    if rising_edge (CLK_280M_i) then 
       if (unsigned(LO_FIFO_data_count_in) < X"1FF0") then
           -- if LO_FIFO is nearly FULL then stop reading from de-randomistion FIFO
           write_in_LO_FIFO_i <= '1';
       else
          write_in_LO_FIFO_i <= '0' ;
       end if;
    end if;
  end process;
  
-- This is the state machine that controls, and generates complete RAW Events.  
-- A completed RAW Event is written to Link Output FIFO.  
-- It also increments a local counter to indicate to the FSM responsible for reading the events, 
-- that there are events waiting to be transmitted to Contol FPGA.  
U4_rd_fsm : process (CLK_280M_i)
    begin
        if CLK_280M_i'event and CLK_280M_i = '1' then
            BCN_FIFO_empty_i       <= BCN_FIFO_empty ;  -- register to remove timing error
            link_err_FIFO_empty_i  <= link_err_FIFO_empty ;  -- register to remove timing error
           -- pipeline the input from ERROR FIFO  
           ORed_err_flg_i          <= FIFO_error_flags_54b(53) ;            -- OR of all error flag to request RAW readout  
           link_err_4b_in_i        <= FIFO_error_flags_54b(52 downto 49) ;   -- 4-bit OR of all channel error map to input into RAW readout
           channel_error_map_i     <= FIFO_error_flags_54b(48 downto 0) ;   -- 49-bit channel error map to input into RAW readout

           -- BCN ID and L1A ID from FIFO
           -- 1b FIFO_RAW_Data_prog_full_i + 1b ORed_err_flags + 1b Privilege Read + 12b BCN_ID_in + 8b ECID + 24b L1A_ID_in
           FIFO_RAW_Data_prog_full_i <= bcn_fifo_47b_in(46) ;        -- FIFO_RAW_Data_prog_full is set so operate in SAFE MODE
           TTC_read_all_in      <= bcn_fifo_47b_in(44) ;             -- privilege read
           BCN_in_i             <= bcn_fifo_47b_in(43 downto 32);    -- BCN from output of fifo
           L1A_ID_EXT_in_i      <= bcn_fifo_47b_in(31 downto 24) ;   -- EXT L1A from output of fifo
           L1A_ID_in_i          <= bcn_fifo_47b_in(23 downto 0) ;    -- L1A from output of fifo
           valid_BCN_i          <= valid_BCN_in ;
           -- input signals
 	       RAW_in_i        <= RAW_in_tmp;     -- register input data to remove timing error
           RAW_in_valid_i  <= RAW_in_valid_tmp;
           if RST_i = '1' then
                current_state           <= idle ;
                RAW_out_i               <= (others => '0') ;
            else
                CASE current_state is
                    when idle =>
                        payld_cntr_rst_i          <= '0' ;
                        frame_cntr_en_i           <= '0' ;
                        RAW_rdout_fifo_rd_en_i    <= (others => '0') ;
                        BCN_fifo_rd_en_out_i      <= '0' ;
                        payld_cntr_en             <= '0' ;      
                        RAW_out_valid_i           <= '0' ;
                        RAW_out_is_char_i         <= '0' ;
                        raw_slice_num_i           <= "0000";
                        safe_mode_i               <= '0' ;
                        i  <= 0 ;
                        ii <= 0 ;
                        rd_on_err_en_i      <= '0' ;    -- clear flag
                        rec_err_flg_4b_i    <= (others => '0') ;    -- clear flag
                        current_state       <= idle ;  
                        if (write_in_LO_FIFO_i = '1' )  then -- if LO_FIFO is not full, less than X"1FF0"
                            if (BCN_FIFO_empty_i = '0') AND (link_err_FIFO_empty_i = '0') then   -- if BCN & link error fifos are not empty
                                -- if LO_FIFO is partial FULL AND RAW_FIFO_prog_full NOT set, then pause
                                if (LO_FIFO_prog_full_in = '1' AND RAW_FIFO_prog_full_in = '0')  then 
                                    current_state           <= idle ;
                                    BCN_fifo_rd_en_out_i    <= '0';
                                    RAW_out_valid_i         <= '0' ;
                                else 
                                    if (RAW_FIFO_prog_full_in = '1')  then -- if RAW_FIFO is partial FULL, then SAFE MODE
                                        safe_mode_i             <= '1' ;    -- enable safe mode flag
                                        RAW_rdout_fifo_rd_en_i  <= '1' & X"FFFFFFFFFFFF" ; -- read 1st word from all RAW FIFO
                                        BCN_fifo_rd_en_out_i    <= '1';       -- read data from BCN fifo
                                        RAW_out_valid_i         <= '0' ;      -- do not save in FIFO
                                        current_state           <= norm_sop_1 ;
                                    else    -- else it is normal operation
                                        BCN_fifo_rd_en_out_i    <= '1' ;      -- read data from BCN fifo
                                        RAW_out_valid_i         <= '0' ;      -- do not save in FIFO
                                        current_state   	    <= rd_bcn_fifo ;
                                    end if;
                                end if;
                             end if;
                         end if;

                    when rd_bcn_fifo =>         -- wait 1 clk for fifo output
                        RAW_rdout_fifo_rd_en_i  <= (others => '0') ;    -- stop RAW FIFO read
						BCN_fifo_rd_en_out_i    <= '0' ;
                        RAW_out_valid_i         <= '0' ;      -- do not save in FIFO
                        current_state   	<= rd_bcn_fifo ;
                        if valid_BCN_i = '1' then             -- if BCN FIFO data are valid
                            if (raw_rd_all_in_i = '1' )  then  -- if must read all RAW data
                                current_state   	    <= norm_sop_1 ;
                            else
                                if (FIFO_RAW_Data_prog_full_i = '1' OR ORed_err_flg_i = '0')  then -- if RAW data FIFO is FULL or there are no errors,  then empty buffers
                                    RAW_rdout_fifo_rd_en_i  <= '1' & X"FFFFFFFFFFFF" ; -- read 1st word from all RAW FIFO
                                    safe_mode_i             <= '1' ;   -- enable safe mode flag
                                    current_state   	    <= norm_sop_1 ;
                                else
                                    current_state   	<= norm_sop_1 ;
                                end if;
                            end if;
                        end if;
                        
--                    when BCN_wait =>         -- wait 1 clk to register L1A_ID and BCN
--                        current_state   	    <= norm_sop_1 ;
--                        if (raw_rd_all_in_i = '1' )  then  -- if must read all RAW data
--                            current_state   	    <= norm_sop_1 ;
--                        else
--                            if (FIFO_RAW_Data_prog_full_i = '1' OR ORed_err_flg_i = '0')  then -- if RAW data FIFO is FULL or there are no errors,  then empty buffers
--                                RAW_rdout_fifo_rd_en_i  <= '1' & X"FFFFFFFFFFFF" ; -- read 1st word from all RAW FIFO
--                                safe_mode_i             <= '1' ;   -- enable safe mode flag
--                            end if;
--                        end if;

                    when norm_sop_1 =>            -- normal data header SOP 1
                        BCN_fifo_rd_en_out_i    <= '0' ;
                        RAW_out_i               <= X"000" & BCN_in_i & ch_sop2 ;  -- 12b res + 12b BCN + K28.3 (0x7C)
                        RAW_out_is_char_i       <= '1';
                        RAW_out_valid_i         <= '1' ;      -- save in FIFO
                        current_state           <= norm_sop_2 ;
                        if ( safe_mode_i = '1' ) then  -- if in SAFE MODE
                            RAW_rdout_fifo_rd_en_i  <= '1' & X"FFFFFFFFFFFF" ; -- read 2nd word from all RAW FIFO
                        else
                            RAW_rdout_fifo_rd_en_i  <= (others => '0') ;    -- stop RAW FIFO read
                        end if;
                           
                    when norm_sop_2 =>            -- normal data header SOP 2
                        RAW_out_i               <= L1A_ID_EXT_in_i & L1A_ID_in_i ;  -- L1A_ID_EXT_in_i & L1A_ID_in_i 8b + 24b
                        RAW_out_is_char_i       <= '0';
                        RAW_out_valid_i         <= '1' ;
                        if ( safe_mode_i = '1' ) then  -- if in SAFE MODE or Read on Error mode
                            RAW_rdout_fifo_rd_en_i  <= '1' & X"FFFFFFFFFFFF" ; -- read 3rd word from all RAW FIFO 
                            current_state           <= empty_raw_fifo ;   -- else empty de-randomisation fifo
                        else        -- if in normal mode operation                            
                            current_state       <= wait_1; -- was rdout_raw_1 ;      -- read normal data
                            RAW_rdout_fifo_rd_en_i(i)  <= '1' ;    -- enable read out from RAW FIFO
                        end if ;
                       
                    when empty_raw_fifo =>     -- control FPGA not ready, empty RAW FIFO
                        RAW_out_valid_i         <= '0' ;
                        if (ii = 4) then        -- read 4 words from all RAW FIFO, exit
                           RAW_rdout_fifo_rd_en_i  <= (others => '0') ;
                           ii <= 0 ;
                           if (ORed_err_flg_i = '0' ) then  -- if in NO Read on Error cycle, generate Error Flags Payload only
                                safe_mode_i     <= '0' ;   -- clear safe mode flag as this is read on error
                                current_state   <= sub_trl_1 ;
                           else
                                current_state   <= norm_eop ;    -- otherwise put out trailer with no payload
                           end if;
                        else
                            RAW_rdout_fifo_rd_en_i  <= '1' & X"FFFFFFFFFFFF" ; -- read out from all RAW FIFO
                            current_state   <= empty_raw_fifo ;
                            ii <= ii + 1 ;
                        end if;
                                       
                    when wait_1 =>        -- for data to arrive
                        fpga_num_i              <= FPGA_mapping(to_integer(unsigned(hw_addr)));
                        opt_link_num_i          <= std_logic_vector(to_unsigned(i,8)) ; -- convert the optical link number to 32b word
                        payld_cntr_en           <= '0';   -- was RAW_in_valid_i(i);	-- count the data into link output fifo
                        RAW_out_valid_i         <= '0';   -- was RAW_in_valid_i(i) ;    -- on valid save in FIFO
						current_state   		<= wait_2 ;

                    when wait_2 =>        -- for data to arrive
                        payld_cntr_en           <= '0';   -- was RAW_in_valid_i(i);	-- count the data into link output fifo
                        RAW_out_valid_i         <= '0';   -- was RAW_in_valid_i(i) ;    -- on valid save in FIFO
						current_state   		<= wait_3 ;

                    when wait_3 =>        -- for data to arrive
                        payld_cntr_en           <= '0';   -- was RAW_in_valid_i(i);	-- count the data into link output fifo
                        RAW_out_valid_i         <= '0';   -- was RAW_in_valid_i(i) ;    -- on valid save in FIFO
						current_state   		<= wait_4 ;

                    when wait_4 =>        -- for data to arrive
						RAW_rdout_fifo_rd_en_i(i)  <= '1' ; -- enable read out from RAW FIFO
						-- count the number of VALID data (as below) into link output fifo
                        payld_cntr_en      <= (RAW_in_valid_i(i) AND (raw_rd_all_in_i OR channel_error_map_i(i) )) ;	
                        -- on valid save in FIFO, AND error_en = '1'  AND override register to read all RAW DATA
                        RAW_out_valid_i      <= (RAW_in_valid_i(i) AND (raw_rd_all_in_i OR channel_error_map_i(i) )) ;    
                        RAW_out_i               <= RAW_in_i(i)(31 downto 0);
--						rec_err_flg_4b_i	    <= RAW_in_i(i)(35 downto 32) ;  -- recovered error flag for mgt link
						rd_on_err_en_i          <= channel_error_map_i(i);
						current_state   		<= rdout_raw_2 ;
--						ii <= ii + 1 ;

                    when rdout_raw_2 =>         -- normal input RAW rd out 7 clks
                        RAW_out_i               <= RAW_in_i(i)(31 downto 0);
                        -- on valid save in FIFO, AND error_en = '1'  AND override register to read all RAW DATA
                        RAW_out_valid_i         <= ( (raw_rd_all_in_i OR rd_on_err_en_i) AND RAW_in_valid_i(i) );
                        -- count the number of VALID data (as above) into link output fifo
                        payld_cntr_en      <= ( (raw_rd_all_in_i OR rd_on_err_en_i) AND RAW_in_valid_i(i) );
                        if (ii = 6) then        -- 7 RAW data are read out, go to next link
                           ii <= 0 ;
                           current_state   <= rdout_raw_3 ;
						   i <= i + 1 ;		-- go to next FIFO read
						   RAW_rdout_fifo_rd_en_i(i)  <= '0' ; -- end read out from RAW FIFO
						   rec_err_flg_4b_i	    <= '0' & RAW_in_i(i)(34 downto 32) ;  -- recovered error flag for mgt link (bit 35 is not used for fibres) 
                        else
							if (ii > 1) then 
								RAW_rdout_fifo_rd_en_i(i)  <= '0' ; -- stop read out from RAW FIFO
								current_state   <= rdout_raw_2 ;
								ii <= ii + 1 ;
							else
								ii <= ii + 1 ;
								RAW_rdout_fifo_rd_en_i(i)  <= '1' ; -- enable read out from RAW FIFO

							end if;
                        end if;

                    when rdout_raw_3 =>         
                        -- count the number of VALID data (as above) into link output fifo
						payld_cntr_en      <= (raw_rd_all_in_i OR rd_on_err_en_i) ;		-- if link output fifo is not full
                        -- raw data trlr = 4b err + reserved 18b + fpga_num 2b + link_num 8b
                        RAW_out_i               <= rec_err_flg_4b_i & "00" & X"0000" & fpga_num_i & opt_link_num_i; 
                        RAW_out_valid_i         <= (raw_rd_all_in_i OR rd_on_err_en_i) ;    -- if link output fifo is not full 
                        if (i = 49) then        -- 49 FIFOs are read out, exit
                           i <= 0 ;
                           current_state   <= sub_trl_1 ;
                        else
							current_state   <= wait_1 ;
--							ii <= ii + 1 ;
							RAW_rdout_fifo_rd_en_i(i)  <= '1' ;	-- enable read out from RAW FIFO
                        end if;

					when sub_trl_1 =>            -- 1st status trailer
                        payld_cntr_en           <= '1' ;        -- count 1st status trailer
                        RAW_out_i               <= channel_error_map_i(31 downto 0);    -- 32b channel error map
                        RAW_out_valid_i         <= '1';    
                        current_state           <= sub_trl_2 ;

					when sub_trl_2 =>            -- 1st status trailer
                        payld_cntr_en      <= '1' ;        -- count 2nd status trailer
                        -- 4b OR_of_error_map_i & 11b & 17b channel error map
                        RAW_out_i               <= link_err_4b_in_i & "00000000000" & channel_error_map_i(48 downto 32);
                        RAW_out_valid_i         <= '1';    
                        current_state           <= wait_5 ;

                    when wait_5 =>        -- delay for the data to write into fifo
                        payld_cntr_en       <= '0' ;        -- stop count
                        RAW_out_valid_i     <= '0' ;
                        current_state       <= norm_eop ;
                            
                    when norm_eop =>        -- normal end of packet
                        payld_cntr_rst_i    <= '1' ;    -- reset the payload counter
                        RAW_out_is_char_i   <= '1';
                        RAW_out_i           <= safe_mode_i & "000" & X"00" & raw_payld_cntr_i(11 downto 0) & ch_eop ;    -- safe_mode + 11b + payld_cntr + 0xDC
                        RAW_out_valid_i     <= '1' ;
                        i <= 0 ;
                        current_state       <= wait_6 ;
                        
                    when wait_6 =>        -- delay for the data to write into fifo
                        payld_cntr_rst_i    <= '0' ;
                        RAW_out_is_char_i   <= '0';
                        RAW_out_valid_i     <= '0' ;
                        current_state       <= wait_7 ;
                            
                    when wait_7 =>        -- delay for the data to write into fifo 
                        frame_cntr_en_i     <= '1' ;    -- increase frame counter by 1
                        current_state       <= idle ;
                                    
                    when others =>
                        NULL;
                end case;
            end if;
            -- register following output signals to remove timing violations
            RAW_out                   <= RAW_out_i ;				-- data out to link output FIFO
            RAW_out_valid             <= RAW_out_valid_i ;            -- data valid to link output FIFO
            RAW_out_is_char           <= RAW_out_is_char_i ;        -- data is CHAR
            frame_cntr_en             <= frame_cntr_en_i ;            -- complete frame in link output fifo
        END IF;           
    end process;

------ generate for ILA  ------

--U4_ila_RAW_error_flags : ila_ipbus_fabric_rd_wr
--PORT MAP (
--    clk    => CLK_280M_i,     -- input clock
--    probe0(23 downto 0) => L1A_ID_in_i,  -- 36b         
--    probe0(31 downto 24) => L1A_ID_EXT_in_i,  -- 36b         
--    probe1(3 downto 0) =>  link_err_4b_in_i,  -- 36b
--    probe1(15 downto 4) =>  BCN_in_i,  -- 36b
--    probe1(31 downto 16) =>  (others => '0'),  -- 36b
--    probe2(0) => ORed_err_flg_i,   -- 1b
--    probe3(0) => valid_BCN_i,     -- 1b
--    probe4 => (others => '0') ,    -- 1b
--    probe5 => channel_error_map_i(31 downto 0),  -- 36b 
--    probe6(16 downto 0) => channel_error_map_i(48 downto 32) ,  -- 36b
--    probe6(31 downto 17) => (others => '0'),     -- 1b
--    probe7 => (others => '0'),     -- 1b
--    probe8(0) => TTC_read_all_in ,    -- 1b
--    probe9(0) => FIFO_RAW_Data_prog_full_i      -- 1b
--);

--U4_ila_FIFO_RAW_Data : ila_ipbus_fabric_rd_wr
--PORT MAP (
--    clk    => CLK_280M_i,     -- input clock
--    probe0 =>  RAW_in_i(0)(31 downto 0),  -- 36b         
--    probe1(3 downto 0) =>  RAW_in_i(0)(35 downto 32),  -- 36b
--    probe1(31 downto 4) => (others => '0'),           -- 1b
--    probe2(0) => RAW_in_valid_i(0),   -- 1b
--    probe3(0) => valid_BCN_i,           -- 1b
--    probe4 => (others => '0') ,   -- 1b
--    probe5 => RAW_in_i(3)(31 downto 0),  -- 36b 
--    probe6(3 downto 0) => RAW_in_i(3)(35 downto 32) ,  -- 36b
--    probe6(31 downto 4) => (others => '0'),           -- 1b
--    probe7(0) => RAW_in_valid_i(3),             -- 1b
--    probe8 => (others => '0') ,          -- 1b
--    probe9 => (others => '0')               -- 1b
--);

end Behavioral;
