----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/09/2018 09:32:00 AM
-- Design Name: 
-- Module Name: pseudo_orbit_gen - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity pseudo_orbit_gen is
  Port (
  clk          :in std_logic;
  bcn          : in std_logic_vector( 4 downto 0 );
  pseudo_orbit : out std_logic
  );
end pseudo_orbit_gen;

architecture Behavioral of pseudo_orbit_gen is

begin

    -- This process check the bcn  and generate  pseudo_orbit if bnc = "11111"
    
    process ( clk)
          begin          
            if clk' event and clk ='1' then
               if (bcn = "11111") then   -- check if bcbn is equal 31
                   pseudo_orbit <= '1';  -- generate single pulse 
                else  
                  pseudo_orbit <= '0';   --- not generate 
               end if;   
               end if;
        end process;       
                  
          
end Behavioral;
