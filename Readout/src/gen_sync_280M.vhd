--! @file
--! @brief Generate Synch at 280MHz
--! @details 
--! \verbatim
--! This module generates a synch signal synchronised to 40MHz and 280MHz clocks
--! one synch pulse is generated - this is 1 in 7 sync'ed to 40M clock
--! \endverbatim
--! \image html 280_40MHz_Synch_Signal.png "280 MHz to 40 MHz Synch Diagram"
--! @author Saeed Taghavi

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library TOB_rdout_lib;

library UNISIM;
use UNISIM.VComponents.all;

--! @copydoc gen_sync_280M.vhd
entity gen_sync_280M is
    Port ( 
        --! Reset in
        RST             : in STD_LOGIC;
        --! Clock 40MHz in 
        clk_40M         : in STD_LOGIC;
        --! Clock 2800MHz in
        clk_280M        : in STD_LOGIC;
        --! 280MHz synch signal output
        sync_280m_out   : out STD_LOGIC
        );
end gen_sync_280M;

--! @copydoc gen_sync_280M.vhd
architecture Behavioral of gen_sync_280M is

    signal LoadR     : std_logic := '0';
    signal LoadF     : std_logic := '0';
    signal Load      : std_logic := '0';

begin
 
   process(clk_40M)
   begin
     if rising_edge(clk_40M) AND RST = '0' then
       LoadR <= not LoadR after 100 ps;
     end if;
   end process;
 
   process(clk_280M)
   begin
     if rising_edge(clk_280M) AND RST = '0' then
       LoadF <= LoadR after 100 ps;
     end if;
   end process;
 
   Load <= LoadR xor LoadF;
 
   sync_280m_out <= Load; 
 
end Behavioral;
