----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/29/2016 03:13:02 PM
-- Design Name: 
-- Module Name: module_information - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;


entity module_information is
generic (addr_width :natural :=3);    
port(
        ipb_clk: in std_logic;
		reset: in std_logic;
		ipbus_in: in ipb_wbus;
		ipbus_out: out ipb_rbus;
         HW_ID       : in std_logic_vector(31 downto 0);
         FPGA_ID     : in std_logic_vector(31 downto 0);
         HW_Position     : in std_logic_vector(31 downto 0);
         FW_Rev      : in std_logic_vector(31 downto 0);
         FW_Tag      : in std_logic_vector(31 downto 0);
         DataFmt_Rev : in std_logic_vector(31 downto 0)
	);
	

end module_information;

architecture Behavioral of module_information is
signal sel:integer;
signal ack: std_logic;

begin

sel <= to_integer(unsigned(ipbus_in.ipb_addr(addr_width-1 downto 0))) when addr_width > 0 else 0;


                 
                 
                                       
 process(ipb_clk,reset)
 
     begin
          if reset ='1' then
             ipbus_out.ipb_rdata <= (others =>'0');
        elsif ipb_clk' event and ipb_clk='1' then
--        if reset ='1' then
--          ipbus_out.ipb_rdata <= (others =>'0');
--      elsif ipbus_in.ipb_strobe= '1' and ipbus_in.ipb_write ='1' then
--             ipbus_out.ipb_rdata <= (others =>'0');
--             end if;
      
      if  sel  = 0 then                
          ipbus_out.ipb_rdata <=  HW_ID;
         ack <= ipbus_in.ipb_strobe and not ack;       
             
          end if;   
             
      if   sel = 1 then
             ipbus_out.ipb_rdata <=  HW_Position;
             ack <= ipbus_in.ipb_strobe and not ack;
             end if;
             
      if   sel = 2 then
             ipbus_out.ipb_rdata <=  FPGA_ID;
             ack <= ipbus_in.ipb_strobe and not ack;
             end if;           
             
       if  sel = 3 then
              ipbus_out.ipb_rdata <=  FW_Rev ;
               ack <= ipbus_in.ipb_strobe and not ack;
             end if;
       
      if  sel = 4 then
             ipbus_out.ipb_rdata <=  FW_Tag ;
              ack <= ipbus_in.ipb_strobe and not ack;
             end if;   
                   
       if  sel  = 5 then
           ipbus_out.ipb_rdata <=  DataFmt_Rev ; 
            ack <= ipbus_in.ipb_strobe and not ack;               
                      
        end if;       
        end if; 
        end process;
       
       ipbus_out.ipb_ack <= ack;
       ipbus_out.ipb_err <= '0';
       
end Behavioral;
