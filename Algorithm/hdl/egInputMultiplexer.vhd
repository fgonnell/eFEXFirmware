--! @file
--! @brief Input Data Multiplexer for eg: addresses SuperCells to the correct sum area
--! @details
--! The selector of this mux is the seed of the cluster (IN_Seed)  plus the phi
--! asimmetry (IN_UpNotDown). According to these values, all the supercells
--! contained in the 9 TTs are addressed to the proper place.
--!
--! This mux is registerd and the latency is 2 clock cycles, to mach the seed
--! finder.
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library work;
use work.DataTypes.all;

--! @copydoc egInputMultiplexer.vhd
entity egInputMultiplexer is
  port (
    CLK : in std_logic;         --!200 MHz clock

    IN_Seed      : in std_logic_vector(1 downto 0);
    IN_UpNotDown : in std_logic;
    IN_Towers    : in TriggerTowers(8 downto 0);

    OUT_EnergyL0 : out DataWords(1 downto 0);
    OUT_EnergyL1 : out DataWords(5 downto 0);
    OUT_EnergyL2 : out DataWords(5 downto 0);
    OUT_EnergyL3 : out DataWords(1 downto 0);

    OUT_REtaCoreData : out DataWords(5 downto 0);
    OUT_REtaEnvData  : out DataWords(14 downto 0);

    OUT_WsCoreData : out DataWords(11 downto 0);
    OUT_WsEnvData  : out DataWords(14 downto 0);

    OUT_HadCoreData   : out DataWords(8 downto 0);
    OUT_HadEnvDataL1  : out DataWords(8 downto 0);
    OUT_HadEnvDataL2  : out DataWords(8 downto 0);
    OUT_HadEnvDataL03 : out DataWords(5 downto 0)
    );
end egInputMultiplexer;

--! @copydoc egInputMultiplexer.vhd
architecture Behavioral of egInputMultiplexer is
  signal Selector     : std_logic_vector(2 downto 0);
  signal ShiftTowers  : TriggerTowers(8 downto 0) := (others => ZERO_TRIGGER_TOWER);
  signal ShiftTowers2 : TriggerTowers(8 downto 0) := (others => ZERO_TRIGGER_TOWER);


  signal OutEnergyL0 : DataWords(OUT_EnergyL0'range) := (others => (others => '0'));
  signal OutEnergyL1 : DataWords(OUT_EnergyL1'range) := (others => (others => '0'));
  signal OutEnergyL2 : DataWords(OUT_EnergyL2'range) := (others => (others => '0'));
  signal OutEnergyL3 : DataWords(OUT_EnergyL3'range) := (others => (others => '0'));

  signal OutREtaCoreData : DataWords(OUT_REtaCoreData'range) := (others => (others => '0'));
  signal OutREtaEnvData  : DataWords(OUT_REtaEnvData'range)  := (others => (others => '0'));

  signal OutWsCoreData : DataWords(OUT_WsCoreData'range) := (others => (others => '0'));
  signal OutWsEnvData  : DataWords(OUT_WsEnvData'range)  := (others => (others => '0'));

  signal OutHadCoreData   : DataWords(OUT_HadCoreData'range)   := (others => (others => '0'));
  signal OutHadEnvDataL1  : DataWords(OUT_HadEnvDataL1'range)  := (others => (others => '0'));
  signal OutHadEnvDataL2  : DataWords(OUT_HadEnvDataL2'range)  := (others => (others => '0'));
  signal OutHadEnvDataL03 : DataWords(OUT_HadEnvDataL03'range) := (others => (others => '0'));


begin

  Selector <= IN_UpNotDown & IN_Seed;

  process (CLK)
  begin
    if rising_edge(CLK) then
      -- Pipeline (a delay of 2 is needed to mach the SeedFinder)
      ShiftTowers2 <= IN_Towers;
      ShiftTowers  <= ShiftTowers2;

      -- Multiplexer

      -------------------------------------------------------------------------------
      -- Energy L0
      -------------------------------------------------------------------------------


      case IN_UpNotDown is
        -- Up
        when '1' =>             --up
          OutEnergyL0 <= (0 => ShiftTowers(4).Layer0(0), 1 => ShiftTowers(7).Layer0(0));

        when others =>          --down
          OutEnergyL0 <= (0 => ShiftTowers(4).Layer0(0), 1 => ShiftTowers(1).Layer0(0));
      end case;

      -------------------------------------------------------------------------------
      -- Energy L1
      -------------------------------------------------------------------------------
      case Selector is
        when "100" =>           --up
          OutEnergyL1 <= (1 downto 0 => ShiftTowers(4).Layer1(1 downto 0), 2 => ShiftTowers(3).Layer1(3),
                          4 downto 3 => ShiftTowers(7).Layer1(1 downto 0), 5 => ShiftTowers(6).Layer1(3));
        when "101" =>
          OutEnergyL1 <= (2 downto 0 => ShiftTowers(4).Layer1(2 downto 0),
                          5 downto 3 => ShiftTowers(7).Layer1(2 downto 0));
        when "110" =>
          OutEnergyL1 <= (2 downto 0 => ShiftTowers(4).Layer1(3 downto 1),
                          5 downto 3 => ShiftTowers(7).Layer1(3 downto 1));

        when "111" =>          --111
          OutEnergyL1 <= (1 downto 0 => ShiftTowers(4).Layer1(3 downto 2), 2 => ShiftTowers(5).Layer1(0),
                          4 downto 3 => ShiftTowers(7).Layer1(3 downto 2), 5 => ShiftTowers(8).Layer1(0));

        when "000" =>           --down
          OutEnergyL1 <= (1 downto 0 => ShiftTowers(4).Layer1(1 downto 0), 2 => ShiftTowers(3).Layer1(3),
                          4 downto 3 => ShiftTowers(1).Layer1(1 downto 0), 5 => ShiftTowers(0).Layer1(3));
        when "001" =>
          OutEnergyL1 <= (2 downto 0 => ShiftTowers(4).Layer1(2 downto 0),
                          5 downto 3 => ShiftTowers(1).Layer1(2 downto 0));
        when "010" =>
          OutEnergyL1 <= (2 downto 0 => ShiftTowers(4).Layer1(3 downto 1),
                          5 downto 3 => ShiftTowers(1).Layer1(3 downto 1));

        when others =>          --011
          OutEnergyL1 <= (1 downto 0 => ShiftTowers(4).Layer1(3 downto 2), 2 => ShiftTowers(5).Layer1(0),
                          4 downto 3 => ShiftTowers(1).Layer1(3 downto 2), 5 => ShiftTowers(2).Layer1(0));

      end case;

      -------------------------------------------------------------------------------
      -- Energy L2
      -------------------------------------------------------------------------------
      case Selector is
        when "100" =>           --up
          OutEnergyL2 <= (1 downto 0 => ShiftTowers(4).Layer2(1 downto 0), 2 => ShiftTowers(3).Layer2(3),
                          4 downto 3 => ShiftTowers(7).Layer2(1 downto 0), 5 => ShiftTowers(6).Layer2(3));
        when "101" =>
          OutEnergyL2 <= (2 downto 0 => ShiftTowers(4).Layer2(2 downto 0),
                          5 downto 3 => ShiftTowers(7).Layer2(2 downto 0));
        when "110" =>
          OutEnergyL2 <= (2 downto 0 => ShiftTowers(4).Layer2(3 downto 1),
                          5 downto 3 => ShiftTowers(7).Layer2(3 downto 1));

        when "111" =>          --111
          OutEnergyL2 <= (1 downto 0 => ShiftTowers(4).Layer2(3 downto 2), 2 => ShiftTowers(5).Layer2(0),
                          4 downto 3 => ShiftTowers(7).Layer2(3 downto 2), 5 => ShiftTowers(8).Layer2(0));

        when "000" =>           --down
          OutEnergyL2 <= (1 downto 0 => ShiftTowers(4).Layer2(1 downto 0), 2 => ShiftTowers(3).Layer2(3),
                          4 downto 3 => ShiftTowers(1).Layer2(1 downto 0), 5 => ShiftTowers(0).Layer2(3));
        when "001" =>
          OutEnergyL2 <= (2 downto 0 => ShiftTowers(4).Layer2(2 downto 0),
                          5 downto 3 => ShiftTowers(1).Layer2(2 downto 0));
        when "010" =>
          OutEnergyL2 <= (2 downto 0 => ShiftTowers(4).Layer2(3 downto 1),
                          5 downto 3 => ShiftTowers(1).Layer2(3 downto 1));

        when others =>          --011
          OutEnergyL2 <= (1 downto 0 => ShiftTowers(4).Layer2(3 downto 2), 2 => ShiftTowers(5).Layer2(0),
                          4 downto 3 => ShiftTowers(1).Layer2(3 downto 2), 5 => ShiftTowers(2).Layer2(0));
      end case;

      -----------------------------------------------------------------------
      -- Energy L3
      -----------------------------------------------------------------------

      case IN_UpNotDown is
        -- Up
        when '1' =>             --up
          OutEnergyL3 <= (0 => ShiftTowers(4).Layer3(0), 1 => ShiftTowers(7).Layer3(0));

        when others =>          --down
          OutEnergyL3 <= (0 => ShiftTowers(4).Layer3(0), 1 => ShiftTowers(1).Layer3(0));

      end case;


      -----------------------------------------------------------------------
      -- REta Core Data
      -----------------------------------------------------------------------
      case Selector is
        -- Up
        when "100" =>           --up
          OutREtaCoreData <= (0 => ShiftTowers(4).Layer2(1), 1 => ShiftTowers(4).Layer2(0), 2 => ShiftTowers(3).Layer2(3),
                              3 => ShiftTowers(7).Layer2(1), 4 => ShiftTowers(7).Layer2(0), 5 => ShiftTowers(6).Layer2(3));
        when "101" =>
          OutREtaCoreData <= (0 => ShiftTowers(4).Layer2(2), 1 => ShiftTowers(4).Layer2(1), 2 => ShiftTowers(4).Layer2(0),
                              3 => ShiftTowers(7).Layer2(2), 4 => ShiftTowers(7).Layer2(1), 5 => ShiftTowers(6).Layer2(0));
        when "110" =>
          OutREtaCoreData <= (0 => ShiftTowers(4).Layer2(3), 1 => ShiftTowers(4).Layer2(2), 2 => ShiftTowers(4).Layer2(1),
                              3 => ShiftTowers(7).Layer2(3), 4 => ShiftTowers(7).Layer2(2), 5 => ShiftTowers(7).Layer2(1));
        when "111" =>
          OutREtaCoreData <= (0 => ShiftTowers(5).Layer2(0), 1 => ShiftTowers(4).Layer2(3), 2 => ShiftTowers(4).Layer2(2),
                              3 => ShiftTowers(8).Layer2(0), 4 => ShiftTowers(7).Layer2(3), 5 => ShiftTowers(7).Layer2(2));
        when "000" =>           --down
          OutREtaCoreData <= (0 => ShiftTowers(4).Layer2(1), 1 => ShiftTowers(4).Layer2(0), 2 => ShiftTowers(3).Layer2(3),
                              3 => ShiftTowers(1).Layer2(1), 4 => ShiftTowers(1).Layer2(0), 5 => ShiftTowers(0).Layer2(3));
        when "001" =>
          OutREtaCoreData <= (0 => ShiftTowers(4).Layer2(2), 1 => ShiftTowers(4).Layer2(1), 2 => ShiftTowers(4).Layer2(0),
                              3 => ShiftTowers(1).Layer2(2), 4 => ShiftTowers(1).Layer2(1), 5 => ShiftTowers(1).Layer2(0));
        when "010" =>
          OutREtaCoreData <= (0 => ShiftTowers(4).Layer2(3), 1 => ShiftTowers(4).Layer2(2), 2 => ShiftTowers(4).Layer2(1),
                              3 => ShiftTowers(1).Layer2(3), 4 => ShiftTowers(1).Layer2(2), 5 => ShiftTowers(1).Layer2(1));
        when others =>          -- 011
          OutREtaCoreData <= (0 => ShiftTowers(5).Layer2(0), 1 => ShiftTowers(4).Layer2(3), 2 => ShiftTowers(4).Layer2(2),
                              3 => ShiftTowers(2).Layer2(0), 4 => ShiftTowers(1).Layer2(3), 5 => ShiftTowers(1).Layer2(2));
      end case;


      -------------------------------------------------------------------------------
      -- REta Env Data
      -------------------------------------------------------------------------------
      case Selector is
        -- Up
        when "100" =>           --up
          OutREtaEnvData <= (0            => ShiftTowers(4).Layer2(3), 1 => ShiftTowers(4).Layer2(2), 2 => ShiftTowers(3).Layer2(2), 3 => ShiftTowers(3).Layer2(1),
                             4            => ShiftTowers(7).Layer2(3), 5 => ShiftTowers(7).Layer2(2), 6 => ShiftTowers(6).Layer2(2), 7 => ShiftTowers(6).Layer2(1),
                             8            => ShiftTowers(0).Layer2(1), 9 => ShiftTowers(0).Layer2(2), 10 => ShiftTowers(0).Layer2(3),
                             14 downto 11 => ShiftTowers(1).Layer2);

        when "101" =>
          OutREtaEnvData <= (0            => ShiftTowers(5).Layer2(0), 1 => ShiftTowers(4).Layer2(3), 2 => ShiftTowers(3).Layer2(3), 3 => ShiftTowers(3).Layer2(2),
                             4            => ShiftTowers(8).Layer2(0), 5 => ShiftTowers(7).Layer2(3), 6 => ShiftTowers(6).Layer2(3), 7 => ShiftTowers(6).Layer2(2),
                             8            => ShiftTowers(2).Layer2(0), 9 => ShiftTowers(0).Layer2(2), 10 => ShiftTowers(0).Layer2(3),
                             14 downto 11 => ShiftTowers(1).Layer2);
        when "110" =>
          OutREtaEnvData <= (0            => ShiftTowers(5).Layer2(1), 1 => ShiftTowers(5).Layer2(0), 2 => ShiftTowers(4).Layer2(0), 3 => ShiftTowers(3).Layer2(1),
                             4            => ShiftTowers(8).Layer2(1), 5 => ShiftTowers(8).Layer2(0), 6 => ShiftTowers(7).Layer2(0), 7 => ShiftTowers(6).Layer2(1),
                             8            => ShiftTowers(2).Layer2(0), 9 => ShiftTowers(2).Layer2(1), 10 => ShiftTowers(0).Layer2(3),
                             14 downto 11 => ShiftTowers(1).Layer2);
        when "111" =>
          OutREtaEnvData <= (0            => ShiftTowers(5).Layer2(2), 1 => ShiftTowers(5).Layer2(1), 2 => ShiftTowers(4).Layer2(1), 3 => ShiftTowers(4).Layer2(0),
                             4            => ShiftTowers(8).Layer2(2), 5 => ShiftTowers(8).Layer2(1), 6 => ShiftTowers(7).Layer2(1), 7 => ShiftTowers(7).Layer2(0),
                             8            => ShiftTowers(2).Layer2(0), 9 => ShiftTowers(2).Layer2(1), 10 => ShiftTowers(2).Layer2(2),
                             14 downto 11 => ShiftTowers(1).Layer2);

        when "000" =>           --down
          OutREtaEnvData <= (0            => ShiftTowers(4).Layer2(3), 1 => ShiftTowers(4).Layer2(2), 2 => ShiftTowers(3).Layer2(2), 3 => ShiftTowers(3).Layer2(1),
                             4            => ShiftTowers(1).Layer2(3), 5 => ShiftTowers(1).Layer2(2), 6 => ShiftTowers(0).Layer2(2), 7 => ShiftTowers(0).Layer2(2),
                             8            => ShiftTowers(6).Layer2(1), 9 => ShiftTowers(6).Layer2(2), 10 => ShiftTowers(6).Layer2(3),
                             14 downto 11 => ShiftTowers(7).Layer2);
        when "001" =>
          OutREtaEnvData <= (0            => ShiftTowers(5).Layer2(1), 1 => ShiftTowers(5).Layer2(0), 2 => ShiftTowers(4).Layer2(0), 3 => ShiftTowers(3).Layer2(1),
                             4            => ShiftTowers(2).Layer2(1), 5 => ShiftTowers(2).Layer2(0), 6 => ShiftTowers(1).Layer2(0), 7 => ShiftTowers(0).Layer2(1),
                             8            => ShiftTowers(8).Layer2(0), 9 => ShiftTowers(6).Layer2(2), 10 => ShiftTowers(6).Layer2(3),
                             14 downto 11 => ShiftTowers(7).Layer2);

        when "010" =>
          OutREtaEnvData <= (0            => ShiftTowers(5).Layer2(1), 1 => ShiftTowers(5).Layer2(0), 2 => ShiftTowers(4).Layer2(0), 3 => ShiftTowers(3).Layer2(1),
                             4            => ShiftTowers(2).Layer2(1), 5 => ShiftTowers(2).Layer2(0), 6 => ShiftTowers(1).Layer2(0), 7 => ShiftTowers(0).Layer2(1),
                             8            => ShiftTowers(8).Layer2(0), 9 => ShiftTowers(8).Layer2(1), 10 => ShiftTowers(6).Layer2(3),
                             14 downto 11 => ShiftTowers(7).Layer2);


        when others =>          -- 011
          OutREtaEnvData <= (0            => ShiftTowers(5).Layer2(2), 1 => ShiftTowers(5).Layer2(1), 2 => ShiftTowers(4).Layer2(1), 3 => ShiftTowers(4).Layer2(0),
                             4            => ShiftTowers(2).Layer2(2), 5 => ShiftTowers(2).Layer2(1), 6 => ShiftTowers(1).Layer2(1), 7 => ShiftTowers(1).Layer2(0),
                             8            => ShiftTowers(8).Layer2(1), 9 => ShiftTowers(8).Layer2(2), 10 => ShiftTowers(8).Layer2(3),
                             14 downto 11 => ShiftTowers(7).Layer2);
      end case;

      -------------------------------------------------------------------------------
      -- Ws Core Data
      -------------------------------------------------------------------------------
      case IN_Seed is
        --up
        when "00" =>
          OutWsCoreData <= (0 => BitLeftShift(ShiftTowers(3).Layer2(2), 2), 1 => ShiftTowers(3).Layer2(3), 2 => ShiftTowers(4).Layer2(1), 3 => BitLeftShift(ShiftTowers(4).Layer2(2), 2),
                            4 => BitLeftShift(ShiftTowers(6).Layer2(2), 2), 5 => ShiftTowers(6).Layer2(3), 6 => ShiftTowers(7).Layer2(1), 7 => BitLeftShift(ShiftTowers(7).Layer2(2), 2),
                            8 => BitLeftShift(ShiftTowers(0).Layer2(2), 2), 9 => ShiftTowers(0).Layer2(3), 10 => ShiftTowers(1).Layer2(1), 11 => BitLeftShift(ShiftTowers(1).Layer2(2), 2));

        when "01" =>
          OutWsCoreData <= (0 => BitLeftShift(ShiftTowers(3).Layer2(3), 2), 1 => ShiftTowers(4).Layer2(0), 2 => ShiftTowers(4).Layer2(2), 3 => BitLeftShift(ShiftTowers(4).Layer2(3), 2),
                            4 => BitLeftShift(ShiftTowers(6).Layer2(3), 2), 5 => ShiftTowers(7).Layer2(0), 6 => ShiftTowers(7).Layer2(2), 7 => BitLeftShift(ShiftTowers(7).Layer2(3), 2),
                            8 => BitLeftShift(ShiftTowers(0).Layer2(3), 2), 9 => ShiftTowers(1).Layer2(0), 10 => ShiftTowers(1).Layer2(2), 11 => BitLeftShift(ShiftTowers(1).Layer2(3), 2));
        when "10" =>
          OutWsCoreData <= (0 => BitLeftShift(ShiftTowers(4).Layer2(0), 2), 1 => ShiftTowers(4).Layer2(1), 2 => ShiftTowers(4).Layer2(3), 3 => BitLeftShift(ShiftTowers(5).Layer2(0), 2),
                            4 => BitLeftShift(ShiftTowers(7).Layer2(0), 2), 5 => ShiftTowers(7).Layer2(1), 6 => ShiftTowers(7).Layer2(3), 7 => BitLeftShift(ShiftTowers(8).Layer2(0), 2),
                            8 => BitLeftShift(ShiftTowers(1).Layer2(0), 2), 9 => ShiftTowers(1).Layer2(1), 10 => ShiftTowers(1).Layer2(3), 11 => BitLeftShift(ShiftTowers(2).Layer2(0), 2));
        when others =>  -- "11"
          OutWsCoreData <= (0 => BitLeftShift(ShiftTowers(4).Layer2(1), 2), 1 => ShiftTowers(4).Layer2(2), 2 => ShiftTowers(5).Layer2(0), 3 => BitLeftShift(ShiftTowers(5).Layer2(1), 2),
                            4 => BitLeftShift(ShiftTowers(7).Layer2(1), 2), 5 => ShiftTowers(7).Layer2(2), 6 => ShiftTowers(8).Layer2(0), 7 => BitLeftShift(ShiftTowers(8).Layer2(1), 2),
                            8 => BitLeftShift(ShiftTowers(1).Layer2(1), 2), 9 => ShiftTowers(1).Layer2(2), 10 => ShiftTowers(2).Layer2(0), 11 => BitLeftShift(ShiftTowers(2).Layer2(1), 2));

      end case;

      -------------------------------------------------------------------------------
      -- Ws Env Data
      -------------------------------------------------------------------------------
      case IN_Seed is
        when "00" =>
          OutWsEnvData <= (0  => ShiftTowers(3).Layer2(2), 1 => ShiftTowers(3).Layer2(3), 2 => ShiftTowers(4).Layer2(0),
                           3  => ShiftTowers(4).Layer2(1), 4 => ShiftTowers(4).Layer2(2),
                           5  => ShiftTowers(6).Layer2(2), 6 => ShiftTowers(6).Layer2(3), 7 => ShiftTowers(7).Layer2(0),
                           8  => ShiftTowers(7).Layer2(1), 9 => ShiftTowers(7).Layer2(2),
                           10 => ShiftTowers(0).Layer2(2), 11 => ShiftTowers(0).Layer2(3), 12 => ShiftTowers(1).Layer2(0),
                           13 => ShiftTowers(1).Layer2(1), 14 => ShiftTowers(1).Layer2(2)
                           );
        when "01" =>
          OutWsEnvData <= (0  => ShiftTowers(3).Layer2(3), 1 => ShiftTowers(4).Layer2(0), 2 => ShiftTowers(4).Layer2(1),
                           3  => ShiftTowers(4).Layer2(2), 4 => ShiftTowers(4).Layer2(3),
                           5  => ShiftTowers(6).Layer2(3), 6 => ShiftTowers(7).Layer2(0), 7 => ShiftTowers(7).Layer2(1),
                           8  => ShiftTowers(7).Layer2(2), 9 => ShiftTowers(7).Layer2(3),
                           10 => ShiftTowers(0).Layer2(3), 11 => ShiftTowers(1).Layer2(0), 12 => ShiftTowers(1).Layer2(1),
                           13 => ShiftTowers(1).Layer2(2), 14 => ShiftTowers(1).Layer2(3)
                           );
        when "10" =>
          OutWsEnvData <= (0  => ShiftTowers(4).Layer2(0), 1 => ShiftTowers(4).Layer2(1), 2 => ShiftTowers(4).Layer2(2),
                           3  => ShiftTowers(4).Layer2(3), 4 => ShiftTowers(5).Layer2(0),
                           5  => ShiftTowers(7).Layer2(0), 6 => ShiftTowers(7).Layer2(1), 7 => ShiftTowers(7).Layer2(2),
                           8  => ShiftTowers(7).Layer2(3), 9 => ShiftTowers(8).Layer2(0),
                           10 => ShiftTowers(1).Layer2(0), 11 => ShiftTowers(1).Layer2(1), 12 => ShiftTowers(1).Layer2(2),
                           13 => ShiftTowers(1).Layer2(3), 14 => ShiftTowers(2).Layer2(0)
                           );
        when others =>
          OutWsEnvData <= (0  => ShiftTowers(4).Layer2(1), 1 => ShiftTowers(4).Layer2(2), 2 => ShiftTowers(4).Layer2(3),
                           3  => ShiftTowers(5).Layer2(0), 4 => ShiftTowers(5).Layer2(1),
                           5  => ShiftTowers(7).Layer2(1), 6 => ShiftTowers(7).Layer2(2), 7 => ShiftTowers(7).Layer2(3),
                           8  => ShiftTowers(8).Layer2(0), 9 => ShiftTowers(8).Layer2(1),
                           10 => ShiftTowers(1).Layer2(1), 11 => ShiftTowers(1).Layer2(2), 12 => ShiftTowers(1).Layer2(3),
                           13 => ShiftTowers(2).Layer2(0), 14 => ShiftTowers(2).Layer2(1)
                           );
      end case;


      -------------------------------------------------------------------------------
      -- HadCore Data
      -------------------------------------------------------------------------------
      OutHadCoreData <= (6 => ShiftTowers(6).Hadron(0), 7 => ShiftTowers(7).Hadron(0), 8 => ShiftTowers(8).Hadron(0),
                         3 => ShiftTowers(3).Hadron(0), 4 => ShiftTowers(4).Hadron(0), 5 => ShiftTowers(5).Hadron(0),
                         0 => ShiftTowers(0).Hadron(0), 1 => ShiftTowers(1).Hadron(0), 2 => ShiftTowers(2).Hadron(0));

      -------------------------------------------------------------------------------
      -- HadCore Env L1
      -------------------------------------------------------------------------------
      case IN_Seed is
        when "00" =>
          OutHadEnvDataL1 <= (6 => ShiftTowers(6).Layer1(3), 7 => ShiftTowers(7).Layer1(0), 8 => ShiftTowers(7).Layer1(1),
                              3 => ShiftTowers(3).Layer1(3), 4 => ShiftTowers(4).Layer1(0), 5 => ShiftTowers(4).Layer1(1),
                              0 => ShiftTowers(0).Layer1(3), 1 => ShiftTowers(1).Layer1(0), 2 => ShiftTowers(1).Layer1(1));

        when "01" =>
          OutHadEnvDataL1 <= (6 => ShiftTowers(7).Layer1(0), 7 => ShiftTowers(7).Layer1(1), 8 => ShiftTowers(7).Layer1(2),
                              3 => ShiftTowers(4).Layer1(0), 4 => ShiftTowers(4).Layer1(1), 5 => ShiftTowers(4).Layer1(2),
                              0 => ShiftTowers(1).Layer1(0), 1 => ShiftTowers(1).Layer1(1), 2 => ShiftTowers(1).Layer1(2));
        when "10" =>
          OutHadEnvDataL1 <= (6 => ShiftTowers(7).Layer1(1), 7 => ShiftTowers(7).Layer1(2), 8 => ShiftTowers(7).Layer1(3),
                              3 => ShiftTowers(4).Layer1(1), 4 => ShiftTowers(4).Layer1(2), 5 => ShiftTowers(4).Layer1(3),
                              0 => ShiftTowers(1).Layer1(1), 1 => ShiftTowers(1).Layer1(2), 2 => ShiftTowers(1).Layer1(3));

        when others =>          --"11"
          OutHadEnvDataL1 <= (6 => ShiftTowers(8).Layer1(0), 7 => ShiftTowers(7).Layer1(2), 8 => ShiftTowers(7).Layer1(3),
                              3 => ShiftTowers(5).Layer1(0), 4 => ShiftTowers(4).Layer1(2), 5 => ShiftTowers(4).Layer1(3),
                              0 => ShiftTowers(2).Layer1(0), 1 => ShiftTowers(1).Layer1(2), 2 => ShiftTowers(1).Layer1(3));
      end case;

      -------------------------------------------------------------------------------
      -- HadCore Env L2
      -------------------------------------------------------------------------------
      case IN_Seed is
        when "00" =>
          OutHadEnvDataL2 <= (6 => ShiftTowers(6).Layer2(3), 7 => ShiftTowers(7).Layer2(0), 8 => ShiftTowers(7).Layer2(1),
                              3 => ShiftTowers(3).Layer2(3), 4 => ShiftTowers(4).Layer2(0), 5 => ShiftTowers(4).Layer2(1),
                              0 => ShiftTowers(0).Layer2(3), 1 => ShiftTowers(1).Layer2(0), 2 => ShiftTowers(1).Layer2(1));

        when "01" =>
          OutHadEnvDataL2 <= (6 => ShiftTowers(7).Layer2(0), 7 => ShiftTowers(7).Layer2(1), 8 => ShiftTowers(7).Layer2(2),
                              3 => ShiftTowers(4).Layer2(0), 4 => ShiftTowers(4).Layer2(1), 5 => ShiftTowers(4).Layer2(2),
                              0 => ShiftTowers(1).Layer2(0), 1 => ShiftTowers(1).Layer2(1), 2 => ShiftTowers(1).Layer2(2));
        when "10" =>
          OutHadEnvDataL2 <= (6 => ShiftTowers(7).Layer2(1), 7 => ShiftTowers(7).Layer2(2), 8 => ShiftTowers(7).Layer2(3),
                              3 => ShiftTowers(4).Layer2(1), 4 => ShiftTowers(4).Layer2(2), 5 => ShiftTowers(4).Layer2(3),
                              0 => ShiftTowers(1).Layer2(1), 1 => ShiftTowers(1).Layer2(2), 2 => ShiftTowers(1).Layer2(3));

        when others =>          --"11"
          OutHadEnvDataL2 <= (6 => ShiftTowers(8).Layer2(0), 7 => ShiftTowers(7).Layer2(2), 8 => ShiftTowers(7).Layer2(3),
                              3 => ShiftTowers(5).Layer2(0), 4 => ShiftTowers(4).Layer2(2), 5 => ShiftTowers(4).Layer2(3),
                              0 => ShiftTowers(2).Layer2(0), 1 => ShiftTowers(1).Layer2(2), 2 => ShiftTowers(1).Layer2(3));
      end case;

    end if;
  end process;
      -----------------------------------------------------------------------
      -- HadCore Env L0 and L3
      -----------------------------------------------------------------------
      OutHadEnvDataL03 <= (3 => ShiftTowers(1).Layer3(0), 4 => ShiftTowers(4).Layer3(0), 5 => ShiftTowers(7).Layer3(0),
                           0 => ShiftTowers(1).Layer0(0), 1 => ShiftTowers(4).Layer0(0), 2 => ShiftTowers(7).Layer0(0));

      OUT_EnergyL0 <= OutEnergyL0;
      OUT_EnergyL1 <= OutEnergyL1;
      OUT_EnergyL2 <= OutEnergyL2;
      OUT_EnergyL3 <= OutEnergyL3;

      OUT_REtaCoreData <= OutREtaCoreData;
      OUT_REtaEnvData  <= OutREtaEnvData;

      OUT_WsCoreData <= OutWsCoreData;
      OUT_WsEnvData  <= OutWsEnvData;

      OUT_HadCoreData   <= OutHadCoreData;
      OUT_HadEnvDataL1  <= OutHadEnvDataL1;
      OUT_HadEnvDataL2  <= OutHadEnvDataL2;
      OUT_HadEnvDataL03 <= OutHadEnvDataL03;


    end Behavioral;
