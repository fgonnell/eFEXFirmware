--! @file
--! @brief Core of the electromagnetic algorithm
--! @details
--! The total latency of thius block is 10 clock cycles.
--! The parameter used to evaluate the conditions are used in clock cycle 6
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
use work.DataTypes.all;

library infrastructure_lib;

--! @copydoc AlgoCore_eg.vhd
entity AlgoCore_eg is
  port (CLK200 : in std_logic;

        IN_ParWs     : in AlgoParameters(2 downto 0);
        IN_ParREta   : in AlgoParameters(2 downto 0);
        IN_ParHadron : in AlgoParameters(2 downto 0);

        IN_Energy_threshold : in DataWord;
        IN_Reta_threshold   : in DataWord;
        IN_Ws_threshold     : in DataWord;
        IN_Had_threshold    : in DataWord;

        IN_Control : in  AlgoRegister;
        OUT_Status : out AlgoRegister;

        IN_Data : in TriggerTowers(8 downto 0);

        OUT_TOB : out TriggerObject_eg);
end AlgoCore_eg;

--! @copydoc AlgoCore_eg.vhd
architecture Behavioral of AlgoCore_eg is

  component Mult
    port (
      CLK : in  std_logic;
      A   : in  std_logic_vector(15 downto 0);
      B   : in  std_logic_vector(7 downto 0);
      P   : out std_logic_vector(23 downto 0)
      );
  end component;

  signal ParREta : AlgoParameters(2 downto 0);
  signal ParWs   : AlgoParameters(2 downto 0);
  signal ParRh   : AlgoParameters(2 downto 0);

-- Seed Finder
  signal SF_Data       : DataWords(5 downto 0);
  signal SF_DataUp     : DataWords(5 downto 0);
  signal SF_DataDown   : DataWords(5 downto 0);
  signal SF_UpNotDown  : std_logic;
  signal SF_Seed       : std_logic_vector(1 downto 0);
  signal SF_IsLocalMax : std_logic;
  signal SF_IsMax      : std_logic;

-- Input Multiplexer
  signal IM_Towers        : TriggerTowers(8 downto 0);
  signal IM_EnergyL0      : DataWords(1 downto 0);
  signal IM_EnergyL1      : DataWords(5 downto 0);
  signal IM_EnergyL2      : DataWords(5 downto 0);
  signal IM_EnergyL3      : DataWords(1 downto 0);
  signal IM_REtaCoreData  : DataWords(5 downto 0);
  signal IM_REtaEnvData   : DataWords(14 downto 0);
  signal IM_WsCoreData    : DataWords(11 downto 0);
  signal IM_WsEnvData     : DataWords(14 downto 0);
  signal IM_HadEnvDataL1  : DataWords(8 downto 0);
  signal IM_HadEnvDataL2  : DataWords(8 downto 0);
  signal IM_HadEnvDataL03 : DataWords(5 downto 0);
  signal IM_HadCoreData   : DataWords(8 downto 0);

-- Multi Adders
  signal MA_EnergyOverflow : std_logic;
  signal MA_EnergySum      : DataWord;

  signal MA_REtaEnvOverflow  : std_logic;
  signal MA_REtaEnvSum       : DataWord;
  signal MA_REtaCoreOverflow : std_logic;
  signal MA_REtaCoreSum      : DataWord;

  signal MA_WsEnvOverflow  : std_logic;
  signal MA_WsEnvSum       : DataWord;
  signal MA_WsCoreOverflow : std_logic;
  signal MA_WsCoreSum      : DataWord;

  signal MA_HadCoreOverflow : std_logic;
  signal MA_HadCoreSum      : DataWord;
  signal MA_HadEnvOverflow  : std_logic;
  signal MA_HadEnvSum       : DataWord;

-- Multipliers
  signal MU_REtaEnvOverflows : std_logic_vector(2 downto 0);
  signal MU_WsCoreOverflows  : std_logic_vector(2 downto 0);
  signal MU_HadCoreOverflows : std_logic_vector(2 downto 0);

  signal MU_REtaEnvMult : DataWords(2 downto 0);
  signal MU_WsCoreMult  : DataWords(2 downto 0);
  signal MU_HadCoreMult : DataWords(2 downto 0);

-- Delays
  signal DL_SeedFinder : std_logic_vector(4 downto 0);
  signal DL_UpNotDown  : std_logic;
  signal DL_Seed       : std_logic_vector(1 downto 0);
  signal DL_IsLocalMax : std_logic;
  signal DL_IsMax      : std_logic;

  signal DL_Overflows       : std_logic_vector(2 downto 0);
  signal DL_REtaEnvOverflow : std_logic;
  signal DL_WsCoreOverflow  : std_logic;
  signal DL_HadCoreOverflow : std_logic;

-- Conditions
  signal REtaCondition     : std_logic_vector(1 downto 0) := (others => '0');
  signal WsCondition       : std_logic_vector(1 downto 0) := (others => '0');
  signal HadCondition      : std_logic_vector(1 downto 0) := (others => '0');
  signal TOBEnergy         : DataWord                     := (others => '0');
  signal TOBEnergyOverflow : std_logic                    := '0';

  signal RetaThr, WsThr, HadThr       : DataWord;
  signal RetaThr_d, WsThr_d, HadThr_d : DataWord;
  signal EnergyThr, EnergyThr_d       : DataWord;
begin
-- INPUT DATA mapping
--
-- +-------+-------+-------+
-- |   6   |   7   |   8   |
-- |       |0 1 2 3|       |
-- |       |* * * *|       |
-- +-------+-------+-------+
-- |   3   |   4   |   5   |
-- |0 1 2 3|0 1 2 3|0 1 2 3|
-- |      %|^ ^ ^ ^|%      |
-- +-------+-------+-------+
-- |       |0 1 2 3|       |
-- |       |* * * *|       |
-- |   0   |   1   |   2   |
-- +-------+-------+-------+
--
-- ^ Used in seed finder as CORE => data(4:1)
-- % Used in seed finder as ENVIRONMENT => data(0) and data (5)
-- * Used in up or down selection

  SF_Data(4 downto 1) <= IN_Data(4).Layer2;
  SF_Data(0)          <= IN_Data(3).Layer2(3);
  SF_Data(5)          <= IN_Data(5).Layer2(0);

  SF_DataUp(0)          <= IN_Data(6).Layer2(3);
  SF_DataUp(4 downto 1) <= IN_Data(7).Layer2;
  SF_DataUp(5)          <= IN_Data(8).Layer2(0);

  SF_DataDown(0)          <= IN_Data(0).Layer2(3);
  SF_DataDown(4 downto 1) <= IN_Data(1).Layer2;
  SF_DataDown(5)          <= IN_Data(2).Layer2(0);

  IM_Towers <= IN_Data;

-- Parameters are connected to external IPBus registers
  ParWs   <= IN_ParWs;
  ParREta <= IN_ParREta;
  ParRh   <= IN_ParHadron;

  EnergyThr <= IN_Energy_threshold;
  RetaThr   <= IN_Reta_threshold;
  HadThr    <= IN_Had_threshold;
  WsThr     <= IN_Ws_threshold;

  SEED_FINDER : entity work.SeedFinder  --latency 2
    port map (
      CLK            => CLK200,
      IN_Data        => SF_Data,
      IN_DataUp      => SF_DataUp,
      IN_DataDown    => SF_DataDown,
      OUT_UpNotDown  => SF_UpNotDown,
      OUT_Seed       => SF_Seed,
      OUT_IsLocalMax => SF_IsLocalMax,
      OUT_IsMax      => SF_IsMax
      );

  INPUT_MULTIPLEXER : entity work.egInputMultiplexer  --latency 2
    port map (
      CLK               => CLK200,
      IN_Seed           => SF_Seed,
      IN_UpNotDown      => SF_UpNotDown,
      IN_Towers         => IM_Towers,
      OUT_EnergyL0      => IM_EnergyL0,
      OUT_EnergyL1      => IM_EnergyL1,
      OUT_EnergyL2      => IM_EnergyL2,
      OUT_EnergyL3      => IM_EnergyL3,
      OUT_REtaCoreData  => IM_REtaCoreData,
      OUT_REtaEnvData   => IM_REtaEnvData,
      OUT_WsCoreData    => IM_WsCoreData,
      OUT_WsEnvData     => IM_WsEnvData,
      OUT_HadCoreData   => IM_HadCoreData,
      OUT_HadEnvDataL1  => IM_HadEnvDataL1,
      OUT_HadEnvDataL2  => IM_HadEnvDataL2,
      OUT_HadEnvDataL03 => IM_HadEnvDataL03);

-------------------------------------------------------------------------------
-- ADDERS
-------------------------------------------------------------------------------

-- Energy
  MULTI_ADDER_ENERGY : entity work.MultiAdder
    generic map (
      stage => 4,
      delay => 3)
    port map (
      CLK          => CLK200,
      IN_Words     => IM_EnergyL0 & IM_EnergyL1&IM_EnergyL2&IM_EnergyL3,  --16
      OUT_Overflow => MA_EnergyOverflow,
      OUT_Word     => MA_EnergySum);

-- REta
  MULTI_ADDER_RETA_ENV : entity work.MultiAdder
    generic map (
      stage => 4,
      delay => 0)
    port map (
      CLK          => CLK200,
      IN_Words     => IM_REtaEnvData & ZERO_DATA_WORD,  -- 15+1
      OUT_Overflow => MA_REtaEnvOverflow,
      OUT_Word     => MA_REtaEnvSum);

  MULTI_ADDER_RETA_CORE : entity work.MultiAdder
    generic map (
      stage => 3,
      delay => 4)
    port map (
      CLK          => CLK200,
      IN_Words     => IM_REtaCoreData & ZERO_DATA_WORD&ZERO_DATA_WORD,  -- 6+2
      OUT_Overflow => MA_REtaCoreOverflow,
      OUT_Word     => MA_REtaCoreSum);

--Ws
  MULTI_ADDER_WS_CORE : entity work.MultiAdder
    generic map (
      stage => 4,
      delay => 0)
    port map (
      CLK          => CLK200,
      IN_Words     => IM_WsCoreData&ZERO_DATA_WORD&ZERO_DATA_WORD&ZERO_DATA_WORD&ZERO_DATA_WORD,  --12+4
      OUT_Overflow => MA_WsCoreOverflow,
      OUT_Word     => MA_WsCoreSum);

  MULTI_ADDER_WS_ENV : entity work.MultiAdder
    generic map (
      stage => 4,
      delay => 3)
    port map (
      CLK          => CLK200,
      IN_Words     => IM_WsEnvData &ZERO_DATA_WORD,  --15+1
      OUT_Overflow => MA_WsEnvOverflow,
      OUT_Word     => MA_WsEnvSum);

--Hadron
  MULTI_ADDER_HAD_CORE : entity work.MultiAdder
    generic map (
      stage => 4,
      delay => 0)
    port map (
      CLK          => CLK200,
      IN_Words     => IM_HadCoreData &ZERO_DATA_WORD&ZERO_DATA_WORD&ZERO_DATA_WORD&ZERO_DATA_WORD&ZERO_DATA_WORD&ZERO_DATA_WORD&ZERO_DATA_WORD,  --9+7
      OUT_Overflow => MA_HadCoreOverflow,
      OUT_Word     => MA_HadCoreSum);

  MULTI_ADDER_HAD_ENV : entity work.MultiAdder
    generic map (
      stage => 5,
      delay => 2)
    port map (
      CLK          => CLK200,
      IN_Words     => IM_HadEnvDataL1&IM_HadEnvDataL2&IM_HadEnvDataL03 & ZERO_DATA_WORD&ZERO_DATA_WORD&ZERO_DATA_WORD&ZERO_DATA_WORD&ZERO_DATA_WORD&ZERO_DATA_WORD&ZERO_DATA_WORD&ZERO_DATA_WORD,  --24+8
      OUT_Overflow => MA_HadEnvOverflow,
      OUT_Word     => MA_HadEnvSum);


-------------------------------------------------------------------------------
-- MULTIPLIERS
-------------------------------------------------------------------------------

-- REta
  RETA_MULTIPLIER : entity work.MultiMultiplier  --delay 3
    generic map (parameters => 3)
    port map (
      CLK           => CLK200,
      IN_Word       => MA_REtaEnvSum,
      IN_parameters => ParREta,
      OUT_Overflow  => MU_REtaEnvOverflows,
      OUT_Words     => MU_REtaEnvMult
      );

-- Ws
  WS_MULTIPLIER : entity work.MultiMultiplier  --delay 3
    generic map (parameters => 3)
    port map (
      CLK           => CLK200,
      IN_Word       => MA_WsCoreSum,
      IN_parameters => ParWs,
      OUT_Overflow  => MU_WsCoreOverflows,
      OUT_Words     => MU_WsCoreMult
      );

--hadronic
  HADRON_MULTIPLIER : entity work.MultiMultiplier  --delay 3
    generic map (parameters => 3)
    port map (
      CLK           => CLK200,
      IN_Word       => MA_HadCoreSum,
      IN_parameters => ParRh,
      OUT_Overflow  => MU_HadCoreOverflows,
      OUT_Words     => MU_HadCoreMult
      );

  Reta_threshold_delay : entity work.Delay
    generic map (
      delay => 3)
    port map (
      CLK      => CLK200,
      IN_Word  => RetaThr,
      OUT_Word => RetaThr_d);


  Ws_threshold_delay : entity work.Delay
    generic map (
      delay => 3)
    port map (
      CLK      => CLK200,
      IN_Word  => WsThr,
      OUT_Word => WsThr_d);

  Had_threshold_delay : entity work.Delay
    generic map (
      delay => 3)
    port map (
      CLK      => CLK200,
      IN_Word  => RetaThr,
      OUT_Word => RetaThr_d);

  Energy_threshold_delay : entity work.Delay
    generic map (
      delay => 3)
    port map (
      CLK      => CLK200,
      IN_Word  => EnergyThr,
      OUT_Word => EnergyThr_d);

-------------------------------------------------------------------------------
-- SEED AND OVERFLOWS DELAY
-------------------------------------------------------------------------------

  SEED_DELAY : entity infrastructure_lib.GeneralDelay
    generic map (
      delay => 9,
      size  => 5)
    port map (
      clk      => CLK200,
      data_in  => (0 => SF_IsMax, 1 => SF_IsLocalMax, 2 => SF_UpNotDown, 4 downto 3 => SF_Seed),
      data_out => DL_SeedFinder
      );


  DL_IsMax      <= DL_SeedFinder(0);
  DL_IsLocalMax <= DL_SeedFinder(1);
  DL_UpNotDown  <= DL_SeedFinder(2);
  DL_Seed       <= DL_SeedFinder(4 downto 3);

  OVERFLOW_DELAY : entity infrastructure_lib.GeneralDelay
    generic map (
      delay => 3,
      size  => 3)
    port map (
      clk      => CLK200,
      data_in  => (0 => MA_REtaEnvOverflow, 1 => MA_WsCoreOverflow, 2 => MA_HadCoreOverflow),
      data_out => DL_Overflows
      );

  DL_REtaEnvOverflow <= DL_Overflows(0);
  DL_WsCoreOverflow  <= DL_Overflows(1);
  DL_HadCoreOverflow <= DL_Overflows(2);

-----------------------------------------------------------------------------
-- TRIGGER-OBJECT CONDITIONS
-----------------------------------------------------------------------------
  CONDITIONS : process (CLK200)
  variable RetaShifted, WsShifted, HadShifted: DataWord;
  begin
    if rising_edge(CLK200) then
      -- add energhy threshold here
      if MA_EnergySum < EnergyThr_d then
        TOBEnergy         <= (others => '0');
        TOBEnergyOverflow <= '0';
      else
        -- add material correction here
        TOBEnergy         <= MA_EnergySum;
        TOBEnergyOverflow <= MA_EnergyOverflow;
      end if;


      -- REta condition
      RetaShifted := BitLeftShift(MA_REtaCoreSum, 3);
      if MA_REtaCoreOverflow = '0' and MA_EnergySum < RetaThr_d then
        if DL_REtaEnvOverflow = '0' then
          if  (RetaShifted > MU_REtaEnvMult(2) and MU_REtaEnvOverflows(2) = '0') then
            REtaCondition <= "11";
          elsif (RetaShifted > MU_REtaEnvMult(1) and MU_REtaEnvOverflows(1) = '0') then
            REtaCondition <= "10";
          elsif (RetaShifted > MU_REtaEnvMult(0) and MU_REtaEnvOverflows(0) = '0') then
            REtaCondition <= "01";
          else
            REtaCondition <= "00";
          end if;
        else
          -- Env sum overflow
          REtaCondition <= "00";
        end if;
      else
        -- Core overflow or energy threshold exceeded
        REtaCondition <= "11";
      end if;

      -- Ws condition
      WsShifted := BitLeftShift(MA_WsEnvSum, 5);
      if MA_WsEnvOverflow = '0' and MA_EnergySum < WsThr_d then
        if DL_WsCoreOverflow = '0' then
          if ( WsShifted > MU_WsCoreMult(2) and MU_WsCoreOverflows(2) = '0') then
            WsCondition <= "11";
          elsif (WsShifted > MU_WsCoreMult(1) and MU_WsCoreOverflows(1) = '0') then
            WsCondition <= "10";
          elsif (WsShifted > MU_WsCoreMult(0) and MU_WsCoreOverflows(0) = '0') then
            WsCondition <= "01";
          else
            WsCondition <= "00";
          end if;
        else
          -- Core sum overflow
          WsCondition <= "00";
        end if;
      else
        -- Env overflow  or energy threshold exceeded
        WsCondition <= "11";
      end if;

      -- hadronic conditions
      HadShifted := BitLeftShift(MA_HadEnvSum, 2);
      if MA_HadEnvOverflow = '0' and MA_EnergySum < HadThr_d then
        if DL_HadCoreOverflow = '0' then
          if (HadShifted > MU_HadCoreMult(2) and MU_HadCoreOverflows(2) = '0') then
            HadCondition <= "11";
          elsif (HadShifted > MU_HadCoreMult(1) and MU_HadCoreOverflows(1) = '0') then
            HadCondition <= "10";
          elsif (HadShifted > MU_HadCoreMult(0) and MU_HadCoreOverflows(0) = '0') then
            HadCondition <= "01";
          else
            HadCondition <= "00";
          end if;
        else
          -- Core sum overflow
          HadCondition <= "00";
        end if;
      else
        -- Env overflow or energy threshold exceeded
        HadCondition <= "11";
      end if;

    end if;
  end process;


-- TOB building
  OUT_TOB.Core.Energy    <= to_TOBEnergy(TOBEnergy);
  OUT_TOB.Core.EnergyOF  <= TOBEnergyOverflow;
  OUT_TOB.Core.REta      <= REtaCondition;
  OUT_TOB.Core.ws        <= WsCondition;
  OUT_TOB.Core.Had       <= HadCondition;
  OUT_TOB.Core.UpNotDown <= DL_UpNotDown;
  OUT_TOB.Core.Seed      <= DL_Seed;
  OUT_TOB.Core.Max       <= DL_IsMax;
  OUT_TOB.Core.LocalMax  <= DL_IsLocalMax;
  OUT_TOB.Position.Eta   <= (others => '0');
  OUT_TOB.Position.Phi   <= (others => '0');

end Behavioral;
