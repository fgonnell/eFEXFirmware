library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;
use work.ipbus_decode_efex_cntrl_mgt_channel.all;

entity gt_information is
  generic (addr_width : natural := 3);
  port(
    ipb_clk          : in  std_logic;
    clk280           : in  std_logic;
    reset            : in  std_logic;
    ipb_rst          : in  std_logic;
    ipb_in           : in  ipb_wbus;
    ipb_out          : out ipb_rbus;
    bc_cntr          : in  std_logic_vector(4 downto 0);
    bc_mux_cntr      : in  std_logic_vector(4 downto 0);
    delay_cntr       : in  std_logic_vector(3 downto 0);
    not_intable      : in  std_logic;
    tx_pd            : in  std_logic;
    rx_pd            : in  std_logic;
    rx_resetdone     : in  std_logic;
    rx_fsm_resetdone : in  std_logic;
    rx_byteisaligned : in  std_logic;
    tx_resetdone     : in  std_logic;
    tx_fsm_resetdone : in  std_logic;
    tx_bufstatus     : in  std_logic_vector (1 downto 0);
    rx_realign       : in  std_logic;
    rx_disperr       : in  std_logic



    );
end gt_information;

architecture Behavioral of gt_information is

  signal gt_rx_nointable_cntr, gt_rx_disperr_cntr, gt_byterealign_cntr                                                                        : std_logic_vector(15 downto 0);
  signal status, gt_rx_nointable_cntr_i, gt_rx_disperr_cntr_i, gt_byterealign_cntr_i, delay_cntr_i, bc_cntr_i, bc_mux_cntr_i: std_logic_vector (31 downto 0);
  signal ipbw : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal ipbr : ipb_rbus_array(N_SLAVES-1 downto 0);

begin


  status                 <= x"00000" & "00" & rx_pd & tx_pd & tx_bufstatus & tx_fsm_resetdone & tx_resetdone & '0' & rx_byteisaligned & rx_fsm_resetdone & rx_resetdone;
  gt_rx_nointable_cntr_i <= X"0000" & gt_rx_nointable_cntr;
  gt_rx_disperr_cntr_i   <= X"0000" & gt_rx_disperr_cntr;
  gt_byterealign_cntr_i  <= X"0000" & gt_byterealign_cntr;
  delay_cntr_i           <= x"0000000" & delay_cntr;
  bc_cntr_i              <= x"000000" & "000"& bc_cntr;
  bc_mux_cntr_i          <= x"000000" & "000"& bc_mux_cntr;


--  sel <= to_integer(unsigned(ipb_in.ipb_addr(addr_width-1 downto 0))) when addr_width > 0 else 0;

  fabric : entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV      => N_SLAVES,  --defined in ipbus_decode_fpga_proc_common_registers
                SEL_WIDTH => ipbus_sel_width)
    port map(
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      sel             => ipbus_sel_efex_cntrl_mgt_channel(ipb_in.ipb_addr),
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      );

  channel_status : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,              --number of control reg         
      N_STAT => 1)              --number of status reg          
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_STATUS),
      ipbus_out => ipbr(N_SLV_STATUS),
      d         => (0 => status),
      q         => open,
      stb       => open);

  channel_notintable : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,              --number of control reg         
      N_STAT => 1)              --number of status reg          
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_NOT_IN_TABLE_COUNTER),
      ipbus_out => ipbr(N_SLV_NOT_IN_TABLE_COUNTER),
      d         => (0 => gt_rx_nointable_cntr_i),
      q         => open,
      stb       => open);

  channel_disperr : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,              --number of control reg         
      N_STAT => 1)              --number of status reg          
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_DISPERR_COUNTER),
      ipbus_out => ipbr(N_SLV_DISPERR_COUNTER),
      d         => (0 => gt_rx_disperr_cntr_i),
      q         => open,
      stb       => open);

  channel_byterealign : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,              --number of control reg         
      N_STAT => 1)              --number of status reg          
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_BYTEREALIGN_COUNTER),
      ipbus_out => ipbr(N_SLV_BYTEREALIGN_COUNTER),
      d         => (0 => gt_byterealign_cntr_i),
      q         => open,
      stb       => open);

  channel_delay : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,              --number of control reg         
      N_STAT => 1)              --number of status reg          
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_DELAY_COUNTER),
      ipbus_out => ipbr(N_SLV_DELAY_COUNTER),
      d         => (0 => delay_cntr_i),
      q         => open,
      stb       => open);

  channel_bc : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,              --number of control reg         
      N_STAT => 1)              --number of status reg          
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_BC_COUNTER),
      ipbus_out => ipbr(N_SLV_BC_COUNTER),
      d         => (0 => bc_cntr_i),
      q         => open,
      stb       => open);

  channel_bc_mux : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,              --number of control reg         
      N_STAT => 1)              --number of status reg          
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_BC_MUX_COUNTER),
      ipbus_out => ipbr(N_SLV_BC_MUX_COUNTER),
      d         => (0 => bc_mux_cntr_i),
      q         => open,
      stb       => open);

  cntr_0 : entity work.counter
    port map(
      clk       => clk280,
      enable2   => rx_resetdone,
      enable1   => not_intable,
      count     => gt_rx_nointable_cntr,
      reset     => reset

      );

  cntr_1 : entity work.counter
    port map(
      clk       => clk280,
      enable2   => rx_resetdone,
      enable1   => rx_disperr,
      count     => gt_rx_disperr_cntr,
      reset     => reset

      );

  cntr_2 : entity work.counter
    port map(
      clk       => clk280,
      enable2   => rx_resetdone,
      enable1   => rx_realign,
      count     => gt_byterealign_cntr,
      reset     => reset

      );
end Behavioral;
