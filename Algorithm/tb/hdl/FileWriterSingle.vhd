----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

use IEEE.NUMERIC_STD.all;

library std;
use std.textio.all;             --TEXT, FILE is here


library algolib;
use algolib.AlgoDataTypes.all;
use algolib.DataTypes.all;
use algolib.Utility.all;

entity FileWriterSingle is
  generic(
    filename : string := "out_file.txt");
  port (
    CLK    : in std_logic;
    RST    : in std_logic := '0';
    TOB_IN : in TriggerObject;
    WE     : in std_logic := '1'
    );
end FileWriterSingle;

--

architecture Behavioral of FileWriterSingle is

  file outfile   : text open write_mode is filename;
  signal cnt     : integer := 0;
  signal syn_cnt : integer := 0;
begin
  process(clk)

  begin
    if RST = '1' then
      print(outfile, "RESET");
    elsif rising_edge(clk) then
      cnt <= cnt + 1;
      if WE = '1' then
        print(outfile,
              "Energy: " & str(to_integer(unsigned(TOB_IN.Core.Energy))) & " OF: " & str(TOB_IN.Core.EnergyOF) & " - " &
              "REta: " & str(to_integer(unsigned(TOB_IN.Core.REta))) &
              " f3: " & str(to_integer(unsigned(TOB_IN.Core.f3))) &
              " Had: " & str(to_integer(unsigned(TOB_IN.Core.Had))) & " - " &
              "UpNotDown: " & str(TOB_IN.Core.UpNotDown) & " Seed: " & str(to_integer(unsigned(TOB_IN.Core.Seed))) & " - " &
              "Max: " & str(TOB_IN.Core.Max) & " LocalMax: " & str(TOB_IN.Core.LocalMax)
              );
      end if;

    end if;

  end process;
end Behavioral;

