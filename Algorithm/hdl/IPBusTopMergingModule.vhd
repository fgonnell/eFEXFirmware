--! @file
--! @brief Top of TOB merging module with IPBus interface
--! @details
--! This module is an IPBus-capable wrapper of TopSorting Modules.
--! It merges the local TOBs together with the TOBs coming from the other 3 FPGAs.
--! It contains 1 4-input sorting modules, 4 input and 1 output spyRAMs, all ipbus controlled.
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
use work.ipbus_decode_efex_merging.all;

use work.DataTypes.all;
use work.AlgoDataTypes.all;

library ipbus_lib;
use ipbus_lib.ipbus_reg_types.all;

use ipbus_lib.ipbus.all;

--! @copydoc IPBusTopMergingModule.vhd
entity IPBusTopMergingModule is
  generic (TEST   : boolean   := false;
           ENABLE : std_logic := '1'
           );
  port (
    CLK : in std_logic;

    IN_Data     : in AlgoTriggerObjects(3 downto 0);
    IN_Sync     : in std_logic;
    IN_BCN_sync : in std_logic_vector(3 downto 0);  -- BCN synch from other FPGAs

    --TOB BCN delay
    IN_local_BCN   : in  std_logic_vector(6 downto 0);
    OUT_merged_BCN : out std_logic_vector(6 downto 0);

    --IPBus connection
    ipb_clk : in  std_logic;
    ipb_rst : in  std_logic;
    ipb_in  : in  ipb_wbus;
    ipb_out : out ipb_rbus;

    OUT_Sync  : out std_logic;
    OUT_Valid : out std_logic;
    OUT_TOB   : out AlgoTriggerObject);
end IPBusTopMergingModule;

--! @copydoc IPBusTopMergingModule.vhd
architecture Behavioral of IPBusTopMergingModule is
  constant N_CTRL : positive := 3;  --number of control reg
  constant N_STAT : positive := 2;  --number of status reg

  -- Algorithm signal

-- IPBus signals
  signal ipb_to_slaves   : ipb_wbus_array(N_SLAVES - 1 downto 0);
  signal ipb_from_slaves : ipb_rbus_array(N_SLAVES - 1 downto 0) := (others => IPB_RBUS_NULL);

  signal write_reg : ipb_reg_v(N_STAT - 1 downto 0) := (others => (others => '0'));
  signal read_reg  : ipb_reg_v(N_CTRL - 1 downto 0);

-- registers
  signal regMergingControl, regInternalSortingControl, regMergingStatus, regOffset : AlgoRegister;

-- i/o signals
  signal MergingStart  : std_logic;
  signal MergedStart   : std_logic;
  signal MergedWrite   : std_logic;
  signal MergedData    : AlgoTriggerObject;
  signal MergedDataOut : AlgoTriggerObject;

  signal MergingDataIn : AlgoTriggerObjects(3 downto 0);  --! Input data before offset
  signal OffsetData    : AlgoTriggerObjects(3 downto 0);  --! Input data after offset, before demux
  signal MergingData   : AlgoTriggerObjects(3 downto 0);  --! Input data after demux


-- spy RAMs
  signal InputRAMOut     : AlgoTriggerObjects(3 downto 0);
  signal FakeInputEnable : std_logic := '0';
  signal SpyInputEnable  : std_logic := '0';

  signal OutputRAMOut     : AlgoTriggerObject;
  signal FakeOutputEnable : std_logic := '0';
  signal SpyOutputEnable  : std_logic := '0';

-- syng signals
  signal Offset0, Offset1, Offset2, Offset3 : std_logic_vector(5 downto 0);
  signal Enable0, Enable1, Enable2, Enable3 : std_logic;

  signal bcn_sync                               : std_logic;
  signal bcn_sync_reg                           : std_logic_vector(6 downto 0);
  signal BCN_cnt3, BCN_cnt2, BCN_cnt1, BCN_cnt0 : std_logic_vector(9 downto 0)  := (others => '0');
  signal BCN_done                               : std_logic_vector(2 downto 0)  := (others => '0');
  signal BCN_counters                           : std_logic_vector(29 downto 0) := (others => '0');
  signal reset_cnt                              : std_logic_vector(15 downto 0) := (others => '0');


  -- to delay BCN and form the merged BCN
  signal merged_bcn_d1, merged_bcn_d2 : std_logic_vector(6 downto 0);
  type BCN_t is array(63 downto 0) of std_logic_vector(6 downto 0);
  signal TOB_BCN_d                    : BCN_t := (others => (others => '0'));

begin
  ENABLED_MERGE : if ENABLE = '1' generate
    IPBUS_FABRIC : entity ipbus_lib.ipbus_fabric_sel
      generic map (
        NSLV      => N_SLAVES,                           --defined in ipbus_sel_address_table_MERGING
        SEL_WIDTH => IPBUS_SEL_WIDTH)
      port map (
        sel => ipbus_sel_efex_merging(ipb_in.ipb_addr),  --function defined in ipbus_sel_address_table_MERGING

        ipb_in          => ipb_in,
        ipb_out         => ipb_out,
        ipb_to_slaves   => ipb_to_slaves,
        ipb_from_slaves => ipb_from_slaves);
  end generate ENABLED_MERGE;

  DISABLED_MERGE : if ENABLE = '0' generate
    IPBUS_FABRIC : entity ipbus_lib.ipbus_fabric_sel
      generic map (
        NSLV      => N_SLAVES,  --defined in ipbus_sel_address_table_MERGING
        SEL_WIDTH => IPBUS_SEL_WIDTH)
      port map (
        sel             => (others => '0'),
        ipb_in          => IPB_WBUS_NULL,
        ipb_out         => open,
        ipb_to_slaves   => open,
        ipb_from_slaves => (others => IPB_RBUS_NULL)
        );
    ipb_out <= (x"DEADBEEF", '1', '0');
  end generate DISABLED_MERGE;

  IPBUS_MERGING_REGISTERS : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => N_CTRL,
      N_STAT => N_STAT)
    port map (
      clk          => ipb_clk,
      reset        => ipb_rst,
      ctrl_default => (2 => x"00000003", others => x"00000000"),
      ipbus_in     => ipb_to_slaves(N_SLV_MERGING_REGISTERS),
      ipbus_out    => ipb_from_slaves(N_SLV_MERGING_REGISTERS),
      d            => write_reg,
      q            => read_reg,
      stb          => open);

  -----------------------------------------------------------------------------
  -- TOB merging
  -----------------------------------------------------------------------------
  TOB_SYNCH_OFFSET : entity work.TOB_synch
    port map (
      CLK => CLK,

      IN_Offset0 => Offset0,
      IN_Offset1 => Offset1,
      IN_Offset2 => Offset2,
      IN_Offset3 => Offset3,

      IN_Enable0 => Enable0,
      IN_Enable1 => Enable1,
      IN_Enable2 => Enable2,
      IN_Enable3 => Enable3,

      IN_Start  => IN_Sync,
      OUT_Start => MergingStart,
      IN_Data   => MergingDataIn,
      OUT_Data  => OffsetData);

--input MUX: real data/ data from input RAM
  MergingData <= InputRAMOut when FakeInputEnable = '1' else OffsetData;


  TOBMerging : entity work.TopSortingModule
    generic map (
      STAGE  => 2,
      N_TOBS => 7)
    port map (
      CLK        => CLK,
      IN_Control => regInternalSortingControl,
      OUT_Status => regMergingStatus,  --not used
      IN_Start   => MergingStart,
      IN_Data    => MergingData,
      OUT_Start  => MergedStart,
      OUT_Write  => MergedWrite,
      OUT_Data   => MergedData);

-- Conversion to external TOBs
  MergedDataOut <= to_AlgoTriggerObject(MergedData);

--------------------------------------------------------------------------------
-- Input RAMs
--------------------------------------------------------------------------------

  inputRAM_1 : entity work.ipbus_sorting_outputRAM_wrapper
    port map (
      clk_ipb => ipb_clk,
      rst     => ipb_rst,
      ipb_in  => ipb_to_slaves(N_SLV_MERGING_INPUT_RAM_1),
      ipb_out => ipb_from_slaves(N_SLV_MERGING_INPUT_RAM_1),

      BCIDIn    => (others => '0'),
      SpyBCIDIN => '0',
      SortedIn  => IN_Data(0),
      rclk      => CLK,
      Sync      => MergingStart,
      we        => SpyInputEnable,
      SortedOut => InputRAMOut(0));

  inputRAM_2 : entity work.ipbus_sorting_outputRAM_wrapper
    port map (
      clk_ipb => ipb_clk,
      rst     => ipb_rst,
      ipb_in  => ipb_to_slaves(N_SLV_MERGING_INPUT_RAM_2),
      ipb_out => ipb_from_slaves(N_SLV_MERGING_INPUT_RAM_2),

      BCIDIn    => (others => '0'),
      SpyBCIDIN => '0',
      SortedIn  => IN_Data(1),
      rclk      => CLK,
      Sync      => MergingStart,
      we        => SpyInputEnable,
      SortedOut => InputRAMOut(1));

  inputRAM_3 : entity work.ipbus_sorting_outputRAM_wrapper
    port map (
      clk_ipb => ipb_clk,
      rst     => ipb_rst,
      ipb_in  => ipb_to_slaves(N_SLV_MERGING_INPUT_RAM_3),
      ipb_out => ipb_from_slaves(N_SLV_MERGING_INPUT_RAM_3),

      BCIDIn    => (others => '0'),
      SpyBCIDIN => '0',
      SortedIn  => IN_Data(2),
      rclk      => CLK,
      Sync      => MergingStart,
      we        => SpyInputEnable,
      SortedOut => InputRAMOut(2));

  inputRAM_4 : entity work.ipbus_sorting_outputRAM_wrapper
    port map (
      clk_ipb => ipb_clk,
      rst     => ipb_rst,
      ipb_in  => ipb_to_slaves(N_SLV_MERGING_INPUT_RAM_4),
      ipb_out => ipb_from_slaves(N_SLV_MERGING_INPUT_RAM_4),

      BCIDIn    => (others => '0'),
      SpyBCIDIN => '0',
      SortedIn  => IN_Data(3),
      rclk      => CLK,
      Sync      => MergingStart,
      we        => SpyInputEnable,
      SortedOut => InputRAMOut(3));


--------------------------------------------------------------------------------
-- Output RAMs
--------------------------------------------------------------------------------

  outputRAM : entity work.ipbus_sorting_outputRAM_wrapper
    port map (
      clk_ipb => ipb_clk,
      rst     => ipb_rst,
      ipb_in  => ipb_to_slaves(N_SLV_MERGING_OUTPUT_RAM),
      ipb_out => ipb_from_slaves(N_SLV_MERGING_OUTPUT_RAM),

      BCIDIn    => (others => '0'),
      SpyBCIDIN => '0',
      SortedIn  => MergedDataOut,
      rclk      => CLK,
      Sync      => MergedStart,
      we        => SpyOutputEnable,
      SortedOut => OutputRAMOut);

--output MUX: real data/ data from output RAM  
  ENABLED_MERGING_OUTPUT : if ENABLE = '1' generate
    -- input
    MergingDataIn <= IN_Data;
    -- output
    OUT_TOB       <= OutputRAMOut when FakeOutputEnable = '1' else MergedDataOut;
    OUT_Valid     <= '1'          when FakeOutputEnable = '1' else MergedWrite;
    OUT_Sync      <= MergedStart;
  end generate ENABLED_MERGING_OUTPUT;

  DISABLED_MERGING_OUTPUT : if ENABLE = '0' generate
    -- input
    MergingDataIn <= (ZERO_ALGO_TRIGGER_OBJECT, ZERO_ALGO_TRIGGER_OBJECT, ZERO_ALGO_TRIGGER_OBJECT, ZERO_ALGO_TRIGGER_OBJECT);
    -- output
    OUT_TOB       <= (others => '0');
    OUT_Valid     <= '0';
    OUT_Sync      <= '0';
  end generate DISABLED_MERGING_OUTPUT;


  BCN_FPGA_sych : process (CLK)
  begin
    if rising_edge(CLK) then
      -- handling BCN counters
      if IN_BCN_sync(3) = '1' then
        BCN_cnt0  <= (others => '0');
        BCN_cnt1  <= (others => '0');
        BCN_cnt2  <= (others => '0');
        BCN_done  <= IN_BCN_sync(2 downto 0);  -- was "000", but this handle the case when start and stop are simulatneous
        reset_cnt <= std_logic_vector(unsigned(reset_cnt) + 1);

      else
        if IN_BCN_sync(0) = '1' or BCN_done(0) = '1' then
          BCN_done(0) <= '1';
          BCN_cnt0    <= BCN_cnt0;
        else
          BCN_done(0) <= BCN_done(0);
          BCN_cnt0    <= std_logic_vector(unsigned(BCN_cnt0) + 1);
        end if;

        if IN_BCN_sync(1) = '1' or BCN_done(1) = '1' then
          BCN_done(1) <= '1';
          BCN_cnt1    <= BCN_cnt1;
        else
          BCN_done(1) <= BCN_done(1);
          BCN_cnt1    <= std_logic_vector(unsigned(BCN_cnt1) + 1);
        end if;

        if IN_BCN_sync(2) = '1' or BCN_done(2) = '1' then
          BCN_done(2) <= '1';
          BCN_cnt2    <= BCN_cnt2;
        else
          BCN_done(2) <= BCN_done(2);
          BCN_cnt2    <= std_logic_vector(unsigned(BCN_cnt2) + 1);
        end if;

        reset_cnt <= reset_cnt;
      end if;
    end if;
  end process;

  BCN_published_ipbus : process (CLK)
  begin
    if rising_edge(CLK) then
      if BCN_done(0) = '1' then
        BCN_counters(9 downto 0) <= BCN_cnt0;
      else
        BCN_counters(9 downto 0) <= BCN_counters(9 downto 0);
      end if;

      if BCN_done(1) = '1' then
        BCN_counters(19 downto 10) <= BCN_cnt1;
      else
        BCN_counters(19 downto 10) <= BCN_counters(19 downto 10);
      end if;

      if BCN_done(2) = '1' then
        BCN_counters(29 downto 20) <= BCN_cnt2;
      else
        BCN_counters(29 downto 20) <= BCN_counters(29 downto 20);
      end if;

    end if;
  end process;


  TOB_BCN_d(0)   <= IN_local_BCN;
  OUT_merged_BCN <= merged_bcn_d2;  -- this will be delayed as much as Offset0 + 2

  local_BCN_delay : process (CLK)
  begin
    if rising_edge(CLK) then
      merged_bcn_d1          <= TOB_BCN_d(to_integer(unsigned(Offset0)));
      merged_bcn_d2          <= merged_bcn_d1;  -- add 2 more clock cycles delay
      TOB_BCN_d(31 downto 1) <= TOB_BCN_d(30 downto 0);
    end if;
  end process;



-- Sorting registers connection
  regMergingControl         <= read_reg(0);
  regInternalSortingControl <= read_reg(1);
  regOffset                 <= read_reg(2);

  write_reg(0) <= "00" & BCN_counters;
  write_reg(1) <= x"0" & reset_cnt & x"00" & "0" & BCN_done(2) & BCN_done(1) & BCN_done(0);


-- Sorting register funcionality
  FakeInputEnable  <= '0' when TEST else regMergingControl(0);
  SpyInputEnable   <= '0' when TEST else regMergingControl(1);
  FakeOutputEnable <= '0' when TEST else regMergingControl(2);
  SpyOutputEnable  <= '0' when TEST else regMergingControl(3);

  Offset0 <= (0 => '1', others => '0') when TEST else regOffset(5 downto 0);
  Offset1 <= (0 => '1', others => '0') when TEST else regOffset(13 downto 8);
  Offset2 <= (0 => '0', others => '0') when TEST else regOffset(21 downto 16);
  Offset3 <= (0 => '1', others => '0') when TEST else regOffset(29 downto 24);

  Enable0 <= '1' when TEST else not regOffset(7);
  Enable1 <= '1' when TEST else not regOffset(15);
  Enable2 <= '0' when TEST else not regOffset(23);
  Enable3 <= '1' when TEST else not regOffset(31);

end architecture;
