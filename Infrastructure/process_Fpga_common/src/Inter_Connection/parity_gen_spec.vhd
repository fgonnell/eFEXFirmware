--
-- VHDL Architecture moh_test.parity_gen.spec
--
-- Created:
--          by - mjs59.UNKNOWN (te7gromit)
--          at - 17:15:59 11/04/15
-- This is even parity generator block.
-- The parity output bit must be 0 when the number of '1' in the data_in
-- is even, or '1' otherwise.

--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;


ENTITY parity_gen IS
 
  Generic( width :integer:= 9);
    PORT
     ( 
        Reset   :IN std_logic; 
        CLK     :IN std_logic;    
        Data_in :IN std_logic_vector(width-1 downto 0);
        data_Parity  :OUT std_logic_vector(width downto 0)
        );
          
END ENTITY parity_gen;

--
ARCHITECTURE spec OF parity_gen IS

BEGIN
  
odd_parity: Process(clk)
        
variable temp: std_logic:='0';   
     
            Begin
             
--                if  Reset ='1' then
--                    data_parity <=  (others=>'0');                   
                 if CLK' event and CLK ='1' then                               
                    temp :=  '0';  
                            
                   for i in 0 to width-1 loop
                     
                     temp := temp xor data_in(i);
                                          
                   end loop;                
                                   
                  data_parity <= (temp & data_in);
                  
                   end if; 
                                           
              end process;
              
             
                   
END ARCHITECTURE spec;

