
--  this wrapper is for MGT with RX at 11.2Gbps, Ref clk = 280MHz,


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
--***********************************Entity Declaration************************

--  this wrapper is for MGT with RX at 11.2Gbps, Ref clk = 280MHz,

entity min_latency_1quad_11g2_Rx_wrapper is
generic
(
    EXAMPLE_SIM_GTRESET_SPEEDUP             : string    := "TRUE";     -- simulation setting for GT SecureIP model
    STABLE_CLOCK_PERIOD                     : integer   := 16  

);
port
(
    SOFT_RESET_RX_IN                        : in   std_logic;
    RXN_IN                                  : in   std_logic_vector(3 downto 0);
    RXP_IN                                  : in   std_logic_vector(3 downto 0);
    Q3_CLK0_GTREFCLK_PAD_N_IN               : in   std_logic;
    Q3_CLK0_GTREFCLK_PAD_P_IN               : in   std_logic;

    GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT3_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT3_RX_FSM_RESET_DONE_OUT               : out  std_logic;
 
    GT0_RXUSRCLK_OUT                        : out  std_logic;
    GT1_RXUSRCLK_OUT                        : out  std_logic;
    GT2_RXUSRCLK_OUT                        : out  std_logic;
    GT3_RXUSRCLK_OUT                        : out  std_logic;

    --_________________________________________________________________________
    --GT0  (X0Y12)
    --____________________________CHANNEL PORTS________________________________
    ------------------------------- Loopback Ports -----------------------------
    gt0_loopback_in                         : in   std_logic_vector(2 downto 0);
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt0_rxdata_out                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    gt0_rxdisperr_out                       : out  std_logic_vector(3 downto 0);
    gt0_rxnotintable_out                    : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    gt0_rxbyteisaligned_out                 : out  std_logic;
    gt0_rxbyterealign_out                   : out  std_logic;
    gt0_rxcommadet_out                      : out  std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    gt0_rxchariscomma_out                   : out  std_logic_vector(3 downto 0);
    gt0_rxcharisk_out                       : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt0_rxresetdone_out                     : out  std_logic;

    --GT1  (X0Y13)
    --____________________________CHANNEL PORTS________________________________
    ------------------------------- Loopback Ports -----------------------------
    gt1_loopback_in                         : in   std_logic_vector(2 downto 0);
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt1_rxdata_out                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    gt1_rxdisperr_out                       : out  std_logic_vector(3 downto 0);
    gt1_rxnotintable_out                    : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    gt1_rxbyteisaligned_out                 : out  std_logic;
    gt1_rxbyterealign_out                   : out  std_logic;
    gt1_rxcommadet_out                      : out  std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    gt1_rxchariscomma_out                   : out  std_logic_vector(3 downto 0);
    gt1_rxcharisk_out                       : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt1_rxresetdone_out                     : out  std_logic;

    --GT2  (X0Y14)
    --____________________________CHANNEL PORTS________________________________
    ------------------------------- Loopback Ports -----------------------------
    gt2_loopback_in                         : in   std_logic_vector(2 downto 0);
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt2_rxdata_out                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    gt2_rxdisperr_out                       : out  std_logic_vector(3 downto 0);
    gt2_rxnotintable_out                    : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    gt2_rxbyteisaligned_out                 : out  std_logic;
    gt2_rxbyterealign_out                   : out  std_logic;
    gt2_rxcommadet_out                      : out  std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    gt2_rxchariscomma_out                   : out  std_logic_vector(3 downto 0);
    gt2_rxcharisk_out                       : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt2_rxresetdone_out                     : out  std_logic;

    --GT3  (X0Y15)
    --____________________________CHANNEL PORTS________________________________
    ------------------------------- Loopback Ports -----------------------------
    gt3_loopback_in                         : in   std_logic_vector(2 downto 0);
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt3_rxdata_out                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    gt3_rxdisperr_out                       : out  std_logic_vector(3 downto 0);
    gt3_rxnotintable_out                    : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    gt3_rxbyteisaligned_out                 : out  std_logic;
    gt3_rxbyterealign_out                   : out  std_logic;
    gt3_rxcommadet_out                      : out  std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    gt3_rxchariscomma_out                   : out  std_logic_vector(3 downto 0);
    gt3_rxcharisk_out                       : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt3_rxresetdone_out                     : out  std_logic;

    --____________________________COMMON PORTS________________________________
     GT0_QPLLLOCK_OUT : out std_logic;
     GT0_QPLLREFCLKLOST_OUT  : out std_logic;
---  GT0_QPLLOUTCLK_OUT  : out std_logic;
--    GT0_QPLLOUTREFCLK_OUT : out std_logic;     
     sysclk_in        : in std_logic

);


end min_latency_1quad_11g2_Rx_wrapper;

architecture RTL of min_latency_1quad_11g2_Rx_wrapper is

    attribute DowngradeIPIdentifiedWarnings: string;
    attribute DowngradeIPIdentifiedWarnings of RTL : architecture is "yes";

    attribute CORE_GENERATION_INFO : string;
    attribute CORE_GENERATION_INFO of RTL : architecture is "min_latency_F1_quad213_11g2_rx,gtwizard_v3_6_5,{protocol_file=Start_from_scratch}";

--**************************Component Declarations*****************************

component min_latency_F1_quad213_11g2_rx_support 
generic
(
    EXAMPLE_SIM_GTRESET_SPEEDUP             : string    := "TRUE";     -- simulation setting for GT SecureIP model
    STABLE_CLOCK_PERIOD                     : integer   := 16  

);
port
(
    SOFT_RESET_RX_IN                        : in   std_logic;
    DONT_RESET_ON_DATA_ERROR_IN             : in   std_logic;
    Q3_CLK0_GTREFCLK_PAD_N_IN               : in   std_logic;
    Q3_CLK0_GTREFCLK_PAD_P_IN               : in   std_logic;

    GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_DATA_VALID_IN                       : in   std_logic;
    GT1_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_DATA_VALID_IN                       : in   std_logic;
    GT2_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_DATA_VALID_IN                       : in   std_logic;
    GT3_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT3_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT3_DATA_VALID_IN                       : in   std_logic;
 
    GT0_RXUSRCLK_OUT                        : out  std_logic;
    GT0_RXUSRCLK2_OUT                       : out  std_logic;
 
    GT1_RXUSRCLK_OUT                        : out  std_logic;
    GT1_RXUSRCLK2_OUT                       : out  std_logic;
 
    GT2_RXUSRCLK_OUT                        : out  std_logic;
    GT2_RXUSRCLK2_OUT                       : out  std_logic;
 
    GT3_RXUSRCLK_OUT                        : out  std_logic;
    GT3_RXUSRCLK2_OUT                       : out  std_logic;

    --_________________________________________________________________________
    --GT0  (X0Y12)
    --____________________________CHANNEL PORTS________________________________
    ------------------------------- Loopback Ports -----------------------------
    gt0_loopback_in                         : in   std_logic_vector(2 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    gt0_eyescanreset_in                     : in   std_logic;
    gt0_rxuserrdy_in                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    gt0_eyescandataerror_out                : out  std_logic;
    gt0_eyescantrigger_in                   : in   std_logic;
    ------------------- Receive Ports - Digital Monitor Ports ------------------
    gt0_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt0_rxdata_out                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    gt0_rxdisperr_out                       : out  std_logic_vector(3 downto 0);
    gt0_rxnotintable_out                    : out  std_logic_vector(3 downto 0);
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    gt0_gthrxn_in                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    gt0_rxphmonitor_out                     : out  std_logic_vector(4 downto 0);
    gt0_rxphslipmonitor_out                 : out  std_logic_vector(4 downto 0);
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    gt0_rxbyteisaligned_out                 : out  std_logic;
    gt0_rxbyterealign_out                   : out  std_logic;
    gt0_rxcommadet_out                      : out  std_logic;
    --------------------- Receive Ports - RX Equalizer Ports -------------------
    gt0_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
    gt0_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
    gt0_rxoutclkfabric_out                  : out  std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    gt0_gtrxreset_in                        : in   std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    gt0_rxchariscomma_out                   : out  std_logic_vector(3 downto 0);
    gt0_rxcharisk_out                       : out  std_logic_vector(3 downto 0);
    ------------------------ Receive Ports -RX AFE Ports -----------------------
    gt0_gthrxp_in                           : in   std_logic;
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt0_rxresetdone_out                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    gt0_gttxreset_in                        : in   std_logic;

    --GT1  (X0Y13)
    --____________________________CHANNEL PORTS________________________________
    ------------------------------- Loopback Ports -----------------------------
    gt1_loopback_in                         : in   std_logic_vector(2 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    gt1_eyescanreset_in                     : in   std_logic;
    gt1_rxuserrdy_in                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    gt1_eyescandataerror_out                : out  std_logic;
    gt1_eyescantrigger_in                   : in   std_logic;
    ------------------- Receive Ports - Digital Monitor Ports ------------------
    gt1_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt1_rxdata_out                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    gt1_rxdisperr_out                       : out  std_logic_vector(3 downto 0);
    gt1_rxnotintable_out                    : out  std_logic_vector(3 downto 0);
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    gt1_gthrxn_in                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    gt1_rxphmonitor_out                     : out  std_logic_vector(4 downto 0);
    gt1_rxphslipmonitor_out                 : out  std_logic_vector(4 downto 0);
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    gt1_rxbyteisaligned_out                 : out  std_logic;
    gt1_rxbyterealign_out                   : out  std_logic;
    gt1_rxcommadet_out                      : out  std_logic;
    --------------------- Receive Ports - RX Equalizer Ports -------------------
    gt1_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
    gt1_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
    gt1_rxoutclkfabric_out                  : out  std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    gt1_gtrxreset_in                        : in   std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    gt1_rxchariscomma_out                   : out  std_logic_vector(3 downto 0);
    gt1_rxcharisk_out                       : out  std_logic_vector(3 downto 0);
    ------------------------ Receive Ports -RX AFE Ports -----------------------
    gt1_gthrxp_in                           : in   std_logic;
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt1_rxresetdone_out                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    gt1_gttxreset_in                        : in   std_logic;

    --GT2  (X0Y14)
    --____________________________CHANNEL PORTS________________________________
    ------------------------------- Loopback Ports -----------------------------
    gt2_loopback_in                         : in   std_logic_vector(2 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    gt2_eyescanreset_in                     : in   std_logic;
    gt2_rxuserrdy_in                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    gt2_eyescandataerror_out                : out  std_logic;
    gt2_eyescantrigger_in                   : in   std_logic;
    ------------------- Receive Ports - Digital Monitor Ports ------------------
    gt2_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt2_rxdata_out                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    gt2_rxdisperr_out                       : out  std_logic_vector(3 downto 0);
    gt2_rxnotintable_out                    : out  std_logic_vector(3 downto 0);
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    gt2_gthrxn_in                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    gt2_rxphmonitor_out                     : out  std_logic_vector(4 downto 0);
    gt2_rxphslipmonitor_out                 : out  std_logic_vector(4 downto 0);
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    gt2_rxbyteisaligned_out                 : out  std_logic;
    gt2_rxbyterealign_out                   : out  std_logic;
    gt2_rxcommadet_out                      : out  std_logic;
    --------------------- Receive Ports - RX Equalizer Ports -------------------
    gt2_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
    gt2_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
    gt2_rxoutclkfabric_out                  : out  std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    gt2_gtrxreset_in                        : in   std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    gt2_rxchariscomma_out                   : out  std_logic_vector(3 downto 0);
    gt2_rxcharisk_out                       : out  std_logic_vector(3 downto 0);
    ------------------------ Receive Ports -RX AFE Ports -----------------------
    gt2_gthrxp_in                           : in   std_logic;
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt2_rxresetdone_out                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    gt2_gttxreset_in                        : in   std_logic;

    --GT3  (X0Y15)
    --____________________________CHANNEL PORTS________________________________
    ------------------------------- Loopback Ports -----------------------------
    gt3_loopback_in                         : in   std_logic_vector(2 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    gt3_eyescanreset_in                     : in   std_logic;
    gt3_rxuserrdy_in                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    gt3_eyescandataerror_out                : out  std_logic;
    gt3_eyescantrigger_in                   : in   std_logic;
    ------------------- Receive Ports - Digital Monitor Ports ------------------
    gt3_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt3_rxdata_out                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    gt3_rxdisperr_out                       : out  std_logic_vector(3 downto 0);
    gt3_rxnotintable_out                    : out  std_logic_vector(3 downto 0);
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    gt3_gthrxn_in                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    gt3_rxphmonitor_out                     : out  std_logic_vector(4 downto 0);
    gt3_rxphslipmonitor_out                 : out  std_logic_vector(4 downto 0);
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    gt3_rxbyteisaligned_out                 : out  std_logic;
    gt3_rxbyterealign_out                   : out  std_logic;
    gt3_rxcommadet_out                      : out  std_logic;
    --------------------- Receive Ports - RX Equalizer Ports -------------------
    gt3_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
    gt3_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
    gt3_rxoutclkfabric_out                  : out  std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    gt3_gtrxreset_in                        : in   std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    gt3_rxchariscomma_out                   : out  std_logic_vector(3 downto 0);
    gt3_rxcharisk_out                       : out  std_logic_vector(3 downto 0);
    ------------------------ Receive Ports -RX AFE Ports -----------------------
    gt3_gthrxp_in                           : in   std_logic;
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt3_rxresetdone_out                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    gt3_gttxreset_in                        : in   std_logic;

    --____________________________COMMON PORTS________________________________
    GT0_QPLLLOCK_OUT : out std_logic;
    GT0_QPLLREFCLKLOST_OUT  : out std_logic;
    GT0_QPLLOUTCLK_OUT  : out std_logic;
    GT0_QPLLOUTREFCLK_OUT : out std_logic;
    sysclk_in        : in std_logic

);
end component;

--***********************************Parameter Declarations********************

--    constant DLY : time := 1 ns;

--************************** Register Declarations ****************************



    --____________________________COMMON PORTS________________________________
    ---------------------- Common Block  - Ref Clock Ports ---------------------
    signal  gt0_gtrefclk1_common_i          : std_logic;
    ------------------------- Common Block - QPLL Ports ------------------------
    signal  gt0_qplllock_i                  : std_logic;
    signal  gt0_qpllrefclklost_i            : std_logic;
    signal  gt0_qpllreset_i                 : std_logic;

    signal  tied_to_ground_i                : std_logic;
    signal  tied_to_ground_vec_i            : std_logic_vector(63 downto 0);
    signal  tied_to_vcc_i                   : std_logic;
    signal  tied_to_vcc_vec_i               : std_logic_vector(7 downto 0);


--**************************** Main Body of Code *******************************
begin

    --  Static signal Assigments
    tied_to_ground_i                             <= '0';
    tied_to_ground_vec_i                         <= x"0000000000000000";
    tied_to_vcc_i                                <= '1';
    tied_to_vcc_vec_i                            <= "11111111";

    ----------------------------- The GT Wrapper -----------------------------
    
    -- Use the instantiation template in the example directory to add the GT wrapper to your design.
    -- In this example, the wrapper is wired up for basic operation with a frame generator and frame 
    -- checker. The GTs will reset, then attempt to align and transmit data. If channel bonding is 
    -- enabled, bonding should occur after alignment.     

  U0 : min_latency_F1_quad213_11g2_rx_support
    generic map
    (
        EXAMPLE_SIM_GTRESET_SPEEDUP     =>      EXAMPLE_SIM_GTRESET_SPEEDUP,
        STABLE_CLOCK_PERIOD             =>      STABLE_CLOCK_PERIOD
    )
    port map
    (
        SOFT_RESET_RX_IN                =>      SOFT_RESET_RX_IN,
        DONT_RESET_ON_DATA_ERROR_IN     =>      tied_to_ground_i,
        Q3_CLK0_GTREFCLK_PAD_N_IN       =>      Q3_CLK0_GTREFCLK_PAD_N_IN,
        Q3_CLK0_GTREFCLK_PAD_P_IN       =>      Q3_CLK0_GTREFCLK_PAD_P_IN,

        GT0_TX_FSM_RESET_DONE_OUT       =>      GT0_TX_FSM_RESET_DONE_OUT,
        GT0_RX_FSM_RESET_DONE_OUT       =>      GT0_RX_FSM_RESET_DONE_OUT,
        GT0_DATA_VALID_IN               =>      tied_to_ground_i,       
        GT1_TX_FSM_RESET_DONE_OUT       =>      GT1_TX_FSM_RESET_DONE_OUT,
        GT1_RX_FSM_RESET_DONE_OUT       =>      GT1_RX_FSM_RESET_DONE_OUT,
        GT1_DATA_VALID_IN               =>      tied_to_ground_i,        
        GT2_TX_FSM_RESET_DONE_OUT       =>      GT2_TX_FSM_RESET_DONE_OUT,
        GT2_RX_FSM_RESET_DONE_OUT       =>      GT2_RX_FSM_RESET_DONE_OUT,
        GT2_DATA_VALID_IN               =>      tied_to_ground_i,        
        GT3_TX_FSM_RESET_DONE_OUT       =>      GT3_TX_FSM_RESET_DONE_OUT,
        GT3_RX_FSM_RESET_DONE_OUT       =>      GT3_RX_FSM_RESET_DONE_OUT,
        GT3_DATA_VALID_IN               =>      tied_to_ground_i,        
 
        GT0_RXUSRCLK_OUT => GT0_RXUSRCLK_OUT,
        GT0_RXUSRCLK2_OUT => open,
     
        GT1_RXUSRCLK_OUT => GT1_RXUSRCLK_OUT,
        GT1_RXUSRCLK2_OUT => open,
     
        GT2_RXUSRCLK_OUT => GT2_RXUSRCLK_OUT,
        GT2_RXUSRCLK2_OUT => open,
     
        GT3_RXUSRCLK_OUT => GT3_RXUSRCLK_OUT,
        GT3_RXUSRCLK2_OUT => open,


        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT0  (X0Y12)

        ------------------------------- Loopback Ports -----------------------------
        gt0_loopback_in                 =>      gt0_loopback_in,
        --------------------- RX Initialization and Reset Ports --------------------
        gt0_eyescanreset_in             =>      tied_to_ground_i,
        gt0_rxuserrdy_in                =>      tied_to_ground_i,
        -------------------------- RX Margin Analysis Ports ------------------------
        gt0_eyescandataerror_out        =>      open,
        gt0_eyescantrigger_in           =>      tied_to_ground_i,
        ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt0_dmonitorout_out             =>      open,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt0_rxdata_out                  =>      gt0_rxdata_out,
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt0_rxdisperr_out               =>      gt0_rxdisperr_out   ,
        gt0_rxnotintable_out            =>      gt0_rxnotintable_out,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt0_gthrxn_in                   =>      RXN_IN(0),
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt0_rxphmonitor_out             =>      open,
        gt0_rxphslipmonitor_out         =>      open,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt0_rxbyteisaligned_out         =>      gt0_rxbyteisaligned_out,
        gt0_rxbyterealign_out           =>      gt0_rxbyterealign_out,
        gt0_rxcommadet_out              =>      gt0_rxcommadet_out,
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt0_rxmonitorout_out            =>      open,
        gt0_rxmonitorsel_in             =>      "00",
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt0_rxoutclkfabric_out          =>      open,
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt0_gtrxreset_in                =>      tied_to_ground_i,
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt0_rxchariscomma_out           =>      gt0_rxchariscomma_out,
        gt0_rxcharisk_out               =>      gt0_rxcharisk_out,
        ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt0_gthrxp_in                   =>      RXP_IN(0),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt0_rxresetdone_out             =>      gt0_rxresetdone_out,
        --------------------- TX Initialization and Reset Ports --------------------
        gt0_gttxreset_in                =>      tied_to_ground_i,



        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT1  (X0Y13)

        ------------------------------- Loopback Ports -----------------------------
        gt1_loopback_in                 =>      gt1_loopback_in,
        --------------------- RX Initialization and Reset Ports --------------------
        gt1_eyescanreset_in             =>      tied_to_ground_i,
        gt1_rxuserrdy_in                =>      tied_to_ground_i,
        -------------------------- RX Margin Analysis Ports ------------------------
        gt1_eyescandataerror_out        =>      open,
        gt1_eyescantrigger_in           =>      tied_to_ground_i,
        ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt1_dmonitorout_out             =>      open,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt1_rxdata_out                  =>      gt1_rxdata_out,
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt1_rxdisperr_out               =>      gt1_rxdisperr_out   ,
        gt1_rxnotintable_out            =>      gt1_rxnotintable_out,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt1_gthrxn_in                   =>      RXN_IN(1),
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt1_rxphmonitor_out             =>      open,
        gt1_rxphslipmonitor_out         =>      open,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt1_rxbyteisaligned_out         =>      gt1_rxbyteisaligned_out,
        gt1_rxbyterealign_out           =>      gt1_rxbyterealign_out,
        gt1_rxcommadet_out              =>      gt1_rxcommadet_out,
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt1_rxmonitorout_out            =>      open,
        gt1_rxmonitorsel_in             =>      "00",
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt1_rxoutclkfabric_out          =>      open,
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt1_gtrxreset_in                =>      tied_to_ground_i,
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt1_rxchariscomma_out           =>      gt1_rxchariscomma_out,
        gt1_rxcharisk_out               =>      gt1_rxcharisk_out,
        ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt1_gthrxp_in                   =>      RXP_IN(1),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt1_rxresetdone_out             =>      gt1_rxresetdone_out,
        --------------------- TX Initialization and Reset Ports --------------------
        gt1_gttxreset_in                =>      tied_to_ground_i,



        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT2  (X0Y14)

        ------------------------------- Loopback Ports -----------------------------
        gt2_loopback_in                 =>      gt2_loopback_in,
        --------------------- RX Initialization and Reset Ports --------------------
        gt2_eyescanreset_in             =>      tied_to_ground_i,
        gt2_rxuserrdy_in                =>      tied_to_ground_i,
        -------------------------- RX Margin Analysis Ports ------------------------
        gt2_eyescandataerror_out        =>      open,
        gt2_eyescantrigger_in           =>      tied_to_ground_i,
        ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt2_dmonitorout_out             =>      open,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt2_rxdata_out                  =>      gt2_rxdata_out,
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt2_rxdisperr_out               =>      gt2_rxdisperr_out   ,
        gt2_rxnotintable_out            =>      gt2_rxnotintable_out,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt2_gthrxn_in                   =>      RXN_IN(2),
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt2_rxphmonitor_out             =>      open,
        gt2_rxphslipmonitor_out         =>      open,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt2_rxbyteisaligned_out         =>      gt2_rxbyteisaligned_out,
        gt2_rxbyterealign_out           =>      gt2_rxbyterealign_out,
        gt2_rxcommadet_out              =>      gt2_rxcommadet_out,
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt2_rxmonitorout_out            =>      open,
        gt2_rxmonitorsel_in             =>      "00",
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt2_rxoutclkfabric_out          =>      open,
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt2_gtrxreset_in                =>      tied_to_ground_i,
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt2_rxchariscomma_out           =>      gt2_rxchariscomma_out,
        gt2_rxcharisk_out               =>      gt2_rxcharisk_out,
        ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt2_gthrxp_in                   =>      RXP_IN(2),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt2_rxresetdone_out             =>      gt2_rxresetdone_out,
        --------------------- TX Initialization and Reset Ports --------------------
        gt2_gttxreset_in                =>      tied_to_ground_i,



        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT3  (X0Y15)

        ------------------------------- Loopback Ports -----------------------------
        gt3_loopback_in                 =>      gt3_loopback_in,
        --------------------- RX Initialization and Reset Ports --------------------
        gt3_eyescanreset_in             =>      tied_to_ground_i,
        gt3_rxuserrdy_in                =>      tied_to_ground_i,
        -------------------------- RX Margin Analysis Ports ------------------------
        gt3_eyescandataerror_out        =>      open,
        gt3_eyescantrigger_in           =>      tied_to_ground_i,
        ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt3_dmonitorout_out             =>      open,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt3_rxdata_out                  =>      gt3_rxdata_out,
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt3_rxdisperr_out               =>      gt3_rxdisperr_out   ,
        gt3_rxnotintable_out            =>      gt3_rxnotintable_out,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt3_gthrxn_in                   =>      RXN_IN(3),
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt3_rxphmonitor_out             =>      open,
        gt3_rxphslipmonitor_out         =>      open,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt3_rxbyteisaligned_out         =>      gt3_rxbyteisaligned_out,
        gt3_rxbyterealign_out           =>      gt3_rxbyterealign_out,
        gt3_rxcommadet_out              =>      gt3_rxcommadet_out,
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt3_rxmonitorout_out            =>      open,
        gt3_rxmonitorsel_in             =>      "00",
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt3_rxoutclkfabric_out          =>      open,
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt3_gtrxreset_in                =>      tied_to_ground_i,
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt3_rxchariscomma_out           =>      gt3_rxchariscomma_out,
        gt3_rxcharisk_out               =>      gt3_rxcharisk_out,
        ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt3_gthrxp_in                   =>      RXP_IN(3),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt3_rxresetdone_out             =>      gt3_rxresetdone_out,
        --------------------- TX Initialization and Reset Ports --------------------
        gt3_gttxreset_in                =>      tied_to_ground_i,



    --____________________________COMMON PORTS________________________________
     GT0_QPLLLOCK_OUT       => GT0_QPLLLOCK_OUT,
     GT0_QPLLREFCLKLOST_OUT  => GT0_QPLLREFCLKLOST_OUT,
     GT0_QPLLOUTCLK_OUT     =>  open,
     GT0_QPLLOUTREFCLK_OUT  => open,
     sysclk_in              => sysclk_in
    );

end;