
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

entity tobo_frame_sm IS
  port( 
      bcn        : in    std_logic_vector (3 DOWNTO 0);
      clk        : in    std_logic;
      reset      : in    std_logic;
      tobo_data  : in    std_logic_vector (31 DOWNTO 0);
      tobo_sync  : in    std_logic;
      tobo_valid : in    std_logic;
      tx_ctrl    : out   std_logic;
      tobo_en    : out   std_logic;
      tx_data    : out   std_logic_vector (31 DOWNTO 0)
   );



end entity tobo_frame_sm ;

 
architecture   Behavioral of tobo_frame_sm is


   signal cntr:unsigned(3 downto 0);

   type state_type is  (
      idle,
      str,
      fin,
      empty_data
   ); 

  signal  current_state : state_type;

begin

   -----------------------------------------------------------------
   clocked_proc : process ( 
      clk,reset
   )
   -----------------------------------------------------------------
   begin
      if (clk' event and  clk = '1') then
         if (reset = '1') THEN
             current_state <= idle;
             -- Default Reset Values
            tx_ctrl <= '0';
            tx_data <= (others => '0');
             cntr <= (others => '0');
             tobo_en <= '0';
           else
         -- Combined Actions
         case current_state IS
            when idle => 
               cntr <= (others => '0');
               tx_data <= x"0000003c";
               tx_ctrl <= '1';
               tobo_en <= '0';
               if (tobo_sync = '1' and tobo_valid ='1') then 
                  tx_data <= tobo_data ;
                  tx_ctrl <= '0' ;
                  current_state <= str;
               else
                  current_state <= idle;
               end if;
            when str => 
               tx_data <= tobo_data ;
               tx_ctrl <= '0' ;
               tobo_en <= '1';
               cntr <= cntr +1;
               if (tobo_valid = '0') then 
                  tx_data <= (others => '0') ;
                  current_state <= empty_data;
               elsif (cntr = 5) then 
                  cntr <= (others => '0') ;
                  tx_data <= x"00" &'0' & bcn &"000" &x"0000" ;
                  tx_ctrl <= '1' ;
                  current_state <= fin;
               else
                  current_state <= str;
               end if;
            when fin => 
               tx_data <= x"00" &'0' & bcn &"000" &x"0000" ; 
               tx_ctrl <= '1' ;
                tobo_en <= '1';
               cntr <= (others => '0') ;
               if (tobo_valid ='1') then 
                  tx_data <= tobo_data ;
                  tx_ctrl <= '0' ;
                  current_state <= str;
               elsif (tobo_valid = '0') then 
                  tx_data <= x"0000003c";
                  tx_ctrl <= '1';
                  current_state <= idle;
               else
                  current_state <= fin;
               end if;
            when empty_data => 
               tx_data <= (others => '0') ;
               tx_ctrl <= '0' ;
                tobo_en <= '1';
               cntr <= cntr +1;
               if (cntr = 5) then 
                  cntr <= (others => '0') ;
                  tx_data <= x"00" &'0' & bcn &"000" &x"0000" ;
                  tx_ctrl <= '1' ;
                  current_state <= fin;
               else
                  current_state <= empty_data;
               end if;
            when OTHERS =>
               current_state <= idle;
         end case;
      end if;
      end if;
   end  process clocked_proc;
 
end  Behavioral ;
