### TTC FANOUT SIGNALS

set_property IOSTANDARD LVDS [get_ports {ttc_L1A_n[*]}]
set_property IOSTANDARD LVDS [get_ports {ttc_L1A_p[*]}]
set_property IOSTANDARD LVDS [get_ports {ttc_BCR_n[*]}]
set_property IOSTANDARD LVDS [get_ports {ttc_BCR_p[*]}]
set_property IOSTANDARD LVDS [get_ports {ttc_ECR_n[*]}]
set_property IOSTANDARD LVDS [get_ports {ttc_ECR_p[*]}]
set_property IOSTANDARD LVDS [get_ports {ttc_pr_rdout_n[*]}]
set_property IOSTANDARD LVDS [get_ports {ttc_pr_rdout_p[*]}]

set_output_delay -clock clk40_clk_ttc -min -5.500 [get_ports {ttc_L1A_n[*]}]
set_output_delay -clock clk40_clk_ttc -max 5.500 [get_ports {ttc_L1A_n[*]}]
set_output_delay -clock clk40_clk_ttc -min -5.500 [get_ports {ttc_L1A_p[*]}]
set_output_delay -clock clk40_clk_ttc -max 5.500 [get_ports {ttc_L1A_p[*]}]
set_output_delay -clock clk40_clk_ttc -min -5.500 [get_ports {ttc_BCR_n[*]}]
set_output_delay -clock clk40_clk_ttc -max 5.500 [get_ports {ttc_BCR_n[*]}]
set_output_delay -clock clk40_clk_ttc -min -5.500 [get_ports {ttc_BCR_p[*]}]
set_output_delay -clock clk40_clk_ttc -max 5.500 [get_ports {ttc_BCR_p[*]}]
set_output_delay -clock clk40_clk_ttc -min -5.500 [get_ports {ttc_ECR_n[*]}]
set_output_delay -clock clk40_clk_ttc -max 5.500 [get_ports {ttc_ECR_n[*]}]
set_output_delay -clock clk40_clk_ttc -min -5.500 [get_ports {ttc_ECR_p[*]}]
set_output_delay -clock clk40_clk_ttc -max 5.500 [get_ports {ttc_ECR_p[*]}]
set_output_delay -clock clk40_clk_ttc -min -5.500 [get_ports {ttc_pr_rdout_n[*]}]
set_output_delay -clock clk40_clk_ttc -max 5.500 [get_ports {ttc_pr_rdout_n[*]}]

set_property PACKAGE_PIN AM22 [get_ports {ttc_L1A_p[0]}]
set_property PACKAGE_PIN AN22 [get_ports {ttc_L1A_n[0]}]
set_property PACKAGE_PIN AM13 [get_ports {ttc_L1A_p[1]}]
set_property PACKAGE_PIN AN13 [get_ports {ttc_L1A_n[1]}]
set_property PACKAGE_PIN C28 [get_ports {ttc_L1A_p[2]}]
set_property PACKAGE_PIN C29 [get_ports {ttc_L1A_n[2]}]
set_property PACKAGE_PIN J14 [get_ports {ttc_L1A_p[3]}]
set_property PACKAGE_PIN H14 [get_ports {ttc_L1A_n[3]}]

set_property PACKAGE_PIN AN24 [get_ports {ttc_BCR_p[0]}]
set_property PACKAGE_PIN AP24 [get_ports {ttc_BCR_n[0]}]
set_property PACKAGE_PIN AM12 [get_ports {ttc_BCR_p[1]}]
set_property PACKAGE_PIN AN12 [get_ports {ttc_BCR_n[1]}]
set_property PACKAGE_PIN E24 [get_ports {ttc_BCR_p[2]}]
set_property PACKAGE_PIN D25 [get_ports {ttc_BCR_n[2]}]
set_property PACKAGE_PIN H17 [get_ports {ttc_BCR_p[3]}]
set_property PACKAGE_PIN G17 [get_ports {ttc_BCR_n[3]}]


set_property PACKAGE_PIN AE14 [get_ports {ttc_ECR_p[0]}]
set_property PACKAGE_PIN AF14 [get_ports {ttc_ECR_n[0]}]
set_property PACKAGE_PIN AP12 [get_ports {ttc_ECR_p[1]}]
set_property PACKAGE_PIN AP11 [get_ports {ttc_ECR_n[1]}]
set_property PACKAGE_PIN A28 [get_ports {ttc_ECR_p[2]}]
set_property PACKAGE_PIN A29 [get_ports {ttc_ECR_n[2]}]
set_property PACKAGE_PIN F14 [get_ports {ttc_ECR_p[3]}]
set_property PACKAGE_PIN E13 [get_ports {ttc_ECR_n[3]}]

set_property PACKAGE_PIN AN20 [get_ports {ttc_pr_rdout_p[0]}]
set_property PACKAGE_PIN AP20 [get_ports {ttc_pr_rdout_n[0]}]
set_property PACKAGE_PIN AJ12 [get_ports {ttc_pr_rdout_p[1]}]
set_property PACKAGE_PIN AK12 [get_ports {ttc_pr_rdout_n[1]}]
set_property PACKAGE_PIN G26 [get_ports {ttc_pr_rdout_p[2]}]
set_property PACKAGE_PIN F26 [get_ports {ttc_pr_rdout_n[2]}]
set_property PACKAGE_PIN G13 [get_ports {ttc_pr_rdout_p[3]}]
set_property PACKAGE_PIN F13 [get_ports {ttc_pr_rdout_n[3]}]

# Control FPGA ready signals to all processor FPGAs
#  F*_F5_RDOUT_p is used for cntl_RAW_rdy_F*_out
#  F*_F5_RDOUT_n is used for cntl_TOB_rdy_F*_out
set_property IOSTANDARD LVCMOS18 [get_ports cntl_RAW_rdy_F1_out]
set_property IOSTANDARD LVCMOS18 [get_ports cntl_TOB_rdy_F1_out]
set_property IOSTANDARD LVCMOS18 [get_ports cntl_RAW_rdy_F2_out]
set_property IOSTANDARD LVCMOS18 [get_ports cntl_TOB_rdy_F2_out]
set_property IOSTANDARD LVCMOS18 [get_ports cntl_RAW_rdy_F3_out]
set_property IOSTANDARD LVCMOS18 [get_ports cntl_TOB_rdy_F3_out]
set_property IOSTANDARD LVCMOS18 [get_ports cntl_RAW_rdy_F4_out]
set_property IOSTANDARD LVCMOS18 [get_ports cntl_TOB_rdy_F4_out]

set_property PACKAGE_PIN AB33 [get_ports cntl_RAW_rdy_F1_out]
set_property PACKAGE_PIN AC34 [get_ports cntl_TOB_rdy_F1_out]
set_property PACKAGE_PIN AG12 [get_ports cntl_RAW_rdy_F2_out]
set_property PACKAGE_PIN AH12 [get_ports cntl_TOB_rdy_F2_out]
set_property PACKAGE_PIN B33 [get_ports cntl_RAW_rdy_F3_out]
set_property PACKAGE_PIN A33 [get_ports cntl_TOB_rdy_F3_out]
set_property PACKAGE_PIN E11 [get_ports cntl_RAW_rdy_F4_out]
set_property PACKAGE_PIN D11 [get_ports cntl_TOB_rdy_F4_out]

# These are the ECRID constrain pins that are for Process FPGA.
# ttc_5 TO ttc_8 p and n was used as single ended
#ECRID from F5 to F1

set_property IOSTANDARD LVCMOS18 [get_ports {ECRID_F1[*]}]
set_output_delay -clock clk40_clk_ttc -min -5.500 [get_ports {ECRID_F1[*]}]
set_output_delay -clock clk40_clk_ttc -max 5.500 [get_ports {ECRID_F1[*]}]
set_property PACKAGE_PIN AC17 [get_ports {ECRID_F1[0]}]
set_property PACKAGE_PIN AD17 [get_ports {ECRID_F1[1]}]
set_property PACKAGE_PIN AE17 [get_ports {ECRID_F1[2]}]
set_property PACKAGE_PIN AE16 [get_ports {ECRID_F1[3]}]
set_property PACKAGE_PIN AF13 [get_ports {ECRID_F1[4]}]
set_property PACKAGE_PIN AG13 [get_ports {ECRID_F1[5]}]
set_property PACKAGE_PIN AD14 [get_ports {ECRID_F1[6]}]
set_property PACKAGE_PIN AE13 [get_ports {ECRID_F1[7]}]

#ECRID from F5 to F2

set_property IOSTANDARD LVCMOS18 [get_ports {ECRID_F2[*]}]
set_output_delay -clock clk40_clk_ttc -min -5.500 [get_ports {ECRID_F2[*]}]
set_output_delay -clock clk40_clk_ttc -max 5.500 [get_ports {ECRID_F2[*]}]
set_property PACKAGE_PIN AK9  [get_ports {ECRID_F2[0]}]
set_property PACKAGE_PIN AL9  [get_ports {ECRID_F2[1]}]
set_property PACKAGE_PIN AN9  [get_ports {ECRID_F2[2]}]
set_property PACKAGE_PIN AP9  [get_ports {ECRID_F2[3]}]
set_property PACKAGE_PIN AL10 [get_ports {ECRID_F2[4]}]
set_property PACKAGE_PIN AM10 [get_ports {ECRID_F2[5]}]
set_property PACKAGE_PIN AL11 [get_ports {ECRID_F2[6]}]
set_property PACKAGE_PIN AM11 [get_ports {ECRID_F2[7]}]

#ECRID from F5 to F3

set_property IOSTANDARD LVCMOS18 [get_ports {ECRID_F3[*]}]
set_output_delay -clock clk40_clk_ttc -min -5.500 [get_ports {ECRID_F3[*]}]
set_output_delay -clock clk40_clk_ttc -max 5.500 [get_ports {ECRID_F3[*]}]
set_property PACKAGE_PIN D29 [get_ports {ECRID_F3[0]}]
set_property PACKAGE_PIN D30 [get_ports {ECRID_F3[1]}]
set_property PACKAGE_PIN J24 [get_ports {ECRID_F3[2]}]
set_property PACKAGE_PIN H24 [get_ports {ECRID_F3[3]}]
set_property PACKAGE_PIN E28 [get_ports {ECRID_F3[4]}]
set_property PACKAGE_PIN E29 [get_ports {ECRID_F3[5]}]
set_property PACKAGE_PIN J25 [get_ports {ECRID_F3[6]}]
set_property PACKAGE_PIN H25 [get_ports {ECRID_F3[7]}]

#ECRID from F5 to F4

set_property IOSTANDARD LVCMOS18 [get_ports {ECRID_F4[*]}]
set_output_delay -clock clk40_clk_ttc -min -5.500 [get_ports {ECRID_F4[*]}]
set_output_delay -clock clk40_clk_ttc -max 5.500 [get_ports {ECRID_F4[*]}]
set_property PACKAGE_PIN E14 [get_ports {ECRID_F4[0]}]
set_property PACKAGE_PIN D14 [get_ports {ECRID_F4[1]}]
set_property PACKAGE_PIN G15 [get_ports {ECRID_F4[2]}]
set_property PACKAGE_PIN F15 [get_ports {ECRID_F4[3]}]
set_property PACKAGE_PIN E17 [get_ports {ECRID_F4[4]}]
set_property PACKAGE_PIN D17 [get_ports {ECRID_F4[5]}]
set_property PACKAGE_PIN E16 [get_ports {ECRID_F4[6]}]
set_property PACKAGE_PIN D16 [get_ports {ECRID_F4[7]}]
