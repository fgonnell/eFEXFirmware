#vivado
set BIN_FILE 1

## FPGA and Vivado strategies and flows
set FPGA xc7vx550tffg1927-2
set SYNTH_STRATEGY "Vivado Synthesis Defaults" 
set SYNTH_FLOW "Vivado Synthesis 2019"
set IMPL_STRATEGY "Performance_Retiming"
set IMPL_FLOW "Vivado Implementation 2019"

### Set Vivado Runs Properties ###
#
# ATTENTION: The \ character must be the last one of each line
#
# The default Vivado run names are: synth_1 for synthesis and impl_1 for implementation.
#
# To find out the exact name and value of the property, use Vivado GUI to click on the checkbox you like.
# This will make Vivado run the set_property command in the Tcl console.
# Then copy and paste the name and the values from the Vivado Tcl console into the lines below.

set PROPERTIES [dict create \
		    synth_1 [dict create \
				STEPS.SYNTH_DESIGN.ARGS.FANOUT_LIMIT 10000 \
				STEPS.SYNTH_DESIGN.ARGS.RETIMING true \
				] \
		    impl_1 [dict create \
				STEPS.OPT_DESIGN.ARGS.DIRECTIVE Default \
			       ]\
		   ]
############################################################

set DESIGN    "[file rootname [file tail [info script]]]"
set PATH_REPO "[file normalize [file dirname [info script]]]/../../"
source $PATH_REPO/Hog/Tcl/create_project.tcl
