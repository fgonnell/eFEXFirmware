onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/clk40
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/clk200
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/rst
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/DataReady
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/output_tobRd
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/ParD3
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/ParCEta
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/ParHadron
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/Control
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/Status
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/AlgoData
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/ipb_rst
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/ipb_clk
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/ipb_in
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/ipb_out
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/clkTOB
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/eg_TOBRd
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/eg_TOB
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/tau_TOBRd
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/tau_TOB
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/load
add wave -noupdate -radix hexadecimal -radixshowbase 0 /tb_algomodule/locked
add wave -noupdate -divider <NULL>
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/CLK200
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/RESET
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/IN_Load
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/ipb_clk
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/ipb_rst
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/ipb_in
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/ipb_out
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/IN_Data
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/OUT_eg_TOBRd
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/OUT_eg_TOB
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/OUT_tau_TOBRd
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/OUT_tau_TOB
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/eg_ParD3
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/eg_ParCEta
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/eg_ParHadron
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/tau_ParD3
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/tau_ParCEta
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/tau_ParHadron
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/eg_Control
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/tau_Control
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/eg_Status
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/tau_Status
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/glob_Position
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/glob_Control
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/glob_Status
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/ipb_to_slaves
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/ipb_from_slaves
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/write_reg
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/read_reg
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/ParRamData
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/ParRamAddress
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/CG_Load
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/Load
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/eg_TOBs
add wave -noupdate -expand -group Top -radix hexadecimal -radixshowbase 0 /tb_algomodule/TOPALGO/tau_TOBs
add wave -noupdate -group InputStage /tb_algomodule/TOPALGO/INPUT_STAGE/CLK200
add wave -noupdate -group InputStage /tb_algomodule/TOPALGO/INPUT_STAGE/IN_Load
add wave -noupdate -group InputStage /tb_algomodule/TOPALGO/INPUT_STAGE/IN_Data
add wave -noupdate -group InputStage /tb_algomodule/TOPALGO/INPUT_STAGE/IN_Position
add wave -noupdate -group InputStage /tb_algomodule/TOPALGO/INPUT_STAGE/OUT_Load
add wave -noupdate -group InputStage /tb_algomodule/TOPALGO/INPUT_STAGE/OutLoad
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/CLK200
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/RESET
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_Load
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_ParameterRAMaddress
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_eg_ParD3
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_eg_ParCEta
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_eg_ParHadron
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_eg_Control
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_eg_Status
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_tau_ParD3
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_tau_ParCEta
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_tau_ParHadron
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_tau_Control
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_tau_Status
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_glob_Position
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/IN_glob_Control
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_glob_Status
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_eg_TOBRd
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_eg_TOB
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_tau_TOBRd
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OUT_tau_TOB
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OutEgTOBRd
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OutEgTOB
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OutTauTOBRd
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/OutTauTOB
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/SR_LeftRight
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/SR_OutData
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/eg_ParD3
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/eg_ParCEta
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/eg_ParHadron
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/eg_Control
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/eg_Status
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/eg_TOBRd
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/eg_TOB
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/tau_ParD3
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/tau_ParCEta
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/tau_ParHadron
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/tau_Control
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/tau_Status
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/tau_TOBRd
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/tau_TOB
add wave -noupdate -group TopAlgo /tb_algomodule/TOPALGO/TOP_ALGO_MODULE/RAMCounter
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1025000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 194
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix -1
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {279980 ps} {1767052 ps}
