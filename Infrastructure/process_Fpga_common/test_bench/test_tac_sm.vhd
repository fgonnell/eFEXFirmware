----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/29/2016 06:57:55 PM
-- Design Name: 
-- Module Name: test_tac_sm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


use ieee.std_logic_unsigned.all;
library algolib;
use algolib.AlgoDataTypes.all;



LIBRARY infrastructure_lib;--LIBRARY xil_defaultlib;
use  infrastructure_lib.all; 
----------------------------

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity test_tac_sm is
--  Port ( );
end test_tac_sm;

architecture Behavioral of test_tac_sm is
SIGNAL CLK_280M, CLK_160M       : std_logic;
SIGNAL MGT_COMMADET : std_logic;
SIGNAL Delay_cnt    : std_logic_vector(3 DOWNTO 0);
SIGNAL Mux_Delay1   : std_logic_vector(2 DOWNTO 0);
SIGNAL TTC_clk      : std_logic;
SIGNAL cntr_enable  : std_logic;
SIGNAL cntrl_mux    : std_logic_vector(1 DOWNTO 0);
SIGNAL data_in      : std_logic_vector(31 DOWNTO 0);
SIGNAL data_out     : std_logic_vector(223 DOWNTO 0);
SIGNAL mux_cntrl,data,delay_num,mux_sel     : std_logic_vector(3 DOWNTO 0);
SIGNAL reset     : std_logic;
signal start        : std_logic;
SIGNAL data_out_reg224 :std_logic_vector (223 downto 0);

signal reg224_latch: std_logic;
signal enable_mgt,ttc_pipe,bcn_synch,delay_latch : std_logic;
signal crc_cntr: std_logic_vector (15 downto 0);
signal disperr_error_i, notable_error_i : std_logic;
signal  data_in_int :std_logic_vector( 33 downto 0);
signal crc_error, crc_error_40:std_logic;

begin

crc_count:entity work.counter 
        Port map(  
             clk       => clk_280M, 
             enable2   => '1',
             enable1   => crc_error, 
             count     => crc_cntr,
             reset     => reset 
            
              );          

test_bench:entity work.testbench_tac_sm
      PORT MAP (
         CLK_280M         => CLK_280M,
         CLK_160M          => CLK_160M,
         MGT_COMMADET => MGT_COMMADET,
         cntrl_mux           => cntrl_mux,
         TTC_clk               => TTC_clk,
         data_in               => data_in,
         enable_mgt        => enable_mgt,
         reset                  => reset,
         start                   => start,
         disperr_error      =>  disperr_error_i,
         notable_error     =>  notable_error_i ,
         stop                  =>  open
      );
 
 --- crc generation
    u1 : entity work.crc_checker
      port map (
        clk               => CLK_280M,
        reset            => reset,
        mgt_commdet => MGT_COMMADET,
        rxdata          => data_in,
        crc_error      => crc_error , 
        crc_error_40 => crc_error_40
        );   

   
               
top_level: entity work.top_synch

       PORT MAP (
           rx_clk280    =>  clk_280M,
           TTC_clk      =>  TTC_clk ,
           reset        =>  reset ,
           enable_mgt   => enable_mgt,
           MGT_Commadet =>  MGT_Commadet, 
           reg_sel       => "0000", 
           mux_sel       => "0000", 
           start         =>  start, 
           rx_resetdone => '1',
           reg224_latch  =>  reg224_latch,
           delay_latch   => delay_latch,
           ttc_pipe     => ttc_pipe,
           delay_num     => delay_num, 
           bcn_synch    => bcn_synch,
           data_out_reg224 => data_out_reg224,              
           data_in       =>  data_in,
           data_out      =>  data_out,
           disperr_error =>  disperr_error_i,
          notable_error =>   notable_error_i,
          crc_error_40   => crc_error_40
           );
           

           

 

end Behavioral;
