-- Dave Sankey, August 2019
-- Reflects address as data on read and latches last write signal

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity TB_ROM is
  generic (
 	ADDR_WIDTH  : integer :=   16	-- width of simulated address bus
 	);
	port(
		clk: in std_logic;
		addr: in std_logic_vector(ADDR_WIDTH - 1 downto 0);
		we: in std_logic := '0';
		d: in std_logic_vector(63 downto 0);
		q: out std_logic_vector(63 downto 0);
		debug: out std_logic_vector(63 downto 0)
	);

end TB_ROM;

architecture reflector of TB_ROM is

Begin

ROM: Process(clk)
  Variable octet_high, octet_low: std_logic_vector(7 downto 0) := (Others => '0');
  Variable debug_int: std_logic_vector(63 downto 0) := (Others => '0');
  Begin
    If rising_edge(clk) then
      if we = '1' then
        debug_int := d;
      end if;
      octet_low(7 downto 1) := addr(6 downto 0);
      octet_high(7 downto 1) := addr(6 downto 0);
      octet_high(0) := '1';
      q <= octet_high & octet_high & octet_high & octet_high & octet_low & octet_low & octet_low & octet_low
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      debug <= debug_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  End process ROM;

End architecture reflector;
