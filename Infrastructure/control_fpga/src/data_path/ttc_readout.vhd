LIBRARY ieee;
USE ieee.std_logic_1164.all;
use work.ipbus.all;
use work.packet_mux_type.all;

LIBRARY infrastructure_lib;
USE infrastructure_lib.all;
use infrastructure_lib.mgt_type.all;

ENTITY ttc_readout IS
	GENERIC(
		NPacket_sources: positive := 2;
		INPUT_RAM_ADDR_WIDTH: positive := 12
	);
	PORT (
		Q_CLK_GTREFCLK_PAD_N_IN  : in std_logic_vector(1 downto 0);
		Q_CLK_GTREFCLK_PAD_P_IN  : in std_logic_vector(1 downto 0);
      rxp_IN                   : in std_logic_vector(5 downto 0);
      rxn_IN                   : in std_logic_vector(5 downto 0);
      txp_OUT                  : out std_logic_vector(3 downto 0);
      txn_OUT                  : out std_logic_vector(3 downto 0);
      --- aurora hub1 links
     -- aurora_txp       :  out std_logic_vector (3 DOWNTO 0);
     -- aurora_txn       :  out std_logic_vector (3 DOWNTO 0);
      aurora_refclk1_p :  in  std_logic;
      aurora_refclk1_n :  in  std_logic;
      --- aurora hub2 links
      aurora_hub2_txp       :  out std_logic_vector (3 DOWNTO 0);
      aurora_hub2_txn       :  out std_logic_vector (3 DOWNTO 0);
      -- aurora_hub2_refclk1_p :  in  std_logic;
      -- aurora_hub2_refclk1_n :  in  std_logic;
	  --- throttle signals to processor FPGAs
	  cntl_RAW_rdy_F1_out	:  out  std_logic;
	  cntl_TOB_rdy_F1_out	:  out  std_logic;
	  cntl_RAW_rdy_F2_out	:  out  std_logic;
	  cntl_TOB_rdy_F2_out	:  out  std_logic;
	  cntl_RAW_rdy_F3_out	:  out  std_logic;
	  cntl_TOB_rdy_F3_out	:  out  std_logic;
	  cntl_RAW_rdy_F4_out	:  out  std_logic;
      cntl_TOB_rdy_F4_out	:  out  std_logic
	);
END ENTITY ttc_readout;

Architecture rtl of ttc_readout is

COMPONENT top_mgt_cfpga is
	Port (
		clk40                    : in  std_logic;                     -- clk40 generatred from ttc clock
		start                    : in  std_logic;
		clk160                   : in  std_logic;
		rx_clk160                : out std_logic_vector(7 downto 0); --11 --- Generated rx clocks of 160MHz from the mgt pll.- Generated rx clocks of 160MHz from the mgt pll.
		Q_CLK_GTREFCLK_PAD_N_IN  : in  std_logic_vector(1 downto 0);  --2
		Q_CLK_GTREFCLK_PAD_P_IN  : in  std_logic_vector(1 downto 0);  --2
		RXN_IN                   : in  std_logic_vector(7 downto 0); --11
		RXP_IN                   : in  std_logic_vector(7 downto 0); --11
		TXN_OUT                  : out std_logic_vector(3 downto 0);   --7
		TXP_OUT                  : out std_logic_vector(3 downto 0);   --7
		rx_resetdone             : out std_logic_vector(7 downto 0);  --11
		rx_fsm_resetdone         : out std_logic_vector(7 downto 0);  --11
		rx_byteisaligned         : out std_logic_vector(7 downto 0);  --11
		tx_resetdone             : out std_logic_vector(7 downto 0);  --11
		tx_fsm_resetdone         : out std_logic_vector(7 downto 0);  --11
		tx_bufstatus             : out std_logic_vector (15 downto 0); --23
		rx_realign               : out std_logic_vector (7 downto 0); --11
		rx_disperr               : out std_logic_vector (31 downto 0); --47  -- rx_disperr for debug
		encode_error             : out std_logic_vector (31 downto 0); --47  --- 10/8 encoder for debug
		mgt_commadret            : out std_logic_vector(5 downto 0);   --9
		loopback                 : in  std_logic_vector(2 downto 0);   --5
		mgt_SOFT_RESET_TX_IN     : in  std_logic_vector(1 downto 0);   --2
		mgt_SOFT_RESET_RX_IN     : in  std_logic_vector(1 downto 0);   --2
		topo_data_fpga1          : out std_logic_vector(31 downto 0);   -- topo tob data from fpga1
		topo_data_fpga2          : out std_logic_vector(31 downto 0);   -- topo tob data from fpga2
		--topo_data_fpga3          : out std_logic_vector(31 downto 0);   -- topo tob data from fpga3
		-- topo_data_fpga4          : out std_logic_vector(31 downto 0);   -- topo tob data from fpga4
		raw_data_fpga1           : out std_logic_vector(31 downto 0);   -- raw  data from fpga1
		raw_data_fpga2           : out std_logic_vector(31 downto 0);   -- raw  data from fpga2
		--raw_data_fpga3           : out std_logic_vector(31 downto 0);   -- raw  data from fpga3
		--raw_data_fpga4           : out std_logic_vector(31 downto 0);    -- raw  data from fpga4
		ttc_rx_data              : out std_logic_vector(31 downto 0);  -- ttc information
		rod_busy                 : out std_logic_vector(31 downto 0)   -- rod busy.
	);
end COMPONENT top_mgt_cfpga;

COMPONENT top_cntrl_synch is
	Port (
		rx_clk160    : in  std_logic;    -- rx clock of the mgt
		TTC_clk      : IN  std_logic;    -- ttc clk of 40MHz
		reset        : in  std_logic;
		enable_mgt   : in  std_logic;    --- if high enable the mgt rx register
		MGT_Commadet : in  std_logic;    -- comma detected for incoming data
		reg_sel      : in  std_logic_vector(3 downto 0); -- setting BC mux
		mux_sel      : in  std_logic_vector(3 downto 0);  -- setting the first stage mux
		delay_num    : out std_logic_vector(3 downto 0); -- counted delay on the first stage of the synchronisation
		start        : in  std_logic;    -- start pulse for the calibration to start
		rx_resetdone : in   std_logic;
		reg128_latch : out  std_logic;
		data_out     : out std_logic_vector(127  downto 0);
		data_in      : in  std_logic_vector(31 downto 0)
	);
end COMPONENT top_cntrl_synch;

COMPONENT aurora_hub2 is
	Port (
		-- TX Stream Interface
		s_axi_tx_tdata      : in  std_logic_vector(63 downto 0);
		s_axi_tx_tvalid     : in  std_logic;
		s_axi_tx_tready     : out std_logic;
		s_axi_tx_tkeep      : in std_logic_vector(7 downto 0);
		s_axi_tx_tlast      : in  std_logic;
		-- User Flow Control TX Interface
		s_axi_ufc_tx_req    : in std_logic;
		s_axi_ufc_tx_ms     : in std_logic_vector(2 downto 0);
		s_axi_ufc_tx_ack    : out std_logic;
		-- V7 Serial I/O
		txp                 : out std_logic_vector(0 to 3);
		txn                 : out std_logic_vector(0 to 3);
		-- GT Reference Clock Interface
		gt_refclk1_p        : in std_logic;
		gt_refclk1_n        : in std_logic;
		-- Error Detection Interface
		tx_hard_err         : out std_logic;
		-- Status
		tx_channel_up       : out std_logic;
		tx_lane_up          : out std_logic_vector(0 to 3);
		-- System Interface
		user_clk_out        : out std_logic;
		sys_reset_out       : out std_logic;
		tx_lock             : out std_logic;
		init_clk            : in  std_logic;
		init_clk_out        : out  std_logic;
		pll_not_locked      : out std_logic;
		tx_resetdone        : out std_logic;
		link_reset          : in std_logic
	);
end COMPONENT aurora_hub2;

COMPONENT axi_stream_fifo is
  Port (
    wr_rst_busy : out STD_LOGIC;
    rd_rst_busy : out STD_LOGIC;
    m_aclk : in STD_LOGIC;
    s_aclk : in STD_LOGIC;
    s_aresetn : in STD_LOGIC;
    s_axis_tvalid : in STD_LOGIC;
    s_axis_tready : out STD_LOGIC;
    s_axis_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_tkeep : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_tlast : in STD_LOGIC;
    s_axis_tuser : in STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axis_tvalid : out STD_LOGIC;
    m_axis_tready : in STD_LOGIC;
    m_axis_tdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axis_tkeep : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axis_tlast : out STD_LOGIC;
    m_axis_tuser : out STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end COMPONENT axi_stream_fifo;



Begin

--- ttc information signal selection
	ttc_L1A       <=   ttc_sync_data(16) & ttc_sync_data(16) & ttc_sync_data(16) & ttc_sync_data(16) ;   -- L1 accept to indicate when an event has been accepted
	ttc_BCR       <=   ttc_sync_data(17) & ttc_sync_data(17) & ttc_sync_data(17) & ttc_sync_data(17) ;   -- bunch counter reset. used to reset local bc counters
	ttc_ECR       <=   ttc_sync_data(18) & ttc_sync_data(18) & ttc_sync_data(18) & ttc_sync_data(18) ;   -- event counter reset
	ttc_pr_rdout  <=   ttc_sync_data(19) & ttc_sync_data(19) & ttc_sync_data(19) & ttc_sync_data(19) ;   -- indicate full data readout has been requested
	link_reset    <=   ttc_sync_data(96);

	ttc_din <= ttc_sync_data(63 downto 32) &  bcn_cntr ;

--- ROD information signal selection
	ttc_L1A_hub2       <=   readout_ctrl(16) & readout_ctrl(16) & readout_ctrl(16) & readout_ctrl(16) ;   -- L1 accept to indicate when an event has been accepted
	ttc_BCR_hub2       <=   readout_ctrl(17) & readout_ctrl(17) & readout_ctrl(17) & readout_ctrl(17) ;   -- bunch counter reset. used to reset local bc counters
	ttc_ECR_hub2       <=   readout_ctrl(18) & readout_ctrl(18) & readout_ctrl(18) & readout_ctrl(18) ;   -- event counter reset
	ttc_pr_rdout_hub2  <=   readout_ctrl(19) & readout_ctrl(19) & readout_ctrl(19) & readout_ctrl(19) ;   -- indicate full data readout has been requested
	link_reset_hub2    <=   readout_ctrl(96);

	aurora_status <=  x"00000" & "000" & aurora_tx_resetdone_hub2  & tx_lock_hub2  & pll_not_locked_hub2  & tx_hard_err_hub2  & tx_channel_up_hub2  & tx_lane_up_hub2 ;

MGT_TX_RX: top_mgt_cfpga
port map (
	clk40                    =>  clk40,
	start                    =>  start,
	clk160                   => clk160,
	rx_clk160                =>  rx_clk160,
	Q_CLK_GTREFCLK_PAD_N_IN  =>  Q_CLK_GTREFCLK_PAD_N_IN , -- (1 downto 0),
	Q_CLK_GTREFCLK_PAD_P_IN  =>  Q_CLK_GTREFCLK_PAD_P_IN, -- (1 downto 0),
	RXN_IN                   =>  RXN_IN_i,
	RXP_IN                   =>  RXP_IN_i,
	TXN_OUT                  =>  TXN_OUT,
	TXP_OUT                  =>  TXP_OUT,
	rx_resetdone             => rx_resetdone    ,
	rx_fsm_resetdone         => rx_fsm_resetdone,
	rx_byteisaligned         => rx_byteisaligned,
	tx_resetdone             => tx_resetdone    ,
	tx_fsm_resetdone         => tx_fsm_resetdone,
	tx_bufstatus             => tx_bufstatus    ,
	rx_realign               => rx_realign      ,
	rx_disperr               => rx_disperr      ,
	encode_error             => encode_error,
	mgt_commadret            => mgt_commadret,
	loopback                 => loopback,
	mgt_SOFT_RESET_TX_IN     => softreset_tx ,
	mgt_SOFT_RESET_RX_IN     => softreset_rx,
	topo_data_fpga1          =>  topo_data_fpga1 , -- topo data from fpga1 through mgt 115 gt_rx0
	topo_data_fpga2          =>  topo_data_fpga2 , -- topo data from fpga2 through mgt 115 gt_rx2
	--topo_data_fpga3          =>  topo_data_fpga3 , -- topo data from fpga3 through mgt 116 gt_rx0
	--topo_data_fpga4          =>  topo_data_fpga4 , -- topo data from fpga4 through mgt 116 gt_rx2
	raw_data_fpga1           =>  raw_data_fpga1  , -- raw data from fpga1 through mgt 115 gt_rx1
	raw_data_fpga2           =>  raw_data_fpga2  , -- raw data from fpga2 through mgt 115 gt_rx3
	--raw_data_fpga3           =>  raw_data_fpga3  , -- raw data from fpga3 through mgt 116 gt_rx1
	--raw_data_fpga4           =>  raw_data_fpga4  , -- raw data from fpga4 through mgt 116 gt_rx3
	ttc_rx_data              =>  ttc_rx_data  ,     -- ttc information data from hub1 through mgt114 gt_rx0
	rod_busy                 =>  ttc_data_hub2 --rod_busy           -- rod busy data from hub2 through mgt114 gt_rx1
);

synch_ttc_combined: top_cntrl_synch
port map (
	rx_clk160        => rx_clk160(4),   --8  -- rx clock of the mgt114
	TTC_clk          => clk40,            -- ttc clock of 40 MHz
	reset            => reset,
	enable_mgt       => mgt_enable(0),    -- enaable of the incoming rx data
	MGT_Commadet     => mgt_commadret(4), --8 -- comma detected for incoming data
	reg_sel          => bc_reg_sel(3 downto 0),       -- setting BC mux              --
	mux_sel          => mux_sel(3 downto 0),          -- setting the first stage mux
	delay_num        => delay_cntr_0,    -- delay count of first stage
	start            => start,            -- start pulse for the calibration to start
	rx_resetdone     => rx_resetdone(0),
	reg128_latch     => reg128_latch,
	data_out         => ttc_sync_data_i,    -- ttc_information synch goes to process fpgasdata_out_reg128, ---data_out_reg128,
	data_in          => ttc_rx_data        -- in coming ttc information data
);

synch_readout_ctrl: top_cntrl_synch
port map (
	rx_clk160        => rx_clk160(5),    --9 -- rx clock of the mgt114
	TTC_clk          => clk40,            -- ttc clock of 40 MHz
	reset            => reset,
	enable_mgt       => mgt_enable(1),    -- enaable of the incoming rx data
	MGT_Commadet     => mgt_commadret(5), --9 comma detected for incoming data
	reg_sel          => bc_reg_sel(7 downto 4),       -- setting BC mux              --
	mux_sel          => mux_sel(7 downto 4),          -- setting the first stage mux
	delay_num        => delay_cntr_1,    -- delay count of first stage
	start            => start,            -- start pulse for the calibration to start
	rx_resetdone     => rx_resetdone(1),
	reg128_latch     => readout_ctrl_latch,
	--data_out_reg128  => ttc_sync_data,    -- ttc_information synch goes to process fpgasdata_out_reg128,
	data_out         => readout_ctrl_i,    -- ttc_information synch goes to process fpgasdata_out_reg128, ---data_out_reg128,
	data_in          =>  ttc_data_hub2 --rod_busy        -- in coming ttc information data
);

top_aurora_hub2: aurora_hub2
port map (
	-- TX Stream Interface
	s_axi_tx_tdata      =>  s_axi_tx_tdata_hub2 ,
	s_axi_tx_tvalid     =>  s_axi_tx_tvalid_hub2,
	s_axi_tx_tready     =>  s_axi_tx_tready_hub2,
	s_axi_tx_tkeep      =>  s_axi_tx_tkeep_hub2 ,
	s_axi_tx_tlast      =>  s_axi_tx_tlast_hub2 ,
	-- User Flow Control TX Interface
	s_axi_ufc_tx_req    =>  s_axi_ufc_tx_req_hub2,
	s_axi_ufc_tx_ms     =>  s_axi_ufc_tx_ms_hub2 ,
	s_axi_ufc_tx_ack    =>  s_axi_ufc_tx_ack_hub2,
	-- V7 Serial I/O
	txp                 =>  aurora_hub2_txp,
	txn                 =>  aurora_hub2_txn,
	-- GT Reference Clock Interface
	gt_refclk1_p        =>  aurora_refclk1_p,
	gt_refclk1_n        =>  aurora_refclk1_n,
	-- Error Detection Interface
	tx_hard_err         =>  tx_hard_err_hub2,
	-- Status
	tx_channel_up       =>  tx_channel_up_hub2,
	tx_lane_up          =>  tx_lane_up_hub2,
	-- System Interface
	user_clk_out        =>  aurora_user_clk_hub2  ,
	sys_reset_out       =>  sys_reset_out_hub2  ,
	tx_lock             =>  tx_lock_hub2        ,
	init_clk            =>  mac_clk      ,
	init_clk_out        =>  init_clk_out_hub2   ,
	pll_not_locked      =>  pll_not_locked_hub2 ,
	tx_resetdone        =>  aurora_tx_resetdone_hub2   ,
	link_reset          =>  link_reset_hub2
);

 efex_packet_builder: efex_packet_builder
      PORT MAP (
         clk            => aurora_user_clk_hub2,
         rst_clk        => sys_reset_out_hub2,
-- FIFO signals
		 packet_data     => packet_data,
		 packet_valid    => packet_valid,
   		 packet_hdr      => packet_hdr,
		 packet_data_end => packet_data_end,
		 packet_ready	 => packet_ready,
-- towards Aurora readout
         payload_data   => s_axi_tx_tdata_hub2,
         payload_valid  => s_axi_tx_tvalid_hub2,
         payload_last   => s_axi_tx_tlast_hub2,
         tready_data    => s_axi_tx_tready_hub2
      );

 rom_packet: rom_packet
      GENERIC MAP (
         DATA_WIDTH     => 64,         -- DATA bus width
         ROM_ADDR_WIDTH     => 5,          -- width of rom address bus
         ROM_LENGTH     => 32,         -- number of words to read from ROM
         FORMAT_VERSION => "001",
         STREAM_ID      => x"01"
      )
      PORT MAP (
         clk            => aurora_user_clk_hub2,
         rst_clk        => sys_reset_out_hub2,
-- Status signals
         eFEX_number    => x"03",
-- FIFO signals
         ttc_rd_en      => ttc_rd_en,
         ttc_dout       => x"0000000" & ttc_dout,
         ttc_fifo_empty => ttc_fifo_empty,
-- towards packet_builder readout
		 packet_data     => packet_data,
		 packet_valid    => packet_valid,
   		 packet_hdr      => packet_hdr,
		 packet_data_end => packet_data_end,
		 packet_ready	 => packet_ready
      );

ttc_sync_data_sig : process(clk40)
   begin
	  if clk40' event and clk40 ='1' then
		ttc_sync_data <= ttc_sync_data_i;
	  end if;
   end process ttc_sync_data_sig;

readout_ctrl_sig : process(clk40)
   begin
	  if clk40' event and clk40 ='1' then
		readout_ctrl <= readout_ctrl_i;
	  end if;
   end process readout_ctrl_sig;

bcn_counter : process(clk40)
  Variable bcn_count: unsigned (11 downto 0) := (Others => '0');
  begin
    if clk40' event and clk40 = '1' then
      if bcn_count = 3563 then  -- wrap around to zero at end of orbit
        bcn_count := (others => '0');
      elsif ttc_sync_data(17) = '1' then  -- reset to 1 at start of orbit
        bcn_count := x"001";
      else
        bcn_count := bcn_count + 1;
      end if;
      bcn_cntr <= std_logic_vector(bcn_count);
    end if;
  end process bcn_counter;
