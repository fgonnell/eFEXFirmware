library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

library infrastructure_lib;
use infrastructure_lib.ipbus_decode_L1CaloEfex.all;
use infrastructure_lib.ControlFPGAPackage.all;

library ipbus_lib;
use ipbus_lib.ipbus.all;
use ipbus_lib.ipbus_trans_decl.all;
use ipbus_lib.mac_arbiter_decl.all;
library unisim;
use unisim.VComponents.all;

entity top_golden_control_fpga is
  generic(
    --! Integer used to distinguish different FPGAs having a slightly different firmware
    FLAVOUR     : integer                       := 0;
    --! Date format DDMMYYYY in decimal
    GLOBAL_DATE : std_logic_vector(31 downto 0) := x"00000000";
    --! Time format  00HHMMSS in decimal
    GLOBAL_TIME : std_logic_vector(31 downto 0) := x"00000000";
    --! Short 7-digit git SHA of the repository
    GLOBAL_SHA  : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the repository (format: MMmmcccc in hex)
    GLOBAL_VER  : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the top folder: list files, tcl file
    TOP_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the top folder, see TOP_SHA
    TOP_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the XMLs
    XML_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the XMLs
    XML_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the Hog submodule
    HOG_SHA : std_logic_vector(31 downto 0) := x"00000000";
    HOG_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the Hog submodule
    CON_SHA : std_logic_vector(31 downto 0) := x"00000000";
    CON_VER : std_logic_vector(31 downto 0) := x"00000000";    

    --! Short 7-digit git SHA
    INFRASTRUCTURE_LIB_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of infrastructure library (format: MMmmcccc in hex)
    INFRASTRUCTURE_LIB_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the ipbus submodule
    IPBUS_LIB_SHA : std_logic_vector(31 downto 0) := x"00000000");

  port(
    gt_clk125_p  : in  std_logic;
    gt_clk125_n  : in  std_logic;
    gmii_gtx_clk : out std_logic;
    gmii_tx_en   : out std_logic;
    gmii_tx_er   : out std_logic;
    gmii_txd     : out std_logic_vector(7 downto 0);
    gmii_rx_clk  : in  std_logic;
    gmii_rx_dv   : in  std_logic;
    gmii_rx_er   : in  std_logic;
    gmii_rxd     : in  std_logic_vector(7 downto 0);
    phy_rstb     : out std_logic;
    clk_40_n     : in  std_logic;
    clk_40_p     : in  std_logic;

    --------------------I2C connections
    i2c_scl_0  : inout std_logic;
    i2c_sda_0  : inout std_logic;
    i2c_rst_0  : out   std_logic;
    ------Pll connections-----
    pll_miso   : in    std_logic;
    pll_le_1   : out   std_logic;
    pll_le_2   : out   std_logic;
    pll_le_3   : out   std_logic;
    pll_clko   : out   std_logic;
    pll_mosi   : out   std_logic;
    pll_lock_1 : in    std_logic;
    pll_lock_2 : in    std_logic;
    pll_lock_3 : in    std_logic;
    -------------Flash--------------    
    flash_csn  : out   std_logic;
    flash_mosi : out   std_logic;
    flash_miso : in    std_logic;


    -----------------------------
    xtal_ttc_clk_sel : out std_logic;
    SYNC_B_CDCE      : out std_logic;
    POWERDN_B_CDCE   : out std_logic;

    -- Signals coming to contrl FPGA

    master_tx_data1 : in std_logic_vector (9 downto 0);
    master_tx_data2 : in std_logic_vector (9 downto 0);
    master_tx_data3 : in std_logic_vector (9 downto 0);
    master_tx_data4 : in std_logic_vector (9 downto 0);

--- signals going out of cntrl FPGA

    master_rx_data1  : out std_logic_vector (9 downto 0);
    master_rx_data2  : out std_logic_vector (9 downto 0);
    master_rx_data3  : out std_logic_vector (9 downto 0);
    master_rx_data4  : out std_logic_vector (9 downto 0);
    master_tx_pause1 : out std_logic;
    master_tx_pause2 : out std_logic;
    master_tx_pause3 : out std_logic;
    master_tx_pause4 : out std_logic;
    hardware_addr    : in  std_logic_vector(11 downto 0);
    leds             : out std_logic_vector (1 downto 0);
    f5_ttc_clk_sel   : out std_logic;
    fpga1_done       : in  std_logic;
    fpga2_done       : in  std_logic;
    fpga3_done       : in  std_logic;
    fpga4_done       : in  std_logic;
    serial_number    : in  std_logic_vector(5 downto 0)
    );


end top_golden_control_fpga;


architecture spec of top_golden_control_fpga is

-- Architecture declarations
component io_delay_control_out
generic
 (-- width of the data for the system
  SYS_W       : integer := 11;
  -- width of the data for the device
  DEV_W       : integer := 11);
port
 (
  -- From the device out to the system
  data_out_from_device    : in    std_logic_vector(DEV_W-1 downto 0);
  data_out_to_pins        : out   std_logic_vector(SYS_W-1 downto 0);

-- Input, Output delay control signals
  delay_clk               : in    std_logic;
  out_delay_reset         : in    std_logic;                    -- Active high synchronous reset for output delay
  out_delay_data_ce       : in    std_logic_vector(SYS_W -1 downto 0);                    -- Enable signal for delay
  out_delay_data_inc      : in    std_logic_vector(SYS_W -1 downto 0);                    -- Delay increment (high), decrement (low) signal
  out_delay_tap_in        : in    std_logic_vector(5*SYS_W -1 downto 0); -- Dynamically loadable delay tap value for output delay
  out_delay_tap_out       : out   std_logic_vector(5*SYS_W -1 downto 0); -- Delay tap value for monitoring output delay
  delay_locked            : out   std_logic;                    -- Locked signal from IDELAYCTRL
  ref_clock               : in    std_logic;                    -- Reference Clock for IDELAYCTRL. Has to come from BUFG.

-- Clock and reset signals
  clk_in                  : in    std_logic;                    -- Fast clock from PLL/MMCM
  io_reset                : in    std_logic);                   -- Reset signal for IO circuit
end component;

component io_delay_control_in
generic
 (-- width of the data for the system
  SYS_W       : integer := 10;
  -- width of the data for the device
  DEV_W       : integer := 10);
port
 (
  -- From the system into the device
  data_in_from_pins       : in    std_logic_vector(SYS_W-1 downto 0);
  data_in_to_device       : out   std_logic_vector(DEV_W-1 downto 0);

-- Input, Output delay control signals
  delay_clk               : in    std_logic;
  in_delay_reset          : in    std_logic;                    -- Active high synchronous reset for input delay
  in_delay_data_ce        : in    std_logic_vector(SYS_W -1 downto 0);                    -- Enable signal for delay
  in_delay_data_inc       : in    std_logic_vector(SYS_W -1 downto 0);                    -- Delay increment (high), decrement (low) signal
  in_delay_tap_in         : in    std_logic_vector(5*SYS_W -1 downto 0); -- Dynamically loadable delay tap value for input delay
  in_delay_tap_out        : out   std_logic_vector(5*SYS_W -1 downto 0); -- Delay tap value for monitoring input delay
  delay_locked            : out   std_logic;                    -- Locked signal from IDELAYCTRL
  ref_clock               : in    std_logic;                    -- Reference Clock for IDELAYCTRL. Has to come from BUFG.

-- Clock and reset signals
  clk_in                  : in    std_logic;                    -- Fast clock from PLL/MMCM
  io_reset                : in    std_logic);                   -- Reset signal for IO circuit
end component;

  signal clk125_fr, clk200, clko_p40, ipb_clk, mac_clk, locked, clk_locked, eth_locked, clk_40                : std_logic;
  signal rst_125, rst_ipb, rst_eth, rst_macclk, sys_rst, soft_rst, rsto_eth                                   : std_logic;
  signal mac_rx_data                                                                                          : std_logic_vector(7 downto 0);
  signal mac_rx_error, mac_rx_valid, mac_rx_last                                                              : std_logic;
  signal mac_tx_data                                                                                          : std_logic_vector(7 downto 0);
  signal mac_tx_error, mac_tx_last, mac_tx_ready, mac_tx_valid                                                : std_logic;
  signal onehz, nuke, rst_ipb_ctrl                                                                            : std_logic;
  signal ipb_out                                                                                              : ipb_wbus;
  signal ipb_in                                                                                               : ipb_rbus;
  signal master_rx_data1_int, master_rx_data2_int, master_rx_data3_int, master_rx_data4_int                   : std_logic_vector(8 downto 0);
  signal master_tx_data1_int, master_tx_data2_int, master_tx_data3_int, master_tx_data4_int                   : std_logic_vector(8 downto 0);
  signal flash_clk, flash_led, scl_enb_0, sda_enb_0, scl_enb_1, sda_enb_1, scl_o_0, sda_o_0, scl_o_1, sda_o_1 : std_logic;
  signal Module_ID, status                                                                                    : std_logic_vector(31 downto 0);
  signal master_tx_err1, master_tx_err2, master_tx_err3, master_tx_err4                                       : std_logic;
  signal control_reg, pulse_reg, reconfig_reg                                                                 : std_logic_vector (31 downto 0);
  signal clkfb_in, gmii_rx_clk_int, clkfb_out                                                                 : std_logic;
  signal select_pll, select_flash                                                                             : std_logic_vector (1 downto 0);
  signal pll_en, flash_csn_int, trigger_reconfig, reconfig, en_reset                                          : std_logic;
  signal data_reg0, data_reg1, SYNC_B_CDCE_int                                                                : std_logic;
  signal flash_addr_int                                                                                       : std_logic_vector(2 downto 0);
  signal flash_mosi_int, f5_flash_disable_int, f5_flash_csn_int                                               : std_logic;
  signal master_link_down1_int, master_link_down2_int, master_link_down3_int, master_link_down4_int           : std_logic;
---------------------------------------------------------------------------------------------------
-- ipbus buses signals
   signal master_tx1_int, master_tx2_int, master_tx3_int, master_tx4_int : std_logic_vector(9 downto 0);
   signal master_pause1_int,master_pause2_int,master_pause3_int,master_pause4_int : std_logic;
   signal master_pause1_reg,master_pause2_reg,master_pause3_reg,master_pause4_reg : std_logic;
   signal master_rx1_int, master_rx2_int, master_rx3_int, master_rx4_int : std_logic_vector(9 downto 0);
   signal master_rx1_reg, master_rx2_reg, master_rx3_reg, master_rx4_reg : std_logic_vector(9 downto 0);
   signal ipbus_ctrl_delay_in, ipbus_status_delay_in : std_logic_vector(31 downto 0);
   signal ipbus_ctrl_delay_out, ipbus_status_delay_out : std_logic_vector(31 downto 0);
   signal dummy_tx1,dummy_tx2,dummy_tx3,dummy_tx4 : std_logic_vector(49 downto 5);
   signal dummy_rx1,dummy_rx2,dummy_rx3,dummy_rx4 : std_logic_vector(54 downto 5);
---------------------------------------------------------------------------------------------------
  signal ipbw         : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal ipbr, ipbr_d : ipb_rbus_array(N_SLAVES-1 downto 0);

  component clk_ttc
    port
      (                                 -- Clock in ports
        -- Clock out ports
        clk40     : out std_logic;
        clk320    : out std_logic;
        clk160    : out std_logic;
        -- Status and control signals
        locked    : out std_logic;
        clk_in1_p : in  std_logic;
        clk_in1_n : in  std_logic
        );
  end component;



begin

  i2c_scl_0 <= scl_o_0 when (scl_enb_0 = '0') else 'Z';
  i2c_sda_0 <= sda_o_0 when (sda_enb_0 = '0') else 'Z';
  i2c_rst_0 <= '1';  -- This will be assigned active low reset signal 

  phy_rstb <= not rst_ipb;

  leds(0) <= clk_locked and onehz;
  leds(1) <= pll_lock_1 or pll_lock_2 or pll_lock_3;

  -- control fpga
  Module_ID <= (31              => '0',   -- user (1) / golden image (0)
                   30 downto 28 => "000",  -- FPGA flavour if 0 => control FPGA
                   27 downto 26 => "00",  -- space for geographic address in process
                   25 downto 20 => serial_number,
                   19 downto 16 => hardware_addr(11 downto 8),  -- shelf address
                   15 downto 12 => hardware_addr(3 downto 0),  -- slot address                 
                   11 downto 0  => X"efe"  -- module ID
                   );

  --  moddule status assignement

  status <= x"0000000" & '0' & pll_lock_3 & pll_lock_2 & pll_lock_1;

  -- Shows which clock is selected on led
  f5_ttc_clk_sel <= control_reg(0);

  trigger_reconfig <= reconfig and clk_locked;  -- if active it will reconfigure the FPGA5
  reconfig         <= reconfig_reg(30);  -- if active it will reconfigure the spi flash.
  POWERDN_B_CDCE   <= not control_reg(1);
  xtal_ttc_clk_sel <= control_reg(0);  -- If low it selects local crystal clcok of 40.08MHz. If high it selects  LHC TTC clock
  en_reset         <= control_reg(6);  -- thi swill generate active low reset pulse that will synchronous the PLLs

  flash_mosi <= flash_mosi_int;

  master_link_down1_int <= not fpga1_done;
  master_link_down2_int <= not fpga2_done;
  master_link_down3_int <= not fpga3_done;
  master_link_down4_int <= not fpga4_done;




  global_ctrl_fabric : entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV      => N_SLAVES,
                SEL_WIDTH => ipbus_sel_width)
    port map(
      ipb_in          => ipb_out,
      ipb_out         => ipb_in,
      sel             => ipbus_sel_L1CaloEfex(ipb_out.ipb_addr),
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      );


---------------------------------------------------------------------------------
  ----- common id registers slave
  ---------------------------------------------------------------------------------------

  common_reg : entity infrastructure_lib.common_id_registers
    port map (
      ipb_clk     => ipb_clk,
      ipb_rst     => rst_ipb,
      ipb_in      => ipbw(N_SLV_COMMON_ID_VERSION),
      ipb_out     => ipbr(N_SLV_COMMON_ID_VERSION),
      Module_ID   => Module_ID,
      xml_version => XML_VER,
      xml_Gitsha  => XML_SHA,
      fw_version  => GLOBAL_VER,
      fw_Gitsha   => GLOBAL_SHA,
      build_date  => GLOBAL_DATE,
      build_time  => GLOBAL_TIME

      );


  U_0 : entity infrastructure_lib.top_udp_config_FPGA
    port map (
      hardware_addr     => hardware_addr,
      serial_number		=> serial_number,
      ipb_clk           => ipb_clk,
      ipb_in            => ipb_in,
      mac_clk           => mac_clk,
      mac_rx_data       => mac_rx_data,
      mac_rx_error      => mac_rx_error,
      mac_rx_last       => mac_rx_last,
      mac_rx_valid      => mac_rx_valid,
      mac_tx_ready      => mac_tx_ready,
      master_tx_data1   => master_tx_data1_int,
      master_tx_data2   => master_tx_data2_int,
      master_tx_data3   => master_tx_data3_int,
      master_tx_data4   => master_tx_data4_int,
      master_tx_err1    => master_tx_err1,
      master_tx_err2    => master_tx_err2,
      master_tx_err3    => master_tx_err3,
      master_tx_err4    => master_tx_err4,
      rst_ipb           => rst_ipb_ctrl,
      rst_macclk        => rst_macclk,
      ipb_out           => ipb_out,
      mac_tx_data       => mac_tx_data,
      mac_tx_error      => mac_tx_error,
      mac_tx_last       => mac_tx_last,
      mac_tx_valid      => mac_tx_valid,
      master_rx_data1   => master_rx_data1_int,
      master_rx_data2   => master_rx_data2_int,
      master_rx_data3   => master_rx_data3_int,
      master_rx_data4   => master_rx_data4_int,
      master_tx_pause1  => master_pause1_int, --to iodelay
      master_tx_pause2  => master_pause2_int, --to iodelay
      master_tx_pause3  => master_pause3_int, --to iodelay
      master_tx_pause4  => master_pause4_int, --to iodelay
      master_link_down1 => master_link_down1_int,
      master_link_down2 => master_link_down2_int,
      master_link_down3 => master_link_down3_int,
      master_link_down4 => master_link_down4_int
      );


  ------- -- Interconnection between control FPGA and Process FPGA1------------------

  U_1 : entity infrastructure_lib.interconnect
    port map (
      mac_clk         => mac_clk,
      master_rx_data  => master_tx1_int,     -- coming from FPGA1
      rst_macclk      => rst_macclk,
      tx_data         => master_rx_data1_int,
      master_rx_err   => master_tx_err1,  -- is set high if data has parity error
      process_tx_data => master_rx1_int,     -- goes to iodelay and then to FPGA1
      rx_data         => master_tx_data1_int  -- goes to top_udp_config block
      );

  ------- -- Interconnection between control FPGA and Process FPGA2------------------
  U_2 : entity infrastructure_lib.interconnect
    port map (
      mac_clk         => mac_clk,
      master_rx_data  => master_tx2_int,     -- coming from FPGA2
      rst_macclk      => rst_macclk,
      tx_data         => master_rx_data2_int,
      master_rx_err   => master_tx_err2,  -- is set high if data has parity error
      process_tx_data => master_rx2_int,     -- goes to iodelay and then to FPGA2
      rx_data         => master_tx_data2_int  -- goes to top_udp_config block
      );

------- -- Interconnection between control FPGA and Process FPGA3------------------

  U_3 : entity infrastructure_lib.interconnect
    port map (
      mac_clk         => mac_clk,
      master_rx_data  => master_tx3_int,     -- coming from FPGA3
      rst_macclk      => rst_macclk,
      tx_data         => master_rx_data3_int,
      master_rx_err   => master_tx_err3,  -- is set high if data has parity error
      process_tx_data => master_rx3_int,     -- goes to iodelay and then to FPGA3
      rx_data         => master_tx_data3_int  -- goes to top_udp_config block
      );
  ------- -- Interconnection between control FPGA and Process FPGA4------------------

  U_4 : entity infrastructure_lib.interconnect
    port map (
      mac_clk         => mac_clk,
      master_rx_data  => master_tx4_int,     -- coming from FPGA4
      rst_macclk      => rst_macclk,
      tx_data         => master_rx_data4_int,
      master_rx_err   => master_tx_err4,  -- is set high if data has parity error
      process_tx_data => master_rx4_int,     -- goes to iodelay and then to FPGA4
      rx_data         => master_tx_data4_int  -- goes to top_udp_config block
      );

  --    DCM clock generation for internal bus, ethernet 
  clocks : entity infrastructure_lib.clocks_7s_extphy
    port map(
      sysclk_p      => gt_clk125_p,
      sysclk_n      => gt_clk125_n,
      clko_125      => mac_clk,
      clko_200      => clk200,
      clko_ipb      => ipb_clk,
      locked        => clk_locked,
      nuke          => nuke,
      soft_rst      => soft_rst,
      rsto_125      => rst_macclk,
      rsto_ipb      => rst_ipb,
      rsto_ipb_ctrl => rst_ipb_ctrl,
      onehz         => onehz
      );




-- --- Ethernet MAC core and PHY interface   
  eth : entity infrastructure_lib.eth_7s_gmii
    port map(
      clk125       => mac_clk,
      clk200       => clk200,
      rst          => rst_macclk,
      gmii_gtx_clk => gmii_gtx_clk,
      gmii_txd     => gmii_txd,
      gmii_tx_en   => gmii_tx_en,
      gmii_tx_er   => gmii_tx_er,
      gmii_rx_clk  => gmii_rx_clk,
      gmii_rxd     => gmii_rxd,
      gmii_rx_dv   => gmii_rx_dv,
      gmii_rx_er   => gmii_rx_er,
      tx_data      => mac_tx_data,
      tx_valid     => mac_tx_valid,
      tx_last      => mac_tx_last,
      tx_error     => mac_tx_error,
      tx_ready     => mac_tx_ready,
      rx_data      => mac_rx_data,
      rx_valid     => mac_rx_valid,
      rx_last      => mac_rx_last,
      rx_error     => mac_rx_error

      );




  -- ipbus slaves live in the entity below, and can expose top-level ports
-- The ipbus fabric is instantiated within.
  infrastructure_control : entity infrastructure_lib.infrastructure_slaves_cntrl

    port map(
      ipb_clk      => ipb_clk,
      ipb_rst      => rst_ipb,
      ipb_in       => ipbw(N_SLV_CNTRL_INFRA),
      ipb_out      => ipbr(N_SLV_CNTRL_INFRA),
      status       => status,
      control_reg  => control_reg,
      reconfig_reg => reconfig_reg,

      ipbus_delay_in_ctrl    => ipbus_ctrl_delay_in,
      ipbus_delay_out_ctrl   => ipbus_ctrl_delay_out,
      ipbus_delay_in_status  => ipbus_status_delay_in,
      ipbus_delay_out_status => ipbus_status_delay_out,

      scl_i_0      => i2c_scl_0,
      scl_o_0      => scl_o_0,
      scl_enb_0    => scl_enb_0,
      sda_i_0      => i2c_sda_0,
      sda_o_0      => sda_o_0,
      sda_enb_0    => sda_enb_0,
      pll_clko     => pll_clko,
      pll_le       => pll_en,
      pll_miso     => pll_miso,
      pll_mosi     => pll_mosi,
      pll_select   => select_pll,
      flash_miso   => flash_miso,
      flash_le     => flash_csn,
      flash_clko   => flash_clk,
      flash_mosi   => flash_mosi
      );


  
  pll_sel : entity infrastructure_lib.pll_selector
    port map
    (
      sel      => select_pll(1 downto 0),
      pll_en_1 => pll_le_1,
      pll_en_2 => pll_le_2,
      pll_en_3 => pll_le_3,
      pll_en   => pll_en

      );




  cclk_o : entity infrastructure_lib.startup
    port map(
      flash_cclk => flash_clk
      );


  configure : entity infrastructure_lib.self_configure
    generic map(
      WBSTAR => x"02000000"
    )
    port map(
      clk       => ipb_clk,             --icap_clk,
      reset     => rst_ipb,
      led_clk   => onehz,
      trigger   => trigger_reconfig,
      indicator => flash_led
      );


  clk40 : clk_ttc
    port map (
      -- Clock out ports  
      clk40     => clk_40,
      clk320    => open,
      clk160    => open,
      -- Status and control signals                
      locked    => open,
      -- Clock in ports
      clk_in1_p => clk_40_p,
      clk_in1_n => clk_40_n
      );

  reset_gen : entity infrastructure_lib.nreset_gen
    port map (
      clk    => clk_40,
      reset  => rst_ipb,
      en_rst => data_reg1,
      nreset => SYNC_B_CDCE_int

      );

  CDC : process (clk_40, rst_ipb)
  begin
    if rst_ipb = '1' then
      data_reg0 <= '0';
      data_reg1 <= '0';
    elsif clk_40' event and clk_40 = '1' then
      data_reg0 <= en_reset;
      data_reg1 <= data_reg0;
    end if;

  end process;

  reg_pip : process (clk_40)
  begin

    if clk_40' event and clk_40 = '0' then
      SYNC_B_CDCE <= SYNC_B_CDCE_int;

    end if;

  end process;


-- Register IPBus interFPGA signals
	IPBusReg: process(mac_clk)
	Begin
		If rising_edge(mac_clk) then
			master_rx1_reg <= master_rx1_int;
			master_pause1_reg <= master_pause1_int;
			master_rx2_reg <= master_rx2_int;
			master_pause2_reg <= master_pause2_int;
			master_rx3_reg <= master_rx3_int;
			master_pause3_reg <= master_pause3_int;
			master_rx4_reg <= master_rx4_int;
			master_pause4_reg <= master_pause4_int;
		end if;
	End Process IPBusReg;

-- IPBUS TX BUS 10bit
--- 1 ---
ipbus_tx1 : io_delay_control_in
   port map
   (
   data_in_from_pins => master_tx_data1,
   data_in_to_device => master_tx1_int,
   delay_clk => mac_clk,
   in_delay_reset => ipbus_ctrl_delay_in(25), --28:25
   in_delay_data_ce => (others => '0'),
   in_delay_data_inc => (others => '0'),
   in_delay_tap_in => ipbus_ctrl_delay_in(4 downto 0)&ipbus_ctrl_delay_in(4 downto 0)&ipbus_ctrl_delay_in(4 downto 0)&
   ipbus_ctrl_delay_in(4 downto 0)&ipbus_ctrl_delay_in(4 downto 0)&ipbus_ctrl_delay_in(4 downto 0)&
   ipbus_ctrl_delay_in(4 downto 0)&ipbus_ctrl_delay_in(4 downto 0)&ipbus_ctrl_delay_in(4 downto 0)&
   ipbus_ctrl_delay_in(4 downto 0),
   in_delay_tap_out(4 downto 0) => ipbus_status_delay_in(4 downto 0),
   in_delay_tap_out(49 downto 5) => dummy_tx1,

   delay_locked => ipbus_status_delay_in(25), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_in(31) --common
);


--IPBUS RX BUS 11bit
ipbus_rx1 : io_delay_control_out
   port map
   (
   data_out_from_device(9 downto 0) => master_rx1_reg,
   data_out_from_device(10) => master_pause1_reg,
   data_out_to_pins(9 downto 0) => master_rx_data1,
   data_out_to_pins(10) => master_tx_pause1,
   delay_clk => mac_clk,
   out_delay_reset => ipbus_ctrl_delay_out(25), --28:25

   out_delay_data_ce => (others => '0'),
   out_delay_data_inc => (others => '0'),
   out_delay_tap_in => ipbus_ctrl_delay_out(4 downto 0)&ipbus_ctrl_delay_out( 4 downto 0)&ipbus_ctrl_delay_out(4 downto 0)&
   ipbus_ctrl_delay_out(4 downto 0)&ipbus_ctrl_delay_out(4 downto 0)&ipbus_ctrl_delay_out(4 downto 0)&
   ipbus_ctrl_delay_out(4 downto 0)&ipbus_ctrl_delay_out(4 downto 0)&ipbus_ctrl_delay_out(4 downto 0)&
   ipbus_ctrl_delay_out(4 downto 0)&ipbus_ctrl_delay_out(4 downto 0),

   out_delay_tap_out(4 downto 0) => ipbus_status_delay_out(4 downto 0),
   out_delay_tap_out(54 downto 5) => dummy_rx1,

   delay_locked => ipbus_status_delay_out(25), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_out(31) --common
);

--- 2 ---
ipbus_tx2 : io_delay_control_in
   port map
   (
   data_in_from_pins => master_tx_data2,
   data_in_to_device => master_tx2_int,
   delay_clk => mac_clk,
   in_delay_reset => ipbus_ctrl_delay_in(26), --28:25
   in_delay_data_ce => (others => '0'),
   in_delay_data_inc => (others => '0'),
   in_delay_tap_in => ipbus_ctrl_delay_in(9 downto 5)&ipbus_ctrl_delay_in(9 downto 5)&ipbus_ctrl_delay_in(9 downto 5)&
   ipbus_ctrl_delay_in(9 downto 5)&ipbus_ctrl_delay_in(9 downto 5)&ipbus_ctrl_delay_in(9 downto 5)&
   ipbus_ctrl_delay_in(9 downto 5)&ipbus_ctrl_delay_in(9 downto 5)&ipbus_ctrl_delay_in(9 downto 5)&
   ipbus_ctrl_delay_in(9 downto 5),
   in_delay_tap_out(4 downto 0) => ipbus_status_delay_in(9 downto 5),
   in_delay_tap_out(49 downto 5) => dummy_tx2,

   delay_locked => ipbus_status_delay_in(26), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_in(31) --common
);


--IPBUS RX BUS 11bit
ipbus_rx2 : io_delay_control_out
   port map
   (
   data_out_from_device(9 downto 0) => master_rx2_reg,
   data_out_from_device(10) => master_pause2_reg,
   data_out_to_pins(9 downto 0) => master_rx_data2,
   data_out_to_pins(10) => master_tx_pause2,
   delay_clk => mac_clk,
   out_delay_reset => ipbus_ctrl_delay_out(26), --28:25

   out_delay_data_ce => (others => '0'),
   out_delay_data_inc => (others => '0'),
   out_delay_tap_in => ipbus_ctrl_delay_out(9 downto 5)&ipbus_ctrl_delay_out( 9 downto 5)&ipbus_ctrl_delay_out(9 downto 5)&
   ipbus_ctrl_delay_out(9 downto 5)&ipbus_ctrl_delay_out(9 downto 5)&ipbus_ctrl_delay_out(9 downto 5)&
   ipbus_ctrl_delay_out(9 downto 5)&ipbus_ctrl_delay_out(9 downto 5)&ipbus_ctrl_delay_out(9 downto 5)&
   ipbus_ctrl_delay_out(9 downto 5)&ipbus_ctrl_delay_out(9 downto 5),

   out_delay_tap_out(4 downto 0) => ipbus_status_delay_out(9 downto 5),
   out_delay_tap_out(54 downto 5) => dummy_rx2,

   delay_locked => ipbus_status_delay_out(26), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_out(31) --common
);

--- 3 ---
ipbus_tx3 : io_delay_control_in
   port map
   (
   data_in_from_pins => master_tx_data3,
   data_in_to_device => master_tx3_int,
   delay_clk => mac_clk,
   in_delay_reset => ipbus_ctrl_delay_in(27), --28:25
   in_delay_data_ce => (others => '0'),
   in_delay_data_inc => (others => '0'),
   in_delay_tap_in => ipbus_ctrl_delay_in(14 downto 10)&ipbus_ctrl_delay_in(14 downto 10)&ipbus_ctrl_delay_in(14 downto 10)&
   ipbus_ctrl_delay_in(14 downto 10)&ipbus_ctrl_delay_in(14 downto 10)&ipbus_ctrl_delay_in(14 downto 10)&
   ipbus_ctrl_delay_in(14 downto 10)&ipbus_ctrl_delay_in(14 downto 10)&ipbus_ctrl_delay_in(14 downto 10)&
   ipbus_ctrl_delay_in(14 downto 10),
   in_delay_tap_out(4 downto 0) => ipbus_status_delay_in(14 downto 10),
   in_delay_tap_out(49 downto 5) => dummy_tx3,

   delay_locked => ipbus_status_delay_in(27), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_in(31) --common
);


--IPBUS RX BUS 11bit
ipbus_rx3 : io_delay_control_out
   port map
   (
   data_out_from_device(9 downto 0) => master_rx3_reg,
   data_out_from_device(10) => master_pause3_reg,
   data_out_to_pins(9 downto 0) => master_rx_data3,
   data_out_to_pins(10) => master_tx_pause3,
   delay_clk => mac_clk,
   out_delay_reset => ipbus_ctrl_delay_out(27), --28:25

   out_delay_data_ce => (others => '0'),
   out_delay_data_inc => (others => '0'),
   out_delay_tap_in => ipbus_ctrl_delay_out(14 downto 10)&ipbus_ctrl_delay_out( 14 downto 10)&ipbus_ctrl_delay_out(14 downto 10)&
   ipbus_ctrl_delay_out(14 downto 10)&ipbus_ctrl_delay_out(14 downto 10)&ipbus_ctrl_delay_out(14 downto 10)&
   ipbus_ctrl_delay_out(14 downto 10)&ipbus_ctrl_delay_out(14 downto 10)&ipbus_ctrl_delay_out(14 downto 10)&
   ipbus_ctrl_delay_out(14 downto 10)&ipbus_ctrl_delay_out(14 downto 10),

   out_delay_tap_out(4 downto 0) => ipbus_status_delay_out(14 downto 10),
   out_delay_tap_out(54 downto 5) => dummy_rx3,

   delay_locked => ipbus_status_delay_out(27), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_out(31) --common
);

--- 4 ---
ipbus_tx4 : io_delay_control_in
   port map
   (
   data_in_from_pins => master_tx_data4,
   data_in_to_device => master_tx4_int,
   delay_clk => mac_clk,
   in_delay_reset => ipbus_ctrl_delay_in(28), --28:25
   in_delay_data_ce => (others => '0'),
   in_delay_data_inc => (others => '0'),
   in_delay_tap_in => ipbus_ctrl_delay_in(19 downto 15)&ipbus_ctrl_delay_in(19 downto 15)&ipbus_ctrl_delay_in(19 downto 15)&
   ipbus_ctrl_delay_in(19 downto 15)&ipbus_ctrl_delay_in(19 downto 15)&ipbus_ctrl_delay_in(19 downto 15)&
   ipbus_ctrl_delay_in(19 downto 15)&ipbus_ctrl_delay_in(19 downto 15)&ipbus_ctrl_delay_in(19 downto 15)&
   ipbus_ctrl_delay_in(19 downto 15),
   in_delay_tap_out(4 downto 0) => ipbus_status_delay_in(19 downto 15),
   in_delay_tap_out(49 downto 5) => dummy_tx4,

   delay_locked => ipbus_status_delay_in(28), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_in(31) --common
);


--IPBUS RX BUS 11bit
ipbus_rx4 : io_delay_control_out
   port map
   (
   data_out_from_device(9 downto 0) => master_rx4_reg,
   data_out_from_device(10) => master_pause4_reg,
   data_out_to_pins(9 downto 0) => master_rx_data4,
   data_out_to_pins(10) => master_tx_pause4,
   delay_clk => mac_clk,
   out_delay_reset => ipbus_ctrl_delay_out(28), --28:25

   out_delay_data_ce => (others => '0'),
   out_delay_data_inc => (others => '0'),
   out_delay_tap_in => ipbus_ctrl_delay_out(19 downto 15)&ipbus_ctrl_delay_out( 19 downto 15)&ipbus_ctrl_delay_out(19 downto 15)&
   ipbus_ctrl_delay_out(19 downto 15)&ipbus_ctrl_delay_out(19 downto 15)&ipbus_ctrl_delay_out(19 downto 15)&
   ipbus_ctrl_delay_out(19 downto 15)&ipbus_ctrl_delay_out(19 downto 15)&ipbus_ctrl_delay_out(19 downto 15)&
   ipbus_ctrl_delay_out(19 downto 15)&ipbus_ctrl_delay_out(19 downto 15),

   out_delay_tap_out(4 downto 0) => ipbus_status_delay_out(19 downto 15),
   out_delay_tap_out(54 downto 5) => dummy_rx4,

   delay_locked => ipbus_status_delay_out(28), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_out(31) --common
);

end Spec;
