--! @file
--! @brief Calorimeter data PISO for process FPGA
--! @details
--! \verbatim
--! This is module takes the calorimeter data in at 224 bits per MGT channel, 
--! then generates 7 x 32-bit data together with 7 x 1b valid and 1b synch signals
--!
--! It also changes the 32-b input data to 36-bit ouput data by adding the 4-bit error flags to the 36-bit word.
--! The 4 bit error flags are used when constructing an RAW Readout Frame (event)
--! \endverbatim
--! @author Saeed Taghavi



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;
use TOB_rdout_lib.data_type_pkg.all;

--! @copydoc PISO_RAW_data.vhd
entity PISO_RAW_data is
    Port (
       RST                      : in STD_LOGIC;
       --! calorimeter data array 49 x 227b input frames
       RAW_link_data_in         : in STD_LOGIC_VECTOR (226 downto 0);  -- 227b input frames
       --! calorimeter data valid/synch signal
       data_sync_in             : in STD_LOGIC;
       clk_in_280M              : in STD_LOGIC;
       --! calorimeter data synch signal out
       data_sync_out            : out STD_LOGIC;
       --! calorimeter data valid signal out
       data_out_valid           : out STD_LOGIC;
       --! calorimeter Error & Data out 36b
       data_out                 : out STD_LOGIC_VECTOR (35 downto 0)
       );
 end PISO_RAW_data;

--! @copydoc PISO_RAW_data.vhd
architecture Behavioral of PISO_RAW_data is

    signal  CLK_280M_i              : std_logic ;
    signal  RST_i                   : std_logic ;
    signal  data_sync_in_1, data_sync_in_2  : std_logic ;
    signal  data_sync_in_3  : std_logic ;
    signal  RAW_data_in_i           : STD_LOGIC_VECTOR (226 downto 0);  -- 224b input frames
--    signal  error_flag_in_i         : STD_LOGIC_VECTOR (3 downto 0);   -- 4b error
    signal  data_sync_out_i         : std_logic ;
    signal  data_out_valid_i        : std_logic ;
    signal  data_out_i              : STD_LOGIC_VECTOR (35 downto 0);   -- 4b error plus 32b data 
    signal  error_flag_i    : STD_LOGIC_VECTOR (2 downto 0);   -- or_of_err_flags 3-b error
    signal  or_of_err_flags : STD_LOGIC;   -- or of 3-b error flags
      
    TYPE STATE_TYPE IS (
       idle, ser_1, ser_2, ser_3, ser_4,
       ser_5, ser_6, ser_7
       );
  
    SIGNAL current_state : STATE_TYPE;    

--  ####### Mark signals  ########
 attribute keep       : string ;
 attribute max_fanout : integer;
 attribute keep of       data_sync_in_1 : signal is "true" ;
 attribute max_fanout of data_sync_in_1 : signal is 40;
 attribute keep of       data_sync_in_2 : signal is "true" ;
 attribute max_fanout of data_sync_in_2 : signal is 40;
 attribute keep of       data_sync_in_3 : signal is "true" ;
 attribute max_fanout of data_sync_in_3 : signal is 40;

--   #######################################

begin

    CLK_280M_i           <= clk_in_280M ;
    RST_i                <= RST ;


    -- ouput
    data_out            <= data_out_i ;
    data_sync_out       <= data_sync_out_i ;
    data_out_valid      <= data_out_valid_i ;

  -- Process U2_280M_clk is used to pipeline the input data.
  -- It also captures the RAW Data from MGT Links in the middle of the 40MHz signal pulse.
U2_280M_clk : process (CLK_280M_i)
  begin
   if rising_edge (CLK_280M_i) then 
      data_sync_in_1  <= data_sync_in ;        -- delay data_sync_in
      data_sync_in_2  <= data_sync_in_1 ;      -- delay data_sync_in
      data_sync_in_3  <= data_sync_in_2 ;      -- delay data_sync_in
   end if;

    if rising_edge (CLK_280M_i) then    -- capture data in middle of 40M clk
       if data_sync_in_2 = '1' then 
          RAW_data_in_i   <= RAW_link_data_in;   -- register input 224b data
--          error_flag_in_i <= '0' & RAW_link_data_in(226 downto 224);      -- register 4b error flags for the current data from the same MGT
       end if;
   end if;
end process;

-- Process U3_wr_to_DPR_fsm is used to convert the 224-bit input data to 7x32-bit data.
-- It also changes the 32-b input data to 36-bit ouput data by adding the 4-bit error flags to the first word.
-- The 4 bit error flags are used when constructing an RAW Readout Frame (event)
U3_wr_to_DPR_fsm : process (CLK_280M_i)
--    variable  error_flag_i    : STD_LOGIC_VECTOR (2 downto 0);   -- or_of_err_flags 3-b error
--    variable  or_of_err_flags : STD_LOGIC;   -- or of 3-b error flags

    begin
        if CLK_280M_i'event and CLK_280M_i = '1' then
            if RST_i = '1' then
                current_state    <= idle ;
                data_out_valid_i <= '0';
            else
                CASE current_state is
                    when idle => 
                        data_sync_out_i <= '0';
                        if data_sync_in_3 = '1' then   -- on start bit of 7 32b words
                            error_flag_i <= RAW_data_in_i(226 downto 224); -- 4b error flags for the current data from the same MGT
                            data_out_i   <= "0000" & RAW_data_in_i(31 downto 0); 
                            data_sync_out_i <= '1';
                            data_out_valid_i <= '1'; 
                            current_state <= ser_1 ;
                        end if;
                    when ser_1 =>
--                        error_flag_i := error_flag_i OR error_flag_in_i(2 downto 0);
                        or_of_err_flags <= OR error_flag_i; -- OR the 3 bits of error flags together
                        data_out_i <=  "0000" & RAW_data_in_i(63 downto 32); 
                        data_sync_out_i  <= '0';
                        data_out_valid_i <= '1'; 
                        current_state <= ser_2 ;
                    when ser_2 =>
--                        error_flag_i := error_flag_i OR error_flag_in_i(2 downto 0);
                        data_out_i <=  "0000" & RAW_data_in_i(95 downto 64); 
                        data_sync_out_i  <= '0';
                        data_out_valid_i <= '1'; 
                        current_state <= ser_3 ;
                    when ser_3 =>
--                        error_flag_i := error_flag_i OR error_flag_in_i(2 downto 0);
                        data_out_i <=  "0000" & RAW_data_in_i(127 downto 96); 
                        data_sync_out_i  <= '0';
                        data_out_valid_i <= '1'; 
                        current_state <= ser_4 ;
                    when ser_4 =>
--                        error_flag_i := error_flag_i OR error_flag_in_i(2 downto 0);
                        data_out_i <=  "0000" & RAW_data_in_i(159 downto 128); 
                        data_sync_out_i  <= '0';
                        data_out_valid_i <= '1'; 
                        current_state <= ser_5 ;
                    when ser_5 =>
--                        error_flag_i := error_flag_i OR error_flag_in_i(2 downto 0);
                        data_out_i <=  "0000" & RAW_data_in_i(191 downto 160);
                        data_sync_out_i  <= '0';
                        data_out_valid_i <= '1'; 
                        current_state <= ser_6 ;
                    when ser_6 =>       
--                        error_flag_i := error_flag_i OR error_flag_in_i(2 downto 0);
                        data_out_i <=  or_of_err_flags & error_flag_i & RAW_data_in_i(223 downto 192); -- add error flags
                        data_sync_out_i  <= '0';
                        data_out_valid_i <= '1'; 
                        current_state <= idle ;
                    when others =>
                        NULL;
                end case;
            end if;
        END IF;              
    end process ;
    
end Behavioral;
