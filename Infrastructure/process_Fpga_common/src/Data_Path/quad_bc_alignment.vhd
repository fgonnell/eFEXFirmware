----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/12/2018 02:43:22 PM
-- Design Name: 
-- Module Name: quad_bc_alignment - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library infrastructure_lib;
use infrastructure_lib.all;


entity quad_bc_alignment is
   Port ( 
          reset    : in std_logic;
          clk      : in std_logic;
          start    : in std_logic;
          bcn_0    : in std_logic_vector(6 downto 0);
          bcn_1    : in std_logic_vector(6 downto 0);
          bcn_2    : in std_logic_vector(6 downto 0);
          bcn_3    : in std_logic_vector(6 downto 0);
          ref_orbit: in std_logic;
          aeqb_ch0 : out std_logic; 
          aeqb_ch1 : out std_logic; 
          aeqb_ch2 : out std_logic; 
          aeqb_ch3 : out std_logic;   
          cntr_ch0 : out std_logic_vector( 6 downto 0);
          cntr_ch1 : out std_logic_vector( 6 downto 0);
          cntr_ch2 : out std_logic_vector( 6 downto 0);
          cntr_ch3 : out std_logic_vector( 6 downto 0) 
          );         
          
end quad_bc_alignment;

architecture Behavioral of quad_bc_alignment is
signal pseudo_orbit_ch0,pseudo_orbit_ch1,pseudo_orbit_ch2,pseudo_orbit_ch3:std_logic;
signal cntr_ch0_int,cntr_ch1_int,cntr_ch2_int,cntr_ch3_int :std_logic_vector( 4 downto 0) ;
signal aeqb_ch0_int,aeqb_ch1_int,aeqb_ch2_int,aeqb_ch3_int :std_logic;

begin


 aeqb_ch0 <=  aeqb_ch0_int;
 aeqb_ch1 <=  aeqb_ch1_int;
 aeqb_ch2 <=  aeqb_ch2_int;
 aeqb_ch3 <=  aeqb_ch3_int;

process_ch0:  process (clk)
                begin                    
                     if clk' event and clk = '1' then
                       if aeqb_ch0_int ='1' then -- enable high                    
                      cntr_ch0 <= "00" & cntr_ch0_int;  -- latch the output the value of the counter
                      end if; 
                    end if; 
                 end process;    
                 
process_ch1:  process (clk)
                 begin                   
                    if clk' event and clk = '1' then
                     if aeqb_ch1_int ='1' then    -- enable high
                        cntr_ch1 <= "00" & cntr_ch1_int; -- latch output the value of the counter
                     end if; 
                   end if; 
                  end process;    
                                  
process_ch2:  process (clk)
                 begin
                    if clk' event and clk = '1' then
                      if aeqb_ch2_int ='1' then         -- enable high                            
                         cntr_ch2 <= "00" & cntr_ch2_int;      -- latch output the value of the counter  
                      end if; 
                    end if; 
               end process;    
                                   
process_ch3:  process (clk)  
                 begin
                    if clk' event and clk = '1' then
                       if aeqb_ch3_int ='1' then         -- enable high                            
                       cntr_ch3 <= "00" & cntr_ch3_int;         -- latch output the value of the counter  
                       end if; 
                     end if; 
                end process;                       
                                  
                                  
                                  
                                  
                                  
                     
                                  
ch0_pseudo: entity infrastructure_lib.pseudo_orbit_gen
          port map (
                  clk   => clk,
                  bcn   => bcn_0(4 downto 0),
                  pseudo_orbit => pseudo_orbit_ch0
                  
                  );

ch1_pseudo: entity infrastructure_lib.pseudo_orbit_gen
          port map (
                  clk   => clk,
                  bcn   => bcn_1(4 downto 0),
                  pseudo_orbit => pseudo_orbit_ch1
                  
                  );
                  
ch2_pseudo: entity infrastructure_lib.pseudo_orbit_gen
           port map (
                  clk   => clk,
                  bcn   => bcn_2(4 downto 0),
                  pseudo_orbit => pseudo_orbit_ch2
                                    
                  );
                  
ch3_pseudo: entity infrastructure_lib.pseudo_orbit_gen
          port map (
                 clk   => clk,
                 bcn   => bcn_3( 4 downto 0),
                 pseudo_orbit => pseudo_orbit_ch3
                                    
                  );                  

sm_orbit_0: entity infrastructure_lib.orbit_sm
          port map (
                 clk        => clk,
                 reset      => reset,
                 ref_orbit  => ref_orbit,          --- use the pseudo orbit signal of channel 0 as a reference           
                 chan_orbit => pseudo_orbit_ch0,   -- pseudo orbit of channel 0                                          
                 start      => start, 
                 A_eq_B     => aeqb_ch0_int,       -- compare the value of the two counters  and if equal set a_eq_b high
                 cntr_value => cntr_ch0_int        -- this the bc countter value                                                       
                                    
                  );     
                  
sm_orbit_1: entity infrastructure_lib.orbit_sm
          port map (
                 clk        => clk,
                 reset      => reset,
                 ref_orbit  => ref_orbit,         --- use the pseudo orbit signal of channel 0 as a reference 
                 chan_orbit => pseudo_orbit_ch1,  -- pseudo orbit of channel 1 
                 start      => start, 
                 A_eq_B     => aeqb_ch1_int,      -- compare the value of the two counters  and if equal set a_eq_b high
                 cntr_value => cntr_ch1_int       -- this the bc countter value                
                                    
                  );                  

sm_orbit_2: entity infrastructure_lib.orbit_sm
          port map (
                 clk        => clk,
                 reset      => reset,
                 ref_orbit  => ref_orbit,         --- use the pseudo orbit signal of channel 0 as a reference           
                 chan_orbit => pseudo_orbit_ch2,  -- pseudo orbit of channel 2                                          
                 start      => start, 
                 A_eq_B     => aeqb_ch2_int,      -- compare the value of the two counters  and if equal set a_eq_b high
                 cntr_value => cntr_ch2_int       -- this the bc countter value                                                        
                                    
                  );         
                  
sm_orbit_3: entity infrastructure_lib.orbit_sm
         port map (
                clk        => clk,
                reset      => reset,
                ref_orbit  => ref_orbit,         --- use the pseudo orbit signal of channel 0 as a reference           
                chan_orbit => pseudo_orbit_ch3,  -- pseudo orbit of channel 3                                          
                start      => start, 
                A_eq_B     => aeqb_ch3_int,      -- compare the value of the two counters  and if equal set a_eq_b high
                cntr_value => cntr_ch3_int       -- this the bc countter value                                                        
                                                      
                 );   
                                         
  end Behavioral;
