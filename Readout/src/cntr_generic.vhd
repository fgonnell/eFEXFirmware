--! @file
--! @brief Generic Counter for process FPGA
--! @details 
--! \verbatim
--! This module is a Generic Counter.
--! The output of counter goes to ZERO when it reacher full count, or if a RESET is applied.
--! \endverbatim
--! @author Saeed Taghavi

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.all;

--! @copydoc cntr_generic.vhd
entity cntr_generic is
    generic (
        width  : integer := 32
        );
    Port (
       --! Enable signal input
       CE   : in STD_LOGIC;
       --! Clock signal input 
       CLK  : in STD_LOGIC;
       --! Reset signal input
       RST  : in STD_LOGIC;
       --! Counter Output signal 
       Q    : out STD_LOGIC_VECTOR (width-1 downto 0)
       );
end cntr_generic;

--! @copydoc cntr_generic.vhd
architecture Behavioral of cntr_generic is

begin
process (CLK)
	variable temp : unsigned (width-1 downto 0);
	begin
	if CLK'event AND CLK = '1' then
     	if RST = '1' then           -- this is a synchronous reset 
      		temp := (others => '0');              -- set counter to offset value given.
		else
		  if CE = '1' then
             temp := temp + 1;			          -- else increment the value by one 
	      end if;
		end if;
	end if;
	Q <= std_logic_vector(temp) ;	 -- push out new value
	end process;

end Behavioral;
