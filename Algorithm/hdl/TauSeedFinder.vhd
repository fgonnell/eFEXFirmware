--! @file
--! @brief Seed Finder for the tau algorithm
--! @details 
--! The Tau seed finder module checks if the central Tower is greater than all the sorrounding ones.
--! The central value must be grearter than up towers (higher phi) and not smaller than down values (lower phi).
--! The central value must be greater than left value and not smaller than right value.
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
use work.DataTypes.all;

--! @copydoc TauSeedFinder.vhd
entity TauSeedFinder is
  port (
    CLK            : in  std_logic;
    IN_Central     : in  DataWord;  --! Sum of TTs in CCW starting from bottom left corner
    IN_Surrounding : in  DataWords(7 downto 0);
    OUT_IsMax      : out std_logic);
end TauSeedFinder;

architecture Behavioral of TauSeedFinder is

signal half1, half2:  std_logic;
--! @copydoc TauSeedFinder.vhd
begin

  --! The Surrounding 0,1,2 are down 3 is right 4,5,6 are top and 7 is left
  process (CLK)
  begin
    if rising_edge(CLK) then
      -- first clock cycle
      -- Up and left: strictly greater than
      half1 <= '1' when IN_Central > IN_Surrounding(4) and IN_Central > IN_Surrounding(5) and IN_Central > IN_Surrounding(6) and IN_Central > IN_Surrounding(7) else '0';

      -- Down and right: not smaller than
      half2 <= '0' when IN_Central < IN_Surrounding(0) or IN_Central < IN_Surrounding(1) or IN_Central < IN_Surrounding(2) or IN_Central < IN_Surrounding(3) else '1';

      --second clock cycle
      OUT_IsMax <= half1 and half2;
    end if;
  end process;
end Behavioral;
