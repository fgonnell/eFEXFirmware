----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/17/2017 03:38:05 PM
-- Design Name: 
-- Module Name: cntrol_registers - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
library TOB_rdout_lib;
use TOB_rdout_lib.data_type_pkg.all;

entity cntrl_registers is
generic (addr_width :natural :=1); 
  Port ( 
  ipb_clk         : in std_logic;
  ipb_rst         : in std_logic;
   ipb_in         : in ipb_wbus;
   ipb_out        : out ipb_rbus;
   pre_ld_wr_addr : out std_logic_vector (31 downto 0);
   DPR_locations_to_rd : out std_logic_vector (31 downto 0)
      
   );
   
end cntrl_registers;

architecture Behavioral of cntrl_registers is

signal sel:integer;
signal ack: std_logic;
signal DPR_locations_to_rd_int,pre_ld_wr_addr_int : std_logic_vector (31 downto 0); 


begin

sel <= to_integer(unsigned(ipb_in.ipb_addr(addr_width-1 downto 0))) when addr_width > 0 else 0;

 process(ipb_clk,ipb_rst)
 
     begin
     
      if ipb_clk' event and ipb_clk='1' then
        if ipb_rst ='1' then
          ipb_out.ipb_rdata <= (others =>'0');
      elsif ipb_in.ipb_strobe= '1' and ipb_in.ipb_write ='1' then
                          
      if   sel = 0 then
             pre_ld_wr_addr_int <= ipb_in.ipb_wdata;
       end if;
             
      if   sel = 1 then
             DPR_locations_to_rd_int  <=  ipb_in.ipb_wdata;
      end if;           
             
       elsif  ipb_in.ipb_strobe= '1' and ipb_in.ipb_write ='0' then
            if sel = 0 then
                 ipb_out.ipb_rdata <= pre_ld_wr_addr_int;
                  ack <= ipb_in.ipb_strobe and not ack;                     
             end if; 
              
             if sel = 1 then
                  ipb_out.ipb_rdata <= DPR_locations_to_rd_int;
                  ack <= ipb_in.ipb_strobe and not ack;
              end if;       
        end if;
        end if;
        end process;
        
       
       ipb_out.ipb_ack <= ack;
       ipb_out.ipb_err <= '0';
 
 DPR_locations_to_rd <= DPR_locations_to_rd_int;
 pre_ld_wr_addr  <= pre_ld_wr_addr_int;
 
end Behavioral;
