library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;
use ipbus_lib.ipbus_reg_types.all;
library infrastructure_lib;
use infrastructure_lib.ipbus_decode_efex_common_id_version.all;

--! @copydoc common_id_registers.vhd
entity common_id_registers is
  port(
    ipb_clk     : in  std_logic;
    ipb_rst     : in  std_logic;
    ipb_in      : in  ipb_wbus;
    ipb_out     : out ipb_rbus;
    Module_ID   : in  std_logic_vector(31 downto 0);
    xml_version : in  std_logic_vector(31 downto 0);
    xml_Gitsha  : in  std_logic_vector(31 downto 0);
    fw_version  : in  std_logic_vector(31 downto 0);
    fw_Gitsha   : in  std_logic_vector(31 downto 0);
    build_date  : in  std_logic_vector(31 downto 0);
    build_time  : in  std_logic_vector(31 downto 0)

    );


end common_id_registers;

architecture Behavioral of common_id_registers is

  signal ipbw : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal ipbr : ipb_rbus_array(N_SLAVES-1 downto 0);

begin

  fabric_common_IDversion : entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV      => N_SLAVES,  --defined in ipbus_decode_common_IdVersion
                SEL_WIDTH => ipbus_sel_width)
    port map(
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      sel             => ipbus_sel_efex_common_id_version(ipb_in.ipb_addr),
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      );

  Moduleid : entity ipbus_lib.ipbus_ctrlreg_v  -- reads the module ID register
    generic map (
      N_CTRL => 0,                             --number of control reg         
      N_STAT => 1)                             --number of status reg          
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_MODULE_ID),
      ipbus_out => ipbr(N_SLV_MODULE_ID),
      d         => (0 => Module_ID),
      q         => open,
      -- qmask     => (others => '0'),                      
      stb       => open);

  Xmlversion : entity ipbus_lib.ipbus_ctrlreg_v  -- reads the module ID register
    generic map (
      N_CTRL => 0,                               --number of control reg         
      N_STAT => 2)                               --number of status reg          
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_XML_VERSION),
      ipbus_out => ipbr(N_SLV_XML_VERSION),
      d         => (0 => xml_version, 1 => xml_Gitsha),
      q         => open,
      -- qmask     => (others => '0'),                      
      stb       => open);

  buildversion : entity ipbus_lib.ipbus_ctrlreg_v  -- reads the module ID register
    generic map (
      N_CTRL => 0,                                 --number of control reg         
      N_STAT => 2)                                 --number of status reg          
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_BUILD_TIME_AND_DATE),
      ipbus_out => ipbr(N_SLV_BUILD_TIME_AND_DATE),
      d         => (0 => build_date, 1 => build_time),
      q         => open,
      -- qmask     => (others => '0'),                      
      stb       => open);

  firmwareversion : entity ipbus_lib.ipbus_ctrlreg_v  -- reads the module ID register
    generic map (
      N_CTRL => 0,                                    --number of control reg         
      N_STAT => 2)                                    --number of status reg          
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_FIRMWARE_VERSION),
      ipbus_out => ipbr(N_SLV_FIRMWARE_VERSION),
      d         => (0 => fw_version, 1 => fw_Gitsha),
      q         => open,
      -- qmask     => (others => '0'),                      
      stb       => open);

end Behavioral;

