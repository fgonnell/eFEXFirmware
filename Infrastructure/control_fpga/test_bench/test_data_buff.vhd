----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.11.2018 14:15:27
-- Design Name: 
-- Module Name: test_data_buff - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.all;
LIBRARY infrastructure_lib;
USE infrastructure_lib.all;

entity test_data_buff is
--  Port ( );
end test_data_buff;

architecture Behavioral of test_data_buff is
signal pe, rxclk,clk160,reset,rd_en,read_enable,valid,fifo_empty:std_logic;
signal rxdata: std_logic_vector(31 downto 0);
signal data_buff: std_logic_vector(71 downto 0);
signal aurora_tready,first,payload_valid, payload_last,rd_fifo :std_logic;         
signal payload_data,s_axi_tx_tdata_hub2  :std_logic_vector( 63 downto 0);
signal s_axi_tx_tlast_hub2, s_axi_tx_tvalid_hub2,tready_data :std_logic;
signal crc_out:std_logic_vector( 19 downto 0);

begin


u0:entity infrastructure_lib.data_buffer
  Port map  ( 
      rx_clk   => clk160, --rxclk,
      clk160   => clk160 ,
      reset    => reset, 
      pe       => pe,
      rxdata   => rxdata,
      data_buff=> data_buff,
      rd_en    => rd_fifo ,
      empty    => fifo_empty,
      valid    => valid
      );


u1: entity infrastructure_lib.testbench_buffer 
   port map( 
      pe     =>  pe    ,
      clk280  =>  rxclk ,
      clk160  =>  clk160  ,
      rxdata =>  rxdata,
      reset  =>  reset,
      rdout  =>  rd_en
   );

-- Declarations
u2 :entity infrastructure_lib.rdout_data_process 
   port map( 
     clk           =>  clk160,
     fifo_empty    =>  fifo_empty,
     reset         =>  reset,
     rxdata_fifo   =>  data_buff(63 downto 0 ),
     tready_data   =>  tready_data , --'1'     ,
     valid         =>  valid            ,
     aurora_tready =>  aurora_tready   ,
     first         =>  first            ,
     payload_data  =>  payload_data     ,
     payload_last  =>  payload_last     ,
     payload_valid =>  payload_valid    ,
     rd_fifo       =>  rd_fifo          
   );

crc_cfpa_readout: entity infrastructure_lib.readout_cfpga
       Port map( 
              clk                 =>   clk160, --aurora_user_clk_hub2,    
              reset               =>   reset, --sys_reset_out_hub2,                   
              ---aurora                              
              aurora_tready       =>   '1' , --s_axi_tx_tready_hub2       ,
              axi4_m_data         =>   s_axi_tx_tdata_hub2  ,
              axi4_m_tlast        =>   s_axi_tx_tlast_hub2  ,
              axi4_m_tvalid       =>   s_axi_tx_tvalid_hub2 ,
              --readout           =>                      
              first               =>  first,
              payload_data        =>  payload_data,  
              payload_valid       =>  payload_valid ,
              payload_last        =>  payload_last ,
              tready_data         =>  tready_data  
              
              );

crc:entity work.CRC20 
--   generic( 
--     Nbits               => 64, 
--     CRC_Width           => 20, 
--     G_Poly              => "0x8349f", 
--     G_InitVal           => x"fffff" 
--     ); 
   Port Map( 
     CRC   =>  crc_out, 
     Calc  =>  '1' ,--run_crc ,
     Clk   =>  clk160,
     DIn   =>  s_axi_tx_tdata_hub2,
     Reset =>  '0' --first --crc_reset
     );







end Behavioral;
