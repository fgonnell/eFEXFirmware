--! @file
--! @brief eFEX Fibre mapping module
--! @details
--! This is eFEX Fibre mapping module
--! @author Ian Brawn
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library algolib;
use algolib.AlgoDataTypes.all;
library work;
use work.synch_type.all;
use work.EfexDataFormats.all;
library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.all;
use TOB_rdout_lib.data_type_pkg.all;

--! @copydoc fibremap_block.vhd
entity fibremap_block is
  generic (FPGA_NUMBER : integer);

  port (
    clk          : in std_logic;
    eFEXPosition_in : in std_logic_vector(31 downto 0);  --! Geographical position of eFEX module for dynamic mapping

    MGT_data_in  : in  mgt_data_out;
    BCID_out      : out BCID_array;
    RAW_data_out : out RAW_data_227_type;
    DataToAlg_out : out Algoinput   -- output to algorithm block
    );
end fibremap_block;

--! @copydoc fibremap_block.vhd
architecture struct of fibremap_block is

  -- The following are 3-dimensional arrays that hold the data from the MGTs,
  -- organised into eta and phi space, but still grouped into fibres.
  signal EmSyncIn     : FibreAllEm;
  signal EmSyncOut    : FibreAllEm;
  signal HadSyncIn    : FibreAllHad;
  signal HadSyncOut   : FibreAllHad;
  signal SpareSyncIn  : FibreAllSpare;
  signal SpareSyncOut : FibreAllSpare;

  signal fpga_number_sig : integer;

--  component RegSyncLogic
--    port (
--      DataIn  : in  std_logic_vector(226 downto 0);
--      DataOut : out std_logic_vector(226 downto 0)
--     --clk:     in  std_logic
--      );
--  end component;


----------------------------------------------------------------------------------------------------
begin
  fpga_number_sig <= FPGA_NUMBER;

  -- Organise data from MGTs using this procedure:
  FibreArrayBuilder (
    fpga_number_sig,
    -- Inputs:
    MGT_DATA_in,
    -- Outputs:
    EmSyncIn,
    HadSyncIn,
    SpareSyncIn
    );

 RAW_data_out <= to_raw_data(EmSyncIn, HadSyncIn);

  -- Instantiate Sync Logic for all channels of Em data:
  gen_Em_eta : for x in 0 to NUM_FIB_Em_ETA-1 generate
    gen_Em_phi : for y in 0 to NUM_FIB_Em_PHI-1 generate
      -- instantiate sync logic...
      Emsync : entity work.RegSyncLogic port map (DataIn => EmSyncIn(x)(y), DataOut => EmSyncOut(x)(y));
    -- EmSyncIn(x)(y) is a 224-bit std_logic_vector
    end generate gen_Em_phi;
  end generate gen_Em_eta;


  -- Instantiate Sync Logic for all channels of Had data:
  gen_Had_eta : for x in 0 to NUM_FIB_Had_ETA-1 generate
    gen_Had_phi : for y in 0 to NUM_FIB_Had_PHI-1 generate
      -- instantiate sync logic...
      Hadsync : entity work.RegSyncLogic port map (DataIn => HadSyncIn(x)(y), DataOut => HadSyncOut(x)(y));
    -- HadSyncIn(x)(y) is a 224-bit std_logic_vector
    end generate gen_Had_phi;
  end generate gen_Had_eta;


  -- Instantiate Sync Logic for all channels of Spare data:
  gen_spare : for x in 0 to NUM_FIB_SPARE-1 generate
    -- instantiate sync logic...
    sparesync : entity work.RegSyncLogic port map (DataIn => SpareSyncIn(x), DataOut => SpareSyncOut(x));
  -- SpareSyncIn(x)(y) is a 224-bit std_logic_vector
  end generate gen_spare;


  -- Re-map output of sync logic into format expected by algorithm logic:
  DataToAlg_out <= AlgoTowerBuilder(EmSyncOut, HadSyncOut, SpareSyncOut, eFEXPosition_in);
  -- And DataToAlg will go off to algorithm logic....

  em_BCID_eta : for x in 0 to NUM_FIB_em_ETA-1 generate
    em_BCID_phi : for y in 0 to NUM_FIB_em_PHI-1 generate
      BCID_out(x*NUM_FIB_em_PHI + y) <= MGT_to_SuperCells(EmSyncOut(x)(y)).BCID;
    end generate em_BCID_phi;
  end generate em_BCID_eta;

  had_BCID_eta : for x in 0 to NUM_FIB_had_ETA-1 generate
    had_BCID_phi : for y in 0 to NUM_FIB_had_PHI-1 generate
      BCID_out(40 + x*NUM_FIB_had_PHI + y) <= MGT_to_SuperCells(HadSyncOut(x)(y)).BCID;
    end generate had_BCID_phi;
  end generate had_BCID_eta;

  BCID_out(59) <= MGT_to_SuperCells(SpareSyncOut(0)).BCID;


end struct;
