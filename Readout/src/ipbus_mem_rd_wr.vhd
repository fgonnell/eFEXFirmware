--! @file
--! @brief Read/Write interface for SPY Memory
--! @details 
--! This is the top module of Read/Write interface for SPY Memory
--! The input are Calorimeter data at the input to the algorithm block
--! the output is 32b including the header and trailer
--! the SPY memory is in parallel with the Link Output FIFO
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library ipbus_lib;
use ieee.numeric_std.all;
use ipbus_lib.ipbus.all;

--! @copydoc ipbus_mem_rd_wr.vhd
entity ipbus_mem_rd_wr is
    port(
        ipb_clk : in std_logic;
        ipb_rst : in std_logic;
        ipbus_in: in ipb_wbus;
        ipbus_out: out ipb_rbus;
        SPY_mem_rd_addr_en     : out STD_LOGIC;                      
        SPY_mem_rd_addr        : out STD_LOGIC_VECTOR (10 downto 0); 
        SPY_mem_rd_data        : in  STD_LOGIC_VECTOR (31 downto 0)
        );
end ipbus_mem_rd_wr;


--! @copydoc ipbus_mem_rd_wr.vhd
architecture Behavioral of ipbus_mem_rd_wr is

    signal ack          : std_logic := '0';
    signal ack2         : std_logic := '0';
    signal rd_en_i      : std_logic := '0';

begin

    process(ipb_clk)
    begin
        if rising_edge(ipb_clk) then
			if ipb_rst = '1' then
                ack <= '0';
                ack2 <= '0';
                rd_en_i <= '0';
            else
                ipbus_out.ipb_rdata     <= SPY_mem_rd_data ;   -- data from SPY memory
                if ipbus_in.ipb_strobe = '1' and ipbus_in.ipb_write = '0' then
                    rd_en_i <= '1';
                else
                    rd_en_i <= '0';
                end if;
            end if ;
        ack2 <= ipbus_in.ipb_strobe and (not ack2) and (not ack);
        ack  <= ack2;
    end if;
    end process;

    SPY_mem_rd_addr_en      <= rd_en_i;        -- strobe the read address
    SPY_mem_rd_addr         <= ipbus_in.ipb_addr(10 downto 0) ;   

    ipbus_out.ipb_ack <= ack;
    ipbus_out.ipb_err <= '0';


end Behavioral;
