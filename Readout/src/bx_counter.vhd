----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/30/2018 03:57:01 PM
-- Design Name: 
-- Module Name: bx_counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


ENTITY bx_counter IS
   PORT( 
      CE      : IN     std_logic;
      CLK     : IN     std_logic;
      RST     : IN     std_logic;
      Q       : OUT    std_logic_vector (11 DOWNTO 0)
   );

-- Declarations

END bx_counter ;

-- this is a loadable 12-bit binary counter
ARCHITECTURE rtl OF bx_counter IS
BEGIN
process (CLK)
	variable temp : unsigned (11 downto 0):= (others => '0') ;
	begin
	if CLK'event AND CLK = '1' then
     	if RST = '1' then                 -- this is a synchronous reset
      		temp := (others => '0');              -- set counter to ZERO
		else
		  if CE = '1' then
		      if temp =3563 then	     -- if count = terminal count then
				temp := (others => '0');			    -- set counter to ZERO
			  else	
				temp := temp + 1;			          -- else increment the value by one 
			  end if;
	      end if;
		end if;
	end if;
	Q <= std_logic_vector(temp) ;	 -- push out new value
	end process;
END rtl;

