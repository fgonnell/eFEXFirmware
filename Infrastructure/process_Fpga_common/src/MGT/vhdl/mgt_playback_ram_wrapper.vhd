-------------------------------------------------------
--! @file
--! @brief Wrapper for the input spy/playback RAM of the MGT
--! @details 
--! generalised from: ipbus_dpram
--! @author Francesco Gonnella
-------------------------------------------------------

--! Use standard library
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

--! Use ipbus library 
library ipbus_lib;
use ipbus_lib.ipbus.all;


--! @copydoc mgt_playback_ram_wrapper.vhd
entity mgt_playback_ram_wrapper is
  generic (DISABLE : std_logic := '0');
  port(
    clk_ipb : in  std_logic;
    rst     : in  std_logic;
    ipb_in  : in  ipb_wbus;
    ipb_out : out ipb_rbus;
    rdy     : in  std_logic;
    rclk : in  std_logic; -- mgt rx clock of 280 MHz
    din  : in  std_logic_vector(31 downto 0);
    we   : in  std_logic := '0';
    q    : out std_logic_vector(31 downto 0);
	kchar: out  std_logic
    
    );

end mgt_playback_ram_wrapper;

--! @copydoc mgt_playback_ram_wrapper.vhd
architecture rtl of mgt_playback_ram_wrapper is

  component mgt_playback_ram
    port (
      clka  : in  std_logic;
      ena   : in  std_logic;
      wea   : in  std_logic_vector(0 downto 0);
      addra : in  std_logic_vector(6 downto 0);
      dina  : in  std_logic_vector(31 downto 0);
      douta : out std_logic_vector(31 downto 0);
      clkb  : in  std_logic;
      enb   : in  std_logic;
      web   : in  std_logic_vector(0 downto 0);
      addrb : in  std_logic_vector(6 downto 0);
      dinb  : in  std_logic_vector(31 downto 0);
      doutb : out std_logic_vector(31 downto 0)
      );
  end component;

  --ipbus signals
  signal ack, ack2    : std_logic;
  signal enb          : std_logic;
  signal addr         : std_logic_vector(6 downto 0);
  signal ipbus_write  : std_logic_vector(0 downto 0);
  signal write_enable : std_logic_vector(0 downto 0);

  --signal for disabling the RAM
  signal ipb_out_int : std_logic_vector(31 downto 0);
  signal q_int       : std_logic_vector(31 downto 0);
  

begin
  IPBUS_RAM : process(ipb_in.ipb_strobe)
  begin
    if rising_edge(clk_ipb) then
      if ipb_in.ipb_strobe = '1' and ipb_in.ipb_write = '1' then
        ipbus_write(0) <= '1';
        
      else
        
        ipbus_write(0) <= '0';
      end if;
       
      ack <= ipb_in.ipb_strobe and (not ack);
--      ack2 <= ipb_in.ipb_strobe and (not ack2) and (not ack);
 --     ack  <= ack2;

    end if;
end process;

  ipb_out.ipb_ack <= ack;
  ipb_out.ipb_err <= '0';
  write_enable(0) <= we;

  PLAYBACK_RAM : mgt_playback_ram
    port map (
      clka  => clk_ipb,
      ena   => ipb_in.ipb_strobe,
      wea   => ipbus_write,
      addra => ipb_in.ipb_addr(6 downto 0),
      dina  => ipb_in.ipb_wdata,
      douta => ipb_out_int,     --ipb_out.ipb_rdata,
      clkb  => rclk,
      enb   => enb,
      web   => (others => '0'),     -- was write_enable,
      addrb => addr,
      dinb  => (others => '0'), -- was din,
      doutb => q_int
      );

  q                 <= q_int       when DISABLE = '0' else (others => '0');
  ipb_out.ipb_rdata <= ipb_out_int when DISABLE = '0' else x"d15ab1ed";

  
  
sm_playback:  ENTITY work.ctrl_playback_ram 
   port map( 
      clk   => rclk,
      rdy   => rdy, 
      reset => rst,
      addr  => addr, 
      en    => enb, 
      kchar => kchar
   );
  
 
  
end rtl;
