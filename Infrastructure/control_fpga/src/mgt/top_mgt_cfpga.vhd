
-- Company: STFC
-- Engineer: Mohammed Siyad
--
-- Create Date: 21.04.2020 15:31:24
-- Design Name:
-- Module Name: top_mgt_cfpga - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
LIBRARY ipbus_lib;
USE ipbus_lib.ipbus.all;
LIBRARY infrastructure_lib;
USE infrastructure_lib.all;
use infrastructure_lib.mgt_type.all;
use infrastructure_lib.packet_mux_type.all;
use infrastructure_lib.ipbus_decode_efex_cntrl_mgt.all;
--!@copydoc top_mgt_cfpga.vhd

entity top_mgt_cfpga is
	GENERIC(
		NProcessorFPGA: positive := 4	-- first TOB then Input Data for each FPGA in turn
	);
	Port (
	    start                   : in  std_logic;
		clk40					: in  std_logic;                     -- clk40 generatred from ttc clock
		clk160					: in  std_logic;
		rx_clk280				: out std_logic_vector(7 downto 0);
		rx_clk160				: out std_logic_vector(3 downto 0); --11 --- Generated rx clocks of 160MHz from the mgt pll.- Generated rx clocks of 160MHz from the mgt pll.
		Q_CLK_GTREFCLK_PAD_N_IN	: in  std_logic_vector(2 downto 0);  --2
		Q_CLK_GTREFCLK_PAD_P_IN	: in  std_logic_vector(2 downto 0);  --2
		RXN_IN					: in  std_logic_vector(11 downto 0); --11
		RXP_IN					: in  std_logic_vector(11 downto 0); --11
		TXN_OUT					: out std_logic_vector(11 downto 0);   --7
		TXP_OUT					: out std_logic_vector(11 downto 0);   --7
		rx_resetdone			: out std_logic_vector(11 downto 0);  --11
		rx_fsm_resetdone		: out std_logic_vector(11 downto 0);  --11
		rx_byteisaligned		: out std_logic_vector(11 downto 0);  --11
		tx_resetdone			: out std_logic_vector(11 downto 0);  --11
		tx_fsm_resetdone		: out std_logic_vector(11 downto 0);  --11
		tx_bufstatus			: out std_logic_vector (23 downto 0); --23
		rx_realign				: out std_logic_vector (11 downto 0); --11
		rx_disperr				: out std_logic_vector (47 downto 0); --47  -- rx_disperr for debug
		encode_error			: out std_logic_vector (47 downto 0); --47  --- 10/8 encoder for debug
		mgt_commadret			: out std_logic_vector(1 downto 0);   --9 (just for incoming Hub data...)
		loopback				: in  std_logic_vector(5 downto 0);   --5
		mgt_SOFT_RESET_TX_IN	: in  std_logic_vector(2 downto 0);   --2
		mgt_SOFT_RESET_RX_IN	: in  std_logic_vector(2 downto 0);   --2
		mgt_QPLLLOCK_OUT        : out std_logic_vector(2 downto 0);
		mgt_QPLLREFCLKLOST_OUT  : out std_logic_vector(2 downto 0); 
-- data from MGT
		data_from_mgt_bus		: out mgt_data_array(NProcessorFPGA*2 - 1 downto 0);	-- first TOB then Input Data for each FPGA in turn
		char_is_k_bus			: out packet_sl_array(NProcessorFPGA*2 - 1 downto 0);	-- first TOB then Input Data for each FPGA in turn
		error_from_mgt_bus		: out packet_sl_array(NProcessorFPGA*2 - 1 downto 0);	-- first TOB then Input Data for each FPGA in turn
		hub1_rx_data			: out std_logic_vector(31 downto 0);  -- ttc information
		hub2_rx_data			: out std_logic_vector(31 downto 0)   -- rod busy.
	);
end top_mgt_cfpga;

architecture Behavioral of top_mgt_cfpga is

COMPONENT ttc_ila

PORT (
	clk : IN STD_LOGIC;



	probe0 : IN STD_LOGIC_VECTOR(83 DOWNTO 0)
);
END COMPONENT  ;

signal probe0                 : std_logic_vector( 67 downto 0);
signal gt_rxpd                : mgt_rxpd_array(2 downto 0);
signal gt_txpd                : mgt_txpd_array(2 downto 0);
signal mgt_DATA_VALID_IN      : std_logic_vector(5 downto 0);
signal MGT_RXN_in,MGT_RXP_in  : mgt_rx_array(2 downto 0);
signal MGT_TXN_in,MGT_TXP_in  : mgt_tx_array(2 downto 0);
signal mgt_TXUSRCLK_OUT,mgt_RXUSRCLK_OUT,mgt_tx_fsm_resetdone, mgt_rx_fsm_resetdone,MGT_Commadet_int : std_logic_vector(11 downto 0);
signal mgt_QPLLREFCLKLOST_OUT_i,mgt_QPLLLOCK_OUT_i : std_logic_vector(1 downto 0);
signal rxdata_quad_array      : mgt_rxdata_array (2 downto 0);
signal mgt_txdata             : mgt_txdata_array(2 downto 0);
signal mgt_loopback_reg       : std_logic_vector (14 downto 0);
signal mgt_txbufstatus        : mgt_txbufstatus_array(2 downto 0);
signal mgt_rxcommadet         : mgt_rxcommadet_array (2 downto 0);
signal mgt_rxbyterealign      : mgt_rxbyterealign_array(2 downto 0);
signal mgt_rx_resetdone       : mgt_rxresetdone_array  (2 downto 0);
signal mgt_rxbyteisaligned    : mgt_rxbyteisaligned_array (2 downto 0);
signal mgt_tx_resetdone       : mgt_txresetdone_array   (2 downto 0);
signal mgt_txcharisk          : mgt_txcharisk_array  (2 downto 0);
signal mgt_loopback_in        : mgt_loopback_array (2 downto 0);
signal mgt_rxchariscomma      : mgt_rxchariskcomm_array(2 downto 0);
signal mgt_rxcharisk          : mgt_rxcharisk_array(2 downto 0);
signal mgt_rxdisperr          : mgt_rxdisperr_array(11 downto 0);
signal mgt_rxnotintable       : mgt_rxnotintable_array(11 downto 0);
signal rxbyteisaligned:std_logic_vector(11 downto 0);
signal mgt_rxcharisk_reg,txcharisk : std_logic_vector(47 downto 0);
signal reset:std_logic;
signal rx_resetdone_i :std_logic_vector(11 downto 0); --11
signal rx_disperr_i, encode_error_i: std_logic_vector (47 downto 0);



begin

rx_disperr    <= rx_disperr_i;
encode_error  <= encode_error_i;


rx_resetdone <= rx_resetdone_i(11 downto 0 );
reset <= '0';

mgt_QPLLLOCK_OUT       <= mgt_QPLLLOCK_OUT_i& '1';   
mgt_QPLLREFCLKLOST_OUT <= mgt_QPLLREFCLKLOST_OUT_i & '0';

        rx_pwr_on_gen:for i in 0 to 2 --loop   --0 to 2
        
        

          generate
             gt_rxpd(i).gt0_rxpd <= "00";
             gt_rxpd(i).gt1_rxpd <= "00";
             gt_rxpd(i).gt2_rxpd <= "00";
             gt_rxpd(i).gt3_rxpd <= "00";
             gt_txpd(i).gt0_txpd <= "11";
             gt_txpd(i).gt1_txpd <= "11";
             gt_txpd(i).gt2_txpd <= "11";
             gt_txpd(i).gt3_txpd <= "11";
          end generate;
          
--            gt_rxpd(0).gt0_rxpd <= "00";
--            gt_rxpd(0).gt1_rxpd <= "00";
--            gt_rxpd(0).gt2_rxpd <= "00";
--            gt_rxpd(0).gt3_rxpd <= "00";
            
--            gt_txpd(0).gt0_txpd <= "11";
--            gt_txpd(0).gt1_txpd <= "11";
--            gt_txpd(0).gt2_txpd <= "11";
--            gt_txpd(0).gt3_txpd <= "11";
            
            


  -----------------------------------
	--rx_clk160 <=  mgt_RXUSRCLK_OUT(3 downto 0);
	rx_clk160 <=  mgt_RXUSRCLK_OUT(11 downto 8);
	rx_clk280 <=  mgt_RXUSRCLK_OUT(7 downto 0);

-- assign rx data
Processor_1_TOB: process(mgt_RXUSRCLK_OUT(0))
	Begin
		If rising_edge(mgt_RXUSRCLK_OUT(0)) then
			data_from_mgt_bus(0) <= rxdata_quad_array(0).gt0_rxdata_out;
			char_is_k_bus(0) <= mgt_rxcharisk(0).gt0_rxcharisk_out(0);
			if (mgt_rxdisperr(0).gt0_rxdisperr = "0000") and (mgt_rxnotintable(0).gt0_rxnotintable = "0000") then
				error_from_mgt_bus(0) <= '0';
			else
				error_from_mgt_bus(0) <= '1';
			end if;
		end if;
	End Process Processor_1_TOB;

Processor_1_Raw: process(mgt_RXUSRCLK_OUT(1))
	Begin
		If rising_edge(mgt_RXUSRCLK_OUT(1)) then
			data_from_mgt_bus(1) <= rxdata_quad_array(0).gt1_rxdata_out;
			char_is_k_bus(1) <= mgt_rxcharisk(0).gt1_rxcharisk_out(0);
			if (mgt_rxdisperr(0).gt1_rxdisperr = "0000") and (mgt_rxnotintable(0).gt1_rxnotintable = "0000") then
				error_from_mgt_bus(1) <= '0';
			else
				error_from_mgt_bus(1) <= '1';
			end if;
		end if;
	End Process Processor_1_Raw;

Processor_2_TOB: process(mgt_RXUSRCLK_OUT(2))
	Begin
		If rising_edge(mgt_RXUSRCLK_OUT(2)) then
			data_from_mgt_bus(2) <= rxdata_quad_array(0).gt2_rxdata_out;
			char_is_k_bus(2) <= mgt_rxcharisk(0).gt2_rxcharisk_out(0);
			if (mgt_rxdisperr(0).gt2_rxdisperr = "0000") and (mgt_rxnotintable(0).gt2_rxnotintable = "0000") then
				error_from_mgt_bus(2) <= '0';
			else
				error_from_mgt_bus(2) <= '1';
			end if;
		end if;
	End Process Processor_2_TOB;

Processor_2_Raw: process(mgt_RXUSRCLK_OUT(3))
	Begin
		If rising_edge(mgt_RXUSRCLK_OUT(3)) then
			data_from_mgt_bus(3) <= rxdata_quad_array(0).gt3_rxdata_out;
			char_is_k_bus(3) <= mgt_rxcharisk(0).gt3_rxcharisk_out(0);
			if (mgt_rxdisperr(0).gt3_rxdisperr = "0000") and (mgt_rxnotintable(0).gt3_rxnotintable = "0000") then
				error_from_mgt_bus(3) <= '0';
			else
				error_from_mgt_bus(3) <= '1';
			end if;
		end if;
	End Process Processor_2_Raw;
	
	
Processor_3_TOB: process(mgt_RXUSRCLK_OUT(4))
	Begin
		If rising_edge(mgt_RXUSRCLK_OUT(4)) then
			data_from_mgt_bus(4) <= rxdata_quad_array(1).gt0_rxdata_out;
			char_is_k_bus(4) <= mgt_rxcharisk(1).gt0_rxcharisk_out(0);
			if (mgt_rxdisperr(1).gt0_rxdisperr = "0000") and (mgt_rxnotintable(1).gt0_rxnotintable = "0000") then
				error_from_mgt_bus(4) <= '0';
			else
				error_from_mgt_bus(4) <= '1';
			end if;
		end if;
	End Process Processor_3_TOB;

Processor_3_Raw: process(mgt_RXUSRCLK_OUT(5))
	Begin
		If rising_edge(mgt_RXUSRCLK_OUT(5)) then
			data_from_mgt_bus(5) <= rxdata_quad_array(1).gt1_rxdata_out;
			char_is_k_bus(5) <= mgt_rxcharisk(1).gt1_rxcharisk_out(0);
			if (mgt_rxdisperr(1).gt1_rxdisperr = "0000") and (mgt_rxnotintable(1).gt1_rxnotintable = "0000") then
				error_from_mgt_bus(5) <= '0';
			else
				error_from_mgt_bus(5) <= '1';
			end if;
		end if;
	End Process Processor_3_Raw;
		
	
	Processor_4_TOB: process(mgt_RXUSRCLK_OUT(6))
	Begin
		If rising_edge(mgt_RXUSRCLK_OUT(6)) then
			data_from_mgt_bus(6) <= rxdata_quad_array(1).gt2_rxdata_out;
			char_is_k_bus(6) <= mgt_rxcharisk(1).gt2_rxcharisk_out(0);
			if (mgt_rxdisperr(1).gt2_rxdisperr = "0000") and (mgt_rxnotintable(1).gt2_rxnotintable = "0000") then
				error_from_mgt_bus(6) <= '0';
			else
				error_from_mgt_bus(6) <= '1';
			end if;
		end if;
	End Process Processor_4_TOB;

Processor_4_Raw: process(mgt_RXUSRCLK_OUT(7))
	Begin
		If rising_edge(mgt_RXUSRCLK_OUT(7)) then
			data_from_mgt_bus(7) <= rxdata_quad_array(1).gt3_rxdata_out;
			char_is_k_bus(7) <= mgt_rxcharisk(1).gt3_rxcharisk_out(0);
			if (mgt_rxdisperr(1).gt3_rxdisperr = "0000") and (mgt_rxnotintable(1).gt3_rxnotintable = "0000") then
				error_from_mgt_bus(7) <= '0';
			else
				error_from_mgt_bus(7) <= '1';
			end if;
		end if;
	End Process Processor_4_Raw;
		
	
  

	hub1_rx_data    <= rxdata_quad_array(2).gt0_rxdata_out; --2
	hub2_rx_data    <= rxdata_quad_array(2).gt1_rxdata_out; --2
	mgt_commadret   <= MGT_Commadet_int(9 downto 8);  

  --------------------------------------------------------

 
  scope_ttc : ttc_ila
PORT MAP (
	clk                  => mgt_TXUSRCLK_OUT(0),--clk160,
	probe0(31 downto 0)  => rxdata_quad_array (2).gt0_rxdata_out,
	probe0(63 downto 32) => rxdata_quad_array (2).gt1_rxdata_out,
	Probe0(64)           => rx_resetdone_i(8),
	probe0(65)           => rx_resetdone_i(9),
	probe0(67 downto 66) => MGT_Commadet_int(9 downto 8),
	probe0(75 downto 68) => rx_disperr_i  (39 downto 32),
	probe0(83 downto 76) => encode_error_i(39 downto 32) 
	
);

  --mgt for receiving topo and raw data from the processing fpga1,fpga2,fpga3 and fpga4

  MGT_TX_RX_6G4 : entity infrastructure_lib.MGT_quad_gen
               generic map ( num_quad_tx_rx => 1 )
     port map (
         ---refclk160                 => clk160,
         TTC_CLK                   =>  clk40,
         MGT_CLK_GTREFCLK_PAD_N_IN =>  Q_CLK_GTREFCLK_PAD_N_IN (2 downto 2 ), --2
         MGT_CLK_GTREFCLK_PAD_P_IN =>  Q_CLK_GTREFCLK_PAD_P_IN (2 downto 2), --2
         mgt_TXUSRCLK_OUT          =>  mgt_TXUSRCLK_OUT(11 downto 8),        --11
         mgt_RXUSRCLK_OUT          =>  mgt_RXUSRCLK_OUT(11 downto 8),        --11
         mgt_SOFT_RESET_TX_IN      =>  mgt_SOFT_RESET_TX_IN(2 downto 2),     --2
         mgt_SOFT_RESET_RX_IN      =>  mgt_SOFT_RESET_RX_IN(2 downto 2),     --2
          -- data
         RXN_IN                    =>  MGT_RXN_IN(2 downto 2),               --2
         RXP_IN                    =>  MGT_RXP_IN(2 downto 2),               --2
         TXN_IN                    =>  MGT_TXN_IN(2 downto 2),               --2
         TXP_IN                    =>  MGT_TXP_IN(2 downto 2),               --2
         rxdata_quad_array         =>  rxdata_quad_array (2 downto 2),       --2
         txdata_quad_array         =>  mgt_txdata(2 downto 2),               --2
    -- status and monitoring  =>                                             --2
         gt_rxpd_array             =>  gt_rxpd (2 downto 2),                 --2
         gt_txpd_array             =>  gt_txpd (2 downto 2),
         mgt_TX_FSM_RESET_DONE     =>  mgt_tx_fsm_resetdone(11 downto 8),    --11
         mgt_RX_FSM_RESET_DONE     =>  mgt_rx_fsm_resetdone(11 downto 8),    --11
         rxbyteisaligned_quad_array=>  mgt_rxbyteisaligned (2 downto 2),       --2
         rxresetdone_quad_array    =>  mgt_rx_resetdone (2 downto 2),          --2
         txresetdone_quad_array    =>  mgt_tx_resetdone (2 downto 2),          --2
         loopback_quad_array       =>  mgt_loopback_in (2 downto 2),           --2
         rxchariscomma_quad_array  =>  mgt_rxchariscomma (2 downto 2),         --2
         rxcharisk_quad_array      =>  mgt_rxcharisk (2 downto 2),             --2
         txcharisk_quad_array      =>  mgt_txcharisk (2 downto 2),             --2
         txbufstatus_quad_array    =>  mgt_txbufstatus (2 downto 2),           --2
         rxbyterealign_quad_array  =>  mgt_rxbyterealign (2 downto 2),         --2
         rxcommadet_quad_array     =>  mgt_rxcommadet (2 downto 2),            --2
         rxdisperr_quad_array      =>  mgt_rxdisperr (2 downto 2),             --2
         rxnotintable_quad_array   =>  mgt_rxnotintable (2 downto 2)           --2
        -- mgt_QPLLREFCLKLOST_OUT    =>  mgt_QPLLREFCLKLOST_OUT (0 downto 0),
        -- mgt_QPLLLOCK_OUT          =>  mgt_QPLLLOCK_OUT(0 downto 0)


       );

       
   --------- mgt quad generate for 11.2 G 
	  
	 MGT_TX_RX_11G2 : entity infrastructure_lib.mgt11g2_tx_rx_cfpga_gen 
	       
		generic map(num_quad_tx_rx => 2)

     port map (
         TTC_CLK                   =>  clk40,
         MGT_CLK_GTREFCLK_PAD_N_IN =>  Q_CLK_GTREFCLK_PAD_N_IN (1 downto 0), 
         MGT_CLK_GTREFCLK_PAD_P_IN =>  Q_CLK_GTREFCLK_PAD_P_IN (1 downto 0), 
         mgt_TXUSRCLK_OUT          =>  mgt_TXUSRCLK_OUT(7 downto 0),        
         mgt_RXUSRCLK_OUT          =>  mgt_RXUSRCLK_OUT(7 downto 0),        
         mgt_SOFT_RESET_TX_IN      =>  mgt_SOFT_RESET_TX_IN(1 downto 0),      
         mgt_SOFT_RESET_RX_IN      =>  mgt_SOFT_RESET_RX_IN(1 downto 0),     
          -- data
         RXN_IN                    =>  MGT_RXN_IN(1 downto 0),              
         RXP_IN                    =>  MGT_RXP_IN(1 downto 0),              
         TXN_IN                    =>  MGT_TXN_IN(1 downto 0),              
         TXP_IN                    =>  MGT_TXP_IN(1 downto 0),              
         rxdata_quad_array         =>  rxdata_quad_array (1 downto 0),      
         txdata_quad_array         =>  mgt_txdata(1 downto 0),              
    -- status and monitoring  =>                   
         mgt_DATA_VALID_IN         => "11111111",
         gt_rxpd_array             =>  gt_rxpd (1 downto 0),                
         gt_txpd_array             =>  gt_txpd (1 downto 0),
         mgt_TX_FSM_RESET_DONE     =>  mgt_tx_fsm_resetdone(7 downto 0),    
         mgt_RX_FSM_RESET_DONE     =>  mgt_rx_fsm_resetdone(7 downto 0),    
         rxbyteisaligned_quad_array=>  mgt_rxbyteisaligned (1 downto 0),    
         rxresetdone_quad_array    =>  mgt_rx_resetdone (1 downto 0),       
         txresetdone_quad_array    =>  mgt_tx_resetdone (1 downto 0),       
        -- loopback_quad_array       =>  mgt_loopback_in (2 downto 1),        
         rxchariscomma_quad_array  =>  mgt_rxchariscomma (1 downto 0),      
         rxcharisk_quad_array      =>  mgt_rxcharisk (1 downto 0),            
         txcharisk_quad_array      =>  mgt_txcharisk (1 downto 0),            
         txbufstatus_quad_array    =>  mgt_txbufstatus (1 downto 0),          
         rxbyterealign_quad_array  =>  mgt_rxbyterealign (1 downto 0),        
         rxcommadet_quad_array     =>  mgt_rxcommadet (1 downto 0),           
         rxdisperr_quad_array      =>  mgt_rxdisperr (1 downto 0),            
         rxnotintable_quad_array   =>  mgt_rxnotintable (1 downto 0),          
         mgt_QPLLREFCLKLOST_OUT    =>  mgt_QPLLREFCLKLOST_OUT_i (1 downto 0),
         mgt_QPLLLOCK_OUT          =>  mgt_QPLLLOCK_OUT_i(1 downto 0)
         
         );
	   	   
	   

        mgt_gen :    for i in 0 to 2 --loop

        generate

            MGT_Commadet_int(i+3*i downto i+3*i)     <= mgt_rxchariscomma(i).gt0_rxchariscomma_out(0 downto 0); -- asginment of the rxchriscomma
            MGT_Commadet_int(i+1+3*i downto i+1+3*i) <= mgt_rxchariscomma(i).gt1_rxchariscomma_out(0 downto 0); -- asginment of the rxchriscomma
            MGT_Commadet_int(i+2+3*i downto i+2+3*i) <= mgt_rxchariscomma(i).gt2_rxchariscomma_out(0 downto 0); -- asginment of the rxchriscomma
            MGT_Commadet_int(i+3+3*i downto i+3+3*i) <= mgt_rxchariscomma(i).gt3_rxchariscomma_out(0 downto 0); -- asginment of the rxchriscomma

            rx_realign (i+3*i)  <= mgt_rxbyterealign(i).gt0_rxbyterealign;  -- asignment of rxbyterealign
            rx_realign(i+1+3*i) <= mgt_rxbyterealign(i).gt1_rxbyterealign;  -- asignment of rxbyterealign
            rx_realign(i+2+3*i) <= mgt_rxbyterealign(i).gt2_rxbyterealign;  -- asignment of rxbyterealign
            rx_realign(i+3+3*i) <= mgt_rxbyterealign(i).gt3_rxbyterealign;  -- asignment of rxbyterealign

            rx_resetdone_i (i+3*i)  <= mgt_rx_resetdone(i).gt0_rxresetdone;    -- asignment of rx_resetdone
            rx_resetdone_i(i+1+3*i) <= mgt_rx_resetdone(i).gt1_rxresetdone;    -- asignment of rx_resetdone
            rx_resetdone_i(i+2+3*i)  <= mgt_rx_resetdone(i).gt2_rxresetdone;   -- asignment of rx_resetdone
            rx_resetdone_i(i+3+3*i)  <= mgt_rx_resetdone(i).gt3_rxresetdone;   -- asignment of rx_resetdone

            rxbyteisaligned (i+3*i)  <= mgt_rxbyteisaligned(i).gt0_rxbyteisaligned;
            rxbyteisaligned(i+1+3*i) <= mgt_rxbyteisaligned(i).gt1_rxbyteisaligned;
            rxbyteisaligned(i+2+3*i) <= mgt_rxbyteisaligned(i).gt2_rxbyteisaligned;
            rxbyteisaligned(i+3+3*i) <= mgt_rxbyteisaligned(i).gt3_rxbyteisaligned;

            mgt_rxcharisk_reg(i+3+15*i downto i+15*i)     <= mgt_rxcharisk(i).gt0_rxcharisk_out;
            mgt_rxcharisk_reg(i+7+15*i downto i+4+15*i)   <= mgt_rxcharisk(i).gt1_rxcharisk_out;
            mgt_rxcharisk_reg(i+11+15*i downto i+8+15*i)  <= mgt_rxcharisk(i).gt2_rxcharisk_out;
            mgt_rxcharisk_reg(i+15+15*i downto i+12+15*i) <= mgt_rxcharisk(i).gt3_rxcharisk_out;


            rx_disperr_i(i+3+15*i downto i+15*i)     <= mgt_rxdisperr(i).gt0_rxdisperr;
            rx_disperr_i(i+7+15*i downto i+4+15*i)   <= mgt_rxdisperr(i).gt1_rxdisperr;
            rx_disperr_i(i+11+15*i downto i+8+15*i)  <= mgt_rxdisperr(i).gt2_rxdisperr;
            rx_disperr_i(i+15+15*i downto i+12+15*i) <= mgt_rxdisperr(i).gt3_rxdisperr;

            encode_error_i(i+3+15*i downto i+15*i)     <= mgt_rxnotintable(i).gt0_rxnotintable; -- asignment of 10b/8b encoding error-
            encode_error_i(i+7+15*i downto i+4+15*i)   <= mgt_rxnotintable(i).gt1_rxnotintable; -- asignment of 10b/8b encoding error
            encode_error_i(i+11+15*i downto i+8+15*i)  <= mgt_rxnotintable(i).gt2_rxnotintable; -- asignment of 10b/8b encoding error
            encode_error_i(i+15+15*i downto i+12+15*i) <= mgt_rxnotintable(i).gt3_rxnotintable; -- asignment of 10b/8b encoding error

            MGT_RXN_IN(i).RXN_IN <= RXN_IN(i+3+3*i downto i+3*i);  -- rx input asignment
            MGT_RXP_IN(i).RXP_IN <= RXP_IN(i+3+3*i downto i+3*i);  -- rx input asignment

            
            TXN_OUT(i+3+3*i downto i+3*i) <= MGT_TXN_IN(i).TXN_OUT;
            TXP_OUT(i+3+3*i downto i+3*i) <= MGT_TXP_IN(i).TXP_OUT;
            
            
        end generate mgt_gen;

   




end Behavioral;


