
####################### GT reference clock constraints #########################
create_clock -period 6.250 -name gt_clk01 [get_ports {Q_CLK_GTREFCLK_PAD_N_IN[0]}]
create_clock -period 6.250 -name gt_clk02 [get_ports {Q_CLK_GTREFCLK_PAD_N_IN[1]}]
create_clock -period 6.250 -name gt_clk03 [get_ports {Q_CLK_GTREFCLK_PAD_N_IN[2]}]

################################# RefClk Location constraints #####################
set_property PACKAGE_PIN AH6 [get_ports {Q_CLK_GTREFCLK_PAD_P_IN[2]}]
set_property PACKAGE_PIN AH5 [get_ports {Q_CLK_GTREFCLK_PAD_N_IN[2]}]
set_property PACKAGE_PIN AD6 [get_ports {Q_CLK_GTREFCLK_PAD_P_IN[0]}]
set_property PACKAGE_PIN AD5 [get_ports {Q_CLK_GTREFCLK_PAD_N_IN[0]}]
set_property PACKAGE_PIN V6 [get_ports {Q_CLK_GTREFCLK_PAD_P_IN[1]}]
set_property PACKAGE_PIN V5 [get_ports {Q_CLK_GTREFCLK_PAD_N_IN[1]}]

###################RX location constraints 114
#set_property PACKAGE_PIN AP5 [get_ports {rxn_IN[0]}]
#set_property PACKAGE_PIN AP6 [get_ports {rxp_IN[0]}]
#set_property PACKAGE_PIN AM5 [get_ports {rxn_IN[1]}]
#set_property PACKAGE_PIN AM6 [get_ports {rxp_IN[1]}]
set_property PACKAGE_PIN AL3 [get_ports {rxn_IN[8]}]
set_property PACKAGE_PIN AL4 [get_ports {rxp_IN[8]}]
set_property PACKAGE_PIN AJ3 [get_ports {rxn_IN[9]}]
set_property PACKAGE_PIN AJ4 [get_ports {rxp_IN[9]}]


####################RX location constraints 115
set_property PACKAGE_PIN AG3 [get_ports {rxn_IN[0]}]
set_property PACKAGE_PIN AG4 [get_ports {rxp_IN[0]}]
set_property PACKAGE_PIN AF5 [get_ports {rxn_IN[1]}]
set_property PACKAGE_PIN AF6 [get_ports {rxp_IN[1]}]
set_property PACKAGE_PIN AE3 [get_ports {rxn_IN[2]}]
set_property PACKAGE_PIN AE4 [get_ports {rxp_IN[2]}]
set_property PACKAGE_PIN AC3 [get_ports {rxn_IN[3]}]
set_property PACKAGE_PIN AC4 [get_ports {rxp_IN[3]}]

####################RX location constraints 116
set_property PACKAGE_PIN AA3 [get_ports {rxn_IN[4]}]
set_property PACKAGE_PIN AA4 [get_ports {rxp_IN[4]}]
set_property PACKAGE_PIN W3 [get_ports {rxn_IN[5]}]
set_property PACKAGE_PIN W4 [get_ports {rxp_IN[5]}]
set_property PACKAGE_PIN U3 [get_ports {rxn_IN[6]}]
set_property PACKAGE_PIN U4 [get_ports {rxp_IN[6]}]
set_property PACKAGE_PIN R3 [get_ports {rxn_IN[7]}]
set_property PACKAGE_PIN R4 [get_ports {rxp_IN[7]}]
