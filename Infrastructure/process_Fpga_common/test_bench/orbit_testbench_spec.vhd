
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

ENTITY orbit_testbench IS
   PORT( 
      
      clk        : OUT    std_logic;
      bcn_ch0    : OUT    std_logic_vector( 6 downto 0);
      bcn_ch1    : OUT    std_logic_vector( 6 downto 0);
      bcn_ch2    : OUT    std_logic_vector( 6 downto 0);
      bcn_ch3    : OUT    std_logic_vector( 6 downto 0);
      reset      : OUT    std_logic;
      ref_orbit  : OUT    std_logic;
      start      : OUT    std_logic
   );

-- Declarations

END ENTITY orbit_testbench ;

--
ARCHITECTURE  spec OF orbit_testbench IS
constant clk_prd : time := 10 ns;  
signal iclk      : std_logic;  
signal cntr :unsigned(5 downto 0);
  
BEGIN
       process 
  
   Begin
	
       Reset      <= '1';  
       bcn_ch0    <= (others => '0'); 
       bcn_ch1    <= (others => '0'); 
       bcn_ch2    <= (others => '0'); 
       bcn_ch3    <= (others => '0'); 
       ref_orbit <= '0';
       start      <= '0';       
       cntr       <= (others => '0');       
       wait for 40 ns;
       Reset       <= '0';
       wait for  50 ns;
       start <= '1';
       wait for clk_prd ;
       --start      <= '0';
       wait for  50 ns;         
       
       bcn_ch0    <= (others => '1'); 
       ref_orbit <= '1';   
       wait for  clk_prd ;   
          
dataloop0: while cntr < 10 loop
          bcn_ch0    <= (others => '0');
          cntr  <= cntr+1;   
          wait for  clk_prd; 
          end loop;
          
          cntr       <= (others => '0');          
          bcn_ch1    <= (others => '1'); 
          wait for  clk_prd ;
          bcn_ch0    <= (others => '1');
          bcn_ch1    <= (others => '0');
                          
           
         
          
dataloop1:while cntr < 10 loop
          bcn_ch1    <= (others => '0'); 
          cntr  <= cntr+1;   
          wait for  clk_prd; 
          bcn_ch0    <= (others => '0');
          end loop;
          
        wait for  clk_prd; 
         cntr       <= (others => '0');
         bcn_ch1    <= (others => '1');    
          wait for  clk_prd ;   
         bcn_ch0    <= (others => '1');  
         
dataloop2: while cntr < 10 loop
                   bcn_ch1    <= (others => '0');
                   cntr  <= cntr+1;   
                   wait for  clk_prd; 
                   bcn_ch0    <= (others => '0');
                   end loop;
                   
                   cntr       <= (others => '0');          
                   bcn_ch1    <= (others => '1'); 
                   wait for  clk_prd ;         
                   bcn_ch0    <= (others => '1'); 
                   
                   
dataloop3:while cntr < 10 loop
                 bcn_ch1 <= (others => '0'); 
                 cntr  <= cntr+1;   
                  wait for  clk_prd;
                  bcn_ch0    <= (others => '0'); 
            end loop;
                             
                cntr <= (others => '0');                     
                wait for  clk_prd ;  
                bcn_ch1    <= (others => '1'); 
                wait for  clk_prd ;
                bcn_ch0    <= (others => '1');                  
dataloop4:while cntr < 10 loop
                bcn_ch1 <= (others => '0'); 
                cntr  <= cntr+1;   
                wait for  clk_prd;                 
                bcn_ch0 <= (others => '0'); 
           end loop;
           
               cntr       <= (others => '0');
                wait for  clk_prd ; 
               bcn_ch1    <= (others => '1');    
               wait for  clk_prd ;   
               bcn_ch0    <= (others => '1');                  
dataloop5: while cntr < 10 loop
              bcn_ch1 <= (others => '0'); 
               cntr  <= cntr+1;   
               wait for  clk_prd;
               bcn_ch0    <= (others => '0');  
           end loop;
                    
                cntr <= (others => '0');                     
             wait for  clk_prd ;  
             bcn_ch1    <= (others => '1'); 
             wait for  clk_prd ;
             --bcn_ch0    <= (others => '1');   
                            
dataloop6: while cntr < 10 loop
             bcn_ch1 <= (others => '0'); 
             cntr  <= cntr+1;   
             wait for  clk_prd;                 
             bcn_ch0 <= (others => '0'); 
        end loop;                                         
              
 
       
        wait for  50 us; 
       
     end process;
     
       
       
       
       
       
  ----------------------------------------------- 
  -- Clock genration of the test bench  
 ------------------------------------------------ 
  
   clock_gen: process
     begin
       iclk<= '1';
        wait for clk_prd/2;
        iclk <= '0';
        wait for clk_prd/2;
      end process clock_gen ;
      clk<= iclk;          
       
END spec ;

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  


