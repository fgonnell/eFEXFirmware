--! @file
--! @brief Core of the tau Algorithm
--! @details
--! The total latency of thius block is 10 clock cycles.
--! The parameter used to evaluate the conditions are used in clock cycle 6
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library work;
use work.DataTypes.all;

library infrastructure_lib;

--! @copydoc AlgoCore_eg.vhd
entity AlgoCore_tau is
  port (CLK200 : in std_logic;

        IN_ParJet           : in AlgoParameters(2 downto 0);
        IN_Energy_threshold : in DataWord;
        IN_Jet_threshold    : in DataWord;


        IN_Control : in  AlgoRegister;
        OUT_Status : out AlgoRegister;
        IN_Data    : in  TriggerTowers(8 downto 0);

        OUT_TOB : out TriggerObject_tau);
end AlgoCore_tau;

--! @copydoc AlgoCore_eg.vhd
architecture Behavioral of AlgoCore_tau is

  component Mult
    port (
      CLK : in  std_logic;
      A   : in  std_logic_vector(15 downto 0);
      B   : in  std_logic_vector(7 downto 0);
      P   : out std_logic_vector(23 downto 0)
      );
  end component;

  signal ParJet : AlgoParameters(2 downto 0);

  -- Little Seed Finder
  signal LSF_Data, LSF_DataUp, LSF_DataDown : DataWords(3 downto 0);
  signal LSF_Seed                           : std_logic_vector (1 downto 0);
  signal LSF_UpNotDown                      : std_logic;

  -- Tau Seed Finder
  signal SF_sum      : DataWords(8 downto 0);
  signal JetThr      : DataWord;
  signal EnergyThr   : DataWord;
  signal JetThr_d    : DataWord;
  signal EnergyThr_d : DataWord;

  signal SF_sum_OF : std_logic_vector(8 downto 0);
  signal SF_IsMax  : std_logic;

  signal IM_Energy_L1   : DataWords(9 downto 0);
  signal IM_Energy_L2   : DataWords(9 downto 0);
  signal IM_Energy_L0   : DataWords(5 downto 0);
  signal IM_Energy_L3   : DataWords(5 downto 0);
  signal IM_Energy_HAD  : DataWords(5 downto 0);
  signal IM_JetCoreData : DataWords(5 downto 0);
  signal IM_JetEnvData  : DataWords(11 downto 0);

  signal MA_EnergySum       : DataWord;
  signal MA_EnergyOverflow  : std_logic;
  signal MA_JetEnvSum       : DataWord;
  signal MA_JetEnvOverflow  : std_logic;
  signal MA_JetCoreOverflow : std_logic;
  signal MA_JetCoreSum      : DataWord;



  signal MU_JetEnvOverflows : std_logic_vector(IN_ParJet'high downto 0);
  signal MU_JetEnvMult      : DataWords(IN_ParJet'high downto 0);

--delayed
  signal Delayed           : std_logic_vector (3 downto 0);
  signal DL_Seed           : std_logic_vector (1 downto 0);
  signal DL_UpNotDown      : std_logic;
  signal DL_IsMax          : std_logic;
  signal DL_Overflows      : std_logic_vector(0 downto 0);
  signal DL_JetEnvOverflow : std_logic;

  --final
  signal TOBEnergy         : DataWord;
  signal TOBEnergyOverflow : std_logic;
  signal JetCondition      : std_logic_vector (1 downto 0);

begin
-- INPUT DATA mapping
--
-- +-------+-------+-------+
-- |   6   |   7   |   8   |
-- |       |0 1 2 3|       |
-- |       |       |       |
-- +-------+-------+-------+
-- |   3   |   4   |   5   |
-- |       |       |0 1 2 3|
-- |       |       |       |
-- +-------+-------+-------+
-- |       |0 1 2 3|       |
-- |       |       |       |
-- |   0   |   1   |   2   |
-- +-------+-------+-------+
--

  LSF_Data     <= IN_Data(4).Layer2;
  LSF_DataUp   <= IN_Data(7).Layer2;
  LSF_DataDown <= IN_Data(1).Layer2;


-- Parameters are connected to external IPBus registers
  ParJet    <= IN_ParJet;
  EnergyThr <= IN_Energy_threshold;
  JetThr    <= IN_Jet_threshold;

  LITTLE_SEED_FINDER : entity work.LittleSeedFinder  --latency 2
    port map (
      CLK           => CLK200,
      IN_Data       => LSF_Data,
      IN_DataUp     => LSF_DataUp,
      IN_DataDown   => LSF_DataDown,
      OUT_UpNotDown => LSF_UpNotDown,
      OUT_Seed      => LSF_Seed
      );

  INPUT_MULTIPLEXER : entity work.tauInputMultiplexer  --latency 2
    port map (
      CLK             => CLK200,
      IN_Seed         => LSF_Seed,
      IN_UpNotDown    => LSF_UpNotDown,
      IN_Towers       => IN_Data,
      OUT_Energy_L0   => IM_Energy_L0,
      OUT_Energy_L1   => IM_Energy_L1,
      OUT_Energy_L2   => IM_Energy_L2,
      OUT_Energy_L3   => IM_Energy_L3,
      OUT_Energy_HAD  => IM_Energy_HAD,
      OUT_JetCoreData => IM_JetCoreData,
      OUT_JetEnvData  => IM_JetEnvData);

-------------------------------------------------------------------------------
-- ADDERS
-------------------------------------------------------------------------------

-- Energy
  ENERGY_ADDER : entity work.MultiAdder
    generic map (
      stage => 6,
      delay => 1)
    port map (
      CLK                       => CLK200,
      IN_Words                  => (5 downto 0 => IM_Energy_L0, 11 downto 6 => IM_Energy_L3, 17 downto 12 => IM_Energy_HAD,
                   30 downto 21 => IM_Energy_L1, 40 downto 31 => IM_Energy_L2, others => ZERO_DATA_WORD),  --38 + 28
      OUT_Overflow              => MA_EnergyOverflow,
      OUT_Word                  => MA_EnergySum);

  SF_ADDER_FOR : for i in 0 to 8 generate
    SEED_FINDER_ADDER : entity work.MultiAdder
      generic map (
        stage => 4,
        delay => 0)
      port map (
        CLK          => CLK200,
        IN_Words     => (0 => IN_Data(i).Layer0(0), 4 downto 1 => IN_Data(i).Layer1, 8 downto 5 => IN_Data(i).Layer2, 9 => IN_Data(i).Layer3(0), 10 => IN_Data(i).Hadron(0), others => ZERO_DATA_WORD),  --36 + 28
        OUT_Overflow => SF_sum_OF(i),
        OUT_Word     => SF_sum(i));
  end generate;


  TAU_SEED_FINDER : entity work.TauSeedFinder  --latency 2
    port map (
      CLK                  => CLK200,
      IN_Surrounding       => (0 => SF_sum(0), 1 => SF_sum(1), 2 => SF_sum(2), 7 => SF_sum(3),
                         3 => SF_sum(5), 6 => SF_sum(6), 5 => SF_sum(7), 4 => SF_sum(8)),
      IN_Central           => SF_sum(4),
      OUT_IsMax            => SF_IsMax         --total latency 6
      );



-- Jet
  MULTI_ADDER_Jet_ENV : entity work.MultiAdder
    generic map (
      stage => 4,
      delay => 0)
    port map (
      CLK          => CLK200,
      IN_Words     => (IM_JetEnvData'range => IM_JetEnvData, others => ZERO_DATA_WORD),  -- 15+4
      OUT_Overflow => MA_JetEnvOverflow,
      OUT_Word     => MA_JetEnvSum);

  MULTI_ADDER_Jet_CORE : entity work.MultiAdder
    generic map (
      stage => 3,
      delay => 4)
    port map (
      CLK          => CLK200,
      IN_Words     => IM_JetCoreData & ZERO_DATA_WORD&ZERO_DATA_WORD,  -- 6+2
      OUT_Overflow => MA_JetCoreOverflow,
      OUT_Word     => MA_JetCoreSum);


-------------------------------------------------------------------------------
-- MULTIPLIERS
-------------------------------------------------------------------------------

-- Jet
  Jet_MULTIPLIER : entity work.MultiMultiplier  --delay 3
    generic map (parameters => 3)
    port map (
      CLK           => CLK200,
      IN_Word       => MA_JetEnvSum,
      IN_parameters => ParJet,
      OUT_Overflow  => MU_JetEnvOverflows,
      OUT_Words     => MU_JetEnvMult
      );

  Jet_threshold_delay : entity work.Delay
    generic map (
      delay => 3)
    port map (
      CLK      => CLK200,
      IN_Word  => JetThr,
      OUT_Word => JetThr_d);

  Energy_threshold_delay : entity work.Delay
    generic map (
      delay => 3)
    port map (
      CLK      => CLK200,
      IN_Word  => EnergyThr,
      OUT_Word => EnergyThr_d);

-------------------------------------------------------------------------------
-- SEED AND OVERFLOWS DELAY
-------------------------------------------------------------------------------
  TAU_SEED_DELAY : entity infrastructure_lib.GeneralDelay
    generic map (
      delay => 4,
      size  => 1)
    port map (
      clk      => CLK200,
      data_in  => (0 => SF_IsMax),
      data_out => Delayed(0 downto 0)
      );

  LITTLE_SEED_DELAY : entity infrastructure_lib.GeneralDelay
    generic map (
      delay => 9,
      size  => 3)
    port map (
      clk      => CLK200,
      data_in  => (0 => LSF_UpNotDown, 2 downto 1 => LSF_Seed),
      data_out => Delayed(3 downto 1)
      );

  DL_IsMax     <= Delayed(0);
  DL_UpNotDown <= Delayed(1);
  DL_Seed      <= Delayed(3 downto 2);


  OVERFLOW_DELAY : entity infrastructure_lib.GeneralDelay  --compensate for the Mult delay
    generic map (
      delay => 3,
      size  => 1)
    port map (
      CLK      => CLK200,
      data_in  => (0 => MA_JetEnvOverflow),
      data_out => DL_Overflows);

  DL_JetEnvOverflow <= DL_Overflows(0);


-----------------------------------------------------------------------------
-- TRIGGER-OBJECT CONDITIONS
-----------------------------------------------------------------------------
  CONDITIONS : process (CLK200)
  variable JetShifted : DataWord;
  begin
    if rising_edge(CLK200) then
      if MA_EnergySum < EnergyThr_d then
        TOBEnergy         <= (others => '0');
        TOBEnergyOverflow <= '0';
      else
        -- add material correction here
        TOBEnergy         <= MA_EnergySum;
        TOBEnergyOverflow <= MA_EnergyOverflow;
      end if;

      -- Jet veto condition
      JetShifted := BitLeftShift(MA_JetCoreSum,0); -- to be decided
      if MA_JetCoreOverflow = '0' and MA_EnergySum < JetThr_d then
        if DL_JetEnvOverflow = '0' then
          if (JetShifted > MU_JetEnvMult(2) and MU_JetEnvOverflows(2) = '0') then
            JetCondition <= "11";
          elsif (JetShifted > MU_JetEnvMult(1) and MU_JetEnvOverflows(1) = '0') then
            JetCondition <= "10";
          elsif (JetShifted > MU_JetEnvMult(0) and MU_JetEnvOverflows(0) = '0') then
            JetCondition <= "01";
          else
            JetCondition <= "00";
          end if;
        else
          -- Env sum overflow
          JetCondition <= "00";
        end if;
      else
        -- Core overflow or exceed energy threshold
        JetCondition <= "11";
      end if;



    end if;
  end process;


-- TOB building
  OUT_TOB.Core.Energy    <= to_TOBEnergy(TOBEnergy);
  OUT_TOB.Core.EnergyOF  <= TOBEnergyOverflow;
  OUT_TOB.Core.UpNotDown <= DL_UpNotDown;
  OUT_TOB.Core.Seed      <= DL_Seed;
  OUT_TOB.Core.IsMax     <= DL_IsMax;
  OUT_TOB.Core.Jet       <= JetCondition;
  OUT_TOB.Position.Eta   <= (others => '0');
  OUT_TOB.Position.Phi   <= (others => '0');

end Behavioral;
