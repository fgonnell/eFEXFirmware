library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;


entity gt_information_ctrl is
generic (addr_width :natural :=3);    
port(
        ipb_clk          : in std_logic;
        clk280           : in std_logic;
		reset            : in std_logic;
		ipb_rst          : in std_logic;
		ipb_in           : in ipb_wbus;
		ipb_out          : out ipb_rbus;
		error_counter    : in std_logic_vector(15 downto 0);
		bc_cntr          : in std_logic_vector(4 downto 0);
		bc_mux_cntr      : in std_logic_vector(4 downto 0);
		delay_cntr       : in std_logic_vector(3 downto 0);
		not_intable      : in std_logic;
		tx_pd            : in std_logic;
        rx_pd            : in std_logic;		
		rx_resetdone     : in std_logic;
        rx_fsm_resetdone : in std_logic;
        rx_byteisaligned : in std_logic;
        tx_resetdone     : in std_logic;
        tx_fsm_resetdone : in std_logic;
        tx_bufstatus     : in std_logic_vector (1 downto 0);		
		rx_realign       : in std_logic;
        rx_disperr       : in std_logic
       
  
  
   );
end gt_information_ctrl;

architecture Behavioral of gt_information_ctrl is

signal gt_rx_nointable_cntr, gt_rx_disperr_cntr, gt_byterealign_cntr :std_logic_vector(15 downto 0);
signal status : std_logic_vector (31 downto 0);

signal sel:integer;
signal ack: std_logic;

begin
status <= x"00000" & "00" & rx_pd & tx_pd &  tx_bufstatus & tx_fsm_resetdone & tx_resetdone & '0' & rx_byteisaligned & rx_fsm_resetdone & rx_resetdone ;

sel <= to_integer(unsigned(ipb_in.ipb_addr(addr_width-1 downto 0))) when addr_width > 0 else 0;

 process(ipb_clk,ipb_rst)   -- reading error counters of the mgt quad
 
     begin
      
      if ipb_clk' event and ipb_clk='1' then
        if ipb_rst ='1' then
          ipb_out.ipb_rdata <= (others =>'0');
      elsif ipb_in.ipb_strobe= '1' and ipb_in.ipb_write ='1' then
             ipb_out.ipb_rdata <= (others =>'0');
             end if;
             
      if   sel = 0 then
             ipb_out.ipb_rdata <=  status;            -- check the status of the pll and reset conditions of the quad
             ack <= ipb_in.ipb_strobe and not ack;
             end if;
             
      if   sel = 1 then
             ipb_out.ipb_rdata <=  X"0000" & gt_rx_nointable_cntr; -- read the notintable error counter
             ack <= ipb_in.ipb_strobe and not ack;
             end if;           
             
       if  sel = 2 then
              ipb_out.ipb_rdata <=  X"0000" & gt_rx_disperr_cntr ; -- read disperr error counter
               ack <= ipb_in.ipb_strobe and not ack;
              end if;
       
       if  sel = 3 then
             ipb_out.ipb_rdata <=  X"0000" & gt_byterealign_cntr ;  -- read byterealign error counter 
              ack <= ipb_in.ipb_strobe and not ack;
              end if; 
                
       if  sel = 4 then
             ipb_out.ipb_rdata <=  x"0000000" & delay_cntr ;  -- read delay  counter  of the first stage
             ack <= ipb_in.ipb_strobe and not ack;
           end if;  
        
       if sel  = 5  then
           ipb_out.ipb_rdata <=  x"000000"  & "000"& bc_cntr ;  -- read bc  counter           
            ack <= ipb_in.ipb_strobe and not ack  ;      
          end if;                             
       if sel  = 6  then
            ipb_out.ipb_rdata <=  x"000000" & "000"& bc_mux_cntr  ;  -- read bc  after mux counter           
             ack <= ipb_in.ipb_strobe and not ack  ;      
           end if;             
       
       
       if sel = 7 then
           ipb_out.ipb_rdata <=  X"0000" & error_counter; -- read error counter of tx phase
            ack <= ipb_in.ipb_strobe and not ack  ;      
         end if;              
         end if; 
           
        end process;
       
       ipb_out.ipb_ack <= ack;
       ipb_out.ipb_err <= '0';




cntr_0:entity work.counter 
        Port map(  
             clk    => clk280, 
             enable2 => rx_resetdone,
             enable1 =>not_intable, 
             count  => gt_rx_nointable_cntr,
             reset  => reset 
  
          );
          
cntr_1:entity work.counter 
        Port map(  
             clk    => clk280, 
             enable2 => rx_resetdone,
             enable1 =>rx_disperr, 
             count  => gt_rx_disperr_cntr,
             reset  => reset 
            
              );          

cntr_2:entity work.counter 
        Port map(  
             clk     => clk280, 
             enable2 => rx_resetdone,
             enable1 => rx_realign, 
             count   => gt_byterealign_cntr,
             reset   => reset 
  
          );
end Behavioral;
