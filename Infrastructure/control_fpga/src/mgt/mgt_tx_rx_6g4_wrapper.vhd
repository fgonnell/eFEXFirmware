
--  this wrapper is for MGT with RX and TX at 11.2Gbps, Ref clk = 280MHz, TX Buffer enabled


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
Library work;
Library xil_defaultlib;

--***********************************Entity Declaration************************

--  this wrapper is for MGT with RX and TX at 11.2Gbps, Ref clk = 280MHz,

entity mgt_tx_rx_6g4_wrapper is
generic
(
    EXAMPLE_SIM_GTRESET_SPEEDUP             : string    := "TRUE";     -- simulation setting for GT SecureIP model
    STABLE_CLOCK_PERIOD                     : integer   := 16  

);
port
(  -- refclk160                               : in   std_logic;                  
    SOFT_RESET_TX_IN                        : in   std_logic;
    SOFT_RESET_RX_IN                        : in   std_logic;
    RXN_IN                                  : in   std_logic_vector(3 downto 0);
    RXP_IN                                  : in   std_logic_vector(3 downto 0);
    TXN_OUT                                 : out  std_logic_vector(3 downto 0);
    TXP_OUT                                 : out  std_logic_vector(3 downto 0);
    Q0_CLK0_GTREFCLK_PAD_N_IN               : in   std_logic;
    Q0_CLK0_GTREFCLK_PAD_P_IN               : in   std_logic;
    GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_RX_FSM_RESET_DONE_OUT               : out  std_logic;    
    GT3_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT3_RX_FSM_RESET_DONE_OUT               : out  std_logic;    
    GT0_TXUSRCLK_OUT                        : out  std_logic;
    GT0_RXUSRCLK_OUT                        : out  std_logic; 
    GT1_TXUSRCLK_OUT                        : out  std_logic;
    GT1_RXUSRCLK_OUT                        : out  std_logic; 
    GT2_TXUSRCLK_OUT                        : out  std_logic;
    GT2_RXUSRCLK_OUT                        : out  std_logic; 
    GT3_TXUSRCLK_OUT                        : out  std_logic;
    GT3_RXUSRCLK_OUT                        : out  std_logic;
    gt0_cpllfbclklost_out                   : out  std_logic;  
    gt0_cplllock_out                        : out  std_logic;  
    gt1_cpllfbclklost_out                   : out  std_logic;
    gt1_cplllock_out                        : out  std_logic;
    gt2_cpllfbclklost_out                   : out  std_logic;
    gt2_cplllock_out                        : out  std_logic;
    gt3_cpllfbclklost_out                   : out  std_logic;
    gt3_cplllock_out                        : out  std_logic;  
    
   -- _________________________________________________________________________
    --GT0  (X0Y0)
    --____________________________CHANNEL PORTS________________________________
    ------------------------------- Loopback Ports -----------------------------
    gt0_loopback_in                         : in   std_logic_vector(2 downto 0);
    ------------------------------ Power-Down Ports ---------------------------- 
    gt0_rxpd_in                             : in   std_logic_vector(1 downto 0); 
    gt0_txpd_in                             : in   std_logic_vector(1 downto 0);     
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt0_rxdata_out                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    gt0_rxdisperr_out                       : out  std_logic_vector(3 downto 0);
    gt0_rxnotintable_out                    : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    gt0_rxbyteisaligned_out                 : out  std_logic;
    gt0_rxbyterealign_out                   : out  std_logic;
    gt0_rxcommadet_out                      : out  std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    gt0_rxchariscomma_out                   : out  std_logic_vector(3 downto 0);
    gt0_rxcharisk_out                       : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt0_rxresetdone_out                     : out  std_logic;

    ------------------ Transmit Ports - TX Data Path interface -----------------
    gt0_txdata_in                           : in   std_logic_vector(31 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    gt0_txresetdone_out                     : out  std_logic;
    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
    gt0_txcharisk_in                        : in   std_logic_vector(3 downto 0);
    ---------------------- Transmit Ports - TX Buffer Ports --------------------
    gt0_txbufstatus_out                     : out  std_logic_vector(1 downto 0);

    --GT1  (X0Y1)
    --____________________________CHANNEL PORTS________________________________
    ------------------------------- Loopback Ports -----------------------------
    gt1_loopback_in                         : in   std_logic_vector(2 downto 0);
    ------------------------------ Power-Down Ports ---------------------------- 
    gt1_rxpd_in                             : in   std_logic_vector(1 downto 0); 
    gt1_txpd_in                             : in   std_logic_vector(1 downto 0);       
    --------------------- RX Initialization and Reset Ports --------------------
    gt1_rxdata_out                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    gt1_rxdisperr_out                       : out  std_logic_vector(3 downto 0);
    gt1_rxnotintable_out                    : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    gt1_rxbyteisaligned_out                 : out  std_logic;
    gt1_rxbyterealign_out                   : out  std_logic;
    gt1_rxcommadet_out                      : out  std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    gt1_rxchariscomma_out                   : out  std_logic_vector(3 downto 0);
    gt1_rxcharisk_out                       : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt1_rxresetdone_out                     : out  std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    gt1_txdata_in                           : in   std_logic_vector(31 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    gt1_txresetdone_out                     : out  std_logic;
    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
    gt1_txcharisk_in                        : in   std_logic_vector(3 downto 0);
    ---------------------- Transmit Ports - TX Buffer Ports --------------------
    gt1_txbufstatus_out                     : out  std_logic_vector(1 downto 0);

    --GT2  (X0Y2)
    --____________________________CHANNEL PORTS________________________________
    ------------------------------- Loopback Ports -----------------------------
    gt2_loopback_in                         : in   std_logic_vector(2 downto 0);
    ------------------------------ Power-Down Ports ---------------------------- 
    gt2_rxpd_in                             : in   std_logic_vector(1 downto 0); 
    gt2_txpd_in                             : in   std_logic_vector(1 downto 0);   
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt2_rxdata_out                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    gt2_rxdisperr_out                       : out  std_logic_vector(3 downto 0);
    gt2_rxnotintable_out                    : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    gt2_rxbyteisaligned_out                 : out  std_logic;
    gt2_rxbyterealign_out                   : out  std_logic;
    gt2_rxcommadet_out                      : out  std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    gt2_rxchariscomma_out                   : out  std_logic_vector(3 downto 0);
    gt2_rxcharisk_out                       : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt2_rxresetdone_out                     : out  std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    gt2_txdata_in                           : in   std_logic_vector(31 downto 0);
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    gt2_txresetdone_out                     : out  std_logic;
    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
    gt2_txcharisk_in                        : in   std_logic_vector(3 downto 0);
    ---------------------- Transmit Ports - TX Buffer Ports --------------------
    gt2_txbufstatus_out                     : out  std_logic_vector(1 downto 0);

    --GT3  (X0Y3)
    --____________________________CHANNEL PORTS________________________________
    ------------------------------- Loopback Ports -----------------------------
    gt3_loopback_in                         : in   std_logic_vector(2 downto 0);
    ------------------------------ Power-Down Ports ---------------------------- 
    gt3_rxpd_in                             : in   std_logic_vector(1 downto 0); 
    gt3_txpd_in                             : in   std_logic_vector(1 downto 0);     
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt3_rxdata_out                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    gt3_rxdisperr_out                       : out  std_logic_vector(3 downto 0);
    gt3_rxnotintable_out                    : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    gt3_rxbyteisaligned_out                 : out  std_logic;
    gt3_rxbyterealign_out                   : out  std_logic;
    gt3_rxcommadet_out                      : out  std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    gt3_rxchariscomma_out                   : out  std_logic_vector(3 downto 0);
    gt3_rxcharisk_out                       : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt3_rxresetdone_out                     : out  std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    gt3_txdata_in                           : in   std_logic_vector(31 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    gt3_txresetdone_out                     : out  std_logic;
    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
    gt3_txcharisk_in                        : in   std_logic_vector(3 downto 0);
    ---------------------- Transmit Ports - TX Buffer Ports --------------------
    gt3_txbufstatus_out                     : out  std_logic_vector(1 downto 0);

    --____________________________COMMON PORTS________________________________
    --GT0_QPLLLOCK_OUT        : out std_logic;
    --GT0_QPLLREFCLKLOST_OUT  : out std_logic;
    sysclk_in               : in std_logic

    );
 

end mgt_tx_rx_6g4_wrapper;


architecture RTL of mgt_tx_rx_6g4_wrapper is


    attribute DowngradeIPIdentifiedWarnings: string;
    attribute DowngradeIPIdentifiedWarnings of RTL : architecture is "yes";

    attribute CORE_GENERATION_INFO : string;
    attribute CORE_GENERATION_INFO of RTL : architecture is "MGT_TX_RX_6G4,gtwizard_v3_6_5,{protocol_file=Start_from_scratch}";

--**************************Component Declarations*****************************



    signal  tied_to_ground_i                : std_logic;
    signal  tied_to_ground_vec_i            : std_logic_vector(63 downto 0);
    signal  tied_to_vcc_i                   : std_logic;
    signal  tied_to_vcc_vec_i               : std_logic_vector(7 downto 0);


    signal  gt0_drpaddr_i                   : std_logic_vector(8 downto 0);
    signal  gt0_drpdi_i                     : std_logic_vector(15 downto 0);
    signal  gt0_drpdo_i                     : std_logic_vector(15 downto 0);
    signal  gt0_drpen_i                     : std_logic;
    signal  gt0_drprdy_i                    : std_logic;
    signal  gt0_drpwe_i                     : std_logic;
    signal  gt1_drpaddr_i                   : std_logic_vector(8 downto 0);
    signal  gt1_drpdi_i                     : std_logic_vector(15 downto 0);
    signal  gt1_drpdo_i                     : std_logic_vector(15 downto 0);
    signal  gt1_drpen_i                     : std_logic;
    signal  gt1_drprdy_i                    : std_logic;
    signal  gt1_drpwe_i                     : std_logic;
    signal  gt2_drpaddr_i                   : std_logic_vector(8 downto 0);  
    signal  gt2_drpdi_i                     : std_logic_vector(15 downto 0); 
    signal  gt2_drpdo_i                     : std_logic_vector(15 downto 0); 
    signal  gt2_drpen_i                     : std_logic;                     
    signal  gt2_drprdy_i                    : std_logic;                     
    signal  gt2_drpwe_i                     : std_logic;                     
    signal  gt3_drpaddr_i                   : std_logic_vector(8 downto 0);  
    signal  gt3_drpdi_i                     : std_logic_vector(15 downto 0); 
    signal  gt3_drpdo_i                     : std_logic_vector(15 downto 0); 
    signal  gt3_drpen_i                     : std_logic;                     
    signal  gt3_drprdy_i                    : std_logic;                     
    signal  gt3_drpwe_i                     : std_logic;                     





--**************************** Main Body of Code *******************************

begin

    --  Static signal Assigments
    tied_to_ground_i                             <= '0';
    tied_to_ground_vec_i                         <= x"0000000000000000";
    tied_to_vcc_i                                <= '1';
    tied_to_vcc_vec_i                            <= "11111111";

 
    ----------------------------- The GT Wrapper -----------------------------
    
    -- Use the instantiation template in the example directory to add the GT wrapper to your design.
    -- In this example, the wrapper is wired up for basic operation with a frame generator and frame 
    -- checker. The GTs will reset, then attempt to align and transmit data. If channel bonding is 
    -- enabled, bonding should occur after alignment.

    
    min_latency_1_quad_rx_tx_support_i : entity work.MGT_TX_RX_6G4_support
    generic map
    (
        EXAMPLE_SIM_GTRESET_SPEEDUP     =>      EXAMPLE_SIM_GTRESET_SPEEDUP,
        STABLE_CLOCK_PERIOD             =>      STABLE_CLOCK_PERIOD
    )
    port map
   (   --refclk160                      =>       refclk160 ,
        SOFT_RESET_TX_IN                =>      SOFT_RESET_TX_IN,
        SOFT_RESET_RX_IN                =>      SOFT_RESET_RX_IN,
        DONT_RESET_ON_DATA_ERROR_IN     =>      tied_to_ground_i,
        Q1_CLK0_GTREFCLK_PAD_N_IN       =>      Q0_CLK0_GTREFCLK_PAD_N_IN,
        Q1_CLK0_GTREFCLK_PAD_P_IN       =>      Q0_CLK0_GTREFCLK_PAD_P_IN,

        GT0_TX_FSM_RESET_DONE_OUT       =>      GT0_TX_FSM_RESET_DONE_OUT,
        GT0_RX_FSM_RESET_DONE_OUT       =>      GT0_RX_FSM_RESET_DONE_OUT,
        GT0_DATA_VALID_IN               =>      '1'        ,
        GT1_TX_FSM_RESET_DONE_OUT       =>      GT1_TX_FSM_RESET_DONE_OUT,
        GT1_RX_FSM_RESET_DONE_OUT       =>      GT1_RX_FSM_RESET_DONE_OUT,
        GT1_DATA_VALID_IN               =>      '1'       ,
        GT2_TX_FSM_RESET_DONE_OUT       =>      GT2_TX_FSM_RESET_DONE_OUT,
        GT2_RX_FSM_RESET_DONE_OUT       =>      GT2_RX_FSM_RESET_DONE_OUT,
        GT2_DATA_VALID_IN               =>      '1'       ,
        GT3_TX_FSM_RESET_DONE_OUT       =>      GT3_TX_FSM_RESET_DONE_OUT,
        GT3_RX_FSM_RESET_DONE_OUT       =>      GT3_RX_FSM_RESET_DONE_OUT,
        GT3_DATA_VALID_IN               =>      '1'        , 
        GT0_TXUSRCLK_OUT                =>      GT0_TXUSRCLK_OUT  ,
        GT0_TXUSRCLK2_OUT               =>      open ,
        GT0_RXUSRCLK_OUT                =>      GT0_RXUSRCLK_OUT  ,
        GT0_RXUSRCLK2_OUT               =>      open ,                                               
        GT1_TXUSRCLK_OUT                =>      GT1_TXUSRCLK_OUT  ,
        GT1_TXUSRCLK2_OUT               =>      open ,
        GT1_RXUSRCLK_OUT                =>      GT1_RXUSRCLK_OUT  ,
        GT1_RXUSRCLK2_OUT               =>      open ,                                               
        GT2_TXUSRCLK_OUT                =>      GT2_TXUSRCLK_OUT  ,
        GT2_TXUSRCLK2_OUT               =>      open ,
        GT2_RXUSRCLK_OUT                =>      GT2_RXUSRCLK_OUT  ,
        GT2_RXUSRCLK2_OUT               =>      open ,
        GT3_TXUSRCLK_OUT                =>      GT3_TXUSRCLK_OUT  ,
        GT3_TXUSRCLK2_OUT               =>      open ,
        GT3_RXUSRCLK_OUT                =>      GT3_RXUSRCLK_OUT  ,
        GT3_RXUSRCLK2_OUT               =>      open ,


        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT0  (X0Y0)

         gt0_cpllfbclklost_out           =>      gt0_cpllfbclklost_out,
         gt0_cplllock_out                =>      gt0_cplllock_out,     
         gt0_cpllreset_in                =>      tied_to_ground_i,
         ---------------------------- Channel - DRP Ports  --------------------------
         gt0_drpaddr_in                  =>      gt0_drpaddr_i,
         gt0_drpdi_in                    =>      gt0_drpdi_i,
         gt0_drpdo_out                   =>      gt0_drpdo_i,
         gt0_drpen_in                    =>      gt0_drpen_i,
         gt0_drprdy_out                  =>      gt0_drprdy_i,
         gt0_drpwe_in                    =>      gt0_drpwe_i,
        ------------------------------- Loopback Ports -----------------------------
        gt0_loopback_in                 =>      gt0_loopback_in,
        ------------------------------ Power-Down Ports ----------------------------
        gt0_rxpd_in                     =>      gt0_rxpd_in,
        gt0_txpd_in                     =>      gt0_txpd_in,        
        --------------------- RX Initialization and Reset Ports --------------------
        gt0_eyescanreset_in             =>      tied_to_ground_i,
        gt0_rxuserrdy_in                =>      tied_to_ground_i,
        -------------------------- RX Margin Analysis Ports ------------------------
        gt0_eyescandataerror_out        =>      open,
        gt0_eyescantrigger_in           =>      tied_to_ground_i,
        ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt0_dmonitorout_out             =>      open,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt0_rxdata_out                  =>      gt0_rxdata_out,
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt0_rxdisperr_out               =>      gt0_rxdisperr_out,
        gt0_rxnotintable_out            =>      gt0_rxnotintable_out,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt0_gthrxn_in                   =>      RXN_IN(0),
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt0_rxphmonitor_out             =>      open,
        gt0_rxphslipmonitor_out         =>      open,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt0_rxbyteisaligned_out         =>      gt0_rxbyteisaligned_out,
        gt0_rxbyterealign_out           =>      gt0_rxbyterealign_out,
        gt0_rxcommadet_out              =>      gt0_rxcommadet_out,
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt0_rxmonitorout_out            =>      open,
        gt0_rxmonitorsel_in             =>      "00",
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt0_rxoutclkfabric_out          =>      open,
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt0_gtrxreset_in                =>      tied_to_ground_i,
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt0_rxchariscomma_out           =>      gt0_rxchariscomma_out,
        gt0_rxcharisk_out               =>      gt0_rxcharisk_out,
        ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt0_gthrxp_in                   =>      RXP_IN(0),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt0_rxresetdone_out             =>      gt0_rxresetdone_out,
        --------------------- TX Initialization and Reset Ports --------------------
        gt0_gttxreset_in                =>      tied_to_ground_i,
        gt0_txuserrdy_in                =>      tied_to_ground_i,
        ---------------------- Transmit Ports - TX Buffer Ports --------------------
        gt0_txbufstatus_out             =>      gt0_txbufstatus_out,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        gt0_txdata_in                   =>      gt0_txdata_in,
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt0_gthtxn_out                  =>      TXN_OUT(0),
        gt0_gthtxp_out                  =>      TXP_OUT(0),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt0_txoutclkfabric_out          =>      open,
        gt0_txoutclkpcs_out             =>      open,
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt0_txresetdone_out             =>      gt0_txresetdone_out,
        ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
        gt0_txcharisk_in                =>      gt0_txcharisk_in,

       --
          ----------------------------------- CPLL Ports -------------------------------
         gt1_cpllfbclklost_out           =>      gt1_cpllfbclklost_out,
         gt1_cplllock_out                =>      gt1_cplllock_out,
         gt1_cpllreset_in                =>      tied_to_ground_i,
         ---------------------------- Channel - DRP Ports  --------------------------
         gt1_drpaddr_in                  =>      gt1_drpaddr_i,
         gt1_drpdi_in                    =>      gt1_drpdi_i,
         gt1_drpdo_out                   =>      gt1_drpdo_i,
         gt1_drpen_in                    =>      gt1_drpen_i,
         gt1_drprdy_out                  =>      gt1_drprdy_i,
         gt1_drpwe_in                    =>      gt1_drpwe_i,        
        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT1  (X0Y1)

        ------------------------------- Loopback Ports -----------------------------
        gt1_loopback_in                 =>      gt1_loopback_in,
        ------------------------------ Power-Down Ports ----------------------------
        gt1_rxpd_in                     =>      gt1_rxpd_in,
        gt1_txpd_in                     =>      gt1_txpd_in,    
        --------------------- RX Initialization and Reset Ports --------------------
        gt1_eyescanreset_in             =>      tied_to_ground_i,
        gt1_rxuserrdy_in                =>      tied_to_ground_i,
        -------------------------- RX Margin Analysis Ports ------------------------
        gt1_eyescandataerror_out        =>      open,
        gt1_eyescantrigger_in           =>      tied_to_ground_i,
        ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt1_dmonitorout_out             =>      open,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt1_rxdata_out                  =>      gt1_rxdata_out,
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt1_rxdisperr_out               =>      gt1_rxdisperr_out,
        gt1_rxnotintable_out            =>      gt1_rxnotintable_out,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt1_gthrxn_in                   =>      RXN_IN(1),
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt1_rxphmonitor_out             =>      open,
        gt1_rxphslipmonitor_out         =>      open,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt1_rxbyteisaligned_out         =>      gt1_rxbyteisaligned_out,
        gt1_rxbyterealign_out           =>      gt1_rxbyterealign_out,
        gt1_rxcommadet_out              =>      gt1_rxcommadet_out,
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt1_rxmonitorout_out            =>      open,
        gt1_rxmonitorsel_in             =>      "00",
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt1_rxoutclkfabric_out          =>      open,
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt1_gtrxreset_in                =>      tied_to_ground_i,
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt1_rxchariscomma_out           =>      gt1_rxchariscomma_out,
        gt1_rxcharisk_out               =>      gt1_rxcharisk_out    ,
        ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt1_gthrxp_in                   =>      RXP_IN(1),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt1_rxresetdone_out             =>      gt1_rxresetdone_out,
        --------------------- TX Initialization and Reset Ports --------------------
        gt1_gttxreset_in                =>      tied_to_ground_i,
        gt1_txuserrdy_in                =>      tied_to_ground_i,
        ---------------------- Transmit Ports - TX Buffer Ports --------------------
        gt1_txbufstatus_out             =>      gt1_txbufstatus_out,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        gt1_txdata_in                   =>      gt1_txdata_in,
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt1_gthtxn_out                  =>      TXN_OUT(1),
        gt1_gthtxp_out                  =>      TXP_OUT(1),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt1_txoutclkfabric_out          =>      open,
        gt1_txoutclkpcs_out             =>      open,
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt1_txresetdone_out             =>      gt1_txresetdone_out,
        ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
        gt1_txcharisk_in                =>      gt1_txcharisk_in,
                                                                                             
      --_____________________________________________________________________
        --_____________________________________________________________________
        --GT2  (X0Y2)
        
        gt2_cpllfbclklost_out           =>      gt2_cpllfbclklost_out,  
        gt2_cplllock_out                =>      gt2_cplllock_out,       
        gt2_cpllreset_in                =>      tied_to_ground_i,     
        --------------------------- Channel - DRP Ports  ------------
        gt2_drpaddr_in                  =>      gt2_drpaddr_i,        
        gt2_drpdi_in                    =>      gt2_drpdi_i,          
        gt2_drpdo_out                   =>      gt2_drpdo_i,          
        gt2_drpen_in                    =>      gt2_drpen_i,          
        gt2_drprdy_out                  =>      gt2_drprdy_i,         
        gt2_drpwe_in                    =>      gt2_drpwe_i,  
        
       ------------------------------- Loopback Ports -----------------------------
        gt2_loopback_in                 =>      gt2_loopback_in,
        ------------------------------ Power-Down Ports ----------------------------
        gt2_rxpd_in                     =>      gt2_rxpd_in,
        gt2_txpd_in                     =>      gt2_txpd_in,      
        -------------------- RX Initialization and Reset Ports --------------------
        gt2_eyescanreset_in             =>      tied_to_ground_i,
        gt2_rxuserrdy_in                =>      tied_to_ground_i,
        -------------------------- RX Margin Analysis Ports ------------------------
        gt2_eyescandataerror_out        =>      open,
        gt2_eyescantrigger_in           =>      tied_to_ground_i,
        ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt2_dmonitorout_out             =>      open,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt2_rxdata_out                  =>      gt2_rxdata_out,
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt2_rxdisperr_out               =>      gt2_rxdisperr_out,
        gt2_rxnotintable_out            =>      gt2_rxnotintable_out,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt2_gthrxn_in                   =>      RXN_IN(2),
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt2_rxphmonitor_out             =>      open,
        gt2_rxphslipmonitor_out         =>      open,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt2_rxbyteisaligned_out         =>      gt2_rxbyteisaligned_out,
        gt2_rxbyterealign_out           =>      gt2_rxbyterealign_out  ,
        gt2_rxcommadet_out              =>      gt2_rxcommadet_out     ,
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt2_rxmonitorout_out            =>      open,
        gt2_rxmonitorsel_in             =>      "00",
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt2_rxoutclkfabric_out          =>      open,
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt2_gtrxreset_in                =>      tied_to_ground_i,
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt2_rxchariscomma_out           =>      gt2_rxchariscomma_out,
        gt2_rxcharisk_out               =>      gt2_rxcharisk_out    ,
        ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt2_gthrxp_in                   =>      RXP_IN(2),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt2_rxresetdone_out             =>      gt2_rxresetdone_out,
        --------------------- TX Initialization and Reset Ports --------------------
        gt2_gttxreset_in                =>      tied_to_ground_i,
        gt2_txuserrdy_in                =>      tied_to_ground_i,
        ---------------------- Transmit Ports - TX Buffer Ports --------------------
        gt2_txbufstatus_out             =>      gt2_txbufstatus_out,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        gt2_txdata_in                   =>      gt2_txdata_in,
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt2_gthtxn_out                  =>      TXN_OUT(2),
        gt2_gthtxp_out                  =>      TXP_OUT(2),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt2_txoutclkfabric_out          =>      open,
        gt2_txoutclkpcs_out             =>      open,
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt2_txresetdone_out             =>      gt2_txresetdone_out,
        ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
        gt2_txcharisk_in                =>      gt2_txcharisk_in,

        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT3  (X0Y3)
        
        gt3_cpllfbclklost_out           =>      gt3_cpllfbclklost_out,  
        gt3_cplllock_out                =>      gt3_cplllock_out,       
        gt3_cpllreset_in                =>      tied_to_ground_i,     
        ---------------------------- Channel - DRP Ports  ------------
        gt3_drpaddr_in                  =>      gt3_drpaddr_i,        
        gt3_drpdi_in                    =>      gt3_drpdi_i,          
        gt3_drpdo_out                   =>      gt3_drpdo_i,          
        gt3_drpen_in                    =>      gt3_drpen_i,          
        gt3_drprdy_out                  =>      gt3_drprdy_i,         
        gt3_drpwe_in                    =>      gt3_drpwe_i,          
        ------------------------------- Loopback Ports -----------------------------
        gt3_loopback_in                 =>      gt3_loopback_in,
        ------------------------------ Power-Down Ports ----------------------------
        gt3_rxpd_in                     =>      gt3_rxpd_in,
        gt3_txpd_in                     =>      gt3_txpd_in,   
        
        --------------------- RX Initialization and Reset Ports --------------------
        gt3_eyescanreset_in             =>      tied_to_ground_i,
        gt3_rxuserrdy_in                =>      tied_to_ground_i,
        -------------------------- RX Margin Analysis Ports ------------------------
        gt3_eyescandataerror_out        =>      open,
        gt3_eyescantrigger_in           =>      tied_to_ground_i,
        ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt3_dmonitorout_out             =>      open,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt3_rxdata_out                  =>      gt3_rxdata_out,
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt3_rxdisperr_out               =>      gt3_rxdisperr_out   ,
        gt3_rxnotintable_out            =>      gt3_rxnotintable_out,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt3_gthrxn_in                   =>      RXN_IN(3),
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        gt3_rxphmonitor_out             =>      open,
        gt3_rxphslipmonitor_out         =>      open,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt3_rxbyteisaligned_out         =>      gt3_rxbyteisaligned_out,
        gt3_rxbyterealign_out           =>      gt3_rxbyterealign_out  ,
        gt3_rxcommadet_out              =>      gt3_rxcommadet_out     ,
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt3_rxmonitorout_out            =>      open,
        gt3_rxmonitorsel_in             =>      "00",
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt3_rxoutclkfabric_out          =>      open,
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt3_gtrxreset_in                =>      tied_to_ground_i,
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt3_rxchariscomma_out           =>      gt3_rxchariscomma_out,
        gt3_rxcharisk_out               =>      gt3_rxcharisk_out    ,
        ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt3_gthrxp_in                   =>      RXP_IN(3),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt3_rxresetdone_out             =>      gt3_rxresetdone_out,
        --------------------- TX Initialization and Reset Ports --------------------
        gt3_gttxreset_in                =>      tied_to_ground_i,
        gt3_txuserrdy_in                =>      tied_to_ground_i,
        ---------------------- Transmit Ports - TX Buffer Ports --------------------
        gt3_txbufstatus_out             =>      gt3_txbufstatus_out,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        gt3_txdata_in                   =>      gt3_txdata_in,
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt3_gthtxn_out                  =>      TXN_OUT(3),
        gt3_gthtxp_out                  =>      TXP_OUT(3),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt3_txoutclkfabric_out          =>      open,
        gt3_txoutclkpcs_out             =>      open,
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt3_txresetdone_out             =>      gt3_txresetdone_out,
        ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
        gt3_txcharisk_in                =>      gt3_txcharisk_in,

    --____________________________COMMON PORTS________________________________
     
     GT0_QPLLOUTCLK_OUT         => open,
     GT0_QPLLOUTREFCLK_OUT      => open,
     sysclk_in                  => sysclk_in
    );

    gt0_drpaddr_i <= (others => '0');
    gt0_drpdi_i <= (others => '0');
    gt0_drpen_i <= '0';
    gt0_drpwe_i <= '0';
    gt1_drpaddr_i <= (others => '0');
    gt1_drpdi_i <= (others => '0');
    gt1_drpen_i <= '0';
    gt1_drpwe_i <= '0';
    gt2_drpaddr_i <= (others => '0');
    gt2_drpdi_i <= (others => '0');
    gt2_drpen_i <= '0';
    gt2_drpwe_i <= '0';
    gt3_drpaddr_i <= (others => '0');
    gt3_drpdi_i <= (others => '0');
    gt3_drpen_i <= '0';
    gt3_drpwe_i <= '0';

end RTL;

