--! @file
--! @brief Fast FIFO, without control signal but with 1 clock tick i/0 delay
--! @details 
--! LastWrite (read) should be set to 1 while writing(reading) the last word
--! It will reset the writing (reading) address at the next clock cycle
--! @author Francesco Gonnella

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library algolib;
use algolib.DataTypes.all;
use algolib.AlgoDataTypes.all;

-------------------------------------------------------------------------------
--! @copydoc FastFifo.vhd
entity FastFifo is

  generic (
    FASTFIFO_DATA_DEPTH    : integer := 8;
    FASTFIFO_ADDRESS_DEPTH : integer := 3
    );

  port (
    clk          : in  std_logic;
    IN_Reset     : in  std_logic;
    IN_Data      : in  AlgoTriggerObject;
    IN_Write     : in  std_logic;
    IN_Ack       : in  std_logic;
    OUT_Empty    : out std_logic;
    OUT_Data     : out AlgoTriggerObject
    );

end entity FastFifo;

-------------------------------------------------------------------------------
--! @copydoc FastFifo.vhd
architecture str of FastFifo is

  -----------------------------------------------------------------------------
  -- Internal signal declarations
  -----------------------------------------------------------------------------
  signal mem          : AlgoTriggerObjects(FASTFIFO_DATA_DEPTH-1 downto 0)  := (others => ZERO_ALGO_TRIGGER_OBJECT);
  signal ReadAddress  : std_logic_vector(FASTFIFO_ADDRESS_DEPTH-1 downto 0) := (others => '0');
  signal WriteAddress : std_logic_vector(FASTFIFO_ADDRESS_DEPTH-1 downto 0) := (others => '0');
  signal Address      : std_logic_vector(FASTFIFO_ADDRESS_DEPTH-1 downto 0) := (others => '0');
  signal Empty        : std_logic;
  
begin  -- architecture str
  Empty     <= '0'                                    when unsigned(WriteAddress) > unsigned(ReadAddress) else '1';

  OUT_Empty <= Empty;
  OUT_data  <= mem(to_integer(unsigned(ReadAddress))) when Empty = '0'                                    else ZERO_ALGO_TRIGGER_OBJECT;

  reading : process (clk)
  begin
    if rising_edge(clk) then
      if IN_Reset = '1' then
        ReadAddress <= (others => '0');
      elsif IN_Ack = '1' and unsigned(ReadAddress) < (FASTFIFO_DATA_DEPTH-1) then
        ReadAddress <= std_logic_vector(unsigned(ReadAddress) + 1);
      else
        ReadAddress <= ReadAddress;     --the FIFO will be stuck to this
                                        --address until a reset is issued
      end if;
    end if;

  end process reading;

  writing : process (clk)
    variable temp : std_logic_vector(1 downto 0);
  begin
    if rising_edge(clk) then
      temp := IN_Reset&IN_Write;  
      case  temp is
        when "11" =>
          WriteAddress <= (0 => '1', others => '0');
        when "10" =>
          WriteAddress <= (others => '0');
        when "01" =>
          if unsigned(WriteAddress) >= FASTFIFO_DATA_DEPTH then
            report "Writing on full FIFO!" severity error;
          end if;
          WriteAddress <= std_logic_vector(unsigned(WriteAddress) + 1);
        when others =>
          WriteAddress <= WriteAddress;
      end case;

    end if;

  end process writing;

  mem(mem'high) <= ZERO_ALGO_TRIGGER_OBJECT;  --This word will be put as output when
                                         --the FIFO is empty

  Address <= WriteAddress when IN_Reset = '0' else (others => '0');

  memory : process (clk)
  begin
    if rising_edge(clk) then
      for i in 0 to mem'high-1 loop
        if IN_Write = '1' and i = to_integer(unsigned(Address)) then
          mem(i) <= IN_Data;
        else
          mem(i) <= mem(i);
        end if;
      end loop;

    end if;
  end process memory;


end architecture str;

-------------------------------------------------------------------------------
