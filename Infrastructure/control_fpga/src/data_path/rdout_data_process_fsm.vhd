
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

ENTITY rdout_data_process IS
   PORT( 
      clk           : IN     std_logic;
      fifo_empty    : IN     std_logic;
      reset         : IN     std_logic;
      rxdata_fifo   : IN     std_logic_vector (63 DOWNTO 0);
      tready_data   : IN     std_logic;
      valid         : IN     std_logic;
      aurora_tready : OUT    std_logic;
      first         : OUT    std_logic;
      payload_data  : OUT    std_logic_vector (63 DOWNTO 0);
      payload_last  : OUT    std_logic;
      payload_valid : OUT    std_logic;
      rd_fifo       : OUT    std_logic
   );



END ENTITY rdout_data_process ;
 
ARCHITECTURE fsm OF rdout_data_process IS
 
constant eof:integer:= 220;

TYPE STATE_TYPE IS (
      idle,
      header,
      pay_load,
      wait0,
      finish,
      wt
   );
 
  
SIGNAL current_state : STATE_TYPE;

BEGIN

   -----------------------------------------------------------------
   clocked_proc : PROCESS ( 
      clk,
      reset
   )
   -----------------------------------------------------------------
   BEGIN
      IF (reset = '1') THEN
         current_state <= idle;
         first         <= '0' ; 
         payload_last  <= '0' ; 
         payload_valid <= '0' ; 
         rd_fifo       <= '0' ;
         aurora_tready <= '0' ;
         payload_data  <=(others =>'0');
         
      ELSIF (clk'EVENT AND clk = '1') THEN

         -- Combined Actions
         CASE current_state IS
            WHEN idle => 
               first         <= '0' ; 
               payload_last  <= '0' ; 
               payload_valid <= '0' ; 
               rd_fifo       <= '0' ;
               aurora_tready <= '0' ;
               IF (fifo_empty ='0' and tready_data ='1') THEN 
                  rd_fifo <='1' ;
                  payload_data <= rxdata_fifo ;
                  current_state <= wt ; 
               ELSE
                  current_state <= idle;
               END IF;
            WHEN wt =>  
               current_state <= header ;  
               
            WHEN header => 
               payload_data <= rxdata_fifo ;
               payload_valid <=  valid ; 
               first <= '1' ;
               aurora_tready <= '1';
               if fifo_empty = '0' AND tready_data = '1' then
                  rd_fifo <= '1' ;
                  else
               payload_valid <= valid ; 
               aurora_tready <= '0';
                rd_fifo <= '0' ;
                first   <= '0' ;
               end if;
               IF (tready_data = '0') THEN 
                  rd_fifo       <= '0' ; 
                  payload_valid <= valid ; 
                  first         <= '0' ;
                  current_state <= wait0;
               ELSE
                  current_state <= pay_load;
               END IF;
            WHEN pay_load => 
               payload_data <= rxdata_fifo ;
               payload_valid <= valid ; 
               aurora_tready <= '1' ;
               first         <= '0' ;
               
               if fifo_empty = '0' AND tready_data = '1' then
                  rd_fifo <= '1' ;
                  else
                rd_fifo <= '0' ;
                payload_valid <= valid ; 
               aurora_tready <= '0';
               end if;
               IF (tready_data = '0') THEN  
                  rd_fifo <= '0' ;
                  payload_valid <= valid ; 
                  current_state <= wait0;
               ELSIF (eof = rxdata_fifo(7 downto 0)) THEN 
                  payload_data <= rxdata_fifo ;
                  payload_valid <= valid ;
                  aurora_tready <= '1';
                  rd_fifo <= '0' ;
                  payload_last <= '1';
                  current_state <= finish;
               ELSE
                  current_state <= pay_load;
               END IF;
            WHEN wait0 => 
               IF (tready_data = '1' and fifo_empty = '0' ) THEN 
                  current_state <= pay_load;
                  payload_valid <= valid ; 
               ELSE
                  current_state <= wait0;
                  payload_valid <= valid ; 
               END IF;
            WHEN finish => 
               payload_data <= rxdata_fifo ;
               payload_valid <= valid ; 
               aurora_tready <= '0';
               rd_fifo <= '0' ;
               payload_last <= '0';
               current_state <= idle;
            WHEN OTHERS =>
               current_state <= idle;
         END CASE;
      END IF;
   END PROCESS clocked_proc;
 
END ARCHITECTURE fsm;
