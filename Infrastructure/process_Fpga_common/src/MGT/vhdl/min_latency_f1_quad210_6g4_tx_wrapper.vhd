----------------------------------------------------------------------------------

--  This wrapper is for Quad 210 TTC MGT with TX at 6.4Gbps, Ref clk = 160MHz,


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library UNISIM;
use UNISIM.VCOMPONENTS.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
--! @copydoc min_latency_F1_quad210_6g4_tx_wrapper.vhd
entity min_latency_F1_quad210_6g4_tx_wrapper is
  generic
    (
      -- Simulation attributes
      EXAMPLE_SIM_GTRESET_SPEEDUP : string  := "FALSE";  -- Set to TRUE to speed up sim reset
      STABLE_CLOCK_PERIOD         : integer := 16
      );
  port
    (

      Q210_Q0_CLK1_GTREFCLK_PAD_N : in  std_logic;                      --! tx reference  clocks
      Q210_Q0_CLK1_GTREFCLK_PAD_P : in  std_logic;                      --! tx reference  clocks
      Q210_GT0_TXUSRCLK           : out std_logic;                      --! tx clock out gt0 
      Q210_GT1_TXUSRCLK           : out std_logic;                      --! tx clock out gt0
      -- tx ports    
      Q210_TXN                    : out std_logic_vector(1 downto 0);
      Q210_TXP                    : out std_logic_vector(1 downto 0);
      --reset signals & status
      Q210_SOFT_RESET_TX          : in  std_logic;                      --! soft reset in tx 
      Q210_GT0_TX_FSM_RESET_DONE  : out std_logic;                      --! tx gt0 fsm reset done 
      Q210_GT1_TX_FSM_RESET_DONE  : out std_logic;                      --! tx gt1 fsm reset done 
      Q210_gt0_txresetdone        : out std_logic;                      --! tx reset done    
      Q210_gt1_txresetdone        : out std_logic;                      --! tx reset done   
      -- cpll status 
      gt0_cpllfbclklost           : out std_logic;                      --! cpll gt0 clock lost
      gt1_cpllfbclklost           : out std_logic;                      --! cpll gt1 clock lost    
      Q210_gt0_cplllock           : out std_logic;                      --! cpll lock out  gt0   
      Q210_gt1_cplllock           : out std_logic;                      --! cpll lock out  gt1
      -- tx data in & control
      Q210_gt0_txdata             : in  std_logic_vector(31 downto 0);  --! txdata in gt0 
      Q210_gt0_txcharisk          : in  std_logic_vector(3 downto 0);   --! txchrisk  in gt0  
      Q210_gt1_txdata             : in  std_logic_vector(31 downto 0);  --! txdata in gt1 
      Q210_gt1_txcharisk          : in  std_logic_vector(3 downto 0);   --!txchrisk  in gt1 
      Q210_sysclk                 : in  std_logic
      );
end min_latency_F1_quad210_6g4_tx_wrapper;

architecture Behavioral of min_latency_F1_quad210_6g4_tx_wrapper is

  attribute DowngradeIPIdentifiedWarnings               : string;
  attribute DowngradeIPIdentifiedWarnings of Behavioral : architecture is "yes";

  attribute CORE_GENERATION_INFO               : string;
  attribute CORE_GENERATION_INFO of Behavioral : architecture is "min_latency_F1_quad210_6g4_tx,gtwizard_v3_6_5,{protocol_file=Start_from_scratch}";

--    component min_latency_F1_quad210_6g4_tx_support
--     generic
--        (
--        -- Simulation attributes
--        EXAMPLE_SIM_GTRESET_SPEEDUP    : string    := "FALSE";    -- Set to TRUE to speed up sim reset
--        STABLE_CLOCK_PERIOD            : integer   := 16 
--        );
--    port
--    (
--    SOFT_RESET_TX_IN                        : in   std_logic;
--    DONT_RESET_ON_DATA_ERROR_IN             : in   std_logic;
--    Q0_CLK1_GTREFCLK_PAD_N_IN               : in   std_logic;
--    Q0_CLK1_GTREFCLK_PAD_P_IN               : in   std_logic;

--    GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
--    GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
--    GT0_DATA_VALID_IN                       : in   std_logic;
--    GT1_TX_FSM_RESET_DONE_OUT               : out  std_logic;
--    GT1_RX_FSM_RESET_DONE_OUT               : out  std_logic;
--    GT1_DATA_VALID_IN                       : in   std_logic;

--    GT0_TXUSRCLK_OUT                        : out  std_logic;
--    GT0_TXUSRCLK2_OUT                       : out  std_logic;

--    GT1_TXUSRCLK_OUT                        : out  std_logic;
--    GT1_TXUSRCLK2_OUT                       : out  std_logic;

--    --_________________________________________________________________________
--    --GT0  (X0Y0)
--    --____________________________CHANNEL PORTS________________________________
--    --------------------------------- CPLL Ports -------------------------------
--    gt0_cpllfbclklost_out                   : out  std_logic;
--    gt0_cplllock_out                        : out  std_logic;
--    gt0_cpllreset_in                        : in   std_logic;
--    ------------------------------- Loopback Ports -----------------------------
--    gt0_loopback_in                         : in   std_logic_vector(2 downto 0);
--    --------------------- RX Initialization and Reset Ports --------------------
--    gt0_eyescanreset_in                     : in   std_logic;
--    -------------------------- RX Margin Analysis Ports ------------------------
--    gt0_eyescandataerror_out                : out  std_logic;
--    gt0_eyescantrigger_in                   : in   std_logic;
--    ------------------- Receive Ports - Digital Monitor Ports ------------------
--    gt0_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
--    --------------------- Receive Ports - RX Equalizer Ports -------------------
--    gt0_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
--    gt0_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
--    ------------- Receive Ports - RX Initialization and Reset Ports ------------
--    gt0_gtrxreset_in                        : in   std_logic;
--    --------------------- TX Initialization and Reset Ports --------------------
--    gt0_gttxreset_in                        : in   std_logic;
--    gt0_txuserrdy_in                        : in   std_logic;
--    ------------------ Transmit Ports - TX Data Path interface -----------------
--    gt0_txdata_in                           : in   std_logic_vector(31 downto 0);
--    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
--    gt0_gthtxn_out                          : out  std_logic;
--    gt0_gthtxp_out                          : out  std_logic;
--    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
--    gt0_txoutclkfabric_out                  : out  std_logic;
--    gt0_txoutclkpcs_out                     : out  std_logic;
--    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
--    gt0_txresetdone_out                     : out  std_logic;
--    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
--    gt0_txcharisk_in                        : in   std_logic_vector(3 downto 0);

--    --GT1  (X0Y1)
--    --____________________________CHANNEL PORTS________________________________
--    --------------------------------- CPLL Ports -------------------------------
--    gt1_cpllfbclklost_out                   : out  std_logic;
--    gt1_cplllock_out                        : out  std_logic;
--    gt1_cpllreset_in                        : in   std_logic;
--    ------------------------------- Loopback Ports -----------------------------
--    gt1_loopback_in                         : in   std_logic_vector(2 downto 0);
--    --------------------- RX Initialization and Reset Ports --------------------
--    gt1_eyescanreset_in                     : in   std_logic;
--    -------------------------- RX Margin Analysis Ports ------------------------
--    gt1_eyescandataerror_out                : out  std_logic;
--    gt1_eyescantrigger_in                   : in   std_logic;
--    ------------------- Receive Ports - Digital Monitor Ports ------------------
--    gt1_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
--    --------------------- Receive Ports - RX Equalizer Ports -------------------
--    gt1_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
--    gt1_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
--    ------------- Receive Ports - RX Initialization and Reset Ports ------------
--    gt1_gtrxreset_in                        : in   std_logic;
--    --------------------- TX Initialization and Reset Ports --------------------
--    gt1_gttxreset_in                        : in   std_logic;
--    gt1_txuserrdy_in                        : in   std_logic;
--    ------------------ Transmit Ports - TX Data Path interface -----------------
--    gt1_txdata_in                           : in   std_logic_vector(31 downto 0);
--    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
--    gt1_gthtxn_out                          : out  std_logic;
--    gt1_gthtxp_out                          : out  std_logic;
--    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
--    gt1_txoutclkfabric_out                  : out  std_logic;
--    gt1_txoutclkpcs_out                     : out  std_logic;
--    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
--    gt1_txresetdone_out                     : out  std_logic;
--    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
--    gt1_txcharisk_in                        : in   std_logic_vector(3 downto 0);

--    GT0_QPLLPD_IN                           : in   std_logic;
--    --____________________________COMMON PORTS________________________________
--     GT0_QPLLOUTCLK_OUT  : out std_logic;
--     GT0_QPLLOUTREFCLK_OUT : out std_logic;
--     sysclk_in        : in std_logic
--    );
--    end component;


--**************************** Wire Declarations ******************************



  ------------------------------- Global Signals -----------------------------

  signal tied_to_ground_i  : std_logic;
  signal tied_to_vcc_i     : std_logic;
  signal tied_to_vcc_vec_i : std_logic_vector(7 downto 0);

  signal Q210_GT1_RX_FSM_RESET_DONE, Q210_GT0_RX_FSM_RESET_DONE : std_logic;



--**************************** Main Body of Code *******************************
begin

  --  Static signal Assigments
  tied_to_ground_i  <= '0';
  tied_to_vcc_i     <= '1';
  tied_to_vcc_vec_i <= "11111111";


  ----------------------------- The GT Wrapper -----------------------------

  -- Use the instantiation template in the example directory to add the GT wrapper to your design.
  -- In this example, the wrapper is wired up for basic operation with a frame generator and frame 
  -- checker. The GTs will reset, then attempt to align and transmit data. If channel bonding is 
  -- enabled, bonding should occur after alignment.


  min_latency_F1_quad210_6g4_tx_support_i : entity work.min_latency_F1_quad210_6g4_tx_support
    generic map
    (
      EXAMPLE_SIM_GTRESET_SPEEDUP => EXAMPLE_SIM_GTRESET_SPEEDUP,
      STABLE_CLOCK_PERIOD         => STABLE_CLOCK_PERIOD
      )
    port map
    (
      SOFT_RESET_TX_IN            => Q210_SOFT_RESET_TX,
      DONT_RESET_ON_DATA_ERROR_IN => tied_to_ground_i,
      Q0_CLK1_GTREFCLK_PAD_N_IN   => Q210_Q0_CLK1_GTREFCLK_PAD_N,
      Q0_CLK1_GTREFCLK_PAD_P_IN   => Q210_Q0_CLK1_GTREFCLK_PAD_P,

      GT0_TX_FSM_RESET_DONE_OUT => Q210_GT0_TX_FSM_RESET_DONE,
      GT0_RX_FSM_RESET_DONE_OUT => Q210_GT0_RX_FSM_RESET_DONE,
      GT0_DATA_VALID_IN         => tied_to_ground_i,
      GT1_TX_FSM_RESET_DONE_OUT => Q210_GT1_TX_FSM_RESET_DONE,
      GT1_RX_FSM_RESET_DONE_OUT => Q210_GT1_RX_FSM_RESET_DONE,
      GT1_DATA_VALID_IN         => tied_to_ground_i,

      GT0_TXUSRCLK_OUT => Q210_GT0_TXUSRCLK,

      GT1_TXUSRCLK_OUT => Q210_GT1_TXUSRCLK,

      --_____________________________________________________________________
      --_____________________________________________________________________
      --GT0  (X0Y0)

      --------------------------------- CPLL Ports -------------------------------
      gt0_cpllfbclklost_out    => gt0_cpllfbclklost,
      gt0_cplllock_out         => Q210_gt0_cplllock,
      gt0_cpllreset_in         => tied_to_ground_i,
      ------------------------------- Loopback Ports -----------------------------
      gt0_loopback_in          => "000",
      --------------------- RX Initialization and Reset Ports --------------------
      gt0_eyescanreset_in      => tied_to_ground_i,
      -------------------------- RX Margin Analysis Ports ------------------------
      gt0_eyescandataerror_out => open,
      gt0_eyescantrigger_in    => tied_to_ground_i,
      ------------------- Receive Ports - Digital Monitor Ports ------------------
      gt0_dmonitorout_out      => open,
      --------------------- Receive Ports - RX Equalizer Ports -------------------
      gt0_rxmonitorout_out     => open,
      gt0_rxmonitorsel_in      => "00",
      ------------- Receive Ports - RX Initialization and Reset Ports ------------
      gt0_gtrxreset_in         => tied_to_ground_i,
      --------------------- TX Initialization and Reset Ports --------------------
      gt0_gttxreset_in         => tied_to_ground_i,
      gt0_txuserrdy_in         => tied_to_ground_i,
      ------------------ Transmit Ports - TX Data Path interface -----------------
      gt0_txdata_in            => Q210_gt0_txdata,
      ---------------- Transmit Ports - TX Driver and OOB signaling --------------
      gt0_gthtxn_out           => Q210_TXN(0),
      gt0_gthtxp_out           => Q210_TXP(0),
      ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
      gt0_txoutclkfabric_out   => open,
      gt0_txoutclkpcs_out      => open,
      ------------- Transmit Ports - TX Initialization and Reset Ports -----------
      gt0_txresetdone_out      => Q210_gt0_txresetdone,
      ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
      gt0_txcharisk_in         => Q210_gt0_txcharisk,

      --_____________________________________________________________________
      --_____________________________________________________________________
      --GT1  (X0Y1)

      --------------------------------- CPLL Ports -------------------------------
      gt1_cpllfbclklost_out    => gt1_cpllfbclklost,
      gt1_cplllock_out         => Q210_gt1_cplllock,
      gt1_cpllreset_in         => tied_to_ground_i,
      ------------------------------- Loopback Ports -----------------------------
      gt1_loopback_in          => "000",
      --------------------- RX Initialization and Reset Ports --------------------
      gt1_eyescanreset_in      => tied_to_ground_i,
      -------------------------- RX Margin Analysis Ports ------------------------
      gt1_eyescandataerror_out => open,
      gt1_eyescantrigger_in    => tied_to_ground_i,
      ------------------- Receive Ports - Digital Monitor Ports ------------------
      gt1_dmonitorout_out      => open,
      --------------------- Receive Ports - RX Equalizer Ports -------------------
      gt1_rxmonitorout_out     => open,
      gt1_rxmonitorsel_in      => "00",
      ------------- Receive Ports - RX Initialization and Reset Ports ------------
      gt1_gtrxreset_in         => tied_to_ground_i,
      --------------------- TX Initialization and Reset Ports --------------------
      gt1_gttxreset_in         => tied_to_ground_i,
      gt1_txuserrdy_in         => tied_to_ground_i,
      ------------------ Transmit Ports - TX Data Path interface -----------------
      gt1_txdata_in            => Q210_gt1_txdata,
      ---------------- Transmit Ports - TX Driver and OOB signaling --------------
      gt1_gthtxn_out           => Q210_TXN(1),
      gt1_gthtxp_out           => Q210_TXP(1),
      ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
      gt1_txoutclkfabric_out   => open,
      gt1_txoutclkpcs_out      => open,
      ------------- Transmit Ports - TX Initialization and Reset Ports -----------
      gt1_txresetdone_out      => Q210_gt1_txresetdone,
      ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
      gt1_txcharisk_in         => Q210_gt1_txcharisk,


      --____________________________COMMON PORTS________________________________
      GT0_QPLLPD_IN         => tied_to_vcc_i,  -- power down QPLL
      GT0_QPLLOUTCLK_OUT    => open,
      GT0_QPLLOUTREFCLK_OUT => open,
      sysclk_in             => Q210_sysclk
      );



end Behavioral;
