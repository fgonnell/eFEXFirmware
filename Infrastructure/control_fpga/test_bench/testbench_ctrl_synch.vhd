 
----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/29/2016 07:20:30 PM
-- Design Name: 
-- Module Name: test_bench_tac_sm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.all;
entity testbench_ctrl_synch is
  Port ( 
      
      clk160    : OUT    std_logic;
      ttc_clk     : OUT    std_logic;
      data_in     : OUT    std_logic_vector (31 DOWNTO 0);
      reset       : OUT    std_logic;
      cntrl_mux   : OUT    std_logic_vector(1 downto 0) ;
      MGT_COMMADET: OUT    std_logic;
      enable_mgt  : OUT    std_logic;
      start       : OUT    std_logic
      
       );
end testbench_ctrl_synch;

architecture Behavioral of testbench_ctrl_synch is

constant clk_prd_40 : time :=  25 ns ;--28 ns; 
constant clk_prd_160 : time :=  6.25 ns ;--28 ns;  
signal iclk_160      : std_logic;
signal iclk_40      : std_logic := '0' ;
signal cntr,cntr1: unsigned(31 downto 0) ;


BEGIN
 
 
  
  process 
  
   Begin
	  
       Reset        <= '1';  
       start        <= '0';
       MGT_COMMADET <= '0';
       enable_mgt  <='1';
       cntrl_mux  <= "00";
       cntr <= (others=>'0'); 
       cntr1 <= (others=>'0');      
       data_in <= x"00000000" ;      
       wait for clk_prd_160 *6 ;
       Reset       <= '0';
       cntrl_mux  <= "01";       
      
       wait for clk_prd_160 *6 ;
       start    <= '0';  
       wait for clk_prd_160 ;
       start    <= '1';      
       -- to create delays of mgtcommdet  
       wait for clk_prd_160 *7  ;       
       MGT_COMMADET <= '1';          
       data_in <= x"000000bc" ;  -- 1
                    
       wait for clk_prd_160 ;
       MGT_COMMADET <= '0';           
       data_in <= x"bbbbbbbb" ;      
       wait for clk_prd_160 ;
       data_in <= x"cccccccc" ;             
       wait for clk_prd_160 ;       
       data_in <= x"dddddddd" ;       
       wait for clk_prd_160 ; 
        MGT_COMMADET <= '1';                  
       data_in <= x"100000bc" ; --2   
       wait for clk_prd_160 ; 
        MGT_COMMADET <= '0';   
       data_in <= x"bbbbbbbb" ;              
       wait for clk_prd_160 ;
       data_in <= x"cccccccc" ;              
       wait for clk_prd_160 ;
       data_in <= x"dddddddd" ;              
       wait for clk_prd_160 ;
        MGT_COMMADET <= '1'; 
       data_in <= x"200000bc" ;   -- 3 
       wait for clk_prd_160 ;
        MGT_COMMADET <= '0'; 
       data_in <= x"eeeeeeee" ;               
       wait for clk_prd_160 ;
       data_in <= x"ffffffff" ;             
       wait for clk_prd_160 ;
       data_in <= x"0000001f" ; 
       wait for clk_prd_160 ;
        MGT_COMMADET <= '1'; 
       data_in <= x"300000bc" ;   -- 4  
       wait for clk_prd_160 ; 
        MGT_COMMADET <= '0';   
       data_in <= x"22222222" ;              
       wait for clk_prd_160 ;
       data_in <= x"33333333" ;              
       wait for clk_prd_160 ;
       data_in <= x"44444444" ;              
       wait for clk_prd_160 ;
       
        

--loop_1:    WHILE cntr < 3  LOOP -- this value must be multiple of 4 
               
--               data_in <= std_logic_vector(cntr);
               
--               cntr <= cntr+1;
                            
--                wait for clk_prd_160 ;
               
                           
--            END LOOP   loop_1;
           
--        wait for clk_prd_160 ;
                      
--            MGT_COMMADET <= '1';   
--            data_in <= x"500000bc" ;        
--            wait for clk_prd_160 ;
--            MGT_COMMADET <= '0';  
            
            
--loop_2:        WHILE cntr1 < 12  LOOP -- this value must be multiple of 4
                           
--               data_in <= std_logic_vector(cntr1);
                           
--               cntr1 <= cntr1+2;
                                        
--              wait for clk_prd_160 ;
                           
                                       
--           END LOOP loop_2  ;   
--            wait for 1 ms;
            
            
            
            
       
       wait for 1 ms;
  end process;   
 
----------------------------------------------- 
  -- Clock genration of the 160M 
 ------------------------------------------------ 
  
   clock_gen_160: process
     begin
       iclk_160<= '1' ;
        wait for clk_prd_160/2;
        iclk_160 <= '0';
        wait for clk_prd_160/2;
      end process clock_gen_160 ;
      clk160<= iclk_160;     


----------------------------------------------- 
  -- Clock genration of the 40M
 ------------------------------------------------ 
  
   clock_gen_40: process
     begin
       wait for 25 ns; -- Start offset 
  clock_loop:  loop    -- This is an infinite loop 
       iclk_40<= '0';
        wait for clk_prd_40/2 ;
        iclk_40 <= '1' ;
        wait for clk_prd_40/2 ;
   end loop clock_loop;     
      end process clock_gen_40 ;
      TTC_clk<= iclk_40 ;     
  
 
    


end Behavioral;
