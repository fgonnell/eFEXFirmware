onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /topsortingmodule_tb/CLK400
add wave -noupdate /topsortingmodule_tb/OUT_Version
add wave -noupdate /topsortingmodule_tb/IN_Control
add wave -noupdate /topsortingmodule_tb/OUT_Status
add wave -noupdate /topsortingmodule_tb/IN_Start
add wave -noupdate /topsortingmodule_tb/IN_Data
add wave -noupdate /topsortingmodule_tb/OUT_Start
add wave -noupdate /topsortingmodule_tb/OUT_Write
add wave -noupdate /topsortingmodule_tb/OUT_Data
add wave -noupdate /topsortingmodule_tb/Clk
add wave -noupdate /glbl/GSR
add wave -noupdate /topsortingmodule_tb/CLK400
add wave -noupdate /topsortingmodule_tb/OUT_Version
add wave -noupdate /topsortingmodule_tb/IN_Control
add wave -noupdate /topsortingmodule_tb/OUT_Status
add wave -noupdate /topsortingmodule_tb/IN_Start
add wave -noupdate /topsortingmodule_tb/IN_Data
add wave -noupdate /topsortingmodule_tb/OUT_Start
add wave -noupdate /topsortingmodule_tb/OUT_Write
add wave -noupdate /topsortingmodule_tb/OUT_Data
add wave -noupdate /topsortingmodule_tb/Clk
add wave -noupdate /glbl/GSR
add wave -noupdate /topsortingmodule_tb/CLK400
add wave -noupdate /topsortingmodule_tb/OUT_Version
add wave -noupdate /topsortingmodule_tb/IN_Control
add wave -noupdate /topsortingmodule_tb/OUT_Status
add wave -noupdate /topsortingmodule_tb/IN_Start
add wave -noupdate /topsortingmodule_tb/IN_Data
add wave -noupdate /topsortingmodule_tb/OUT_Start
add wave -noupdate /topsortingmodule_tb/OUT_Write
add wave -noupdate /topsortingmodule_tb/OUT_Data
add wave -noupdate /topsortingmodule_tb/Clk
add wave -noupdate /glbl/GSR
add wave -noupdate /topsortingmodule_tb/CLK400
add wave -noupdate /topsortingmodule_tb/OUT_Version
add wave -noupdate /topsortingmodule_tb/IN_Control
add wave -noupdate /topsortingmodule_tb/OUT_Status
add wave -noupdate /topsortingmodule_tb/IN_Start
add wave -noupdate /topsortingmodule_tb/IN_Data
add wave -noupdate /topsortingmodule_tb/OUT_Start
add wave -noupdate /topsortingmodule_tb/OUT_Write
add wave -noupdate /topsortingmodule_tb/OUT_Data
add wave -noupdate /topsortingmodule_tb/Clk
add wave -noupdate /glbl/GSR
add wave -noupdate /topsortingmodule_tb/CLK400
add wave -noupdate /topsortingmodule_tb/OUT_Version
add wave -noupdate /topsortingmodule_tb/IN_Control
add wave -noupdate /topsortingmodule_tb/OUT_Status
add wave -noupdate /topsortingmodule_tb/IN_Start
add wave -noupdate -radix hexadecimal -childformat {{/topsortingmodule_tb/IN_Data(7) -radix hexadecimal} {/topsortingmodule_tb/IN_Data(6) -radix hexadecimal} {/topsortingmodule_tb/IN_Data(5) -radix hexadecimal} {/topsortingmodule_tb/IN_Data(4) -radix hexadecimal} {/topsortingmodule_tb/IN_Data(3) -radix hexadecimal} {/topsortingmodule_tb/IN_Data(2) -radix hexadecimal} {/topsortingmodule_tb/IN_Data(1) -radix hexadecimal} {/topsortingmodule_tb/IN_Data(0) -radix hexadecimal}} -radixshowbase 0 -expand -subitemconfig {/topsortingmodule_tb/IN_Data(7) {-height 17 -radix hexadecimal -radixshowbase 0} /topsortingmodule_tb/IN_Data(6) {-height 17 -radix hexadecimal -radixshowbase 0} /topsortingmodule_tb/IN_Data(5) {-height 17 -radix hexadecimal -radixshowbase 0} /topsortingmodule_tb/IN_Data(4) {-height 17 -radix hexadecimal -radixshowbase 0} /topsortingmodule_tb/IN_Data(3) {-height 17 -radix hexadecimal -radixshowbase 0} /topsortingmodule_tb/IN_Data(2) {-height 17 -radix hexadecimal -radixshowbase 0} /topsortingmodule_tb/IN_Data(1) {-height 17 -radix hexadecimal -radixshowbase 0} /topsortingmodule_tb/IN_Data(0) {-height 17 -radix hexadecimal -radixshowbase 0}} /topsortingmodule_tb/IN_Data
add wave -noupdate /topsortingmodule_tb/OUT_Start
add wave -noupdate /topsortingmodule_tb/OUT_Write
add wave -noupdate -radix hexadecimal -radixshowbase 0 /topsortingmodule_tb/OUT_Data
add wave -noupdate /topsortingmodule_tb/Clk
add wave -noupdate /topsortingmodule_tb/STAGE
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/CLK400
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/OUT_Version
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/IN_Control
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/OUT_Status
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/IN_Start
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/IN_Data
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/OUT_Start
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/OUT_Write
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/OUT_Data
add wave -noupdate -expand -group TOPSorter -expand /topsortingmodule_tb/DUT/connector
add wave -noupdate -expand -group TOPSorter -expand /topsortingmodule_tb/DUT/start
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/writ(6)
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/writ(5)
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/writ(4)
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/writ(3)
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/writ(2)
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/writ(1)
add wave -noupdate -expand -group TOPSorter /topsortingmodule_tb/DUT/writ(0)
add wave -noupdate -expand -group FirstSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_1/clk
add wave -noupdate -expand -group FirstSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_1/IN_LastRead
add wave -noupdate -expand -group FirstSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_1/IN_LastWrite
add wave -noupdate -expand -group FirstSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_1/IN_Write
add wave -noupdate -expand -group FirstSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_1/IN_Data
add wave -noupdate -expand -group FirstSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_1/DataInL
add wave -noupdate -expand -group FirstSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_1/IN_Ack
add wave -noupdate -expand -group FirstSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_1/mem
add wave -noupdate -expand -group FirstSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_1/ReadAddress
add wave -noupdate -expand -group FirstSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_1/WriteAddress
add wave -noupdate -expand -group FirstSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_1/WriteAddressL
add wave -noupdate -expand -group FirstSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_1/OUT_Empty
add wave -noupdate -expand -group FirstSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_1/OUT_Data
add wave -noupdate -expand -group FirstSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_2/clk
add wave -noupdate -expand -group FirstSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_2/IN_LastRead
add wave -noupdate -expand -group FirstSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_2/IN_LastWrite
add wave -noupdate -expand -group FirstSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_2/IN_Data
add wave -noupdate -expand -group FirstSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_2/IN_Write
add wave -noupdate -expand -group FirstSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_2/IN_Ack
add wave -noupdate -expand -group FirstSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_2/OUT_Empty
add wave -noupdate -expand -group FirstSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_2/OUT_Data
add wave -noupdate -expand -group FirstSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_2/mem
add wave -noupdate -expand -group FirstSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_2/DataInL
add wave -noupdate -expand -group FirstSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_2/ReadAddress
add wave -noupdate -expand -group FirstSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_2/WriteAddress
add wave -noupdate -expand -group FirstSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/FastFifo_2/WriteAddressL
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/clk
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/IN_Start
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/IN_Write
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/IN_Data
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/LastRead
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/OutWrite
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/OutStart
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/Done
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/WriteInt
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/Write0
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/Write1
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/LastWrite
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/Empty
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/Ack
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/Acknowledge
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/Data
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/TOB_Count
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/OneOrTwo
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/OUT_Start
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/OUT_Write
add wave -noupdate -expand -group FirstSorter /topsortingmodule_tb/DUT/stage_gen(2)/ifFirst/sorter_gen0(0)/PAR_SORTER/OUT_Data
add wave -noupdate -expand -group SecondSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_1/clk
add wave -noupdate -expand -group SecondSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_1/IN_LastRead
add wave -noupdate -expand -group SecondSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_1/IN_LastWrite
add wave -noupdate -expand -group SecondSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_1/IN_Data
add wave -noupdate -expand -group SecondSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_1/IN_Write
add wave -noupdate -expand -group SecondSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_1/IN_Ack
add wave -noupdate -expand -group SecondSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_1/OUT_Empty
add wave -noupdate -expand -group SecondSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_1/OUT_Data
add wave -noupdate -expand -group SecondSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_1/mem
add wave -noupdate -expand -group SecondSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_1/DataInL
add wave -noupdate -expand -group SecondSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_1/ReadAddress
add wave -noupdate -expand -group SecondSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_1/WriteAddress
add wave -noupdate -expand -group SecondSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_1/WriteAddressL
add wave -noupdate -expand -group SecondSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_2/clk
add wave -noupdate -expand -group SecondSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_2/IN_LastRead
add wave -noupdate -expand -group SecondSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_2/IN_LastWrite
add wave -noupdate -expand -group SecondSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_2/IN_Data
add wave -noupdate -expand -group SecondSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_2/IN_Write
add wave -noupdate -expand -group SecondSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_2/IN_Ack
add wave -noupdate -expand -group SecondSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_2/OUT_Empty
add wave -noupdate -expand -group SecondSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_2/OUT_Data
add wave -noupdate -expand -group SecondSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_2/mem
add wave -noupdate -expand -group SecondSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_2/DataInL
add wave -noupdate -expand -group SecondSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_2/ReadAddress
add wave -noupdate -expand -group SecondSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_2/WriteAddress
add wave -noupdate -expand -group SecondSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/FastFifo_2/WriteAddressL
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/clk
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/IN_Start
add wave -noupdate -expand -group SecondSorter -expand /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/IN_Write
add wave -noupdate -expand -group SecondSorter -expand /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/IN_Data
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/OUT_Start
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/OUT_Write
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/OUT_Data
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/LastRead
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/OutWrite
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/OutStart
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/Done
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/WriteInt
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/Write0
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/Write1
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/LastWrite
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/Empty
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/Ack
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/Acknowledge
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/Data
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/OneOrTwo
add wave -noupdate -expand -group SecondSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(0)/PAR_SORTER/TOB_Count
add wave -noupdate -group LastSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_1/clk
add wave -noupdate -group LastSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_1/IN_LastRead
add wave -noupdate -group LastSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_1/IN_LastWrite
add wave -noupdate -group LastSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_1/IN_Data
add wave -noupdate -group LastSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_1/IN_Write
add wave -noupdate -group LastSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_1/IN_Ack
add wave -noupdate -group LastSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_1/OUT_Empty
add wave -noupdate -group LastSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_1/OUT_Data
add wave -noupdate -group LastSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_1/mem
add wave -noupdate -group LastSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_1/DataInL
add wave -noupdate -group LastSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_1/ReadAddress
add wave -noupdate -group LastSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_1/WriteAddress
add wave -noupdate -group LastSorter -group Fifo1 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_1/WriteAddressL
add wave -noupdate -group LastSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_2/clk
add wave -noupdate -group LastSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_2/IN_LastRead
add wave -noupdate -group LastSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_2/IN_LastWrite
add wave -noupdate -group LastSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_2/IN_Data
add wave -noupdate -group LastSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_2/IN_Write
add wave -noupdate -group LastSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_2/IN_Ack
add wave -noupdate -group LastSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_2/OUT_Empty
add wave -noupdate -group LastSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_2/OUT_Data
add wave -noupdate -group LastSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_2/mem
add wave -noupdate -group LastSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_2/DataInL
add wave -noupdate -group LastSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_2/ReadAddress
add wave -noupdate -group LastSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_2/WriteAddress
add wave -noupdate -group LastSorter -group Fifo2 /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/FastFifo_2/WriteAddressL
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/clk
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/IN_Start
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/IN_Write
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/IN_Data
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/OUT_Start
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/OUT_Write
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/OUT_Data
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/LastRead
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/OutWrite
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/OutStart
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/Done
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/WriteInt
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/Write0
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/Write1
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/LastWrite
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/Empty
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/Ack
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/Acknowledge
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/Data
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/OneOrTwo
add wave -noupdate -group LastSorter /topsortingmodule_tb/DUT/stage_gen(1)/ifAll/sorter_gen(1)/PAR_SORTER/TOB_Count
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {180000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 324
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix -1
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {173742 ps} {213774 ps}
