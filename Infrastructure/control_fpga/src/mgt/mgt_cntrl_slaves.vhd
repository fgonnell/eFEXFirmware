----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.10.2018 12:10:25
-- Design Name: 
-- Module Name: mgt_cntrl_slaves - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library ipbus_lib;
use ipbus_lib.ipbus.all;
LIBRARY infrastructure_lib;
USE infrastructure_lib.all;
use infrastructure_lib.mgt_type.all;
use infrastructure_lib.ipbus_decode_efex_cntrl_mgt.all;

entity mgt_cntrl_slaves is
 Port ( 
      rx_clk160           : in std_logic_vector(3 downto 0);
      rx_clk280           : in std_logic_vector(7 downto 0);       
      ipb_clk             : in std_logic;
      ipb_rst             : in std_logic;
      ipb_in              : in ipb_wbus;
      ipb_out             : out ipb_rbus;
      --mgt quad
      loopback            : out std_logic_vector(5 downto 0);
      softreset_tx       : out std_logic_vector(2 downto 0);
      softreset_rx        : out std_logic_vector(2 downto 0);      
      qpll_lock           : in  std_logic_vector(2 downto 0);
      qpll_refclklost     : in  std_logic_vector(2 downto 0);         
      rx_resetdone        : in std_logic_vector (11 downto 0);
      rx_fsm_resetdone    : in std_logic_vector (11 downto 0);
      rx_byteisaligned    : in std_logic_vector (11 downto 0);
      tx_resetdone        : in std_logic_vector (11 downto 0);
      tx_fsm_resetdone    : in std_logic_vector (11 downto 0);
      tx_bufstatus        : in std_logic_vector (23 downto 0);
      rx_realign          : in std_logic_vector (11 downto 0);
      rx_disperr          : in std_logic_vector (47 downto 0);
      encode_error        : in std_logic_vector (47 downto 0);
      bc_reg_sel          : out std_logic_vector (15 downto 0);
      delay_cntr_0        : in  std_logic_vector (3 downto 0);
      delay_cntr_1        : in  std_logic_vector (3 downto 0);
      mux_sel             : out std_logic_vector (15 downto 0);
      mgt_enable          : out std_logic_vector(3 downto 0)           
  
 );
 
-- attribute dont_touch : string;
-- attribute dont_touch of mgt_cntrl_slaves : entity is "true|yes";
end mgt_cntrl_slaves;

architecture Behavioral of mgt_cntrl_slaves is

signal ipbw: ipb_wbus_array(N_SLAVES-1 downto 0);
signal ipbr, ipbr_d: ipb_rbus_array(N_SLAVES-1 downto 0);
signal txclk,txresetdone,txfsm_resetdone,mgt_enable_i  : std_logic_vector(3 downto 0 );
signal mux_sel_i,bc_reg_sel_i : std_logic_vector(15 downto 0);

begin

--mux_sel     <= mux_sel_i(7 downto 0);
--bc_reg_sel  <= bc_reg_sel_i(16 downto 8);
--mgt_enable  <= mgt_enable_i(1 downto 0);



mgt_fabric : entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV      => N_SLAVES, --defined in ipbus_decode_mgt_slvs
                SEL_WIDTH => ipbus_sel_width)
    port map(
      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      sel             => ipbus_sel_efex_cntrl_mgt(ipb_in.ipb_addr),
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      );

quad_0: entity work.cntrl_mgt_quad_slaves 
  Port map ( 
     clk280              => rx_clk160(3 downto 0),             
     ipb_clk             => ipb_clk,            
     ipb_rst             => ipb_rst ,           
     ipb_in              => ipbw(N_SLV_MGT_114),            
     ipb_out             => ipbr(N_SLV_MGT_114),  
     tx_pd               => "0000", --tx power on
     rx_pd               => "1111", -- rx power on               
     loopback            => loopback(5 downto 3),
     bc_reg_sel          => bc_reg_sel, 
     mux_sel             => mux_sel,    
     bc_cntr_0           => (others => '0'),
     bc_cntr_1           => (others => '0'),
     bc_cntr_2           => (others => '0'),
     bc_cntr_3           => (others => '0'),
     bc_mux_cntr_0       => (others => '0'),
     bc_mux_cntr_1       => (others => '0'),
     bc_mux_cntr_2       => (others => '0'),
     bc_mux_cntr_3       => (others => '0'),  
     delay_cntr_0        => (others => '0'),
     delay_cntr_1        => (others => '0'),
     delay_cntr_2        => delay_cntr_0 ,
     delay_cntr_3        => delay_cntr_1 ,
     softreset_tx        => softreset_tx(2) ,      
     softreset_rx        => softreset_rx(2) ,   
     mgt_enable          => mgt_enable ,
     phase_mux           => open,        
     qpll_lock           => qpll_lock(2),          
     qpll_refclklost     => qpll_refclklost(2) ,   
     rx_resetdone        => rx_resetdone(11 downto 8) ,      
     rx_fsm_resetdone    => rx_fsm_resetdone(11 downto 8),   
     rx_byteisaligned    => rx_byteisaligned(11 downto 8),   
     tx_resetdone        => tx_resetdone(11 downto 8) ,      
     tx_fsm_resetdone    => tx_fsm_resetdone(11 downto 8),   
     tx_bufstatus        => tx_bufstatus(23 downto 16),       
     rx_realign          => rx_realign(11 downto 8) ,        
     rx_disperr          => rx_disperr(47 downto 32),         
     encode_error        => encode_error(47 downto 32)       
   );

quad_1: entity work.cntrl_mgt_quad_slaves
  Port map ( 
     clk280              => rx_clk280(3 downto 0),             
     ipb_clk             => ipb_clk,            
     ipb_rst             => ipb_rst ,           
     ipb_in              => ipbw(N_SLV_MGT_115),           
     ipb_out             => ipbr(N_SLV_MGT_115),  
     tx_pd               => "0000", --- tx power on
     rx_pd               => "1111", --- rx power on          
     loopback            => loopback(2 downto 0),
     bc_reg_sel          => open,             
     mux_sel             => open,            
     bc_cntr_0           => (others => '0'), 
     bc_cntr_1           => (others => '0'), 
     bc_cntr_2           => (others => '0'), 
     bc_cntr_3           => (others => '0'), 
     bc_mux_cntr_0       => (others => '0'), 
     bc_mux_cntr_1       => (others => '0'), 
     bc_mux_cntr_2       => (others => '0'), 
     bc_mux_cntr_3       => (others => '0'), 
     delay_cntr_0        => (others => '0'), 
     delay_cntr_1        => (others => '0'), 
     delay_cntr_2        => (others => '0'), 
     delay_cntr_3        => (others => '0'), 
     softreset_tx        => softreset_tx(0) ,      
     softreset_rx        => softreset_rx(0) ,      
     mgt_enable          => open ,
     phase_mux           => open, 
     qpll_lock           => qpll_lock(0),          
     qpll_refclklost     => qpll_refclklost(0) ,   
     rx_resetdone        => rx_resetdone(3 downto 0) ,      
     rx_fsm_resetdone    => rx_fsm_resetdone(3 downto 0),   
     rx_byteisaligned    => rx_byteisaligned(3 downto 0),   
     tx_resetdone        => tx_resetdone(3 downto 0) ,      
     tx_fsm_resetdone    => tx_fsm_resetdone(3 downto 0),   
     tx_bufstatus        => tx_bufstatus(7 downto 0),       
     rx_realign          => rx_realign(3 downto 0) ,        
     rx_disperr          => rx_disperr(15 downto 0),         
     encode_error        => encode_error(15 downto 0)        
     
  );
quad_2: entity work.cntrl_mgt_quad_slaves 
    Port map ( 
       clk280              => rx_clk280(7 downto 4),             
       ipb_clk             => ipb_clk,            
       ipb_rst             => ipb_rst ,           
       ipb_in              => ipbw(N_SLV_MGT_116),           
       ipb_out             => ipbr(N_SLV_MGT_116),  
       tx_pd               => "0000", --- tx power down
       rx_pd               => "1111", --- rx power on          
       loopback            => open, 
       bc_reg_sel          =>  open,             
       mux_sel             =>  open,            
       bc_cntr_0           => (others => '0'), 
       bc_cntr_1           => (others => '0'), 
       bc_cntr_2           => (others => '0'), 
       bc_cntr_3           => (others => '0'), 
       bc_mux_cntr_0       => (others => '0'), 
       bc_mux_cntr_1       => (others => '0'), 
       bc_mux_cntr_2       => (others => '0'), 
       bc_mux_cntr_3       => (others => '0'), 
       delay_cntr_0        => (others => '0'), 
       delay_cntr_1        => (others => '0'), 
       delay_cntr_2        => (others => '0'), 
       delay_cntr_3        => (others => '0'), 
       softreset_tx        => softreset_tx(1) ,      
       softreset_rx        => softreset_rx(1) ,      
       mgt_enable          => open, 
       phase_mux           => open, 
       qpll_lock           => qpll_lock(1),          
       qpll_refclklost     => qpll_refclklost(1) ,   
       rx_resetdone        => rx_resetdone(7 downto 4) ,      
       rx_fsm_resetdone    => rx_fsm_resetdone(7 downto 4),   
       rx_byteisaligned    => rx_byteisaligned(7 downto 4),   
       tx_resetdone        => tx_resetdone(7 downto 4) ,      
       tx_fsm_resetdone    => tx_fsm_resetdone(7 downto 4),   
       tx_bufstatus        => tx_bufstatus(15 downto 8),       
       rx_realign          => rx_realign(7 downto 4) ,        
       rx_disperr          => rx_disperr(31 downto 16),         
       encode_error        => encode_error(31 downto 16)        
       
    );

       
 

end Behavioral;
