
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

ENTITY ctrl_playback_ram IS
   PORT( 
      clk   : IN     std_logic;
      rdy   : IN     std_logic;
      reset : IN     std_logic;
      addr  : OUT    std_logic_vector (6 DOWNTO 0);
      en    : OUT    std_logic;
      kchar : OUT    std_logic
   );



END ENTITY ctrl_playback_ram ;

--
 
ARCHITECTURE Behavioral OF ctrl_playback_ram IS

TYPE STATE_TYPE IS (  idle, s0, s1,s2);

SIGNAL current_state : STATE_TYPE;
   
BEGIN

   -----------------------------------------------------------------
   clocked_proc : PROCESS ( 
      clk,
      reset
   )
   -----------------------------------------------------------------
   variable cntr: unsigned(3 downto 0):= "0000";
   variable addr_ram:unsigned (6 downto 0):= "0000000";
   BEGIN
      IF (reset = '1') THEN
         current_state <= idle;
         -- Default Reset Values
         addr <= (others => '0');
         cntr := (others => '0');
         en <= '0';
         kchar <= '0';
      ELSIF (clk'EVENT AND clk = '1') THEN

         -- Combined Actions
         CASE current_state IS
            WHEN idle => 
               cntr := (others => '0');
               kchar <= '0';
               en <= '0';
               addr <= (others => '0');
               addr_ram := (others => '0');
               IF (rdy = '1') THEN 
                  en <= '1' ;
                  current_state <= s0;
               ELSE
                  current_state <= idle;
               END IF;
            WHEN s0 => 
               en    <= '1';
               addr  <= std_logic_vector(addr_ram);
               kchar <= '0';
               cntr := cntr +1;
               addr_ram := addr_ram +1;
               IF (rdy= '0') THEN 
                  current_state <= idle;
                  
               ELSE
                  current_state <= s1;
               END IF;
               
            WHEN s1 => 
               en    <= '1';   
               addr  <= std_logic_vector(addr_ram);
               kchar <= '1';
               cntr  := cntr +1;
               addr_ram := addr_ram +1;
                current_state <= s2;        
               
            WHEN s2 => 
               en    <= '1';
               addr  <= std_logic_vector(addr_ram);
               kchar <= '0';
               cntr := cntr +1;
               addr_ram := addr_ram +1;
               
               IF (rdy ='0' ) THEN 
                  current_state <= idle;
               ELSIF (cntr = 7) THEN 
                  cntr := ( others => '0');
                   addr  <= std_logic_vector(addr_ram);
                   addr_ram := addr_ram +1;
                  current_state <= s0;
               ELSE
                  current_state <= s2;
               END IF;
            WHEN OTHERS =>
               current_state <= idle;
         END CASE;
      END IF;
   END PROCESS clocked_proc;
 
END ARCHITECTURE Behavioral;
