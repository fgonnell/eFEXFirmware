----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/16/2017 04:30:31 PM
-- Design Name: 
-- Module Name: data_type_pkg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
-- use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

package data_type_pkg is

    -- CHAR characters for insertion into event packet are defined here:
    constant ch_idle    : std_logic_vector(7 downto 0)  := X"BC"  ;    -- idle char is K28.5
    constant ch_sop1    : std_logic_vector(7 downto 0)  := X"3C"  ;    -- TOB/XTOB star of packet char is K28.1
    constant ch_sop2    : std_logic_vector(7 downto 0)  := X"7C"  ;    -- CALO DATA star of packet char is K28.3
    constant ch_eop     : std_logic_vector(7 downto 0)  := X"DC"  ;    -- end  of packet char is K28.6

    constant chan_no : integer := 49 ;    -- number of optical input links
    constant width   : integer := 49 ;
    
    -- this is the array of 49 x 224b RAW data in from 49 MGTs
    TYPE RAW_data_224_type is array (width-1 downto 0) of STD_LOGIC_VECTOR (223 downto 0);
    
    -- this is the array of 49 x 227b RAW data in from 49 MGTs
    TYPE RAW_data_227_type is array (width-1 downto 0) of STD_LOGIC_VECTOR (226 downto 0);
    
    -- this is the array of 49 x 228b RAW data in from 49 MGTs
    TYPE RAW_data_228_type is array (width-1 downto 0) of STD_LOGIC_VECTOR (227 downto 0);

    -- this is the array of 49 x 232b RAW data in from 49 MGTs
    TYPE RAW_data_232_type is array (width-1 downto 0) of STD_LOGIC_VECTOR (231 downto 0);

    -- this is the array of 49 of 36b RAW data from DPRAM into FIFO
    TYPE DPR_RAW_out_36_type is array (width-1 downto 0) of STD_LOGIC_VECTOR (35 downto 0);

    -- this is the array of 49 x 4b error flags in from 49 MGTs
    TYPE link_error_type is array (width-1 downto 0) of STD_LOGIC_VECTOR (3 downto 0);

    -- this is the array of 7 TOBS of 32b & 1b valid
    TYPE TOBs_7_type is array (6 downto 0) of STD_LOGIC_VECTOR (32 downto 0); -- 32b TOB & 1b valid

    -- this is the array of 80 e/g TOBS of 32bx2 & 1b valid
    TYPE TOBs_80_type is array (79 downto 0) of STD_LOGIC_VECTOR (32 downto 0); -- 32b TOB & 1b valid
	
	-- this is the array of 49 words each 9 bits
	TYPE 	t_49_arr_9b is array (width-1 downto 0) of STD_LOGIC_VECTOR(8 downto 0);

	-- this is the array of 49 words each 1 bits
	TYPE 	t_49_arr_1b is array (width-1 downto 0) of STD_LOGIC;

    --!  array of 8, holding XTOB_BCN (7b) & 5b valid & 170b XTOB_data_out   
     TYPE array_8_of_182b is array (7 downto 0) of STD_LOGIC_VECTOR (181 downto 0);
                      
    --!  array of 8, holding 170b XTOB_data_out    
     TYPE array_8_of_170b is array (7 downto 0) of STD_LOGIC_VECTOR (169 downto 0);
                      
    --!  array of 8, holding 5b XTOB_valid_out signlas    
     TYPE array_8_of_5b is array (7 downto 0) of STD_LOGIC_VECTOR (4 downto 0);
                      
    --!  array of 8, holding 5b XTOB FIFO data count  
     TYPE array_8_of_9b is array (7 downto 0) of STD_LOGIC_VECTOR (8 downto 0);                 

     function to_TOBs_80_type (X : std_logic_vector(2559 downto 0); V : std_logic_vector(39 downto 0)) return TOBs_80_type;
     
     function to_TOBs_7_type (X : std_logic_vector(223 downto 0); V : std_logic_vector(6 downto 0)) return TOBs_7_type;

     function stdv ( inp : std_logic ) return std_logic_vector;
        
end data_type_pkg;

package body data_type_pkg is

    -- this function changes the 2560b XTOB (64b x 40) into 80 * 32b TOB & 1b Valid
    -- X = 2560b Topo TOB
    -- V = 40b Valid signal        
    function to_TOBs_80_type (X : std_logic_vector(2559 downto 0); V : std_logic_vector(39 downto 0)) return TOBs_80_type is
        variable Y : TOBs_80_type;
        variable i : integer;
     begin
        for i in 0 to 39 loop -- there are 40 64b words that go into 80 32b words
          Y(i*2)   := V(i) & X((i*32+32)-1     downto (i*32));
          Y(i*2+1) := V(i) & X(((i+1)*32+32)-1 downto ((i+1)*32));
        end loop;  -- i
        return Y;
     end;

    -- this function changes the 224b Topo TOB into 7 * 32b TOB & 1b Valid
    -- X = 224b Topo TOB
    -- V = 7b Valid signal        
    function to_TOBs_7_type (X : std_logic_vector(223 downto 0); V : std_logic_vector(6 downto 0)) return TOBs_7_type is
         variable Y : TOBs_7_type;
         variable i : integer;
      begin
         --  for i in Y'range loop
         for i in 0 to 6 loop
           Y(i) := V(i) & X((i*32+32)-1 downto (i*32));
         end loop;  -- i
         return Y;
      end;
        
    -- this function get a std_logic bit and returns a std_logic_vector(0 downto 0)
    -- for use for the dual port ram enable signal
    function stdv ( inp : std_logic ) return std_logic_vector is
        variable temp : std_logic_vector(0 downto 0);
    begin
        temp(0) := inp;
        return temp;
    end stdv;


end data_type_pkg;
