----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;

use IEEE.NUMERIC_STD.all;

library std;
use std.textio.all;                     --TEXT, FILE is here


library algolib;
use algolib.DataTypes.all;
use algolib.Utility.all;


entity FileWriter is
  generic(
    data_width : integer := 32;
    sync_delimiter : string := "---";
    filename   : string  := "out_file.txt");
  port (
    CLK     : in std_logic;
    RST     : in std_logic := '0';
    DATA_IN : in std_logic_vector(data_width-1 downto 0);
    SYNC    : in std_logic := '0';
    WE      : in std_logic
    );
end FileWriter;

--
architecture Behavioral of FileWriter is

  file outfile : text open write_mode is filename;
  signal cnt : integer := 0;
  signal syn_cnt : integer := 0;  
begin
  process(clk)

  begin
    if RST = '1' then
        print(outfile, "RESET");
        cnt     <= 0;
        syn_cnt <= 0;
    elsif rising_edge(clk) then
      cnt <= cnt + 1;
      if WE = '1' then
        if SYNC = '1' then
          syn_cnt <= syn_cnt + 1;
          print(outfile, hstr(DATA_IN) & " " & sync_delimiter & " " & str(syn_cnt) & " " & str(cnt));
        else
          print(outfile, hstr(DATA_IN));
        end if;
      end if;

    end if;

  end process;
end Behavioral;

