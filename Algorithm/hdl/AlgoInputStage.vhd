--! @file
--! @brief Input Stage Module
--! @details
--! This module receives the input data in the AlgoInput format and converts it
--! into the TriggerTowerMatrix format.
--! The 10-bit samples coming from LAr in a multi liner scale are also
--! converted at this stage by means of the EnergyConverter Module
--! @author Francesco Gonnella


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;


library work;
use work.DataTypes.all;
use work.AlgoDataTypes.all;

--!@copydoc AlgoInputStage.vhd
entity AlgoInputStage is
  port (CLK200      : in std_logic;
        CLK280      : in std_logic;
        IN_Load     : in std_logic;
        IN_Data     : in AlgoInput;
        IN_Position : in AlgoRegister;
        IN_BCID     : in BCID_array;

        IN_threshold_l0  : in std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
        IN_threshold_l1  : in std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
        IN_threshold_l2  : in std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
        IN_threshold_l3  : in std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
        IN_threshold_had : in std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);

        OUT_Load     : out std_logic;
        OUT_BCID_200 : out BCID_array;
        OUT_Data     : out TriggerTowerMatrix
        );
end AlgoInputStage;

--!@copydoc AlgoInputStage.vhd
architecture Behavioral of AlgoInputStage is
  signal OutData          : TriggerTowerMatrix;
  signal Load_i, Load_d   : std_logic;
  signal BCID_i, BCID_d, BCID_200 : BCID_array;
  signal BCID_280         : BCID_t;
  signal is_hadronic      : std_logic;

--  ####### Mark signals  ########

  attribute keep                   : string;
  attribute max_fanout             : integer;
  attribute keep of OUT_Load       : signal is "true";
  attribute max_fanout of OUT_Load : signal is 50;

--   #######################################

begin
  Load_i      <= IN_Load;
  BCID_i      <= IN_BCID;
  is_hadronic <= '1' when IN_Position(7 downto 5) = "000" else '0';

  column_for : for i in 0 to INPUT_COLUMNS - 1 generate
    row_for : for j in 0 to INPUT_ROWS - 1 generate

      layer0_for : for k in 0 to LAYER0-1 generate
        EnergyConverter0 : entity work.EnergyConverter
          port map (
            clk          => clk200,
            IN_threshold => IN_threshold_l0,
            IN_Load      => Load_i,
            IN_Data      => IN_Data(i)(j).Layer0(k),
            OUT_Data     => OutData(i)(j).Layer0(k));
      end generate layer0_for;

      layer1_for : for k in 0 to LAYER1-1 generate
        EnergyConverter1 : entity work.EnergyConverter
          port map (
            clk          => clk200,
            IN_threshold => IN_threshold_l0,
            IN_Load      => Load_i,
            IN_Data      => IN_Data(i)(j).Layer1(k),
            OUT_Data     => OutData(i)(j).Layer1(k));
      end generate layer1_for;

      layer2_for : for k in 0 to LAYER2-1 generate
        EnergyConverter2 : entity work.EnergyConverter
          port map (
            clk          => clk200,
            IN_threshold => IN_threshold_l0,
            IN_Load      => Load_i,
            IN_Data      => IN_Data(i)(j).Layer2(k),
            OUT_Data     => OutData(i)(j).Layer2(k));
      end generate layer2_for;

      layer3_for : for k in 0 to LAYER3-1 generate
        EnergyConverter3 : entity work.EnergyConverter
          port map (
            clk          => clk200,
            IN_threshold => IN_threshold_l0,
            IN_Load      => Load_i,
            IN_Data      => IN_Data(i)(j).Layer3(k),
            OUT_Data     => OutData(i)(j).Layer3(k));
      end generate layer3_for;

      EnergyConverterH : entity work.EnergyConverter  -- These cells (Tile) have a linear encoding with .500 GeV LSB
        port map (
          clk          => clk200,
          is_hadronic  => is_hadronic,
          IN_threshold => IN_threshold_l0,
          IN_Load      => Load_i,
          IN_Data      => IN_Data(i)(j).Hadron(0),
          OUT_Data     => OutData(i)(j).Hadron(0));

    end generate row_for;
  end generate column_for;

  OUT_Data <= OutData;

  process (CLK200)
  begin
    if rising_edge(CLK200) then
      Load_d   <= Load_i;
      OUT_Load <= Load_d;
    end if;
  end process;

  process (CLK200)
  begin
    if rising_edge(CLK200) then
      if Load_i = '1' then
        BCID_200 <= BCID_i;
      else
        BCID_200 <= BCID_200;
      end if;
      BCID_d <= BCID_200;
    end if;
  end process;

  OUT_BCID_200 <= BCID_d;

end Behavioral;


