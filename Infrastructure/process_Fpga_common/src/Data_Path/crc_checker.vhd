library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.all;

 entity crc_checker is
 port (
    clk         :in std_logic;
   reset        :in std_logic;
	mgt_commdet :in std_logic;
	rxdata      :in std_logic_vector(31 downto 0);
	crc_error   :out std_logic;
	crc_error_40   :out std_logic
	);
 end crc_checker;	
 architecture behavioral of crc_checker is
 constant REVERSE_BIT_ORDER : boolean := TRUE;

 signal start_rxcrc_i : std_logic;
 signal rx_data_i     : std_logic_vector(31 downto 0);
 signal rx_crc_i      : std_logic_vector(8 downto 0);
 
 
   type state_type is (  idle, high_crc  ) ;
   signal current_state : state_type;
   signal count_40 :unsigned (2 downto 0) :=  "000";  
   
  begin
 
 pipe_start : process(clk)
  variable count: unsigned(2 downto 0);
         begin
            if clk' event and clk ='1' then
               if mgt_commdet ='1' or count = 6 then
                 count := (others => '0');
                 start_rxcrc_i <= '1';
               else
                 count := count + 1;
                 start_rxcrc_i <= '0';
               end if;
            end if;
         end process;
        
 -- pipeline the incoming rx data
 pipe_rxdata: process (clk)
           begin 
		    if clk' event and clk ='1' then
		       if mgt_commdet ='1' then
		           rx_data_i <= rxdata (31 downto 8) & x"00";
		          else
			    rx_data_i <= rxdata;
			end if;
			end if;
         end process;
	
  
 RX: entity work.osum_crc9d32
    generic map(
      REVERSE_BIT_ORDER => REVERSE_BIT_ORDER)
    port map (
      d_in       => rx_data_i,
      crc_start  => start_rxcrc_i,
      clock      => clk,
      crc_out    => rx_crc_i
      );
-- check crc is zero
  Check_crc: process (clk)
  begin
     if clk' event and clk ='1' then
        if start_rxcrc_i = '1' and rx_crc_i /= "000000000" then 
		   crc_error  <= '1'; 
	 else 
	      crc_error <= '0'; 
	 end if; 
     end if;
   end process;	 
   
   -- check crc 40m is zero
   -- This small state machine that is used to hold the crc error upto 6 clock cycle of 280M, in oder to capture ar same time to the incoming data
  Check_crc_40: process (clk) 
  
  begin
     if clk' event and clk ='1' then
        if  (reset = '1') then 
         current_state <= idle;
           count_40 <= (others => '0');
          crc_error_40 <= '0';
         else  
         count_40 <= (others => '0');
          crc_error_40 <= '0';
     case current_state IS
           when idle => 
                  if start_rxcrc_i = '1' and rx_crc_i /= "000000000" then 
                  current_state <= high_crc ;
                 count_40 <=count_40+1;
                 crc_error_40  <= '1'; 
              else
                  current_state <= idle;
              end if ;  
           when  high_crc => 
                count_40 <=count_40+1;
                crc_error_40  <= '1'; 
              if (  count_40= 5) then
                    count_40 <= (others =>'0');
                     current_state <= idle;   
               end if; 
               when  others =>
                     current_state <= idle;
               end case;
     end if;
     end if;
   end process;	 
   
   
   
end behavioral;