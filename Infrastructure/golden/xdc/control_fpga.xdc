####################################################################################
# Constraint file is for Golden contrl fpga  (XC7VX330TFFG1157-2)                  #
####################################################################################
create_clock -period 8.000 -name sysclk [get_ports gt_clk125_p]
create_clock -period 8.000 -name gmii_rx_clk [get_ports gmii_rx_clk]
create_clock -period 32.000 -name ipb_clk [get_nets ipb_clk]
create_clock -period 8.000 -name mac_clk [get_nets mac_clk]

set_clock_groups -asynchronous -group [get_clocks ipb_clk]
set_clock_groups -asynchronous -group [get_clocks mac_clk]

set_property IOSTANDARD LVDS [get_ports gt_clk125_n]
set_property IOSTANDARD LVDS [get_ports gt_clk125_p]
set_property PACKAGE_PIN AE31 [get_ports gt_clk125_p]
set_property PACKAGE_PIN AF31 [get_ports gt_clk125_n]

set_property DIFF_TERM TRUE [get_ports gt_clk125_p]
set_property DIFF_TERM TRUE [get_ports gt_clk125_n]

set_property IOSTANDARD LVDS [get_ports clk_40_p]
set_property IOSTANDARD LVDS [get_ports clk_40_n]
set_property PACKAGE_PIN AL29 [get_ports clk_40_p]
set_property PACKAGE_PIN AL30 [get_ports clk_40_n]

########## Hardware address  pins

set_property IOSTANDARD LVCMOS18 [get_ports {hardware_addr[*]}]
set_property PACKAGE_PIN AB28 [get_ports {hardware_addr[0]}]
set_property PACKAGE_PIN AB27 [get_ports {hardware_addr[1]}]
set_property PACKAGE_PIN Y27 [get_ports {hardware_addr[2]}]
set_property PACKAGE_PIN W27 [get_ports {hardware_addr[3]}]
set_property PACKAGE_PIN W26 [get_ports {hardware_addr[4]}]
set_property PACKAGE_PIN Y26 [get_ports {hardware_addr[5]}]
set_property PACKAGE_PIN V25 [get_ports {hardware_addr[6]}]
set_property PACKAGE_PIN V24 [get_ports {hardware_addr[7]}]
set_property PACKAGE_PIN AB26 [get_ports {hardware_addr[8]}]
set_property PACKAGE_PIN AA26 [get_ports {hardware_addr[9]}]
set_property PACKAGE_PIN W25 [get_ports {hardware_addr[10]}]
set_property PACKAGE_PIN W24 [get_ports {hardware_addr[11]}]


############ Serial number ################

set_property IOSTANDARD LVCMOS18 [get_ports {serial_number[*]}]
set_property PACKAGE_PIN N32 [get_ports {serial_number[0]}]
set_property PACKAGE_PIN P32 [get_ports {serial_number[1]}]
set_property PACKAGE_PIN T26 [get_ports {serial_number[2]}]
set_property PACKAGE_PIN U25 [get_ports {serial_number[3]}]
set_property PACKAGE_PIN U27 [get_ports {serial_number[4]}]
set_property PACKAGE_PIN U26 [get_ports {serial_number[5]}]

############ SPI Flash #############################


set_property PACKAGE_PIN AL33 [get_ports flash_csn]
set_property IOSTANDARD LVCMOS18 [get_ports flash_csn]
set_property PACKAGE_PIN AN33 [get_ports flash_mosi]
set_property IOSTANDARD LVCMOS18 [get_ports flash_mosi]
set_property PACKAGE_PIN AN34 [get_ports flash_miso]
set_property IOSTANDARD LVCMOS18 [get_ports flash_miso]


##############SPI PLL###########################

set_property PACKAGE_PIN AN27 [get_ports POWERDN_B_CDCE]
set_property IOSTANDARD LVCMOS18 [get_ports POWERDN_B_CDCE]

set_property PACKAGE_PIN AN28 [get_ports SYNC_B_CDCE]
set_property IOSTANDARD LVCMOS18 [get_ports SYNC_B_CDCE]


set_property PACKAGE_PIN AM25 [get_ports pll_mosi]
set_property IOSTANDARD LVCMOS18 [get_ports pll_mosi]

set_property PACKAGE_PIN AM28 [get_ports pll_clko]
set_property IOSTANDARD LVCMOS18 [get_ports pll_clko]


set_property PACKAGE_PIN AP25 [get_ports pll_miso]
set_property IOSTANDARD LVCMOS18 [get_ports pll_miso]

set_property PACKAGE_PIN AN25 [get_ports pll_le_1]
set_property IOSTANDARD LVCMOS18 [get_ports pll_le_1]



set_property PACKAGE_PIN AN29 [get_ports pll_le_2]
set_property IOSTANDARD LVCMOS18 [get_ports pll_le_2]

set_property PACKAGE_PIN AM27 [get_ports pll_le_3]
set_property IOSTANDARD LVCMOS18 [get_ports pll_le_3]

set_property PACKAGE_PIN AP26 [get_ports pll_lock_1]
set_property IOSTANDARD LVCMOS18 [get_ports pll_lock_1]

set_property PACKAGE_PIN AP29 [get_ports pll_lock_2]
set_property IOSTANDARD LVCMOS18 [get_ports pll_lock_2]
set_property PACKAGE_PIN AP27 [get_ports pll_lock_3]
set_property IOSTANDARD LVCMOS18 [get_ports pll_lock_3]


##################################################
set_property PACKAGE_PIN AK28 [get_ports xtal_ttc_clk_sel]
set_property IOSTANDARD LVCMOS18 [get_ports xtal_ttc_clk_sel]


############ I2C   ###############################
set_property PACKAGE_PIN AL26 [get_ports i2c_scl_0]
set_property IOSTANDARD LVCMOS18 [get_ports i2c_scl_0]

set_property PACKAGE_PIN AL25 [get_ports i2c_sda_0]
set_property IOSTANDARD LVCMOS18 [get_ports i2c_sda_0]

set_property PACKAGE_PIN AM26 [get_ports i2c_rst_0]
set_property IOSTANDARD LVCMOS18 [get_ports i2c_rst_0]
###################################################


################# LEDs ####################
set_property IOSTANDARD LVCMOS18 [get_ports {leds[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {leds[0]}]
set_property SLEW SLOW [get_ports {leds[1]}]
set_property SLEW SLOW [get_ports {leds[0]}]
set_property PACKAGE_PIN Y24 [get_ports {leds[0]}]
set_property PACKAGE_PIN AA25 [get_ports {leds[1]}]

set_property IOSTANDARD LVCMOS18 [get_ports {f5_ttc_clk_sel}]
set_property SLEW SLOW [get_ports {f5_ttc_clk_sel}]
set_property PACKAGE_PIN AD32 [get_ports {f5_ttc_clk_sel}]
#######################################################





######################################################
#
###############  PHY connections #####################
### Tx side

set_property IOSTANDARD LVCMOS18 [get_ports {gmii_txd[*]}]

set_property PACKAGE_PIN M20 [get_ports {gmii_txd[0]}]
set_property PACKAGE_PIN L20 [get_ports {gmii_txd[1]}]
set_property PACKAGE_PIN J20 [get_ports {gmii_txd[2]}]
set_property PACKAGE_PIN H20 [get_ports {gmii_txd[3]}]
set_property PACKAGE_PIN J19 [get_ports {gmii_txd[4]}]
set_property PACKAGE_PIN K18 [get_ports {gmii_txd[5]}]
set_property PACKAGE_PIN L18 [get_ports {gmii_txd[6]}]
set_property PACKAGE_PIN M18 [get_ports {gmii_txd[7]}]

set_property PACKAGE_PIN H23 [get_ports gmii_tx_en]
set_property IOSTANDARD LVCMOS18 [get_ports gmii_tx_en]

set_property PACKAGE_PIN G20 [get_ports gmii_tx_er]
set_property IOSTANDARD LVCMOS18 [get_ports gmii_tx_er]

### RX side
set_property IOSTANDARD LVCMOS18 [get_ports {gmii_rxd[*]}]

#set_property IOB TRUE [get_ports {gmii_rxd[*]}]

set_property PACKAGE_PIN E21 [get_ports {gmii_rxd[0]}]
set_property PACKAGE_PIN B20 [get_ports {gmii_rxd[1]}]   
set_property PACKAGE_PIN C20 [get_ports {gmii_rxd[2]}]
set_property PACKAGE_PIN F21 [get_ports {gmii_rxd[3]}]
set_property PACKAGE_PIN F19 [get_ports {gmii_rxd[4]}]
set_property PACKAGE_PIN D21 [get_ports {gmii_rxd[5]}]
set_property PACKAGE_PIN G22 [get_ports {gmii_rxd[6]}]
set_property PACKAGE_PIN H22 [get_ports {gmii_rxd[7]}]



set_property PACKAGE_PIN D20 [get_ports gmii_rx_dv]
set_property IOSTANDARD LVCMOS18 [get_ports gmii_rx_dv]


set_property PACKAGE_PIN C19 [get_ports gmii_rx_er]
set_property IOSTANDARD LVCMOS18 [get_ports gmii_rx_er]


set_property PACKAGE_PIN G21 [get_ports gmii_rx_clk]            
set_property IOSTANDARD LVCMOS18 [get_ports gmii_rx_clk]


set_property PACKAGE_PIN A20 [get_ports gmii_gtx_clk]
set_property IOSTANDARD LVCMOS18 [get_ports gmii_gtx_clk]


set_property PACKAGE_PIN A19 [get_ports phy_rstb]
set_property IOSTANDARD LVCMOS18 [get_ports phy_rstb]

####################### timing constrain

#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets gmii_rx_clk_IBUF]

#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets clk_rx/inst/clk_in1_clk_wiz_0]


####### Signals coming from FPGA1
set_property IOSTANDARD LVCMOS18 [get_ports {master_tx_data1[*]}]

set_property PACKAGE_PIN AM23 [get_ports {master_tx_data1[0]}]
set_property PACKAGE_PIN AJ21 [get_ports {master_tx_data1[1]}]
set_property PACKAGE_PIN AK23 [get_ports {master_tx_data1[2]}]
set_property PACKAGE_PIN AK24 [get_ports {master_tx_data1[3]}]
set_property PACKAGE_PIN AH23 [get_ports {master_tx_data1[4]}]
set_property PACKAGE_PIN AJ22 [get_ports {master_tx_data1[5]}]
set_property PACKAGE_PIN AP21 [get_ports {master_tx_data1[6]}]
set_property PACKAGE_PIN AL20 [get_ports {master_tx_data1[7]}]
set_property PACKAGE_PIN AG20 [get_ports {master_tx_data1[8]}]
set_property PACKAGE_PIN AG22 [get_ports {master_tx_data1[9]}]

########## Signals going to FPGA1
set_output_delay -clock mac_clk -min 4.000 [get_ports {master_rx_data1[*]}]
set_output_delay -clock mac_clk -max -0.500 [get_ports {master_rx_data1[*]}]

set_output_delay -clock mac_clk -min 4.000 [get_ports master_tx_pause1]
set_output_delay -clock mac_clk -max -0.500 [get_ports master_tx_pause1]


set_property IOSTANDARD LVCMOS18 [get_ports master_tx_pause1]
set_property PACKAGE_PIN AM20 [get_ports master_tx_pause1]

set_property IOSTANDARD LVCMOS18 [get_ports {master_rx_data1[*]}]

set_property PACKAGE_PIN AN23 [get_ports {master_rx_data1[0]}]
set_property PACKAGE_PIN AK21 [get_ports {master_rx_data1[1]}]
set_property PACKAGE_PIN AL24 [get_ports {master_rx_data1[2]}]
set_property PACKAGE_PIN AK22 [get_ports {master_rx_data1[3]}]
set_property PACKAGE_PIN AH22 [get_ports {master_rx_data1[4]}]
set_property PACKAGE_PIN AG23 [get_ports {master_rx_data1[5]}]
set_property PACKAGE_PIN AL23 [get_ports {master_rx_data1[6]}]
set_property PACKAGE_PIN AP22 [get_ports {master_rx_data1[7]}]
set_property PACKAGE_PIN AH20 [get_ports {master_rx_data1[8]}]
set_property PACKAGE_PIN AJ20 [get_ports {master_rx_data1[9]}]


####### Signals coming from FPGA2
set_property IOSTANDARD LVCMOS18 [get_ports {master_tx_data2[*]}]

set_property PACKAGE_PIN AK18 [get_ports {master_tx_data2[0]}]
set_property PACKAGE_PIN AG17 [get_ports {master_tx_data2[1]}]
set_property PACKAGE_PIN AP17 [get_ports {master_tx_data2[2]}]
set_property PACKAGE_PIN AM17 [get_ports {master_tx_data2[3]}]
set_property PACKAGE_PIN AM18 [get_ports {master_tx_data2[4]}]
set_property PACKAGE_PIN AJ17 [get_ports {master_tx_data2[5]}]
set_property PACKAGE_PIN AL16 [get_ports {master_tx_data2[6]}]
set_property PACKAGE_PIN AL15 [get_ports {master_tx_data2[7]}]
set_property PACKAGE_PIN AN14 [get_ports {master_tx_data2[8]}]
set_property PACKAGE_PIN AN15 [get_ports {master_tx_data2[9]}]

######### Signals going to FPGA2

set_output_delay -clock mac_clk -min 4.000 [get_ports {master_rx_data2[*]}]
set_output_delay -clock mac_clk -max -0.500 [get_ports {master_rx_data2[*]}]

set_output_delay -clock mac_clk -min 4.000 [get_ports master_tx_pause2]
set_output_delay -clock mac_clk -max -0.500 [get_ports master_tx_pause2]

set_property IOSTANDARD LVCMOS18 [get_ports master_tx_pause2]
set_property PACKAGE_PIN AJ16 [get_ports master_tx_pause2]

set_property IOSTANDARD LVCMOS18 [get_ports {master_rx_data2[*]}]

set_property PACKAGE_PIN AK17 [get_ports {master_rx_data2[0]}]
set_property PACKAGE_PIN AL18 [get_ports {master_rx_data2[1]}]
set_property PACKAGE_PIN AP16 [get_ports {master_rx_data2[2]}]
set_property PACKAGE_PIN AN17 [get_ports {master_rx_data2[3]}]
set_property PACKAGE_PIN AN18 [get_ports {master_rx_data2[4]}]
set_property PACKAGE_PIN AM16 [get_ports {master_rx_data2[5]}]
set_property PACKAGE_PIN AM15 [get_ports {master_rx_data2[6]}]
set_property PACKAGE_PIN AL14 [get_ports {master_rx_data2[7]}]
set_property PACKAGE_PIN AP14 [get_ports {master_rx_data2[8]}]
set_property PACKAGE_PIN AP15 [get_ports {master_rx_data2[9]}]



####### Signals coming from FPGA3
set_property IOSTANDARD LVCMOS18 [get_ports {master_tx_data3[*]}]

set_property PACKAGE_PIN H32 [get_ports {master_tx_data3[0]}]
set_property PACKAGE_PIN J30 [get_ports {master_tx_data3[1]}]
set_property PACKAGE_PIN H33 [get_ports {master_tx_data3[2]}]
set_property PACKAGE_PIN G28 [get_ports {master_tx_data3[3]}]
set_property PACKAGE_PIN G30 [get_ports {master_tx_data3[4]}]
set_property PACKAGE_PIN K32 [get_ports {master_tx_data3[5]}]
set_property PACKAGE_PIN K31 [get_ports {master_tx_data3[6]}]
set_property PACKAGE_PIN K34 [get_ports {master_tx_data3[7]}]
set_property PACKAGE_PIN L30 [get_ports {master_tx_data3[8]}]
set_property PACKAGE_PIN L33 [get_ports {master_tx_data3[9]}]

######### Signals going to FPGA3

set_output_delay -clock mac_clk -min 4.000 [get_ports {master_rx_data3[*]}]
set_output_delay -clock mac_clk -max -0.500 [get_ports {master_rx_data3[*]}]

set_output_delay -clock mac_clk -min 4.000 [get_ports master_tx_pause3]
set_output_delay -clock mac_clk -max -0.500 [get_ports master_tx_pause3]



set_property IOSTANDARD LVCMOS18 [get_ports master_tx_pause3]
set_property PACKAGE_PIN H29 [get_ports master_tx_pause3]

set_property IOSTANDARD LVCMOS18 [get_ports {master_rx_data3[*]}]

set_property PACKAGE_PIN K33 [get_ports {master_rx_data3[0]}]
set_property PACKAGE_PIN G32 [get_ports {master_rx_data3[1]}]
set_property PACKAGE_PIN J34 [get_ports {master_rx_data3[2]}]
set_property PACKAGE_PIN J32 [get_ports {master_rx_data3[3]}]
set_property PACKAGE_PIN G31 [get_ports {master_rx_data3[4]}]
set_property PACKAGE_PIN H34 [get_ports {master_rx_data3[5]}]
set_property PACKAGE_PIN L34 [get_ports {master_rx_data3[6]}]
set_property PACKAGE_PIN J31 [get_ports {master_rx_data3[7]}]
set_property PACKAGE_PIN F29 [get_ports {master_rx_data3[8]}]
set_property PACKAGE_PIN H30 [get_ports {master_rx_data3[9]}]


#########Signals coming from FPGA4
set_property IOSTANDARD LVCMOS18 [get_ports {master_tx_data4[*]}]

set_property PACKAGE_PIN J10 [get_ports {master_tx_data4[0]}]
set_property PACKAGE_PIN K11 [get_ports {master_tx_data4[1]}]
set_property PACKAGE_PIN M10 [get_ports {master_tx_data4[2]}]
set_property PACKAGE_PIN K12 [get_ports {master_tx_data4[3]}]
set_property PACKAGE_PIN M13 [get_ports {master_tx_data4[4]}]
set_property PACKAGE_PIN H10 [get_ports {master_tx_data4[5]}]
set_property PACKAGE_PIN L13 [get_ports {master_tx_data4[6]}]
set_property PACKAGE_PIN L9 [get_ports {master_tx_data4[7]}]
set_property PACKAGE_PIN M11 [get_ports {master_tx_data4[8]}]
set_property PACKAGE_PIN H12 [get_ports {master_tx_data4[9]}]

######### Signals going to FPGA4

set_output_delay -clock mac_clk -min 4.000 [get_ports {master_rx_data4[*]}]
set_output_delay -clock mac_clk -max -0.500 [get_ports {master_rx_data4[*]}]

set_output_delay -clock mac_clk -min 4.000 [get_ports master_tx_pause4]
set_output_delay -clock mac_clk -max -0.500 [get_ports master_tx_pause4]

set_property IOSTANDARD LVCMOS18 [get_ports master_tx_pause4]
set_property PACKAGE_PIN H8 [get_ports master_tx_pause4]

set_property IOSTANDARD LVCMOS18 [get_ports {master_rx_data4[*]}]

set_property PACKAGE_PIN L15 [get_ports {master_rx_data4[0]}]
set_property PACKAGE_PIN H15 [get_ports {master_rx_data4[1]}]
set_property PACKAGE_PIN K13 [get_ports {master_rx_data4[2]}]
set_property PACKAGE_PIN L11 [get_ports {master_rx_data4[3]}]
set_property PACKAGE_PIN M16 [get_ports {master_rx_data4[4]}]
set_property PACKAGE_PIN L10 [get_ports {master_rx_data4[5]}]
set_property PACKAGE_PIN J12 [get_ports {master_rx_data4[6]}]
set_property PACKAGE_PIN J9 [get_ports {master_rx_data4[7]}]
set_property PACKAGE_PIN K9 [get_ports {master_rx_data4[8]}]
set_property PACKAGE_PIN M12 [get_ports {master_rx_data4[9]}]



#######################################################

#IO DELAY constraints
set_property IODELAY_GROUP io_delay_control_in_group [get_cells ipbus_tx*/inst/pins[*].idelaye2_bus]
set_property IODELAY_GROUP io_delay_control_in_group [get_cells ipbus_rx*/inst/pins[*].odelaye2_bus]

#set_property IOB TRUE [get_ports SYNC_B_CDCE]

set_property PACKAGE_PIN R29 [get_ports fpga1_done]
set_property IOSTANDARD LVCMOS18 [get_ports fpga1_done]
set_property PACKAGE_PIN R28 [get_ports fpga2_done]
set_property IOSTANDARD LVCMOS18 [get_ports fpga2_done]
set_property PACKAGE_PIN P29 [get_ports fpga3_done]
set_property IOSTANDARD LVCMOS18 [get_ports fpga3_done]
set_property PACKAGE_PIN P30 [get_ports fpga4_done]
set_property IOSTANDARD LVCMOS18 [get_ports fpga4_done]
