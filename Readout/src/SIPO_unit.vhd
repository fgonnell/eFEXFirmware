--! @file
--! @brief XTOB SIPO for process FPGA
--! @details 
--! This module convert 5x520b e/g XTOBs into one 2560b XTObs and 40b Valid signals for Circular DPRAM
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library algolib;
use algolib.AlgoDataTypes.all;

library Infrastructure_lib;
 
--! @copydoc SIPO_unit.vhd
entity SIPO_unit is
    Port ( 
        XTOB_in          : in STD_LOGIC_VECTOR (33 downto 0);  -- 33b XTOBs from 1 algo block         
        XTOB_valid_in    : in STD_LOGIC ;     -- 1B valid signal 
        XTOB_sync_in     : in STD_LOGIC;
       --! sorted XTOB BCN with delay through ALGO/sorting block
        ALGO_XTOB_BCN_in : in std_logic_vector(6 downto 0);
        clk_in           : in STD_LOGIC;
        XTOB_sync_out    : out STD_LOGIC;
        --! XTOBs valid signals 5b
        XTOB_valid_out   : out STD_LOGIC_VECTOR (4 downto 0);   
        --! 5 XTOBs (34b) * 5 = 170b
        XTOB_out         : out STD_LOGIC_VECTOR (169 downto 0);
        --! sorted XTOB BCN with delay through SIPO block   
        ALGO_XTOB_BCN_out      : out std_logic_vector(6 downto 0)
    );
end SIPO_unit;

--! @copydoc SIPO_unit.vhd
architecture Behavioral of SIPO_unit is
    signal  reg_1_tmp        : std_logic_vector(33 downto 0);  -- 34b XTOB
    signal  reg_2_tmp        : std_logic_vector(33 downto 0);
    signal  reg_3_tmp        : std_logic_vector(33 downto 0);
    signal  reg_4_tmp        : std_logic_vector(33 downto 0);
    signal  reg_5_tmp        : std_logic_vector(33 downto 0);
    
    signal  valid_1_tmp      : std_logic;    -- 1b valid XTOB
    signal  valid_2_tmp      : std_logic;
    signal  valid_3_tmp      : std_logic;
    signal  valid_4_tmp      : std_logic;
    signal  valid_5_tmp      : std_logic;
    
    signal  sync_1_tmp       : std_logic;
    signal  sync_2_tmp       : std_logic;
    signal  sync_3_tmp       : std_logic;
    signal  sync_4_tmp       : std_logic;
    signal  sync_5_tmp       : std_logic;

--  ####### attribute for signals  ########
    attribute max_fanout : integer;
    attribute keep       : string ;
    attribute keep of       sync_1_tmp    : signal is "true" ;
    attribute max_fanout of sync_1_tmp    : signal is 60;
    attribute keep of       XTOB_sync_out : signal is "true" ;
    attribute max_fanout of XTOB_sync_out : signal is 60;

BEGIN

U1_proc : process (clk_in)
    begin
        if(clk_in'event and clk_in = '1') then
           sync_5_tmp <= XTOB_sync_in;       -- register input sync data
           sync_4_tmp <= sync_5_tmp;      
           sync_3_tmp <= sync_4_tmp;      
           sync_2_tmp <= sync_3_tmp;      
           sync_1_tmp <= sync_2_tmp;
                 
           reg_5_tmp  <= XTOB_in ;         -- register input data 
           reg_4_tmp <= reg_5_tmp;       
           reg_3_tmp <= reg_4_tmp;       
           reg_2_tmp <= reg_3_tmp;       
           reg_1_tmp <= reg_2_tmp;       
    
           valid_5_tmp  <= XTOB_valid_in ;  -- register TOB valid data
           valid_4_tmp  <= valid_5_tmp;       
           valid_3_tmp  <= valid_4_tmp;       
           valid_2_tmp  <= valid_3_tmp;       
           valid_1_tmp  <= valid_2_tmp;       

-- moved following statements into the process so ad 1 clk cycle pipeline
           XTOB_out        <= reg_5_tmp & reg_4_tmp & reg_3_tmp & reg_2_tmp & reg_1_tmp;
           XTOB_valid_out  <= valid_5_tmp & valid_4_tmp & valid_3_tmp & valid_2_tmp & valid_1_tmp;
           XTOB_sync_out   <= sync_1_tmp;

        end if;
    end process;

--U2_proc : process (clk_in_280M)
--    begin
--        if(clk_in_280M'event and clk_in_280M = '1') then
--            if XTOB_sync_in = '1' then
--                 ALGO_XTOB_BCN_out <= ALGO_XTOB_BCN_in ;   -- no need to delay more than 1 clk as stays constant for 7 clks
--            end if;
--        end if;
--    end process U2_proc;

U2_XTOB_BCN_Delay : entity Infrastructure_lib.GeneralDelay
    generic map (
      delay => 2,   --generates delay of 2 + 1 clocks
      size  => 7)
    port map (
      clk      => clk_in,
      data_in  => ALGO_XTOB_BCN_in,
      data_out => ALGO_XTOB_BCN_out);

    
end Behavioral;
