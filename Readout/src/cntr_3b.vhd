--! @file
--! @brief 3b counter
--! @details 
--! This counter counts from 0 to 6 and resets to 0,
--! it is used to count the number of 32b calorimeter data words.
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.all;
library TOB_rdout_lib;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cntr_3b is
    Port ( 
           RST : in STD_LOGIC;
           CLK : in STD_LOGIC;
           CE : in STD_LOGIC;
           TC : out STD_LOGIC;
           Q : out STD_LOGIC_VECTOR (2 downto 0)
           );
end cntr_3b;

architecture Behavioral of cntr_3b is
    constant temp_i : unsigned := "110" ;   -- forces counter to count only 7 clks

begin

process (CLK)
	variable temp      : unsigned (2 downto 0):= (others => '0') ;
    variable  TC_i      : std_logic;	
       
   begin
	if CLK'event AND CLK = '1' then
     	if RST = '1' then
     	  TC_i := '0' ;                      -- this is a synchronous reset
      	  temp := "000";   -- set terminal count value given.
		elsif CE = '1' then
			if temp = temp_i then	     -- if count = terminal count then
				temp := "000";			    -- set TC 
				TC_i := '1' ;
			else	
				temp := temp + 1;
				TC_i := '0' ;			          -- else increment the value by one 
			end if;
		end if;
	end if;
	Q <= std_logic_vector(temp);	 -- push out new value
	TC <= TC_i ;
	end process;

end Behavioral;
