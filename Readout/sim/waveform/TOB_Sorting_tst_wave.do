onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {TOB Sorting SIPO}
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/L1A_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOB_FIFO_sw_rst
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOBs_32b_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOBs_sync_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOBs_valid_flg_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/ALGO_TOB_BCN_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/FIFO_rd_en
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/link_err_4b_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/clk_280M_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/pre_ld_wr_addr_TOB
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/DPR_locations_to_rd
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOBs_FIFO_pFULL_THRESH_assert
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOBs_FIFO_pFULL_THRESH_negate
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/rdout_T_TOB_209b
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOBs_FIFO_data_count
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOBs_data_full
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOBs_data_empty
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOBs_data_valid
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOBs_data_prog_full
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/clk_in_280M_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/SIPO_T_TOB_out_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/SIPO_T_TOB_sync_out_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/T_TOB_valid_out_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/DPR_T_TOBs_in_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/DPR_T_TOBs_out_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/rdout_T_TOB_209b_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/DPR_T_TOB_rd_addr_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/DPR_T_TOB_wr_addr_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/DRP_T_TOB_rd_en_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/SIPO_T_TOB_sync_vec_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/L1A_in_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/FIFO_wr_en_tmp
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/ALGO_TOB_BCN_out_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/link_err_4b_out_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOBs_FIFO_data_count_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/tied_to_vcc_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOBs_data_valid_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOBs_data_prog_full_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOBs_data_full_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOBs_data_empty_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOB_BCID_in_ila
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/TOB_BCID_out_ila
add wave -noupdate -divider {TOB Sorting FSM}
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/CLK_IN
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/TOB_FIFO_sw_rst
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/L1A_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/SIPO_sync_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/pre_ld_wr_addr
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/DPR_locations_to_rd
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/DPR_rd_addr
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/DPR_wr_addr
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/DRP_rd_en
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/FIFO_wr_en
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/FIFO_wr_en_tmp
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/DPR_rd_addr_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/DPR_rd_addr_tmp
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/slice_count_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/current_state
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U3_TOBs_wr_FSM/count
add wave -noupdate -divider {TOB Sorting DPRAM}
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U4_T_TOB_DRP/clka
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U4_T_TOB_DRP/ena
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U4_T_TOB_DRP/wea
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U4_T_TOB_DRP/addra
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U4_T_TOB_DRP/dina
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U4_T_TOB_DRP/clkb
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U4_T_TOB_DRP/enb
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U4_T_TOB_DRP/addrb
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U4_T_TOB_DRP/doutb
add wave -noupdate -divider {TOB Sorting FIFO}
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U5_T_TOBs_fifo/clk
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U5_T_TOBs_fifo/din
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U5_T_TOBs_fifo/wr_en
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U5_T_TOBs_fifo/rd_en
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U5_T_TOBs_fifo/prog_full_thresh_assert
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U5_T_TOBs_fifo/prog_full_thresh_negate
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U5_T_TOBs_fifo/dout
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U5_T_TOBs_fifo/full
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U5_T_TOBs_fifo/empty
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U5_T_TOBs_fifo/valid
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U5_T_TOBs_fifo/data_count
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U1_TOBs_sorting/U5_T_TOBs_fifo/prog_full
add wave -noupdate -divider {BCN FIFO}
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U0_FIFO_BCN_L1A/rst
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U0_FIFO_BCN_L1A/wr_clk
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U0_FIFO_BCN_L1A/wr_en
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U0_FIFO_BCN_L1A/din
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U0_FIFO_BCN_L1A/rd_clk
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U0_FIFO_BCN_L1A/rd_en
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U0_FIFO_BCN_L1A/dout
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U0_FIFO_BCN_L1A/valid
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U0_FIFO_BCN_L1A/empty
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U0_FIFO_BCN_L1A/rd_data_count
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U0_FIFO_BCN_L1A/prog_full_thresh_assert
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U0_FIFO_BCN_L1A/prog_full_thresh_negate
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U0_FIFO_BCN_L1A/full
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U0_FIFO_BCN_L1A/prog_full
add wave -noupdate -divider {RD MUX FSM}
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/tst_fsm_cntr
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/TOB_FIFO_sw_rst
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/hw_addr
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/rdout_T_TOB_209b_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/valid_T_TOB_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/TOB_type_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/XTOBs_eg_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/valid_XTOBs_eg_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/XTOBs_tau_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/valid_XTOBs_tau_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/clk_280M_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/TOB_FIFO_empty_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/TOB_prog_full_flag_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/DPR_locations_to_rd
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/rdout_expected
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/FIFO_BCN_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/FIFO_L1A_ID
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/FIFO_L1A_ID_EXT
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/BCN_FIFO_valid_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/BCN_FIFO_valid_in_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/LO_FIFO_prog_full_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/LO_FIFO_data_count_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/frame_cntr_en
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/BCN_FIFO_rd_en_out
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/TOB_rdout_fifo_rd_en_out
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/corrective_trailer
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/slice_count_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/link_err_flg_in_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/TOB_BCN_in_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/FIFO_BCN_in_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/XTOB_eg_BCN_in_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/XTOB_tau_BCN_in_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/FIFO_L1A_ID_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/FIFO_L1A_ID_EXT_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/write_in_LO_FIFO_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/CLK_280M_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/cntr_rst_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/TOB_cntr_rst
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/T_TOB_eg_cntr_en_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/T_TOB_cntr_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/T_TOB_cntr_1dly
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/XTOB_eg_cntr_en_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/XTOB_eg_cntr_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/XTOB_eg_cntr_1dly
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/XTOB_tau_cntr_en_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/XTOB_tau_cntr_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/XTOB_tau_cntr_1dly
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/trailer_cntr_en
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/payld_cntr_rst
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/payld_cntr_rst_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/payld_cntr_en
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/TOB_payld_cntr_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/TOB_payld_cntr_1dly
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/frame_cntr_en_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/slice_no_txed
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/T_TOB_eg_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/T_TOB_eg_valid_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/valid_T_TOB_in_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/valid_XTOBs_eg_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/XTOB_eg_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/XTOB_eg_valid_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/XTOB_tau_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/XTOB_tau_valid_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/safe_mode_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/miss_sop_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/wr_en_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/current_state
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/BCN_FIFO_rd_en_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/TOBs_out_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/TOB_out_valid_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/TOB_out_is_char_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/TOBs_out
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/TOB_out_valid
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/TOB_out_is_char
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/j
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/k
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/n
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/m
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U6_rd_mux_fsm/m_s_count
add wave -noupdate -divider {link o/p FIFO}
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U7_Link_output_FIFO/clk
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U7_Link_output_FIFO/srst
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U7_Link_output_FIFO/din
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U7_Link_output_FIFO/wr_en
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U7_Link_output_FIFO/rd_en
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U7_Link_output_FIFO/prog_full_thresh_assert
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U7_Link_output_FIFO/prog_full_thresh_negate
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U7_Link_output_FIFO/dout
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U7_Link_output_FIFO/full
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U7_Link_output_FIFO/empty
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U7_Link_output_FIFO/valid
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U7_Link_output_FIFO/data_count
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U7_Link_output_FIFO/prog_full
add wave -noupdate -divider {SPY RAM}
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U12_TOB_SPY_mem/clk
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U12_TOB_SPY_mem/rst
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U12_TOB_SPY_mem/ipb_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U12_TOB_SPY_mem/ipb_out
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U12_TOB_SPY_mem/we
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U12_TOB_SPY_mem/d
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U12_TOB_SPY_mem/q
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U12_TOB_SPY_mem/addr
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U12_TOB_SPY_mem/sel
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U12_TOB_SPY_mem/rsel
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U12_TOB_SPY_mem/ack
add wave -noupdate -divider {SPY RAM wr addr}
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U13_spy_mem_wr_addr/CE
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U13_spy_mem_wr_addr/CLK
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U13_spy_mem_wr_addr/RST
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U13_spy_mem_wr_addr/Q
add wave -noupdate -divider {FIFO to MGT FSM}
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/TOB_FIFO_sw_rst
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/ctrl_TOB_ready_in
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/frame_counter
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/FIFO_MGT_TOBs
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/FIFO_MGT_TOB_valid
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/MGT_FIFO_full
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/MGT_FIFO_empty
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/MGT_FIFO_prog_full
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/frame_counter_dec_en
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/clk_in_280M_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/frame_counter_dec_en_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/TOBs_in_tmp_1dly
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/TOB_in_is_char_tmp_1dly
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/TOBs_in_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/TOB_in_is_char_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/T_TOB_out_valid_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/current_state
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/MGT_fifo_rd_en_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/FIFO_MGT_rd_en
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/TOBs_out_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/TOB_out_is_char_i
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/T_TOBs_out
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/T_TOB_is_char
add wave -noupdate /readout_data_tb/U12_TOBs_readout/U8_TOB_Link_output_FIFO_FSM/T_TOB_out_valid
add wave -noupdate -divider {New Divider}
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {6300000000 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 384
configure wave -valuecolwidth 189
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {5971874992 fs} {6628125008 fs}
