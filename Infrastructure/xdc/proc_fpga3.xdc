###### This is XDC file for processing FPGA 3 ###########
create_clock -period 8.000  -name gt_clk125_p [get_ports gt_clk125_p]

###### Ethernet RefClk (125MHz) bank17 (pdf pag.44)
set_property IOSTANDARD LVDS [get_ports gt_clk125_p]
set_property PACKAGE_PIN K24 [get_ports gt_clk125_n] 
set_property PACKAGE_PIN K23 [get_ports gt_clk125_p]
set_property IOSTANDARD LVDS [get_ports gt_clk125_n]
set_property DIFF_TERM TRUE [get_ports gt_clk125_p]
set_property DIFF_TERM TRUE [get_ports gt_clk125_n]

create_generated_clock -name mac_clk [get_pins clock_resources/clk_gen/inst/mmcm_adv_inst/CLKOUT0]
create_generated_clock -name ipb_clk [get_pins clock_resources/clk_gen/inst/mmcm_adv_inst/CLKOUT1]

set_clock_groups -asynchronous -group [get_clocks ipb_clk]
set_clock_groups -asynchronous -group [get_clocks mac_clk]

###### Creating delay for the processing FPGA
#set_input_delay  -clock mac_clk -min 0.000 [get_ports {master_rx_data[*]}]
#set_input_delay -clock mac_clk -max 2.000 [get_ports {master_rx_data[*]}]
#set_input_delay -clock mac_clk -min 0.000 [get_ports master_tx_pause]
#set_input_delay -clock mac_clk -max 2.000 [get_ports master_tx_pause]
#
#set_output_delay -clock mac_clk -min 4.000 [get_ports {master_tx_data[*]}]
#set_output_delay -clock mac_clk -max -0.500 [get_ports {master_tx_data[*]}]

###### SPI generated clock
create_generated_clock -name flash_spi_clk -source [get_pins slaves/spi_flash/spi_clk_reg/C] -divide_by 2 [get_pins slaves/spi_flash/spi_clk_reg/Q]

###### FPGA1_GCLK1_40M08 Bank 19 (pdf pag.44)
set_property IOSTANDARD LVDS [get_ports ttc_clk_p]
set_property IOSTANDARD LVDS [get_ports ttc_clk_n]
set_property PACKAGE_PIN K27 [get_ports ttc_clk_p]
set_property PACKAGE_PIN J27 [get_ports ttc_clk_n]
set_property DIFF_TERM TRUE [get_ports ttc_clk_p]
set_property DIFF_TERM TRUE [get_ports ttc_clk_n]

###### SPI Flash Pins
set_property PACKAGE_PIN BA29 [get_ports flash_csn]
set_property IOSTANDARD LVCMOS18 [get_ports flash_csn]
set_property PACKAGE_PIN BC27 [get_ports flash_mosi]
set_property IOSTANDARD LVCMOS18 [get_ports flash_mosi]
set_property PACKAGE_PIN BD27 [get_ports flash_miso]
set_property IOSTANDARD LVCMOS18 [get_ports flash_miso]

###### LEDs   #########################
set_property IOSTANDARD LVCMOS18 [get_ports flash_led]
set_property PACKAGE_PIN AP29 [get_ports flash_led]

###### F3 Geographyical address
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_geo_addr[*]}]
set_property PACKAGE_PIN T24 [get_ports {fpga_geo_addr[0]}]
set_property PACKAGE_PIN T25 [get_ports {fpga_geo_addr[1]}]

###### Hardware address   Pins
set_property IOSTANDARD LVCMOS18 [get_ports {hardware_addr[*]}]
set_property PACKAGE_PIN AM27 [get_ports {hardware_addr[0]}]
set_property PACKAGE_PIN AK29 [get_ports {hardware_addr[1]}]
set_property PACKAGE_PIN AR28 [get_ports {hardware_addr[2]}]
set_property PACKAGE_PIN AN28 [get_ports {hardware_addr[3]}]
set_property PACKAGE_PIN AK28 [get_ports {hardware_addr[4]}]
set_property PACKAGE_PIN AN30 [get_ports {hardware_addr[5]}]
set_property PACKAGE_PIN AM30 [get_ports {hardware_addr[6]}]
set_property PACKAGE_PIN AN29 [get_ports {hardware_addr[7]}]

###### Tx and Rx signal between FPGA1 and Control FPGA ##########
set_property IOSTANDARD LVCMOS18 [get_ports {master_tx_data[*]}]

set_property PACKAGE_PIN  B23   [get_ports {master_tx_data[0]}]
set_property PACKAGE_PIN  C25   [get_ports {master_tx_data[1]}]
set_property PACKAGE_PIN  C23   [get_ports {master_tx_data[2]}]
set_property PACKAGE_PIN  A24   [get_ports {master_tx_data[3]}]
set_property PACKAGE_PIN  D24   [get_ports {master_tx_data[4]}]
set_property PACKAGE_PIN  E23   [get_ports {master_tx_data[5]}]
set_property PACKAGE_PIN  G23   [get_ports {master_tx_data[6]}]
set_property PACKAGE_PIN  F24   [get_ports {master_tx_data[7]}]
set_property PACKAGE_PIN  H23   [get_ports {master_tx_data[8]}]
set_property PACKAGE_PIN  G25   [get_ports {master_tx_data[9]}]

set_property IOSTANDARD LVCMOS18 [get_ports master_tx_pause]
set_property PACKAGE_PIN AV18 [get_ports master_tx_pause]

set_property IOSTANDARD LVCMOS18 [get_ports {master_rx_data[*]}]

set_property PACKAGE_PIN  A23  [get_ports {master_rx_data[0]}]
set_property PACKAGE_PIN  B25  [get_ports {master_rx_data[1]}]
set_property PACKAGE_PIN  C24  [get_ports {master_rx_data[2]}]
set_property PACKAGE_PIN  A25  [get_ports {master_rx_data[3]}]
set_property PACKAGE_PIN  D25  [get_ports {master_rx_data[4]}]
set_property PACKAGE_PIN  E24  [get_ports {master_rx_data[5]}]
set_property PACKAGE_PIN  F23  [get_ports {master_rx_data[6]}]
set_property PACKAGE_PIN  F25  [get_ports {master_rx_data[7]}]
set_property PACKAGE_PIN  H24  [get_ports {master_rx_data[8]}]
set_property PACKAGE_PIN  G26  [get_ports {master_rx_data[9]}]

###### master tx and rx
set_property IOB TRUE [get_ports {master_rx_data[*]}]
set_property IOB TRUE [get_ports {master_tx_data[*]}]
set_property IOB TRUE [get_ports master_tx_pause]

###### Control FPGA Ready signal inputs
# F1_F5_RDOUT_0_p pin is used for RAW
# F1_F5_RDOUT_0_n pin is used for TOB
set_property IOSTANDARD LVCMOS18 [get_ports {ctrl_RAW_ready_in}]
set_property IOSTANDARD LVCMOS18 [get_ports {ctrl_TOB_ready_in}]
set_property PACKAGE_PIN B21 [get_ports {ctrl_RAW_ready_in}]
set_property PACKAGE_PIN A21 [get_ports {ctrl_TOB_ready_in}]

###### ECRID from F5 to F3  TTC_5_p TO TTC_8_n
set_property IOSTANDARD LVCMOS18 [get_ports {ECRID[*]}]
set_property IOB TRUE [get_ports {ECRID[*]}]
set_property PACKAGE_PIN D26 [get_ports {ECRID[0]}]
set_property PACKAGE_PIN D27 [get_ports {ECRID[1]}]
set_property PACKAGE_PIN G28 [get_ports {ECRID[2]}]
set_property PACKAGE_PIN F28 [get_ports {ECRID[3]}]
set_property PACKAGE_PIN F26 [get_ports {ECRID[4]}]
set_property PACKAGE_PIN E26 [get_ports {ECRID[5]}]
set_property PACKAGE_PIN F29 [get_ports {ECRID[6]}]
set_property PACKAGE_PIN E29 [get_ports {ECRID[7]}]

###### TTC information signals
set_property IOSTANDARD LVDS [get_ports {ttc_inform_n[*]}]
set_property IOSTANDARD LVDS [get_ports {ttc_inform_p[*]}]
set_property DIFF_TERM true  [get_ports {ttc_inform_p[*]}]
set_property DIFF_TERM true  [get_ports {ttc_inform_n[*]}]
set_property PACKAGE_PIN A28 [get_ports {ttc_inform_p[0]}]
set_property PACKAGE_PIN D29 [get_ports {ttc_inform_p[1]}]
set_property PACKAGE_PIN C27 [get_ports {ttc_inform_p[2]}]
set_property PACKAGE_PIN C28 [get_ports {ttc_inform_p[3]}]
set_property IOB TRUE [get_ports {ttc_inform_n[*]}]
set_property IOB TRUE [get_ports {ttc_inform_p[*]}]
