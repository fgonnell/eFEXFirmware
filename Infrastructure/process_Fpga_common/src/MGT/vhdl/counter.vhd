--! @file
--! @brief Counter with double enable
--! @details 
--! If the maximum is reache dthe counter will hold its value
--! @author Francesco Gonnella
--! @author Mohammes Siyad
--! @author Saeed Taghavi
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity counter is
  generic (DEPTH : integer := 16);
  port (
    clk     : in  std_logic;
    enable1 : in  std_logic;
    enable2 : in  std_logic := '1';
    count   : out std_logic_vector (DEPTH - 1 downto 0);
    reset   : in  std_logic
    );

end counter;

architecture Behavioral of counter is
  signal cntr                          : unsigned (DEPTH-1 downto 0);
  signal enable2_i, enable1_i, RESET_i : std_logic;     -- added by ST to remove timing error 18.03.2020
  constant ones                        : unsigned(DEPTH-1 downto 0) := (others => '1');
begin
  process(clk)
  begin
    if clk' event and clk = '1' then
      enable2_i <= enable2;
      RESET_i   <= RESET;
      enable1_i <= enable1;
      if RESET_i = '1' then
        cntr <= (others => '0');
      elsif (enable1_i = '1' and enable2_i = '1') then
        if cntr = ones then
          cntr <= cntr;
        else
          cntr <= cntr + 1;
        end if;
      else
        cntr <= cntr;
      end if;
    end if;
  end process;

  count <= std_logic_vector(cntr);

end Behavioral;
