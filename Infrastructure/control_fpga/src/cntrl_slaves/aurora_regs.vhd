Library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;
use ipbus_lib.ipbus_reg_types.all;
library infrastructure_lib;
use infrastructure_lib.ipbus_decode_efex_aurora.all;

--! @copydoc aurora_registers.vhd
entity aurora_registers is
    
port(
         ipb_clk    : in std_logic;
         ipb_rst    : in std_logic;
	     ipb_in     : in ipb_wbus;
	     ipb_out    : out ipb_rbus;
	     aurora_status_1  : in std_logic_vector(31 downto 0);
         aurora_status_2  : in std_logic_vector(31 downto 0)
         
	);
	
end aurora_registers;

architecture Behavioral of aurora_registers is

signal ipbw: ipb_wbus_array(N_SLAVES-1 downto 0);
signal ipbr, ipbr_d: ipb_rbus_array(N_SLAVES-1 downto 0);
begin



fabric_common_IDversion: entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV => N_SLAVES, --defined in ipbus_decode_common_IdVersion
	             SEL_WIDTH => ipbus_sel_width)
          port map(
             ipb_in => ipb_in,
             ipb_out => ipb_out,
             sel => ipbus_sel_efex_aurora (ipb_in.ipb_addr),
             ipb_to_slaves => ipbw,
             ipb_from_slaves => ipbr
           ); 

status_aurora_1: entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,              --number of control reg         
      N_STAT => 1)              --number of status reg          
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_AURORA_STATUS_1),
      ipbus_out => ipbr(N_SLV_AURORA_STATUS_1),
      d         => (0 => aurora_status_1),
      q         => open,
      stb       => open);


status_aurora_2: entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 0,              --number of control reg         
      N_STAT => 1)              --number of status reg          
    port map(
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_AURORA_STATUS_2),
      ipbus_out => ipbr(N_SLV_AURORA_STATUS_2),
      d         => (0 => aurora_status_2),
      q         => open,
      stb       => open);


end Behavioral;