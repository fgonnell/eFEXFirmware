-- generalised from: ipbus_dpram
-- F. Gonnella, November 2016
--


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
library ipbus_lib;

-- To compile within FTM firmware:
-- 1) comment the following line
-- 2) uncomment the following 2 lines

use ipbus_lib.ipbus.all;                --comment for FTM

--library xil_defaultlib;                 --uncomment for FTM
--use xil_defaultlib.ipbus.all;           -- uncomment for FTM

use work.DataTypes.all;

entity ipbus_dpram_wrapper is
  port(
    clk_ipb : in  std_logic;
    rst     : in  std_logic;
    ipb_in  : in  ipb_wbus;
    ipb_out : out ipb_rbus;
    rclk    : in  std_logic;
    we      : in  std_logic := '0';
    q       : out std_logic_vector(PARAMETER_RAM_DATA_WIDTH-1 downto 0);
    addr    : in  std_logic_vector(2 downto 0)
    );

end ipbus_dpram_wrapper;

architecture rtl of ipbus_dpram_wrapper is

  component AlgoParameterRAM
    port (
      clka  : in  std_logic;
      ena   : in  std_logic;
      wea   : in  std_logic_vector(0 downto 0);
      addra : in  std_logic_vector(PARAMETER_RAM_ADDR_WIDTH-1 downto 0);
      dina  : in  std_logic_vector(31 downto 0);
      douta : out std_logic_vector(31 downto 0);
      clkb  : in  std_logic;
      enb   : in  std_logic;
      web   : in  std_logic_vector(0 downto 0);
      addrb : in  std_logic_vector(2 downto 0);
      dinb  : in  std_logic_vector(PARAMETER_RAM_DATA_WIDTH-1 downto 0);
      doutb : out std_logic_vector(PARAMETER_RAM_DATA_WIDTH-1 downto 0)
      );
  end component;


--RAM signals
  signal ram_ena   : std_logic;
  signal ram_wea   : std_logic_vector(0 downto 0);
  signal ram_addra : std_logic_vector(PARAMETER_RAM_ADDR_WIDTH-1 downto 0);
  signal ram_dina  : std_logic_vector(31 downto 0);
  signal ram_douta : std_logic_vector(31 downto 0);

  signal ram_clkb  : std_logic;
  signal ram_enb   : std_logic;
  signal ram_web   : std_logic_vector(0 downto 0);
  signal ram_addrb : std_logic_vector(2 downto 0);
  signal ram_dinb  : std_logic_vector(PARAMETER_RAM_DATA_WIDTH-1 downto 0);
  signal ram_doutb : std_logic_vector(PARAMETER_RAM_DATA_WIDTH-1 downto 0);

  --ipbus signals
  signal ack          : std_logic;
  signal ack2         : std_logic;
--  signal ack3        : std_logic;
  signal ipbus_write  : std_logic_vector(0 downto 0);
  signal write_enable : std_logic_vector(0 downto 0);

begin
  ram_dinb <= (others => '0');

  IPBUS_RAM : process(clk_ipb)
  begin
    if rising_edge(clk_ipb) then
      if ipb_in.ipb_strobe = '1' and ipb_in.ipb_write = '1' then
        ipbus_write(0) <= '1';
      else
        ipbus_write(0) <= '0';
      end if;
      ack2 <= ipb_in.ipb_strobe and (not ack2) and (not ack);
--      ack2 <= ack3;
      ack  <= ack2;

    end if;
  end process;

  ipb_out.ipb_ack <= ack;
  ipb_out.ipb_err <= '0';


  write_enable(0) <= we;
  ALGO_PARAMETER_RAM : AlgoParameterRAM
    port map (
      clka  => clk_ipb,
      ena   => ipb_in.ipb_strobe,       --was '1' 
      wea   => ipbus_write,
      addra => ipb_in.ipb_addr(PARAMETER_RAM_ADDR_WIDTH-1 downto 0),
      dina  => ipb_in.ipb_wdata,
      douta => ipb_out.ipb_rdata,
      clkb  => rclk,
      enb   => '1',
      web   => write_enable,
      addrb => addr,
      dinb  => ram_dinb,
      doutb => q);
end rtl;
