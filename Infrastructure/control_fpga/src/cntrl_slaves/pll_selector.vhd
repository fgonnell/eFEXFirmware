----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Mohammed Siyad
-- 
-- Create Date: 10/22/2015 11:57:18 AM
-- Design Name: 
-- Module Name: pll_selector - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--  This will allow one PLL to be select in order to program  
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity pll_selector is
    Port ( sel : in STD_LOGIC_VECTOR (1 downto 0);
           pll_en_1 : out STD_LOGIC;
           pll_en_2 : out STD_LOGIC;
           pll_en_3 : out STD_LOGIC;
           pll_en : in STD_LOGIC);
end pll_selector;

architecture Behavioral of pll_selector is




begin
              
    pll_en_1 <= pll_en when sel = "00" else '1';
    pll_en_2 <= pll_en when sel = "01" else '1';
    pll_en_3 <= pll_en when sel = "10" else '1';          

end Behavioral;
