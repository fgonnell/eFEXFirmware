--! @file
--! @brief TOB synchronisation module
--! @details 
--! Delays each of the 4 input data bus by a number of clock cycles specified by offset.
--! The maximum delay is 63 clock cycles, the minimum is 0.
--! The IN_Start signal must be synchronous with input data 0 and will be delayed by Offset0.
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
use work.DataTypes.all;
use work.AlgoDataTypes.all;

--! @copydoc TOB_synch.vhd
entity TOB_synch is
  port (
    CLK        : in std_logic;
    IN_Offset0 : in std_logic_vector(5 downto 0);
    IN_Offset1 : in std_logic_vector(5 downto 0);
    IN_Offset2 : in std_logic_vector(5 downto 0);
    IN_Offset3 : in std_logic_vector(5 downto 0);

    IN_Enable0 : in std_logic;
    IN_Enable1 : in std_logic;
    IN_Enable2 : in std_logic;
    IN_Enable3 : in std_logic;    

    IN_Start  : in  std_logic;
    OUT_Start : out std_logic;

    IN_Data  : in  AlgoTriggerObjects(3 downto 0);
    OUT_Data : out AlgoTriggerObjects(3 downto 0));
end TOB_synch;

--! @copydoc TOB_synch.vhd
architecture Behavioral of TOB_synch is

  type array_of_trigger_objects is array(63 downto 0) of AlgoTriggerObjects(3 downto 0);
  signal DelayedData  : array_of_trigger_objects;
  signal DelayedStart : std_logic_vector(63 downto 0);


begin

  DelayedData(0)(0)  <= IN_Data(0) when IN_Enable0 = '1' else ZERO_ALGO_TRIGGER_OBJECT;
  DelayedData(0)(1)  <= IN_Data(1) when IN_Enable1 = '1' else ZERO_ALGO_TRIGGER_OBJECT;
  DelayedData(0)(2)  <= IN_Data(2) when IN_Enable2 = '1' else ZERO_ALGO_TRIGGER_OBJECT;
  DelayedData(0)(3)  <= IN_Data(3) when IN_Enable3 = '1' else ZERO_ALGO_TRIGGER_OBJECT;  

  
  DelayedStart(0) <= IN_Start;

  dalay_proc : process (CLK)
  begin
    if rising_edge(CLK) then
      DelayedData(DelayedData'high-1 downto 1)   <= DelayedData(DelayedData'high-2 downto 0);
      DelayedStart(DelayedStart'high-1 downto 1) <= DelayedStart(DelayedStart'high-2 downto 0);
    end if;

  end process;


  OUT_Data  <= (DelayedData(to_integer(unsigned(IN_Offset3)))(3), DelayedData(to_integer(unsigned(IN_Offset2)))(2), DelayedData(to_integer(unsigned(IN_Offset1)))(1), DelayedData(to_integer(unsigned(IN_Offset0)))(0));
  OUT_Start <= DelayedStart(to_integer(unsigned(IN_Offset0)));



end Behavioral;
