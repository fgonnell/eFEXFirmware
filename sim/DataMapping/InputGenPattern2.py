"""
Created on April 21 2020
@author Alexios Stampekis
Pattern 2: Create 64 BCs. For each BC all words of a single MGT
are "on", and equal to 1...20. Call it "an advanced" test pattern.
"""

import sys
import InputComponents as InC

def Run():
    MGTs=[]

    #------------------1st method: by using set_bc_word method----------------#
    
    for i in range(64):
        mgt=InC.MGT(i, 65) # plus one ("empty") BC to account for the offset
        for j in range(20):
            mgt.set_bc_word(i, j, j+1)     # arguments are: nbc, word#, word_value
            # In the Output Components module just do: InW[i,i,j]=j+1
        MGTs.append(mgt)
    
    #-----------------2nd method: by using set_bc_words method-----------------#
    """
    MyEnergies=[]
    for i in range(20):
        MyEnergies.append(i+1)
    for i in range(64):
        mgt=InC.MGT(i, 65)
        mgt.set_bc_words(i, MyEnergies)
        MGTs.append(mgt)
    """
    #------------------------------------------------------------------------#
 
    print("Writing to files")
    for v in MGTs:
        v.export_file()

    print("Done")


if __name__=="__main__":
    sys.exit(Run() or 0)

