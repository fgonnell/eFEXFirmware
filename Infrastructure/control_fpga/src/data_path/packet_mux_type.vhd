library ieee;
use ieee.std_logic_1164.all;

package packet_mux_type is
	type mgt_data_array is array(natural range <>) of std_logic_vector(31 downto 0);
	type packet_data_array is array(natural range <>) of std_logic_vector(63 downto 0);
	type packet_metadata_array is array(natural range <>) of std_logic_vector(71 downto 0);
	type packet_ttc_array is array(natural range <>) of std_logic_vector(43 downto 0);
	type packet_sl_array is array(natural range <>) of std_logic;
end package packet_mux_type;
