onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider Clocks
add wave -noupdate /tobs_data_rdout_tb/U0_clk_wiz_tst/clk_160M
add wave -noupdate /tobs_data_rdout_tb/U0_clk_wiz_tst/clk_200M
add wave -noupdate /tobs_data_rdout_tb/U0_clk_wiz_tst/clk_280M
add wave -noupdate /tobs_data_rdout_tb/U0_clk_wiz_tst/reset
add wave -noupdate /tobs_data_rdout_tb/U0_clk_wiz_tst/locked
add wave -noupdate /tobs_data_rdout_tb/U0_clk_wiz_tst/clk_in1
add wave -noupdate -divider RAW_data_array
add wave -noupdate -divider {Slave TOB readout}
add wave -noupdate /tobs_data_rdout_tb/slave_1/ipb_rst
add wave -noupdate /tobs_data_rdout_tb/slave_1/ipb_clk
add wave -noupdate /tobs_data_rdout_tb/slave_1/IPb_in
add wave -noupdate /tobs_data_rdout_tb/slave_1/IPb_out
add wave -noupdate /tobs_data_rdout_tb/slave_1/L1A_ID
add wave -noupdate /tobs_data_rdout_tb/slave_1/BCN_ID
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_WR_ADDR_OFFSET
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_SLICES_TO_RD
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_FIFO_FULL_THRESH_ASSERT
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_FIFO_FULL_THRESH_NEGATE
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_FIFO_DATA_COUNT
add wave -noupdate /tobs_data_rdout_tb/slave_1/XTOB_EG_FIFO_FULL_THRESH_ASSERT
add wave -noupdate /tobs_data_rdout_tb/slave_1/XTOB_EG_FIFO_FULL_THRESH_NEGATE
add wave -noupdate /tobs_data_rdout_tb/slave_1/XTOB_EG_FIFO_DATA_COUNT
add wave -noupdate /tobs_data_rdout_tb/slave_1/XTOB_TAU_FIFO_FULL_THRESH_ASSERT
add wave -noupdate /tobs_data_rdout_tb/slave_1/XTOB_TAU_FIFO_FULL_THRESH_NEGATE
add wave -noupdate /tobs_data_rdout_tb/slave_1/XTOB_TAU_FIFO_DATA_COUNT
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_Link_output_FIFO_FULL_THRESH_ASSERT
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_Link_output_FIFO_FULL_THRESH_NEGATE
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_LINK_OUTPUT_FIFO_RD_DATA_COUNT
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_LINK_OUTPUT_FIFO_WR_DATA_COUNT
add wave -noupdate /tobs_data_rdout_tb/slave_1/SPY_TOB_mem_wr_addr
add wave -noupdate /tobs_data_rdout_tb/slave_1/SPY_TOB_mem_rd_addr_en
add wave -noupdate /tobs_data_rdout_tb/slave_1/SPY_TOB_mem_rd_addr
add wave -noupdate /tobs_data_rdout_tb/slave_1/SPY_TOB_mem_rd_data
add wave -noupdate /tobs_data_rdout_tb/slave_1/ipbw
add wave -noupdate /tobs_data_rdout_tb/slave_1/ipbr
add wave -noupdate /tobs_data_rdout_tb/slave_1/ipbr_d
add wave -noupdate /tobs_data_rdout_tb/slave_1/L1A_ID_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/BCN_ID_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_WR_ADDR_OFFSET_REG_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_SLICES_TO_RD_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_FIFO_FULL_THRESH_ASSERT_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_FIFO_FULL_THRESH_NEGATE_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_FIFO_DATA_COUNT_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/XTOB_EG_FIFO_FULL_THRESH_ASSERT_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/XTOB_EG_FIFO_FULL_THRESH_NEGATE_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/XTOB_EG_FIFO_DATA_COUNT_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/XTOB_TAU_FIFO_FULL_THRESH_ASSERT_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/XTOB_TAU_FIFO_FULL_THRESH_NEGATE_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/XTOB_TAU_FIFO_DATA_COUNT_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_Link_output_FIFO_FULL_THRESH_ASSERT_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_Link_output_FIFO_FULL_THRESH_NEGATE_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_LINK_OUTPUT_FIFO_RD_DATA_COUNT_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/TOB_LINK_OUTPUT_FIFO_WR_DATA_COUNT_i
add wave -noupdate /tobs_data_rdout_tb/slave_1/SPY_TOB_mem_wr_addr_i
add wave -noupdate -divider {XTOBs TOBs Out}
add wave -noupdate -divider {Sorted TOBs Out}
add wave -noupdate -divider Modules
add wave -noupdate -divider -height 50 Algorithm
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 5} {3625000000 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 213
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix -1
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 fs} {21 us}
