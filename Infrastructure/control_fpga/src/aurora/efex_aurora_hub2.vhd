----------------------------------------------------------------------------------
-- Company:STFC 
-- Engineer: Mohammed Siyad
-- 
-- Create Date: 14.11.2018 18:38:21
-- Design Name: 
-- Module Name: efex_aurora - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
LIBRARY infrastructure_lib;
USE infrastructure_lib.all;


entity efex_aurora_hub2 is
  Port ( 
         -- TX Stream Interface
          s_axi_tx_tdata      : in  std_logic_vector(63 downto 0);
          s_axi_tx_tvalid     : in  std_logic;
          s_axi_tx_tready     : out std_logic;
          s_axi_tx_tkeep      : in std_logic_vector(7 downto 0);
          s_axi_tx_tlast      : in  std_logic;
         -- User Flow Control TX Interface
          s_axi_ufc_tx_req    : in std_logic;
          s_axi_ufc_tx_ms     : in std_logic_vector(2 downto 0);
          s_axi_ufc_tx_ack    : out std_logic;  
         -- V7 Serial I/O
          txp                 : out std_logic_vector(0 to 3);
          txn                 : out std_logic_vector(0 to 3);
         -- GT Reference Clock Interface
          gt_refclk1_p        : in std_logic;
          gt_refclk1_n        : in std_logic;
         -- Error Detection Interface
          tx_hard_err         : out std_logic;
         -- Status
          tx_channel_up       : out std_logic;
          tx_lane_up          : out std_logic_vector(0 to 3);
         -- System Interface
          user_clk_out        : out std_logic;
          sys_reset_out       : out std_logic;
          tx_lock             : out std_logic;
          init_clk            : in  std_logic; 
          init_clk_out        : out  std_logic;
          pll_not_locked      : out std_logic;
          tx_resetdone        : out std_logic;
          link_reset          : in std_logic        
                 
         
         
         );
end efex_aurora_hub2;

architecture Behavioral of efex_aurora is
signal tx_system_reset_i,gt_reset_i: std_logic;

begin


reset_timer : entity infrastructure_lib.aurora_reset
  port map 
   (
   init_clk   => init_clk,
   BTN0       => '0', 
   rst_sw     => link_reset,
   tx_reset   => tx_system_reset_i,
   tx_GTReset => gt_reset_i ,
   rx_reset   => open,
   rx_GTReset => open
   );

aurora_core: entity infrastructure_lib.aurora_wrapper_hub2
 Port map
 ( 
       -- TX Stream Interface
        s_axi_tx_tdata      =>  s_axi_tx_tdata  ,
        s_axi_tx_tvalid     =>  s_axi_tx_tvalid ,
        s_axi_tx_tready     =>  s_axi_tx_tready ,
        s_axi_tx_tkeep      =>  s_axi_tx_tkeep  ,
        s_axi_tx_tlast      =>  s_axi_tx_tlast  ,
       -- User Flow Control TX Interface
        s_axi_ufc_tx_req    => s_axi_ufc_tx_req,
        s_axi_ufc_tx_ms     => s_axi_ufc_tx_ms ,
        s_axi_ufc_tx_ack    => s_axi_ufc_tx_ack,
       -- V7 Serial I/O
        txp                 =>  txp ,
        txn                 =>  txn ,
       -- GT Reference Clock Interface
        gt_refclk1_p        =>  gt_refclk1_p,
        gt_refclk1_n        =>  gt_refclk1_n,
       -- Error Detection Interface
        tx_hard_err         => tx_hard_err ,
       -- Status
        tx_channel_up       => tx_channel_up,
        tx_lane_up          => tx_lane_up   ,
       -- System Interface
        user_clk_out        => user_clk_out    ,
        sys_reset_out       => sys_reset_out   ,
        gt_reset            => gt_reset_i        ,
        tx_system_reset     => tx_system_reset_i ,
        tx_lock             => tx_lock         ,
        init_clk            => init_clk        ,
        init_clk_out        => init_clk_out    ,
        pll_not_locked      => pll_not_locked  ,
        tx_resetdone        => tx_resetdone    

);



end Behavioral;
