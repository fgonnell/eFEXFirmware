###### This is XDC file for processing Golden FPGA 1 ###########

###### Ethernet RefClk (125MHz)
set_property IOSTANDARD LVDS [get_ports gt_clk125_p]
set_property PACKAGE_PIN AT18 [get_ports gt_clk125_p]
set_property PACKAGE_PIN AU18 [get_ports gt_clk125_n]
set_property IOSTANDARD LVDS [get_ports gt_clk125_n]
set_property DIFF_TERM TRUE [get_ports gt_clk125_p]
set_property DIFF_TERM TRUE [get_ports gt_clk125_n]

###### FPGA1_GCLK1_40M08
set_property IOSTANDARD LVDS [get_ports ttc_clk_p]
set_property IOSTANDARD LVDS [get_ports ttc_clk_n]
set_property PACKAGE_PIN J27 [get_ports ttc_clk_n]
set_property PACKAGE_PIN K27 [get_ports ttc_clk_p]
set_property DIFF_TERM TRUE [get_ports ttc_clk_p]
set_property DIFF_TERM TRUE [get_ports ttc_clk_n]

###### MAC clock
create_generated_clock -name mac_clk [get_pins clk_wiz_0/clk_gen/inst/mmcm_adv_inst/CLKOUT0]
set_clock_groups -asynchronous -group [get_clocks mac_clk]

create_generated_clock -name flash_spi_clk -source [get_pins slaves/spi_flash/spi_clk_reg/C] -divide_by 2 [get_pins slaves/spi_flash/spi_clk_reg/Q]

###### SPI Flash Pins
set_property PACKAGE_PIN BA29 [get_ports flash_csn]
set_property IOSTANDARD LVCMOS18 [get_ports flash_csn]

set_property PACKAGE_PIN BC27 [get_ports flash_mosi]
set_property IOSTANDARD LVCMOS18 [get_ports flash_mosi]

set_property PACKAGE_PIN BD27 [get_ports flash_miso]
set_property IOSTANDARD LVCMOS18 [get_ports flash_miso]

###### LEDs #########################
set_property IOSTANDARD LVCMOS18 [get_ports flash_led]
set_property PACKAGE_PIN AP29 [get_ports flash_led]

###### F1 Geographyical address
set_property IOSTANDARD LVCMOS18 [get_ports {fpga_geo_addr[*]}]
set_property PACKAGE_PIN T24 [get_ports {fpga_geo_addr[0]}]
set_property PACKAGE_PIN T25 [get_ports {fpga_geo_addr[1]}]

###### Hardware address   Pins
set_property IOSTANDARD LVCMOS18 [get_ports {hardware_addr[*]}]
set_property PACKAGE_PIN AN28 [get_ports {hardware_addr[0]}]
set_property PACKAGE_PIN AN30 [get_ports {hardware_addr[1]}]
set_property PACKAGE_PIN AJ29 [get_ports {hardware_addr[2]}]
set_property PACKAGE_PIN AK27 [get_ports {hardware_addr[3]}]
set_property PACKAGE_PIN AM30 [get_ports {hardware_addr[4]}]
set_property PACKAGE_PIN AM28 [get_ports {hardware_addr[5]}]
set_property PACKAGE_PIN AN29 [get_ports {hardware_addr[6]}]
set_property PACKAGE_PIN AK28 [get_ports {hardware_addr[7]}]

###### Tx and Rx signal between FPGA1 and Control FPGA ##########
set_property IOSTANDARD LVCMOS18 [get_ports {master_tx_data[*]}]

set_property PACKAGE_PIN BA18 [get_ports {master_tx_data[0]}]
set_property PACKAGE_PIN BC19 [get_ports {master_tx_data[1]}]
set_property PACKAGE_PIN BB17 [get_ports {master_tx_data[2]}]
set_property PACKAGE_PIN BD17 [get_ports {master_tx_data[3]}]
set_property PACKAGE_PIN BC15 [get_ports {master_tx_data[4]}]
set_property PACKAGE_PIN BB16 [get_ports {master_tx_data[5]}]
set_property PACKAGE_PIN AW17 [get_ports {master_tx_data[6]}]
set_property PACKAGE_PIN AY18 [get_ports {master_tx_data[7]}]
set_property PACKAGE_PIN AW16 [get_ports {master_tx_data[8]}]
set_property PACKAGE_PIN BA16 [get_ports {master_tx_data[9]}]

set_property IOSTANDARD LVCMOS18 [get_ports master_tx_pause]
set_property PACKAGE_PIN AV18 [get_ports master_tx_pause]

set_property IOSTANDARD LVCMOS18 [get_ports {master_rx_data[*]}]

set_property PACKAGE_PIN BB18 [get_ports {master_rx_data[0]}]
set_property PACKAGE_PIN BC18 [get_ports {master_rx_data[1]}]
set_property PACKAGE_PIN BC17 [get_ports {master_rx_data[2]}]
set_property PACKAGE_PIN BD16 [get_ports {master_rx_data[3]}]
set_property PACKAGE_PIN BD15 [get_ports {master_rx_data[4]}]
set_property PACKAGE_PIN BB15 [get_ports {master_rx_data[5]}]
set_property PACKAGE_PIN AY16 [get_ports {master_rx_data[6]}]
set_property PACKAGE_PIN AY17 [get_ports {master_rx_data[7]}]
set_property PACKAGE_PIN AW15 [get_ports {master_rx_data[8]}]
set_property PACKAGE_PIN BA15 [get_ports {master_rx_data[9]}]

set_property IOB TRUE [get_ports {master_rx_data[*]}]
set_property IOB TRUE [get_ports {master_tx_data[*]}]
set_property IOB TRUE [get_ports master_tx_pause]
