 
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;


ENTITY testbench_buffer IS
   PORT( 
      pe     : OUT    std_logic;
      clk280  : OUT    std_logic;
      clk160  : OUT    std_logic;
      rxdata : OUT    std_logic_VECTOR (31 DOWNTO 0);
      reset  : OUT    std_logic;
      rdout  : OUT    std_logic
   );

-- Declarations

END ENTITY testbench_buffer ;

--
ARCHITECTURE spec OF testbench_buffer IS
constant clk_prd160 : time := 6.25 ns; 
constant clk_prd280 : time := 3.57 ns;   
signal iclk160,iclk280      : std_logic;   
signal cntr      : unsigned(31 downto 0);

BEGIN
  
  process 
  
   Begin
	
       Reset  <= '1';
       pe     <= '0';
       rdout  <= '0';
       rxdata <= (others => '0');  
       cntr   <=  (others => '0');
       wait for 25 ns;
       Reset       <= '0';
       wait for  75 ns; 
       rxdata <= x"00000abc";  -- idle k28.5 
       wait for clk_prd160*5 ;     
       rxdata <= x"00000a7c";  -- start of frame k28.1 with L1ID bit 31 to 8
       wait for clk_prd160 ;
       rxdata <= x"aaa0045c"; -- bcn number
       wait for  clk_prd160 ;
       rxdata <= x"00000001"; -- payload 1
       wait for clk_prd160 ;
       rxdata <= x"00000002"; -- payload 2
       wait for  clk_prd160 ; 
       rxdata <= x"00000003"; -- payload 3
       wait for clk_prd160 ;
       rxdata <= x"00000004"; -- payload 4
       wait for  clk_prd160 ;
       rxdata <= x"00000005"; -- payload 5
       wait for clk_prd160 ;
       rxdata <= x"00000006"; -- payload 6
       
       wait for clk_prd160;
       rxdata <= x"00000007"; -- trailer 
        wait for clk_prd160 ;
       rxdata <= x"000000dc"; -- end of frame  k28.6 
       wait for  clk_prd160*6;
      
       rdout  <= '1';
       wait for  clk_prd160*5 ;
       rdout  <= '0';          
       wait for  clk_prd160;
       rxdata <= x"0000007C";  -- start of frame k28.1 with L1ID bit 31 to 8
        wait for  clk_prd160 ;
dataloop: while cntr < 128 loop
              rxdata <=  std_logic_vector(cntr);
              cntr  <= cntr+1;
              wait for clk_prd160;
          end loop;
          rxdata <= x"111111dc"; -- end of frame  k28.6 
           wait for  clk_prd160*4;
                rdout  <= '1';
                wait for  clk_prd160*128 ;
                rdout  <= '0';          
                wait for  clk_prd160;
          wait for 1 us;
   
   
   
       
     end process;
     
       
       
       
       
       
  ----------------------------------------------- 
  -- Clock genration of the test bench 280MHz  
 ------------------------------------------------ 
  
   clock_gen280: process
     begin
       iclk280<= '1';
        wait for clk_prd280/2;
        iclk280 <= '0';
        wait for clk_prd280/2;
      end process clock_gen280 ;
      clk280<= iclk280;          
  ----------------------------------------------- 
        -- Clock genration of the test bench 160MHz  
       ------------------------------------------------ 
        
         clock_gen160: process
           begin
             iclk160<= '1';
              wait for clk_prd160/2;
              iclk160 <= '0';
              wait for clk_prd160/2;
            end process clock_gen160 ;
            clk160<= iclk160;            
       
END ARCHITECTURE spec;
