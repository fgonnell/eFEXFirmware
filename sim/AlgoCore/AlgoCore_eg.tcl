#restart -force
onerror {resume}

proc TOB_decode { parent signal name} {
    quietly virtual signal -install $parent [concat $parent/$signal\(31 downto 30\)] $name\_FPGA
    quietly virtual signal -install $parent [concat $parent/$signal\(29 downto 27\)] $name\_ETA
    quietly virtual signal -install $parent [concat $parent/$signal\(26 downto 24\)] $name\_PHI
    quietly virtual signal -install $parent [concat $parent/$signal\(23 downto 22\)] $name\_Had
    quietly virtual signal -install $parent [concat $parent/$signal\(21 downto 20\)] $name\_f3
    quietly virtual signal -install $parent [concat $parent/$signal\(19 downto 18\)] $name\_Reta
    quietly virtual signal -install $parent [concat $parent/$signal\(14\)]           $name\_Max
    quietly virtual signal -install $parent [concat $parent/$signal\(15\)]           $name\_UpNotDown
    quietly virtual signal -install $parent [concat $parent/$signal\(17 downto 16\)] $name\_Seed
    quietly virtual signal -install $parent [concat $parent/$signal\(11 downto 0\)]  $name\_Energy
    add wave -noupdate -group $name [concat $parent/$name\_FPGA	   ]
    add wave -noupdate -group $name [concat $parent/$name\_ETA	   ]
    add wave -noupdate -group $name [concat $parent/$name\_PHI	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Had	   ]
    add wave -noupdate -group $name [concat $parent/$name\_f3	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Reta	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Max	   ]
    add wave -noupdate -group $name [concat $parent/$name\_UpNotDown]
    add wave -noupdate -group $name [concat $parent/$name\_Seed	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Energy   ]
}

proc XTOB_decode { parent signal name} {
    quietly virtual signal -install $parent [concat $parent/$signal\(31 downto 30\)] $name\_FPGA
    quietly virtual signal -install $parent [concat $parent/$signal\(29 downto 27\)] $name\_ETA
    quietly virtual signal -install $parent [concat $parent/$signal\(26 downto 24\)] $name\_PHI
    quietly virtual signal -install $parent [concat $parent/$signal\(23 downto 22\)] $name\_Had
    quietly virtual signal -install $parent [concat $parent/$signal\(21 downto 20\)] $name\_f3
    quietly virtual signal -install $parent [concat $parent/$signal\(19 downto 18\)] $name\_Reta
    quietly virtual signal -install $parent [concat $parent/$signal\(14\)]           $name\_Max
    quietly virtual signal -install $parent [concat $parent/$signal\(15\)]           $name\_UpNotDown
    quietly virtual signal -install $parent [concat $parent/$signal\(17 downto 16\)] $name\_Seed
    quietly virtual signal -install $parent [concat $parent/$signal\(63 downto 48\)]  $name\_Energy
    add wave -noupdate -group $name [concat $parent/$name\_FPGA	   ]
    add wave -noupdate -group $name [concat $parent/$name\_ETA	   ]
    add wave -noupdate -group $name [concat $parent/$name\_PHI	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Had	   ]
    add wave -noupdate -group $name [concat $parent/$name\_f3	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Reta	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Max	   ]
    add wave -noupdate -group $name [concat $parent/$name\_UpNotDown]
    add wave -noupdate -group $name [concat $parent/$name\_Seed	   ]
    add wave -noupdate -group $name [concat $parent/$name\_Energy   ]
}

quietly WaveActivateNextPane {} 0


add wave -noupdate  tb_algocore_eg/EG_ALGO//CLK200
add wave -noupdate -color Orange  tb_algocore_eg/EG_ALGO//IN_Data
add wave -noupdate  tb_algocore_eg/EG_ALGO//IN_ParD3
add wave -noupdate  tb_algocore_eg/EG_ALGO//IN_ParCEta
add wave -noupdate  tb_algocore_eg/EG_ALGO//IN_ParHadron
add wave -noupdate  tb_algocore_eg/EG_ALGO//IN_Control
add wave -noupdate  tb_algocore_eg/EG_ALGO//ParCEta
add wave -noupdate  tb_algocore_eg/EG_ALGO//ParD3
add wave -noupdate  tb_algocore_eg/EG_ALGO//ParRh

# Seed Finder
add wave -noupdate  -group SeedFinder tb_algocore_eg/EG_ALGO//SEED_FINDER/CLK
add wave -noupdate -color Orange  -group SeedFinder tb_algocore_eg/EG_ALGO//SEED_FINDER/IN_Data
add wave -noupdate -color Orange  -group SeedFinder tb_algocore_eg/EG_ALGO//SEED_FINDER/IN_DataUp
add wave -noupdate -color Orange  -group SeedFinder tb_algocore_eg/EG_ALGO//SEED_FINDER/IN_DataDown
add wave -noupdate  -group SeedFinder tb_algocore_eg/EG_ALGO//SEED_FINDER/Result
add wave -noupdate  -group SeedFinder tb_algocore_eg/EG_ALGO//SEED_FINDER/IsMaxMax
add wave -noupdate  -group SeedFinder tb_algocore_eg/EG_ALGO//SEED_FINDER/UpOrDown
add wave -noupdate -color Cyan  -group SeedFinder tb_algocore_eg/EG_ALGO//SEED_FINDER/OUT_UpNotDown
add wave -noupdate -color Cyan  -group SeedFinder tb_algocore_eg/EG_ALGO//SEED_FINDER/OUT_Seed
add wave -noupdate -color Cyan  -group SeedFinder tb_algocore_eg/EG_ALGO//SEED_FINDER/OUT_IsLocalMax
add wave -noupdate -color Cyan  -group SeedFinder tb_algocore_eg/EG_ALGO//SEED_FINDER/OUT_IsMax

add wave -noupdate  tb_algocore_eg/EG_ALGO//SF_Data
add wave -noupdate  tb_algocore_eg/EG_ALGO//SF_DataUp
add wave -noupdate  tb_algocore_eg/EG_ALGO//SF_DataDown
add wave -noupdate  tb_algocore_eg/EG_ALGO//SF_UpNotDown
add wave -noupdate  tb_algocore_eg/EG_ALGO//SF_Seed
add wave -noupdate  tb_algocore_eg/EG_ALGO//SF_IsLocalMax
add wave -noupdate  tb_algocore_eg/EG_ALGO//SF_IsMax

# Input Mux
add wave -noupdate  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/CLK
add wave -noupdate  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/IN_Seed
add wave -noupdate  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/IN_UpNotDown
add wave -noupdate  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/IN_Towers
add wave -noupdate  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/Selector
add wave -noupdate  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/ShiftTowers
add wave -noupdate  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/ShiftTowers2
add wave -noupdate -color Cyan  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/OUT_EnergyL0
add wave -noupdate -color Cyan  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/OUT_EnergyL1
add wave -noupdate -color Cyan  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/OUT_EnergyL2
add wave -noupdate -color Cyan  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/OUT_EnergyL3
add wave -noupdate -color Cyan  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/OUT_REtaCoreData
add wave -noupdate -color Cyan  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/OUT_REtaEnvData
add wave -noupdate -color Cyan  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/OUT_WsCoreData
add wave -noupdate -color Cyan  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/OUT_WsEnvData
add wave -noupdate -color Cyan  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/OUT_HadCoreData
add wave -noupdate -color Cyan  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/OUT_HadEnvDataL1
add wave -noupdate -color Cyan  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/OUT_HadEnvDataL2
add wave -noupdate -color Cyan  -group InputMux tb_algocore_eg/EG_ALGO//INPUT_MULTIPLEXER/OUT_HadEnvDataL3

add wave -noupdate  tb_algocore_eg/EG_ALGO//IM_Towers
add wave -noupdate  tb_algocore_eg/EG_ALGO//IM_EnergyL0
add wave -noupdate  tb_algocore_eg/EG_ALGO//IM_EnergyL1
add wave -noupdate  tb_algocore_eg/EG_ALGO//IM_EnergyL2
add wave -noupdate  tb_algocore_eg/EG_ALGO//IM_EnergyL3
add wave -noupdate  tb_algocore_eg/EG_ALGO//IM_REtaCoreData
add wave -noupdate  tb_algocore_eg/EG_ALGO//IM_REtaEnvData
add wave -noupdate  tb_algocore_eg/EG_ALGO//IM_WsCoreData
add wave -noupdate  tb_algocore_eg/EG_ALGO//IM_WsEnvData
add wave -noupdate  tb_algocore_eg/EG_ALGO//IM_HadEnvDataL1
add wave -noupdate  tb_algocore_eg/EG_ALGO//IM_HadEnvDataL2
add wave -noupdate  tb_algocore_eg/EG_ALGO//IM_HadEnvDataL3
add wave -noupdate  tb_algocore_eg/EG_ALGO//IM_HadCoreData

# Adders
foreach cond {ENERGY RETA_ENV RETA_CORE WS_ENV WS_CORE HAD_CORE HAD_ENV}  {
    add wave -noupdate  -group Adder$cond tb_algocore_eg/EG_ALGO//MULTI_ADDER_$cond/stage
    add wave -noupdate  -group Adder$cond tb_algocore_eg/EG_ALGO//MULTI_ADDER_$cond/delay
    add wave -noupdate  -group Adder$cond tb_algocore_eg/EG_ALGO//MULTI_ADDER_$cond/CLK
    add wave -noupdate  -group Adder$cond tb_algocore_eg/EG_ALGO//MULTI_ADDER_$cond/IN_Words
    add wave -noupdate  -group Adder$cond tb_algocore_eg/EG_ALGO//MULTI_ADDER_$cond/connector
    add wave -noupdate  -group Adder$cond tb_algocore_eg/EG_ALGO//MULTI_ADDER_$cond/carry
    add wave -noupdate  -group Adder$cond tb_algocore_eg/EG_ALGO//MULTI_ADDER_$cond/DelayedOut
    add wave -noupdate  -group Adder$cond tb_algocore_eg/EG_ALGO//MULTI_ADDER_$cond/DelayedOverflow
    add wave -noupdate -color Cyan  -group Adder$cond tb_algocore_eg/EG_ALGO//MULTI_ADDER_$cond/OUT_Overflow
    add wave -noupdate -color Cyan  -group Adder$cond tb_algocore_eg/EG_ALGO//MULTI_ADDER_$cond/OUT_Word

}

add wave -noupdate  tb_algocore_eg/EG_ALGO//MA_EnergyOverflow
add wave -noupdate  tb_algocore_eg/EG_ALGO//MA_EnergySum
add wave -noupdate  tb_algocore_eg/EG_ALGO//MA_REtaEnvOverflow
add wave -noupdate  tb_algocore_eg/EG_ALGO//MA_REtaEnvSum
add wave -noupdate  tb_algocore_eg/EG_ALGO//MA_REtaCoreOverflow
add wave -noupdate  tb_algocore_eg/EG_ALGO//MA_REtaCoreSum
add wave -noupdate  tb_algocore_eg/EG_ALGO//MA_WsEnvOverflow
add wave -noupdate  tb_algocore_eg/EG_ALGO//MA_WsEnvSum
add wave -noupdate  tb_algocore_eg/EG_ALGO//MA_WsCoreOverflow
add wave -noupdate  tb_algocore_eg/EG_ALGO//MA_WsCoreSum
add wave -noupdate  tb_algocore_eg/EG_ALGO//MA_HadCoreOverflow
add wave -noupdate  tb_algocore_eg/EG_ALGO//MA_HadCoreSum
add wave -noupdate  tb_algocore_eg/EG_ALGO//MA_HadEnvOverflow
add wave -noupdate  tb_algocore_eg/EG_ALGO//MA_HadEnvSum

# Multipliers
foreach cond {RETA WS HADRON}  {
    add wave -noupdate  -group MultReta tb_algocore_eg/EG_ALGO//$cond\_MULTIPLIER/CLK
    add wave -noupdate  -group MultReta tb_algocore_eg/EG_ALGO//$cond\_MULTIPLIER/IN_Word
    add wave -noupdate  -group MultReta tb_algocore_eg/EG_ALGO//$cond\_MULTIPLIER/IN_parameters
    add wave -noupdate  -group MultReta tb_algocore_eg/EG_ALGO//$cond\_MULTIPLIER/OUT_Overflow
    add wave -noupdate  -group MultReta tb_algocore_eg/EG_ALGO//$cond\_MULTIPLIER/OUT_Words
    add wave -noupdate  -group MultReta tb_algocore_eg/EG_ALGO//$cond\_MULTIPLIER/Param
    add wave -noupdate  -group MultReta tb_algocore_eg/EG_ALGO//$cond\_MULTIPLIER/Data
    add wave -noupdate  -group MultReta tb_algocore_eg/EG_ALGO//$cond\_MULTIPLIER/Overflow
    add wave -noupdate  -group MultReta tb_algocore_eg/EG_ALGO//$cond\_MULTIPLIER/Result_big
    add wave -noupdate  -group MultReta tb_algocore_eg/EG_ALGO//$cond\_MULTIPLIER/Result
}

add wave -noupdate  tb_algocore_eg/EG_ALGO//MU_REtaEnvOverflows
add wave -noupdate  tb_algocore_eg/EG_ALGO//MU_WsCoreOverflows
add wave -noupdate  tb_algocore_eg/EG_ALGO//MU_HadCoreOverflows
add wave -noupdate  tb_algocore_eg/EG_ALGO//MU_REtaEnvMult
add wave -noupdate  tb_algocore_eg/EG_ALGO//MU_WsCoreMult
add wave -noupdate  tb_algocore_eg/EG_ALGO//MU_HadCoreMult

add wave -noupdate  -group SeedDealy tb_algocore_eg/EG_ALGO//SEED_DELAY/delay
add wave -noupdate  -group SeedDealy tb_algocore_eg/EG_ALGO//SEED_DELAY/size
add wave -noupdate  -group SeedDealy tb_algocore_eg/EG_ALGO//SEED_DELAY/CLK
add wave -noupdate  -group SeedDealy tb_algocore_eg/EG_ALGO//SEED_DELAY/IN_Signal
add wave -noupdate  -group SeedDealy tb_algocore_eg/EG_ALGO//SEED_DELAY/IN_Word
add wave -noupdate  -group SeedDealy tb_algocore_eg/EG_ALGO//SEED_DELAY/OUT_Signal
add wave -noupdate  -group SeedDealy tb_algocore_eg/EG_ALGO//SEED_DELAY/OUT_Word
add wave -noupdate  -group SeedDealy tb_algocore_eg/EG_ALGO//SEED_DELAY/DelayedWord
add wave -noupdate  -group SeedDealy tb_algocore_eg/EG_ALGO//SEED_DELAY/DelayedSignal

add wave -noupdate  tb_algocore_eg/EG_ALGO//DL_SeedFinder
add wave -noupdate  tb_algocore_eg/EG_ALGO//DL_UpNotDown
add wave -noupdate  tb_algocore_eg/EG_ALGO//DL_Seed
add wave -noupdate  tb_algocore_eg/EG_ALGO//DL_IsLocalMax
add wave -noupdate  tb_algocore_eg/EG_ALGO//DL_IsMax
add wave -noupdate  tb_algocore_eg/EG_ALGO//DL_Overflows
add wave -noupdate  tb_algocore_eg/EG_ALGO//DL_REtaEnvOverflow
add wave -noupdate  tb_algocore_eg/EG_ALGO//DL_WsCoreOverflow
add wave -noupdate  tb_algocore_eg/EG_ALGO//DL_HadCoreOverflow

add wave -noupdate  tb_algocore_eg/EG_ALGO//REtaCondition
add wave -noupdate  tb_algocore_eg/EG_ALGO//WsCondition
add wave -noupdate  tb_algocore_eg/EG_ALGO//HadCondition
add wave -noupdate  tb_algocore_eg/EG_ALGO//TOBEnergy
add wave -noupdate  tb_algocore_eg/EG_ALGO//TOBEnergyOverflow

add wave -noupdate  tb_algocore_eg/EG_ALGO//OUT_Status
add wave -color Cyan -noupdate tb_algocore_eg/EG_ALGO//OUT_TOB

##########################################

TreeUpdate [SetDefaultTree]
WaveRestoreCursors {Start {3300000 ps} 1} {XTOBs {3389285 ps} 1} {Sorted_TOBs {3417857 ps} 1} {Cursor1 {4000 ns} 0}

quietly wave cursor active 4
configure wave -namecolwidth 213
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix -1
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
