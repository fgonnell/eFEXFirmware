library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package ControlFPGAPackage is

  function F_FPGA_NUMBER (flavour             : in integer) return integer;
  function F_GOLDEN (flavour                  : in integer) return boolean;

end package ControlFPGAPackage;

package body ControlFPGAPackage is

  function F_FPGA_NUMBER (flavour : in integer) return integer is
    variable num : integer;
  begin
    if FLAVOUR < 10 then
      num := FLAVOUR;
    elsif FLAVOUR < 100 then
      num := FLAVOUR - 10;
    elsif FLAVOUR < 1000 then
      num := FLAVOUR - 100;
    else
      num := FLAVOUR - 1000;
    end if;

    return num;
  end function;

  function F_GOLDEN (flavour : in integer) return boolean is
    variable gold : boolean;
  begin
    if FLAVOUR < 10 then
      gold := true;
    else
      gold := false;
    end if;

    return gold;
  end function;
end ControlFPGAPackage;
