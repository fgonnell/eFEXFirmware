-- Parse incoming packet (Input Data or TOB) into dpram and FIFO
--
-- Dave Sankey, September 2019

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY mgt_readout_receiver IS
  GENERIC(
	ADDR_WIDTH: positive := 16;
	FPGA_NO: std_logic_vector(1 downto 0) := "00"
  );
  port(
	clk_mgt: in std_logic;
	rst_mgt: in std_logic;
-- data from MGT
	data_from_mgt: in std_logic_vector(31 downto 0);
	char_is_k: in std_logic;
	error_from_mgt: in std_logic;
-- data to DPRAM
	MGT_RAM_addr: out std_logic_vector(ADDR_WIDTH downto 0);
	MGT_RAM_we: out std_logic := '0';
	MGT_RAM_din: out std_logic_vector(31 downto 0);
	MGT_RAM_data: in std_logic_vector(31 downto 0);
-- data to IPBus RAM
	IPBus_RAM_addr: out std_logic_vector(9 downto 0);
	IPBus_RAM_din: out std_logic_vector(31 DOWNTO 0);
	IPBus_RAM_we: out std_logic;
-- metadata to FIFO etc.
	fifo_data: out std_logic_vector(71 downto 0);
	fifo_we: out std_logic;
	free_space: out std_logic_vector(ADDR_WIDTH downto 0)
  );
END ENTITY mgt_readout_receiver;

Architecture rtl of mgt_readout_receiver is

  TYPE STATE_TYPE IS (
    initial,
    idle, -- K28.5
    new_packet, -- K28.1/3
    save_l1id,
	save_payload,
	padding, -- K28.6...
	corrective_trailer, -- K28.6/K28.5...
    write_metadata -- K28.6/K28.5
  );

  signal state_sig: STATE_TYPE := initial;
  signal data_from_mgt_sig, data_from_mgt_buf, data_to_RAM_sig, data_from_RAM_sig: std_logic_vector(31 downto 0);
  signal payload_length_sig: std_logic_vector(8 downto 0);
  signal data_to_fifo_sig: std_logic_vector(71 downto 0);
  signal addr_sig: std_logic_vector(ADDR_WIDTH downto 0);
  signal we_sig, char_is_k_sig, error_from_mgt_sig, write_fifo_sig, TOB_mode_sig, TOB_trailer_OK_sig, Multi_slice_sig: std_logic := '0';
  signal rst_mgt_sig: std_logic := '1';
  signal Errors_sig: std_logic_vector(4 downto 0); -- MultiSlice, Safe Mode, Protocol/Link, Trailer, Length

begin

Reset_stretcher: process(clk_mgt)
  Variable stretch: std_logic_vector(3 downto 0) := (Others => '1');
  begin
    if rising_edge(clk_mgt) then
      if rst_mgt = '1' then
        stretch := (Others => '1');
      end if;
      rst_mgt_sig <= stretch(3)
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
	  stretch := stretch(2 downto 0) & "0";
	end if;
  end process Reset_stretcher;

Input_buffer: process(clk_mgt)
  begin
    if rising_edge(clk_mgt) then
      data_from_RAM_sig <= MGT_RAM_data
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      data_from_mgt_sig <= data_from_mgt
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      data_from_mgt_buf <= data_from_mgt_sig
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      char_is_k_sig <= char_is_k
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      error_from_mgt_sig <= error_from_mgt
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process Input_buffer;

Output_buffer: process(clk_mgt)
  begin
    if rising_edge(clk_mgt) then
      MGT_RAM_addr <= addr_sig
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      MGT_RAM_we <= we_sig
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      MGT_RAM_din <= data_to_RAM_sig
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      fifo_data <= data_to_fifo_sig
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      fifo_we <= write_fifo_sig
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process Output_buffer;

IPBus_RAM: process(clk_mgt)
  Variable Last_Addr: std_logic_vector(31 downto 0) := (Others => '0');
  Begin
	if rising_edge(clk_mgt) then
	   if (we_sig = '1') and (addr_sig(9 downto 0) /= "0000000000") then
	      Last_Addr(ADDR_WIDTH downto 0) := addr_sig;
		  IPBus_RAM_addr <= addr_sig(9 downto 0)
	-- pragma translate_off
		  after 2 ns
	-- pragma translate_on
		  ;
		  IPBus_RAM_we <= we_sig
	-- pragma translate_off
		  after 2 ns
	-- pragma translate_on
		  ;
		  IPBus_RAM_din <= data_to_RAM_sig
	-- pragma translate_off
		  after 2 ns
	-- pragma translate_on
		  ;
	   else
		  IPBus_RAM_addr <= (Others => '0')
	-- pragma translate_off
		  after 2 ns
	-- pragma translate_on
		  ;
		  IPBus_RAM_we <= '1'
	-- pragma translate_off
		  after 2 ns
	-- pragma translate_on
		  ;
		  IPBus_RAM_din <= Last_Addr
	-- pragma translate_off
		  after 2 ns
	-- pragma translate_on
		  ;
	   end if;
	end if;
  end process IPBus_RAM;

FreeSpace_block: process(clk_mgt)
  variable Last_Read, Last_Write, Used_Space: unsigned(ADDR_WIDTH+1 downto 0) := (others => '0');
  variable ram_delay: std_logic_vector(7 downto 0) := (Others => '1');
  Begin
	if rising_edge(clk_mgt) then
	  if rst_mgt_sig = '1' then
		Used_Space := (others => '0');
		Last_Read := (others => '0');
		Last_Write := (others => '0');
	  elsif ram_delay(0) = '0' then -- data coming back from DPRAM
		if data_from_RAM_sig(ADDR_WIDTH+1 downto 0) = std_logic_vector(Last_Read) then -- stable value...
		  if Last_Write < Last_Read then
			Last_Write(ADDR_WIDTH+1) := '1';
		  end if;
		  Used_Space := Last_Write - Last_Read;
		end if;
		Last_Read := unsigned(data_from_RAM_sig(ADDR_WIDTH+1 downto 0));
	  end if;
	  free_space <= (others => '1')
	-- pragma translate_off
	  after 2 ns
-- pragma translate_on
	  ;
	end if;
  end process FreeSpace_block;

Payload_length_block: process(clk_mgt)
  variable payload_length: unsigned(8 downto 0) := (Others => '0');
  begin
    if rising_edge(clk_mgt) then
      payload_length_sig <= std_logic_vector(payload_length)
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      if state_sig = idle then
        payload_length := (Others => '0');
      else
        payload_length := payload_length + 1;
      end if;
    end if;
  end process Payload_length_block;

Trailer_checker: process(clk_mgt)
  variable expected_length: unsigned(7 downto 0) := (Others => '0');
  variable TOB_trailer_OK, Multi_slice: std_logic := '0';
  begin
    if rising_edge(clk_mgt) then
      if char_is_k_sig = '0' then -- data...
-- First work out number of 64 bit words for XTOBs
        expected_length := unsigned("00" & data_from_mgt_sig(15 downto 10));
        expected_length := expected_length + unsigned(data_from_mgt_sig(9 downto 4));
-- then number of 64 bit words for even number of TOBs
        expected_length := expected_length + unsigned(data_from_mgt_sig(3 downto 2));
-- 32 bit length of TOB frame OK (expected_length*2 + 1 for 32 bit word alignment)
        if payload_length_sig = std_logic_vector(expected_length) & '1' then
          TOB_trailer_OK := TOB_mode_sig;
-- Safe Mode and no TOBs!
        elsif (data_from_mgt_sig(19) = '1') and (payload_length_sig = std_logic_vector(to_unsigned(1, 9))) then
          TOB_trailer_OK := TOB_mode_sig;
        else
          TOB_trailer_OK := '0';
        end if;
        if data_from_mgt_sig(18 downto 16) = "000" then
          Multi_slice := '0';
        else
          Multi_slice := TOB_mode_sig;
        end if;
      else
        expected_length := (Others => '0');
        TOB_trailer_OK := '0';
        Multi_slice := '0';
      end if;
      TOB_trailer_OK_sig <= TOB_trailer_OK
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      Multi_slice_sig <= Multi_slice
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process Trailer_checker;

Output_Data_engine: process(clk_mgt)
-- Format of FIFO is
--  fifo_data(71 downto 56): starting 64 bit address of payload data in RAM
--  fifo_data(55 downto 48): number of 64 bit words for payload data excluding trailer
--  fifo_data(47 downto 44): local error flags excluding header mismatch and processor timeout
--  fifo_data(43 downto 12): extended L1ID
--  fifo_data(11 downto 0):  BCN
-- Metadata written to RAM is:
--  Word-2(23 downto 16): number of 64 bit words for payload data
--  Word-2(15 downto 12): local error flags excluding header mismatch and processor timeout
--  Word-2(11 downto 0):  BCN
--  Word-1: extended L1ID
  variable data_to_RAM: std_logic_vector(31 downto 0) := (Others => '0');
  variable data_to_fifo: std_logic_vector(71 downto 0) := (Others => '0');
  variable write_fifo: std_logic := '0';
  variable delay_addr: std_logic_vector(2 downto 0) := (Others => '0');
  variable actual_length: std_logic_vector(8 downto 0);
  begin
    if rising_edge(clk_mgt) then
      data_to_RAM := (Others => '0');
      write_fifo := '0';
-- Ensure that start address of payload data is captured (could happen in save_payload, write_metadata or Others)
	  if delay_addr(0) = '1' then -- capture start address of payload data...
		data_to_fifo(55+ADDR_WIDTH downto 56) := addr_sig(ADDR_WIDTH downto 1);
	  end if;
	  delay_addr := "0" & delay_addr(2 downto 1);
	  case state_sig is
	    when initial => -- reset metadata...
		  data_to_fifo := (Others => '0');
	    when idle => -- reset metadata...
		  data_to_fifo := (Others => '0');
		  actual_length := (Others => '0');
		when new_packet =>  -- capture BCN
		  data_to_fifo(11 downto 0) := data_from_mgt_buf(19 downto 8);
		when save_l1id => -- capture L1ID
		  data_to_fifo(43 downto 12) := data_from_mgt_buf;
		  data_to_RAM := data_from_mgt_buf;
		  delay_addr := "100";
		when save_payload =>
		  data_to_RAM := data_from_mgt_buf;
		  actual_length := payload_length_sig; -- remember true length of payload...
		when corrective_trailer =>
		  data_to_RAM(31 downto 26) := '1' & Errors_sig;
		  data_to_RAM(25 downto 24) := FPGA_NO;
		  data_to_RAM(23 downto 12) := data_to_fifo(11 downto 0);
		  data_to_RAM(8 downto 0) := actual_length;
		when write_metadata =>  -- write metadata
		  data_to_fifo(55 downto 44) := payload_length_sig(8 downto 1) & Errors_sig(3 downto 0);
		  data_to_RAM(23 downto 16) := payload_length_sig(8 downto 1);
		  data_to_RAM(15 downto 12) := Errors_sig(3 downto 0);
		  data_to_RAM(11 downto 0) := data_to_fifo(11 downto 0);
		  write_fifo := '1';
		when Others => -- padding
	  end case;
      data_to_RAM_sig <= data_to_RAM
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      data_to_fifo_sig <= data_to_fifo
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      write_fifo_sig <= write_fifo
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process Output_Data_engine;

RAM_control:  process(clk_mgt)
  variable addr_int, next_addr, start_addr: unsigned(ADDR_WIDTH downto 0) := (others => '0');
  variable write_enable: std_logic := '0';
  constant read_addr: unsigned(ADDR_WIDTH downto 0) := (Others => '0');
  constant max_addr: unsigned(ADDR_WIDTH downto 0) := (Others => '1');
  begin
    if rising_edge(clk_mgt) then
      write_enable := '0';
      if rst_mgt_sig = '1' then
-- initial write address is actually 2 as 64b side writes read pointer back to 0, but we increment it again at new packet...
        next_addr := read_addr + 1;
        addr_int := read_addr;
      else
        case state_sig is
          when initial =>  -- read pointer...
            addr_int := read_addr;
          when idle =>  -- read pointer...
            addr_int := read_addr;
          when new_packet =>  -- read pointer, but save incremented write pointer...
            addr_int := read_addr;
            if next_addr = max_addr then
              next_addr := read_addr + 2;
            else
              next_addr := next_addr + 1;
            end if;
            start_addr := next_addr;
          when write_metadata =>  -- write meta data to saved pointer...
            addr_int := start_addr;
            write_enable := '1';
          when Others => -- increment write pointer...
            if next_addr = max_addr then
              next_addr := read_addr + 2;
            else
              next_addr := next_addr + 1;
            end if;
            addr_int := next_addr;
            write_enable := '1';
        end case;
      end if;
      addr_sig <= std_logic_vector(addr_int)
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      we_sig <= write_enable
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process RAM_control;

State_machine: process(clk_mgt)
  variable TOB_mode, Bad_Packet, Length_Error: std_logic := '0';
  variable Errors: std_logic_vector(4 downto 0);
  variable next_state: STATE_TYPE := initial;
  constant idle_frame: std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(188, 32)); -- 0x"000000BC"
  begin
    if rising_edge(clk_mgt) then
      if rst_mgt_sig = '1' then
        next_state := initial;
        TOB_mode := '0';
      else
        if error_from_mgt_sig = '1' then
          Bad_Packet := '1';
        end if;
        Case state_sig is
          When initial =>
            if (error_from_mgt_sig = '0') and (char_is_k_sig = '1') and (data_from_mgt_sig = idle_frame) then
              next_state := idle;
            else
              next_state := initial;
            end if;
          When idle =>
            Errors := (Others => '0');
            if (error_from_mgt_sig = '1') or (char_is_k_sig = '0') then
              next_state := initial;
      	    elsif (char_is_k_sig = '1') and (data_from_mgt_sig(7) = '0') and (data_from_mgt_sig(5 downto 0) = "111100") then
              if data_from_mgt_sig(6) = '0' then -- K28.1 not K28.3
                TOB_mode := '1';
              else
                TOB_mode := '0';
              end if;
              next_state := new_packet;
            else
              Bad_Packet := '0';
              next_state := idle;
            end if;
          When new_packet =>
            next_state := save_l1id;
          When save_l1id =>
            next_state := save_payload;
          When save_payload =>
      	    if char_is_k_sig = '1' then
      	      If data_from_mgt_sig(19 downto 8) = "000" & payload_length_sig then
      	        Length_Error := '0';
      	      else
      	        Length_Error := '1';
      	      end if;
      	      Errors := Multi_slice_sig & data_from_mgt_sig(31) & Bad_Packet & '0' & Length_Error;
              if (Bad_Packet = '0') and (Length_Error = '0') and (TOB_mode = TOB_trailer_OK_sig) then
                next_state := write_metadata;
              else
 				if Multi_slice_sig = TOB_trailer_OK_sig then  -- not multi-slice and not TOB_trailer_OK_sig or not TOB_mode!
 				  Errors(1) := TOB_mode;
 				end if;
                if payload_length_sig(0) = '1' then -- odd length packet, skip padding in corrective trailer
                  next_state := corrective_trailer;
                else
                  next_state := padding;
                end if;
              end if;
            else
              next_state := save_payload;
            end if;
          When padding =>
            next_state := corrective_trailer;
          When corrective_trailer =>
            next_state := write_metadata;
          When Others => -- write_metadata and anything else
            next_state := idle;
        end case;
      end if;
      state_sig <= next_state
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      TOB_mode_sig <= TOB_mode
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      Errors_sig <= Errors
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process State_machine;

end rtl;
