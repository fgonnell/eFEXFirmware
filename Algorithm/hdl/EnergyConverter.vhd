--! @file
--! @brief Energy converter module
--! @details 
--! 
--! Thie module converts energy value from the multi-linear 10-bit code
--! provided by LAr to the 16-bit linear code used by the algorithm.
--! Input values between 0 and 31 represent negative energy. This modules maps
--! them to 0 in the output.
--! @author Francesco Gonnella

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
use work.DataTypes.all;
use work.AlgoDataTypes.all;

--! @copydoc EnergyConverter.vhd
entity EnergyConverter is
  port (
    clk          : in  std_logic;
    is_hadronic  : in  std_logic := '0';
    IN_threshold : in  std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
    IN_Load      : in  std_logic;
    IN_Data      : in  std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
    OUT_Data     : out std_logic_vector(DATA_WIDTH-1 downto 0)
    );
end entity EnergyConverter;


--! @copydoc EnergyConverter.vhd
architecture str of EnergyConverter is
  -----------------------------------------------------------------------------
  -- Internal signal declarations
  -----------------------------------------------------------------------------
  signal Data : std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);

  signal IntegerOutData0 : unsigned(DATA_WIDTH-7 downto 0);
  signal IntegerOutData1 : unsigned(DATA_WIDTH-6 downto 0);
  signal IntegerOutData2 : unsigned(DATA_WIDTH-5 downto 0);
  signal IntegerOutData3 : unsigned(DATA_WIDTH-4 downto 0);
  signal IntegerOutData4 : unsigned(DATA_WIDTH-3 downto 0);
  signal IntegerOutData5 : unsigned(DATA_WIDTH-1 downto 0);
  signal OverThreshold   : std_logic;
  signal Data2, Data4    : std_logic_vector(DATA_WIDTH-1 downto 0);
  signal Saturated       : std_logic;
  signal Invalid         : std_logic;


begin  -- architecture str
  -----------------------------------------------------------------------------
  -- Component instantiations
  -----------------------------------------------------------------------------

  Sync_proc : process (clk)
  begin
    if rising_edge(clk) then
      if IN_Load = '1' then
        Data          <= IN_Data;
        OverThreshold <= '0' when unsigned(IN_Data) < unsigned(IN_threshold)       else '1';
        Invalid       <= '1' when IN_Data = "1111111110" or IN_Data = "1111111101" else '0';
        Saturated     <= '1' when IN_Data = "1111111111"                           else '0';
        Data2         <= '0'&'0'&'0'&'0'& IN_Data &'0'&'0';
        Data4         <= '0'&'0'& IN_Data &'0'&'0'&'0'&'0';

        IntegerOutData0 <= unsigned(IN_Data) - 32;
        IntegerOutData1 <= unsigned(IN_Data &'0') - 128;
        IntegerOutData2 <= unsigned(IN_Data &'0'&'0') - 512;
        IntegerOutData3 <= unsigned(IN_Data &'0'&'0'&'0') - 2048;
        IntegerOutData4 <= unsigned(IN_Data &'0'&'0'&'0'&'0') - 8192;
        IntegerOutData5 <= unsigned(IN_Data(3 downto 0) &'0'&'0'&'0'&'0'&'0'&'0'&'0'&'0'&'0'&'0'&'0'&'0') - 4137152;
      else
        Data          <= Data;
        OverThreshold <= OverThreshold;
        Invalid       <= Invalid;
        Saturated     <= Saturated;
        Data2         <= Data2;
        Data4         <= Data4;

        IntegerOutData0 <= IntegerOutData0;
        IntegerOutData1 <= IntegerOutData1;
        IntegerOutData2 <= IntegerOutData2;
        IntegerOutData3 <= IntegerOutData3;
        IntegerOutData4 <= IntegerOutData4;
        IntegerOutData5 <= IntegerOutData5;
      end if;

      OUT_Data <= "0000000000000000" when OverThreshold = '0' else
                  "1111111111111111"                                  when Saturated = '1' else
                  std_logic_vector(unsigned(Data2) + unsigned(Data4)) when is_hadronic = '1' else
                  "0000000000000000"                                  when Invalid = '1' else

                  std_logic_vector(IntegerOutData5)                                           when unsigned(Data) > 1011 else
                  (DATA_WIDTH-3 downto 0 => std_logic_vector(IntegerOutData4), others => '0') when unsigned(Data) > 767 else
                  (DATA_WIDTH-4 downto 0 => std_logic_vector(IntegerOutData3), others => '0') when unsigned(Data) > 383 else
                  (DATA_WIDTH-5 downto 0 => std_logic_vector(IntegerOutData2), others => '0') when unsigned(Data) > 191 else
                  (DATA_WIDTH-6 downto 0 => std_logic_vector(IntegerOutData1), others => '0') when unsigned(Data) > 95 else
                  (DATA_WIDTH-7 downto 0 => std_logic_vector(IntegerOutData0), others => '0') when unsigned(Data) > 31 else
                  "0000000000000000";
    end if;
  end process Sync_proc;


end architecture str;

-------------------------------------------------------------------------------
