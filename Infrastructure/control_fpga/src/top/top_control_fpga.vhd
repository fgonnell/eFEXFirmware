--! @file
--! @brief Top of the control FPGA
--! @details
--! Top module of eFEX control FPGA
--! @author Mohammed Siyad

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;
library unisim;
use unisim.VComponents.all;
library infrastructure_lib;
use infrastructure_lib.all;
use infrastructure_lib.mgt_type.all;
use infrastructure_lib.ControlFPGAPackage.all;
use infrastructure_lib.ipbus_decode_L1CaloEfex.all;
use infrastructure_lib.packet_mux_type.all;

--! @copydoc top_control_fpga.vhd
entity top_control_fpga is
  generic (
    --! Integer used to distinguish different FPGAs having a slightly different firmware
    FLAVOUR     : integer                       := 0;
    --! Date format DDMMYYYY in decimal
    GLOBAL_DATE : std_logic_vector(31 downto 0) := x"00000000";
    --! Time format  00HHMMSS in decimal
    GLOBAL_TIME : std_logic_vector(31 downto 0) := x"00000000";
    --! Short 7-digit git SHA of the repository
    GLOBAL_SHA  : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the repository (format: MMmmcccc in hex)
    GLOBAL_VER  : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the XMLs
    XML_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the XMLs
    XML_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the top folder: list file, xdcs, XMLs, tcl file and this file
    TOP_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Version of the top folder, see TOP_SHA
    TOP_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the Hog submodule
    HOG_SHA : std_logic_vector(31 downto 0) := x"00000000";
    HOG_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Short 7-digit git SHA of the Hog submodule
    CON_SHA : std_logic_vector(31 downto 0) := x"00000000";
    CON_VER : std_logic_vector(31 downto 0) := x"00000000";

    --! Version of infrastructure library (format: MMmmcccc in hex)
    INFRASTRUCTURE_LIB_VER : std_logic_vector(31 downto 0) := x"00000000";
    --! Short 7-digit git SHA
    INFRASTRUCTURE_LIB_SHA : std_logic_vector(31 downto 0) := x"00000000";
    --! Short 7-digit git SHA of the ipbus submodule
    IPBUS_LIB_SHA          : std_logic_vector(31 downto 0) := x"00000000";
    N_PROCESSORFPGA        : positive                      := 4  -- number of Processor FPGA connected in top_mgt_cfgpa
    );


  port(
    gt_clk125_p          : in  std_logic;
    gt_clk125_n          : in  std_logic;
    gmii_gtx_clk         : out std_logic;
    gmii_tx_en           : out std_logic;
    gmii_tx_er           : out std_logic;
    gmii_txd             : out std_logic_vector(7 downto 0);
    gmii_rx_clk          : in  std_logic;
    gmii_rx_dv           : in  std_logic;
    gmii_rx_er           : in  std_logic;
    gmii_rxd             : in  std_logic_vector(7 downto 0);
    phy_rstb             : out std_logic;
    clk_40_n             : in  std_logic;
    clk_40_p             : in  std_logic;

    ----mgt
    Q_CLK_GTREFCLK_PAD_N_IN : in    std_logic_vector(2 downto 0);
    Q_CLK_GTREFCLK_PAD_P_IN : in    std_logic_vector(2 downto 0);
    rxp_IN                  : in    std_logic_vector (9 downto 0);
    rxn_IN                  : in    std_logic_vector (9 downto 0);
    txp_OUT                 : out   std_logic_vector(9 downto 0);
    txn_OUT                 : out   std_logic_vector(9 downto 0);
    --------------------I2C connections
    i2c_scl_0               : inout std_logic;
    i2c_sda_0               : inout std_logic;
    i2c_rst_0               : out   std_logic;

    ------Pll connections-----
    pll_miso   : in  std_logic;
    pll_le_1   : out std_logic;
    pll_le_2   : out std_logic;
    pll_le_3   : out std_logic;
    pll_clko   : out std_logic;
    pll_mosi   : out std_logic;
    pll_lock_1 : in  std_logic;
    pll_lock_2 : in  std_logic;
    pll_lock_3 : in  std_logic;

    -------------Flash--------------
    flash_csn        : out std_logic;
    flash_mosi       : out std_logic;
    flash_miso       : in  std_logic;
    xtal_ttc_clk_sel : out std_logic;
    SYNC_B_CDCE      : out std_logic;
    POWERDN_B_CDCE   : out std_logic;


-- Signals coming to contrl FPGA

    master_tx_data1 : in std_logic_vector (9 downto 0);
    master_tx_data2 : in std_logic_vector (9 downto 0);
    master_tx_data3 : in std_logic_vector (9 downto 0);
    master_tx_data4 : in std_logic_vector (9 downto 0);

-- signals going out of cntrl FPGA

    master_rx_data1  : out std_logic_vector (9 downto 0);
    master_rx_data2  : out std_logic_vector (9 downto 0);
    master_rx_data3  : out std_logic_vector (9 downto 0);
    master_rx_data4  : out std_logic_vector (9 downto 0);
    master_tx_pause1 : out std_logic;
    master_tx_pause2 : out std_logic;
    master_tx_pause3 : out std_logic;
    master_tx_pause4 : out std_logic;
    hardware_addr    : in  std_logic_vector(11 downto 0);
    leds             : out std_logic_vector (1 downto 0);
    fpga1_done       : in  std_logic;
    fpga2_done       : in  std_logic;
    fpga3_done       : in  std_logic;
    fpga4_done       : in  std_logic;
    -- TTC information
    ttc_L1A_p        : out std_logic_vector (3 downto 0);
    ttc_L1A_n        : out std_logic_vector (3 downto 0);
    ttc_BCR_p        : out std_logic_vector (3 downto 0);
    ttc_BCR_n        : out std_logic_vector (3 downto 0);
    ttc_ECR_p        : out std_logic_vector (3 downto 0);
    ttc_ECR_n        : out std_logic_vector (3 downto 0);
    ttc_pr_rdout_p   : out std_logic_vector (3 downto 0);
    ttc_pr_rdout_n   : out std_logic_vector (3 downto 0);

-- ECRID to processor FPGAs
    ECRID_F1		: out std_logic_vector (7 downto 0);
    ECRID_F2		: out std_logic_vector (7 downto 0);
    ECRID_F3		: out std_logic_vector (7 downto 0);
    ECRID_F4		: out std_logic_vector (7 downto 0);

    serial_number : in std_logic_vector(5 downto 0);

    --- aurora hub1 links
    aurora_hub1_txp       : out std_logic_vector (3 downto 0);
    aurora_hub1_txn       : out std_logic_vector (3 downto 0);
    aurora_hub1_refclk1_p : in  std_logic;
    aurora_hub1_refclk1_n : in  std_logic;
    --- aurora hub2 links
    aurora_hub2_txp       : out std_logic_vector (3 downto 0);
    aurora_hub2_txn       : out std_logic_vector (3 downto 0);
    aurora_hub2_refclk1_p : in  std_logic;
    aurora_hub2_refclk1_n : in  std_logic;
    --- throttle signals to processor FPGAs
    cntl_RAW_rdy_F1_out   : out std_logic;
    cntl_TOB_rdy_F1_out   : out std_logic;
    cntl_RAW_rdy_F2_out   : out std_logic;
    cntl_TOB_rdy_F2_out   : out std_logic;
    cntl_RAW_rdy_F3_out   : out std_logic;
    cntl_TOB_rdy_F3_out   : out std_logic;
    cntl_RAW_rdy_F4_out   : out std_logic;
    cntl_TOB_rdy_F4_out   : out std_logic
    );


end top_control_fpga;

--! @copydoc top_control_fpga.vhd
architecture spec of top_control_fpga is
------------------- chipscope for debuging
  component ila_0
    port (
      clk    : in std_logic;
      probe0 : in std_logic_vector(67 downto 0)
      );
  end component ila_0;


--COMPONENT ila_ttc_sync

--PORT (
--      clk : IN STD_LOGIC;
--      probe0 : IN STD_LOGIC_VECTOR(143 DOWNTO 0)
--);
--END COMPONENT  ;


  component clk_ttc is
    port (
      clk40     : out std_logic;
      clk320    : out std_logic;
      clk160    : out std_logic;
      locked    : out std_logic;
      clk_in1_p : in  std_logic;
      clk_in1_n : in  std_logic
      );

  end component clk_ttc;



  component top_mgt_cfpga is
    generic(
      NProcessorFPGA : positive := 4  -- first TOB then Input Data for each FPGA in turn
      );
    port (
      start                   : in  std_logic;
      clk40                   : in  std_logic;  -- clk40 generatred from ttc clock
      clk160                  : in  std_logic;
      rx_clk160               : out std_logic_vector(3 downto 0);  --11 --- Generated rx clocks of 160MHz from the mgt pll.- Generated rx clocks of 160MHz from the mgt pll.
      rx_clk280               : out std_logic_vector(7 downto 0);
      Q_CLK_GTREFCLK_PAD_N_IN : in  std_logic_vector(2 downto 0);  --2
      Q_CLK_GTREFCLK_PAD_P_IN : in  std_logic_vector(2 downto 0);  --2
      RXN_IN                  : in  std_logic_vector(11 downto 0);   --11
      RXP_IN                  : in  std_logic_vector(11 downto 0);   --11
      TXN_OUT                 : out std_logic_vector(11 downto 0);   --7
      TXP_OUT                 : out std_logic_vector(11 downto 0);   --7
      rx_resetdone            : out std_logic_vector(11 downto 0);   --11
      rx_fsm_resetdone        : out std_logic_vector(11 downto 0);   --11
      rx_byteisaligned        : out std_logic_vector(11 downto 0);   --11
      tx_resetdone            : out std_logic_vector(11 downto 0);   --11
      tx_fsm_resetdone        : out std_logic_vector(11 downto 0);   --11
      tx_bufstatus            : out std_logic_vector (23 downto 0);  --23
      rx_realign              : out std_logic_vector (11 downto 0);  --11
      rx_disperr              : out std_logic_vector (47 downto 0);  --47  -- rx_disperr for debug
      encode_error            : out std_logic_vector (47 downto 0);  --47  --- 10/8 encoder for debug
      mgt_commadret           : out std_logic_vector(1 downto 0);  --9 (just for incoming Hub data...)
      loopback                : in  std_logic_vector(5 downto 0);  --5
      mgt_SOFT_RESET_TX_IN    : in  std_logic_vector(2 downto 0);  --2
      mgt_SOFT_RESET_RX_IN    : in  std_logic_vector(2 downto 0);  --2
      mgt_QPLLLOCK_OUT        : out std_logic_vector(2 downto 0);
      mgt_QPLLREFCLKLOST_OUT  : out std_logic_vector(2 downto 0);


-- data from MGT
      data_from_mgt_bus  : out mgt_data_array(NProcessorFPGA*2 - 1 downto 0);  -- first TOB then Input Data for each FPGA in turn
      char_is_k_bus      : out packet_sl_array(NProcessorFPGA*2 - 1 downto 0);  -- first TOB then Input Data for each FPGA in turn
      error_from_mgt_bus : out packet_sl_array(NProcessorFPGA*2 - 1 downto 0);  -- first TOB then Input Data for each FPGA in turn
      hub1_rx_data       : out std_logic_vector(31 downto 0);  -- ttc information
      hub2_rx_data       : out std_logic_vector(31 downto 0)   -- rod busy.
      );
  end component top_mgt_cfpga;

  component packet_block is
	GENERIC(
		NTTC_clients: positive := 1;
		NProcessorFPGA: positive := 1;	-- first TOB then Input Data for each FPGA in turn
		NAurora_links: positive := 1;
		DATA_RAM_ADDR_WIDTH: positive := 12;
		INPUT_RAM_ADDR_WIDTH: positive := 11
	);
	PORT (
-- clocks
		clk40			: in std_logic;
		clk_mgt_bus		: in packet_sl_array(NProcessorFPGA*2 - 1 downto 0);
		clk_320			: in std_logic;
		clk_ipb			: in std_logic;
		rst_ipb			: in std_logic;
		ECR_320			: in std_logic;
-- eFEX number
		eFEX_number		: in std_logic_vector(7 downto 0);
-- TTC FIFO data
		rst_ttc			: in std_logic;
		ttc_wr_en		: in std_logic;
		ttc_din			: in std_logic_vector(43 downto 0);
-- data from MGT
		data_from_mgt_bus	: in mgt_data_array(NProcessorFPGA*2 - 1 downto 0);		-- first TOB then Input Data for each FPGA in turn
		char_is_k_bus		: in packet_sl_array(NProcessorFPGA*2 - 1 downto 0);	-- first TOB then Input Data for each FPGA in turn
		error_from_mgt_bus	: in packet_sl_array(NProcessorFPGA*2 - 1 downto 0);	-- first TOB then Input Data for each FPGA in turn
-- data to Aurora
		payload_data_bus	: out packet_data_array(NAurora_links - 1 downto 0);
		payload_valid_bus	: out packet_sl_array(NAurora_links - 1 downto 0);
		payload_last_bus	: out packet_sl_array(NAurora_links - 1 downto 0);
		tready_data_bus		: in packet_sl_array(NAurora_links - 1 downto 0);
-- kludge for packet enable
		packet_enable_vld	: in std_logic;
		packet_enable		: in std_logic_vector(NProcessorFPGA*2 downto 0)
	);
  end component packet_block;

  component axi_stream_fifo is
    port (
      wr_rst_busy   : out std_logic;
      rd_rst_busy   : out std_logic;
      m_aclk        : in  std_logic;
      s_aclk        : in  std_logic;
      s_aresetn     : in  std_logic;
      s_axis_tvalid : in  std_logic;
      s_axis_tready : out std_logic;
      s_axis_tdata  : in  std_logic_vector (63 downto 0);
      s_axis_tkeep  : in  std_logic_vector (7 downto 0);
      s_axis_tlast  : in  std_logic;
      s_axis_tuser  : in  std_logic_vector (3 downto 0);
      m_axis_tvalid : out std_logic;
      m_axis_tready : in  std_logic;
      m_axis_tdata  : out std_logic_vector (63 downto 0);
      m_axis_tkeep  : out std_logic_vector (7 downto 0);
      m_axis_tlast  : out std_logic;
      m_axis_tuser  : out std_logic_vector (3 downto 0)
      );
  end component axi_stream_fifo;

component io_delay_control_out
generic
 (-- width of the data for the system
  SYS_W       : integer := 11;
  -- width of the data for the device
  DEV_W       : integer := 11);
port
 (
  -- From the device out to the system
  data_out_from_device    : in    std_logic_vector(DEV_W-1 downto 0);
  data_out_to_pins        : out   std_logic_vector(SYS_W-1 downto 0);

-- Input, Output delay control signals
  delay_clk               : in    std_logic;
  out_delay_reset         : in    std_logic;                    -- Active high synchronous reset for output delay
  out_delay_data_ce       : in    std_logic_vector(SYS_W -1 downto 0);                    -- Enable signal for delay
  out_delay_data_inc      : in    std_logic_vector(SYS_W -1 downto 0);                    -- Delay increment (high), decrement (low) signal
  out_delay_tap_in        : in    std_logic_vector(5*SYS_W -1 downto 0); -- Dynamically loadable delay tap value for output delay
  out_delay_tap_out       : out   std_logic_vector(5*SYS_W -1 downto 0); -- Delay tap value for monitoring output delay
  delay_locked            : out   std_logic;                    -- Locked signal from IDELAYCTRL
  ref_clock               : in    std_logic;                    -- Reference Clock for IDELAYCTRL. Has to come from BUFG.

-- Clock and reset signals
  clk_in                  : in    std_logic;                    -- Fast clock from PLL/MMCM
  io_reset                : in    std_logic);                   -- Reset signal for IO circuit
end component;

component io_delay_control_in
generic
 (-- width of the data for the system
  SYS_W       : integer := 10;
  -- width of the data for the device
  DEV_W       : integer := 10);
port
 (
  -- From the system into the device
  data_in_from_pins       : in    std_logic_vector(SYS_W-1 downto 0);
  data_in_to_device       : out   std_logic_vector(DEV_W-1 downto 0);

-- Input, Output delay control signals
  delay_clk               : in    std_logic;
  in_delay_reset          : in    std_logic;                    -- Active high synchronous reset for input delay
  in_delay_data_ce        : in    std_logic_vector(SYS_W -1 downto 0);                    -- Enable signal for delay
  in_delay_data_inc       : in    std_logic_vector(SYS_W -1 downto 0);                    -- Delay increment (high), decrement (low) signal
  in_delay_tap_in         : in    std_logic_vector(5*SYS_W -1 downto 0); -- Dynamically loadable delay tap value for input delay
  in_delay_tap_out        : out   std_logic_vector(5*SYS_W -1 downto 0); -- Delay tap value for monitoring input delay
  delay_locked            : out   std_logic;                    -- Locked signal from IDELAYCTRL
  ref_clock               : in    std_logic;                    -- Reference Clock for IDELAYCTRL. Has to come from BUFG.

-- Clock and reset signals
  clk_in                  : in    std_logic;                    -- Fast clock from PLL/MMCM
  io_reset                : in    std_logic);                   -- Reset signal for IO circuit
end component;


  -- Architecture declarations
  type ecrid_reg_array is array(3 downto 0) of std_logic_vector(7 downto 0);
  type eFEX_mapping_array is array(15 downto 0) of std_logic_vector(3 downto 0);
  constant eFEX_mapping: eFEX_mapping_array := (x"C", x"B", x"0", x"A", x"1", x"9", x"2", x"8", x"3", x"7", x"4", x"6", x"5", x"C", x"C", x"C"); -- Logical Slot Address to eFEX number
  signal efex_number: std_logic_vector(3 downto 0);
  -- ipbus signals
  signal clk125_fr, clk200, clk320, clk160, clko_p40, ipb_clk, mac_clk, locked, clk_locked, eth_locked, clk40, clk280 : std_logic;
  signal rx_clk280                                                                                                    : std_logic_vector(7 downto 0);
  signal rst_125, rst_ipb, rst_eth, rst_macclk, sys_rst, soft_rst, rsto_eth, enable_pll_rst                           : std_logic;
  signal pseudo_orbit                                                                                                 : std_logic;
  signal mac_rx_data                                                                                                  : std_logic_vector(7 downto 0);
  signal mac_rx_error, mac_rx_valid, mac_rx_last                                                                      : std_logic;
  signal mac_tx_data                                                                                                  : std_logic_vector(7 downto 0);
  signal mac_tx_error, mac_tx_last, mac_tx_ready, mac_tx_valid                                                        : std_logic;
  signal onehz, nuke, rst_ipb_ctrl                                                                                    : std_logic;
  signal ipb_master_out                                                                                               : ipb_wbus;
  signal ipb_master_in                                                                                                : ipb_rbus;
  signal ipbw                                                                                                         : ipb_wbus_array(N_SLAVES-1 downto 0);
  signal ipbr, ipbr_d                                                                                                 : ipb_rbus_array(N_SLAVES-1 downto 0);
  signal ipb_in                                                                                                       : ipb_rbus;
  signal ipb_out                                                                                                      : ipb_wbus;
  signal master_rx_data1_int, master_rx_data2_int, master_rx_data3_int, master_rx_data4_int                           : std_logic_vector(8 downto 0);
  signal master_tx_data1_int, master_tx_data2_int, master_tx_data3_int, master_tx_data4_int                           : std_logic_vector(8 downto 0);
  signal flash_clk, flash_led, scl_enb_0, sda_enb_0, scl_enb_1, sda_enb_1, scl_o_0, sda_o_0, scl_o_1, sda_o_1         : std_logic;
  signal Module_ID, status                                                                                            : std_logic_vector(31 downto 0);
  signal master_tx_err1, master_tx_err2, master_tx_err3, master_tx_err4                                               : std_logic;
  -----------------------------------------------------------------------------------------------------
  signal control_reg, pulse_reg, reconfig_reg                                                                         : std_logic_vector (31 downto 0);
  signal clkfb_in, gmii_rx_clk_int, clkfb_out                                                                         : std_logic;
  signal select_pll, select_flash                                                                                     : std_logic_vector (1 downto 0);
  signal pll_en, flash_csn_int, trigger_reconfig, reconfig, en_reset                                                  : std_logic;
  signal data_reg0, data_reg1, reg128_latch                                                                           : std_logic;
  signal flash_addr_int                                                                                               : std_logic_vector(2 downto 0);
  signal flash_mosi_int, f5_flash_disable_int, f5_flash_csn_int                                                       : std_logic;
  signal f1_flash_sel_int, f2_flash_sel_int, f3_flash_sel_int, f4_flash_sel_int                                       : std_logic;
  signal probe                                                                                                        : std_logic_vector(12 downto 0);
  signal start                                                                                                        : std_logic;
  -- mgt signals
  signal hub1_rx_data, hub2_rx_data                                                                                   : std_logic_vector(31 downto 0);
  signal rx_er_count                                                                                                  : std_logic_vector(31 downto 0);
  signal hub1_combined_ttc, hub1_combined_ttc_i, data_out_reg128, hub2_combined_ttc, hub2_combined_ttc_i, ttc_hub2_syn  : std_logic_vector(127 downto 0);
  signal mgt_enable                                                                                                   : std_logic_vector(3 downto 0);
  signal bc_reg_sel, mux_sel                                                                                          : std_logic_vector(15 downto 0);
  signal delay_cntr_0, delay_cntr_1                                                                                   : std_logic_vector(3 downto 0);
  signal rx_resetdone_mgt114                                                                                          : std_logic_vector (1 downto 0);
  signal mgt_commadret                                                                                                : std_logic_vector (1 downto 0);
  signal rx_clk160                                                                                                    : std_logic_vector(3 downto 0);
  signal RXN_IN_i, RXP_IN_i                                                                                           : std_logic_vector(11 downto 0);
  signal encode_114, rx_disperr_114                                                                                   : std_logic_vector(3 downto 0);
  signal ttc_BCR, ttc_L1A, ttc_ECR, ttc_pr_rdout                                                                      : std_logic;
  signal ttc_ECRID                                                                                                    : std_logic_vector(7 downto 0);
  signal probe0                                                                                                       : std_logic_vector(67 downto 0);
  signal sof_topo_fpga1, sof_topo_fpga2, sof_topo_fpga3, sof_topo_fpga4                                               : std_logic;
  signal sof_raw_fpga1, sof_raw_fpga2, sof_raw_fpga3, sof_raw_fpga4                                                   : std_logic;
  signal rx_resetdone, tx_resetdone, tx_fsm_resetdone, rx_fsm_resetdone, rx_realign, rx_byteisaligned                 : std_logic_vector(11 downto 0);
  signal loopback                                                                                                     : std_logic_vector (5 downto 0);
  signal rx_disperr, encode_error                                                                                     : std_logic_vector(47 downto 0);
  signal qpll_lock, qpll_refclklost, softreset_tx, softreset_rx                                                       : std_logic_vector(2 downto 0);
  signal tx_bufstatus                                                                                                 : std_logic_vector(23 downto 0);
  signal gmii_rx_er_i, reset, hub2_combined_ttc_latch, link_reset_hub1, link_reset_hub2                                         : std_logic;

---Aurora hub1 signal declarations
  signal s_axi_tx_tdata_hub1                                                                               : std_logic_vector (63 downto 0);
  signal s_axi_tx_tvalid_hub1, s_axi_tx_tready_hub1, s_axi_tx_tlast_hub1                                   : std_logic;
  signal s_axi_tx_tkeep_hub1                                                                               : std_logic_vector (7 downto 0);
  signal s_axi_ufc_tx_req_hub1, s_axi_ufc_tx_ack_hub1                                                      : std_logic;
  signal s_axi_ufc_tx_ms_hub1                                                                              : std_logic_vector (2 downto 0);
  signal aurora_status_hub1                                                                                : std_logic_vector(31 downto 0);
  signal tx_lane_up_hub1                                                                                   : std_logic_vector (3 downto 0);
  signal tx_channel_up_hub1, tx_hard_err_hub1, tx_lock_hub1, pll_not_locked_hub1, aurora_tx_resetdone_hub1 : std_logic;
  signal aurora_user_clk_hub1, sys_reset_out_hub1, init_clk_out_hub1                                       : std_logic;

---Aurora hub2 signal declarations
  signal s_axi_tx_tdata_hub2                                                                               : std_logic_vector (63 downto 0);
  signal s_axi_tx_tvalid_hub2, s_axi_tx_tready_hub2, s_axi_tx_tlast_hub2                                   : std_logic;
  signal s_axi_tx_tkeep_hub2                                                                               : std_logic_vector (7 downto 0);
  signal s_axi_ufc_tx_req_hub2, s_axi_ufc_tx_ack_hub2                                                      : std_logic;
  signal s_axi_ufc_tx_ms_hub2                                                                              : std_logic_vector (2 downto 0);
  signal aurora_status_hub2                                                                                : std_logic_vector(31 downto 0);
  signal tx_lane_up_hub2                                                                                   : std_logic_vector (3 downto 0);
  signal tx_channel_up_hub2, tx_hard_err_hub2, tx_lock_hub2, pll_not_locked_hub2, aurora_tx_resetdone_hub2 : std_logic;
  signal aurora_user_clk_hub2, sys_reset_out_hub2, init_clk_out_hub2                                       : std_logic;

  signal payload_data                                                                               : std_logic_vector(63 downto 0);
  signal payload_valid, payload_last, tready_data, first                                            : std_logic;
  signal master_link_down1_int, master_link_down2_int, master_link_down3_int, master_link_down4_int : std_logic;
  signal fifo_empty, valid, ECR_320                                                                 : std_logic;
  signal bcn_cntr, BCN                                                                              : std_logic_vector (11 downto 0);

-- Readout signals
  signal ttc_din            : std_logic_vector(43 downto 0);
-- data from MGT
  signal clk_mgt_bus        : packet_sl_array(N_PROCESSORFPGA*2 - 1 downto 0);
  signal data_from_mgt_bus  : mgt_data_array(N_PROCESSORFPGA*2 - 1 downto 0);
  signal char_is_k_bus      : packet_sl_array(N_PROCESSORFPGA*2 - 1 downto 0);
  signal error_from_mgt_bus : packet_sl_array(N_PROCESSORFPGA*2 - 1 downto 0);
-- data to Aurora FIFOs
  signal payload_data_bus   : packet_data_array(0 downto 0);
  signal payload_valid_bus  : packet_sl_array(0 downto 0);
  signal payload_last_bus   : packet_sl_array(0 downto 0);
  signal tready_data_bus    : packet_sl_array(0 downto 0);

  signal fpga_number                                     : integer;
  signal golden                                          : boolean;
  signal TXN_OUT_i, TXP_OUT_i                            : std_logic_vector(11 downto 0);
  signal ftm_ttc_mode                                    : std_logic;
  signal sel_mode                                        : std_logic_vector(4 downto 0);
  signal d_i, q_i                                        : std_logic_vector (11 downto 0);
  signal ttc_l1A_i, ttc_bcr_i, ttc_ecr_i, ttc_pr_rdout_i : std_logic_vector(3 downto 0);
  signal ttc_ECRID_i                                     : ecrid_reg_array;
  signal ttc_ecr_tff: std_logic;
  signal ecr320_tff_buf: std_logic_vector(1 downto 0);

---------------------------------------------------------------------------------------------------
-- ipbus buses signals
   signal master_tx1_int, master_tx2_int, master_tx3_int, master_tx4_int : std_logic_vector(9 downto 0);
   signal master_pause1_int,master_pause2_int,master_pause3_int,master_pause4_int : std_logic;
   signal master_pause1_reg,master_pause2_reg,master_pause3_reg,master_pause4_reg : std_logic;
   signal master_rx1_int, master_rx2_int, master_rx3_int, master_rx4_int : std_logic_vector(9 downto 0);
   signal master_rx1_reg, master_rx2_reg, master_rx3_reg, master_rx4_reg : std_logic_vector(9 downto 0);
   signal ipbus_ctrl_delay_in, ipbus_status_delay_in : std_logic_vector(31 downto 0);
   signal ipbus_ctrl_delay_out, ipbus_status_delay_out : std_logic_vector(31 downto 0);
   signal dummy_tx1,dummy_tx2,dummy_tx3,dummy_tx4 : std_logic_vector(49 downto 5);
   signal dummy_rx1,dummy_rx2,dummy_rx3,dummy_rx4 : std_logic_vector(54 downto 5);
---------------------------------------------------------------------------------------------------

  attribute KEEP: string;
  attribute KEEP of mac_clk: signal is "TRUE";

  attribute ASYNC_REG: string;
  attribute ASYNC_REG of ttc_ecr_tff: signal is "TRUE";
  attribute ASYNC_REG of ecr320_tff_buf: signal is "TRUE";

begin

  fpga_number <= F_FPGA_NUMBER(FLAVOUR);
  golden      <= F_GOLDEN(FLAVOUR);
  efex_number <= eFEX_mapping(to_integer(unsigned(hardware_addr(3 downto 0))));

  cntl_RAW_rdy_F1_out <= '1';
  cntl_TOB_rdy_F1_out <= '1';
  cntl_RAW_rdy_F2_out <= '1';
  cntl_TOB_rdy_F2_out <= '1';
  cntl_RAW_rdy_F3_out <= '1';
  cntl_TOB_rdy_F3_out <= '1';
  cntl_RAW_rdy_F4_out <= '1';
  cntl_TOB_rdy_F4_out <= '1';
  ttc_hub2_syn        <= hub2_combined_ttc;

  combined_ttc_ila : ila_0
    port map (
      clk                 => clk40,
      probe0(11 downto 0) => hub1_combined_ttc(11 downto 0),	-- K28.5, TOB XOFF, Input Data XOFF, System_Reset Hub1
      probe0(15 downto 12) => hub1_combined_ttc(19 downto 16),	-- L1A, BCR, ECR, Priv Readout (Hub 1)
      probe0(47 downto 16) => hub1_combined_ttc(63 downto 32),	-- L1ID (Hub 1)
      probe0(48)          => hub1_combined_ttc(96),				-- link_reset Hub1
      probe0(49)          => hub1_combined_ttc(100),			-- link_up Hub1
      probe0(50)          => hub1_combined_ttc(104),			-- link_enable Hub1
      probe0(51)          => '0',
      probe0(63 downto 52) => hub2_combined_ttc(11 downto 0),	-- K28.5, TOB XOFF, Input Data XOFF, System_Reset Hub2
      probe0(64)          => hub2_combined_ttc(96),				-- link_reset Hub2
      probe0(65)          => hub2_combined_ttc(100),				-- link_up Hub2
      probe0(66)          => hub2_combined_ttc(104),				-- link_enable Hub2
      probe0(67)          => '0'
      );

  ttc_din <= hub1_combined_ttc(63 downto 32) & bcn_cntr;

  output_ila : ila_0
    port map (
      clk                 => aurora_user_clk_hub1,
      probe0(63 downto 0) => s_axi_tx_tdata_hub1,
      probe0(64)          => s_axi_tx_tvalid_hub1,
      probe0(65)          => s_axi_tx_tlast_hub1,
      probe0(66)          => s_axi_tx_tready_hub1,
      probe0(67)          => '0'
      );


-----------------------------------------------------------------------------------
-------lvds
--------------------------------------------------------------------------
  ttc_bcr_gen : for i in 0 to 3 generate
    bcr : OBUFDS
      port map(
        o  => ttc_BCR_p(i),             --! ttc bcr fanout to the process fpgas
        ob => ttc_BCR_n(i),
        i  => ttc_BCR_i(i)
        );

  end generate;

  ttc_L1A_gen : for i in 0 to 3 generate
    L1A : OBUFDS
      port map(
        o  => ttc_L1A_p(i),             --! ttc L1A fanout to the process fpgas
        ob => ttc_L1A_n(i),
        i  => ttc_L1A_i(i)
        );

  end generate;


  ttc_ecr_gen : for i in 0 to 3 generate
    ecr : OBUFDS
      port map(
        o  => ttc_ECR_p(i),             --! ttc ecr fanout to the process fpgas
        ob => ttc_ECR_n(i),
        i  => ttc_ECR_i(i)
        );

  end generate;

  ttc_pr_rdout_gen : for i in 0 to 3 generate
    pr_rd : OBUFDS
      port map(
        o  => ttc_pr_rdout_p(i),  --! ttc privilage read out fanout to the process fpgas
        ob => ttc_pr_rdout_n(i),
        i  => ttc_pr_rdout_i(i)
        );

  end generate;

-- ECRID LVCMOS

	ECRID_F1 <= ttc_ECRID_i(0);
	ECRID_F2 <= ttc_ECRID_i(1);
	ECRID_F3 <= ttc_ECRID_i(2);
	ECRID_F4 <= ttc_ECRID_i(3);

-------------------------------------------------------------
-- i2c bus settings
------------------------------------------------------------
  i2c_scl_0 <= scl_o_0 when (scl_enb_0 = '0') else 'Z';
  i2c_sda_0 <= sda_o_0 when (sda_enb_0 = '0') else 'Z';
  i2c_rst_0 <= '1';  -- This will be assigned active low reset signal

--------------------------------------------------------------------
  phy_rstb <= not rst_ipb;

  leds(0) <= clk_locked and onehz;
  leds(1) <= pll_lock_1 or pll_lock_2 or pll_lock_3;
  -- control fpga
  --  moddule status assignement
  Module_ID <= (31           => '1',    -- user (1) / golden image (0)
                30 downto 28 => "000",  -- FPGA flavour if 0 => control FPGA
                27 downto 26 => "00",  -- space for geographic address in process
                25 downto 20 => serial_number,
                19 downto 16 => hardware_addr(11 downto 8),  -- shelf address
                15 downto 12 => efex_number,   -- true eFEX number!
                11 downto 0  => X"efe"  -- module ID
                );

  status <= x"0000000" & '0' & pll_lock_3 & pll_lock_2 & pll_lock_1;

  trigger_reconfig <= reconfig and clk_locked;  -- if active it will reconfigure the FPGA5
  reconfig         <= reconfig_reg(30);  -- if active it will reconfigure the spi flash.
  xtal_ttc_clk_sel <= control_reg(0);  -- If low it selects local crystal clcok of 40.08MHz. If high it selects  LHC TTC clock
  enable_pll_rst   <= control_reg(6);  -- This will generate active low reset pulse that will synchronous the PLLs
  POWERDN_B_CDCE   <= not control_reg(2);       --'1';
  start            <= control_reg(1);  -- will start the synchronisation of incoming ttc information data with 40 MHz
  ftm_ttc_mode     <= control_reg(3);   -- delay TTC info to processor FPGAs

  master_link_down1_int <= not fpga1_done;
  master_link_down2_int <= not fpga2_done;
  master_link_down3_int <= not fpga3_done;
  master_link_down4_int <= not fpga4_done;


  global_ctrl_fabric : entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV      => N_SLAVES,
                SEL_WIDTH => ipbus_sel_width,
                GENERATE_ILA => true)
    port map(
      clk			  => ipb_clk,
      ipb_in          => ipb_out,
      ipb_out         => ipb_in,
      sel             => ipbus_sel_L1CaloEfex(ipb_out.ipb_addr),
      ipb_to_slaves   => ipbw,
      ipb_from_slaves => ipbr
      );


---------------------------------------------------------------------------------
  ----- common id registers slave
  ---------------------------------------------------------------------------------------

  common_reg : entity infrastructure_lib.common_id_registers
    port map (
      ipb_clk     => ipb_clk,
      ipb_rst     => rst_ipb,
      ipb_in      => ipbw(N_SLV_COMMON_ID_VERSION),
      ipb_out     => ipbr(N_SLV_COMMON_ID_VERSION),
      Module_ID   => Module_ID,
      xml_version => XML_VER,
      xml_Gitsha  => XML_SHA,
      fw_version  => GLOBAL_VER,
      fw_Gitsha   => GLOBAL_SHA,
      build_date  => GLOBAL_DATE,
      build_time  => GLOBAL_TIME

      );

  aurora_reg : entity infrastructure_lib.aurora_registers

    port map (
      ipb_clk         => ipb_clk,
      ipb_rst         => rst_ipb,
      ipb_in          => ipbw(N_SLV_AURORA_STATUS),
      ipb_out         => ipbr(N_SLV_AURORA_STATUS),
      aurora_status_1 => aurora_status_hub1,
      aurora_status_2 => aurora_status_hub2
      );

  U_0 : entity infrastructure_lib.top_udp_config_FPGA
    port map (
      ipb_clk           => ipb_clk,
      ipb_in            => ipb_in,      --ipb_master_in,
      ipb_out           => ipb_out,     --ipb_master_out,
      hardware_addr     => hardware_addr,
      serial_number     => serial_number,
      mac_clk           => mac_clk,
      mac_rx_data       => mac_rx_data,
      mac_rx_error      => mac_rx_error,
      mac_rx_last       => mac_rx_last,
      mac_rx_valid      => mac_rx_valid,
      mac_tx_ready      => mac_tx_ready,
      master_tx_data1   => master_tx_data1_int,
      master_tx_data2   => master_tx_data2_int,
      master_tx_data3   => master_tx_data3_int,
      master_tx_data4   => master_tx_data4_int,
      master_tx_err1    => master_tx_err1,
      master_tx_err2    => master_tx_err2,
      master_tx_err3    => master_tx_err3,
      master_tx_err4    => master_tx_err4,
      rst_ipb           => rst_ipb_ctrl,
      rst_macclk        => rst_macclk,
      mac_tx_data       => mac_tx_data,
      mac_tx_error      => mac_tx_error,
      mac_tx_last       => mac_tx_last,
      mac_tx_valid      => mac_tx_valid,
      master_rx_data1   => master_rx_data1_int,
      master_rx_data2   => master_rx_data2_int,
      master_rx_data3   => master_rx_data3_int,
      master_rx_data4   => master_rx_data4_int,
      master_tx_pause1  => master_pause1_int, --to iodelay
      master_tx_pause2  => master_pause2_int, --to iodelay
      master_tx_pause3  => master_pause3_int, --to iodelay
      master_tx_pause4  => master_pause4_int, --to iodelay
      master_link_down1 => master_link_down1_int,
      master_link_down2 => master_link_down2_int,
      master_link_down3 => master_link_down3_int,
      master_link_down4 => master_link_down4_int
      );


  ------- -- Interconnection between control FPGA and Process FPGA1------------------

  U_1 : entity infrastructure_lib.interconnect
    port map (
      mac_clk         => mac_clk,
      master_rx_data  => master_tx1_int,     -- coming from FPGA1
      rst_macclk      => rst_macclk,
      tx_data         => master_rx_data1_int,
      master_rx_err   => master_tx_err1,  -- is set high if data has parity error
      process_tx_data => master_rx1_int,     -- goes to iodelay and then to FPGA1
      rx_data         => master_tx_data1_int  -- goes to top_udp_config block
      );

  ------- -- Interconnection between control FPGA and Process FPGA2------------------
  U_2 : entity infrastructure_lib.interconnect
    port map (
      mac_clk         => mac_clk,
      master_rx_data  => master_tx2_int,     -- coming from FPGA2
      rst_macclk      => rst_macclk,
      tx_data         => master_rx_data2_int,
      master_rx_err   => master_tx_err2,  -- is set high if data has parity error
      process_tx_data => master_rx2_int,     -- goes to iodelay and then to FPGA2
      rx_data         => master_tx_data2_int  -- goes to top_udp_config block
      );

------- -- Interconnection between control FPGA and Process FPGA3------------------

  U_3 : entity infrastructure_lib.interconnect
    port map (
      mac_clk         => mac_clk,
      master_rx_data  => master_tx3_int,     -- coming from FPGA3
      rst_macclk      => rst_macclk,
      tx_data         => master_rx_data3_int,
      master_rx_err   => master_tx_err3,  -- is set high if data has parity error
      process_tx_data => master_rx3_int,     -- goes to iodelay and then to FPGA3
      rx_data         => master_tx_data3_int  -- goes to top_udp_config block
      );
  ------- -- Interconnection between control FPGA and Process FPGA4------------------

  U_4 : entity infrastructure_lib.interconnect
    port map (
      mac_clk         => mac_clk,
      master_rx_data  => master_tx4_int,     -- coming from FPGA4
      rst_macclk      => rst_macclk,
      tx_data         => master_rx_data4_int,
      master_rx_err   => master_tx_err4,  -- is set high if data has parity error
      process_tx_data => master_rx4_int,     -- goes to iodelay and then to FPGA4
      rx_data         => master_tx_data4_int  -- goes to top_udp_config block
      );

  --    DCM clock generation for internal bus, ethernet
  clocks : entity infrastructure_lib.clocks_7s_extphy
    port map(
      sysclk_p      => gt_clk125_p,
      sysclk_n      => gt_clk125_n,
      clko_125      => mac_clk,
      clko_200      => clk200,
      clko_ipb      => ipb_clk,
      locked        => clk_locked,
      nuke          => '0',             --nuke,
      soft_rst      => '0',             --soft_rst,
      rsto_125      => rst_macclk,
      rsto_ipb      => rst_ipb,
      rsto_ipb_ctrl => rst_ipb_ctrl,
      onehz         => onehz
      );




-- --- Ethernet MAC core and PHY interface
  eth : entity infrastructure_lib.eth_7s_gmii
    port map(
      clk125       => mac_clk,
      clk200       => clk200,
      rst          => rst_macclk,
      gmii_gtx_clk => gmii_gtx_clk,
      gmii_txd     => gmii_txd,
      gmii_tx_en   => gmii_tx_en,
      gmii_tx_er   => gmii_tx_er,
      gmii_rx_clk  => gmii_rx_clk,
      gmii_rxd     => gmii_rxd,
      gmii_rx_dv   => gmii_rx_dv,
      gmii_rx_er   => gmii_rx_er,
      tx_data      => mac_tx_data,
      tx_valid     => mac_tx_valid,
      tx_last      => mac_tx_last,
      tx_error     => mac_tx_error,
      tx_ready     => mac_tx_ready,
      rx_data      => mac_rx_data,
      rx_valid     => mac_rx_valid,
      rx_last      => mac_rx_last,
      rx_error     => mac_rx_error

      );


--  ----------------------------------------------------------
  --- control fpga mgts.
  ---------------------------------------------------------------

-- RXN_IN_i <= RXN_IN ; --& "00" ;
-- RXP_IN_i <= RXP_IN ;  --& "00" ;

  RXN_IN_i <= "00" & RXN_IN;
  RXP_IN_i <= "00" & RXP_IN;
  TXN_OUT  <= TXN_OUT_i(9 downto 0);
  TXP_OUT  <= TXP_OUT_i(9 downto 0);


  MGT_TX_RX : top_mgt_cfpga

    generic map (NPROCESSORFPGA => N_PROCESSORFPGA)
    port map (
      start                   => start,
      clk40                   => clk40,
      clk160                  => clk160,
      rx_clk160               => rx_clk160,
      rx_clk280               => rx_clk280,
      Q_CLK_GTREFCLK_PAD_N_IN => Q_CLK_GTREFCLK_PAD_N_IN,  -- (1 downto 0),
      Q_CLK_GTREFCLK_PAD_P_IN => Q_CLK_GTREFCLK_PAD_P_IN,  -- (1 downto 0),
      RXN_IN                  => RXN_IN_i,
      RXP_IN                  => RXP_IN_i,
      TXN_OUT                 => TXN_OUT_i,
      TXP_OUT                 => TXP_OUT_i,
      rx_resetdone            => rx_resetdone,
      rx_fsm_resetdone        => rx_fsm_resetdone,
      rx_byteisaligned        => rx_byteisaligned,
      tx_resetdone            => tx_resetdone,
      tx_fsm_resetdone        => tx_fsm_resetdone,
      tx_bufstatus            => tx_bufstatus,
      rx_realign              => rx_realign,
      rx_disperr              => rx_disperr,
      encode_error            => encode_error,
      mgt_commadret           => mgt_commadret,
      loopback                => loopback,
      mgt_SOFT_RESET_TX_IN    => softreset_tx,
      mgt_SOFT_RESET_RX_IN    => softreset_rx,
      mgt_QPLLLOCK_OUT        => qpll_lock,
      mgt_QPLLREFCLKLOST_OUT  => qpll_refclklost,
-- data from MGT
      data_from_mgt_bus       => data_from_mgt_bus,  -- first TOB then Input Data for each FPGA in turn
      char_is_k_bus           => char_is_k_bus,  -- first TOB then Input Data for each FPGA in turn
      error_from_mgt_bus      => error_from_mgt_bus,  -- first TOB then Input Data for each FPGA in turn
      hub1_rx_data            => hub1_rx_data,
      hub2_rx_data            => hub2_rx_data
      );

---- mgt ipbus registers

  mgt_slaves : entity infrastructure_lib.mgt_cntrl_slaves
    port map (
      rx_clk160        => rx_clk160,
      rx_clk280        => rx_clk280,
      ipb_clk          => ipb_clk,
      ipb_rst          => rst_ipb,
      ipb_in           => ipbw(N_SLV_CNTRL_MGT),
      ipb_out          => ipbr(N_SLV_CNTRL_MGT),
      --mgt quad
      loopback         => loopback,
      softreset_tx     => softreset_tx,
      softreset_rx     => softreset_rx,
      qpll_lock        => qpll_lock,
      qpll_refclklost  => qpll_refclklost,
      rx_resetdone     => rx_resetdone,
      rx_fsm_resetdone => rx_fsm_resetdone,
      rx_byteisaligned => rx_byteisaligned,
      tx_resetdone     => tx_resetdone,
      tx_fsm_resetdone => tx_fsm_resetdone,
      tx_bufstatus     => tx_bufstatus,
      rx_realign       => rx_realign,
      rx_disperr       => rx_disperr,
      encode_error     => encode_error,
      bc_reg_sel       => bc_reg_sel,
      delay_cntr_0     => delay_cntr_0,
      delay_cntr_1     => delay_cntr_1,
      mux_sel          => mux_sel,
      mgt_enable       => mgt_enable

      );


-------------------------------------------------------------------------
--- Hub 1 synchronisation block
----------------------------------------------------------------------

  synch_ttc_combined : entity infrastructure_lib.top_cntrl_synch

    port map (
      rx_clk160    => rx_clk160(2),  --8  -- rx clock of the mgt114
      TTC_clk      => clk40,            -- ttc clock of 40 MHz
      reset        => reset,
      enable_mgt   => mgt_enable(0),    -- enaable of the incoming rx data
      MGT_Commadet => mgt_commadret(0),  --8 -- comma detected for incoming data
      reg_sel      => bc_reg_sel(11 downto 8),  -- setting BC mux              --
      mux_sel      => mux_sel(11 downto 8),  -- setting the first stage mux
      delay_num    => delay_cntr_0,     -- delay count of first stage
      start        => start,  -- start pulse for the calibration to start
      rx_resetdone => rx_resetdone(8),  --rx_resetdone(2),
      reg128_latch => reg128_latch,
      data_out     => hub1_combined_ttc_i,  -- ttc_information synch goes to process fpgasdata_out_reg128, ---data_out_reg128,
      data_in      => hub1_rx_data      -- in coming ttc information data
      );

  process(clk40)
  begin
    if clk40' event and clk40 = '1' then
      hub1_combined_ttc <= hub1_combined_ttc_i;
      ttc_L1A       <= hub1_combined_ttc(16);  -- L1 accept to indicate when an event has been accepted
      ttc_BCR       <= hub1_combined_ttc(17);  -- bunch counter reset. used to reset local bc counters
      ttc_ECR       <= hub1_combined_ttc(18);  -- event counter reset
      if hub1_combined_ttc(16) = '1' then
      	ttc_ECRID     <= hub1_combined_ttc(63 downto 56);  -- Incoming ECRID
	    ttc_pr_rdout  <= hub1_combined_ttc(19) or control_reg(4);  -- indicate full data readout has been requested
      else
      	ttc_ECRID     <= ttc_ECRID;  -- Latched ECRID
	    ttc_pr_rdout  <= ttc_pr_rdout;  -- indicate full data readout has been requested
      end if;
    end if;
  end process;

  link_reset_hub1 <= hub1_combined_ttc(96);

  ttc_din <= hub1_combined_ttc(63 downto 32) & bcn_cntr;

  bcn_counter : process(clk40)
    variable bcn_count : unsigned (11 downto 0) := (others => '0');
  begin

    if clk40' event and clk40 = '1' then
      if bcn_count = 3563 then          -- wrap around to zero at end of orbit
        bcn_count := (others => '0');
      elsif hub1_combined_ttc(17) = '1' then  -- reset to 1 at start of orbit
        bcn_count := x"001";
      else
        bcn_count := bcn_count + 1;
      end if;
      bcn_cntr <= std_logic_vector(bcn_count);
    end if;
  end process bcn_counter;

---------------------------------------------------------------------------------------------------------------------------------------------
---- Hub 2 synchronisation block
--------------------------------------------------------------------------------------------

  synch_hub2_combined_ttc : entity infrastructure_lib.top_cntrl_synch
    port map (
      rx_clk160    => rx_clk160(3),  --9 -- rx clock of the mgt114
      TTC_clk      => clk40,            -- ttc clock of 40 MHz
      reset        => reset,
      enable_mgt   => mgt_enable(1),    -- enaable of the incoming rx data
      MGT_Commadet => mgt_commadret(1),  --9 comma detected for incoming data
      reg_sel      => bc_reg_sel(15 downto 12),  -- setting BC mux              --
      mux_sel      => mux_sel(15 downto 12),  -- setting the first stage mux
      delay_num    => delay_cntr_1,     -- delay count of first stage
      start        => start,  -- start pulse for the calibration to start
      rx_resetdone => rx_resetdone(9),  --rx_resetdone(3),
      reg128_latch => hub2_combined_ttc_latch,
      data_out     => hub2_combined_ttc_i,  -- ttc_information synch goes to process fpgasdata_out_reg128, ---data_out_reg128,
      data_in      => hub2_rx_data  --rod_busy        -- in coming ttc information data
      );

  process(clk40)
  begin
    if clk40' event and clk40 = '1' then
      hub2_combined_ttc      <= hub2_combined_ttc_i;
    end if;
  end process;

  link_reset_hub2 <= hub2_combined_ttc(96);

  d_i <= ttc_ECRID & ttc_pr_rdout & ttc_ECR & ttc_BCR & ttc_L1A;

  ftm_ttc_mode_sel : process(clk40)
  begin
    if clk40' event and clk40 = '1' then
      if ftm_ttc_mode = '1' then
        sel_mode <= "11111";
      else
        sel_mode <= (others => '0');
      end if;
    end if;
  end process;

  shift_mux12 : for i in 0 to 11 generate
    SRLC32E_inst_12 : SRLC32E
      generic map (
        INIT => X"00000000")
      port map (
        Q   => q_i(i),                  -- SRL data output
        Q31 => Open,
        A   => sel_mode,                -- Select input
        CE  => '1',                     -- Clock enable input
        CLK => CLK40,                   -- Clock input
        D   => d_i(i)                   -- SRL data input
        );
  end generate shift_mux12;

-- register ttc information signal selection for IOBs to Processor FPGAs
  pipe_gen : for i in 0 to 3 generate
    pipe_ttc : process(clk40)
    begin
      if clk40' event and clk40 = '1' then
        ttc_L1A_i(i)		<= q_i(0);
        ttc_BCR_i(i)		<= q_i(1);
        ttc_ECR_i(i)		<= q_i(2);
        ttc_pr_rdout_i(i)	<= q_i(3);
        ttc_ECRID_i(i)		<= q_i(11 downto 4);
      end if;
    end process;
  end generate pipe_gen;

-- generate ECR in 320 MHz domain

ttc_ecr_clk40: process(clk40)
	begin
		if rising_edge(clk40) then
			if reset = '1' then
				ttc_ecr_tff <= '0';
			elsif ttc_ECR = '1' then
-- infer a toggle flip flop in source domain
				ttc_ecr_tff <= not ttc_ecr_tff;
			else
				ttc_ecr_tff <= ttc_ecr_tff;
			end if;
		end if;
	end process ttc_ecr_clk40;

ttc_ecr_clk320: process(clk320)
	begin
		if rising_edge(clk320) then
			ecr320_tff_buf <= ecr320_tff_buf(0) & ttc_ecr_tff;
			if (ecr320_tff_buf(1) xor ecr320_tff_buf(0)) = '1' then
				ECR_320 <= '1';
			else
				ECR_320 <= '0';
			end if;
		end if;
	end process ttc_ecr_clk320;

  clk_mgt_bus_gen : for i in 0 to N_PROCESSORFPGA*2 - 1 generate
    clk_mgt_bus(i) <= rx_clk280(i);
  end generate clk_mgt_bus_gen;

  readout_packet_block : packet_block
    generic map (
      NTTC_clients         => 1,
      NProcessorFPGA       => N_PROCESSORFPGA,
      NAurora_links        => 1,
      DATA_RAM_ADDR_WIDTH  => 12,
      INPUT_RAM_ADDR_WIDTH => 12
      )
    port map (
-- clocks
      clk40              => clk40,
      clk_mgt_bus        => clk_mgt_bus,         --(1 downto 0),
      clk_320            => clk320,
      clk_ipb            => ipb_clk,
      rst_ipb            => rst_ipb,
      ECR_320			 => ECR_320,
-- Shelf Address and eFEX number
      eFEX_number(7 downto 4) => hardware_addr(11 downto 8),
      eFEX_number(3 downto 0) => efex_number,
-- TTC FIFO data
      rst_ttc            => reset,
      ttc_wr_en          => hub1_combined_ttc(16),
      ttc_din            => ttc_din,
-- data from MGT
      data_from_mgt_bus  => data_from_mgt_bus,   --( 1 downto 0),
      char_is_k_bus      => char_is_k_bus,       --     (1 downto 0),
      error_from_mgt_bus => error_from_mgt_bus,  --(1 downto 0),
-- data to Aurora
      payload_data_bus   => payload_data_bus,
      payload_valid_bus  => payload_valid_bus,
      payload_last_bus   => payload_last_bus,
      tready_data_bus    => tready_data_bus,
      packet_enable_vld	 => control_reg(7),
      packet_enable(8 downto 0) => control_reg(16 downto 8)
      );

  hub1_axi_stream_fifo : axi_stream_fifo
    port map (
      wr_rst_busy   => open,
      rd_rst_busy   => open,
      m_aclk        => aurora_user_clk_hub1,
      s_aclk        => clk320,
      s_aresetn     => Locked,  -- Global reset: This signal is active-Low.
      s_axis_tvalid => payload_valid_bus(0),
      s_axis_tready => tready_data_bus(0),
      s_axis_tdata  => payload_data_bus(0),
      s_axis_tkeep  => (others => '0'),
      s_axis_tlast  => payload_last_bus(0),
      s_axis_tuser  => (others => '0'),
      m_axis_tvalid => s_axi_tx_tvalid_hub1,
      m_axis_tready => s_axi_tx_tready_hub1,
      m_axis_tdata  => s_axi_tx_tdata_hub1,
      m_axis_tkeep  => s_axi_tx_tkeep_hub1,
      m_axis_tlast  => s_axi_tx_tlast_hub1,
      m_axis_tuser  => open
      );

  hub2_axi_stream_fifo : axi_stream_fifo
    port map (
      wr_rst_busy   => open,
      rd_rst_busy   => open,
      m_aclk        => aurora_user_clk_hub2,
      s_aclk        => clk320,
      s_aresetn     => Locked,  -- Global reset: This signal is active-Low.
      s_axis_tvalid => payload_valid_bus(0),
      s_axis_tready => open, -- tready_data_bus(0),
      s_axis_tdata  => payload_data_bus(0),
      s_axis_tkeep  => (others => '0'),
      s_axis_tlast  => payload_last_bus(0),
      s_axis_tuser  => (others => '0'),
      m_axis_tvalid => s_axi_tx_tvalid_hub2,
      m_axis_tready => s_axi_tx_tready_hub2,
      m_axis_tdata  => s_axi_tx_tdata_hub2,
      m_axis_tkeep  => s_axi_tx_tkeep_hub2,
      m_axis_tlast  => s_axi_tx_tlast_hub2,
      m_axis_tuser  => open
      );

--------------------------------------------------------------------------------------------------------------------------------------------
-- Aurora hub1 connections
--------------------------------------------------------------------------------------------------------------------------------------
  top_aurora_hub1 : entity infrastructure_lib.aurora_hub2

    port map (
      -- TX Stream Interface
      s_axi_tx_tdata   => s_axi_tx_tdata_hub1,
      s_axi_tx_tvalid  => s_axi_tx_tvalid_hub1,
      s_axi_tx_tready  => s_axi_tx_tready_hub1,
      s_axi_tx_tkeep   => s_axi_tx_tkeep_hub1,
      s_axi_tx_tlast   => s_axi_tx_tlast_hub1,
      -- User Flow Control TX Interface
      s_axi_ufc_tx_req => s_axi_ufc_tx_req_hub1,
      s_axi_ufc_tx_ms  => s_axi_ufc_tx_ms_hub1,
      s_axi_ufc_tx_ack => s_axi_ufc_tx_ack_hub1,
      -- V7 Serial I/O
      txp              => aurora_hub1_txp,
      txn              => aurora_hub1_txn,
      -- GT Reference Clock Interface

      gt_refclk1_p   => aurora_hub1_refclk1_p,  --aurora_refclk1_p,
      gt_refclk1_n   => aurora_hub1_refclk1_n,  --aurora_refclk1_n,
      -- Error Detection Interface
      tx_hard_err    => tx_hard_err_hub1,
      -- Status
      tx_channel_up  => tx_channel_up_hub1,
      tx_lane_up     => tx_lane_up_hub1,
      -- System Interface
      user_clk_out   => aurora_user_clk_hub1,
      sys_reset_out  => sys_reset_out_hub1,
      tx_lock        => tx_lock_hub1,
      init_clk       => mac_clk,
      init_clk_out   => init_clk_out_hub1,
      pll_not_locked => pll_not_locked_hub1,
      tx_resetdone   => aurora_tx_resetdone_hub1,
      link_reset     => link_reset_hub1
      );

  aurora_status_hub1 <= x"00000" & "000" & aurora_tx_resetdone_hub1 & tx_lock_hub1 & pll_not_locked_hub1 & tx_hard_err_hub1 & tx_channel_up_hub1 & tx_lane_up_hub1;


--------------------------------------------------------------------------------------------------------------------------------------------
-- Aurora hub2 connections
--------------------------------------------------------------------------------------------------------------------------------------
  top_aurora_hub2 : entity infrastructure_lib.aurora_hub2

    port map (
      -- TX Stream Interface
      s_axi_tx_tdata   => s_axi_tx_tdata_hub2,
      s_axi_tx_tvalid  => s_axi_tx_tvalid_hub2,
      s_axi_tx_tready  => s_axi_tx_tready_hub2,
      s_axi_tx_tkeep   => s_axi_tx_tkeep_hub2,
      s_axi_tx_tlast   => s_axi_tx_tlast_hub2,
      -- User Flow Control TX Interface
      s_axi_ufc_tx_req => s_axi_ufc_tx_req_hub2,
      s_axi_ufc_tx_ms  => s_axi_ufc_tx_ms_hub2,
      s_axi_ufc_tx_ack => s_axi_ufc_tx_ack_hub2,
      -- V7 Serial I/O
      txp              => aurora_hub2_txp,
      txn              => aurora_hub2_txn,
      -- GT Reference Clock Interface

      gt_refclk1_p   => aurora_hub2_refclk1_p,  --aurora_refclk1_p,
      gt_refclk1_n   => aurora_hub2_refclk1_n,  --aurora_refclk1_n,
      -- Error Detection Interface
      tx_hard_err    => tx_hard_err_hub2,
      -- Status
      tx_channel_up  => tx_channel_up_hub2,
      tx_lane_up     => tx_lane_up_hub2,
      -- System Interface
      user_clk_out   => aurora_user_clk_hub2,
      sys_reset_out  => sys_reset_out_hub2,
      tx_lock        => tx_lock_hub2,
      init_clk       => mac_clk,
      init_clk_out   => init_clk_out_hub2,
      pll_not_locked => pll_not_locked_hub2,
      tx_resetdone   => aurora_tx_resetdone_hub2,
      link_reset     => link_reset_hub2
      );

  aurora_status_hub2 <= x"00000" & "000" & aurora_tx_resetdone_hub2 & tx_lock_hub2 & pll_not_locked_hub2 & tx_hard_err_hub2 & tx_channel_up_hub2 & tx_lane_up_hub2;



-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -- ipbus slaves live in the entity below, and can expose top-level ports
-- The ipbus fabric is instantiated within.
--------------------------------------------------------------------------------------------------------

  infrastructure_control : entity infrastructure_lib.infrastructure_slaves_cntrl
    port map(
      ipb_clk      => ipb_clk,
      ipb_rst      => rst_ipb,
      ipb_in       => ipbw(N_SLV_CNTRL_INFRA),
      ipb_out      => ipbr(N_SLV_CNTRL_INFRA),
      status       => status,
      control_reg  => control_reg,
      reconfig_reg => reconfig_reg,

      ipbus_delay_in_ctrl    => ipbus_ctrl_delay_in,
      ipbus_delay_out_ctrl   => ipbus_ctrl_delay_out,
      ipbus_delay_in_status  => ipbus_status_delay_in,
      ipbus_delay_out_status => ipbus_status_delay_out,

      scl_i_0      => i2c_scl_0,
      scl_o_0      => scl_o_0,
      scl_enb_0    => scl_enb_0,
      sda_i_0      => i2c_sda_0,
      sda_o_0      => sda_o_0,
      sda_enb_0    => sda_enb_0,
      pll_clko     => pll_clko,
      pll_le       => pll_en,
      pll_miso     => pll_miso,
      pll_mosi     => pll_mosi,
      pll_select   => select_pll,
      flash_miso   => flash_miso,
      flash_le     => flash_csn,
      flash_clko   => flash_clk,
      flash_mosi   => flash_mosi

      );


----------------------------------------------------
--- pll select and flash
------------------------------------------------------

  pll_sel : entity infrastructure_lib.pll_selector
    port map
    (
      sel      => select_pll(1 downto 0),
      pll_en_1 => pll_le_1,
      pll_en_2 => pll_le_2,
      pll_en_3 => pll_le_3,
      pll_en   => pll_en

      );


  cclk_o : entity infrastructure_lib.startup
    port map(
      flash_cclk => flash_clk
      );

------------------------------------------------------------
---- generate config pulse
--------------------------------------------------------------
  configure : entity infrastructure_lib.self_configure
    generic map (WBSTAR => x"02000000")  -- Warm Boot Start Address
    port map(
      clk       => ipb_clk,              --icap_clk,
      led_clk   => onehz,
      reset     => rst_ipb,
      trigger   => trigger_reconfig,
      indicator => flash_led
      );
-------------------------------------------------------
--- clock generation of 40M,160M and 280M from the TTC
------------------------------------------------------
  ttc_clk : clk_ttc
    port map (
      -- Clock out ports
      clk40     => clk40,
      clk320    => clk320,
      clk160    => clk160,
      -- Status and control signals
      locked    => locked,
      -- Clock in ports
      clk_in1_p => clk_40_p,
      clk_in1_n => clk_40_n
      );
  reset <= not locked;

  reset_pll : entity infrastructure_lib.nreset_pll
    port map(
      clk40          => clk40,
      enable_pll_rst => enable_pll_rst,
      rst_ipb        => rst_ipb,
      SYNC_B_CDCE    => SYNC_B_CDCE

      );

-- Register IPBus interFPGA signals
	IPBusReg: process(mac_clk)
	Begin
		If rising_edge(mac_clk) then
			master_rx1_reg <= master_rx1_int;
			master_pause1_reg <= master_pause1_int;
			master_rx2_reg <= master_rx2_int;
			master_pause2_reg <= master_pause2_int;
			master_rx3_reg <= master_rx3_int;
			master_pause3_reg <= master_pause3_int;
			master_rx4_reg <= master_rx4_int;
			master_pause4_reg <= master_pause4_int;
		end if;
	End Process IPBusReg;

-- IPBUS TX BUS 10bit
--- 1 ---
ipbus_tx1 : io_delay_control_in
   port map
   (
   data_in_from_pins => master_tx_data1,
   data_in_to_device => master_tx1_int,
   delay_clk => mac_clk,
   in_delay_reset => ipbus_ctrl_delay_in(25), --28:25
   in_delay_data_ce => (others => '0'),
   in_delay_data_inc => (others => '0'),
   in_delay_tap_in => ipbus_ctrl_delay_in(4 downto 0)&ipbus_ctrl_delay_in(4 downto 0)&ipbus_ctrl_delay_in(4 downto 0)&
   ipbus_ctrl_delay_in(4 downto 0)&ipbus_ctrl_delay_in(4 downto 0)&ipbus_ctrl_delay_in(4 downto 0)&
   ipbus_ctrl_delay_in(4 downto 0)&ipbus_ctrl_delay_in(4 downto 0)&ipbus_ctrl_delay_in(4 downto 0)&
   ipbus_ctrl_delay_in(4 downto 0),
   in_delay_tap_out(4 downto 0) => ipbus_status_delay_in(4 downto 0),
   in_delay_tap_out(49 downto 5) => dummy_tx1,

   delay_locked => ipbus_status_delay_in(25), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_in(31) --common
);


--IPBUS RX BUS 11bit
ipbus_rx1 : io_delay_control_out
   port map
   (
   data_out_from_device(9 downto 0) => master_rx1_reg,
   data_out_from_device(10) => master_pause1_reg,
   data_out_to_pins(9 downto 0) => master_rx_data1,
   data_out_to_pins(10) => master_tx_pause1,
   delay_clk => mac_clk,
   out_delay_reset => ipbus_ctrl_delay_out(25), --28:25

   out_delay_data_ce => (others => '0'),
   out_delay_data_inc => (others => '0'),
   out_delay_tap_in => ipbus_ctrl_delay_out(4 downto 0)&ipbus_ctrl_delay_out( 4 downto 0)&ipbus_ctrl_delay_out(4 downto 0)&
   ipbus_ctrl_delay_out(4 downto 0)&ipbus_ctrl_delay_out(4 downto 0)&ipbus_ctrl_delay_out(4 downto 0)&
   ipbus_ctrl_delay_out(4 downto 0)&ipbus_ctrl_delay_out(4 downto 0)&ipbus_ctrl_delay_out(4 downto 0)&
   ipbus_ctrl_delay_out(4 downto 0)&ipbus_ctrl_delay_out(4 downto 0),

   out_delay_tap_out(4 downto 0) => ipbus_status_delay_out(4 downto 0),
   out_delay_tap_out(54 downto 5) => dummy_rx1,

   delay_locked => ipbus_status_delay_out(25), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_out(31) --common
);

--- 2 ---
ipbus_tx2 : io_delay_control_in
   port map
   (
   data_in_from_pins => master_tx_data2,
   data_in_to_device => master_tx2_int,
   delay_clk => mac_clk,
   in_delay_reset => ipbus_ctrl_delay_in(26), --28:25
   in_delay_data_ce => (others => '0'),
   in_delay_data_inc => (others => '0'),
   in_delay_tap_in => ipbus_ctrl_delay_in(9 downto 5)&ipbus_ctrl_delay_in(9 downto 5)&ipbus_ctrl_delay_in(9 downto 5)&
   ipbus_ctrl_delay_in(9 downto 5)&ipbus_ctrl_delay_in(9 downto 5)&ipbus_ctrl_delay_in(9 downto 5)&
   ipbus_ctrl_delay_in(9 downto 5)&ipbus_ctrl_delay_in(9 downto 5)&ipbus_ctrl_delay_in(9 downto 5)&
   ipbus_ctrl_delay_in(9 downto 5),
   in_delay_tap_out(4 downto 0) => ipbus_status_delay_in(9 downto 5),
   in_delay_tap_out(49 downto 5) => dummy_tx2,

   delay_locked => ipbus_status_delay_in(26), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_in(31) --common
);


--IPBUS RX BUS 11bit
ipbus_rx2 : io_delay_control_out
   port map
   (
   data_out_from_device(9 downto 0) => master_rx2_reg,
   data_out_from_device(10) => master_pause2_reg,
   data_out_to_pins(9 downto 0) => master_rx_data2,
   data_out_to_pins(10) => master_tx_pause2,
   delay_clk => mac_clk,
   out_delay_reset => ipbus_ctrl_delay_out(26), --28:25

   out_delay_data_ce => (others => '0'),
   out_delay_data_inc => (others => '0'),
   out_delay_tap_in => ipbus_ctrl_delay_out(9 downto 5)&ipbus_ctrl_delay_out( 9 downto 5)&ipbus_ctrl_delay_out(9 downto 5)&
   ipbus_ctrl_delay_out(9 downto 5)&ipbus_ctrl_delay_out(9 downto 5)&ipbus_ctrl_delay_out(9 downto 5)&
   ipbus_ctrl_delay_out(9 downto 5)&ipbus_ctrl_delay_out(9 downto 5)&ipbus_ctrl_delay_out(9 downto 5)&
   ipbus_ctrl_delay_out(9 downto 5)&ipbus_ctrl_delay_out(9 downto 5),

   out_delay_tap_out(4 downto 0) => ipbus_status_delay_out(9 downto 5),
   out_delay_tap_out(54 downto 5) => dummy_rx2,

   delay_locked => ipbus_status_delay_out(26), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_out(31) --common
);

--- 3 ---
ipbus_tx3 : io_delay_control_in
   port map
   (
   data_in_from_pins => master_tx_data3,
   data_in_to_device => master_tx3_int,
   delay_clk => mac_clk,
   in_delay_reset => ipbus_ctrl_delay_in(27), --28:25
   in_delay_data_ce => (others => '0'),
   in_delay_data_inc => (others => '0'),
   in_delay_tap_in => ipbus_ctrl_delay_in(14 downto 10)&ipbus_ctrl_delay_in(14 downto 10)&ipbus_ctrl_delay_in(14 downto 10)&
   ipbus_ctrl_delay_in(14 downto 10)&ipbus_ctrl_delay_in(14 downto 10)&ipbus_ctrl_delay_in(14 downto 10)&
   ipbus_ctrl_delay_in(14 downto 10)&ipbus_ctrl_delay_in(14 downto 10)&ipbus_ctrl_delay_in(14 downto 10)&
   ipbus_ctrl_delay_in(14 downto 10),
   in_delay_tap_out(4 downto 0) => ipbus_status_delay_in(14 downto 10),
   in_delay_tap_out(49 downto 5) => dummy_tx3,

   delay_locked => ipbus_status_delay_in(27), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_in(31) --common
);


--IPBUS RX BUS 11bit
ipbus_rx3 : io_delay_control_out
   port map
   (
   data_out_from_device(9 downto 0) => master_rx3_reg,
   data_out_from_device(10) => master_pause3_reg,
   data_out_to_pins(9 downto 0) => master_rx_data3,
   data_out_to_pins(10) => master_tx_pause3,
   delay_clk => mac_clk,
   out_delay_reset => ipbus_ctrl_delay_out(27), --28:25

   out_delay_data_ce => (others => '0'),
   out_delay_data_inc => (others => '0'),
   out_delay_tap_in => ipbus_ctrl_delay_out(14 downto 10)&ipbus_ctrl_delay_out( 14 downto 10)&ipbus_ctrl_delay_out(14 downto 10)&
   ipbus_ctrl_delay_out(14 downto 10)&ipbus_ctrl_delay_out(14 downto 10)&ipbus_ctrl_delay_out(14 downto 10)&
   ipbus_ctrl_delay_out(14 downto 10)&ipbus_ctrl_delay_out(14 downto 10)&ipbus_ctrl_delay_out(14 downto 10)&
   ipbus_ctrl_delay_out(14 downto 10)&ipbus_ctrl_delay_out(14 downto 10),

   out_delay_tap_out(4 downto 0) => ipbus_status_delay_out(14 downto 10),
   out_delay_tap_out(54 downto 5) => dummy_rx3,

   delay_locked => ipbus_status_delay_out(27), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_out(31) --common
);

--- 4 ---
ipbus_tx4 : io_delay_control_in
   port map
   (
   data_in_from_pins => master_tx_data4,
   data_in_to_device => master_tx4_int,
   delay_clk => mac_clk,
   in_delay_reset => ipbus_ctrl_delay_in(28), --28:25
   in_delay_data_ce => (others => '0'),
   in_delay_data_inc => (others => '0'),
   in_delay_tap_in => ipbus_ctrl_delay_in(19 downto 15)&ipbus_ctrl_delay_in(19 downto 15)&ipbus_ctrl_delay_in(19 downto 15)&
   ipbus_ctrl_delay_in(19 downto 15)&ipbus_ctrl_delay_in(19 downto 15)&ipbus_ctrl_delay_in(19 downto 15)&
   ipbus_ctrl_delay_in(19 downto 15)&ipbus_ctrl_delay_in(19 downto 15)&ipbus_ctrl_delay_in(19 downto 15)&
   ipbus_ctrl_delay_in(19 downto 15),
   in_delay_tap_out(4 downto 0) => ipbus_status_delay_in(19 downto 15),
   in_delay_tap_out(49 downto 5) => dummy_tx4,

   delay_locked => ipbus_status_delay_in(28), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_in(31) --common
);


--IPBUS RX BUS 11bit
ipbus_rx4 : io_delay_control_out
   port map
   (
   data_out_from_device(9 downto 0) => master_rx4_reg,
   data_out_from_device(10) => master_pause4_reg,
   data_out_to_pins(9 downto 0) => master_rx_data4,
   data_out_to_pins(10) => master_tx_pause4,
   delay_clk => mac_clk,
   out_delay_reset => ipbus_ctrl_delay_out(28), --28:25

   out_delay_data_ce => (others => '0'),
   out_delay_data_inc => (others => '0'),
   out_delay_tap_in => ipbus_ctrl_delay_out(19 downto 15)&ipbus_ctrl_delay_out( 19 downto 15)&ipbus_ctrl_delay_out(19 downto 15)&
   ipbus_ctrl_delay_out(19 downto 15)&ipbus_ctrl_delay_out(19 downto 15)&ipbus_ctrl_delay_out(19 downto 15)&
   ipbus_ctrl_delay_out(19 downto 15)&ipbus_ctrl_delay_out(19 downto 15)&ipbus_ctrl_delay_out(19 downto 15)&
   ipbus_ctrl_delay_out(19 downto 15)&ipbus_ctrl_delay_out(19 downto 15),

   out_delay_tap_out(4 downto 0) => ipbus_status_delay_out(19 downto 15),
   out_delay_tap_out(54 downto 5) => dummy_rx4,

   delay_locked => ipbus_status_delay_out(28), --28:25
   ref_clock => clk200,
   clk_in => mac_clk,
   io_reset => ipbus_ctrl_delay_out(31) --common
);



end Spec;
