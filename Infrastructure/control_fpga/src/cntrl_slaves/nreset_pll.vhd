----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/28/2018 11:55:56 AM
-- Design Name: 
-- Module Name: nreset_pll - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity nreset_pll is
 PORT( 
     clk40          : IN     std_logic;
     enable_pll_rst : IN     std_logic;
     rst_ipb        : IN     std_logic;
     SYNC_B_CDCE    : OUT    std_logic   
    
    );
    
  end nreset_pll;

architecture Behavioral of nreset_pll is
signal data_reg1,data_reg0,SYNC_B_CDCE_int : std_logic;

begin



reset_gen: entity work.nreset_gen
       port map (
        clk =>  clk40,
        reset=> rst_ipb,
        en_rst=>data_reg1 ,
        nreset => SYNC_B_CDCE_int
     
       );
       
CDC :process (clk40,rst_ipb)
        begin
           if rst_ipb ='1' then
              data_reg0 <= '0' ;
              data_reg1 <= '0';
            elsif clk40' event and clk40 ='1' then
            data_reg0 <= enable_pll_rst;
            data_reg1 <= data_reg0;
           end if; 
           
       end process;    
       
reg_pip:process (clk40)
            begin
                        
                if clk40' event and clk40 ='0' then
                SYNC_B_CDCE <= SYNC_B_CDCE_int ;
                
               end if; 
           
       end process;       
   
       
       
end Behavioral;
