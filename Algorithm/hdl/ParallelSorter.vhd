--! @file
--! @brief Parallel sorter module
--! @details 
--! Parallel sorter for TOBs
--! @author Francesco Gonnella

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.DataTypes.all;
use work.AlgoDataTypes.all;

-------------------------------------------------------------------------------

--! @copydoc ParallelSorter.vhd
entity ParallelSorter is
  generic (
    USE_EXTERNAL_WRITE : boolean := false;
    N_TOBS_IN          : integer := 5;
    N_TOBS_OUT         : integer := 10
    );

  port (
    clk      : in std_logic;
    IN_Start : in std_logic_vector(1 downto 0);
    IN_Write : in std_logic_vector(1 downto 0);
    IN_Data  : in AlgoTriggerObjects(1 downto 0);

    OUT_Start : out std_logic;
    OUT_Write : out std_logic;
    OUT_Data  : out AlgoTriggerObject
    );

end entity ParallelSorter;

-------------------------------------------------------------------------------

--! @copydoc ParallelSorter.vhd
architecture str of ParallelSorter is
  -----------------------------------------------------------------------------
  -- Internal signal declarations
  -----------------------------------------------------------------------------
  constant N_TOBS : integer := N_TOBS_OUT - N_TOBS_IN;

  signal FifoReset                : std_logic;
  signal OutWrite, OutStart       : std_logic;
  signal Done                     : std_logic;
  signal WriteInt, Write0, Write1 : std_logic;
  signal Ack                      : std_logic_vector(1 downto 0);
  signal Acknowledge              : std_logic                       := '0';
  signal Data                     : AlgoTriggerObjects(1 downto 0);
  signal OneOrTwo                 : integer range 0 to 1            := 0;
  signal TOB_Count                : integer range 0 to N_TOBS_OUT-1 := 0;
  signal Start                    : std_logic;

begin  -- architecture str

  -----------------------------------------------------------------------------
  -- Component instantiations
  -----------------------------------------------------------------------------
  Start  <= IN_Start(0) or IN_Start(1);
  Write0 <= IN_Write(0) when USE_EXTERNAL_WRITE else (WriteInt or Start);
  Write1 <= IN_Write(1) when USE_EXTERNAL_WRITE else (WriteInt or Start);

  FastFifo_1 : entity work.FastFifo
    generic map (
      FASTFIFO_DATA_DEPTH => N_TOBS_OUT)
    port map (
      clk       => clk,
      IN_Reset  => FifoReset,
      IN_Data   => IN_Data(0),
      IN_Write  => Write0,
      IN_Ack    => Ack(0) and (not Done),
      OUT_Empty => open,
      OUT_Data  => Data(0));

  FastFifo_2 : entity work.FastFifo
    generic map (
      FASTFIFO_DATA_DEPTH => N_TOBS_OUT)
    port map (
      clk       => clk,
      IN_Reset  => FifoReset,
      IN_Data   => IN_Data(1),
      IN_Write  => Write1,
      IN_Ack    => Ack(1) and (not Done),
      OUT_Empty => open,
      OUT_Data  => Data(1));

  OneOrTwo <= 0           when (TOBEnergy(Data(0)) > TOBEnergy(Data(1)))                               else 1;
  Done     <= '1'         when (unsigned(TOBEnergy(Data(0))) = 0 and unsigned(TOBEnergy(Data(1))) = 0) else '0';
  Ack(0)   <= Acknowledge when OneOrTwo = 0                                                            else '0';
  Ack(1)   <= Acknowledge when OneOrTwo = 1                                                            else '0';

  delayProc : process (clk) is
  begin
    if rising_edge(clk) then
      OUT_Data  <= Data(OneOrTwo);
      OUT_Start <= OutStart;
      OUT_Write <= (OutWrite or OutStart) and not Done;
    end if;
  end process;

  CounterProc : process (clk) is
  begin  -- process SortingProc
    if rising_edge(clk) then
      if Start = '1' then
        if TOB_Count > 0 then
          report "Early start!" severity error;
        end if;
        if IN_Start /= "11" then
          report "Start error: one of the 2 starts is 0!" severity error;
        end if;

        OutStart  <= '1';
        TOB_Count <= N_TOBS_OUT-1;  --because it actually starts one tick before
      else
        OutStart <= '0';
        if TOB_Count > 0 then
          TOB_Count <= TOB_Count - 1;
        else
          TOB_Count <= 0;
        end if;

      end if;
    end if;
  end process CounterProc;



  WritingProc : process(clk) is
  begin
    if rising_edge(clk) then
      if TOB_Count > N_TOBS+2 or Start = '1' then  --writing
        WriteInt <= '1';

      elsif TOB_Count < N_TOBS+2 then   --done
        WriteInt <= '0';

      else                              --last writing cycle
        WriteInt <= '1';
      end if;
    end if;
  end process WritingProc;

  ReadingProc : process (clk) is
  begin  -- process SortingProc
    if rising_edge(clk) then
      if TOB_Count > 1 or Start = '1' then  --reading
        FifoReset   <= '0';
        OutWrite    <= '1';             -- protection against both fifo empty
        Acknowledge <= '1';

      elsif TOB_Count > 0 then          --last reading cycle (Count=1)
        FifoReset   <= '1';
        OutWrite    <= '1';
        Acknowledge <= '0';

      else                              --done (Count=0)
        FifoReset   <= '0';
        OutWrite    <= '0';
        Acknowledge <= '0';
      end if;
    end if;
  end process ReadingProc;



end architecture str;

-------------------------------------------------------------------------------
