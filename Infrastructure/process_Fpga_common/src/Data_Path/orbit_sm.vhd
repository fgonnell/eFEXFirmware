
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

ENTITY orbit_sm IS
   PORT( 
      chan_orbit : IN     std_logic;
      clk        : IN     std_logic;
      ref_orbit  : IN     std_logic;
      reset      : IN     std_logic;
      start      : IN     std_logic;
      A_eq_B     : OUT    std_logic;
      cntr_value : OUT    std_logic_vector (4 DOWNTO 0)
   );


END ENTITY orbit_sm ;

ARCHITECTURE fsm OF orbit_sm IS
 -- Architecture Declarations
  signal cntr_0, cntr_1 :unsigned(4 downto 0);

  SUBTYPE STATE_TYPE IS 
     std_logic_vector(5 DOWNTO 0);

  -- Hard encoding
  CONSTANT idle : STATE_TYPE := "000001";
  CONSTANT cnt0_inc : STATE_TYPE := "000010";
  CONSTANT cnt_ref0 : STATE_TYPE := "000100";
  CONSTANT cnt1_inc : STATE_TYPE := "001000";
  CONSTANT cnt_ref1 : STATE_TYPE := "010000";
  CONSTANT s1 : STATE_TYPE := "100000";

  -- Declare current and next state signals
  SIGNAL current_state : STATE_TYPE;

BEGIN

  -----------------------------------------------------------------
  clocked_proc : PROCESS ( 
     clk
  )
  -----------------------------------------------------------------
  BEGIN
    IF (clk'EVENT AND clk = '1') THEN
     IF (reset = '1') THEN
        current_state <= idle;
        -- Default Reset Values
        A_eq_B <= '0';
        cntr_value <= (others => '0');
        cntr_0 <= (others => '0');
        cntr_1 <= (others => '0');
     ELSE 
  -- Default Assignment To Internals and Outputs
               cntr_0 <= (others => '0');
               cntr_1 <= (others => '0');
               A_eq_B <= '0';
               cntr_value <= (others => '0');
      
               -- Combined Actions
               CASE current_state IS
                  WHEN idle => 
                     cntr_0 <= (others => '0');
                     cntr_1 <= (others => '0');
                     IF (start ='1') THEN 
                        current_state <= cnt0_inc;
                     ELSE
                        current_state <= idle;
                     END IF;
                  WHEN cnt0_inc => 
                     IF (ref_orbit = '1') THEN 
                        cntr_0 <= cntr_0 +1;
                        current_state <= cnt_ref0;
                     ELSE
                        current_state <= cnt0_inc;
                     END IF;
                  WHEN cnt_ref0 => 
                     cntr_0 <= cntr_0 +1;
                     IF (chan_orbit ='1') THEN 
                        cntr_0 <= cntr_0;
                        current_state <= cnt1_inc;
                     ELSE
                        current_state <= cnt_ref0;
                     END IF;
                  WHEN cnt1_inc => 
                     cntr_0 <= cntr_0;
                     IF (ref_orbit = '1') THEN 
                        cntr_1 <= cntr_1 +1;
                        current_state <= cnt_ref1;
                     ELSE
                        current_state <= cnt1_inc;
                     END IF;
                  WHEN cnt_ref1 => 
                     cntr_1 <= cntr_1 +1;
                     cntr_0 <= cntr_0;
                     IF (chan_orbit ='1') THEN 
                        cntr_1 <= cntr_1;
                        current_state <= s1;
                     ELSE
                        current_state <= cnt_ref1;
                     END IF;
                  WHEN s1 => 
                     cntr_0 <= cntr_0;
                     cntr_1 <= cntr_1;
                     IF (cntr_0 = cntr_1) THEN 
                        A_eq_B <= '1';
                        cntr_value <= std_logic_vector(cntr_0);
                        current_state <= idle;
                     ELSE
                        cntr_0 <= (others => '0');
                        cntr_1 <= (others => '0');
                        current_state <= cnt0_inc;
                     END IF;
                  WHEN OTHERS =>
                     current_state <= idle;
               END CASE;
            END IF;
            END IF;
         END PROCESS clocked_proc;
   
END ARCHITECTURE fsm;