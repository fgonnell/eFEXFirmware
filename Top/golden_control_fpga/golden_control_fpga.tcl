#vivado
set BIN_FILE 1

## FPGA and Vivado strategies and flows
set FPGA xc7vx330tffg1157-2
set SYNTH_STRATEGY "Vivado Synthesis Defaults" 
set SYNTH_FLOW {Vivado Synthesis 2019}
set IMPL_STRATEGY "Vivado Implementation Defaults"
set IMPL_FLOW {Vivado Implementation 2019}
############################################################

set DESIGN    "[file rootname [file tail [info script]]]"

set PATH_REPO "[file normalize [file dirname [info script]]]/../../"
source $PATH_REPO/Hog/Tcl/create_project.tcl

