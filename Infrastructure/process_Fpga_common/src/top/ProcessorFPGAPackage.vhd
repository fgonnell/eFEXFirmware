library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library infrastructure_lib;
use infrastructure_lib.mgt_type.all;

package ProcessorFPGAPackage is

  function F_FPGA_NUMBER (flavour             : in integer) return integer;
  function F_MGT_QUAD_ENABLE (fpga_number     : in integer) return std_logic_vector;
  function F_MGT_USE_OTHER_CLK_N (fpga_number : in integer) return integer;
  function F_IPBUS_PORT_N (fpga_number        : in integer) return std_logic_vector;
  function F_MGT_USE_OTHER_CLK (fpga_number   : in integer) return std_logic_vector;
  function F_MGT_TX_POWER (fpga_number        : in integer) return std_logic_vector;
  function F_MGT_RX_POWER (fpga_number        : in integer) return std_logic_vector;
  function F_ENABLE_MERGING (fpga_number      : in integer) return std_logic;

  procedure MGT_SELECTOR_RX (
    signal fpga_number          : in  integer;
    signal in_BC_Reg_sel        : in  std_logic_vector(319 downto 0);
    signal in_mux_sel           : in  std_logic_vector(319 downto 0);
    signal in_delay_num         : out std_logic_vector(319 downto 0);
    signal in_bc_cntr_0         : out std_logic_vector(139 downto 0);
    signal in_bc_cntr_1         : out std_logic_vector(139 downto 0);
    signal in_bc_cntr_2         : out std_logic_vector(139 downto 0);
    signal in_bc_cntr_3         : out std_logic_vector(139 downto 0);
    signal in_bc_mux_cntr_0     : out std_logic_vector(139 downto 0);
    signal in_bc_mux_cntr_1     : out std_logic_vector(139 downto 0);
    signal in_bc_mux_cntr_2     : out std_logic_vector(139 downto 0);
    signal in_bc_mux_cntr_3     : out std_logic_vector(139 downto 0);
    signal in_mgt_RXUSRCLK_OUT  : in  std_logic_vector(79 downto 0);
    signal in_enable_mgt        : in  std_logic_vector(79 downto 0);
    signal in_bcn_synch         : out std_logic_vector(79 downto 0);
    signal in_crc_error_chan    : out std_logic_vector(79 downto 0);
    signal in_notable_error    : in std_logic_vector(79 downto 0);
    signal in_disperr_error    : in std_logic_vector(79 downto 0);
    signal in_rx_resetdone      : in  std_logic_vector(79 downto 0);
    signal out_BC_Reg_sel       : out std_logic_vector(255 downto 0);
    signal out_mux_sel          : out std_logic_vector(255 downto 0);
    signal out_delay_num        : in  std_logic_vector(255 downto 0);
    signal out_bc_cntr_0        : in  std_logic_vector(111 downto 0);
    signal out_bc_cntr_1        : in  std_logic_vector(111 downto 0);
    signal out_bc_cntr_2        : in  std_logic_vector(111 downto 0);
    signal out_bc_cntr_3        : in  std_logic_vector(111 downto 0);
    signal out_bc_mux_cntr_0    : in  std_logic_vector(111 downto 0);
    signal out_bc_mux_cntr_1    : in  std_logic_vector(111 downto 0);
    signal out_bc_mux_cntr_2    : in  std_logic_vector(111 downto 0);
    signal out_bc_mux_cntr_3    : in  std_logic_vector(111 downto 0);
    signal out_mgt_RXUSRCLK_OUT : out std_logic_vector(63 downto 0);
    signal out_enable_mgt       : out std_logic_vector(63 downto 0);
    signal out_bcn_synch        : in  std_logic_vector(63 downto 0);
    signal out_crc_error_chan   : in  std_logic_vector(63 downto 0);
    signal out_disperr_error   : out  std_logic_vector(63 downto 0);
    signal out_notable_error   : out  std_logic_vector(63 downto 0);
    
    signal out_rx_resetdone     : out std_logic_vector(63 downto 0);
    signal in_MGT_Commadet      : in  std_logic_vector(79 downto 0);
    signal in_MGT_Data          : in  mgt_rxdata_array (19 downto 0);
    signal out_MGT_Commadet     : out std_logic_vector(63 downto 0);
    signal out_MGT_Data         : out mgt_rxdata_array (15 downto 0);

    signal in_rxdata_mgt0 : out std_logic_vector(639 downto 0);
    signal in_rxdata_mgt1 : out std_logic_vector(639 downto 0);
    signal in_rxdata_mgt2 : out std_logic_vector(639 downto 0);
    signal in_rxdata_mgt3 : out std_logic_vector(639 downto 0);

    signal out_rxdata_mgt0 : in std_logic_vector(511 downto 0);
    signal out_rxdata_mgt1 : in std_logic_vector(511 downto 0);
    signal out_rxdata_mgt2 : in std_logic_vector(511 downto 0);
    signal out_rxdata_mgt3 : in std_logic_vector(511 downto 0);

    signal in_ram_data_mgt0 : in std_logic_vector(639 downto 0);
    signal in_ram_data_mgt1 : in std_logic_vector(639 downto 0);
    signal in_ram_data_mgt2 : in std_logic_vector(639 downto 0);
    signal in_ram_data_mgt3 : in std_logic_vector(639 downto 0);

    signal out_ram_data_mgt0 : out std_logic_vector(511 downto 0);
    signal out_ram_data_mgt1 : out std_logic_vector(511 downto 0);
    signal out_ram_data_mgt2 : out std_logic_vector(511 downto 0);
    signal out_ram_data_mgt3 : out std_logic_vector(511 downto 0);

    signal in_kchar  : in  std_logic_vector(79 downto 0);
    signal out_kchar : out std_logic_vector(63 downto 0)



    );

  procedure MGT_SELECTOR_TX (
    signal fpga_number : in integer;

    signal in_mgt_usr_clk : in std_logic_vector(79 downto 0);
    signal in_txdata_0    : in std_logic_vector(33 downto 0);
    signal in_txdata_1    : in std_logic_vector(33 downto 0);
    signal in_txdata_2    : in std_logic_vector(33 downto 0);
    signal in_txdata_3    : in std_logic_vector(33 downto 0);
    signal in_txdata_4    : in std_logic_vector(33 downto 0);
    signal in_txdata_5    : in std_logic_vector(33 downto 0);
    signal in_txdata_6    : in std_logic_vector(33 downto 0);
    signal in_txdata_7    : in std_logic_vector(33 downto 0);
    signal in_txdata_8    : in std_logic_vector(33 downto 0);
    signal in_txdata_9    : in std_logic_vector(33 downto 0);
    signal in_txdata_10   : in std_logic_vector(33 downto 0);
    signal in_txdata_11   : in std_logic_vector(33 downto 0);

    --signal in_mgt_TXUSRCLK_OUT : in std_logic_vector(11 downto 0);

    signal in_topo_k                : in  std_logic;
    signal in_raw_k                 : in  std_logic;
    signal in_topo_data             : in  std_logic_vector(31 downto 0);
    signal in_raw_data              : in  std_logic_vector(31 downto 0);
    signal out_topo_tob_clk         : out std_logic;
    signal out_topo_raw_clk         : out std_logic;
    signal out_mgt_usr_clk          : out std_logic_vector(11 downto 0);
    signal out_txcharisk_quad_array : out mgt_txcharisk_array(19 downto 0);

    signal out_txdata_quad_array : out mgt_txdata_array(19 downto 0)
    );

end package ProcessorFPGAPackage;

package body ProcessorFPGAPackage is

  function F_FPGA_NUMBER (flavour : in integer) return integer is
    variable num : integer;
  begin
    if FLAVOUR < 10 then
      num := FLAVOUR;
    elsif FLAVOUR < 100 then
      num := FLAVOUR - 10;
    elsif FLAVOUR < 1000 then
      num := FLAVOUR - 100;
    else
      num := FLAVOUR - 1000;
    end if;

    return num;
  end function;

--- functions to configure MGTs for FPGA 1, 2, 3, and 4
  function F_MGT_QUAD_ENABLE (fpga_number : in integer) return std_logic_vector is
    variable V_MGT_QUAD_ENABLE : std_logic_vector(19 downto 0);
  begin
    if fpga_number = 1 then
      V_MGT_QUAD_ENABLE := "0000"&"1111"&"1111"&"1111"&"1111";  -- there must be 4 zeroes
    elsif fpga_number = 2 then
      V_MGT_QUAD_ENABLE := "1110"&"1100"&"0111"&"1111"&"1111";  -- there must be 4 zeroes
    elsif fpga_number = 3 then
      V_MGT_QUAD_ENABLE := "1111"&"1011"&"1111"&"1110"&"0011";  -- there must be 4 zeroes
    elsif fpga_number = 4 then
      V_MGT_QUAD_ENABLE := "1111"&"0111"&"1111"&"1110"&"0011";  -- there must be 4 zeroes
    else
      V_MGT_QUAD_ENABLE := x"00000";
    end if;
    return V_MGT_QUAD_ENABLE;
  end function F_MGT_QUAD_ENABLE;

  function F_MGT_USE_OTHER_CLK (fpga_number : in integer) return std_logic_vector is
    variable V_MGT_USE_OTHER_CLK : std_logic_vector(19 downto 0);
  begin
    if fpga_number = 1 then
      V_MGT_USE_OTHER_CLK := x"00400";
    elsif fpga_number = 2 then
      V_MGT_USE_OTHER_CLK := x"00400";
    elsif fpga_number = 3 then
      V_MGT_USE_OTHER_CLK := x"02000";
    elsif fpga_number = 4 then
      V_MGT_USE_OTHER_CLK := x"00100";
    else
      V_MGT_USE_OTHER_CLK := x"00000";
    end if;
    return V_MGT_USE_OTHER_CLK;
  end function F_MGT_USE_OTHER_CLK;

  function F_MGT_USE_OTHER_CLK_N (fpga_number : in integer) return integer is
    variable V_MGT_USE_OTHER_CLK_N : integer;
  begin
    if fpga_number = 1 then
      V_MGT_USE_OTHER_CLK_N := 10;
    elsif fpga_number = 2 then
      V_MGT_USE_OTHER_CLK_N := 10;
    elsif fpga_number = 3 then
      V_MGT_USE_OTHER_CLK_N := 13;
    elsif fpga_number = 4 then
      V_MGT_USE_OTHER_CLK_N := 8;
    else
      V_MGT_USE_OTHER_CLK_N := 0;
    end if;
    return V_MGT_USE_OTHER_CLK_N;
  end function F_MGT_USE_OTHER_CLK_N;

  function F_IPBUS_PORT_N (fpga_number : in integer) return std_logic_vector is
    variable V_IPBUS_PORT_N : std_logic_vector(15 downto 0);
  begin
    if fpga_number = 1 then
      V_IPBUS_PORT_N := x"C352";
    elsif fpga_number = 2 then
      V_IPBUS_PORT_N := x"C353";
    elsif fpga_number = 3 then
      V_IPBUS_PORT_N := x"C354";
    elsif fpga_number = 4 then
      V_IPBUS_PORT_N := x"C355";
    else
      V_IPBUS_PORT_N := x"0000";
    end if;
    return V_IPBUS_PORT_N;
  end function F_IPBUS_PORT_N;

  function F_MGT_TX_POWER (fpga_number : in integer) return std_logic_vector is
    variable V_MGT_TX_POWER : std_logic_vector(79 downto 0);
  begin
    if fpga_number = 1 then
      V_MGT_TX_POWER := x"0000_00ff_f300_0000_0000";
    elsif fpga_number = 2 then
      V_MGT_TX_POWER := x"fff0_0000_0300_0000_0000";
    elsif fpga_number = 3 then
      V_MGT_TX_POWER := x"0000_0030_0000_0000_0000";
    elsif fpga_number = 4 then
      V_MGT_TX_POWER := x"0000_0000_0003_0000_0000";
    else
      V_MGT_TX_POWER := x"0000_0000_0000_0000_0000";
    end if;
    return V_MGT_TX_POWER;
  end function F_MGT_TX_POWER;

  function F_MGT_RX_POWER (fpga_number : in integer) return std_logic_vector is
    variable V_MGT_RX_POWER : std_logic_vector(79 downto 0);
  begin
    if fpga_number = 1 then
      --V_MGT_RX_POWER:=x"0000_e0f8_20ff_ffff_ffff";
      V_MGT_RX_POWER := x"0000_f0ff_f0ff_ffff_ffff";
    elsif fpga_number = 2 then
      --V_MGT_RX_POWER:=x"1ab0_0700_00ff_ffff_ffff";
      V_MGT_RX_POWER := x"fff0_0f00_00ff_ffff_ffff";
    elsif fpga_number = 3 then
      --V_MGT_RX_POWER:=x"ffff_f003_39ff_fff0_0091";
      V_MGT_RX_POWER := x"ffff_f00f_ffff_fff0_00ff";
    elsif fpga_number = 4 then
      --V_MGT_RX_POWER:=x"03ac_0fff_ff70_fff0_00ff";
      V_MGT_RX_POWER := x"0fff_0fff_fff0_fff0_00ff";
    else
      V_MGT_RX_POWER := x"0000_0000_0000_0000_0000";
    end if;
    return V_MGT_RX_POWER;
  end function F_MGT_RX_POWER;

  function F_ENABLE_MERGING (fpga_number : in integer) return std_logic is
    variable V_ENABLE_MERGING : std_logic;
  begin
    if fpga_number = 1 then
      V_ENABLE_MERGING := '1';
    elsif fpga_number = 2 then
      V_ENABLE_MERGING := '1';
    elsif fpga_number = 3 then
      V_ENABLE_MERGING := '0';
    elsif fpga_number = 4 then
      V_ENABLE_MERGING := '0';
    else
      V_ENABLE_MERGING := '0';
    end if;
    return V_ENABLE_MERGING;
  end function F_ENABLE_MERGING;

  procedure MGT_SELECTOR_RX
    (
     signal fpga_number          : in  integer;
    signal in_BC_Reg_sel        : in  std_logic_vector(319 downto 0);
    signal in_mux_sel           : in  std_logic_vector(319 downto 0);
    signal in_delay_num         : out std_logic_vector(319 downto 0);
    signal in_bc_cntr_0         : out std_logic_vector(139 downto 0);
    signal in_bc_cntr_1         : out std_logic_vector(139 downto 0);
    signal in_bc_cntr_2         : out std_logic_vector(139 downto 0);
    signal in_bc_cntr_3         : out std_logic_vector(139 downto 0);
    signal in_bc_mux_cntr_0     : out std_logic_vector(139 downto 0);
    signal in_bc_mux_cntr_1     : out std_logic_vector(139 downto 0);
    signal in_bc_mux_cntr_2     : out std_logic_vector(139 downto 0);
    signal in_bc_mux_cntr_3     : out std_logic_vector(139 downto 0);
    signal in_mgt_RXUSRCLK_OUT  : in  std_logic_vector(79 downto 0);
    signal in_enable_mgt        : in  std_logic_vector(79 downto 0);
    signal in_bcn_synch         : out std_logic_vector(79 downto 0);
    signal in_crc_error_chan    : out std_logic_vector(79 downto 0);
    signal in_notable_error    : in std_logic_vector(79 downto 0);
    signal in_disperr_error    : in std_logic_vector(79 downto 0);
    signal in_rx_resetdone      : in  std_logic_vector(79 downto 0);
    signal out_BC_Reg_sel       : out std_logic_vector(255 downto 0);
    signal out_mux_sel          : out std_logic_vector(255 downto 0);
    signal out_delay_num        : in  std_logic_vector(255 downto 0);
    signal out_bc_cntr_0        : in  std_logic_vector(111 downto 0);
    signal out_bc_cntr_1        : in  std_logic_vector(111 downto 0);
    signal out_bc_cntr_2        : in  std_logic_vector(111 downto 0);
    signal out_bc_cntr_3        : in  std_logic_vector(111 downto 0);
    signal out_bc_mux_cntr_0    : in  std_logic_vector(111 downto 0);
    signal out_bc_mux_cntr_1    : in  std_logic_vector(111 downto 0);
    signal out_bc_mux_cntr_2    : in  std_logic_vector(111 downto 0);
    signal out_bc_mux_cntr_3    : in  std_logic_vector(111 downto 0);
    signal out_mgt_RXUSRCLK_OUT : out std_logic_vector(63 downto 0);
    signal out_enable_mgt       : out std_logic_vector(63 downto 0);
    signal out_bcn_synch        : in  std_logic_vector(63 downto 0);
    signal out_crc_error_chan   : in  std_logic_vector(63 downto 0);
    signal out_disperr_error   : out  std_logic_vector(63 downto 0);
    signal out_notable_error   : out  std_logic_vector(63 downto 0);
    
    signal out_rx_resetdone     : out std_logic_vector(63 downto 0);
    signal in_MGT_Commadet      : in  std_logic_vector(79 downto 0);
    signal in_MGT_Data          : in  mgt_rxdata_array (19 downto 0);
    signal out_MGT_Commadet     : out std_logic_vector(63 downto 0);
    signal out_MGT_Data         : out mgt_rxdata_array (15 downto 0);

    signal in_rxdata_mgt0 : out std_logic_vector(639 downto 0);
    signal in_rxdata_mgt1 : out std_logic_vector(639 downto 0);
    signal in_rxdata_mgt2 : out std_logic_vector(639 downto 0);
    signal in_rxdata_mgt3 : out std_logic_vector(639 downto 0);

    signal out_rxdata_mgt0 : in std_logic_vector(511 downto 0);
    signal out_rxdata_mgt1 : in std_logic_vector(511 downto 0);
    signal out_rxdata_mgt2 : in std_logic_vector(511 downto 0);
    signal out_rxdata_mgt3 : in std_logic_vector(511 downto 0);

    signal in_ram_data_mgt0 : in std_logic_vector(639 downto 0);
    signal in_ram_data_mgt1 : in std_logic_vector(639 downto 0);
    signal in_ram_data_mgt2 : in std_logic_vector(639 downto 0);
    signal in_ram_data_mgt3 : in std_logic_vector(639 downto 0);

    signal out_ram_data_mgt0 : out std_logic_vector(511 downto 0);
    signal out_ram_data_mgt1 : out std_logic_vector(511 downto 0);
    signal out_ram_data_mgt2 : out std_logic_vector(511 downto 0);
    signal out_ram_data_mgt3 : out std_logic_vector(511 downto 0);

    signal in_kchar  : in  std_logic_vector(79 downto 0);
    signal out_kchar : out std_logic_vector(63 downto 0)
      ) is
    ----------
--variables here
    variable v_BC_Reg_sel       : std_logic_vector(255 downto 0);
    variable v_mux_sel          : std_logic_vector(255 downto 0);
    variable v_delay_num        : std_logic_vector(319 downto 0);
    variable v_bc_cntr_0        : std_logic_vector(139 downto 0);
    variable v_bc_cntr_1        : std_logic_vector(139 downto 0);
    variable v_bc_cntr_2        : std_logic_vector(139 downto 0);
    variable v_bc_cntr_3        : std_logic_vector(139 downto 0);
    variable v_bc_mux_cntr_0    : std_logic_vector(139 downto 0);
    variable v_bc_mux_cntr_1    : std_logic_vector(139 downto 0);
    variable v_bc_mux_cntr_2    : std_logic_vector(139 downto 0);
    variable v_bc_mux_cntr_3    : std_logic_vector(139 downto 0);
    variable v_mgt_RXUSRCLK_OUT : std_logic_vector(63 downto 0);
    variable v_enable_mgt       : std_logic_vector(63 downto 0);
    variable v_bcn_synch        : std_logic_vector(79 downto 0);
    variable v_crc_error_chan   : std_logic_vector(79 downto 0);
    variable v_disperr_error   : std_logic_vector(63 downto 0);
    variable v_notable_error   : std_logic_vector(63 downto 0);
    
    variable v_rx_resetdone     : std_logic_vector(63 downto 0);

    variable v_rxdata_mgt0 : std_logic_vector(639 downto 0);
    variable v_rxdata_mgt1 : std_logic_vector(639 downto 0);
    variable v_rxdata_mgt2 : std_logic_vector(639 downto 0);
    variable v_rxdata_mgt3 : std_logic_vector(639 downto 0);

    variable v_ram_data_mgt0 : std_logic_vector(511 downto 0);
    variable v_ram_data_mgt1 : std_logic_vector(511 downto 0);
    variable v_ram_data_mgt2 : std_logic_vector(511 downto 0);
    variable v_ram_data_mgt3 : std_logic_vector(511 downto 0);
    variable v_kchar         : std_logic_vector(63 downto 0);

    variable v_MGT_Commadet : std_logic_vector(63 downto 0);
    variable v_MGT_Data     : mgt_rxdata_array (15 downto 0);
    variable m              : integer := 1;
  begin
    --     in: 16-11 & 9-0    out:  XXX 15-10 X 9-0      
    if fpga_number = 1 then
      m            := 1;
      v_MGT_Data   := in_MGT_Data(17*m - 1 downto 11*m) & in_MGT_Data(10*m - 1 downto 0);
      m            := 16;
      v_BC_Reg_sel := in_BC_Reg_sel(17*m - 1 downto 11*m) & in_BC_Reg_sel(10*m - 1 downto 0);
      v_mux_sel    := in_mux_sel(17*m - 1 downto 11*m) & in_mux_sel(10*m - 1 downto 0);

      v_delay_num     := x"0000_0000_0000" & out_delay_num(16*m - 1 downto 10*m) & x"0000" & out_delay_num(10*m - 1 downto 0);
      m               := 7;
      v_bc_cntr_0     := "0000000"&"0000000"&"0000000" & out_bc_cntr_0(16*m - 1 downto 10*m) & "0000000" & out_bc_cntr_0(10*m - 1 downto 0);
      v_bc_cntr_1     := "0000000"&"0000000"&"0000000" & out_bc_cntr_1(16*m - 1 downto 10*m) & "0000000" & out_bc_cntr_1(10*m - 1 downto 0);
      v_bc_cntr_2     := "0000000"&"0000000"&"0000000" & out_bc_cntr_2(16*m - 1 downto 10*m) & "0000000" & out_bc_cntr_2(10*m - 1 downto 0);
      v_bc_cntr_3     := "0000000"&"0000000"&"0000000" & out_bc_cntr_3(16*m - 1 downto 10*m) & "0000000" & out_bc_cntr_3(10*m - 1 downto 0);
      v_bc_mux_cntr_0 := "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_0(16*m - 1 downto 10*m) & "0000000" & out_bc_mux_cntr_0(10*m - 1 downto 0);
      v_bc_mux_cntr_1 := "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_1(16*m - 1 downto 10*m) & "0000000" & out_bc_mux_cntr_1(10*m - 1 downto 0);
      v_bc_mux_cntr_2 := "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_2(16*m - 1 downto 10*m) & "0000000" & out_bc_mux_cntr_2(10*m - 1 downto 0);
      v_bc_mux_cntr_3 := "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_3(16*m - 1 downto 10*m) & "0000000" & out_bc_mux_cntr_3(10*m - 1 downto 0);

      m             := 32;
      v_rxdata_mgt0 := x"0000000_00000000_000000000" & out_rxdata_mgt0(16*m - 1 downto 10*m) & x"00000000" & out_rxdata_mgt0(10*m - 1 downto 0);
      v_rxdata_mgt1 := x"0000000_00000000_000000000" & out_rxdata_mgt1(16*m - 1 downto 10*m) & x"00000000" & out_rxdata_mgt1(10*m - 1 downto 0);
      v_rxdata_mgt2 := x"0000000_00000000_000000000" & out_rxdata_mgt2(16*m - 1 downto 10*m) & x"00000000" & out_rxdata_mgt2(10*m - 1 downto 0);
      v_rxdata_mgt3 := x"0000000_00000000_000000000" & out_rxdata_mgt3(16*m - 1 downto 10*m) & x"00000000" & out_rxdata_mgt3(10*m - 1 downto 0);

      v_ram_data_mgt0 := in_ram_data_mgt0(17*m - 1 downto 11*m) & in_ram_data_mgt0(10*m - 1 downto 0);
      v_ram_data_mgt1 := in_ram_data_mgt1(17*m - 1 downto 11*m) & in_ram_data_mgt1(10*m - 1 downto 0);
      v_ram_data_mgt2 := in_ram_data_mgt2(17*m - 1 downto 11*m) & in_ram_data_mgt2(10*m - 1 downto 0);
      v_ram_data_mgt3 := in_ram_data_mgt3(17*m - 1 downto 11*m) & in_ram_data_mgt3(10*m - 1 downto 0);

      m                  := 4;
      v_mgt_RXUSRCLK_OUT := in_mgt_RXUSRCLK_OUT(17*m - 1 downto 11*m) & in_mgt_RXUSRCLK_OUT(10*m - 1 downto 0);
      v_disperr_error           := in_disperr_error(17*m - 1 downto 11*m) & in_disperr_error(10*m - 1 downto 0);
      v_notable_error          := in_notable_error(17*m - 1 downto 11*m) & in_notable_error(10*m - 1 downto 0);
      
      v_bcn_synch        := x"0_0_0" & out_bcn_synch(16*m - 1 downto 10*m) & x"0" & out_bcn_synch(10*m - 1 downto 0);
      v_crc_error_chan   := x"0_0_0" & out_crc_error_chan(16*m - 1 downto 10*m) & x"0" & out_crc_error_chan(10*m - 1 downto 0);   
      v_rx_resetdone := in_rx_resetdone(17*m - 1 downto 11*m) & in_rx_resetdone(10*m - 1 downto 0);
      v_enable_mgt   := in_enable_mgt(17*m - 1 downto 11*m) & in_enable_mgt(10*m - 1 downto 0);
      v_MGT_Commadet := in_MGT_Commadet(17*m - 1 downto 11*m) & in_MGT_Commadet(10*m - 1 downto 0);
      v_kchar := in_kchar (17*m - 1 downto 11*m) & in_kchar (10*m - 1 downto 0);



    -- in: 19-17 & 15-13 & 9-0     out: 15-13 X 12-10 XXX 9-0
    elsif fpga_number = 2 then
      m            := 1;
      v_MGT_Data   := in_MGT_Data (20*m - 1 downto 17*m) & in_MGT_Data(16*m - 1 downto 13*m) & in_MGT_Data(10*m - 1 downto 0);
      m            := 16;
      v_BC_Reg_sel := in_BC_Reg_sel (20*m - 1 downto 17*m) & in_BC_Reg_sel (16*m - 1 downto 13*m) & in_BC_Reg_sel (10*m - 1 downto 0);
      v_mux_sel    := in_mux_sel (20*m - 1 downto 17*m) & in_mux_sel (16*m - 1 downto 13*m) & in_mux_sel (10*m - 1 downto 0);

      v_delay_num     := out_delay_num(16*m - 1 downto 13*m) & x"0000" & out_delay_num(13*m - 1 downto 10*m) & x"0000_0000_0000" & out_delay_num(10*m - 1 downto 0);
      m               := 7;
      v_bc_cntr_0     := out_bc_cntr_0(16*m - 1 downto 13*m) & "0000000" & out_bc_cntr_0(13*m - 1 downto 10*m) & "0000000"&"0000000"&"0000000" & out_bc_cntr_0(10*m - 1 downto 0);
      v_bc_cntr_1     := out_bc_cntr_1(16*m - 1 downto 13*m) & "0000000" & out_bc_cntr_1(13*m - 1 downto 10*m) & "0000000"&"0000000"&"0000000" & out_bc_cntr_1(10*m - 1 downto 0);
      v_bc_cntr_2     := out_bc_cntr_2(16*m - 1 downto 13*m) & "0000000" & out_bc_cntr_2(13*m - 1 downto 10*m) & "0000000"&"0000000"&"0000000" & out_bc_cntr_2(10*m - 1 downto 0);
      v_bc_cntr_3     := out_bc_cntr_3(16*m - 1 downto 13*m) & "0000000" & out_bc_cntr_3(13*m - 1 downto 10*m) & "0000000"&"0000000"&"0000000" & out_bc_cntr_3(10*m - 1 downto 0);
      v_bc_mux_cntr_0 := out_bc_mux_cntr_0(16*m - 1 downto 13*m) & "0000000" & out_bc_mux_cntr_0(13*m - 1 downto 10*m) & "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_0(10*m - 1 downto 0);
      v_bc_mux_cntr_1 := out_bc_mux_cntr_1(16*m - 1 downto 13*m) & "0000000" & out_bc_mux_cntr_1(13*m - 1 downto 10*m) & "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_1(10*m - 1 downto 0);
      v_bc_mux_cntr_2 := out_bc_mux_cntr_2(16*m - 1 downto 13*m) & "0000000" & out_bc_mux_cntr_2(13*m - 1 downto 10*m) & "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_2(10*m - 1 downto 0);
      v_bc_mux_cntr_3 := out_bc_mux_cntr_3(16*m - 1 downto 13*m) & "0000000" & out_bc_mux_cntr_3(13*m - 1 downto 10*m) & "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_3(10*m - 1 downto 0);

      m             := 32;
      v_rxdata_mgt0 := out_rxdata_mgt0(16*m - 1 downto 13*m) & x"00000000" & out_rxdata_mgt0(13*m - 1 downto 10*m) & x"00000000_00000000_00000000" & out_rxdata_mgt0(10*m - 1 downto 0);
      v_rxdata_mgt1 := out_rxdata_mgt1(16*m - 1 downto 13*m) & x"00000000" & out_rxdata_mgt1(13*m - 1 downto 10*m) & x"00000000_00000000_00000000" & out_rxdata_mgt1(10*m - 1 downto 0);
      v_rxdata_mgt2 := out_rxdata_mgt2(16*m - 1 downto 13*m) & x"00000000" & out_rxdata_mgt2(13*m - 1 downto 10*m) & x"00000000_00000000_00000000" & out_rxdata_mgt2(10*m - 1 downto 0);
      v_rxdata_mgt3 := out_rxdata_mgt3(16*m - 1 downto 13*m) & x"00000000" & out_rxdata_mgt3(13*m - 1 downto 10*m) & x"00000000_00000000_00000000" & out_rxdata_mgt3(10*m - 1 downto 0);


      v_ram_data_mgt0 := in_ram_data_mgt0(20*m - 1 downto 17*m) & in_ram_data_mgt0(16*m - 1 downto 13*m) & in_ram_data_mgt0(10*m - 1 downto 0);
      v_ram_data_mgt1 := in_ram_data_mgt1(20*m - 1 downto 17*m) & in_ram_data_mgt1(16*m - 1 downto 13*m) & in_ram_data_mgt1(10*m - 1 downto 0);
      v_ram_data_mgt2 := in_ram_data_mgt2(20*m - 1 downto 17*m) & in_ram_data_mgt2(16*m - 1 downto 13*m) & in_ram_data_mgt2(10*m - 1 downto 0);
      v_ram_data_mgt3 := in_ram_data_mgt3(20*m - 1 downto 17*m) & in_ram_data_mgt3(16*m - 1 downto 13*m) & in_ram_data_mgt3(10*m - 1 downto 0);


      m                  := 4;
      v_mgt_RXUSRCLK_OUT := in_mgt_RXUSRCLK_OUT (20*m - 1 downto 17*m) & in_mgt_RXUSRCLK_OUT (16*m - 1 downto 13*m) & in_mgt_RXUSRCLK_OUT (10*m - 1 downto 0);
      v_disperr_error  := in_disperr_error (20*m - 1 downto 17*m) & in_disperr_error (16*m - 1 downto 13*m) & in_disperr_error (10*m - 1 downto 0);
      v_notable_error  := in_notable_error (20*m - 1 downto 17*m) & in_notable_error (16*m - 1 downto 13*m) & in_notable_error(10*m - 1 downto 0);
            
      v_bcn_synch        := out_bcn_synch(16*m - 1 downto 13*m) & x"0" & out_bcn_synch (13*m - 1 downto 10*m) & x"0_0_0" & out_bcn_synch(10*m - 1 downto 0);
      v_crc_error_chan   := out_crc_error_chan(16*m - 1 downto 13*m) & x"0" & out_crc_error_chan (13*m - 1 downto 10*m) & x"0_0_0" & out_crc_error_chan(10*m - 1 downto 0);
      v_rx_resetdone := in_rx_resetdone (20*m - 1 downto 17*m) & in_rx_resetdone (16*m - 1 downto 13*m) & in_rx_resetdone (10*m - 1 downto 0);
      v_enable_mgt   := in_enable_mgt (20*m - 1 downto 17*m) & in_enable_mgt (16*m - 1 downto 13*m) & in_enable_mgt (10*m - 1 downto 0);
      v_MGT_Commadet := in_MGT_Commadet (20*m - 1 downto 17*m) & in_MGT_Commadet (16*m - 1 downto 13*m) & in_MGT_Commadet (10*m - 1 downto 0);
      
      v_kchar := in_kchar (20*m - 1 downto 17*m) & in_kchar (16*m - 1 downto 13*m) & in_kchar (10*m - 1 downto 0);

    -- in: 20-15 & 12-5 & 1-0  out: 15-10 XX 9-2 XXX 1-0
    elsif fpga_number = 3 then

      m            := 1;
      v_MGT_Data   := in_MGT_Data(20*m - 1 downto 14*m) & in_MGT_Data(13*m - 1 downto 5*m) & in_MGT_Data(2*m - 1 downto 0);
      m            := 16;
      v_BC_Reg_sel := in_BC_Reg_sel (20*m - 1 downto 14*m) & in_BC_Reg_sel (13*m - 1 downto 5*m)& in_BC_Reg_sel (2*m - 1 downto 0);
      v_mux_sel    := in_mux_sel (20*m - 1 downto 14*m) & in_mux_sel (13*m - 1 downto 5*m)& in_mux_sel (2*m - 1 downto 0);

      v_delay_num     := out_delay_num(16*m - 1 downto 10*m) & x"0000" & out_delay_num(10*m - 1 downto 2*m) & x"0000_0000_0000" & out_delay_num(2*m - 1 downto 0);
      m               := 7;
      v_bc_cntr_0     := out_bc_cntr_0(16*m - 1 downto 10*m) & "0000000" & out_bc_cntr_0(10*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_cntr_0(2*m - 1 downto 0);
      v_bc_cntr_1     := out_bc_cntr_1(16*m - 1 downto 10*m) & "0000000" & out_bc_cntr_1(10*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_cntr_1(2*m - 1 downto 0);
      v_bc_cntr_2     := out_bc_cntr_2(16*m - 1 downto 10*m) & "0000000" & out_bc_cntr_2(10*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_cntr_2(2*m - 1 downto 0);
      v_bc_cntr_3     := out_bc_cntr_3(16*m - 1 downto 10*m) & "0000000" & out_bc_cntr_3(10*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_cntr_3(2*m - 1 downto 0);
      v_bc_mux_cntr_0 := out_bc_mux_cntr_0(16*m - 1 downto 10*m) & "0000000" & out_bc_mux_cntr_0(10*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_0(2*m - 1 downto 0);
      v_bc_mux_cntr_1 := out_bc_mux_cntr_1(16*m - 1 downto 10*m) & "0000000" & out_bc_mux_cntr_1(10*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_1(2*m - 1 downto 0);
      v_bc_mux_cntr_2 := out_bc_mux_cntr_2(16*m - 1 downto 10*m) & "0000000" & out_bc_mux_cntr_2(10*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_2(2*m - 1 downto 0);
      v_bc_mux_cntr_3 := out_bc_mux_cntr_3(16*m - 1 downto 10*m) & "0000000" & out_bc_mux_cntr_3(10*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_3(2*m - 1 downto 0);

      m             := 32;
      v_rxdata_mgt0 := out_rxdata_mgt0(16*m - 1 downto 10*m) & x"00000000" & out_rxdata_mgt0(10*m - 1 downto 2*m) & x"00000000_00000000_00000000" & out_rxdata_mgt0(2*m - 1 downto 0);
      v_rxdata_mgt1 := out_rxdata_mgt1(16*m - 1 downto 10*m) & x"00000000" & out_rxdata_mgt1(10*m - 1 downto 2*m) & x"00000000_00000000_00000000" & out_rxdata_mgt1(2*m - 1 downto 0);
      v_rxdata_mgt2 := out_rxdata_mgt2(16*m - 1 downto 10*m) & x"00000000" & out_rxdata_mgt2(10*m - 1 downto 2*m) & x"00000000_00000000_00000000" & out_rxdata_mgt2(2*m - 1 downto 0);
      v_rxdata_mgt3 := out_rxdata_mgt3(16*m - 1 downto 10*m) & x"00000000" & out_rxdata_mgt3(10*m - 1 downto 2*m) & x"00000000_00000000_00000000" & out_rxdata_mgt3(2*m - 1 downto 0);

      v_ram_data_mgt0 := in_ram_data_mgt0(20*m - 1 downto 14*m) & in_ram_data_mgt0(13*m - 1 downto 5*m) & in_ram_data_mgt0(2*m - 1 downto 0);
      v_ram_data_mgt1 := in_ram_data_mgt1(20*m - 1 downto 14*m) & in_ram_data_mgt1(13*m - 1 downto 5*m) & in_ram_data_mgt1(2*m - 1 downto 0);
      v_ram_data_mgt2 := in_ram_data_mgt2(20*m - 1 downto 14*m) & in_ram_data_mgt2(13*m - 1 downto 5*m) & in_ram_data_mgt2(2*m - 1 downto 0);
      v_ram_data_mgt3 := in_ram_data_mgt3(20*m - 1 downto 14*m) & in_ram_data_mgt3(13*m - 1 downto 5*m) & in_ram_data_mgt3(2*m - 1 downto 0);

      m                  := 4;
      v_mgt_RXUSRCLK_OUT := in_mgt_RXUSRCLK_OUT (20*m - 1 downto 14*m) & in_mgt_RXUSRCLK_OUT (13*m - 1 downto 5*m) & in_mgt_RXUSRCLK_OUT (2*m - 1 downto 0);
      v_disperr_error := in_disperr_error (20*m - 1 downto 14*m) & in_disperr_error (13*m - 1 downto 5*m) & in_disperr_error (2*m - 1 downto 0);
      v_notable_error := in_notable_error (20*m - 1 downto 14*m) &  in_notable_error (13*m - 1 downto 5*m) &  in_notable_error (2*m - 1 downto 0);

      v_bcn_synch      := out_bcn_synch(16*m - 1 downto 10*m) & x"0" & out_bcn_synch (10*m - 1 downto 2*m) & x"0_0_0" & out_bcn_synch(2*m - 1 downto 0);
      v_crc_error_chan := out_crc_error_chan(16*m - 1 downto 10*m) & x"0" & out_crc_error_chan (10*m - 1 downto 2*m) & x"0_0_0" & out_crc_error_chan(2*m - 1 downto 0);  
      v_rx_resetdone := in_rx_resetdone(20*m - 1 downto 14*m) & in_rx_resetdone (13*m - 1 downto 5*m) & in_rx_resetdone (2*m - 1 downto 0);

      v_enable_mgt   := in_enable_mgt(20*m - 1 downto 14*m) & in_enable_mgt (13*m - 1 downto 5*m)&in_enable_mgt (2*m - 1 downto 0);
      v_MGT_Commadet := in_MGT_Commadet(20*m - 1 downto 14*m) & in_MGT_Commadet (13*m - 1 downto 5*m) & in_MGT_Commadet (2*m - 1 downto 0);

      v_kchar := in_kchar(20*m - 1 downto 14*m) & in_kchar (13*m - 1 downto 5*m) & in_kchar (2*m - 1 downto 0);
-- in: 19-9 & 7-5 & 1-0  out: 15-5 X 4-2 XXX 1-0       
    elsif fpga_number = 4 then
      m            := 1;
      v_MGT_Data   := in_MGT_Data(20*m - 1 downto 9*m) & in_MGT_Data(8*m - 1 downto 5*m) & in_MGT_Data(2*m - 1 downto 0);
      m            := 16;
      v_BC_Reg_sel := in_BC_Reg_sel (20*m - 1 downto 9*m) & in_BC_Reg_sel(8*m - 1 downto 5*m) & in_BC_Reg_sel (2*m - 1 downto 0);
      v_mux_sel    := in_mux_sel (20*m - 1 downto 9*m) & in_mux_sel(8*m - 1 downto 5*m) & in_mux_sel (2*m - 1 downto 0);

      v_delay_num     := out_delay_num(16*m - 1 downto 5*m) & x"0000" & out_delay_num(5*m - 1 downto 2*m) & x"0000_0000_0000" & out_delay_num(2*m - 1 downto 0);
      m               := 7;
      v_bc_cntr_0     := out_bc_cntr_0(16*m - 1 downto 5*m) & "0000000" & out_bc_cntr_0(5*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_cntr_0(2*m - 1 downto 0);
      v_bc_cntr_1     := out_bc_cntr_1(16*m - 1 downto 5*m) & "0000000" & out_bc_cntr_1(5*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_cntr_1(2*m - 1 downto 0);
      v_bc_cntr_2     := out_bc_cntr_2(16*m - 1 downto 5*m) & "0000000" & out_bc_cntr_2(5*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_cntr_2(2*m - 1 downto 0);
      v_bc_cntr_3     := out_bc_cntr_3(16*m - 1 downto 5*m) & "0000000" & out_bc_cntr_3(5*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_cntr_3(2*m - 1 downto 0);
      v_bc_mux_cntr_0 := out_bc_mux_cntr_0(16*m - 1 downto 5*m) & "0000000" & out_bc_mux_cntr_0(5*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_0(2*m - 1 downto 0);
      v_bc_mux_cntr_1 := out_bc_mux_cntr_1(16*m - 1 downto 5*m) & "0000000" & out_bc_mux_cntr_1(5*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_1(2*m - 1 downto 0);
      v_bc_mux_cntr_2 := out_bc_mux_cntr_2(16*m - 1 downto 5*m) & "0000000" & out_bc_mux_cntr_2(5*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_2(2*m - 1 downto 0);
      v_bc_mux_cntr_3 := out_bc_mux_cntr_3(16*m - 1 downto 5*m) & "0000000" & out_bc_mux_cntr_3(5*m - 1 downto 2*m) & "0000000"&"0000000"&"0000000" & out_bc_mux_cntr_3(2*m - 1 downto 0);

      m             := 32;
      v_rxdata_mgt0 := out_rxdata_mgt0(16*m - 1 downto 5*m) & x"00000000" & out_rxdata_mgt0(5*m - 1 downto 2*m) & x"00000000_00000000_00000000" & out_rxdata_mgt0(2*m - 1 downto 0);
      v_rxdata_mgt1 := out_rxdata_mgt1(16*m - 1 downto 5*m) & x"00000000" & out_rxdata_mgt1(5*m - 1 downto 2*m) & x"00000000_00000000_00000000" & out_rxdata_mgt1(2*m - 1 downto 0);
      v_rxdata_mgt2 := out_rxdata_mgt2(16*m - 1 downto 5*m) & x"00000000" & out_rxdata_mgt2(5*m - 1 downto 2*m) & x"00000000_00000000_00000000" & out_rxdata_mgt2(2*m - 1 downto 0);
      v_rxdata_mgt3 := out_rxdata_mgt3(16*m - 1 downto 5*m) & x"00000000" & out_rxdata_mgt3(5*m - 1 downto 2*m) & x"00000000_00000000_00000000" & out_rxdata_mgt3(2*m - 1 downto 0);


      v_ram_data_mgt0 := in_ram_data_mgt0(20*m - 1 downto 9*m) & in_ram_data_mgt0(8*m - 1 downto 5*m) & in_ram_data_mgt0(2*m - 1 downto 0);
      v_ram_data_mgt1 := in_ram_data_mgt1(20*m - 1 downto 9*m) & in_ram_data_mgt1(8*m - 1 downto 5*m) & in_ram_data_mgt1(2*m - 1 downto 0);
      v_ram_data_mgt2 := in_ram_data_mgt2(20*m - 1 downto 9*m) & in_ram_data_mgt2(8*m - 1 downto 5*m) & in_ram_data_mgt2(2*m - 1 downto 0);
      v_ram_data_mgt3 := in_ram_data_mgt3(20*m - 1 downto 9*m) & in_ram_data_mgt3(8*m - 1 downto 5*m) & in_ram_data_mgt3(2*m - 1 downto 0);

      m                  := 4;
      v_mgt_RXUSRCLK_OUT := in_mgt_RXUSRCLK_OUT (20*m - 1 downto 9*m) & in_mgt_RXUSRCLK_OUT(8*m - 1 downto 5*m) & in_mgt_RXUSRCLK_OUT(2*m - 1 downto 0);
      v_disperr_error := in_disperr_error (20*m - 1 downto 9*m) & in_disperr_error(8*m - 1 downto 5*m) & in_disperr_error(2*m - 1 downto 0);
      v_notable_error := in_notable_error (20*m - 1 downto 9*m) & in_notable_error(8*m - 1 downto 5*m) & in_notable_error(2*m - 1 downto 0);
      
      v_bcn_synch        := out_bcn_synch(16*m - 1 downto 5*m) & x"0" & out_bcn_synch (5*m - 1 downto 2*m) & x"0_0_0" & out_bcn_synch(2*m - 1 downto 0);
      v_crc_error_chan   := out_crc_error_chan(16*m - 1 downto 5*m) & x"0" & out_crc_error_chan (5*m - 1 downto 2*m) & x"0_0_0" & out_crc_error_chan(2*m - 1 downto 0);
     
      v_rx_resetdone := in_rx_resetdone (20*m - 1 downto 9*m) & in_rx_resetdone(8*m - 1 downto 5*m) & in_rx_resetdone (2*m - 1 downto 0);

      v_enable_mgt   := in_enable_mgt (20*m - 1 downto 9*m) & in_enable_mgt(8*m - 1 downto 5*m) & in_enable_mgt (2*m - 1 downto 0);
      v_MGT_Commadet := in_MGT_Commadet (20*m - 1 downto 9*m) & in_MGT_Commadet(8*m - 1 downto 5*m) & in_MGT_Commadet (2*m - 1 downto 0);

      v_kchar := in_kchar (20*m - 1 downto 9*m) & in_kchar(8*m - 1 downto 5*m) & in_kchar (2*m - 1 downto 0);

    end if;

    out_BC_Reg_sel       <= v_BC_Reg_sel;
    out_mux_sel          <= v_mux_sel;
    in_delay_num         <= v_delay_num;
    in_bc_cntr_0         <= v_bc_cntr_0;
    in_bc_cntr_1         <= v_bc_cntr_1;
    in_bc_cntr_2         <= v_bc_cntr_2;
    in_bc_cntr_3         <= v_bc_cntr_3;
    in_bc_mux_cntr_0     <= v_bc_mux_cntr_0;
    in_bc_mux_cntr_1     <= v_bc_mux_cntr_1;
    in_bc_mux_cntr_2     <= v_bc_mux_cntr_2;
    in_bc_mux_cntr_3     <= v_bc_mux_cntr_3;
    out_mgt_RXUSRCLK_OUT <= v_mgt_RXUSRCLK_OUT;
    out_enable_mgt       <= v_enable_mgt;
    in_bcn_synch           <= v_bcn_synch;
    in_crc_error_chan    <= v_crc_error_chan;
    out_disperr_error       <= v_disperr_error;
    out_notable_error      <= v_notable_error;
    out_rx_resetdone     <= v_rx_resetdone;
    out_MGT_Commadet     <= v_MGT_Commadet;
    out_MGT_Data         <= v_MGT_Data;

    in_rxdata_mgt0 <= v_rxdata_mgt0;
    in_rxdata_mgt1 <= v_rxdata_mgt1;
    in_rxdata_mgt2 <= v_rxdata_mgt2;
    in_rxdata_mgt3 <= v_rxdata_mgt3;

    out_kchar <= v_kchar;

    out_ram_data_mgt0 <= v_ram_data_mgt0;
    out_ram_data_mgt1 <= v_ram_data_mgt1;
    out_ram_data_mgt2 <= v_ram_data_mgt2;
    out_ram_data_mgt3 <= v_ram_data_mgt3;

  end MGT_SELECTOR_RX;



  procedure MGT_SELECTOR_TX (
    signal fpga_number : in integer;

    signal in_mgt_usr_clk : in std_logic_vector(79 downto 0);

    signal in_txdata_0  : in std_logic_vector(33 downto 0);
    signal in_txdata_1  : in std_logic_vector(33 downto 0);
    signal in_txdata_2  : in std_logic_vector(33 downto 0);
    signal in_txdata_3  : in std_logic_vector(33 downto 0);
    signal in_txdata_4  : in std_logic_vector(33 downto 0);
    signal in_txdata_5  : in std_logic_vector(33 downto 0);
    signal in_txdata_6  : in std_logic_vector(33 downto 0);
    signal in_txdata_7  : in std_logic_vector(33 downto 0);
    signal in_txdata_8  : in std_logic_vector(33 downto 0);
    signal in_txdata_9  : in std_logic_vector(33 downto 0);
    signal in_txdata_10 : in std_logic_vector(33 downto 0);
    signal in_txdata_11 : in std_logic_vector(33 downto 0);


    signal in_topo_k    : in std_logic;
    signal in_raw_k     : in std_logic;
    signal in_topo_data : in std_logic_vector(31 downto 0);
    signal in_raw_data  : in std_logic_vector(31 downto 0);

    signal out_topo_tob_clk : out std_logic;
    signal out_topo_raw_clk : out std_logic;
    signal out_mgt_usr_clk  : out std_logic_vector(11 downto 0);

    signal out_txcharisk_quad_array : out mgt_txcharisk_array(19 downto 0);
    signal out_txdata_quad_array    : out mgt_txdata_array(19 downto 0)

    ) is

    variable v_txcharisk_quad_array : mgt_txcharisk_array(19 downto 0) := (others => ZERO_MGT_TXCHARISK);
    variable v_txdata_quad_array    : mgt_txdata_array(19 downto 0)    := (others => ZERO_MGT_TXDATA);

    variable v_topo_tob_clk : std_logic                     := '0';
    variable v_topo_raw_clk : std_logic                     := '0';
    variable v_mgt_usr_clk  : std_logic_vector(11 downto 0) := (others => '0');


  begin

    --210(first 2),211,212,213
    if fpga_number = 1 then  --111,112,210(first 2 SLOW),211 (prototype)
      v_txcharisk_quad_array(10).gt0_txcharisk := "000"& in_topo_k;
      v_txcharisk_quad_array(10).gt1_txcharisk := "000"& in_raw_k;

      v_txcharisk_quad_array(11).gt0_txcharisk := "000"& in_txdata_0(32);
      v_txcharisk_quad_array(11).gt1_txcharisk := "000"& in_txdata_1(32);
      v_txcharisk_quad_array(11).gt2_txcharisk := "000"& in_txdata_2(32);
      v_txcharisk_quad_array(11).gt3_txcharisk := "000"& in_txdata_3(32);
      v_txcharisk_quad_array(12).gt0_txcharisk := "000"& in_txdata_4(32);
      v_txcharisk_quad_array(12).gt1_txcharisk := "000"& in_txdata_5(32);
      v_txcharisk_quad_array(12).gt2_txcharisk := "000"& in_txdata_6(32);
      v_txcharisk_quad_array(12).gt3_txcharisk := "000"& in_txdata_7(32);
      v_txcharisk_quad_array(13).gt0_txcharisk := "000"& in_txdata_8(32);
      v_txcharisk_quad_array(13).gt1_txcharisk := "000"& in_txdata_9(32);
      v_txcharisk_quad_array(13).gt2_txcharisk := "000"& in_txdata_10(32);
      v_txcharisk_quad_array(13).gt3_txcharisk := "000"& in_txdata_11(32);

      v_txdata_quad_array(10).gt0_txdata_in := in_topo_data;
      v_txdata_quad_array(10).gt1_txdata_in := in_raw_data;

      v_txdata_quad_array(11).gt0_txdata_in := in_txdata_0(31 downto 0);
      v_txdata_quad_array(11).gt1_txdata_in := in_txdata_1(31 downto 0);
      v_txdata_quad_array(11).gt2_txdata_in := in_txdata_2(31 downto 0);
      v_txdata_quad_array(11).gt3_txdata_in := in_txdata_3(31 downto 0);
      v_txdata_quad_array(12).gt0_txdata_in := in_txdata_4(31 downto 0);
      v_txdata_quad_array(12).gt1_txdata_in := in_txdata_5(31 downto 0);
      v_txdata_quad_array(12).gt2_txdata_in := in_txdata_6(31 downto 0);
      v_txdata_quad_array(12).gt3_txdata_in := in_txdata_7(31 downto 0);
      v_txdata_quad_array(13).gt0_txdata_in := in_txdata_8(31 downto 0);
      v_txdata_quad_array(13).gt1_txdata_in := in_txdata_9(31 downto 0);
      v_txdata_quad_array(13).gt2_txdata_in := in_txdata_10(31 downto 0);
      v_txdata_quad_array(13).gt3_txdata_in := in_txdata_11(31 downto 0);

      v_topo_raw_clk := in_mgt_usr_clk(41);
      v_topo_tob_clk := in_mgt_usr_clk(40);
      --                                        13                                    12                                  11
      v_mgt_usr_clk  := in_mgt_usr_clk(13*4+3 downto 13*4) & in_mgt_usr_clk(12*4+3 downto 12*4) & in_mgt_usr_clk(11*4+3 downto 11*4);

    elsif fpga_number = 2 then          --210(first 2),217,218,219
      --119,210(first 2 SLOW),217,218 (prototype)

      v_txcharisk_quad_array(10).gt0_txcharisk := "000"& in_topo_k;
      v_txcharisk_quad_array(10).gt1_txcharisk := "000"& in_raw_k;

      v_txcharisk_quad_array(17).gt0_txcharisk := "000"& in_txdata_0(32);
      v_txcharisk_quad_array(17).gt1_txcharisk := "000"& in_txdata_1(32);
      v_txcharisk_quad_array(17).gt2_txcharisk := "000"& in_txdata_2(32);
      v_txcharisk_quad_array(17).gt3_txcharisk := "000"& in_txdata_3(32);

      v_txcharisk_quad_array(18).gt0_txcharisk := "000"& in_txdata_4(32);
      v_txcharisk_quad_array(18).gt1_txcharisk := "000"& in_txdata_5(32);
      v_txcharisk_quad_array(18).gt2_txcharisk := "000"& in_txdata_6(32);
      v_txcharisk_quad_array(18).gt3_txcharisk := "000"& in_txdata_7(32);
      v_txcharisk_quad_array(19).gt0_txcharisk := "000"& in_txdata_8(32);
      v_txcharisk_quad_array(19).gt1_txcharisk := "000"& in_txdata_9(32);
      v_txcharisk_quad_array(19).gt2_txcharisk := "000"& in_txdata_10(32);
      v_txcharisk_quad_array(19).gt3_txcharisk := "000"& in_txdata_11(32);

      v_txdata_quad_array(10).gt0_txdata_in := in_topo_data;
      v_txdata_quad_array(10).gt1_txdata_in := in_raw_data;

      v_txdata_quad_array(17).gt0_txdata_in := in_txdata_0(31 downto 0);
      v_txdata_quad_array(17).gt1_txdata_in := in_txdata_1(31 downto 0);
      v_txdata_quad_array(17).gt2_txdata_in := in_txdata_2(31 downto 0);
      v_txdata_quad_array(17).gt3_txdata_in := in_txdata_3(31 downto 0);
      v_txdata_quad_array(18).gt0_txdata_in := in_txdata_4(31 downto 0);
      v_txdata_quad_array(18).gt1_txdata_in := in_txdata_5(31 downto 0);
      v_txdata_quad_array(18).gt2_txdata_in := in_txdata_6(31 downto 0);
      v_txdata_quad_array(18).gt3_txdata_in := in_txdata_7(31 downto 0);
      v_txdata_quad_array(19).gt0_txdata_in := in_txdata_8(31 downto 0);
      v_txdata_quad_array(19).gt1_txdata_in := in_txdata_9(31 downto 0);
      v_txdata_quad_array(19).gt2_txdata_in := in_txdata_10(31 downto 0);
      v_txdata_quad_array(19).gt3_txdata_in := in_txdata_11(31 downto 0);

      v_topo_raw_clk := in_mgt_usr_clk(41);
      v_topo_tob_clk := in_mgt_usr_clk(40);
      --                                       19                                    18                                   17
      v_mgt_usr_clk  := in_mgt_usr_clk(19*4+3 downto 19*4) & in_mgt_usr_clk(18*4+3 downto 18*4) & in_mgt_usr_clk(17*4+3 downto 17*4);

    elsif fpga_number = 3 then  --first 2 MGTs of quad 213 (same as prototype)
      v_txcharisk_quad_array(13).gt0_txcharisk := "000"& in_topo_k;
      v_txcharisk_quad_array(13).gt1_txcharisk := "000"& in_raw_k;

      v_txdata_quad_array(13).gt0_txdata_in := in_topo_data;
      v_txdata_quad_array(13).gt1_txdata_in := in_raw_data;

      v_mgt_usr_clk  := (others => '0');
      v_topo_raw_clk := in_mgt_usr_clk(53);
      v_topo_tob_clk := in_mgt_usr_clk(52);

    elsif fpga_number = 4 then  --first 2 MGTs of quad 118 (same as prototype
      v_txcharisk_quad_array(8).gt0_txcharisk := "000"& in_topo_k;
      v_txcharisk_quad_array(8).gt1_txcharisk := "000"& in_raw_k;

      v_txdata_quad_array(8).gt0_txdata_in := in_topo_data;
      v_txdata_quad_array(8).gt1_txdata_in := in_raw_data;

      v_mgt_usr_clk  := (others => '0');
      v_topo_raw_clk := in_mgt_usr_clk(33);
      v_topo_tob_clk := in_mgt_usr_clk(32);

    else
      --do something here
      v_mgt_usr_clk := (others => '0');

    end if;
    out_txcharisk_quad_array <= v_txcharisk_quad_array;
    out_txdata_quad_array    <= v_txdata_quad_array;
    out_topo_tob_clk         <= v_topo_tob_clk;
    out_topo_raw_clk         <= v_topo_raw_clk;
    out_mgt_usr_clk          <= v_mgt_usr_clk;
  end MGT_SELECTOR_TX;

end ProcessorFPGAPackage;
