 
---------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/07/2019 04:28:02 PM
-- Design Name: 
-- Module Name: module_information - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;


entity commonid_information is
generic (addr_width :natural :=1);    
   port(
         ipb_clk  : in std_logic;
         reset    : in std_logic;
         ipbus_in : in ipb_wbus;
	     ipbus_out: out ipb_rbus;
         data_0      : in std_logic_vector(31 downto 0);
         data_1      : in std_logic_vector(31 downto 0)
        
	);
	

end commonid_information;

architecture Behavioral of commonid_information is
signal sel:integer;
signal ack: std_logic;
begin

sel <= to_integer(unsigned(ipbus_in.ipb_addr(addr_width-1 downto 0))) when addr_width > 0 else 0;

 process(ipb_clk,reset)
 
     begin
     
      if ipb_clk' event and ipb_clk='1' then
        if reset ='1' then
          ipbus_out.ipb_rdata <= (others =>'0');
      elsif ipbus_in.ipb_strobe= '1' and ipbus_in.ipb_write ='1' then
             ipbus_out.ipb_rdata <= (others =>'0');
        end if;
             
         if   sel = 0 and ipbus_in.ipb_strobe= '1' and ipbus_in.ipb_write ='0'then            
                ipbus_out.ipb_rdata <=  data_0;
                ack <= ipbus_in.ipb_strobe and not ack;
          
          end if;            
      
                
          if sel = 1 and ipbus_in.ipb_strobe= '1' and ipbus_in.ipb_write ='0'then    
              ipbus_out.ipb_rdata <=  data_1;
               ack <= ipbus_in.ipb_strobe and not ack;
               
          end if; 
 
          end if;       
       
        end process;
       
       ipbus_out.ipb_ack <= ack;
       ipbus_out.ipb_err <= '0';
       
end Behavioral;
