--! @file
--! @brief Top of Sorting TOBs for process FPGA
--! @details 
--! \verbatim
--! This module receives 7 x 32-bit TOBs together with 7 valid signals in series, 
--! and sorts them into a 6 x 32-bit parallel word and 6 bit valid word to store in Circular DPRAM.
--! The 7th TOB is ignored as a trailer is added to transfer the data to TOPO.
--! In order to be able to read multi-slices, the TOBs and XTOBs must be converted into long parallel data words.
--!
--! This module is only instanstiated for Process FPGA 1 and 2, and is disabled for Process FPGA 3 and 4.
--!
--! Only Process FPGA 1 and 2 transmit e/g and tau events to TOPO, and the off-line Readout,
--! but Process FPGA 3 and 4 do not transmit to TOPO instead they send their TOBs to Process FPGA 1 and 2.
--! Process FPGA 3 and 4 transmit XTOB only events to the Control FPGA.
--!
--! The FSM then reads data from DPRAM into FIFO upon arraival of L1A
--! The SIPO data of 271b is saved every BC tick to enable multi-slice readout.
--!
--! \endverbatim 
--! @author Saeed Taghavi


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;

--! @copydoc T_TOBs_sorting.vhd
entity T_TOBs_sorting is
    Generic
        ( 
    --! Integer used to distinguish different FPGAs having a slightly different firmware
    FPGA_NUMBER : integer
        ) ; 
    Port ( 
       --! L1A signal input 
       L1A_in                : in STD_LOGIC;
       --! TOB Readout FIFO reset Pulsed by software command
       TOB_FIFO_sw_rst           : in std_logic ;       
       --! Sorted TOBs 32b * 7 in series
       TOBs_32b_in             : in STD_LOGIC_VECTOR (31 downto 0);   
       --! Sorted TOB synch signal - indicating the start of the 7 TOB data words.
       TOBs_sync_in            : in STD_LOGIC;                        
       --! Sorted TOB valid signal - used to write TOBs into de-randomisation TOB FIFO
       TOBs_valid_flg_in       : in STD_LOGIC;                        
       --! Sorted TOB BCN with delay through ALGO/sorting block
       ALGO_TOB_BCN_in         : in std_logic_vector(6 downto 0);
       --! Read sorted TOB data from de-randomisation TOB FIFO into TOB  Link Output FIFO
       FIFO_rd_en               : in STD_LOGIC;                        
       --! 4b error flags for 49 links - OR of 49 fibre error flags together.
       link_err_4b_in           : in STD_LOGIC_VECTOR (3 downto 0);    
       --! 280Mhz input signal  
       clk_280M_in              : in STD_LOGIC;
       --! Latency pre-load for TOB Circular DRPAM write address
       pre_ld_wr_addr_TOB        : in STD_LOGIC_VECTOR (8 downto 0);
       --! Number of multi-slice locations to read from DPRAM
       DPR_locations_to_rd   : in STD_LOGIC_VECTOR (2 downto 0); 
       --! Threshold to assert FIFO prog full flag
       TOBs_FIFO_pFULL_THRESH_assert     : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
       --! Threshold to de-assert FIFO prog full flag
       TOBs_FIFO_pFULL_THRESH_negate     : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
       --! Sorted TOBs output of fifo 209b = 7b TOB_BCN +4b error+6b valid +192b TOB data
       rdout_T_TOB_209b         : out STD_LOGIC_VECTOR (208 downto 0);    
       --! Sorted TOBs FIFO occupancy count
       TOBs_FIFO_data_count     : out STD_LOGIC_VECTOR (8 downto 0) ; 
       --! Sorted TOBs FIFO full flag    
       TOBs_data_full           : out STD_LOGIC;
       --! Sorted TOBs FIFO empty flag    
       TOBs_data_empty          : out STD_LOGIC;
       --! Sorted TOBs FIFO data valid signal    
       TOBs_data_valid          : out STD_LOGIC;
       --! Sorted TOBs FIFO prog full flag    
       TOBs_data_prog_full      : out STD_LOGIC
       );
    end T_TOBs_sorting;

--! @copydoc T_TOBs_sorting.vhd
architecture RTL of T_TOBs_sorting is

--************************** Register Declarations ****************************    
 
    signal  clk_in_280M_i          : STD_LOGIC;
    -- Sorted TOBs signals
    signal  SIPO_T_TOB_out_i       : STD_LOGIC_VECTOR (191 downto 0);   -- T_TOBs 32b * 6 = 192
    signal  SIPO_T_TOB_sync_out_i  : STD_LOGIC;                 
    signal  T_TOB_valid_out_i      : STD_LOGIC_VECTOR (5 downto 0);     -- T TOBs valid 6b (ignore 7th TOB)
                     
    signal  DPR_T_TOBs_in_i         : STD_LOGIC_VECTOR (208 downto 0);  -- 209b = 7b TOB_BCN +4b error+6b valid +192b data
    signal  DPR_T_TOBs_out_i        : STD_LOGIC_VECTOR (208 downto 0);  -- 209b = 7b TOB_BCN +4b error+6b valid +192b data 
    signal  rdout_T_TOB_209b_i        : STD_LOGIC_VECTOR (208 downto 0);  -- 209b = 7b TOB_BCN +4b error+6b valid +192b data 

    signal  DPR_T_TOB_rd_addr_i     : STD_LOGIC_VECTOR (8 downto 0);
    signal  DPR_T_TOB_wr_addr_i     : STD_LOGIC_VECTOR (8 downto 0);
    signal  DRP_T_TOB_rd_en_i       : std_logic ;
    
    signal  SIPO_T_TOB_sync_vec_i   : STD_LOGIC_VECTOR (0 downto 0);
    signal  FIFO_wr_en_i, L1A_in_i  : std_logic;
    signal  FIFO_wr_en_tmp          : std_logic ;

    signal  ALGO_TOB_BCN_out_i      : std_logic_vector (6 downto 0); -- sorted TOB BCN with delay through ALGO/sorting block
    signal  link_err_4b_out_i       :STD_LOGIC_VECTOR (3 downto 0) ;
    signal  TOBs_FIFO_data_count_i  :STD_LOGIC_VECTOR (8 downto 0) ;
    signal  tied_to_vcc_i           : std_logic;
    signal  TOBs_data_valid_i       : std_logic;
    signal  TOBs_data_prog_full_i, TOBs_data_full_i, TOBs_data_empty_i            : std_logic;

    signal TOB_BCID_in_ila        : STD_LOGIC_VECTOR (31 downto 0);
    signal TOB_BCID_out_ila       : STD_LOGIC_VECTOR (31 downto 0);

begin

    tied_to_vcc_i  <=   '1';

    clk_in_280M_i  <= clk_280M_in ;  
    
--! @details
--! \verbatim
--! SIPO for Sorted TOBs module is responsible for generating a parallel word,
--! consisting of 6 x 32-bit TOBs and 6 valid flags.
--! \endverbatim 
U1_TOBs_eg : entity TOB_rdout_lib.SIPO_TOPO_TOBs_unit    -- convert 6*32b Topo TOBs into one 192b word for Circular DPR
    Port map (                                                                                                 
        clk_in_280M              =>  clk_in_280M_i,                                                             
        TOBs_in                  =>  TOBs_32b_in,        -- 32b TOPO TOB                                   
        TOBs_sync_in             =>  TOBs_sync_in,                                                        
        TOB_valid_flg_in         =>  TOBs_valid_flg_in, 
        link_err_4b_in           =>  link_err_4b_in,    -- input from top 4b error flags CT + BN2 + TIE + EIE
        ALGO_TOB_BCN_in          =>  ALGO_TOB_BCN_in,  -- input from top                                     
        TOPO_TOB_out             =>  SIPO_T_TOB_out_i,      -- 6 * 32b TOPO TOBs           
        TOPO_TOB_sync_out        =>  SIPO_T_TOB_sync_out_i,     -- plus Sync signal
        TOPO_TOB_valid_out       =>  T_TOB_valid_out_i,          -- plus Valid signal
        ALGO_TOB_BCN_out         =>  ALGO_TOB_BCN_out_i,
        link_err_4b_out          =>  link_err_4b_out_i
        );

    -- L1A needs to be delayed by 1 clk so correct value is store just prior to another L1A increments the counter
    -- 209b = 7b TOB_BCN +4b error+6b valid +192b data
    DPR_T_TOBs_in_i <=  ALGO_TOB_BCN_out_i & link_err_4b_out_i  & T_TOB_valid_out_i & SIPO_T_TOB_out_i;
    SIPO_T_TOB_sync_vec_i(0)     <= SIPO_T_TOB_sync_out_i ;   -- active once every 7 clks

--! @details
--! \verbatim
--! This state machine is responsible for reading 6 x 32-bit TOBs and 6 valid flags from Circular DPRAM,
--! and storing them into de-randomisation FIFO for the TOBs..
--! \endverbatim 
U3_TOBs_wr_FSM :  entity TOB_rdout_lib.fsm_TOB_wr_to_FIFO
    PORT map (
        CLK_IN                 => clk_in_280M_i ,
        TOB_FIFO_sw_rst        => TOB_FIFO_sw_rst,    -- RST OR TOB_FIFO_sw_rst
        L1A_in                 => L1A_in_i ,
        -- this is sync signal which enables WR addr to increase 
        -- can be either TOB_eg_sync_in or TOB_tau_sync_in as they are identical
        SIPO_sync_in           => TOBs_sync_in ,  
        pre_ld_wr_addr         => pre_ld_wr_addr_TOB ,        -- i/p
        DPR_locations_to_rd    => DPR_locations_to_rd ,   -- number of DRP locations to read 1 to 5
        DPR_rd_addr            => DPR_T_TOB_rd_addr_i ,
        DPR_wr_addr            => DPR_T_TOB_wr_addr_i ,
        DRP_rd_en              => DRP_T_TOB_rd_en_i ,
        FIFO_wr_en             => FIFO_wr_en_i
       );
       
--! @details
--! \verbatim
--! This is Circular DPRAM for 192-bit sorted TOBs, 6-bit valid flags and 12-bit BCN
--! only 6 TOBs are written, 7th is ignored.
--! Each location of DPRAM stores all the TOBs for for one BCN.
--! \endverbatim 
U4_T_TOB_DRP : DPR_209b_512     -- Topo TOBs  into DPRAM
    PORT map (
        clka   => clk_in_280M_i,    -- runs at 280M but writes 1 in 7 clks
        ena    => tied_to_vcc_i  , 
        wea    => SIPO_T_TOB_sync_vec_i  ,   -- this is wr en using input sync sig
        addra  => DPR_T_TOB_wr_addr_i ,
        dina   => DPR_T_TOBs_in_i  ,         -- this is TOBs together 242b = 7b+4b+7b+224b TOB_BCN+ERR+valid+T_TOB
        clkb   => clk_in_280M_i  ,
        enb    => DRP_T_TOB_rd_en_i  ,       -- clk/rst/addr en for DPRAM port B
        addrb  => DPR_T_TOB_rd_addr_i  ,
        doutb  => DPR_T_TOBs_out_i           --  TOB out 242b = 7b+4b+7b+224b TOB_BCN+ERR+valid+T_TOB
        );

--! @details
--! \verbatim
--! This is de-randomistion FIFO for 192-bit sorted TOBs, 6-bit valid flags and 12-bit BCN.
--! When an L1A arrives, the data associated with correct BCN is transferred from Circular DPRAM into de-randomistion FIFO.
--! \endverbatim 
U5_T_TOBs_fifo : FIFO_209b_512      -- Topo TOBs into FIFO on L1A
    PORT MAP (
        clk         => clk_in_280M_i,
        srst        => TOB_FIFO_sw_rst,         -- RST OR TOB_FIFO_sw_rst
        din         => DPR_T_TOBs_out_i ,       --  TOB out 242b = 7b+4b+7b+224b TOB_BCN+ERR+valid+T_TOB
        wr_en       => FIFO_wr_en_tmp,          -- stop wrtie if prog FULL is set
        rd_en       => FIFO_rd_en,
        dout        => rdout_T_TOB_209b_i  ,      --  TOB out 242b = 7b+4b+7b+224b TOB_BCN+ERR+valid+T_TOB
        data_count  => TOBs_FIFO_data_count_i,    -- TOBs FIFO occupancy count
        full        => TOBs_data_full_i,
        empty       => TOBs_data_empty_i,
        valid       => TOBs_data_valid_i,
        prog_full   => TOBs_data_prog_full_i,
        prog_full_thresh_assert     => TOBs_FIFO_pFULL_THRESH_assert,
        prog_full_thresh_negate     => TOBs_FIFO_pFULL_THRESH_negate
        );
    
-- output signals:
-- If Process FPGA 1 & 2, then assign the outputs to TOBs data
-- If Process FPGA 3 & 4, then assign the outputs to ZERO, no TOB data in these FPGAs
U1_TOB_sorting_gen : if FPGA_NUMBER = 1 or FPGA_NUMBER = 2 generate
       rdout_T_TOB_209b         <= rdout_T_TOB_209b_i ;
       TOBs_FIFO_data_count     <= TOBs_FIFO_data_count_i ; 
       TOBs_data_full           <= TOBs_data_full_i;
       TOBs_data_empty          <= TOBs_data_empty_i;
       TOBs_data_valid          <= TOBs_data_valid_i;
       TOBs_data_prog_full      <= TOBs_data_prog_full_i;
       FIFO_wr_en_tmp <= FIFO_wr_en_i AND (NOT TOBs_data_prog_full_i) ; -- prevent wr to FIFO if pFULL = 1
       L1A_in_i                 <= L1A_in ;

    else generate
       rdout_T_TOB_209b         <= (others => '0');    
       TOBs_FIFO_data_count     <= (others => '0') ;     
       TOBs_data_full           <= '0';
       TOBs_data_empty          <= '0';
       TOBs_data_valid          <= '0';
       TOBs_data_prog_full      <= '0';
       FIFO_wr_en_tmp           <= '0' ; -- diable FIFO_wr_en 
       L1A_in_i                 <= '0' ; -- diable L1A input
    end generate U1_TOB_sorting_gen;
      

    TOB_BCID_in_ila  <= X"000000" & "00" & DPR_T_TOBs_out_i(208 downto 203);
    TOB_BCID_out_ila <= X"000000" & "00" & rdout_T_TOB_209b_i(208 downto 203);

--U5_ila_T_TOBs_fifo : ila_ipbus_fabric_rd_wr
--PORT MAP (
--	clk    =>  clk_in_280M_i,     
--	probe0 =>  DPR_T_TOBs_out_i(31 downto 0),       -- 32b    
--	probe1 =>  DPR_T_TOBs_out_i(63 downto 32),  -- 32b 
--	probe2(0) =>  FIFO_wr_en_i,   -- 1b
--	probe3(0) =>  FIFO_rd_en,           -- 1b
--	probe4(0) =>  L1A_in_i ,   -- 1b
--	probe5 =>  rdout_T_TOB_209b_i(31 downto 0),  -- 32b 
--	probe6 =>  rdout_T_TOB_209b_i(63 downto 32),        -- 32b
--	probe7(0) =>   SIPO_T_TOB_sync_out_i,             -- 1b
--	probe8(0) =>   FIFO_wr_en_tmp ,       -- 1b
--    probe9 =>   (others => '0')              -- 1b
--);  

end RTL;
