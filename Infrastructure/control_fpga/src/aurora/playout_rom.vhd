----------------------------------------------------------------------------------
-- Company:   University of Cambridge
-- Engineer:  Ed Flaherty
-- 
-- Create Date: 18.10.2017 14:52:01
-- Design Name: 
-- Module Name: playout_rom - RTL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- This is a playout ROM that provides bursts.   During a burst, the entire contents of the memory is dumped. 
-- The gap between bursts is set by the GENERIC GAP_WIDTH.  The duration of the gap is 2**GAP_WIDTH clocks. 
----------------------------------------------------------------------------------
--
--this module's interface implements a subset of the AXI4 streaming protocol
--run this simulstion to see the timing
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity playout_rom is
   generic
(
 DATA_WIDTH  : integer :=   64;      -- DATA bus width
 ADDR_WIDTH  : integer :=   5;       -- width of rom address bus
 GAP_WIDTH   : integer :=   8;       -- width of gap counter ex; 5 makes gap of 32 cycles 
 --STRB_WIDTH  : integer :=   2;       -- STROBE bus width
 REM_WIDTH   : integer :=   1        -- REM bus width
);  
 
 Port ( 
        clock                        : in    STD_LOGIC;
        reset                        : in    STD_LOGIC;
        AXI4_M_TDATA                 : out   std_logic_vector (DATA_WIDTH-1 downto 0);
--        AXI4_M_TKEEP                 : out   std_logic_vector (STRB_WIDTH-1 downto 0);
        AXI4_M_TKEEP                 : out   std_logic_vector ((data_width/8)-1 downto 0);
        AXI4_M_TVALID                : out   std_logic;
        AXI4_M_TLAST                 : out   std_logic;
        AXI4_M_TREADY                : in    std_logic;
        first                        : out   std_logic

    );
        

end playout_rom;

architecture RTL of playout_rom is
signal rom_addr : STD_LOGIC_VECTOR (ADDR_WIDTH-1 downto 0)  := (others => '0');
signal addr_limit : STD_LOGIC_VECTOR (ADDR_WIDTH-1 downto 0)  := (others => '1');
signal rom_data : STD_LOGIC_VECTOR (DATA_WIDTH-1 downto 0);
signal data_reg : STD_LOGIC_VECTOR (DATA_WIDTH-1 downto 0)  := (others => '0');
signal gap_count : STD_LOGIC_VECTOR (GAP_WIDTH-1 downto 0) := (others => '0');
signal play  : std_logic := '0'; 
signal tvalid  : std_logic := '0';
signal gap_limit : STD_LOGIC_VECTOR (GAP_WIDTH-1 downto 0) := (others => '1');
signal t_keep : STD_LOGIC_VECTOR ((data_width/8)-1 downto 0) := (others => '1');

component dist_mem_ROM 
Port ( 
    a : in STD_LOGIC_VECTOR ( ADDR_WIDTH-1 downto 0 );
    spo : out STD_LOGIC_VECTOR (DATA_WIDTH-1 downto 0 )
  ); 
end component; 

begin
gap_limit <= (others => '1');
addr_limit <= (others => '1');


rom : dist_mem_ROM
port map (
   a       =>  rom_addr, 
   spo     =>  rom_data 
);


--Adress Register 
process (clock) begin 
  if rising_edge (clock) then 
    if reset = '1' or play = '0' then 
       rom_addr <= (others => '0');
--adding tready       
    elsif (AXI4_M_TREADY and play) = '1' then
       rom_addr <= rom_addr + 1; 
    else 
       rom_addr <= rom_addr;
    end if;
  end if;  
 end process;   
    
--gap count 
process (clock) begin 
  if rising_edge (clock) then 
    if reset = '1' then 
      gap_count <= (others => '0');
    elsif play ='0' then 
      gap_count <= gap_count+1;  
    end if; 
  end if; 
end process;      


--play 
process (clock) begin 
  if rising_edge (clock) then 
--     if (reset = '1') or (rom_addr(addr_width-1 downto 0) = ("11111")) then 
     if (reset = '1') or (rom_addr = addr_limit) then 
         play <= '0'; 
     elsif (gap_count = gap_limit) then
         play <= '1'; 
     end if; 
   end if; 
end process; 
         
--dat reg 
process (clock) begin 
  if rising_edge (clock) then
     if (AXI4_M_TREADY and play) = '1' then
        data_reg <= rom_data;
     else
        data_reg <= data_reg;
     end if;       
   end if; 
end process; 

--tvalid
process (clock) begin 
  if rising_edge (clock) then 
    tvalid <= play; 
  end if; 
end process; 

--first        added mjsiyad

process (clock) begin 
  if rising_edge (clock) then 
    first <= play and not tvalid; 
  end if; 
end process; 

      
AXI4_M_TDATA    <= data_reg;   
AXI4_M_TVALID   <= tvalid;      
AXI4_M_TLAST    <= tvalid and not play;
--AXI4_M_TKEEP    <= (others => '0'); 
t_keep <= (others => '1'); 
AXI4_M_TKEEP    <= t_keep; 
 
      
end RTL;
