"""
Created on March 7 2020
@author Alexios Stampekis
Pattern 3: Just an auxiliary pattern, very basic, for illustration reasons. 
We create a number of 40 BCs (random but "enough"), and fill only one MGT 
(here MGT0), only one word (here word_14) with a value (here word_value=1),
for a specific BC (here BC=20).
"""

import sys
import InputComponents as InC

def Run():
    mgt=InC.MGT(0, 40)   # arguments are id, Nbc
    mgt.set_bc_word(20, 14, 1)   # arguments are: nbc, word#, word_value
    # in the Output Components module just do: InW[20, 0, 14]=1
    # We only care for ONE MGT-WORD mapping, so we have
    # more than enough BCs around the "active" one
    
    MGTs=[]    # all the others are zero filled
    for i in range(1, 64):
        MGT=InC.MGT(i, 40)
        MGTs.append(MGT)
    
    print("Writing to files")
    mgt.export_file()
    for v in MGTs:
        v.export_file()

    print("Done")


if __name__=="__main__":
    sys.exit(Run() or 0)

