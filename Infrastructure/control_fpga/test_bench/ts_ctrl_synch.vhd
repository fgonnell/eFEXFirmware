 
----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/29/2016 06:57:55 PM
-- Design Name: 
-- Module Name: test_tac_sm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library xil_defaultlib;



entity ts_ctrl_synch is
--  Port ( );
end ts_ctrl_synch;

architecture Behavioral of ts_ctrl_synch is
SIGNAL CLK160      : std_logic;
SIGNAL MGT_COMMADET : std_logic;
SIGNAL Delay_cnt    : std_logic_vector(3 DOWNTO 0);
SIGNAL Mux_Delay1   : std_logic_vector(2 DOWNTO 0);
SIGNAL TTC_clk      : std_logic;
SIGNAL cntr_enable  : std_logic;
SIGNAL cntrl_mux    : std_logic_vector(1 DOWNTO 0);
SIGNAL data_in      : std_logic_vector(31 DOWNTO 0);
SIGNAL data_out     : std_logic_vector(127 DOWNTO 0);
SIGNAL mux_cntrl,data,delay_num,mux_sel     : std_logic_vector(3 DOWNTO 0);
SIGNAL reset        : std_logic;
signal start        : std_logic;
SIGNAL data_out_reg128 :std_logic_vector (127 downto 0);

signal reg128_latch: std_logic;
signal enable_mgt,ttc_pipe,bcn_synch,delay_latch : std_logic;


begin



test_bench:entity work.testbench_ctrl_synch
      PORT MAP (
         CLK160     => CLK160,
         MGT_COMMADET => MGT_COMMADET,
         cntrl_mux    => cntrl_mux,
         TTC_clk      => TTC_clk,
         data_in      => data_in,
         enable_mgt   => enable_mgt,
         reset        => reset,
         start        => start
        
      );
    

     
               
top_level: entity work.top_cntrl_synch

       PORT MAP (
           rx_clk160    =>  clk160,                                          
           TTC_clk      =>  TTC_clk ,                                         
           reset        =>  reset ,                                             
           enable_mgt   => enable_mgt,                                      
           MGT_Commadet =>  MGT_Commadet,                                 
           reg_sel       => "0000",                                            
           mux_sel       => "0000",                                           
           start         =>  start,                                      
           rx_resetdone => '1',                                                  
           reg128_latch  =>  reg128_latch,                               
           --delay_latch   => delay_latch,                               
           --ttc_pipe     => ttc_pipe,                                     
           delay_num     => delay_num,                                      
           --bcn_synch    => bcn_synch,
           --data_out_reg128 => data_out_reg128,              
           data_in       =>  data_in,
           data_out      =>  data_out
           );
           

           

 

end Behavioral;
