--------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 26.01.2017 10:49:31
-- Design Name: eFEX Trigger Algorithm
-- Module Name: Multi-Multiplier - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Multiple Multiplier
-- 
-- Dependencies: DataTypes package, Mult component
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.all;


-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library work;
use work.DataTypes.all;

entity MultiMultiplier is
  generic (parameters : integer := 3);

  port (
    CLK           : in  std_logic;
    IN_Word       : in  DataWord;
    IN_parameters : in  AlgoParameters(parameters-1 downto 0);
    OUT_Overflow  : out std_logic_vector(parameters-1 downto 0);
    OUT_Words     : out DataWords(parameters-1 downto 0)
    );

end MultiMultiplier;

architecture Behavioral of MultiMultiplier is

  component Mult
    port (
      CLK : in  std_logic;
      A   : in  std_logic_vector(15 downto 0);
      B   : in  std_logic_vector(7 downto 0);
      P   : out std_logic_vector(23 downto 0)
      );
  end component;

  type param_t is array(natural range <>) of std_logic_vector(MULTIPLIER_PAR_INPUT_WIDTH-1 downto 0);
  type data_t is array(natural range <>) of std_logic_vector(MULTIPLIER_DATA_INPUT_WIDTH-1 downto 0);
  type result_big_t is array(natural range <>) of std_logic_vector(MULTIPLIER_OUTPUT_WIDTH-1 downto 0);

-- Multipliers input
  signal Param : param_t(parameters-1 downto 0);
  signal Data  : data_t(parameters-1 downto 0);

-- Multipliers output
  signal Overflow   : std_logic_vector(parameters-1 downto 0);
  signal Result_big : result_big_t(parameters-1 downto 0);
  signal Result     : DataWords(parameters-1 downto 0);

  constant ZERO_Result : std_logic_vector(Result_big(0)'high downto Result(0)'high+1) := (others => '0');


begin
  OUT_Words <=  Result;
  OUT_Overflow <=  Overflow;
  
  MULT_FOR : for i in 0 to parameters-1 generate

    Param(i) <= (minimum(Param(i)'high, IN_parameters(i)'high) downto 0 => IN_parameters(i)(minimum(Param(i)'high, IN_parameters(i)'high) downto 0), others => '0');
    Data(i)  <= (minimum(Data(i)'high, IN_Word'high) downto 0           => IN_Word(minimum(Data(i)'high, IN_Word'high) downto 0), others => '0');
    -- to make it REALLY multi input just change IN_word into IN_Word(i)
    -- to do that change the input port from DataWord into DataWords(parameters-1
    -- downto 0)

    MULTIPLIER : Mult                   --delay 3
      port map (
        CLK => CLK,
        A   => Data(i),
        B   => Param(i),
        P   => Result_big(i)
        );

    Result(i)   <= Result_big(i)(Result(i)'high downto 0);
    Overflow(i) <= '1' when Result_big(i)(Result_big(i)'high downto Result(i)'high+1) /= ZERO_Result else '0';
  end generate MULT_FOR;

end Behavioral;
