-- Dave Sankey, September 2019
-- Emulate MGT giving counting data

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity TB_MGT_emulator is
  	GENERIC(
		SoF_char: std_logic_vector(7 downto 0) := x"3C" -- 3C = TOB, 7C = Input Data
    );
	port(
		clk_mgt			: IN STD_LOGIC;
-- TTC FIFO signals
-- Format of FIFO is
-- fifo_dout(71 downto 56): N/A
-- fifo_dout(55 downto 48): number of 64 bit words for payload data
-- fifo_dout(47 downto 44): N/A
-- fifo_dout(43 downto 12): extended L1ID
-- fifo_dout(11 downto 0):  BCN
	    fifo_rd_en		: OUT STD_LOGIC;
    	fifo_dout		: IN STD_LOGIC_VECTOR(71 DOWNTO 0);
    	fifo_empty		: IN STD_LOGIC;
-- data from MGT
		data_from_mgt	: out std_logic_vector(31 downto 0);
		char_is_k		: out std_logic;
		error_from_mgt	: out std_logic
	);

end TB_MGT_emulator;

architecture initial of TB_MGT_emulator is

  TYPE STATE_TYPE IS (
    idle,
	read_fifo,
    wait_fifo,
    send_sof,
    send_l1id,
    send_payload,
    do_padding,
    send_trailer,
    send_eof
  );

  signal counter_sig: std_logic_vector(8 downto 0);
  signal state_sig: STATE_TYPE;

Begin

  error_from_mgt <= '0';

Payload:  process(clk_mgt)
  variable counter_int: unsigned(8 downto 0) := (others => '0');
  variable octet: std_logic_vector(7 downto 0);
  variable output_data, l1id_int: std_logic_vector(31 downto 0);
  variable char_is_k_int: std_logic;
  begin
    if rising_edge(clk_mgt) then
      char_is_k_int := '0';
      case state_sig is
        when send_sof =>
	 	  l1id_int := fifo_dout(43 downto 12);
	 	  output_data := x"000" & fifo_dout(11 downto 0) & SoF_char;
	      char_is_k_int := '1';
        when send_l1id =>
	 	  output_data := l1id_int;
        when send_payload =>
          octet := std_logic_vector(counter_int(7 downto 0));
		  output_data := octet & octet & octet & octet;
          counter_int := counter_int + 1;
        when do_padding =>
	 	  output_data := (Others => '0');
          counter_int := counter_int + 1;
        when send_trailer =>
          if SoF_char = x"3C" then
        	output_data := x"00000" & "00" & std_logic_vector(counter_int(6 downto 1)) & x"0";
          else
	 		output_data := (Others => '0');
	 	  end if;
          counter_int := counter_int + 1;
        when send_eof =>
          output_data := x"0000" & std_logic_vector(counter_int(7 downto 0)) & x"DC";
	      char_is_k_int := '1';
        when Others =>
          counter_int := (Others => '0');
          output_data := x"000000BC";
	      char_is_k_int := '1';
      end case;
      counter_sig <= std_logic_vector(counter_int)
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      data_from_mgt <= output_data
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      char_is_k <= char_is_k_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process Payload;

state_machine:  process(clk_mgt)
  variable read_fifo_int, empty_frame: std_logic := '0';
  variable next_state: STATE_TYPE := idle;
  variable end_counter_int: unsigned(8 downto 0) := (others => '0');
  begin
    if rising_edge(clk_mgt) then
	  read_fifo_int := '0';
      case state_sig is
        when read_fifo =>
		  read_fifo_int := '1';
		  next_state := wait_fifo;
        when wait_fifo =>
		  next_state := send_sof;
        when send_sof =>
          if unsigned(fifo_dout(55 downto 48)) = 0 then
            empty_frame := '1';
          else
            empty_frame := '0';
	 	    end_counter_int := unsigned(fifo_dout(55 downto 48) & '0') - 1;
	 	  end if;
		  next_state := send_l1id;
        when send_l1id =>
          if empty_frame = '1' then
		    next_state := do_padding;
          else
		    next_state := send_payload;
          end if;
        when send_payload =>
		  if unsigned(counter_sig) = end_counter_int then
            next_state := do_padding;
          else
		    next_state := send_payload;
	 	  end if;
        when do_padding =>
          next_state := send_trailer;
        when send_trailer =>
          next_state := send_eof;
        when send_eof =>
          next_state := idle;
        when others =>  -- idle
		  if fifo_empty = '0' then
	    	next_state := read_fifo;
	      else
	    	next_state := idle;
		  end if;
      end case;
      state_sig <= next_state
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
      fifo_rd_en <= read_fifo_int
-- pragma translate_off
      after 2 ns
-- pragma translate_on
      ;
    end if;
  end process state_machine;

End architecture initial;
