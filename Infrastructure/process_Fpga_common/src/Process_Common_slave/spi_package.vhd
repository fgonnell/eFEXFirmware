library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- 2015/07/14: spi types added

package spi is

-- The signals going from master to slaves
	type spi_mo is
    record
        clk: std_logic;
      	mosi: std_logic;
      	le: std_logic;
    end record;

-- The signals going from slaves to master	 
	type spi_mi is
    record
        miso: std_logic;
    end record;
	 
end spi;

