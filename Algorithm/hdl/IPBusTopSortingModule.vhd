--! @file
--! @brief Top of TOB sorting module with IPBus interface
--! @details 
--! This module is an IPBus-capable wrapper of the TopSorting Modules.
--! It contains 2 8-input sorting modules, one for e/g one for tau, 2 input and
--! 2 output spyRAMs, all ipbus controlled.
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
use work.ipbus_decode_efex_sorting.all;

use work.DataTypes.all;
use work.AlgoDataTypes.all;

library ipbus_lib;
use ipbus_lib.ipbus_reg_types.all;

use ipbus_lib.ipbus.all;

library infrastructure_lib;

--! @copydoc IPBusTopSortingModule.vhd
entity IPBusTopSortingModule is
  port (CLK : in std_logic;

        IN_eg_Data  : in AlgoOutput;
        IN_tau_Data : in AlgoOutput;
        IN_BCID     : in BCID_t;
        IN_Sync     : in std_logic;

        --IPBus connection
        ipb_clk : in  std_logic;
        ipb_rst : in  std_logic;
        ipb_in  : in  ipb_wbus;
        ipb_out : out ipb_rbus;


        OUT_BCID     : out BCID_t;
        OUT_eg_Sync  : out std_logic;
        OUT_eg_Valid : out std_logic;
        OUT_eg_TOB   : out AlgoTriggerObject;

        OUT_tau_Sync  : out std_logic;
        OUT_tau_Valid : out std_logic;
        OUT_tau_TOB   : out AlgoTriggerObject);

end IPBusTopSortingModule;

--! @copydoc IPBusTopSortingModule.vhd
architecture Behavioral of IPBusTopSortingModule is
  constant N_CTRL : positive := 4;  --number of control reg 
  constant N_STAT : positive := 4;  --number of status reg

  -- Algorithm signal

-- IPBus signals
  signal ipb_to_slaves   : ipb_wbus_array(N_SLAVES - 1 downto 0);
  signal ipb_from_slaves : ipb_rbus_array(N_SLAVES - 1 downto 0) := (others => IPB_RBUS_NULL);

  signal write_reg : ipb_reg_v(N_STAT - 1 downto 0) := (others => (others => '0'));
  signal read_reg  : ipb_reg_v(N_CTRL - 1 downto 0);

-- registers
  signal regControl_eg : AlgoRegister;
  signal regStatus_eg  : AlgoRegister;

  signal regControl_tau : AlgoRegister;
  signal regStatus_tau  : AlgoRegister;



  signal regDebug : AlgoRegisters(3 downto 0);  --to change

  signal AlgoStart    : std_logic;
  signal AlgoData_eg  : AlgoTriggerObjects(7 downto 0);
  signal AlgoData_tau : AlgoTriggerObjects(7 downto 0);

  signal SortStart_eg   : std_logic;
  signal SortWrite_eg   : std_logic;
  signal SortData_eg    : AlgoTriggerObject;
  signal SortedData_eg  : AlgoTriggerObject;
  signal SortingData_eg : AlgoTriggerObjects(7 downto 0);

  signal SortStart_tau   : std_logic;
  signal SortWrite_tau   : std_logic;
  signal SortData_tau    : AlgoTriggerObject;
  signal SortedData_tau  : AlgoTriggerObject;
  signal SortingData_tau : AlgoTriggerObjects(7 downto 0);

-- spy RAMs
  signal InputRAMOut_eg, InputRAMOut_tau         : AlgoOutput;
  signal FakeInputEnable_eg, FakeInputEnable_tau : std_logic := '0';
  signal SpyInputEnable_eg, SpyInputEnable_tau   : std_logic := '0';

  signal OutputRAMOut_eg, OutputRAMOut_tau         : AlgoTriggerObject;
  signal FakeOutputEnable_eg, FakeOutputEnable_tau : std_logic := '0';
  signal SpyOutputEnable_eg, SpyOutputEnable_tau   : std_logic := '0';
  signal SpyBCID_eg, SpyBCID_tau                   : std_logic := '0';
  signal SpyBCID_out_eg, SpyBCID_out_tau           : std_logic := '0';
  signal BCID_int                                  : BCID_t;

begin

  IPBUS_FABRIC : entity ipbus_lib.ipbus_fabric_sel
    generic map (
      NSLV      => N_SLAVES,                           --defined in ipbus_sel_address_table_SORTING
      SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map (
      sel => ipbus_sel_efex_sorting(ipb_in.ipb_addr),  --function defined in ipbus_sel_address_table_SORTING

      ipb_in          => ipb_in,
      ipb_out         => ipb_out,
      ipb_to_slaves   => ipb_to_slaves,
      ipb_from_slaves => ipb_from_slaves);

  IPBUS_SORTING_REGISTERS : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => N_CTRL,         --number of control reg 
      N_STAT => N_STAT)         --number of status reg
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipb_to_slaves(N_SLV_SORTING_REGISTERS),
      ipbus_out => ipb_from_slaves(N_SLV_SORTING_REGISTERS),
      d         => write_reg,
      q         => read_reg,
--    qmask     => (others => '0'),     
      stb       => open);


  -- a generic being tau/eg should be implemented to use different sorting
  -- priorities for the two algos

  -----------------------------------------------------------------------------
  -- Electromagnetic TOB sorting
  -----------------------------------------------------------------------------
  SortingData_eg <= to_AlgoTriggerObjects(IN_eg_Data);

--input MUX: real data/ data from input RAM
  AlgoData_eg <= to_AlgoTriggerObjects(InputRAMOut_eg) when FakeInputEnable_eg = '1' else SortingData_eg;


  eg_inputRAM : entity work.ipbus_sorting_inputRAM_wrapper
    port map (
      clk_ipb => ipb_clk,
      rst     => ipb_rst,
      ipb_in  => ipb_to_slaves(N_SLV_SORTING_INPUT_RAM_EG),
      ipb_out => ipb_from_slaves(N_SLV_SORTING_INPUT_RAM_EG),

      SortingIn  => IN_eg_Data,
      BCIDIn     => IN_BCID,
      SpyBCIDIN  => SpyBCID_eg,
      rclk       => CLK,
      Sync       => AlgoStart,
      we         => SpyInputEnable_eg,
      SortingOut => InputRAMOut_eg);

  TopSorting_eg : entity work.TopSortingModule
    generic map (
      STAGE => 3)
    port map (
      CLK        => CLK,
      IN_Control => regControl_eg,
      OUT_Status => regStatus_eg,
      IN_Start   => AlgoStart,
      IN_Data    => AlgoData_eg,
      OUT_Start  => SortStart_eg,
      OUT_Write  => SortWrite_eg,
      OUT_Data   => SortData_eg);

-- Conversion to external TOBs
  SortedData_eg <= to_AlgoTriggerObject(SortData_eg);

  eg_outputRAM : entity work.ipbus_sorting_outputRAM_wrapper
    port map (
      clk_ipb => ipb_clk,
      rst     => ipb_rst,
      ipb_in  => ipb_to_slaves(N_SLV_SORTING_OUTPUT_EG_RAM),
      ipb_out => ipb_from_slaves(N_SLV_SORTING_OUTPUT_EG_RAM),

      SortedIn  => SortedData_eg,
      BCIDIn    => BCID_int,
      SpyBCIDIN => SpyBCID_out_eg,
      rclk      => CLK,
      Sync      => AlgoStart,
      we        => SpyOutputEnable_eg,
      SortedOut => OutputRAMOut_eg);

--output MUX: real data/ data from output RAM  
  OUT_eg_TOB   <= OutputRAMOut_eg when FakeOutputEnable_eg = '1' else SortedData_eg;
  OUT_eg_Sync  <= SortStart_eg;
-- data valid should be done better...
  OUT_eg_Valid <= '1'             when FakeOutputEnable_eg = '1' else SortWrite_eg;

  -----------------------------------------------------------------------------
  -- Tau TOB sorting
  -----------------------------------------------------------------------------
  SortingData_tau <= to_AlgoTriggerObjects(IN_tau_Data);

--input MUX: real data/ data from input RAM
  AlgoData_tau <= to_AlgoTriggerObjects(InputRAMOut_tau) when FakeInputEnable_tau = '1' else SortingData_tau;


  tau_inputRAM : entity work.ipbus_sorting_inputRAM_wrapper
    port map (
      clk_ipb => ipb_clk,
      rst     => ipb_rst,
      ipb_in  => ipb_to_slaves(N_SLV_SORTING_INPUT_RAM_TAU),
      ipb_out => ipb_from_slaves(N_SLV_SORTING_INPUT_RAM_TAU),

      SortingIn  => IN_tau_Data,
      BCIDIn     => IN_BCID,
      SpyBCIDIN  => SpyBCID_tau,
      rclk       => CLK,
      Sync       => AlgoStart,
      we         => SpyInputEnable_tau,
      SortingOut => InputRAMOut_tau);

  TopSorting_tau : entity work.TopSortingModule
    generic map (
      STAGE => 3)
    port map (
      CLK        => CLK,
      IN_Control => regControl_tau,
      OUT_Status => regStatus_tau,
      IN_Start   => AlgoStart,
      IN_Data    => AlgoData_tau,
      OUT_Start  => SortStart_tau,
      OUT_Write  => SortWrite_tau,
      OUT_Data   => SortData_tau);

-- Conversion to external TOBs
  SortedData_tau <= to_AlgoTriggerObject(SortData_tau);

  tau_outputRAM : entity work.ipbus_sorting_outputRAM_wrapper
    port map (
      clk_ipb => ipb_clk,
      rst     => ipb_rst,
      ipb_in  => ipb_to_slaves(N_SLV_SORTING_OUTPUT_TAU_RAM),
      ipb_out => ipb_from_slaves(N_SLV_SORTING_OUTPUT_TAU_RAM),

      SortedIn  => SortedData_tau,
      BCIDIn    => BCID_int,
      SpyBCIDIN => SpyBCID_out_tau,
      rclk      => CLK,
      Sync      => AlgoStart,
      we        => SpyOutputEnable_tau,
      SortedOut => OutputRAMOut_tau);

  BCID_Delay : entity infrastructure_lib.GeneralDelay
    generic map (
      delay => 7,
      size  => 7)
    port map (
      clk      => CLK,
      data_in  => IN_BCID,
      data_out => BCID_int);

  OUT_BCID <= BCID_int;



--output MUX: real data/ data from output RAM  
  OUT_tau_TOB   <= OutputRAMOut_tau when FakeOutputEnable_tau = '1' else SortedData_tau;
  OUT_tau_Sync  <= SortStart_tau;
-- data valid should be done better...
  OUT_tau_Valid <= '1'              when FakeOutputEnable_tau = '1' else SortWrite_tau;




-- Sorting registers connection
  regControl_eg  <= read_reg(00);
  regControl_tau <= read_reg(01);
  regDebug(0)    <= read_reg(02);
  regDebug(1)    <= read_reg(03);

  write_reg(0) <= regStatus_eg;
  write_reg(1) <= regStatus_tau;
  write_reg(2) <= regDebug(2);
  write_reg(3) <= regDebug(3);

  AlgoStart <= IN_Sync;

-- Sorting register funcionality
  FakeInputEnable_eg  <= regControl_eg(0);
  SpyInputEnable_eg   <= regControl_eg(1);
  FakeOutputEnable_eg <= regControl_eg(2);
  SpyOutputEnable_eg  <= regControl_eg(3);
  SpyBCID_eg          <= regControl_eg(4);
  SpyBCID_out_eg      <= regControl_eg(5);

  FakeInputEnable_tau  <= regControl_tau(0);
  SpyInputEnable_tau   <= regControl_tau(1);
  FakeOutputEnable_tau <= regControl_tau(2);
  SpyOutputEnable_tau  <= regControl_tau(3);
  SpyBCID_tau          <= regControl_tau(4);
  SpyBCID_out_tau      <= regControl_tau(5);

end architecture;
