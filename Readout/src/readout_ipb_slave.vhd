--! @file
--! @brief IPBUS readout slave definitions of registers used in the Top Level Readout Block.
--! @details 
--! \verbatim
--! This is the IPBUS readout slave definitions of registers used in the Top Level Readout Block.
--! There are 3 section:
--!   1.  Top Level Registers.
--!   2.  RAW Calorimeter Readout Registers.
--!   3.  XTOB/TOB Readout Registers.
--! \endverbatim
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library ipbus_lib;
use ipbus_lib.ipbus.all;

library TOB_rdout_lib;
use TOB_rdout_lib.data_type_pkg.all;
use TOB_rdout_lib.TOB_rdout_ip_pkg.all;
use TOB_rdout_lib.ipbus_decode_efex_readout.all;	-- decoder package

--! @copydoc readout_ipb_slave.vhd
entity readout_ipb_slave is
    Port  ( 
        --! IPBus Reset input
        ipb_rst          : in std_logic ;
        --! IPBus Clock input
        ipb_clk          : in std_logic ;
        --! IPBus input bus going from master to slaves
        IPb_in           : in ipb_wbus;      -- The signals going from master to slaves
        --! IPBus output bus going from slaves to master
        IPb_out          : out ipb_rbus;     -- The signals going from slaves to master
        --!  8b Ext L1A ID from TTC input + 24b L1A ID of counter from TTC FIFO - inserted in the event header
        L1A_ID_Event	 : in STD_LOGIC_VECTOR (31 downto 0);        
        --! 8b Extended L1A ID + 24b L1A ID of L1A counter
        L1A_ID    		 : in STD_LOGIC_VECTOR (31 downto 0);        
        --! Bunch Crossing Number
        BCN_in           : in STD_LOGIC_VECTOR (11 downto 0); 
        --! Write Address Offset for TOB circular DPRAM       
        TOB_WR_ADDR_OFFSET                  : out STD_LOGIC_VECTOR (8 downto 0);        
        --! Write Address Offset for XTOB e/g circular DPRAM       
        XTOB_EG_WR_ADDR_OFFSET              : out STD_LOGIC_VECTOR (8 downto 0);        
        --! Write Address Offset for XTOB tau circular DPRAM       
        XTOB_TAU_WR_ADDR_OFFSET             : out STD_LOGIC_VECTOR (8 downto 0);        
        --! Number of cosecutive Slices to read from TOBs circular DPRAM       
        TOB_SLICES_TO_RD                    : out STD_LOGIC_VECTOR (2 downto 0);        
        --! Prog Full flag assert threshold for de-randomisation FIFO of TOBs Readout        
        TOB_FIFO_pFULL_THRESH_ASSERT        : out STD_LOGIC_VECTOR (8 downto 0);        
        --! Prog Full flag negate threshold for de-randomisation FIFO of TOBs Readout        
        TOB_FIFO_pFULL_THRESH_NEGATE        : out STD_LOGIC_VECTOR (8 downto 0);        
        --! Occupancy level of de-randomisation FIFO of TOBs Readout        
        TOB_FIFO_DATA_COUNT                 : in  STD_LOGIC_VECTOR (8 downto 0);        
        --! Prog Full flag assert threshold for de-randomisation FIFO of XTOBs e/g Readout        
        XTOB_EG_FIFO_pFULL_THRESH_ASSERT    : out STD_LOGIC_VECTOR (8 downto 0);        
        --! Prog Full flag negate threshold for de-randomisation FIFO of XTOBs e/g Readout        
        XTOB_EG_FIFO_pFULL_THRESH_NEGATE    : out STD_LOGIC_VECTOR (8 downto 0);        
        --! Occupancy level of de-randomisation FIFO of XTOBs e/g Readout        
        XTOB_EG_FIFO_DATA_COUNT             : in  STD_LOGIC_VECTOR (8 downto 0);        
        --! Prog Full flag assert threshold for de-randomisation FIFO of XTOBs tau Readout        
        XTOB_TAU_FIFO_pFULL_THRESH_ASSERT   : out STD_LOGIC_VECTOR (8 downto 0);        
        --! Prog Full flag negate threshold for de-randomisation FIFO of XTOBs tau Readout        
        XTOB_TAU_FIFO_pFULL_THRESH_NEGATE   : out STD_LOGIC_VECTOR (8 downto 0);
        --! Occupancy level of de-randomisation FIFO of XTOBs tau Readout        
        XTOB_TAU_FIFO_DATA_COUNT            : in  STD_LOGIC_VECTOR (8 downto 0);       
        --! Prog Full flag assert threshold for Link Output FIFO of TOBs Readout        
        TOB_Link_output_FIFO_pFULL_THRESH_ASSERT : out STD_LOGIC_VECTOR (12 downto 0);
        --! Prog Full flag negate threshold for Link Output FIFO of TOBs Readout        
        TOB_Link_output_FIFO_pFULL_THRESH_NEGATE : out STD_LOGIC_VECTOR (12 downto 0);
        --! Occupancy level of Link Output FIFO of TOBs Readout        
        TOB_LINK_OUTPUT_FIFO_RD_DATA_COUNT       : in  STD_LOGIC_VECTOR (12 downto 0);
        --! Full and Empty flags for all FIFOs in the TOBs Readout Logic       
        TOB_data_FIFO_flags                      : in  STD_LOGIC_VECTOR (11 downto 0);
        --! Write Address Offset for RAW circular DPRAM
        RAW_WR_ADDR_OFFSET           : out STD_LOGIC_VECTOR (8 downto 0);
        --! Prog Full flag assert threshold for de-randomisation FIFO of RAW Data Readout        
        RAW_FIFO_pFULL_THRESH_ASSERT : out STD_LOGIC_VECTOR (8 downto 0);
        --! Prog Full flag negate threshold for de-randomisation FIFO of RAW Data Readout        
        RAW_FIFO_pFULL_THRESH_NEGATE : out STD_LOGIC_VECTOR (8 downto 0);
        --! Prog Full flag assert threshold for TTC FIFO 
        BCN_FIFO_pFULL_THRESH_assert : out STD_LOGIC_VECTOR (8 downto 0);
        --! Prog Full flag negate threshold for TTC FIFO 
        BCN_FIFO_pFULL_THRESH_negate : out STD_LOGIC_VECTOR (8 downto 0);
       --! Occupancy level of Link Output FIFO of RAW Data Readout
        Link_output_FIFO_RAW_rd_data_count        : in  STD_LOGIC_VECTOR (12 downto 0);    -- occupancy of RAW output link MGT FIFO
        --! Prog Full flag assert threshold for Link Output FIFO of RAW Data Readout        
        Link_output_FIFO_RAW_pfull_thresh_assert  : out STD_LOGIC_VECTOR (12 downto 0);   -- Link_output_FIFO prog_full
        --! Prog Full flag negate threshold for Link Output FIFO of RAW Data Readout        
        Link_output_FIFO_RAW_pfull_thresh_negate  : out STD_LOGIC_VECTOR (12 downto 0);   -- Link_output_FIFO prog_full
        --! Full flag assert threshold for de-randomisation FIFO of RAW Data Readout        
		RAW_FIFO_FULL_THRESH_ASSERT   : out STD_LOGIC_VECTOR(8 downto 0);
        --! Full flag negate threshold for de-randomisation FIFO of RAW Data Readout        
		RAW_FIFO_FULL_THRESH_NEGATE   : out STD_LOGIC_VECTOR(8 downto 0);
       --! Occupancy level ofde-randomisation FIFO of RAW Data Readout
		RAW_FIFO_data_count           : in  STD_LOGIC_VECTOR(8 downto 0);
		--! Number of complete RAW Frames in Link Output FIFO to be transmitted to cFPGA
        RAW_frame_count        : in  STD_LOGIC_VECTOR (31 downto 0);
        --! Full and Empty flags for all FIFOs in the RAW Readout Logic
        RAW_data_FIFO_flags    : in STD_LOGIC_VECTOR (8 downto 0);
        --! Current write address of TOBs SPY Memory - used for IPBus access and to calculate occupancy 
		SPY_TOB_mem_wr_addr    : in STD_LOGIC_VECTOR (10 downto 0); 
		--! IPBus access bus signal going to TOB SPY DPRAM
		ipbus_out_tob_dpram    : out ipb_wbus;     -- signal going to TOB SPY DPRAM
		--! IPBus access bus signal coming from TOB SPY DPRAM
        ipbus_in_tob_dpram     : in  ipb_rbus;     -- signal coming from TOB SPY DPRAM
        --! Control Pulse Register for RAW Readout, all values are set to ZERO following a write to this register
        RDOUT_PULSE_REG        : out STD_LOGIC_VECTOR (31 downto 0);
        --! Current write address of RAW SPY Memory - used for IPBus access and to calculate occupancy 
		SPY_RAW_mem_wr_addr    : in STD_LOGIC_VECTOR (10 downto 0);
		--! IPBus access bus signal going to RAW SPY DPRAM 
		ipbus_out_raw_dpram    : out ipb_wbus;     -- signal going to RAW SPY DPRAM
		--! IPBus access bus signal coming from RAW SPY DPRAM
        ipbus_in_raw_dpram     : in  ipb_rbus;     -- signal coming from RAW SPY DPRAM
        --! Test Control Register at Top Level Readout Firmware
        TEST_CONTROL_REG	   : out  STD_LOGIC_VECTOR (31 downto 0);
       --! 54-b error flags from the Error Flag FIFO to IPBUS register
       link_error_flags        : in STD_LOGIC_VECTOR (53 downto 0) 
        
        );    
end readout_ipb_slave;

--! @copydoc readout_ipb_slave.vhd
architecture Behavioral of readout_ipb_slave is
    signal ipbw: ipb_wbus_array(N_SLAVES - 1 downto 0);
    signal ipbr, ipbr_d: ipb_rbus_array(N_SLAVES-1 downto 0);

    signal RDOUT_PULSE_REG_i  : STD_LOGIC_VECTOR (31 downto 0);
    signal rst_pulse_reg_i    : STD_LOGIC := '0';
    signal rst_pulse_reg_ii   : STD_LOGIC;
    signal IPbw_in_i          : ipb_wbus;     -- The signals going from master to slaves
    signal IPbr_out_i         : ipb_rbus;     -- The signals going from slaves to master
	signal TEST_CONTROL_REG_i : STD_LOGIC_VECTOR (31 downto 0);

-- ILA Signals    
    signal ipb_write_tmp  : STD_LOGIC_VECTOR (0 downto 0) ;
    signal ipb_strobe_tmp : STD_LOGIC_VECTOR (0 downto 0) ;
    signal ipb_ack_tmp    : STD_LOGIC_VECTOR (0 downto 0) ;
    signal ipb_err_tmp    : STD_LOGIC_VECTOR (0 downto 0) ; 
    signal ILA_pulse_vec  : STD_LOGIC_VECTOR (0 downto 0) ; 
	signal ipbus_out_raw_dpram_i : ipb_wbus;     -- signal going to RAW SPY DPRAM
    signal ipbus_in_raw_dpram_i  : ipb_rbus;     -- signal coming from RAW SPY DPRAM
	signal ipbus_out_tob_dpram_i : ipb_wbus;     -- signal going to TOB SPY DPRAM
    signal ipbus_in_tob_dpram_i  : ipb_rbus;     -- signal coming from TOB SPY DPRAM

begin

    -- outputs
    ipb_out             <= IPbr_out_i ;
    RDOUT_PULSE_REG     <= RDOUT_PULSE_REG_i ;
	TEST_CONTROL_REG    <= TEST_CONTROL_REG_i ;

    ipbus_out_raw_dpram  <= ipbus_out_raw_dpram_i ;     -- signal going to RAW SPY DPRAM
    ipbus_in_raw_dpram_i <= ipbus_in_raw_dpram    ;     -- signal coming from RAW SPY DPRAM

    ipbus_out_tob_dpram  <= ipbus_out_tob_dpram_i ;     -- signal going to TOB SPY DPRAM
    ipbus_in_tob_dpram_i <= ipbus_in_tob_dpram    ;     -- signal coming from TOB SPY DPRAM

--! @details
--! \verbatim
--! The IPBus bus fabric, which also has address select logic and data multiplexers.
--! This version selects the addressed slave depending on the state of incoming control lines.
--! \endverbatim
U1_rdout_fabric: entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV => N_SLAVES,
	    SEL_WIDTH => ipbus_sel_width)
    port map(
       ipb_in => ipb_in,
       ipb_out => IPbr_out_i,
       sel => ipbus_sel_efex_readout(ipb_in.ipb_addr),
       ipb_to_slaves => ipbw,
       ipb_from_slaves => ipbr
     ); 

--! @details
--! \verbatim
--! This is 32b register, which is read/written through the IPBus.
--! \endverbatim

U2_Test_Cntl_Reg : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 1,                 --number of control reg 
      N_STAT => 0)                 --number of status reg
    port map (
      clk       => ipb_clk,
      reset     => ipb_rst,
      ipbus_in  => ipbw(N_SLV_TEST_CONTROL_REG),
      ipbus_out => ipbr(N_SLV_TEST_CONTROL_REG),
      d         => (others=> (others=> '0')),
      q(0)      => (TEST_CONTROL_REG_i),
--    qmask     => (others => '0'),     
      stb       => open
      ); 
		 
	
	 

--! @details
--! \verbatim
--! This is 32b register, which is read/written through the IPBus.
--! This is a Pulse Register and automatically resets to ZERO after one clock cycle. 
--! \endverbatim

 U3_pulsed_register : entity ipbus_lib.ipbus_ctrlreg_v
    generic map (
      N_CTRL => 1,                 --number of control reg 
      N_STAT => 0)                 --number of status reg
    port map (
      clk       => ipb_clk,
      reset     => rst_pulse_reg_ii,    -- ipb_rst,
      ipbus_in  => ipbw(N_SLV_RDOUT_PULSE_REG),
      ipbus_out => ipbr(N_SLV_RDOUT_PULSE_REG),
      d         => (others=> (others=> '0')),
      q(0)      => (RDOUT_PULSE_REG_i),
--    qmask     => (others => '0'),     
      stb       => open
      );

    rst_pulse_reg_ii <= rst_pulse_reg_i OR ipb_rst;
    
U4_rst_pulse_reg : process (ipb_clk)    -- this generates the reset for pulsed register.
    begin
    if rising_edge(ipb_clk) then
        if RDOUT_PULSE_REG_i /= X"00000000"  then
            rst_pulse_reg_i <= '1';
        else
            rst_pulse_reg_i <= '0';
        end if;
    end if;
end process;

       
--! @details
--! \verbatim
--! TOB/XTOB data readout slave registers
--! \endverbatim
U4_TOB_slave: entity TOB_rdout_lib.slave_TOB_readout
  port map(
	 ipb_clk     => ipb_clk,  
	 ipb_rst     => ipb_rst,  
	 ipb_in     => ipbw(N_SLV_TOB),
	 ipb_out    => ipbr(N_SLV_TOB),
	 L1A_ID_Event => L1A_ID_Event,
	 L1A_ID     => L1A_ID,			
	 BCN_in     => BCN_in,
	 TOB_WR_ADDR_OFFSET   			  => TOB_WR_ADDR_OFFSET,			 
	 XTOB_EG_WR_ADDR_OFFSET   		  => XTOB_EG_WR_ADDR_OFFSET,			 
	 XTOB_TAU_WR_ADDR_OFFSET   		  => XTOB_TAU_WR_ADDR_OFFSET,			 
	 TOB_SLICES_TO_RD                 => TOB_SLICES_TO_RD,                
	 TOB_FIFO_pFULL_THRESH_ASSERT      => TOB_FIFO_pFULL_THRESH_ASSERT,     
	 TOB_FIFO_pFULL_THRESH_NEGATE      => TOB_FIFO_pFULL_THRESH_NEGATE,     
	 TOB_FIFO_DATA_COUNT              => TOB_FIFO_DATA_COUNT,       
	 XTOB_EG_FIFO_pFULL_THRESH_ASSERT  => XTOB_EG_FIFO_pFULL_THRESH_ASSERT, 
	 XTOB_EG_FIFO_pFULL_THRESH_NEGATE  => XTOB_EG_FIFO_pFULL_THRESH_NEGATE, 
	 XTOB_EG_FIFO_DATA_COUNT          => XTOB_EG_FIFO_DATA_COUNT,        
	 XTOB_TAU_FIFO_pFULL_THRESH_ASSERT => XTOB_TAU_FIFO_pFULL_THRESH_ASSERT,
	 XTOB_TAU_FIFO_pFULL_THRESH_NEGATE => XTOB_TAU_FIFO_pFULL_THRESH_NEGATE,
	 XTOB_TAU_FIFO_DATA_COUNT         => XTOB_TAU_FIFO_DATA_COUNT,
	 TOB_Link_output_FIFO_pFULL_THRESH_ASSERT => TOB_Link_output_FIFO_pFULL_THRESH_ASSERT,
	 TOB_Link_output_FIFO_pFULL_THRESH_NEGATE => TOB_Link_output_FIFO_pFULL_THRESH_NEGATE,
	 TOB_LINK_OUTPUT_FIFO_RD_DATA_COUNT      => TOB_LINK_OUTPUT_FIFO_RD_DATA_COUNT     ,
	 TOB_data_FIFO_flags        =>  TOB_data_FIFO_flags ,
	 SPY_TOB_mem_wr_addr        =>  SPY_TOB_mem_wr_addr       , -- TOB/XTOB data SPY memory wr addr pointer 
	 ipbus_out_tob_dpram    =>  ipbus_out_tob_dpram_i    ,    -- o/p signal going to TOB SPY DPRAM
     ipbus_in_tob_dpram     =>  ipbus_in_tob_dpram_i           -- i/p signal coming from TOB SPY DPRAM
	 ); 
		   
--! @details
--! \verbatim
--! Calorimeter RAW data readout slave registers
--! \endverbatim
U5_RAW_slave: entity TOB_rdout_lib.slave_RAW_readout
  port map(
	 ipb_clk  => ipb_clk,  
	 ipb_rst  => ipb_rst,  
	 ipb_in   => ipbw(N_SLV_RAW),
	 ipb_out  => ipbr(N_SLV_RAW),
	 RAW_WR_ADDR_OFFSET           =>  RAW_WR_ADDR_OFFSET,
	 RAW_FIFO_pFULL_THRESH_ASSERT =>  RAW_FIFO_pFULL_THRESH_ASSERT,    -- o/p
	 RAW_FIFO_pFULL_THRESH_NEGATE =>  RAW_FIFO_pFULL_THRESH_NEGATE,    -- o/p
	 
	 BCN_FIFO_pFULL_THRESH_assert =>    BCN_FIFO_pFULL_THRESH_assert ,     -- o/p
	 BCN_FIFO_pFULL_THRESH_negate =>    BCN_FIFO_pFULL_THRESH_negate ,     -- o/p
	 
	 Link_output_FIFO_pFULL_THRESH_ASSERT =>  Link_output_FIFO_RAW_pfull_thresh_assert   ,  -- o/p
	 Link_output_FIFO_pFULL_THRESH_NEGATE =>  Link_output_FIFO_RAW_pfull_thresh_negate   ,  -- o/p
	 Link_output_FIFO_rd_data_count       =>  Link_output_FIFO_RAW_rd_data_count        ,  -- i/p
	 RAW_frame_count                      => RAW_frame_count,
     RAW_FIFO_FULL_THRESH_ASSERT   => RAW_FIFO_FULL_THRESH_ASSERT,
	 RAW_FIFO_FULL_THRESH_NEGATE   => RAW_FIFO_FULL_THRESH_NEGATE,
	 RAW_FIFO_data_count           => RAW_FIFO_data_count,
	 RAW_data_FIFO_flags    =>  RAW_data_FIFO_flags,
	 SPY_RAW_mem_wr_addr    =>  SPY_RAW_mem_wr_addr   ,  -- RAW calo data SPY memory wr addr pointer 
	 ipbus_out_raw_dpram    =>  ipbus_out_raw_dpram_i    ,    -- o/p signal going to RAW SPY DPRAM
	 ipbus_in_raw_dpram     =>  ipbus_in_raw_dpram_i,           -- i/p signal coming from RAW SPY DPRAM
	 link_error_flags       =>  link_error_flags       -- 54-b error flags from the Error Flag FIFO to IPBUS register
	 
   ); 


---- ############ ILA  ###################
--    ipb_write_tmp(0)  <=  ipb_in.ipb_write;
--    ipb_strobe_tmp(0) <=  ipb_in.ipb_strobe;
--    ipb_ack_tmp(0)    <=  IPbr_out_i.ipb_ack;
--    ipb_err_tmp(0)    <=  IPbr_out_i.ipb_err;
--    ILA_pulse_vec(0)     <= RDOUT_PULSE_REG_i(0);

--U6_ila_rdout_ipb_slave : ila_ipbus_fabric_rd_wr
--PORT MAP (
--	clk => ipb_clk,
--	probe0 => ipb_in.ipb_addr,     -- 32b  
--	probe1 => ipb_in.ipb_wdata,    -- 32b  
--	probe2 => ipb_write_tmp,           -- 1b
--	probe3 => ipb_strobe_tmp,          -- 1b
--	probe4 => ILA_pulse_vec,       -- 1b
--	probe5 => IPbr_out_i.ipb_rdata,    --32b 
--	probe6 => ipb_ack_tmp ,            -- 1b
--	probe7 => ipb_err_tmp ,            -- 1b
--	probe8 => "0" ,             -- 1b
--    probe9 => "0"               -- 1b);
--    );

end Behavioral;
