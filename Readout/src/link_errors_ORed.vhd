--! @file
--! @brief OR all the input error flags together
--! @details 
--! \verbatim
--! OR all the input error flags together to create the 4-bit error signal to be added to the event 
--! Also check the 4-bit error flag to create 1-b flag to request data on error
--! \endverbatim
--! @author Saeed Taghavi



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

library TOB_rdout_lib;
use TOB_rdout_lib.data_type_pkg.all;
use TOB_rdout_lib.TOB_rdout_ip_pkg.all;

--! @copydoc link_errors_ORed.vhd
entity link_errors_ORed is
    Generic
    (  N : integer ) ;    --! number of input fibre channels
    Port (
       clk_in            : in STD_LOGIC;
       rst_in            : in STD_LOGIC;
       en_error_valid_in : in STD_LOGIC;
       err_flag_in       : in link_error_type;                 -- array of ERROR bits for each MGT input link
       err_flag_out      : out STD_LOGIC_VECTOR (3 downto 0);  -- 4-bit link error input into RAW readout  
       channel_error_map : out STD_LOGIC_VECTOR (48 downto 0);  -- 49-bit channel error map to input into RAW readout  
       req_err_rd_raw    : out std_logic                       -- flag requesting RAW data readout on error
       );
end link_errors_ORed;

--! @copydoc link_errors_ORed.vhd
architecture Behavioral of link_errors_ORed is
    
    signal   err_flag_tmp           : link_error_type ;
--    signal   n                      : integer := 0 ;
    
begin
      --     OR all error flags and then set ERR RDOUT bit
    err_flag_tmp(0) <= err_flag_in(0);
    gen: for i in 1 to N-1 generate       -- 48 channels
            err_flag_tmp(i) <= err_flag_tmp(i-1) OR err_flag_in(i); -- error flag for all links.
    end generate;
  
--! create flag to read calorimeter data on error
U0: process (clk_in)
    begin 
    if (clk_in'event and clk_in = '1') then
        if rst_in = '1' then 
            err_flag_out <= (others => '0') ;
            req_err_rd_raw <= '0' ;
        else 
            if en_error_valid_in = '1' then 
                err_flag_out <= '0' & err_flag_tmp(N-1)(2 downto 0);      -- set output only 3 bit as 4th is for internal use. 
                if err_flag_tmp(N-1) /= "0000" then     -- if error set, RAW data request
                    req_err_rd_raw <= '1' ;   -- set flag to read calorimeter data on error
                else
                    req_err_rd_raw <= '0' ;
                end if;
            end if;
        end if ;
    end if;
    end process;

--! create 49-bit channel error map
U1: process (clk_in)
    begin 
        if (clk_in'event and clk_in = '1') then
            if rst_in = '1' then 
                channel_error_map <= (others => '0') ;
            else 
                if en_error_valid_in = '1' then 
                    for i in 0 to 48 loop
                        channel_error_map(i) <=  err_flag_in(i)(3); -- every OR'ed error flag is assigned to 1 bit of the register.
                    end loop;
                end if;
            end if;
        end if;
    end process;

end Behavioral;
