--! @file
--! @brief 2-word adder
--! @details 
--! Add a description here
--! @author Francesco Gonnella

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
use work.DataTypes.all;

--! @copydoc Adder.vhd
entity Adder is
  port (
    CLK : in std_logic;

    IN_Carry  : in  std_logic_vector(1 downto 0);
    OUT_Carry : out std_logic;

    IN_Words : in  DataWords(1 downto 0);
    OUT_Word : out DataWord
    );
end Adder;

--! @copydoc Adder.vhd
architecture Behavioral of Adder is

  signal OutWord  : DataWord  := (others => '0');
  signal OutCarry : std_logic := '0';

begin

  process (CLK)
    variable Sum : std_logic_vector(OUT_Word'high+1 downto 0) := (others => '0');
  begin
    if rising_edge(CLK) then
      Sum      := std_logic_vector(unsigned('0'&IN_Words(0)) + unsigned('0'&IN_Words(1)));
      OutWord  <= Sum(OUT_Word'high downto 0);
      OutCarry <= Sum(OUT_Word'high+1) or IN_Carry(0) or IN_Carry(1);
    end if;
  end process;

  OUT_Word  <= OutWord;
  OUT_Carry <= OutCarry;

end Behavioral;


