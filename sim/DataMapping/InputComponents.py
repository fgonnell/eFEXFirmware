"""
Created on Mar 7 2020
@author: Alexios Stampekis
Module containing classes used to fill the Input
(MGTs and Data words (energies)) to the Simulation
"""
class BunchCrossing:
    '''
    A class that represents one bunch crossing
    input for 1 MGT; so 20 energies, forming 
    7 8-digit hexadecimal values
    It is initialized with all words (or energies
    if you prefer) equal to zero
    '''    
    def __init__(self, bc, crc, quality):
        self.bc = bc
        self.crc = crc
        self.quality = quality
        self.Energies = []
        for i in range(20):
            self.Energies.append(0)
        
    def set_energy(self, n, E):
        """
        here we set the nth of the 20 enegies of 1 mgt for 1bc
        to E (remember range 20 is from 0 to 19)
        """
        self.Energies[n]=E

    def set_energies(self, E):
        """
        In case we have a certain list of word values (Energies)
        to fill a BC with; MUST be a list of 20 elements!
        """
        self.Energies=E

    def get_hex_words(self):
        B=[]                 #list of binary energies
        for e in self.Energies:
            a=bin(e)         #conversion to binary string
            a1=a[2:]
            a2=a1.zfill(10)
            B.append(a2)
            
        b=bin(self.bc)
        b1=b[2:]
        b2=b1.zfill(7)
        q=bin(self.quality)
        q1=q[2:]
        q2=q1.zfill(8)
    
        c=bin(self.crc)
        c1=c[2:]
        c2=c1.zfill(9)
    
        C=[0,0,0,0,0,0,0]     #list of 7 32-bit words
        C[0]=b2[2:4]+B[1]+B[0]+b2[0:2]+q2
        C[1]=B[19][0:2]+B[4]+B[3]+B[2]
        C[2]=B[19][2:4]+B[7]+B[6]+B[5]
        C[3]=B[19][4:6]+B[10]+B[9]+B[8]
        C[4]=B[19][6:8]+B[13]+B[12]+B[11]
        C[5]=B[19][8:10]+B[16]+B[15]+B[14]
        C[6]=c2+b2[4:7]+B[18]+B[17]
   
        D=[]             #list of hex words
        for v in C:
            f=int(v, 2)      #convert bin to int
            f1=hex(f)        #convert to hex
            f2=f1[2:]        #discard 0x
            f3=f2.zfill(8)   #make sure values span 8 digits
            D.append(f3)     #append in list
          
        E=[]            # hex to print!
        for i, v in enumerate (D): 
            if i == 0:
                c=v+" 1"
            else:
                c=v+" 0"
            E.append(c)
            
        return E

class MGT:
    """
    class that concatenates a number of BCs into
    one MGT txt file
    """
    def __init__(self, ID, Nbc):
        """
        MGT initialization
        The BCs are initialized with crc=0, quality=0
        If you want to initialize them with some crc, q, 
        just increase parameters you pass to init...
        """
        self.ID = ID
        self.NUM_BC = Nbc
        self.BCs=[]
        for i in range(Nbc):
            self.BCs.append(BunchCrossing(i, 0, 0))

    def set_bc_word(self, nbc, n_word, Energy):
        self.BCs[nbc].set_energy(n_word, Energy)

    def set_bc_words(self, nbc, Energies):
        self.BCs[nbc].set_energies(Energies)

    def set_bc_crc_q(self, nbc, crc, quality):
        """
        in case you want a defined crc and q for a bc
        """
        self.BCs[nbc].crc=crc
        self.BCs[nbc].q=quality

    def export_file(self):
        f1 = open('MGT'+str(self.ID)+'.txt', 'w')
        for v in self.BCs:
            for val in v.get_hex_words():
                f1.write(val+'\n')
        f1.close()

        
