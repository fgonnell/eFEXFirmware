----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/272016 18:53:27 PM
-- Design Name: 
-- Module Name: tac_sm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.all;
LIBRARY xil_defaultlib;

---First stage timing auto calibration  state machine that will generate delay count

entity tac_sm is
  Port ( 
       clk_280M     : IN     std_logic;
       reset        : IN     std_logic;
       TTC_CLK_edge : IN     std_logic;
       MGT_Commadet : IN     std_logic;
       Start        : IN     std_logic;
       rx_resetdone : IN     std_logic;
       Reg_enable   : OUT    std_logic;
       Mux_Value    :OUT      std_logic_vector (3 DOWNTO 0)
       
       );
end tac_sm;

ARCHITECTURE fsm OF tac_sm IS

-- Architecture Declarations
   signal delay_count:unsigned (3 downto 0);

--   SUBTYPE STATE_TYPE IS 
--      std_logic_vector(3 DOWNTO 0);
 
--   -- Hard encoding
--   CONSTANT idle : STATE_TYPE := "0001";
--   CONSTANT wt_rstdone : STATE_TYPE := "0010";
--   CONSTANT wt_comma : STATE_TYPE := "0100";
--   CONSTANT wt_ttc_redge : STATE_TYPE := "1000";

    TYPE STATE_TYPE IS (
       idle, wt_rstdone, wt_comma, wt_ttc_redge
       );
   -- Declare current and next state signals
   SIGNAL current_state : STATE_TYPE;


BEGIN

   -----------------------------------------------------------------
   clocked_proc : PROCESS ( 
      clk_280M
   )
   -----------------------------------------------------------------
   BEGIN
    IF (clk_280M'EVENT AND clk_280M = '1') THEN
      IF (reset = '1') THEN
         current_state <= idle;
         -- Default Reset Values
         mux_value <= (others => '0');
         reg_enable <= '0';
         delay_count <= (others => '0');
      ELSE
         -- Default Assignment To Internals and Outputs
         delay_count <= (others => '0');
         mux_value <= (others => '0');
         reg_enable <= '0';

         -- Combined Actions
         CASE current_state IS
            WHEN idle => 
               IF (start ='1') THEN 
                  current_state <= wt_rstdone;
               ELSE
                  current_state <= idle;
               END IF;
            WHEN wt_rstdone => 
               IF (rx_resetdone ='1') THEN 
                  current_state <= wt_comma;
               ELSE
                  current_state <= wt_rstdone;
               END IF;
            WHEN wt_comma => 
               IF (TTC_CLK_edge = '1' and MGT_Commadet ='1') THEN 
                  delay_count <= (others=>'0');
                  reg_enable   <=  '1';
                  current_state <= idle;
               ELSIF (MGT_Commadet ='1') THEN 
                  delay_count <= delay_count+1;
                  current_state <= wt_ttc_redge;
               ELSE
                  current_state <= wt_comma;
               END IF;
            WHEN wt_ttc_redge => 
               delay_count <= delay_count+1;
               IF (TTC_CLK_edge ='1') THEN 
                  delay_count <= (others =>'0');
                  mux_value   <= std_logic_vector(delay_count);
                  reg_enable   <=  '1';
                  current_state <= idle;
               ELSE
                  current_state <= wt_ttc_redge;
               END IF;
            WHEN OTHERS =>
               current_state <= idle;
         END CASE;
      END IF;
      END IF;
   END PROCESS clocked_proc;
 
   
END fsm;
