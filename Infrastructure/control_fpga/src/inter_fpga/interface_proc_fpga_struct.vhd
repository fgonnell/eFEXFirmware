-- VHDL Entity ipbus_v2_lib.interface_proc_fpga.symbol
--
-- Created:
--          by - mjs59.UNKNOWN (te7gromit)
--          at - 16:36:28 01/11/16
--

--
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
library ipbus_lib;


ENTITY interface_proc_fpga IS
	Generic (
		constant IPBUFWIDTH: positive := 5
	);
   PORT(
      mac_clk            : IN     std_logic;
      mac_rx_data        : IN     std_logic_vector (7 DOWNTO 0);
      mac_rx_error       : IN     std_logic;
      mac_rx_last        : IN     std_logic;
      mac_rx_valid       : IN     std_logic;
      mac_tx_ready       : IN     STD_LOGIC;
      master_tx_data     : IN     std_logic_vector (8 DOWNTO 0);
      master_tx_err      : IN     std_logic;
      rarp_rx_data       : IN     std_logic_vector (7 DOWNTO 0);
      rarp_rx_last       : IN     std_logic;
      rarp_rx_valid      : IN     std_logic;
      rst_macclk         : IN     std_logic;
      mac_tx_data        : OUT    STD_LOGIC_VECTOR (7 DOWNTO 0);
      mac_tx_error       : OUT    STD_LOGIC;
      mac_tx_last        : OUT    STD_LOGIC;
      mac_tx_valid       : OUT    STD_LOGIC;
      master_rx_data     : OUT    std_logic_vector (8 DOWNTO 0);
      master_tx_pause    : OUT    STD_LOGIC;
      master_link_down   : IN     std_logic;
      slaves_Got_IP_addr : OUT    std_logic
   );

-- Declarations

END interface_proc_fpga ;


ARCHITECTURE struct OF interface_proc_fpga IS

   -- Architecture declarations

   -- Internal signal declarations
   SIGNAL FIFO_Full    : STD_LOGIC;
   SIGNAL FIFO_WriteEn : std_logic;
   SIGNAL FIFO_data    : std_logic_vector(9 DOWNTO 0);





BEGIN

   -- Instance port mappings.
   U_2 : entity ipbus_lib.UDP_master_fifo
      GENERIC MAP (
         BUFWIDTH => IPBUFWIDTH
      )
      PORT MAP (
         mac_clk         => mac_clk,
         rst_macclk      => rst_macclk,
         FIFO_WriteEn    => FIFO_WriteEn,
         FIFO_Data       => FIFO_data,
         FIFO_Full       => FIFO_Full,
         master_tx_pause => master_tx_pause,
         mac_tx_ready    => mac_tx_ready,
         mac_tx_data     => mac_tx_data,
         mac_tx_error    => mac_tx_error,
         mac_tx_last     => mac_tx_last,
         mac_tx_valid    => mac_tx_valid
      );
   U_0 : entity ipbus_lib.UDP_master_if
      PORT MAP (
         mac_clk        => mac_clk,
         rst_macclk     => rst_macclk,
         mac_rx_data    => mac_rx_data,
         mac_rx_error   => mac_rx_error,
         mac_rx_last    => mac_rx_last,
         mac_rx_valid   => mac_rx_valid,
         rarp_rx_data   => rarp_rx_data,
         rarp_rx_last   => rarp_rx_last,
         rarp_rx_valid  => rarp_rx_valid,
         Got_IP_addr    => slaves_Got_IP_addr,
         FIFO_Full      => FIFO_Full,
         FIFO_data      => FIFO_data,
         FIFO_WriteEn   => FIFO_WriteEn,
         master_rx_data => master_rx_data,
         master_tx_data => master_tx_data,
         master_tx_err  => master_tx_err,
         master_link_down  => master_link_down
      );

END struct;
