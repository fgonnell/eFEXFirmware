#!/bin/bash
OLD_DIR=`pwd`
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "${DIR}"
./Hog/Tcl/copy-xml.tcl $@
cd "${OLD_DIR}"
