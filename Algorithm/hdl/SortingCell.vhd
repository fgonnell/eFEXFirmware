--! @file
--! @brief Serial sorter Cell
--! @details 
--! This is the unit of the serial sorter
--! @author Francesco Gonnella

library ieee;
use ieee.std_logic_1164.all;

library work;
use work.DataTypes.all;
use work.AlgoDataTypes.all;

-------------------------------------------------------------------------------

--!@copydoc SortingCell.vhd
entity SortingCell is

--  generic (
--    );

  port (
    clk         : in  std_logic;
    IN_Clear    : in  std_logic;
    IN_Previous : in  AlgoTriggerObject;
    OUT_Next    : out AlgoTriggerObject;
    IN_Parallel : in  AlgoTriggerObject;
    IN_Inhibit  : in  std_logic;
    OUT_Inhibit : out std_logic
    );

end entity SortingCell;

-------------------------------------------------------------------------------

--!@copydoc SortingCell.vhd
architecture str of SortingCell is

  -----------------------------------------------------------------------------
  -- Internal signal declarations
  -----------------------------------------------------------------------------
  signal StoredWord : AlgoTriggerObject := ZERO_ALGO_TRIGGER_OBJECT;
  signal Inhibit    : std_logic     := '0';
  signal Selector   : std_logic_vector(2 downto 0);

--  ####### Attributes  ########
    attribute keep       : string ;
    attribute max_fanout : integer;
    
    attribute keep of       StoredWord : signal is "true" ; 
    attribute max_fanout of StoredWord : signal is 30;
-- ###########################
      
begin  -- architecture str

  Inhibit     <= '0' when (TOBEnergy(IN_Parallel) < TOBEnergy(StoredWord)) else '1';
  OUT_Inhibit <= Inhibit or IN_Clear;
  OUT_Next    <= StoredWord;

  Selector <= (2 => IN_Clear, 1 => IN_Inhibit, 0 => Inhibit);

  process (clk)
  begin
    if rising_edge(clk) then
      case Selector is
        when "000" =>
          StoredWord <= StoredWord;
        when "001" =>
          StoredWord <= IN_Parallel;
        when "010" =>
          StoredWord <= IN_Previous;
        when "011" =>
          StoredWord <= IN_Previous;
        when "100" =>
          StoredWord <= IN_Parallel;
        when "101" =>
          StoredWord <= IN_Parallel;
        when "110" =>
          StoredWord <= ZERO_ALGO_TRIGGER_OBJECT;
        when "111" =>
          StoredWord <= ZERO_ALGO_TRIGGER_OBJECT;
        when others => null;


      end case;
      
    end if;
  end process;


end architecture str;

-------------------------------------------------------------------------------
