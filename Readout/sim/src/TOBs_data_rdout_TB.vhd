--! @file
--! @brief TOB/XTOB Readout Test Bench for process FPGA
--! @details 
--! This module received Sorted TOBs & XTOBs data and produces events of 32b words for transmission to control FPGA
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library TOB_rdout_sim;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;
use TOB_rdout_lib.data_type_pkg.all;
use TOB_rdout_lib.ipbus_decode_efex_readout.all;	-- decoder package

library ipbus_lib;
use ipbus_lib.ipbus.all;

--! @copydoc TOBs_data_rdout_TB.vhd
entity TOBs_data_rdout_TB is
--  Port ( );
end TOBs_data_rdout_TB;



architecture Behavioral of TOBs_data_rdout_TB is


    signal  hw_addr                 : std_logic_vector(1 downto 0) ; 
    signal  tied_to_ground_i        : std_logic;
    signal  tied_to_vcc_i           : std_logic;
    signal  tied_high_i             : std_logic_vector(0 downto 0);
    signal  locked_i                : std_logic;

--    signal  TX_DATA_OUT_i           : std_logic_vector(223 downto 0);
    
    signal  L1A_in_i                : std_logic ;
    signal  BCN_in_i                : STD_LOGIC_VECTOR (11 downto 0) := (others => '0' );
    signal  BCN_in_reg              : STD_LOGIC_VECTOR (11 downto 0)  := (others => '0' );
    signal  L1A_ID_i                : STD_LOGIC_VECTOR (23 downto 0)  := (others => '0' );
--    signal  TXCTRL_OUT_i            : std_logic_vector(3 downto 0)  := (others => '0' );
    
    signal link_error_flags_i      : link_error_type;      -- array 49 x 4 bit per link

    signal  pre_ld_wr_addr_i        : STD_LOGIC_VECTOR (8 downto 0)  := (others => '0' );

    signal sys_rst                  : std_logic := '0';
    signal clk_40M                  : std_logic := '0';
    constant clk_40_period          : time := 25 ns;
    signal clk_40M_ld               : std_logic := '0';
    signal clk_160M                 : std_logic := '0';
    signal clk_200M                 : std_logic := '0';
    constant clk_200_period         : time := 5 ns;
    signal clk_280M                 : std_logic := '0';
    
    signal ctrl_ready_i             : std_logic := '0';
    
    signal TOB_eg_512b_in_i        :  STD_LOGIC_VECTOR (511 downto 0);   -- XTOBs e/g 64b * 8
    signal TOB_tau_512b_in_i       :  STD_LOGIC_VECTOR (511 downto 0);   -- XTOBs tau 64b * 8

    signal TX_TOB_eg_DATA_OUT_i      : std_logic_vector(31 downto 0) ;       -- TOB e/g
    signal TXCTRL_TOB_eg_OUT_i       : std_logic_vector(3 downto 0) ;        -- TOB e/g
    signal TOB_eg_Valid_flg_in_i     : std_logic_vector(7 downto 0) ;        -- TOB e/g
    signal TX_TOB_tau_DATA_OUT_i     : std_logic_vector(31 downto 0) ;      -- TOB tau
    signal TXCTRL_TOB_tau_OUT_i      : std_logic_vector(3 downto 0) ;       -- TOB tau
    signal TOB_tau_Valid_flg_in_i    : std_logic_vector(7 downto 0) ;       -- TOB tau
    signal link_err_flg_in_i         : std_logic_vector(3 downto 0) ;
    signal rand_out_40M              : std_logic_vector(15 downto 0) ;
    signal rand_out_280M             : std_logic_vector(15 downto 0) ;

    signal TX_TOB_topo_DATA_OUT_i         : std_logic_vector(31 downto 0) ;      -- TOB tau
    signal TXCTRL_TOB_topo_OUT_i          : std_logic_vector(3 downto 0) ;       -- TOB tau
    
--    signal T_TOB_eg_32b_in_i       :  STD_LOGIC_VECTOR (31 downto 0);
--    signal T_TOB_eg_strt_in_i      :  STD_LOGIC;                     
--    signal T_TOB_eg_wr_in_i        :  STD_LOGIC;                     
--    signal T_TOB_tau_32b_in_i      :  STD_LOGIC_VECTOR (31 downto 0);
--    signal T_TOB_tau_strt_in_i     :  STD_LOGIC;                     
--    signal T_TOB_tau_wr_in_i       :  STD_LOGIC;                     
      
    signal  DPR_locations_to_rd_i   : STD_LOGIC_VECTOR (2 downto 0);

    signal  ipb_rst_i       : STD_LOGIC := '0'; 
--    signal  ipb_clk_i       : STD_LOGIC := '0'; 
    signal  IPb_in_i        : ipb_wbus;      -- The signals going from master to slaves
    signal  IPb_outI        : ipb_rbus;     -- The signals going from slaves to master

    signal  reg1                : std_logic := '0'; 
    signal  reg2                : std_logic := '0';
    signal  L1A_sw_pulse_i      : std_logic := '0';
	signal  RDOUT_TOB_PULSE_REG_i       : STD_LOGIC_VECTOR (31 downto 0);

    signal  SPY_mem_wr_addr_i       : STD_LOGIC_VECTOR (10 downto 0);       -- TOB data SPY memory wr addr pointer 
    signal  SPY_mem_rd_addr_en_i    : STD_LOGIC := '0';                                        -- TOB data SPY memory rd addr en      
    signal  SPY_mem_rd_addr_i       : STD_LOGIC_VECTOR (10 downto 0) := (others => '0');       -- TOB data SPY memory rd addr         
    signal  SPY_mem_rd_data_i       : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');       -- TOB data SPY memory rd data 

    signal  XTOB_eg_FIFO_full_thresh_assert_i	: STD_LOGIC_VECTOR (8 downto 0) := "111111110";
    signal  XTOB_eg_FIFO_full_thresh_negate_i   : STD_LOGIC_VECTOR (8 downto 0) := "111011110";

    signal  XTOB_tau_FIFO_full_thresh_assert_i	: STD_LOGIC_VECTOR (8 downto 0) := "111111110";
    signal  XTOB_tau_FIFO_full_thresh_negate_i  : STD_LOGIC_VECTOR (8 downto 0) := "111011110";

    signal  T_TOBs_FIFO_full_thresh_assert_i	: STD_LOGIC_VECTOR (8 downto 0) := "111111110";
    signal  T_TOBs_FIFO_full_thresh_negate_i	: STD_LOGIC_VECTOR (8 downto 0) := "111011110";

    signal  Link_output_FIFO_full_thresh_assert_i   : STD_LOGIC_VECTOR (12 downto 0) := "1111111101111";
    signal  Link_output_FIFO_full_thresh_negate_i   : STD_LOGIC_VECTOR (12 downto 0) := "1111110111111";
    signal  Link_output_FIFO_rd_data_count_i        : STD_LOGIC_VECTOR (12 downto 0) := (others => '0');   -- occupancy of RAW output link MGT FIFO
    signal  Link_output_FIFO_wr_data_count_i        : STD_LOGIC_VECTOR (12 downto 0) := (others => '0');   -- occupancy of RAW output link MGT FIFO
	
	signal  T_TOB_FIFO_data_count_i		: STD_LOGIC_VECTOR (8 downto 0);
	signal  XTOB_eg_FIFO_data_count_i	: STD_LOGIC_VECTOR (8 downto 0);
	signal  XTOB_TAU_FIFO_DATA_COUNT_i	: STD_LOGIC_VECTOR (8 downto 0);
	signal  TOB_data_FIFO_flags_i       : STD_LOGIC_VECTOR (11 downto 0);

    
begin

    tied_to_ground_i        <=   '0';
    tied_to_vcc_i           <=   '1';
    tied_high_i             <= (others => '1') ;

    hw_addr <= "00"  ;  -- set to F1 in test

    clk_40M_proc : process
      begin
         clk_40M <= '1';
         wait for clk_40_period/2;  --for 25 ns signal is '0'.
         clk_40M <= '0';
         wait for clk_40_period/2;  --for next 25 ns signal is '1'.
    end process;

    clk_40M_ld_proc : process       -- 40MHz 20% duty cycle
      begin
         clk_40M_ld <= '1';
         wait for clk_200_period;  
         clk_40M_ld <= '0';
         wait for (clk_200_period * 4);  
    end process;


--U0_clk_wiz_tst : clk_wiz_tst
--    port map (
--        clk_160M     => clk_160M , 
--        clk_200M     => clk_200M , 
--        clk_280M     => clk_280M ,
--        reset        => '0' , 
--        locked       => locked_i , 
--        clk_in1      => clk_40M  
--    );

    sys_rst <= NOT locked_i ;       -- system reset

U0_TOB_eg_data_gen : entity TOB_rdout_sim.gen_32b_TOBs_tst  -- get test data TOB e/g
    generic map
    (
        WORDS_IN_BRAM =>   721 ,
        FILE_NAME     => "TOBs_rom_init_tx_tst.dat" 
    )   
    port map
    (
        -- User Interface
        TX_DATA_OUT     => TX_TOB_eg_DATA_OUT_i ,
        TXCTRL_OUT      => TXCTRL_TOB_eg_OUT_i  ,
        -- System Interface
        USER_CLK        => clk_280M ,      
        SYSTEM_RESET    => sys_rst  
    );
    
U1_TOB_tau_data_gen : entity TOB_rdout_sim.gen_32b_TOBs_tst  -- get test data TOB e/g
    generic map
    (
        WORDS_IN_BRAM =>   721 ,
        FILE_NAME     => "TOBs_rom_init_tx_tst.dat" 
    )   
    port map
    (
        -- User Interface
        TX_DATA_OUT     => TX_TOB_tau_DATA_OUT_i ,
        TXCTRL_OUT      => TXCTRL_TOB_tau_OUT_i  ,
        -- System Interface
        USER_CLK        => clk_280M ,      
        SYSTEM_RESET    => sys_rst  
    );
    
U1_TOB_topo_data_gen : entity TOB_rdout_sim.gen_32b_TOBs_tst  -- get test data TOPO TOBs
    generic map
    (
        WORDS_IN_BRAM =>   526 ,
        FILE_NAME     => "TOBs_topo_rom_init_tx_tst.dat"  
    )   
    port map
    (
        -- User Interface
        TX_DATA_OUT     => TX_TOB_topo_DATA_OUT_i ,
        TXCTRL_OUT      => TXCTRL_TOB_topo_OUT_i  ,
        -- System Interface
        USER_CLK        => clk_280M ,      
        SYSTEM_RESET    => sys_rst  
    );
                
rand_num_gen_40M : entity TOB_rdout_sim.rand_num_gen_16b
  Port map (
      rst         =>  sys_rst ,
      clk         =>  clk_40M , 
      MAC_addr    =>  X"abcd0001" ,
      rand_out    =>  rand_out_40M
      );
    
rand_num_gen_280M : entity TOB_rdout_sim.rand_num_gen_16b
        Port map (
            rst         =>  sys_rst  ,
            clk         =>  clk_280M ,
            MAC_addr    =>  X"abcd0002" ,
            rand_out    =>  rand_out_280M
            );
          

U3 : entity TOB_rdout_sim.L1A_gen_tst 
    Port map
    (   
        clk_in   => clk_40M ,
        rst      => sys_rst ,
        L1A_out  => L1A_in_i
        );

    link_error_flags_i <= (others => (others => '0'));

U4_BCN_gen : entity TOB_rdout_lib.cntr_ld_12b
    generic map (
        T_count       => X"EAC"   ,  -- bunch count total
        ld_count      => X"080"
        )

    Port map ( 
       CE         => '1' ,
       CLK        => clk_40M ,
       RST        => sys_rst ,
       LD         => sys_rst ,
       Q          => BCN_in_i
       );
 
  --! L1A counter 
U8_L1A_gen : entity TOB_rdout_lib.cntr_L1A_generic
     Port map ( 
--          CE   =>  L1A_in_i ,   -- L1A pulse from the test generator
        CE   =>  L1A_sw_pulse_i       ,    -- L1A_sw_pulse_i under s/w control
        CLK  =>  clk_40M ,
        RST  =>  sys_rst,
        Q    =>  L1A_ID_i
        ); 
       
U6_clk_proc : process (clk_280M)
    begin
        if clk_280M'event AND clk_280M = '1' then
            if  ( TXCTRL_TOB_eg_OUT_i(0) = '1' ) then 
                BCN_in_reg <= BCN_in_i ;
            end if;
        end if;
    end process;
    
    TOB_eg_Valid_flg_in_i <= '0' & rand_out_280M(6 downto 0) ;    -- 8 bit assignment for TOB e/g valid

    TOB_tau_Valid_flg_in_i <= '0' & rand_out_280M(15 downto 9) ;    -- 8 bit assignment for TOB tau valid

    link_err_flg_in_i  <= rand_out_280M(11 downto 8) ;    -- 4 bit assignment for link error flag
    
    ctrl_ready_i <= '1' ;         -- for test set to 1


       TOB_eg_512b_in_i     <= TX_TOB_eg_DATA_OUT_i & TX_TOB_eg_DATA_OUT_i & TX_TOB_eg_DATA_OUT_i & TX_TOB_eg_DATA_OUT_i 
                             & TX_TOB_eg_DATA_OUT_i & TX_TOB_eg_DATA_OUT_i & TX_TOB_eg_DATA_OUT_i & TX_TOB_eg_DATA_OUT_i 
                             & TX_TOB_eg_DATA_OUT_i & TX_TOB_eg_DATA_OUT_i & TX_TOB_eg_DATA_OUT_i & TX_TOB_eg_DATA_OUT_i  
                             & TX_TOB_eg_DATA_OUT_i & TX_TOB_eg_DATA_OUT_i & TX_TOB_eg_DATA_OUT_i & TX_TOB_eg_DATA_OUT_i ;
                             

       TOB_tau_512b_in_i    <= TX_TOB_tau_DATA_OUT_i & TX_TOB_tau_DATA_OUT_i &TX_TOB_tau_DATA_OUT_i &TX_TOB_tau_DATA_OUT_i                
                             & TX_TOB_tau_DATA_OUT_i & TX_TOB_tau_DATA_OUT_i &TX_TOB_tau_DATA_OUT_i &TX_TOB_tau_DATA_OUT_i 
                             & TX_TOB_tau_DATA_OUT_i &TX_TOB_tau_DATA_OUT_i &TX_TOB_tau_DATA_OUT_i &TX_TOB_tau_DATA_OUT_i  
                             & TX_TOB_tau_DATA_OUT_i &TX_TOB_tau_DATA_OUT_i &TX_TOB_tau_DATA_OUT_i &TX_TOB_tau_DATA_OUT_i ;
                                              

U7_TOBs_readout : entity TOB_rdout_lib.TOBs_rdout        -- data arranged from lower numbers to higher as LSB to MSB
--    Generic map 
--        ( TOPO_TOB_mode => '1' )     -- 1 = TOPO TOB read out is enabled
    Port map ( 
       RST                  => sys_rst ,
       hw_addr              => hw_addr ,
       RST_spy_mem_wr_addr  => '0' ,
       TOB_FIFO_sw_rst      => '0' ,
       -- XTOBs e/g 64b * 8 as input data is 32, we have 32b * 16    
       TOB_eg_512b_in       => TOB_eg_512b_in_i,
       TOB_eg_Valid_flg_in  => TOB_eg_Valid_flg_in_i ,
       TOB_eg_sync_in       => TXCTRL_TOB_eg_OUT_i (0) ,
       -- XTOBs tau 64b * 8 
       TOB_tau_512b_in       => TOB_tau_512b_in_i,
       TOB_tau_Valid_flg_in  => TOB_tau_Valid_flg_in_i ,
       TOB_tau_sync_in      => TXCTRL_TOB_tau_OUT_i (0),          -- TOB tau  sync signal      
       -- TOPO TOBs read out
       T_TOB_32b_in          => TX_TOB_topo_DATA_OUT_i  ,       -- TOPO TOBs e/g 32b * 7 is series
       T_TOB_sync_in         => TXCTRL_TOB_topo_OUT_i(0)  ,     -- TOPO TOB e/g start signal
       T_TOB_valid_flg_in    => rand_out_280M(0)    ,           -- TOPO TOB e/g write signal

       link_err_4b_in       => link_err_flg_in_i ,   
       clk_280M_in          => clk_280M ,   
       tob_txoutclk         => clk_160M ,   
       L1A_in               => L1A_sw_pulse_i,      -- TXCTRL_TOB_eg_OUT_i (3) , -- bit 4 of test data file                  
       BCN_ID_in            => BCN_in_reg ,
       L1A_ID_in            => L1A_ID_i ,
       ctrl_ready_in        => ctrl_ready_i ,
       rdout_expected       =>  '0' ,    -- error readout expected                                                                     
       TOB_out_to_MGT_is_char => open ,
       TOB_out_to_MGT         => open ,
       pre_ld_wr_adrr         => pre_ld_wr_addr_i ,
       DPR_locations_to_rd    => DPR_locations_to_rd_i ,
       
       TOB_data_FIFO_flags    => TOB_data_FIFO_flags_i ,     
       XTOB_eg_FIFO_data_count          =>   XTOB_eg_FIFO_data_count_i,
       XTOB_eg_FIFO_full_thresh_assert  =>   XTOB_eg_FIFO_full_thresh_assert_i, --"111111110",
       XTOB_eg_FIFO_full_thresh_negate  =>   XTOB_eg_FIFO_full_thresh_negate_i, --"111011110",
       
       XTOB_tau_FIFO_data_count         => XTOB_tau_FIFO_data_count_i,     
       XTOB_tau_FIFO_full_thresh_assert =>  XTOB_tau_FIFO_full_thresh_assert_i, -- "111111110",
       XTOB_tau_FIFO_full_thresh_negate =>  XTOB_tau_FIFO_full_thresh_negate_i, --"111011110",
       
       T_TOB_FIFO_data_count           =>  T_TOB_FIFO_data_count_i, 
       T_TOBs_FIFO_full_thresh_assert  =>  T_TOBs_FIFO_full_thresh_assert_i,  --"111111110",
       T_TOBs_FIFO_full_thresh_negate  =>  T_TOBs_FIFO_full_thresh_negate_i,  --"111011110",
       
       Link_output_FIFO_full_thresh_assert   =>  Link_output_FIFO_full_thresh_assert_i,  -- "1111111111110",
       Link_output_FIFO_full_thresh_negate   =>  Link_output_FIFO_full_thresh_negate_i,  -- "1111011111110" ,
       Link_output_FIFO_rd_data_count        => Link_output_FIFO_rd_data_count_i,
       Link_output_FIFO_wr_data_count        => Link_output_FIFO_wr_data_count_i,
       
       SPY_TOB_mem_wr_addr     => SPY_mem_wr_addr_i    ,    -- SPY memory wr_addr (read only) 
       SPY_TOB_mem_rd_addr_en  => SPY_mem_rd_addr_en_i ,    -- 1b rd addr en                  
       SPY_TOB_mem_rd_addr     => SPY_mem_rd_addr_i    ,    -- 11b rd_addr                    
       SPY_TOB_mem_rd_data     => SPY_mem_rd_data_i        -- SPY memory data (read only)    
       );

slave_1: entity TOB_rdout_lib.slave_TOB_readout
  port map(
	 ipb_clk     => clk_40M,  
	 ipb_rst     => sys_rst,  
	 ipb_in     => IPb_in_i,
	 ipb_out    => IPb_outI,
	 L1A_ID     => L1A_ID_i,			-- output reg
	 BCN_ID     => BCN_in_reg,
	 TOB_WR_ADDR_OFFSET   			  => pre_ld_wr_addr_i,			 
	 TOB_SLICES_TO_RD                 => DPR_locations_to_rd_i,                
	 TOB_FIFO_FULL_THRESH_ASSERT      => T_TOBs_FIFO_full_thresh_assert_i,     
	 TOB_FIFO_FULL_THRESH_NEGATE      => T_TOBs_FIFO_full_thresh_negate_i,     
	 TOB_FIFO_DATA_COUNT              => T_TOB_FIFO_data_count_i,       
	 XTOB_EG_FIFO_FULL_THRESH_ASSERT  => XTOB_EG_FIFO_FULL_THRESH_ASSERT_i, 
	 XTOB_EG_FIFO_FULL_THRESH_NEGATE  => XTOB_EG_FIFO_FULL_THRESH_NEGATE_i, 
	 XTOB_EG_FIFO_DATA_COUNT          => XTOB_eg_FIFO_data_count_i,        
	 XTOB_TAU_FIFO_FULL_THRESH_ASSERT => XTOB_TAU_FIFO_FULL_THRESH_ASSERT_i,
	 XTOB_TAU_FIFO_FULL_THRESH_NEGATE => XTOB_TAU_FIFO_FULL_THRESH_NEGATE_i,
	 XTOB_TAU_FIFO_DATA_COUNT         => XTOB_TAU_FIFO_DATA_COUNT_i,
	 TOB_Link_output_FIFO_FULL_THRESH_ASSERT => Link_output_FIFO_full_thresh_assert_i,
	 TOB_Link_output_FIFO_FULL_THRESH_NEGATE => Link_output_FIFO_full_thresh_negate_i,
	 TOB_LINK_OUTPUT_FIFO_RD_DATA_COUNT      => Link_output_FIFO_rd_data_count_i     ,
	 TOB_LINK_OUTPUT_FIFO_WR_DATA_COUNT      => Link_output_FIFO_wr_data_count_i     ,
	 TOB_data_FIFO_flags        => TOB_data_FIFO_flags_i ,
	 SPY_TOB_mem_wr_addr        =>  SPY_mem_wr_addr_i       , -- TOB/XTOB data SPY memory wr addr pointer 
	 SPY_TOB_mem_rd_addr_en     =>  SPY_mem_rd_addr_en_i    , -- TOB/XTOB data SPY memory rd addr en
	 SPY_TOB_mem_rd_addr        =>  SPY_mem_rd_addr_i       , -- TOB/XTOB data SPY memory rd addr
	 SPY_TOB_mem_rd_data        =>  SPY_mem_rd_data_i         -- TOB/XTOB data SPY memory rd data
	 ); 	   

U13_ipbus_TOB_slave_tst : entity TOB_rdout_sim.ipbus_TOB_slave_tst  -- get test data 
--    generic map (N_SLAVES => 2)
    port map (
       ipb_rst     => sys_rst ,     -- i/p
       ipb_clk     => clk_40M ,     -- i/p
       IPb_in      => IPb_in_i  ,  -- o/p
       IPb_out     => IPb_outI ,    -- i/p
       L1A_tst_out => L1A_sw_pulse_i  -- o/p
       );

--U14_L1A_gen_sw_tst : process (clk_40M)
--   begin
--    if clk_40M'event and clk_40M = '1' then
--        if sys_rst = '1' then
--            reg1    <= '0' ;
--            reg2    <= '0' ;
--            L1A_sw_pulse_i    <= '0' ;
--        else
--            reg1    <= RDOUT_TOB_PULSE_REG_i(0) ;   -- this is test L1A under s/w control
--            reg2    <= reg1 ;
--            L1A_sw_pulse_i    <= reg1 AND (NOT reg2) ;
--        end if;
--    end if;
--        end process;	
	
	
	
	
	   
	   
	   
	   
end Behavioral;
