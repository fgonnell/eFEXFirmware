----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/03/2017 02:03:01 PM
-- Design Name: 
-- Module Name: L1A_gen_tst - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

entity L1A_gen_tst is
    Port (  clk_in : in STD_LOGIC;
            rst    : in STD_LOGIC;
            L1A_pulse : out STD_LOGIC);
end L1A_gen_tst;

architecture clk_pulse of L1A_gen_tst is
    constant clk_40_period : time := 25 ns;
	signal cntr : unsigned (20 downto 0):= (others => '0') ;

begin
  process(clk_in)
     begin
        if clk_in' event and clk_in ='1' then 
              cntr<= cntr+1;         -- free running counter at 40MHz
--              if cntr = 100 then     -- generate pulse every 100 KHz
              if cntr = 150 then     -- generate pulse every 75 KHz
--              if cntr = 400000 then     -- generate pulse every 100 Hz
                 L1A_pulse <= '1' ; 
                 cntr <= (others => '0'); 
              else   
                 L1A_pulse <= '0';
              end if;
         end if;
       end process;
                
end clk_pulse;

architecture time_pulse of L1A_gen_tst is
signal cntr : unsigned (20 downto 0):= (others => '0') ;

begin
  process
     begin
        L1A_pulse <= '0';
        wait for 7 us;  --alighn to 40MHz clk for 1st data to be BC
        L1A_pulse <= '1';
        wait for 25 ns ;  --for next 25 ns signal is '1'.
        L1A_pulse <= '0';
        wait for 500 ns ;  -- signal is '0'.
        L1A_pulse <= '1';
        wait for 25 ns ;  --for next 25 ns signal is '1'.
        L1A_pulse <= '0';
        wait for 0.4 us;  -- signal is '0'.
        L1A_pulse <= '1';
        wait for 50 ns ;  --for next 25 ns signal is '1'.
        L1A_pulse <= '0';
        wait for 0.3 us;  -- signal is '0'.
        L1A_pulse <= '1';
        wait for 75 ns ;  --for next 25 ns signal is '1'.
        L1A_pulse <= '0';
        wait for 0.4 us;  -- signal is '0'.
        L1A_pulse <= '1';
        wait for 100 ns ;  --for next 25 ns signal is '1'.
        L1A_pulse <= '0';
        wait for 0.5 us;  -- signal is '0'.
        L1A_pulse <= '1';
        wait for 25 ns ;  --for next 25 ns signal is '1'.
        L1A_pulse <= '0';
        wait ;
     end process;
                
end time_pulse;
