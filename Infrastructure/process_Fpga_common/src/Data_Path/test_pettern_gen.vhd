library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity test_pettern_gen is
   port( 
      clk      : IN     std_logic;
      enable   : IN     std_logic;
      rst      : IN     std_logic;
      bcn      : IN     std_logic_vector (3 downto 0 );
      test_data : OUT    std_logic_vector (33 DOWNTO 0)
      
   );


end entity test_pettern_gen ;

 
architecture fsm OF test_pettern_gen IS

signal data_out :std_logic_vector(31 downto 0 ):= (others => '0');
signal sync,crc_start:std_logic;
signal cntr     :unsigned(2 downto 0);

   type state_type is (
      idle,
      gen_bc,
      gen_55,
      gen_aa, 
      start
   );
 
   -- Declare current and next state signals
   signal current_state : STATE_TYPE;

begin

test_data <= crc_start & sync & data_out;



   -----------------------------------------------------------------
   clocked_proc : PROCESS ( clk)
   
 begin
    if (clk'event and clk = '1') then
      if (rst = '1') then
         current_state <= idle;
         -- Default Reset Values
         data_out <= (others => '0');
         sync <= '0';
         crc_start <= '0';
         cntr <= (others => '0');
      else
         -- Default Assignment To Internals and Outputs
         sync     <= '0';
         crc_start<= '0';
         data_out <= (others => '0');
         cntr     <= (others => '0');
         -- Combined Actions
         case current_state IS
            when idle =>    
                 if (enable ='1') then 
                  current_state <= start;
               else
                  current_state <= idle;
               end if;
            when gen_bc => 
               sync <= '1';
               data_out <= '0' & x"00" & bcn & "000" & x"0000";
               cntr <= (others => '0');
               if (enable = '0')then 
                   current_state <= idle;
                else 
                current_state <= start;
                end if ;                              
              
            when gen_55 => 
               cntr <= cntr +1;
               data_out <= x"55555555";
               if (cntr =5) then 
                   current_state <= gen_bc;
                  else 
                   current_state <= gen_aa;
                   end if;
                   
            when gen_aa => 
               cntr <= cntr +1;
               data_out <= x"aaaaaaaa";
               current_state <= gen_55;
               
            when start =>     
               crc_start <= '1';    
               cntr <= cntr +1;    
               current_state <= gen_55;    
               data_out <= x"00000efe";                                                
                                               
             when others=>                     
               current_state <= idle;               
                                   
              end case;                     
             end if;                     
           end if;                     
       end process clocked_proc;                     
                                 
 end ARCHITECTURE fsm;                     
                                
                                
                                
                                