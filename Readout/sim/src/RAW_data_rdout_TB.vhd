----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/23/2017 10:26:45 AM
-- Design Name: 
-- Module Name: RAW_data_rdout_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library TOB_rdout_sim;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;
use TOB_rdout_lib.data_type_pkg.all;
use TOB_rdout_lib.ipbus_decode_efex_readout.all;	-- decoder package

library ipbus_lib;
use ipbus_lib.ipbus.all;

entity RAW_data_rdout_TB is
--  Port ( );
end RAW_data_rdout_TB;

architecture Behavioral of RAW_data_rdout_TB is

    signal  ii        : integer range 0 to 50 := 0;

    signal  tied_to_ground_i        : std_logic;
    signal  tied_to_vcc_i           : std_logic;
    signal  tied_high_i             : std_logic_vector(0 downto 0);
    signal  locked_i                : std_logic;
    signal  hw_addr_i               : std_logic_vector(1 downto 0) ; 

    signal  TX_DATA_OUT_i           : std_logic_vector(223 downto 0);
    
    signal  L1A_in_i                : std_logic ;
    signal  BCN_in_i                : STD_LOGIC_VECTOR (11 downto 0) := (others => '0' );
    signal  BCN_in_reg              : STD_LOGIC_VECTOR (11 downto 0)  := (others => '0' );
    signal  L1A_ID_i                : STD_LOGIC_VECTOR (23 downto 0)  := (others => '0' );
    signal  TXCTRL_OUT_i            : std_logic_vector(3 downto 0) ;
    
    signal RAW_out_to_MGT_is_char_i : std_logic := '0';
    signal RAW_data_out_i           : STD_LOGIC_VECTOR (31 downto 0);
    signal RAW_data_in_i            : RAW_data_224_type;    -- array 49 x 224b input frames
    signal link_error_flags_i       : link_error_type;      -- array 49 x 4 bit per link

    signal  pre_ld_wr_addr_i        : STD_LOGIC_VECTOR (8 downto 0);

    signal sys_rst                  : std_logic := '0';
    signal clk_40M                  : std_logic := '0';
    constant clk_40_period          : time := 25 ns;
    signal clk_40M_ld               : std_logic := '0';
    signal clk_160M                 : std_logic := '0';
    signal clk_200M                 : std_logic := '0';
    constant clk_200_period         : time := 5 ns;
    signal clk_280M                 : std_logic := '0';
    
    signal back_pressure_in_i       : std_logic := '0';


--    signal TX_TOB_eg_DATA_OUT_i      : std_logic_vector(31 downto 0) ;       -- TOB e/g
    signal TXCTRL_TOB_eg_OUT_i       : std_logic_vector(3 downto 0) := "0000";        -- TOB e/g
    signal TOB_eg_Valid_flg_in_i     : std_logic_vector(7 downto 0) := "00000000" ;        -- TOB e/g
--    signal TX_TOB_tau_DATA_OUT_i     : std_logic_vector(31 downto 0) ;      -- TOB tau
--    signal TXCTRL_TOB_tau_OUT_i      : std_logic_vector(3 downto 0) ;       -- TOB tau
    signal TOB_tau_Valid_flg_in_i    : std_logic_vector(7 downto 0) := "00000000" ;       -- TOB tau
    signal link_err_flg_in_i         : std_logic_vector(3 downto 0) := "0000" ;
    signal rand_out_40M              : std_logic_vector(15 downto 0) ;
    signal rand_out_280M             : std_logic_vector(15 downto 0) ;
   

    signal  RAW_FIFO_FULL_THRESH_ASSERT_i   : STD_LOGIC_VECTOR (8 downto 0) := "111101111";   -- MGT_FIFO
    signal  RAW_FIFO_FULL_THRESH_NEGATE_i   : STD_LOGIC_VECTOR (8 downto 0) := "110111111";   -- MGT_FIFO    

    signal  BCN_FIFO_full_thresh_assert_i   : STD_LOGIC_VECTOR (8 downto 0) := "111101111";   -- MGT_FIFO
    signal  BCN_FIFO_full_thresh_negate_i   : STD_LOGIC_VECTOR (8 downto 0) := "110111111";   -- MGT_FIFO    

    signal  Link_output_FIFO_RAW_rd_data_count_i        : STD_LOGIC_VECTOR (12 downto 0) := (others => '0');   -- occupancy of RAW output link MGT FIFO
    signal  Link_output_FIFO_RAW_wr_data_count_i        : STD_LOGIC_VECTOR (12 downto 0) := (others => '0');   -- occupancy of RAW output link MGT FIFO
    signal  Link_output_FIFO_RAW_full_thresh_assert_i   : STD_LOGIC_VECTOR (12 downto 0) := "1111111101111";   -- MGT_FIFO	
    signal  Link_output_FIFO_RAW_full_thresh_negate_i   : STD_LOGIC_VECTOR (12 downto 0) := "1111110111111";   -- MGT_FIFObegin
    
    signal  frame_count_i           :  STD_LOGIC_VECTOR (31 downto 0) := (others => '0');      -- TOB data Frame counter
    signal  RAW_frame_count_i       :  STD_LOGIC_VECTOR (31 downto 0) := (others => '0');      -- calo data Frame counter
--    signal  RDOUT_PULSE_REG_i       : STD_LOGIC_VECTOR (31 downto 0);
    
    signal  SPY_mem_wr_addr_i       : STD_LOGIC_VECTOR (10 downto 0) := (others => '0');       -- calo data SPY memory wr addr pointer  
    signal  SPY_mem_rd_addr_en_i    : STD_LOGIC := '0';                                        -- calo data SPY memory rd addr en      
    signal  SPY_mem_rd_addr_i       : STD_LOGIC_VECTOR (10 downto 0) := (others => '0');       -- calo data SPY memory rd addr         
    signal  SPY_mem_rd_data_i       : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');       -- calo data SPY memory rd data         
    
    signal  ipb_rst_i       : STD_LOGIC := '0'; 
    signal  ipb_clk_i       : STD_LOGIC := '0'; 
    signal  IPb_in_i        : ipb_wbus;      -- The signals going from master to slaves
    signal  IPb_outI        : ipb_rbus;     -- The signals going from slaves to master

    signal  reg1                : std_logic := '0'; 
    signal  reg2                : std_logic := '0';
    signal  L1A_sw_pulse_i      : std_logic := '0';

            

begin

    tied_to_ground_i        <=   '0';
    tied_to_vcc_i           <=   '1';
    tied_high_i             <= (others => '1') ;
        
    pre_ld_wr_addr_i      <= "000010000" ;        -- latency pre load for DRP wr address =16
    
    hw_addr_i <= "01" ;
    

U0_clk_40M : process
      begin
         clk_40M <= '1';
         wait for clk_40_period/2;  --for 25 ns signal is '0'.
         clk_40M <= '0';
         wait for clk_40_period/2;  --for next 25 ns signal is '1'.
    end process;

U1_clk_40M_ld : process       -- 40MHz 20% duty cycle
      begin
         clk_40M_ld <= '1';
         wait for clk_200_period;  
         clk_40M_ld <= '0';
         wait for (clk_200_period * 4);  
    end process;


U2_clk_tst : clk_wiz_tst
    port map (
        clk_160M     => clk_160M , 
        clk_200M     => clk_200M , 
        clk_280M     => clk_280M ,
        reset        => '0' , 
        locked       => locked_i , 
        clk_in1      => clk_40M  
    );

    sys_rst <= NOT locked_i ;       -- system reset

                
U3_RAW_data : entity TOB_rdout_sim.gen_224b_raw_data_tst  -- get test data 
    generic map
    (
        WORDS_IN_BRAM =>   515,
        FILE_NAME     => "RAW_data_rom_init_tx_tst.dat"
    )   
    port map
    (
        -- User Interface
        TX_DATA_OUT     =>  TX_DATA_OUT_i ,     -- std_logic_vector(223 downto 0)
        RAW_data_out    =>  RAW_data_in_i ,     -- array 49 x 224b input frames
        TXCTRL_OUT      =>  TXCTRL_OUT_i  ,     -- start bit for data valid
        -- System Interface
        USER_CLK        => clk_40M ,     -- data changes at 200MHz 
        SYSTEM_RESET    => sys_rst  
    );
    

U4_rand_40M : entity TOB_rdout_lib.ran_num_gen_16b
  Port map (
      rst         =>  sys_rst ,
      clk         =>  clk_40M , 
      MAC_addr    =>  X"abcd0001" ,
      rand_out    =>  rand_out_40M
      );
    
U5_rand_280M : entity TOB_rdout_lib.ran_num_gen_16b
    Port map (
        rst         =>  sys_rst  ,
        clk         =>  clk_280M ,
        MAC_addr    =>  X"abcd0002" ,
        rand_out    =>  rand_out_280M
        );
          

U6_L1A_gen : entity TOB_rdout_sim.L1A_gen_tst 
    Port map
    (   
        clk_in   => clk_40M ,
        rst      => sys_rst ,
        L1A_out  => L1A_in_i
        );


U7_BCN_gen : entity TOB_rdout_lib.cntr_ld_12b
    generic map (
        T_count       => X"EAC"   ,  -- bunch count total
        ld_count      => X"080"
        )

    Port map ( 
       CE         => '1' ,
       CLK        => clk_40M ,
       RST        => sys_rst ,
       LD         => sys_rst ,
       Q          => BCN_in_i
       );

--! L1A counter 
U8_L1A_gen : entity TOB_rdout_lib.cntr_L1A_generic
   Port map ( 
--          CE   =>  L1A_in_i ,   -- L1A pulse from the test generator
      CE   =>  L1A_sw_pulse_i       ,    -- L1A_sw_pulse_i under s/w control
      CLK  =>  clk_40M ,
      RST  =>  sys_rst,
      Q    =>  L1A_ID_i
      ); 
        
U9_RAW_readout : entity TOB_rdout_lib.RAW_data_rdout
    Port map ( 
        RST              => sys_rst          ,
        hw_addr          => hw_addr_i ,    -- i/p
        RAW_data_in      => RAW_data_in_i  ,  -- array 49 x 224b input frames
        TOBs_sync_in     => clk_40M_ld ,        -- TXCTRL_OUT_i (0)  ,
        clk_280M_in      => clk_280M ,
        clk_160M_in      => clk_160M ,
        clk_40M_in       => clk_40M   ,
        ctrl_ready_in    => '1' ,               -- control FPGA is ready to take data
        RAW_TXOUTCLK     => clk_160M ,          -- must be changed to 280MHz for production
--        L1A_in           => L1A_in_i       ,    -- L1A pulse from the test generator
        L1A_in           => L1A_sw_pulse_i       ,    -- L1A_sw_pulse_i under s/w control
        BCN_ID_in        => BCN_in_i       ,
        L1A_ID_in        => L1A_ID_i       ,
        link_error_flags_in  => link_error_flags_i , -- array 49 x 4 bit per link
        pre_ld_wr_adrr       => pre_ld_wr_addr_i ,              -- latency pre load for DRP wr address    
        full             => open ,
        empty            => open ,
        valid            => open ,
        prog_full        => open ,
        RAW_out_to_MGT_is_char     => RAW_out_to_MGT_is_char_i,           -- Raw data valid to MGT
        RAW_data_out    => RAW_data_out_i ,                 -- raw data 32b data to MGT
        frame_count     => RAW_frame_count_i,
        RAW_FIFO_FULL_THRESH_ASSERT => RAW_FIFO_FULL_THRESH_ASSERT_i,
        RAW_FIFO_FULL_THRESH_NEGATE => RAW_FIFO_FULL_THRESH_NEGATE_i,
		
		BCN_FIFO_full_thresh_assert => BCN_FIFO_full_thresh_assert_i , 
        BCN_FIFO_full_thresh_negate => BCN_FIFO_full_thresh_negate_i ,
        --! Link output FIFO before MGT
        Link_output_FIFO_RAW_full_thresh_assert  => Link_output_FIFO_RAW_full_thresh_assert_i ,   -- Link_output_FIFO
        Link_output_FIFO_RAW_full_thresh_negate  => Link_output_FIFO_RAW_full_thresh_negate_i ,  -- Link_output_FIFO
        Link_output_FIFO_RAW_rd_data_count => Link_output_FIFO_RAW_rd_data_count_i,         -- occupancy of RAW output link MGT FIFO
        Link_output_FIFO_RAW_wr_data_count => Link_output_FIFO_RAW_wr_data_count_i,          -- occupancy of RAW output link MGT FIFO
		
        SPY_mem_wr_addr   =>  SPY_mem_wr_addr_i ,     -- SPY memory wr_addr (read only)
        SPY_mem_rd_addr_en => SPY_mem_rd_addr_en_i,
		SPY_mem_rd_addr   =>  SPY_mem_rd_addr_i   ,   -- 1b rd_en + 11b rd_addr        
        SPY_mem_rd_data   =>  SPY_mem_rd_data_i       -- SPY memory data (read only)   
				
        );


U10_clk_proc_280 : process (clk_280M)
    begin
        if clk_280M'event AND clk_280M = '1' then
            if  ( TXCTRL_TOB_eg_OUT_i(0) = '1' ) then 
                BCN_in_reg <= BCN_in_i ;
            end if;
        end if;
    end process;
    
U11_clk_proc_40 : process (clk_40M)
        begin
            if clk_40M'event AND clk_40M = '1' then
                if sys_rst = '1' then
                    link_error_flags_i <= (others => (others => '0'));
                    ii <= 0;
                else
                     link_error_flags_i(ii) <= rand_out_40M(3 downto 0) after 50 ps;
                     -- link_error_flags_i(ii+1) <= link_error_flags_i(ii);
                     if  ii = 47 then 
                        ii <= 0;
                     else
                        ii <= ii +1 ; 
                     end if;                                            
                end if;
            end if;
        end process;
    
        
    TOB_eg_Valid_flg_in_i <= '0' & rand_out_280M(6 downto 0) ;    -- 8 bit assignment for TOB e/g valid

    TOB_tau_Valid_flg_in_i <= '0' & rand_out_280M(15 downto 9) ;    -- 8 bit assignment for TOB tau valid

    link_err_flg_in_i  <= rand_out_280M(11 downto 8) ;    -- 4 bit assignment for link error flag
    
    back_pressure_in_i <= '0' ;         -- for test set to 0


slave_2: entity TOB_rdout_lib.slave_RAW_readout
          port map(
            ipb_clk  => clk_40M,  
            ipb_rst  => sys_rst,  
             ipb_in  => IPb_in_i,
             ipb_out => IPb_outI,
             RAW_FIFO_FULL_THRESH_ASSERT  =>  RAW_FIFO_FULL_THRESH_ASSERT_i,    -- o/p
             RAW_FIFO_FULL_THRESH_NEGATE  =>  RAW_FIFO_FULL_THRESH_NEGATE_i,    -- o/p
             
             BCN_FIFO_full_thresh_assert    =>    BCN_FIFO_full_thresh_assert_i ,        -- o/p
             BCN_FIFO_full_thresh_negate    =>    BCN_FIFO_full_thresh_negate_i ,     -- o/p
             
             Link_output_FIFO_FULL_THRESH_ASSERT   =>  Link_output_FIFO_RAW_full_thresh_assert_i   ,  -- o/p
             Link_output_FIFO_FULL_THRESH_NEGATE   =>  Link_output_FIFO_RAW_full_thresh_negate_i   ,  -- o/p
             Link_output_FIFO_rd_data_count        =>  Link_output_FIFO_RAW_rd_data_count_i        ,  -- i/p
             Link_output_FIFO_wr_data_count        =>  Link_output_FIFO_RAW_wr_data_count_i ,         -- i/p
             RAW_frame_count                       => RAW_frame_count_i,
             SPY_mem_wr_addr        =>  SPY_mem_wr_addr_i       , -- calo data SPY memory wr addr pointer 
             SPY_mem_rd_addr_en     =>  SPY_mem_rd_addr_en_i    , -- calo data SPY memory rd addr en
             SPY_mem_rd_addr        =>  SPY_mem_rd_addr_i       , -- calo data SPY memory rd addr
             SPY_mem_rd_data        =>  SPY_mem_rd_data_i         -- calo data SPY memory rd data
           ); 

U13_ipbus_slave_tst : entity TOB_rdout_sim.ipbus_slave_tst  -- get test data 
--    generic map (N_SLAVES => 2)
    port map (
       ipb_rst     => sys_rst ,     -- i/p
       ipb_clk     => clk_40M ,     -- i/p
       IPb_in      => IPb_in_i  ,  -- o/p
       IPb_out     => IPb_outI  ,   -- i/p
       L1A_tst_out => L1A_sw_pulse_i  -- o/p
       );

--U14_L1A_gen_sw_tst : process (clk_40M)
--   begin
--    if clk_40M'event and clk_40M = '1' then
--        if sys_rst = '1' then
--            reg1    <= '0' ;
--            reg2    <= '0' ;
--            L1A_sw_pulse_i    <= '0' ;
--        else
--            reg1    <= RDOUT_PULSE_REG_i(0) ;   -- this is test L1A under s/w control
--            reg2    <= reg1 ;
--            L1A_sw_pulse_i    <= reg1 AND (NOT reg2) ;
--        end if;
--    end if;
--        end process;

end Behavioral;
