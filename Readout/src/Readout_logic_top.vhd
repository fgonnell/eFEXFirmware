--! @file 
--! @brief Top Level of Readout Logic for process FPGA
--! @details 
--! \verbatim
--! This is the top module of the Readout Logic  
--! The input are: 
--!    - Calorimeter data extracted from the real-time path at the input to the algorithm block  
--!    - XTOBs for tau or e/g data from the real-time path at the output of the algorithm block  
--!    - Sorted TOBs for tau or e/g data from the real-time path at the output of the merging block 
--! 
--! The Readout output is two streams of 32b data and consists of both RAW Calorimeter data and Sorted TOBs & XTOBs, 
--! as defined in eFEX data format document.  
--!
--! Two streams of 32b data is fed to the two MGTs through Link Output FIFOs for tranfer to Control FPGA  
--! \endverbatim
--! \image html pFPGA_Readout_Block.png "Readout Logic Block Diagram"  
--! \verbatim
--!
--! This block consists of four submodules:  
--!     Submodule 1 : TOBs_Readout  
--!     Submodule 2 : RAW_data_Readout  
--!     Submodule 2 : readout_ipb_slave  
--!     Submodule 2 : L1A counter  
--!
--! TOBs_Readout Module is responsible for reading the sorted TOBs and XTOBS from outputs of Algo Block, create a full  
--! events, storing only valid TOBs and XTOBs.   
--! The events are stored in Link Output FIFO ready to be sent to cFPGA via an MGT link. 
--! Process FPGA 1 reads e/g TOBs and Process FPGA 2 reads tau TOBs, so same firmware in both FPGAs, use FPGA_NUMBER generic to differentiate.
--!
--! RAW_data_Readout Module is responsible for reading the RAW calorimeter data from outputs synchronisation block.  
--! This module creates RAW events, by reading fibre input data. 
--! There are 40 ECAL and 9 HCAL fibre links.   
--! The events are stored in Link Output FIFO ready to be sent to cFPGA via an MGT link.    
--!
--! Readout_ipb_slave Module is responsible for communication with IPBus.  
--! All registers in RAW and TOB blocks are accessed trough this module.  
--! The events are stored in Link Output FIFO ready to be sent to cFPGA via an MGT link.    
--!
--! L1A_cntr Module is responsible for generating L1A_ID.  
--! The first event has L1A_ID of ZERO.   
--! \endverbatim
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

library TOB_rdout_lib;
use TOB_rdout_lib.data_type_pkg.all;
use TOB_rdout_lib.TOB_rdout_ip_pkg.all;

library ipbus_lib;
use ipbus_lib.ipbus.all;

library algolib;
use algolib.AlgoDataTypes.all;

--! @copydoc Readout_logic_top.vhd
entity Readout_logic_top is
    Generic
        ( 
    --! Integer used to distinguish different FPGAs having a slightly different firmware
    FPGA_NUMBER : integer
        ) ; 
    Port ( 
       RST         : in std_logic ;     -- from 40MHz MMCM lock signal
       --! FPGA Hardware Address
       hw_addr     : in STD_LOGIC_VECTOR(1 downto 0) ;
       --! ipb_rst signal is input from master to slaves
       ipb_rst     : in std_logic ;
       --! ipb_clk signal is input from master to slaves
       ipb_clk     : in std_logic ;
       --! IPb_in signal going from master to slaves
       IPb_in      : in ipb_wbus;
       --! IPb_out signal going from slaves to master
       IPb_out     : out ipb_rbus; 
       --! 200Mhz input signal    
       clk_200M_in : in STD_LOGIC;
       --! 280Mhz input signal    
       clk_280M_in : in STD_LOGIC;
       --! 160MHz clock - not currently used in production eFEX
       clk_160M_in : in STD_LOGIC; 
       --! 40Mhz input signal    
       clk_40M_in  : in STD_LOGIC;
       --! TOB TXOUTCLK to read XTOB/TOB data to MGT for transmission to control FPGA
       TOB_TXOUTCLK  : in STD_LOGIC; 
       --! Calorimeter TXOUTCLK to read Calorimeter data to MGT for transmission to control FPGA
       RAW_TXOUTCLK  : in STD_LOGIC;
       -- eXtended TOB data readout signals
       --! XTOBs e/g 64b * 8
       XTOB_eg_in             : in  AlgoXOutput;    -- array 8 x 64b words XTOB e/g
       --! 8b XTOB e/g has valid d
       XTOB_eg_Valid_flg_in   : in STD_LOGIC_VECTOR (7 downto 0);  
       --! XTOB e/g  sync sig
       XTOB_eg_sync_in        : in STD_LOGIC;                      
       --! XTOBs tau 64b 
       XTOB_tau_in            : in AlgoXOutput;        -- array 8 x 64b words XTOB tau
       --! 8b XTOB tau has valid d
       XTOB_tau_Valid_flg_in  : in STD_LOGIC_VECTOR (7 downto 0);  
       --! XTOB tau  sync sig
       XTOB_tau_sync_in       : in STD_LOGIC;
       --! XTOB BC_ID with delay through ALGO/sorting block
       OUT_XTOB_BCN          : in std_logic_vector(6 downto 0);                      
       --! Sorted TOB data readout 32b * 7 in series, only 6 is used - F1 reads e/g TOBs and F2 reads tau TOBs
       T_TOB_32b_in     : in STD_LOGIC_VECTOR (31 downto 0);   
       --! sorted TOB start signal
       T_TOB_sync_in    : in STD_LOGIC; 
       --! sorted TOB valid signal                       
       T_TOB_valid_in      : in STD_LOGIC;
       --! sorted TOB BC_ID with delay through ALGO/sorting block
       OUT_TOB_BCN         : in std_logic_vector(6 downto 0);
       --! TOB Type 0 = e/g for pFPGA 1, TOB Type 1 = tau for pFPGA 2                        
       TOB_type_in         : in STD_LOGIC;
       --! L1A signal input 
       L1A_in           : in STD_LOGIC;
       --!ECR signal input
       ECR_in             : in STD_LOGIC; 
       --!BCR signal input
       BCR_in             : in STD_LOGIC;
       --! Privilege Read signal input (previledge read)
       TTC_read_all_in             : in STD_LOGIC;
       --! Local BCN generated in Process FPGA
       local_BCN_out       : out  STD_LOGIC_VECTOR (11 downto 0);
       --! Extended L1A ID provided by TTC - ECRID
       TTC_L1A_ID_EXT_in  : in  STD_LOGIC_VECTOR (7 downto 0);
       --! Ready signal from control FPGA to receive TOBs data
       ctrl_TOB_ready_in    : in  std_logic ;
       --! Ready signal from control FPGA to receive RAW calorimeter data
       ctrl_RAW_ready_in        : in  std_logic ;
       --! 32b data out to MGT is CHAR    
       TOB_out_is_char  : out STD_LOGIC;
       --! 32b sorted XTOB/TOB out to MGT
       TOB_out          : out STD_LOGIC_VECTOR (31 downto 0);   
       --! RAW data readout signals       
       --! ERROR flags array 49 x 4 bit per link
       --link_error_flags_in  : in link_error_type ;
       --! calorimeter data array 49 x 224b input frames              
       RAW_data_in          : in RAW_data_227_type;  
       --! calorimeter data 32b out to MGT is CHAR                
       RAW_out_is_char      : out STD_LOGIC; 
       --! calorimeter data 32b out to MGT                       
       RAW_data_out         : out STD_LOGIC_VECTOR (31 downto 0)    
       );
end Readout_logic_top;

--! @copydoc Readout_logic_top.vhd 
architecture Behavioral of Readout_logic_top is

--************************** Register Declarations ****************************    

    -- number of DRP locations (Slices) to read 1 to 5
    signal  DPR_locations_to_rd_i   : STD_LOGIC_VECTOR (2 downto 0) ; 
    signal  pre_ld_RAW_wr_addr_i    : STD_LOGIC_VECTOR (8 downto 0) ; -- The write address offset pre load for Circular DRPAM of RAW data
    signal  pre_ld_TOB_wr_addr_i      : STD_LOGIC_VECTOR (8 downto 0) ;
    signal  pre_ld_XTOB_eg_wr_addr_i  : STD_LOGIC_VECTOR (8 downto 0) ;
    signal  pre_ld_XTOB_tau_wr_addr_i : STD_LOGIC_VECTOR (8 downto 0) ;
    signal  RAW_data_tmp            : RAW_data_228_type;     -- array 49 x 228b input frames
--    signal  link_error_flags_i      : link_error_type;      -- array 49 x 4 bit per link
    
    signal  L1A_ID_i                : STD_LOGIC_VECTOR (23 downto 0);
    signal  L1A_ID_EXT_i            : STD_LOGIC_VECTOR (7 downto 0);
    signal  L1A_ID_EXT_ii           : STD_LOGIC_VECTOR (7 downto 0);
    signal  RST_i, BCR_1dly         : STD_LOGIC  ; 
    signal  TTC_read_all_1dly       : STD_LOGIC  := '0' ; 
    signal  L1A_cntr_rst_i          : STD_LOGIC  := '0' ; 
    signal  L1A_cntr_rst_tmp        : STD_LOGIC  := '0' ; 
    
    signal  XTOB_eg_FIFO_pFULL_THRESH_assert_i  : STD_LOGIC_VECTOR (8 downto 0) ;   -- XTOB_eg__FIFO
    signal  XTOB_eg_FIFO_pFULL_THRESH_negate_i  : STD_LOGIC_VECTOR (8 downto 0) ;   -- XTOB_eg__FIFO        
    signal  XTOB_eg_FIFO_data_count_i          : STD_LOGIC_VECTOR (8 downto 0) ;   -- XTOB_eg__FIFO        
    signal  XTOB_tau_FIFO_pFULL_THRESH_assert_i : STD_LOGIC_VECTOR (8 downto 0) ;   -- XTOB_tau_FIFO
    signal  XTOB_tau_FIFO_pFULL_THRESH_negate_i : STD_LOGIC_VECTOR (8 downto 0) ;   -- XTOB_tau_FIFO        
    signal  XTOB_tau_FIFO_data_count_i         : STD_LOGIC_VECTOR (8 downto 0) ;   -- XTOB_tau_FIFO        

    signal  TOB_FIFO_pFULL_THRESH_assert_i  : STD_LOGIC_VECTOR (8 downto 0) ;   -- sorted TOBs_FIFO
    signal  TOB_FIFO_pFULL_THRESH_negate_i  : STD_LOGIC_VECTOR (8 downto 0) ;   -- sorted TOBs_FIFO
    signal  TOB_FIFO_data_count_i          : STD_LOGIC_VECTOR (8 downto 0) ;   -- sorted TOBs_FIFO
    signal  TOB_Link_outpout_FIFO_pFULL_THRESH_assert_i   : STD_LOGIC_VECTOR (12 downto 0) ;   -- MGT_FIFO
    signal  TOB_Link_outpout_FIFO_pFULL_THRESH_negate_i   : STD_LOGIC_VECTOR (12 downto 0) ;   -- MGT_FIFO    
    signal  TOB_Link_outpout_FIFO_rd_data_count_i        : STD_LOGIC_VECTOR (12 downto 0) ;   -- MGT_FIFO

    signal  RAW_FIFO_pFULL_THRESH_ASSERT_i   : STD_LOGIC_VECTOR (8 downto 0) ;   -- MGT_FIFO
    signal  RAW_FIFO_pFULL_THRESH_NEGATE_i   : STD_LOGIC_VECTOR (8 downto 0) ;   -- MGT_FIFO    

    signal  BCN_FIFO_pFULL_THRESH_assert_i   : STD_LOGIC_VECTOR (8 downto 0) ;   -- MGT_FIFO
    signal  BCN_FIFO_pFULL_THRESH_negate_i   : STD_LOGIC_VECTOR (8 downto 0) ;   -- MGT_FIFO    

    signal  Link_output_FIFO_RAW_rd_data_count_i        : STD_LOGIC_VECTOR (12 downto 0) ;   -- occupancy of RAW output link MGT FIFO
    signal  Link_output_FIFO_RAW_pfull_thresh_assert_i   : STD_LOGIC_VECTOR (12 downto 0) ;   -- MGT_FIFO	
    signal  Link_output_FIFO_RAW_pfull_thresh_negate_i   : STD_LOGIC_VECTOR (12 downto 0) ;   -- MGT_FIFObegin
    
    signal  RAW_frame_count_i       :  STD_LOGIC_VECTOR (31 downto 0) ;      -- calo data Frame counter
    signal  RDOUT_PULSE_REG_i       : STD_LOGIC_VECTOR (31 downto 0) ;       -- pulse register in Readout block
    signal  SPY_mem_wr_addr_i       : STD_LOGIC_VECTOR (10 downto 0) ;       -- calo data SPY memory wr addr pointer  
    
    signal  SPY_TOB_mem_wr_addr_i       : STD_LOGIC_VECTOR (10 downto 0) ;       -- TOB data SPY memory wr addr pointer  

    signal  reg1 , reg2         : std_logic := '0' ; 
    signal  reg3 , reg4         : std_logic := '0' ; 
    signal  reg5 , reg6         : std_logic := '0' ; 
    signal  reg7 , reg8         : std_logic := '0' ;
    signal  reg9 , reg10        : std_logic := '0' ;
    signal  reg11 , reg12       : std_logic := '0' ;
    signal  L1A_sw_pulse_i      : std_logic := '0'; 
	signal  L1A_in_ORed         : std_logic ;
	signal  BC_cntr_load_en_i   : std_logic ;
    signal  TOB_err_4b_in_i     : STD_LOGIC_VECTOR (3 downto 0);    -- 4b error flags CT + BN2 + TIE + EIE
    signal  TEST_CONTROL_REG_i  : std_logic_VECTOR (31 downto 0);       -- control register for test purposes

    signal  RAW_data_FIFO_flags_i   : STD_LOGIC_VECTOR (8 downto 0); 
    signal  TOB_data_FIFO_flags_i   : STD_LOGIC_VECTOR (11 downto 0); 
    signal  RAW_FIFO_sw_rst_i       : STD_LOGIC ; 
    signal  TOB_FIFO_sw_rst_i       : STD_LOGIC ; 
    signal  SPY_RAM_wr_addr_rst_i   : STD_LOGIC ; 

    signal  XTOB_eg_512b_in_i        : AlgoXOutput;  -- i/p TOBs e/g 64b * 8               
    signal  XTOB_eg_Valid_flg_in_i   : STD_LOGIC_VECTOR (7 downto 0) ;  -- i/p 8b TOB e/g has valid data      
    signal  XTOB_eg_sync_in_i        : STD_LOGIC ;  -- i/p TOB e/g  sync signal           
    -- XTOBs tau 64b * 8                                                                     
    signal  XTOB_tau_512b_in_i       : AlgoXOutput; -- i/p TOBs tau 32b * 8               
    signal  XTOB_tau_Valid_flg_in_i  : STD_LOGIC_VECTOR (7 downto 0) ; -- i/p 8b TOB tau has valid data      
    signal  XTOB_tau_sync_in_i       : STD_LOGIC ;-- i/p TOB tau  sync signal           
    -- Topo TOB data readout signals                                                         
    -- F1 reads e/g TOBs and F2 reads tau TOBs                                               
    -- so same firmware in both FPGAs, use hw addr to differentiate                          
    signal  T_TOB_32b_in_i       : STD_LOGIC_VECTOR (31 downto 0); -- i/p sorted TOBs e/g 32b * 7 is series
    signal  T_TOB_sync_in_i      : STD_LOGIC ; -- i/p sorted TOB e/g start signal      
    signal  T_TOB_valid_in_i     : STD_LOGIC ; -- i/p sorted TOB e/g write signal    
    
    signal OUT_TOB_BCN_i        : std_logic_vector (6 downto 0); -- sorted TOB BC_ID with delay through ALGO/sorting block
    signal OUT_XTOB_BCN_i       : std_logic_vector (6 downto 0); -- sorted XTOB BC_ID with delay through ALGO/sorting block
    
    signal  ECR_1dly                   : std_logic ;    -- ECR signal delayed by 1 clk
    signal  raw_rd_all_i               : std_logic ;    -- readout all raw data

    signal  ctrl_RAW_ready_i           : std_logic ;    -- Control FPGA Ready signal internal
    signal  ctrl_TOB_ready_i           : std_logic ;    -- Control FPGA Ready signal internal
    signal  RAW_FIFO_FULL_THRESH_ASSERT_i : STD_LOGIC_VECTOR(8 downto 0);  --  threshold to asser full flag for RAW FIFO
    signal  RAW_FIFO_FULL_THRESH_NEGATE_i : STD_LOGIC_VECTOR(8 downto 0);  --  threshold to remove full flag for RAW FIFO
    signal  RAW_FIFO_data_count_i         : STD_LOGIC_VECTOR(8 downto 0);  --  count data RAW FIFO
   
    signal  TOB_out_is_char_i          : std_logic ;
    signal  TOB_out_i                  : STD_LOGIC_VECTOR(31 downto 0);
    
    signal  RAW_out_is_char_i          : std_logic ;
    signal  RAW_data_out_i             : STD_LOGIC_VECTOR(31 downto 0);

    signal  L1A_ID_int                 : STD_LOGIC_VECTOR(31 downto 0); 
    signal  L1A_ID_Event_i             : STD_LOGIC_VECTOR(31 downto 0); 
    signal  local_BCN_i                : std_logic_vector(11 downto 0);
    signal  FIFO_error_flags_54b_i     : STD_LOGIC_VECTOR (53 downto 0);
       
	signal ipbus_out_raw_dpram_i : ipb_rbus;     -- signal going to RAW SPY DPRAM
    signal ipbus_in_raw_dpram_i  : ipb_wbus;     -- signal coming from RAW SPY DPRAM
	signal ipbus_out_tob_dpram_i : ipb_rbus;     -- signal going to TOB SPY DPRAM
    signal ipbus_in_tob_dpram_i  : ipb_wbus;     -- signal coming from TOB SPY DPRAM
    

--  ####### attributes  ########
    attribute keep       : string ;
    attribute max_fanout : integer;
    attribute keep of       L1A_ID_i : signal is "true" ;
    attribute max_fanout of L1A_ID_i : signal is 40;
    attribute keep of       L1A_ID_EXT_ii : signal is "true" ;
    attribute max_fanout of L1A_ID_EXT_ii : signal is 40;

--  attribute equivalent_register_removal: string;
--  attribute equivalent_register_removal of Behavioral : architecture is "no";

--   #######################################

begin

    RST_i               <= RST;             -- from 40MHz MMCM lock signal
        
    TOB_out_is_char  <= TOB_out_is_char_i ;
    TOB_out          <= TOB_out_i ;          -- o/p
    
    RAW_out_is_char   <= RAW_out_is_char_i ;     -- Raw data valid to MGT
    RAW_data_out      <= RAW_data_out_i  ;          -- raw data 32b data to MGT

-- Pipeline the TOBs and cFPGA Ready input signals by one 280 MHz clock
proc2 : process (clk_280M_in)
begin
    if rising_edge (clk_280M_in) then
        -- register the signals
        ctrl_RAW_ready_i  <= ctrl_RAW_ready_in  ; 
        ctrl_TOB_ready_i  <= ctrl_TOB_ready_in ;
        -- register the TOB  data by one clock to move away from the real time circuit
        T_TOB_32b_in_i    <=  T_TOB_32b_in  ;
        T_TOB_sync_in_i   <=  T_TOB_sync_in ;
        T_TOB_valid_in_i  <=  T_TOB_valid_in   ; 
        OUT_TOB_BCN_i     <= OUT_TOB_BCN;      -- sorted TOB BC_ID with delay through ALGO/sorting block
    end if;
  end process;

-- Pipeline the XTOBs input signals by one 200 MHz clock
proc3 : process (clk_200M_in)
begin
    if rising_edge (clk_200M_in) then
        -- register the XTOB data by one clock to move away from the real time circuit
        XTOB_eg_512b_in_i       <= XTOB_eg_in     ;
        XTOB_eg_Valid_flg_in_i  <= XTOB_eg_Valid_flg_in ;
        XTOB_eg_sync_in_i       <= XTOB_eg_sync_in     ;
        XTOB_tau_512b_in_i      <= XTOB_tau_in     ;
        XTOB_tau_Valid_flg_in_i <= XTOB_tau_Valid_flg_in ;
        XTOB_tau_sync_in_i      <= XTOB_tau_sync_in     ;
        OUT_XTOB_BCN_i          <= OUT_XTOB_BCN;     -- sorted XTOB BC_ID with delay through ALGO/sorting block
    end if;
  end process;

-- TOBs_Readout Module is responsible for reading the sorted TOBs and XTOBS from outputs of Algo Block,
U0_TOBs_readout : entity TOB_rdout_lib.TOBs_rdout
    generic map(FPGA_NUMBER => FPGA_NUMBER)
    Port map ( 
       RST                  => RST_i,   -- i/p
       hw_addr              => hw_addr ,    -- i/p
       RST_spy_mem_wr_addr  => SPY_RAM_wr_addr_rst_i ,   -- i/p SPY memory read address reset to ZERO
       TOB_FIFO_sw_rst      => TOB_FIFO_sw_rst_i,       -- i/p TOB Readout FIFO reset Pulse under s/w control
       -- XTOBs e/g 64b * 8 as input data is 32, we have 32b * 16
       XTOB_eg_512b_in       => XTOB_eg_512b_in_i ,         -- i/p array 8 * 64b TOBs e/g        
       XTOB_eg_Valid_flg_in  => XTOB_eg_Valid_flg_in_i ,    -- i/p 8b TOB e/g has valid data 
       XTOB_eg_sync_in       => XTOB_eg_sync_in_i ,         -- i/p TOB e/g  sync signal  
       -- XTOBs tau 64b * 8    
       XTOB_tau_512b_in      => XTOB_tau_512b_in_i ,        -- i/p array 8 * 64b  TOBs tau          
       XTOB_tau_Valid_flg_in => XTOB_tau_Valid_flg_in_i ,   -- i/p 8b TOB tau has valid data 
       XTOB_tau_sync_in      => XTOB_tau_sync_in_i ,        -- i/p TOB tau  sync signal
       OUT_XTOB_BCN          => OUT_XTOB_BCN_i,     -- sorted XTOB BCN with delay through ALGO/sorting block     
       -- TOBs data readout signals
       -- F1 reads e/g TOBs and F2 reads tau TOBs
       -- so same firmware in both FPGAs, use hw addr to differentiate
       TOBs_32b_in          =>  T_TOB_32b_in_i,        -- i/p sorted TOBs e/g 32b * 7 is series
       TOBS_sync_in         =>  T_TOB_sync_in_i,       -- i/p sorted TOB e/g start signal
       TOBs_valid_flg_in    =>  T_TOB_valid_in_i,         -- i/p sorted TOB e/g write signal
       OUT_TOB_BCN          => OUT_TOB_BCN_i,      -- sorted TOB BC_ID with delay through ALGO/sorting block
       TOB_type_in          => TOB_type_in ,
       TOB_err_4b_in        => TOB_err_4b_in_i ,   -- i/p  4b error flags CT + BN2 + TIE + EIE
       clk_40M_in           => clk_40M_in   ,       -- i/p 
       clk_200M_in          => clk_200M_in   ,       -- i/p 
       clk_280M_in          => clk_280M_in  ,       -- i/p 
       ipb_clk              => ipb_clk  ,           -- i/p 
       TOB_TXOUTCLK         => TOB_TXOUTCLK ,       -- i/p
       L1A_in               => L1A_in_ORed  ,        -- input from TTC_L1A or SW_L1A
       BCN_ID_in            => local_BCN_i,            -- i/p
       L1A_ID_in            => L1A_ID_int,            -- i/p 8b Ext L1A ID from TTC input + 24b L1A ID of counter
       ctrl_TOB_ready_in    => ctrl_TOB_ready_i ,   -- i/p
--       rdout_expected       => req_err_rd_raw_i ,   -- i/p Error Readout Expected
       
       TOB_out_to_MGT_is_char => TOB_out_is_char_i ,  -- o/p
       TOB_out_to_MGT         => TOB_out_i ,          -- o/p
       L1A_ID_Event_out       => L1A_ID_Event_i,     -- o/p 8b L1A ID Extended + 24b L1A ID of the current event       
       
       pre_ld_TOB_wr_addr       => pre_ld_TOB_wr_addr_i  ,    -- i/p latency pre load for TOB DRP wr address 
       pre_ld_eg_XTOB_wr_addr   => pre_ld_XTOB_eg_wr_addr_i  ,    -- i/p latency pre load for e/g XTOB DRP wr address 
       pre_ld_tau_XTOB_wr_addr  => pre_ld_XTOB_tau_wr_addr_i  ,    -- i/p latency pre load for tau XTOB DRP wr address 
       cntr_load_en           => '0',      -- was cntr_load_en_i ,   
       DPR_locations_to_rd    => DPR_locations_to_rd_i ,    -- i/p number of DRP locations to read 1 to 5
       TOB_data_FIFO_flags    => TOB_data_FIFO_flags_i ,    -- TOB block FIFO Flags
       -- XTOBs
       XTOB_eg_FIFO_data_count           =>  XTOB_eg_FIFO_data_count_i,    -- o/p                     
       XTOB_eg_FIFO_pFULL_THRESH_assert  =>  XTOB_eg_FIFO_pFULL_THRESH_assert_i , -- i/p add correct register
       XTOB_eg_FIFO_pFULL_THRESH_negate  =>  XTOB_eg_FIFO_pFULL_THRESH_negate_i , -- i/p
       
       XTOB_tau_FIFO_data_count          =>  XTOB_tau_FIFO_data_count_i,         -- o/p
       XTOB_tau_FIFO_pFULL_THRESH_assert =>  XTOB_tau_FIFO_pFULL_THRESH_assert_i, -- i/p
       XTOB_tau_FIFO_pFULL_THRESH_negate =>  XTOB_tau_FIFO_pFULL_THRESH_negate_i, -- i/p
       -- sorted TOBs                                
       T_TOB_FIFO_data_count             =>  TOB_FIFO_data_count_i,           -- o/p
       T_TOBs_FIFO_pFULL_THRESH_assert   =>  TOB_FIFO_pFULL_THRESH_assert_i,   -- i/p
       T_TOBs_FIFO_pFULL_THRESH_negate   =>  TOB_FIFO_pFULL_THRESH_negate_i,   -- i/p
              
       Link_output_FIFO_pFULL_THRESH_assert  =>  TOB_Link_outpout_FIFO_pFULL_THRESH_assert_i ,   -- i/p
       Link_output_FIFO_pFULL_THRESH_negate  =>  TOB_Link_outpout_FIFO_pFULL_THRESH_negate_i ,   -- i/p
       Link_output_FIFO_rd_data_count        =>  TOB_Link_outpout_FIFO_rd_data_count_i,  -- o/p
       SPY_TOB_mem_wr_addr    =>  SPY_TOB_mem_wr_addr_i    , -- TOB/XTOB data SPY memory wr addr pointer 
	   ipbus_out_tob_dpram    =>  ipbus_out_tob_dpram_i    , -- o/p signal going to TOB SPY DPRAM
       ipbus_in_tob_dpram     =>  ipbus_in_tob_dpram_i      -- i/p signal coming from TOB SPY DPRAM
       );

    raw_rd_all_i <= TEST_CONTROL_REG_i(1) ;                  -- software/priviledge readout all raw data
    
    L1A_ID_int <= L1A_ID_EXT_ii & L1A_ID_i;  -- i/p 8b Ext L1A ID from TTC input + 24b L1A ID of counter

-- RAW_data_Readout Module is responsible for reading the RAW calorimeter data from outputs synchronisation block. 
U1_RAW_readout : entity TOB_rdout_lib.RAW_data_rdout
    generic map(FPGA_NUMBER => FPGA_NUMBER)
    Port map ( 
       RST                 => RST_i,
       hw_addr             => hw_addr ,    -- i/p
       RST_spy_mem_wr_addr => SPY_RAM_wr_addr_rst_i ,   -- i/p SPY memory read address reset to ZERO
       RAW_FIFO_sw_rst     => RAW_FIFO_sw_rst_i ,       -- i/p this is RAW Readout FIFO reset Pulse under s/w control
       RAW_data_in         => RAW_data_in  ,          -- i/p array 49 x 227b input frames (7 x 32b)
       clk_280M_in         => clk_280M_in  ,
       clk_40M_in          => clk_40M_in   ,
       ipb_clk             => ipb_clk  ,            -- i/p 
       ctrl_RAW_ready_in   => ctrl_RAW_ready_i ,    -- i/p
       RAW_TXOUTCLK        => RAW_TXOUTCLK ,
       L1A_in              => L1A_in_ORed ,          -- input from TTC_L1A or SW_L1A
       BCN_ID_in           => local_BCN_i ,
       L1A_ID_in           => L1A_ID_int ,  -- i/p 8b Ext L1A ID from TTC input + 24b L1A ID of counter
--       link_error_flags_in    => link_error_flags_i ,         -- array 49 x 4 bit error per link
       raw_rd_all_in          => raw_rd_all_i ,               -- i/p  readout all raw data links
       pre_ld_wr_addr         => pre_ld_RAW_wr_addr_i,        -- The write address offset pre load for Circular DRPAM-- latency pre load for DRP wr address
       RAW_FIFO_FULL_THRESH_ASSERT   => RAW_FIFO_FULL_THRESH_ASSERT_i,
	   RAW_FIFO_FULL_THRESH_NEGATE   => RAW_FIFO_FULL_THRESH_NEGATE_i,
	   RAW_FIFO_data_count           => RAW_FIFO_data_count_i,
       cntr_load_en           => '0',        -- was cntr_load_en_i ,
       RAW_data_FIFO_flags    => RAW_data_FIFO_flags_i ,       -- RAW data FIFO flags
       RAW_out_to_MGT_is_char       => RAW_out_is_char_i ,     -- Raw data valid to MGT
       RAW_data_out                 => RAW_data_out_i  ,       -- raw data 32b data to MGT
       frame_count                  => RAW_frame_count_i ,
       TTC_read_all_in              => TTC_read_all_1dly,     -- TTC_Privilege Read signal input (previledge read)
       RAW_FIFO_pFULL_THRESH_ASSERT  => RAW_FIFO_pFULL_THRESH_ASSERT_i,   -- i/p
       RAW_FIFO_pFULL_THRESH_NEGATE  => RAW_FIFO_pFULL_THRESH_NEGATE_i,   -- i/p
       
	   BCN_FIFO_pFULL_THRESH_assert	=>	BCN_FIFO_pFULL_THRESH_assert_i ,		-- i/p
       BCN_FIFO_pFULL_THRESH_negate	=>  BCN_FIFO_pFULL_THRESH_negate_i ,  	-- i/p
       
       Link_output_FIFO_RAW_rd_data_count        =>	Link_output_FIFO_RAW_rd_data_count_i       ,	-- i/p     
       Link_output_FIFO_RAW_pfull_thresh_assert   =>	Link_output_FIFO_RAW_pfull_thresh_assert_i  ,
       Link_output_FIFO_RAW_pfull_thresh_negate   => Link_output_FIFO_RAW_pfull_thresh_negate_i ,  

       SPY_mem_wr_addr   =>  SPY_mem_wr_addr_i    ,    -- SPY memory wr_addr (read only)
	   ipbus_out_raw_dpram    =>  ipbus_out_raw_dpram_i ,    -- (o/p) signal from RAW SPY DPRAM
       ipbus_in_raw_dpram     =>  ipbus_in_raw_dpram_i,      -- (i/p) signal to RAW SPY DPRAM
       link_error_flags       =>  FIFO_error_flags_54b_i     -- (o/p) error flags from the ERROR Flag FIFO
       );

-- Readout_ipb_slave Module is responsible for communication with IPBus.
-- it has an interface to IPBus module, an interface to RAW Data IPBus Slave and an interface to TOB Data IPBus Slave.   
U4_rdout_ipb_slave : entity TOB_rdout_lib.readout_ipb_slave
    Port map ( 
        ipb_rst     => ipb_rst  ,   -- i/p
        ipb_clk     => ipb_clk  ,   -- i/p
        IPb_in      => IPb_in  ,    -- i/p
        IPb_out     => IPb_out ,    -- o/p
        L1A_ID_Event  => L1A_ID_Event_i ,   -- i/p 32b L1A ID of current event
        L1A_ID        => L1A_ID_int ,     -- i/p 8b Ext L1A ID from TTC input + 24b L1A ID of counter
        BCN_in        => local_BCN_i ,    -- i/p
        -- TOB/XTOB           
        TOB_WR_ADDR_OFFSET               =>  pre_ld_TOB_wr_addr_i,          -- o/p
        XTOB_EG_WR_ADDR_OFFSET           =>  pre_ld_XTOB_eg_wr_addr_i,          -- o/p
        XTOB_TAU_WR_ADDR_OFFSET          =>  pre_ld_XTOB_tau_wr_addr_i,          -- o/p
        TOB_SLICES_TO_RD                 =>  DPR_locations_to_rd_i,         -- o/p
        TOB_FIFO_pFULL_THRESH_ASSERT      =>  TOB_FIFO_pFULL_THRESH_assert_i, -- o/p
        TOB_FIFO_pFULL_THRESH_NEGATE      =>  TOB_FIFO_pFULL_THRESH_negate_i, -- o/p
        TOB_FIFO_DATA_COUNT               =>  TOB_FIFO_data_count_i,         -- i/p 
        XTOB_EG_FIFO_pFULL_THRESH_ASSERT  =>  XTOB_eg_FIFO_pFULL_THRESH_assert_i, -- o/p
        XTOB_EG_FIFO_pFULL_THRESH_NEGATE  =>  XTOB_eg_FIFO_pFULL_THRESH_negate_i, -- o/p
        XTOB_EG_FIFO_DATA_COUNT           =>  XTOB_eg_FIFO_data_count_i,         -- i/p
        XTOB_TAU_FIFO_pFULL_THRESH_ASSERT =>  XTOB_tau_FIFO_pFULL_THRESH_assert_i,    -- o/p
        XTOB_TAU_FIFO_pFULL_THRESH_NEGATE =>  XTOB_tau_FIFO_pFULL_THRESH_negate_i,    -- o/p
        XTOB_TAU_FIFO_DATA_COUNT          =>  XTOB_tau_FIFO_data_count_i,            -- i/p
        TOB_Link_output_FIFO_pFULL_THRESH_ASSERT      =>  TOB_Link_outpout_FIFO_pFULL_THRESH_assert_i,-- o/p
        TOB_Link_output_FIFO_pFULL_THRESH_NEGATE      =>  TOB_Link_outpout_FIFO_pFULL_THRESH_negate_i,-- o/p
        TOB_LINK_OUTPUT_FIFO_RD_DATA_COUNT           =>  TOB_Link_outpout_FIFO_rd_data_count_i,     -- i/p
        TOB_data_FIFO_flags             => TOB_data_FIFO_flags_i ,      -- i/p 12 bits
        
        -- RAW calorimeter data                      =>  ,
        RAW_WR_ADDR_OFFSET           =>  pre_ld_RAW_wr_addr_i,          -- o/p
        RAW_FIFO_pFULL_THRESH_ASSERT  =>  RAW_FIFO_pFULL_THRESH_ASSERT_i,    -- o/p
        RAW_FIFO_pFULL_THRESH_NEGATE  =>  RAW_FIFO_pFULL_THRESH_NEGATE_i,    -- o/p
        
	    BCN_FIFO_pFULL_THRESH_assert	=>	BCN_FIFO_pFULL_THRESH_assert_i ,   -- o/p
        BCN_FIFO_pFULL_THRESH_negate	=>  BCN_FIFO_pFULL_THRESH_negate_i ,   -- o/p
        Link_output_FIFO_RAW_pfull_thresh_assert   =>  Link_output_FIFO_RAW_pfull_thresh_assert_i  ,    -- o/p
        Link_output_FIFO_RAW_pfull_thresh_negate   =>  Link_output_FIFO_RAW_pfull_thresh_negate_i  ,    -- o/p
        Link_output_FIFO_RAW_rd_data_count        =>  Link_output_FIFO_RAW_rd_data_count_i       ,  -- i/p
        RAW_FIFO_FULL_THRESH_ASSERT   => RAW_FIFO_FULL_THRESH_ASSERT_i,   -- o/p
	    RAW_FIFO_FULL_THRESH_NEGATE   => RAW_FIFO_FULL_THRESH_NEGATE_i,   -- o/p
	    RAW_FIFO_data_count           => RAW_FIFO_data_count_i,   -- i/p
        RAW_frame_count        =>  RAW_frame_count_i ,
        RAW_data_FIFO_flags    =>  RAW_data_FIFO_flags_i ,    -- (i/p) 9 bits
        SPY_TOB_mem_wr_addr    =>  SPY_TOB_mem_wr_addr_i ,    -- (i/p) TOB/XTOB data SPY memory wr addr pointer 
	    ipbus_out_tob_dpram    =>  ipbus_in_tob_dpram_i ,     -- (o/p) signal to TOB SPY DPRAM
        ipbus_in_tob_dpram     =>  ipbus_out_tob_dpram_i ,    -- (i/p) signal from TOB SPY DPRAM
        RDOUT_PULSE_REG        =>  RDOUT_PULSE_REG_i ,        -- (o/p) pulsed register
        SPY_RAW_mem_wr_addr    =>  SPY_mem_wr_addr_i    ,     -- (o/p) RAW calo data SPY memory wr addr pointer 
	    ipbus_out_raw_dpram    =>  ipbus_in_raw_dpram_i ,     -- (o/p) signal to RAW SPY DPRAM
        ipbus_in_raw_dpram     =>  ipbus_out_raw_dpram_i ,    -- (i/p) signal from RAW SPY DPRAM
        TEST_CONTROL_REG	   =>  TEST_CONTROL_REG_i, 	      -- (o/p) control register for testing purposes
        link_error_flags       =>  FIFO_error_flags_54b_i     -- (i/p) error flags from the ERROR Flag FIFO
        
        );

-- U5_bcn_counter process is responsible for generating BCN = Bunch Crossing Number.  
-- The orbit is 3564 long, from 0 to 3563.  
-- Cunch Crossing counter is set to 1 upon receiving TTC BCR  
U5_bcn_counter : process(clk_40M_in)
    variable bcn_count : unsigned (11 downto 0) := (others => '0');
  begin

    if rising_edge (clk_40M_in) then
    -- register TTC input signals to keep them in synch
      L1A_ID_EXT_i  <= TTC_L1A_ID_EXT_in ;
      BCR_1dly      <= BCR_in;  -- delay by another clk to match the delay on L1A_i
      ECR_1dly      <= ECR_in; -- delay by 1 clk to match L1A delay  
      TTC_read_all_1dly <= TTC_read_all_in; -- delay by 1 clk to match L1A delay

      if BCR_1dly = '1' then
        bcn_count := ("000000000001");
      else
        if bcn_count = 3563 then
          bcn_count := (others => '0');
        else
          bcn_count := bcn_count + 1;
        end if;
      end if;
      local_BCN_i <= std_logic_vector(bcn_count);
    end if;
  end process;

    L1A_cntr_rst_tmp <= ECR_1dly OR L1A_cntr_rst_i; -- L1A_ID rst by s/w or ECR

-- L1A Counter Module is responsible for generating L1A_ID.  
-- The counter counts when it receives either a TTC L1A or a Software L1A.  
-- This counter is reset by either TTC ECR or Software L1A Reset.  
-- Software L1A Reset is selected when bit 1 of resister rdout_pulse_reg is set to 1.  
-- The rdout_pulse_reg register is pulsed, and after 1 clock cycle the contents are reset to all Zeros.  
U7_L1A_cntr : entity TOB_rdout_lib.cntr_L1A_generic
    Port map (
       CE   =>  L1A_in_ORed ,  -- TTC L1A or s/w L1A 
       CLK  =>  clk_40M_in ,
       RST  =>  L1A_cntr_rst_tmp,
       Q    =>  L1A_ID_i        -- o/p
       );

-- L1A_gen_sw_tst process is responsible to generate L1A pulse under sofware control.  
-- L1A is generated by setting bit 0 of register rdout_pulse_reg to 1.  
-- The rdout_pulse_reg register is pulsed, and after 1 clock cycle the contents are reset to all Zeros.  
-- Software L1A is selected when bit 0 of register Test_Control_Reg is set to ZERO  
-- To use TTC L1A input signal bit 0 of register Test_Control_Reg must be set to 1  
U5_L1A_gen_sw_tst : process (clk_40M_in)
   begin
    if clk_40M_in'event and clk_40M_in = '1' then
            reg1    <= RDOUT_PULSE_REG_i(0) ;   -- this is Software L1A
            reg2    <= reg1 ;
            L1A_sw_pulse_i    <= reg1 AND (NOT reg2) ;
    end if;
  end process;
   
-- U6_MUXF7 process is responsible to OR the TTC L1A with Software L1A.  
-- This ensures the L1A counter functions correctly with both types of L1As.  
-- TTC L1A is selected when bit 0 of resister Test_Control_Reg is ONE  
-- Software L1A is selected when bit 0 of resister Test_Control_Reg is ZERO  
U6_MUXF7 : process (clk_40M_in) -- clk reg the MUX to remove timing (L1A is seen as 8 clks instead of 7 clks)
    begin
        if clk_40M_in'event and clk_40M_in = '0' then   -- sample on FALLING EDGE clock
            L1A_ID_EXT_ii <= L1A_ID_EXT_i ;         -- delay L1A_ID_EXT by 1 tick to match delay on L1A input
            if TEST_CONTROL_REG_i(0) = '1' then     -- this is 2nd tick delay on L1A
                L1A_in_ORed <= L1A_in ;    -- TTC L1A (only enabled when selected)
            else
                L1A_in_ORed <= L1A_sw_pulse_i;  -- (default) Test s/w L1A
            end if;
        end if;
    end process;
	
-- sw_rst_L1A_id process generates a Software Reset to  L1A_ID counter.  
-- Software L1A Reset is selected when bit 1 of resister rdout_pulse_reg is set 1o 1  
-- The rdout_pulse_reg register is pulsed, and after 1 clock cycle the contents are reset to all Zeros.  
U8_sw_rst_L1A_id : process (clk_40M_in)
    begin
        if clk_40M_in'event and clk_40M_in = '1' then
            reg5    <= RDOUT_PULSE_REG_i(1) ;   -- this is L1A ID Software Reset
            reg6    <= reg5 ;
            L1A_cntr_rst_i    <= reg5 AND (NOT reg6) ;
        end if;
    end process;

-- RAW_FIFO_sw_rst process generates a Software Reset to 49 RAW FIFO blocks which store the input calorimeter data.  
-- RAW FIFO Software Reset is selected when bit 2 of resister rdout_pulse_reg is set 1o 1  
-- The rdout_pulse_reg register is pulsed, and after 1 clock cycle the contents are reset to all Zeros.  
U9_RAW_FIFO_sw_rst : process (clk_40M_in)
    begin
        if clk_40M_in'event and clk_40M_in = '1' then
            reg7    <= RDOUT_PULSE_REG_i(2) ;   -- this is RAW Data FIFO Software Reset
            reg8    <= reg7 ;
            RAW_FIFO_sw_rst_i    <= reg7 AND (NOT reg8) ;
        end if;
    end process;

-- TOB_FIFO_sw_rst process generates a Software Reset to 3 FIFO blocks which store TOB and XTOB data.  
-- TOB FIFO Software Reset is selected when bit 3 of resister rdout_pulse_reg is set 1o 1  
-- The rdout_pulse_reg register is pulsed, and after 1 clock cycle the contents are reset to all Zeros.  
U10_TOB_FIFO_sw_rst : process (clk_40M_in)
    begin
        if clk_40M_in'event and clk_40M_in = '1' then
            reg9    <= RDOUT_PULSE_REG_i(3) ;   -- this is TOB/XTOB Data FIFO Software Reset
            reg10    <= reg9 ;
            TOB_FIFO_sw_rst_i    <= reg9 AND (NOT reg10) ;
        end if;
    end process;

-- BC_cntr_load_en process generates a Software Load signal to load a value into Bunch Crossing Counter.  
-- BC_cntr_load_en is selected when bit 4 of resister rdout_pulse_reg is set 1o 1  
-- The rdout_pulse_reg register is pulsed, and after 1 clock cycle the contents are reset to all Zeros.  
U11_BC_cntr_load_en : process (clk_40M_in)
    begin
        if clk_40M_in'event and clk_40M_in = '1' then
            reg3    <= RDOUT_PULSE_REG_i(4) ;   -- this is BC counter load enable signal
            reg4    <= reg3 ;
            BC_cntr_load_en_i    <= reg3 AND (NOT reg4) ;
        end if;
    end process;
    
-- SPY_RAM_wr_addr_rst process generates a Software Reset to reset the write address of Spy Memoreis to ZERO.  
-- This reset is applied to both RAW and TOB/XTOB Spy Memories.  
-- SPY_RAM_wr_addr_rst is selected when bit 5 of resister rdout_pulse_reg is set 1o 1  
-- The rdout_pulse_reg register is pulsed, and after 1 clock cycle the contents are reset to all Zeros.  
U12_SPY_RAM_wr_addr_rst : process (clk_40M_in)
    begin
        if clk_40M_in'event and clk_40M_in = '1' then
            reg11    <= RDOUT_PULSE_REG_i(5) ;   -- this is SPY Memory Write Address Software Reset
            reg12    <= reg11 ;
            SPY_RAM_wr_addr_rst_i    <= reg11 AND (NOT reg12) ;
        end if;
    end process;
        
end Behavioral;
