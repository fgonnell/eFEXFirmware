-- 07/05/2018
-- Created by rjs

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY crc_add IS
    generic(
    REVERSE_BIT_ORDER : boolean := false);
   PORT( 
      clock      : IN     std_logic;
      crc_en     : IN     std_logic;
      crc_in     : IN     std_logic_vector(8 downto 0);
      data_in    : IN     std_logic_vector(31 downto 0);
      data_out   : OUT    std_logic_vector(31 downto 0) := (others => '0')
   );

END crc_add ;

------------------------------------
ARCHITECTURE rtl OF crc_add IS


   SIGNAL retimed_data  : std_logic_vector(31 downto 0) := (others => '0');
   SIGNAL data_int    : std_logic_vector(31 downto 0) := (others => '0');
   signal insert_crc : std_logic := '0';
   
   constant DELAY: time := 2 ns;

  
BEGIN

	process(clock,data_in)
	begin
	if rising_edge(clock) then
		retimed_data <= data_in;
		insert_crc <= crc_en;
	end if;
	end process;

    if_rbo_g :
    if REVERSE_BIT_ORDER generate
        data_int <= crc_in & retimed_data(22 downto 0) when insert_crc = '1' else retimed_data;
    end generate;
                
    if_not_rbo_g :
    if not REVERSE_BIT_ORDER generate
        data_int <=  retimed_data(22 downto 0) & crc_in when insert_crc = '1' else retimed_data;
    end generate;
                
    data_out <= data_int;-- after DELAY;
    
END rtl;


