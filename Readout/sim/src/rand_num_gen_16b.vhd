--! @file
--! @brief Random number generator
--! @details 
--! This file use a 32b number to generate 16b random number,
--! these random numbers are used with idle CHAR code to balance the transmission line.
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--! @copydoc ran_num_gen_16b.vhd
entity ran_num_gen_16b is
  Port (
        rst         : in std_logic ;
        clk         : in std_logic ;
        MAC_addr    : in std_logic_vector (31 downto 0) ;
        rand_out    : out std_logic_vector (15 downto 0) 
        );
end ran_num_gen_16b;

--! @copydoc ran_num_gen_16b.vhd
architecture Behavioral of ran_num_gen_16b is
    
   begin

    U0_random : process(clk)
    -- xorshift rng based on http://b2d-f9r.blogspot.co.uk/2010/08/16-bit-xorshift-rng-now-with-more.html
    -- using triplet 5, 3, 1 (next 5, 7, 4)
        variable x, y, t : std_logic_vector(15 downto 0);
      begin
        if rising_edge(clk) then
          if rst = '1' then
            x := MAC_addr(31 downto 16);
            y := MAC_addr(15 downto 0);
          else
            t := x xor (x(10 downto 0) & "00000");
            x := y;
            y := (y xor ("0" & y(15 downto 1))) xor (t xor ("000" & t(15 downto 3)));
          end if;
          rand_out <= y  ;  --  after 100 ps
        end if;
  end process;

end Behavioral;
