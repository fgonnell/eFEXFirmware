-- MGT buffer block to readout
--
-- Dave Sankey, Sep 2019

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

LIBRARY ipbus_lib;
USE ipbus_lib.ipbus.all;
LIBRARY infrastructure_lib;
use infrastructure_lib.packet_mux_type.all;

ENTITY mgt_buffer IS
  GENERIC(
	RAM_ADDR_WIDTH		: positive := 12;
	INPUT_FPGA_NO		: std_logic_vector(1 downto 0) := "00";
 	DATA_FORMAT_VERSION	: std_logic_vector (2 DOWNTO 0) := "001"
  );
  port (
-- Static signals
	eFEX_number			: IN std_logic_vector(7 downto 0);
 	Stream_Type			: IN std_logic_vector (1 DOWNTO 0) := "00"; -- "00" TOB, "10" Input Data, "11" Debug
-- data from MGT
	clk_mgt				: in std_logic;
	data_from_mgt		: in std_logic_vector(31 downto 0);
	char_is_k			: in std_logic;
	error_from_mgt		: in std_logic;
	free_space			: out std_logic_vector(RAM_ADDR_WIDTH downto 0);
	fifo_full			: OUT std_logic;
-- data to packet builder
	clk_320				: in std_logic;
	rst_320				: in std_logic;
	packet_data			: OUT std_logic_vector (63 DOWNTO 0);
	packet_valid		: OUT std_logic;
   	packet_hdr			: OUT std_logic;
	packet_data_end		: OUT std_logic;
	packet_ready		: IN std_logic;
-- interface to IPBus
	clk_ipb				: in std_logic;
	rst_ipb				: in std_logic;
	ipb_in				: in ipb_wbus;
	ipb_out				: out ipb_rbus
  );
END ENTITY mgt_buffer;

Architecture rtl of mgt_buffer is

COMPONENT ila_0
	PORT (
		clk : IN STD_LOGIC;
		probe0 : IN STD_LOGIC_VECTOR(67 DOWNTO 0)
	);
END COMPONENT ila_0;

Component data_packet is
  generic (
 	ADDR_WIDTH  : integer :=   16;	-- width of ram address bus (maximum value is 16)
 	FORMAT_VERSION: std_logic_vector (2 DOWNTO 0) := "001"
 	);
  port (
	clk					: in std_logic;
	rst_clk				: in std_logic;
-- Status signals
	eFEX_number			: IN std_logic_vector(7 downto 0);
 	Stream_ID			: IN std_logic_vector (7 DOWNTO 0) := x"81";
-- FIFO signals
-- Format of FIFO is
-- fifo_dout(71 downto 56): starting 64 bit address of payload data in RAM
-- fifo_dout(55 downto 48): number of 64 bit words for payload data excluding trailer
-- fifo_dout(47 downto 44): local error flags excluding header mismatch and processor timeout
-- fifo_dout(43 downto 12): extended L1ID
-- fifo_dout(11 downto 0):  BCN
    fifo_rd_en			: OUT STD_LOGIC;
    fifo_dout			: IN STD_LOGIC_VECTOR(71 DOWNTO 0);
    fifo_empty			: IN STD_LOGIC;
-- RAM signals
    RAM_data			: IN std_logic_vector (63 DOWNTO 0);
    RAM_addr			: OUT std_logic_vector(ADDR_WIDTH - 1 downto 0);
    RAM_din				: OUT std_logic_vector (63 DOWNTO 0);
    RAM_we				: OUT STD_LOGIC;
-- towards packet_builder readout
	packet_data         : OUT std_logic_vector (63 DOWNTO 0);
	packet_valid        : OUT std_logic;
   	packet_hdr          : OUT std_logic;
	packet_data_end     : OUT std_logic;
	packet_ready		: IN std_logic
  );
end Component data_packet;

Component mgt_readout_receiver IS
  GENERIC(
	ADDR_WIDTH: positive := 16;
	FPGA_NO: std_logic_vector(1 downto 0) := "00"
  );
  port(
	clk_mgt: in std_logic;
	rst_mgt: in std_logic;
-- data from MGT
	data_from_mgt: in std_logic_vector(31 downto 0);
	char_is_k: in std_logic;
	error_from_mgt: in std_logic;
-- data to DPRAM
	MGT_RAM_addr: out std_logic_vector(ADDR_WIDTH downto 0);
	MGT_RAM_we: out std_logic := '0';
	MGT_RAM_din: out std_logic_vector(31 downto 0);
	MGT_RAM_data: in std_logic_vector(31 downto 0);
-- data to IPBus RAM
	IPBus_RAM_addr: out std_logic_vector(9 downto 0);
	IPBus_RAM_din: out std_logic_vector(31 DOWNTO 0);
	IPBus_RAM_we: out std_logic;
-- metadata to FIFO etc.
	fifo_data: out std_logic_vector(71 downto 0);
	fifo_we: out std_logic;
	free_space: out std_logic_vector(ADDR_WIDTH downto 0)
  );
END Component mgt_readout_receiver;

Component dpram32_64 is
	generic(
		ADDR_WIDTH: positive := 12
	);
	port(
		clk32: in std_logic;
		we32: in std_logic := '0';
		d32: in std_logic_vector(31 downto 0);
		q32: out std_logic_vector(31 downto 0);
		addr32: in std_logic_vector(ADDR_WIDTH downto 0);
		clk64: in std_logic;
		we64: in std_logic := '0';
		d64: in std_logic_vector(63 downto 0);
		q64: out std_logic_vector(63 downto 0);
		addr64: in std_logic_vector(ADDR_WIDTH - 1 downto 0)
	);
end Component dpram32_64;

Component MGT_metadata_fifo is
	Port (
		rst : in STD_LOGIC;
		wr_clk : in STD_LOGIC;
		rd_clk : in STD_LOGIC;
		din : in STD_LOGIC_VECTOR ( 71 downto 0 );
		wr_en : in STD_LOGIC;
		rd_en : in STD_LOGIC;
		dout : out STD_LOGIC_VECTOR ( 71 downto 0 );
		full : out STD_LOGIC;
		empty : out STD_LOGIC
	);
end Component MGT_metadata_fifo;

Component ipbus_ported_dpram is
	generic(
		ADDR_WIDTH: positive;
		DATA_WIDTH: positive := 32
	);
	port(
		clk: in std_logic;
		rst: in std_logic;
		ipb_in: in ipb_wbus;
		ipb_out: out ipb_rbus;
		rclk: in std_logic;
		we: in std_logic := '0';
		d: in std_logic_vector(DATA_WIDTH - 1 downto 0) := (others => '0');
		q: out std_logic_vector(DATA_WIDTH - 1 downto 0);
		addr: in std_logic_vector(ADDR_WIDTH - 1 downto 0)
	);
end Component ipbus_ported_dpram;

   SIGNAL Stream_ID			: std_logic_vector(7 DOWNTO 0);

   SIGNAL RAM_data			: std_logic_vector(63 DOWNTO 0);
   SIGNAL RAM_addr			: std_logic_vector(RAM_ADDR_WIDTH - 1 downto 0);
   SIGNAL RAM_din			: std_logic_vector(63 DOWNTO 0);
   SIGNAL RAM_we			: std_logic;

   SIGNAL MGT_RAM_data		: std_logic_vector(31 DOWNTO 0);
   SIGNAL MGT_RAM_addr		: std_logic_vector(RAM_ADDR_WIDTH downto 0);
   SIGNAL MGT_RAM_din		: std_logic_vector(31 DOWNTO 0);
   SIGNAL MGT_RAM_we		: std_logic;

   SIGNAL mgt_ttc_data     	: std_logic_vector(71 DOWNTO 0);
   SIGNAL mgt_ttc_we		: std_logic;
   SIGNAL mgt_ttc_dout     	: std_logic_vector(71 DOWNTO 0);
   SIGNAL mgt_ttc_fifo_empty: std_logic;
   SIGNAL mgt_ttc_rd_en		: std_logic;

   SIGNAL IPBus_RAM_addr	: std_logic_vector(9 downto 0);
   SIGNAL IPBus_RAM_din		: std_logic_vector(31 DOWNTO 0);
   SIGNAL IPBus_RAM_we		: std_logic;

   SIGNAL rst_mgt_sig		: std_logic;

Begin

   Stream_ID <= x"01" when (Stream_Type = "00") else Stream_Type & INPUT_FPGA_NO & eFEX_number(3 downto 0);
   fifo_full <= mgt_ttc_we; -- kludge to bring out mgt_ttc_we

   Data_Source : data_packet
      GENERIC MAP (
         ADDR_WIDTH    => RAM_ADDR_WIDTH,
         FORMAT_VERSION=> DATA_FORMAT_VERSION
      )
      PORT MAP (
         clk            => clk_320,
         rst_clk        => rst_320,
         eFEX_number    => eFEX_number,
         Stream_ID		=> Stream_ID,
         fifo_rd_en     => mgt_ttc_rd_en,
         fifo_dout      => mgt_ttc_dout,
         fifo_empty     => mgt_ttc_fifo_empty,
    	 RAM_data		=> RAM_data,
    	 RAM_addr		=> RAM_addr,
    	 RAM_din		=> RAM_din,
    	 RAM_we			=> RAM_we,
         packet_data    => packet_data,
         packet_valid   => packet_valid,
         packet_hdr     => packet_hdr,
         packet_data_end=> packet_data_end,
         packet_ready   => packet_ready
      );

	MGT_receiver : mgt_readout_receiver
	  GENERIC MAP (
		ADDR_WIDTH      => RAM_ADDR_WIDTH,
		FPGA_NO			=> INPUT_FPGA_NO
      )
      PORT MAP (
		clk_mgt 		=> clk_mgt,
		rst_mgt			=> rst_mgt_sig,
	-- data from MGT
		data_from_mgt	=> data_from_mgt,
		char_is_k		=> char_is_k,
		error_from_mgt	=> error_from_mgt,
	-- data to DPRAM
		MGT_RAM_addr	=> MGT_RAM_addr,
		MGT_RAM_we		=> MGT_RAM_we,
		MGT_RAM_din		=> MGT_RAM_din,
		MGT_RAM_data	=> MGT_RAM_data,
-- data to IPBus RAM
		IPBus_RAM_addr	=> IPBus_RAM_addr,
		IPBus_RAM_din	=> IPBus_RAM_din,
		IPBus_RAM_we	=> IPBus_RAM_we,
	-- metadata to FIFO etc.
		fifo_data		=> mgt_ttc_data,
		fifo_we			=> mgt_ttc_we,
		free_space		=> free_space
	  );

	Data_RAM : dpram32_64
	  GENERIC MAP (
		ADDR_WIDTH      => RAM_ADDR_WIDTH
      )
      PORT MAP (
		clk32			=> clk_mgt,
		we32			=> MGT_RAM_we,
		d32				=> MGT_RAM_din,
		q32				=> MGT_RAM_data,
		addr32			=> MGT_RAM_addr,
        clk64           => clk_320,
    	we64			=> RAM_we,
    	d64				=> RAM_din,
    	q64				=> RAM_data,
    	addr64			=> RAM_addr
	  );

	MGT_FIFO : MGT_metadata_fifo
      PORT MAP (
        rst				=> rst_mgt_sig,
		wr_clk			=> clk_mgt,
		din				=> mgt_ttc_data,
		wr_en			=> mgt_ttc_we,
		rd_clk			=> clk_320,
    	rd_en			=> mgt_ttc_rd_en,
    	dout			=> mgt_ttc_dout,
    	full			=> OPEN,
    	empty			=> mgt_ttc_fifo_empty
	  );

--	fifo_ila : ila_0
--		PORT MAP (
--			clk					=> clk_320,
--			probe0(66 downto 0)	=> mgt_ttc_dout(66 downto 0),
--			probe0(67)			=> mgt_ttc_fifo_empty
--		);

	IPbus_RAM : ipbus_ported_dpram
	  GENERIC MAP (
		ADDR_WIDTH		=> 10,
		DATA_WIDTH		=> 32
	  )
	  PORT MAP (
		clk				=> clk_ipb,
		rst				=> rst_ipb,
		ipb_in			=> ipb_in,
		ipb_out			=> ipb_out,
		rclk			=> clk_mgt,
		we				=> IPBus_RAM_we,
		d				=> IPBus_RAM_din,
		q				=> Open,
		addr			=> IPBus_RAM_addr
	  );

Reset_block: process(clk_mgt)
	Variable stretch: std_logic_vector(7 downto 0) := (Others => '1');
	Variable reset_done: std_logic := '0';
	begin
		if rising_edge(clk_mgt) then
			if reset_done = '0' then
				stretch := (Others => '1');
				reset_done := '1';
			end if;
			rst_mgt_sig <= stretch(7)
-- pragma translate_off
			after 2 ns
-- pragma translate_on
			;
			stretch := stretch(6 downto 0) & "0";
		end if;
	end process Reset_block;

END Architecture rtl;
