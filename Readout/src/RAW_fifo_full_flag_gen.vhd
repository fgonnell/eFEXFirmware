--! @file
--! @brief Generate Full Flag for RAW data de-randomisation FIFO
--! @details 
--! \verbatim
--! This Module monitors the occupancy level of one de-randomisation FIFO for channel 20.
--! Channel 20 has been chosen in order to be in the middle of the FPGA, but any other channel can be selected.
--! The programmable Assert and Negate registers control the occupancy levels at which the FULL Flag is asserted and negated.
--! Therefore it generates Full Flag for RAW data de-randomisation FIFO by monitoring the data count 
--! This flag has to be on a 7 word boundary (each input = 7 x 32-bit words), as for each L1A 7x32-bit words are transferred.
--! \endverbatim
--! @author Saeed Taghavi



library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


--! @copydoc RAW_fifo_full_flag_gen.vhd
entity RAW_fifo_full_flag_gen is
    Port (
       rst_in         : in STD_LOGIC;
       clk_in         : in STD_LOGIC;
	   --! RAW FIFO data count input
       fifo_data_count    : in STD_LOGIC_VECTOR (8 downto 0);
	   --! assert count input to set Full Flag 
       assert_count_in    : in STD_LOGIC_VECTOR (8 downto 0);
	   --! negate count input to set Full Flag 
       negate_count_in    : in STD_LOGIC_VECTOR (8 downto 0);
	   --! Full Flag for RAW input FIFO
       full_flag_out   : out STD_LOGIC    
       );
end RAW_fifo_full_flag_gen;

--! @copydoc RAW_fifo_full_flag_gen.vhd
architecture Behavioral of RAW_fifo_full_flag_gen is
    
begin
  
U0: process (clk_in)
    begin 
    if (clk_in'event and clk_in = '1') then
        if (rst_in = '1') then
            full_flag_out <= '0' ;      -- set flag ZERO
        else
			if (unsigned(fifo_data_count) >= unsigned(assert_count_in)) then  -- greater or equal to 
				full_flag_out <= '1' ;      -- set full flag
			else
				if (unsigned(fifo_data_count) <= unsigned(negate_count_in)) then -- smaller or equal to 
				   full_flag_out <= '0' ;      -- set flag ZERO
				end if;
			end if;
        end if;
    end if;
    end process;

end Behavioral;
