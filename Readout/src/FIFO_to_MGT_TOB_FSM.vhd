--! @file
--! @brief FSM to read TOB/XTOB data from Link Output FIFO for process FPGA
--! @details 
--! This module reads a complete TOB/XTOB event together with Header and Trailer and writes the entire event into the MGT
--! The frame counter is incremented/decremented as events are written/read from the Link_output FIFO
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.ALL;
use TOB_rdout_lib.data_type_pkg.all;

--! @copydoc FIFO_to_MGT_TOB_FSM.vhd
entity FIFO_to_MGT_TOB_FSM is
    Port ( 
--        RST                      : in STD_LOGIC;
        --! TOB Readout FIFO reset Pulse by software command ORed with SYS_RST
        TOB_FIFO_sw_rst          : in std_logic ;       
        clk_in_280M              : in STD_LOGIC;
       --! Ready signal from control FPGA to receive data from process FPGA
        ctrl_TOB_ready_in        : in  std_logic ;
        --! Frame counter register
        frame_counter            : in STD_LOGIC_VECTOR (11 downto 0);
        --! TOB/XTOBs from Link Output FIFO
        FIFO_MGT_TOBs            : in STD_LOGIC_VECTOR (32 downto 0);
        --! TOB/XTOBs from Link Output FIFO valid signal
        FIFO_MGT_TOB_valid       : in STD_LOGIC;
        --! Link Output FIFO FULL Flag
        MGT_FIFO_full            : in std_logic;
        --! Link Output FIFO Empty Flag
        MGT_FIFO_empty           : in std_logic;
        --! Link Output FIFO partial FULL Flag
        MGT_FIFO_prog_full       : in std_logic;
        --! Read enalbe singal to Link Output FIFO
        FIFO_MGT_rd_en           : out STD_LOGIC;
        --! Decrement Frame Counter
        frame_counter_dec_en     : out STD_LOGIC;
        --! 32b TOB/XTOB to connect to output MGT to control FPGA
        T_TOBs_out               : out STD_LOGIC_VECTOR (31 downto 0);
        --! TOB/XTOB is CHAR to connect to output MGT to control FPGA
        T_TOB_is_char            : out STD_LOGIC;
        --! TOB/XTOB is valid signal to wr to SPY memory
        T_TOB_out_valid          : out STD_LOGIC
        );
end FIFO_to_MGT_TOB_FSM;

--! @copydoc FIFO_to_MGT_TOB_FSM.vhd
architecture Behavioral of FIFO_to_MGT_TOB_FSM is

-- CHAR constants are defined in data_type_pkg.vhd
-- for reference only
--    constant ch_idle    : std_logic_vector(7 downto 0)  := X"BC"  ;    -- idle char is K28.5
--    constant ch_sop1    : std_logic_vector(7 downto 0)  := X"3C"  ;    -- star of packet char 1 is K28.1
--    constant ch_sop2    : std_logic_vector(7 downto 0)  := X"5C"  ;    -- star of packet char 2 is K28.2
--    constant ch_eop     : std_logic_vector(7 downto 0)  := X"DC"  ;    -- end  of packet char is K28.6

--************************** Register Declarations ****************************    
    signal  clk_in_280M_i       : STD_LOGIC;                  
                  
    signal  T_TOB_out_valid_i    : STD_LOGIC;
    signal  TOB_in_is_char_i    : STD_LOGIC;
    signal  TOBs_in_i           : STD_LOGIC_VECTOR (31 downto 0);
    signal  MGT_fifo_rd_en_i    : STD_LOGIC;
    signal  TOBs_out_i, TOBs_in_tmp_1dly                : STD_LOGIC_VECTOR (31 downto 0);
    signal  TOB_out_is_char_i, TOB_in_is_char_tmp_1dly  : STD_LOGIC;

    signal  frame_counter_dec_en_i       : STD_LOGIC;


    TYPE STATE_TYPE IS (
       idle, rd_fifo1, rd_fifo2, tx_data_SOF1, tx_data_SOF2, tx_data1, tx_data2, tx_data_EOF1, tx_data_EOF2, 
       wait1, wait2, wait3, wait4
       );
  
    SIGNAL current_state : STATE_TYPE;

begin
    -- input ports
    clk_in_280M_i        <= clk_in_280M ;
    
    -- output port
--    T_TOBs_out           <= TOBs_out_i;                  -- TOB data out to MGT
--    T_TOB_is_char        <= TOB_out_is_char_i ;          -- TOB data out to MGT is CHAR
--	T_TOB_out_valid      <= T_TOB_out_valid_i ;	         -- TOB data out is valid
    FIFO_MGT_rd_en       <= MGT_fifo_rd_en_i ;
    
    -- output ports
--    frame_counter_dec_en <= frame_counter_dec_en_i ;


U1_clk_proc : process (clk_in_280M_i)   -- to remove timing errors
    begin
        if clk_in_280M_i'event and clk_in_280M_i = '1' then
            -- output ports
            T_TOBs_out           <= TOBs_out_i;                  -- TOB data out to MGT
            T_TOB_is_char        <= TOB_out_is_char_i ;          -- TOB data out to MGT is CHAR
            T_TOB_out_valid      <= T_TOB_out_valid_i ;	         -- TOB data out is valid
--            FIFO_MGT_rd_en       <= MGT_fifo_rd_en_i ;
            
            frame_counter_dec_en <= frame_counter_dec_en_i ;
        end if;
    end process;

U2_clk_proc : process (clk_in_280M_i)   -- to remove timing errors
    begin
        if clk_in_280M_i'event and clk_in_280M_i = '1' then
            if MGT_fifo_rd_en_i = '1' then 
                TOB_in_is_char_tmp_1dly     <= FIFO_MGT_TOBs(32);           -- TOB data from FIFO is CHAR
                TOBs_in_tmp_1dly            <= FIFO_MGT_TOBs(31 downto 0);  -- TOB data from FIFO
            end if;
        end if;
    end process;

--                TOB_in_is_char_tmp_1dly     <= FIFO_MGT_TOBs(32);           -- TOB data from FIFO is CHAR
--                TOBs_in_tmp_1dly            <= FIFO_MGT_TOBs(31 downto 0);  -- TOB data from FIFO

U4_rd_fsm : process (clk_in_280M_i)
    begin
--        MGT_FIFO_empty_i <= MGT_FIFO_empty ;    -- cater for the empty going high on last word
        if clk_in_280M_i'event and clk_in_280M_i = '1' then
            TOB_in_is_char_i     <= TOB_in_is_char_tmp_1dly;           -- TOB data from FIFO is CHAR
            TOBs_in_i            <= TOBs_in_tmp_1dly(31 downto 0);  -- TOB data from FIFO
            T_TOB_out_valid_i    <= '0';
            if ( TOB_FIFO_sw_rst = '1' )then            -- signal is RST OR TOB_FIFO_sw_rst
                current_state           <= idle ;
            else
    
                CASE current_state is
                    when idle =>
                        T_TOB_out_valid_i         <= '0' ;
                        frame_counter_dec_en_i  <= '0' ;
                        MGT_fifo_rd_en_i        <= '0' ;
                        TOB_out_is_char_i       <= '1';
                        TOBs_out_i              <= X"000000" & ch_idle ; -- K28.5 idle = BC
                        -- if no data available wait 
                        if ctrl_TOB_ready_in = '1' then     -- if CNTL FPGA ready to receive data
                            if (frame_counter > X"000" )  then
								-- if FRAME fifo is not empty, read DATA fifos											  
                                current_state     <= rd_fifo1 ;
                           end if;
                        end if;

                    when rd_fifo1 =>
                        T_TOB_out_valid_i   <= '0' ;
                        TOB_out_is_char_i   <= '1';                                                         
                        TOBs_out_i          <= X"000000" & ch_idle ; -- K28.5 idle
                        MGT_fifo_rd_en_i    <= '1';           -- read data from fifos
                        if (TOBs_in_tmp_1dly(7 downto 0) = X"3C" AND TOB_in_is_char_tmp_1dly = '1') then      -- check fist data in order to save the SOF data and char
--                            TOB_out_is_char_i <= TOB_in_is_char_i;
--                            TOBs_out_i        <= TOBs_in_i       ;
                            current_state       <= tx_data_SOF1 ;
                        else
                            -- if fifo is not empty, read fifos
                            current_state <= rd_fifo1 ;
                        end if;
                       
                    when tx_data_SOF1 =>
                        MGT_fifo_rd_en_i  <= '1';           -- read data from fifos
                        TOB_out_is_char_i <= TOB_in_is_char_i;
                        TOBs_out_i        <= TOBs_in_i       ;
                        T_TOB_out_valid_i   <= '1' ;
                        current_state     <= tx_data1 ;
                           
                    when wait3 =>       -- this is a FIFO rd to ensure correct SOF
                        T_TOB_out_valid_i   <= '0' ;
                        MGT_fifo_rd_en_i  <= '1';           -- read data from fifos 
                        TOB_out_is_char_i <= '1';
                        TOBs_out_i        <= X"000000"& ch_idle ; -- K28.5 idle
                        current_state     <= wait4 ;   -- was 

                    when wait4 =>       -- this is a FIFO rd to ensure correct SOF
                        MGT_fifo_rd_en_i  <= '1';           -- read data from fifos
                        TOB_out_is_char_i   <= TOB_in_is_char_i;    
                        TOBs_out_i          <= TOBs_in_i       ;
                        T_TOB_out_valid_i     <= '1' ;
                        current_state     <= tx_data1 ;   -- was 

                    when tx_data1 =>
                        MGT_fifo_rd_en_i  <= '1';           -- read data from fifos
                        TOB_out_is_char_i   <= TOB_in_is_char_i;    
                        TOBs_out_i          <= TOBs_in_i       ;
                        T_TOB_out_valid_i     <= '1' ;
                        if (TOBs_in_tmp_1dly(7 downto 0) = X"DC" AND TOB_in_is_char_tmp_1dly = '1') then	-- check last data in order to save the SOF data and char
                            frame_counter_dec_en_i  <= '1' ;
                            MGT_fifo_rd_en_i    <= '0';           -- read data from fifos
                            current_state       <= tx_data_EOF1 ;
                        else
                            MGT_fifo_rd_en_i  <= '1';           -- read data from fifos
                            -- if fifo is not empty, read fifos
                            current_state <= tx_data1 ;
                        end if;
                        
                    when tx_data_EOF1 =>
                        frame_counter_dec_en_i  <= '0' ;
                        TOB_out_is_char_i   <= TOB_in_is_char_i;    
                        TOBs_out_i          <= TOBs_in_i       ;
                        T_TOB_out_valid_i   <= '1' ;
--                        TOB_out_is_char_i   <= '1';
--                        TOBs_out_i          <= X"000000" & ch_idle ; -- K28.5 idle
                        current_state       <= wait1 ;   -- was 
                       
                    when wait1 =>
                        T_TOB_out_valid_i   <= '0' ;       
                        TOB_out_is_char_i   <= '1';
                        TOBs_out_i          <= X"000000"& ch_idle ; -- K28.5 idle
                        current_state       <= wait2 ;   -- was 

                    when wait2 =>
                        T_TOB_out_valid_i   <= '0' ;       
                        TOB_out_is_char_i   <= '1';
                        TOBs_out_i          <= X"000000"& ch_idle ; -- K28.5 idle
                        current_state       <= tx_data_EOF2 ;   -- was

                    when tx_data_EOF2 =>
                        T_TOB_out_valid_i   <= '0' ;
                        TOB_out_is_char_i   <= '1';
                        TOBs_out_i          <= X"000000" & ch_idle ; -- K28.5 idle
                        current_state       <= tx_data_EOF2 ;
                        if ctrl_TOB_ready_in = '1' then
                           if (frame_counter = X"000" ) then      -- if frame_counter = X"000"
                               if (TOBs_in_i(7 downto 0) = X"DC" AND TOB_in_is_char_i = '1') then   -- no event is in FIFO (end of last event)
                                  current_state       <= idle ;
                               end if;
                            else        -- if frame_counter /= X"000"
                               MGT_fifo_rd_en_i <= '1';           -- read data from fifos
                               if (TOBs_in_i(7 downto 0) = X"3C" AND TOB_in_is_char_i = '1') then     -- more events are in FIFO (start of next event)
                                  current_state <= wait3 ;
                               end if;
                            end if;
                         end if;
                        
                    when others =>
                        NULL;
                end case;
            end if;
        end if;
    end process;

end Behavioral;
