-- Company: STFC
-- Engineer: Mohammed Siyad
-- 
-- Create Date: 01/13/2016 05:53:15 PM
-- Design Name: process FPGA
-- Module Name: process_FPGA_ipbus - Behavioral
-- Project Name: Atlas
-- Target Devices: 
-- Tool Versions: Vivado 2015.2
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.0 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
LIBRARY work;
library ipbus_lib;
USE ipbus_lib.ipbus.all;
USE ipbus_lib.ipbus_trans_decl.all;
USE ipbus_lib.mac_arbiter_decl.all;

entity proc_FPGAs is
 GENERIC(IPBUSPORT : std_logic_vector(15 DOWNTO 0));
 PORT( 
     ipb_clk         : IN     std_logic;
     mac_clk         : IN     std_logic;
     rst_ipb         : IN     std_logic;
     rst_macclk      : IN     std_logic;
     ipb_in          : IN     ipb_rbus;
     ipb_out         : OUT    ipb_wbus;
     master_rx_data  : IN     std_logic_vector (9 DOWNTO 0);
     master_tx_pause : IN     std_logic; 
     master_tx_data  : OUT    std_logic_vector (9 DOWNTO 0)

);

end proc_FPGAs;




architecture Behavioral of proc_FPGAs is


SIGNAL actual_ip_addr     : std_logic_vector(31 DOWNTO 0);
SIGNAL actual_mac_addr    : std_logic_vector(47 DOWNTO 0);
SIGNAL IP_addr            : std_logic_vector(31 DOWNTO 0);
SIGNAL MAC_addr           : std_logic_vector(47 DOWNTO 0);
SIGNAL RARP               : std_logic;
SIGNAL Remote_Got_IP_addr: std_logic;
SIGNAL enable             : std_logic;
SIGNAL ipb_grant          : std_logic := '1';

SIGNAL mac_rx_data : std_logic_vector(7 DOWNTO 0);
SIGNAL mac_rx_error, mac_rx_last,mac_rx_valid : std_logic;
SIGNAL slave_rx_data : std_logic_vector(8 DOWNTO 0);
SIGNAL slave_rx_err: std_logic;

SIGNAL mac_tx_data   : std_logic_vector(7 DOWNTO 0);
SIGNAL mac_tx_error,mac_tx_last,mac_tx_ready,mac_tx_valid  : std_logic;
SIGNAL slave_tx_data : std_logic_vector(8 DOWNTO 0);


BEGIN
   
   IP_addr <= (others =>'0');   
   MAC_addr <= (others =>'0');                                
   Enable <= '1';
   RARP   <= '1';
   ipb_grant <='1';

----- process fpga
   -- Instance port mappings.
   U_0 :entity ipbus_lib.UDP_slave_if
      PORT MAP (
         mac_clk        => mac_clk,
         rst_macclk     => rst_macclk,
         mac_rx_data    => mac_rx_data,
         mac_rx_error   => mac_rx_error,
         mac_rx_last    => mac_rx_last,
         mac_rx_valid   => mac_rx_valid,
         Got_IP_addr    => Remote_Got_IP_addr,
         mac_tx_ready   => mac_tx_ready,
         mac_tx_data    => mac_tx_data,
         mac_tx_error   => mac_tx_error,
         mac_tx_last    => mac_tx_last,
         mac_tx_valid   => mac_tx_valid,
         slave_rx_data  => slave_rx_data,
         slave_rx_err   => slave_rx_err,
         slave_tx_pause => master_tx_pause,
         slave_tx_data  => slave_tx_data
      );

  U_1 : entity work.interconnect
      PORT MAP (
         mac_clk         => mac_clk,
         master_rx_data  => master_rx_data,
         rst_macclk      => rst_macclk,
         tx_data         => slave_tx_data,
         master_rx_err   => slave_rx_err,
         process_tx_data => master_tx_data,
         rx_data         => slave_rx_data
      );
   U_2 :entity ipbus_lib.ipbus_ctrl
      GENERIC MAP (
         MAC_CFG       => EXTERNAL,
         IP_CFG        => EXTERNAL,
         -- Number of address bits to select RX or TX buffer in UDP I/F
         -- Number of RX and TX buffers is 2**BUFWIDTH
         BUFWIDTH      => 4,
         -- Numer of address bits to select internal buffer in UDP I/F
         -- Number of internal buffers is 2**INTERNALWIDTH
         INTERNALWIDTH => 1,
         -- Number of address bits within each buffer in UDP I/F
         -- Size of each buffer is 2**ADDRWIDTH
         ADDRWIDTH     => 11,
         -- UDP port for IPbus traffic in this instance of UDP I/F
         IPBUSPORT     => IPBUSPORT, --x"C352",
         -- Flag whether this UDP I/F instance ignores everything except IPBus traffic
         SECONDARYPORT => '1',
         N_OOB         => 0
      )
      PORT MAP (
         mac_clk         => mac_clk,
         rst_macclk      => rst_macclk,
         ipb_clk         => ipb_clk,
         rst_ipb         => rst_ipb,
         mac_rx_data     => mac_rx_data,
         mac_rx_valid    => mac_rx_valid,  
         mac_rx_last     => mac_rx_last,
         mac_rx_error    => mac_rx_error,
         mac_tx_data     => mac_tx_data,
         mac_tx_valid    => mac_tx_valid,
         mac_tx_last     => mac_tx_last,
         mac_tx_error    => mac_tx_error,
         mac_tx_ready    => mac_tx_ready,
         ipb_out         => ipb_out,
         ipb_in          => ipb_in,
         ipb_req         => OPEN,
         ipb_grant       => ipb_grant,
         mac_addr        => MAC_addr,
         ip_addr         => IP_addr,
         enable          => enable,
         RARP_select     => RARP,
         actual_mac_addr => actual_mac_addr,
         actual_ip_addr  => actual_ip_addr,
         Got_IP_addr     => Remote_Got_IP_addr,
         pkt             => OPEN
        
      );
END Behavioral;
