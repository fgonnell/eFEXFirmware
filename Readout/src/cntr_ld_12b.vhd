-- 12 bit loadable counter with terminal count.


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cntr_ld_12b is
    generic (
        T_count       : in STD_LOGIC_VECTOR (11 downto 0);
        ld_count      : in STD_LOGIC_VECTOR (11 downto 0)
        );

    Port ( 
       CE   : in STD_LOGIC;
       CLK  : in STD_LOGIC;
       RST  : in STD_LOGIC;
       LD   : in STD_LOGIC;
       Q    : out STD_LOGIC_VECTOR (11 downto 0)
       );
end cntr_ld_12b;

architecture Behavioral of cntr_ld_12b is

begin
process (CLK)
	variable temp : unsigned (11 downto 0):= (others => '0') ;
	variable terminal_cnt : unsigned (11 downto 0):= (others => '0') ;
	begin
--	if RST = '1' then                       -- this is an asynchronous reset
--		temp := unsigned(load);              -- set counter to offset value given.
--		terminal_cnt := unsigned(T_count);   -- set terminal count value given.
	if CLK'event AND CLK = '1' then
     	if RST = '1' OR LD = '1' then           -- this is a synchronous reset or load
      		temp := unsigned(ld_count);              -- set counter to offset value given.
      		terminal_cnt := unsigned(T_count);   -- set terminal count value given.
		elsif CE = '1' then
			if temp = terminal_cnt then	     -- if count = terminal count then
				temp := X"000";			    -- set counter to 1 
			else	
				   temp := temp + 1;			          -- else increment the value by one 
			end if;
		 end if;
	end if;
	Q <= std_logic_vector(temp) after 50 ps;	 -- push out new value
	end process;

end Behavioral;
