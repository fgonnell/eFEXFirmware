--- Top_udp_config_FPGA
-- This design combines the control of the UDP of the four processing FPGAs
--- It creates the interconnection between the control and four processing FPGAs
--
-- -- Cretaed by Mohammed Siyad on  18/1/2016


LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
library ipbus_lib;
use ipbus_lib.ipbus.ALL;
USE ipbus_lib.ipbus_trans_decl.all;
USE ipbus_lib.mac_arbiter_decl.all;

ENTITY top_udp_config_FPGA IS
	Generic (
		constant IPBUSBUFWIDTH: positive := 6
	);
   PORT(
      ipb_clk          : IN     std_logic;
      ipb_in           : IN     ipb_rbus;
      hardware_addr    : IN     std_logic_vector (11 DOWNTO 0);
  	  serial_number    : IN 	std_logic_vector (5 DOWNTO 0) := (others => '0');
      mac_clk          : IN     std_logic;
      mac_rx_data      : IN     std_logic_vector (7 DOWNTO 0);
      mac_rx_error     : IN     std_logic;
      mac_rx_last      : IN     std_logic;
      mac_rx_valid     : IN     std_logic;
      mac_tx_ready     : IN     std_logic;
      master_tx_data1  : IN     std_logic_vector (8 DOWNTO 0);
      master_tx_data2  : IN     std_logic_vector (8 DOWNTO 0);
      master_tx_data3  : IN     std_logic_vector (8 DOWNTO 0);
      master_tx_data4  : IN     std_logic_vector (8 DOWNTO 0);
      master_tx_err1   : IN     std_logic;
      master_tx_err2   : IN     std_logic;
      master_tx_err3   : IN     std_logic;
      master_tx_err4   : IN     std_logic;
      rst_ipb          : IN     std_logic;
      rst_macclk       : IN     std_logic;
      ipb_out          : OUT    ipb_wbus;
      mac_tx_data      : OUT    std_logic_vector (7 DOWNTO 0);
      mac_tx_error     : OUT    std_logic;
      mac_tx_last      : OUT    std_logic;
      mac_tx_valid     : OUT    std_logic;
      master_rx_data1  : OUT    std_logic_vector (8 DOWNTO 0);
      master_rx_data2  : OUT    std_logic_vector (8 DOWNTO 0);
      master_rx_data3  : OUT    std_logic_vector (8 DOWNTO 0);
      master_rx_data4  : OUT    std_logic_vector (8 DOWNTO 0);
      master_tx_pause1 : OUT    STD_LOGIC;
      master_tx_pause2 : OUT    STD_LOGIC;
      master_tx_pause3 : OUT    STD_LOGIC;
      master_tx_pause4 : OUT    STD_LOGIC;
      master_link_down1: IN     std_logic;
      master_link_down2: IN     std_logic;
      master_link_down3: IN     std_logic;
      master_link_down4: IN     std_logic
   );

-- Declarations

END top_udp_config_FPGA ;



ARCHITECTURE struct OF top_udp_config_FPGA IS

   -- Architecture declarations

   -- Internal signal declarations
   SIGNAL Master_got_IP_addr  : std_logic;
   SIGNAL ip_addr, actual_ip_addr      : std_logic_vector(31 DOWNTO 0);
   SIGNAL mac_addr, actual_mac_addr     : std_logic_vector(47 DOWNTO 0);
   SIGNAL enable              : std_logic;
   SIGNAL ipb_grant           : std_logic := '1';
   SIGNAL rarp_rx_data        : std_logic_vector(7 DOWNTO 0);
   SIGNAL rarp_rx_last        : std_logic;
   SIGNAL rarp_rx_valid       : std_logic;
   SIGNAL slaves_Got_IP_addr  : std_logic;
   SIGNAL slaves_Got_IP_addr1 : std_logic;
   SIGNAL slaves_Got_IP_addr2 : std_logic;
   SIGNAL slaves_Got_IP_addr3 : std_logic;
   SIGNAL slaves_Got_IP_addr4 : std_logic;
   SIGNAL src_tx_data_bus     : mac_arbiter_slv_array(4 DOWNTO 0);
   SIGNAL src_tx_error_bus    : mac_arbiter_sl_array(4 DOWNTO 0);
   SIGNAL src_tx_last_bus     : mac_arbiter_sl_array(4 DOWNTO 0);
   SIGNAL src_tx_ready_bus    : mac_arbiter_sl_array(4 DOWNTO 0);
   SIGNAL src_tx_valid_bus    : mac_arbiter_sl_array(4 DOWNTO 0);





BEGIN

   ipb_grant <='1';
   slaves_got_IP_addr <= slaves_got_IP_addr1 and slaves_got_IP_addr2 and slaves_got_IP_addr3  and slaves_got_IP_addr4;


   -- Instance port mappings.
   U_0 :entity work.interface_proc_fpga
      GENERIC MAP (
         IPBUFWIDTH => IPBUSBUFWIDTH
      )
      PORT MAP (
         mac_clk            => mac_clk,
         mac_rx_data        => mac_rx_data,
         mac_rx_error       => mac_rx_error,
         mac_rx_last        => mac_rx_last,
         mac_rx_valid       => mac_rx_valid,
         mac_tx_ready       => src_tx_ready_bus(1),
         master_tx_data     => master_tx_data1,
         master_tx_err      => master_tx_err1,
         rarp_rx_data       => rarp_rx_data,
         rarp_rx_last       => rarp_rx_last,
         rarp_rx_valid      => rarp_rx_valid,
         rst_macclk         => rst_macclk,
         mac_tx_data        => src_tx_data_bus(1),
         mac_tx_error       => src_tx_error_bus(1),
         mac_tx_last        => src_tx_last_bus(1),
         mac_tx_valid       => src_tx_valid_bus(1),
         master_rx_data     => master_rx_data1,
         master_tx_pause    => master_tx_pause1,
         master_link_down   => master_link_down1,
         slaves_Got_IP_addr => slaves_Got_IP_addr1
      );
   U_1 : entity work.interface_proc_fpga
      GENERIC MAP (
         IPBUFWIDTH => IPBUSBUFWIDTH
      )
      PORT MAP (
         mac_clk            => mac_clk,
         mac_rx_data        => mac_rx_data,
         mac_rx_error       => mac_rx_error,
         mac_rx_last        => mac_rx_last,
         mac_rx_valid       => mac_rx_valid,
         mac_tx_ready       => src_tx_ready_bus(2),
         master_tx_data     => master_tx_data2,
         master_tx_err      => master_tx_err2,
         rarp_rx_data       => rarp_rx_data,
         rarp_rx_last       => rarp_rx_last,
         rarp_rx_valid      => rarp_rx_valid,
         rst_macclk         => rst_macclk,
         mac_tx_data        => src_tx_data_bus(2),
         mac_tx_error       => src_tx_error_bus(2),
         mac_tx_last        => src_tx_last_bus(2),
         mac_tx_valid       => src_tx_valid_bus(2),
         master_rx_data     => master_rx_data2,
         master_tx_pause    => master_tx_pause2,
         master_link_down   => master_link_down2,
         slaves_Got_IP_addr => slaves_Got_IP_addr2
      );
   U_2 : entity work.interface_proc_fpga
      GENERIC MAP (
         IPBUFWIDTH => IPBUSBUFWIDTH
      )
      PORT MAP (
         mac_clk            => mac_clk,
         mac_rx_data        => mac_rx_data,
         mac_rx_error       => mac_rx_error,
         mac_rx_last        => mac_rx_last,
         mac_rx_valid       => mac_rx_valid,
         mac_tx_ready       => src_tx_ready_bus(3),
         master_tx_data     => master_tx_data3,
         master_tx_err      => master_tx_err3,
         rarp_rx_data       => rarp_rx_data,
         rarp_rx_last       => rarp_rx_last,
         rarp_rx_valid      => rarp_rx_valid,
         rst_macclk         => rst_macclk,
         mac_tx_data        => src_tx_data_bus(3),
         mac_tx_error       => src_tx_error_bus(3),
         mac_tx_last        => src_tx_last_bus(3),
         mac_tx_valid       => src_tx_valid_bus(3),
         master_rx_data     => master_rx_data3,
         master_tx_pause    => master_tx_pause3,
         master_link_down   => master_link_down3,
         slaves_Got_IP_addr => slaves_Got_IP_addr3
      );
   U_3 : entity work.interface_proc_fpga
      GENERIC MAP (
         IPBUFWIDTH => IPBUSBUFWIDTH
      )
      PORT MAP (
         mac_clk            => mac_clk,
         mac_rx_data        => mac_rx_data,
         mac_rx_error       => mac_rx_error,
         mac_rx_last        => mac_rx_last,
         mac_rx_valid       => mac_rx_valid,
         mac_tx_ready       => src_tx_ready_bus(4),
         master_tx_data     => master_tx_data4,
         master_tx_err      => master_tx_err4,
         rarp_rx_data       => rarp_rx_data,
         rarp_rx_last       => rarp_rx_last,
         rarp_rx_valid      => rarp_rx_valid,
         rst_macclk         => rst_macclk,
         mac_tx_data        => src_tx_data_bus(4),
         mac_tx_error       => src_tx_error_bus(4),
         mac_tx_last        => src_tx_last_bus(4),
         mac_tx_valid       => src_tx_valid_bus(4),
         master_rx_data     => master_rx_data4,
         master_tx_pause    => master_tx_pause4,
         master_link_down   => master_link_down4,
         slaves_Got_IP_addr => slaves_Got_IP_addr4
      );
   U_5 : entity ipbus_lib.ipbus_ctrl
      GENERIC MAP (
         MAC_CFG       => EXTERNAL,
         IP_CFG        => EXTERNAL,
         -- Number of address bits to select RX or TX buffer in UDP I/F
         -- Number of RX and TX buffers is 2**BUFWIDTH
         BUFWIDTH      => 4,
         -- Numer of address bits to select internal buffer in UDP I/F
         -- Number of internal buffers is 2**INTERNALWIDTH
         INTERNALWIDTH => 1,
         -- Number of address bits within each buffer in UDP I/F
         -- Size of each buffer is 2**ADDRWIDTH
         ADDRWIDTH     => 11,
         -- UDP port for IPbus traffic in this instance of UDP I/F
         IPBUSPORT     => x"C351",
         -- Flag whether this UDP I/F instance ignores everything except IPBus traffic
         SECONDARYPORT => '0',
         N_OOB         => 0
      )
      PORT MAP (
         mac_clk         => mac_clk,
         rst_macclk      => rst_macclk,
         ipb_clk         => ipb_clk,
         rst_ipb         => rst_ipb,
         mac_rx_data     => mac_rx_data,
         mac_rx_valid    => mac_rx_valid,
         mac_rx_last     => mac_rx_last,
         mac_rx_error    => mac_rx_error,
         mac_tx_data     => src_tx_data_bus(0),
         mac_tx_valid    => src_tx_valid_bus(0),
         mac_tx_last     => src_tx_last_bus(0),
         mac_tx_error    => src_tx_error_bus(0),
         mac_tx_ready    => src_tx_ready_bus(0),
         ipb_out         => ipb_out,
         ipb_in          => ipb_in,
         ipb_req         => OPEN,
         ipb_grant       => ipb_grant,
         mac_addr        => mac_addr,
         ip_addr         => ip_addr,
         enable          => enable,
         RARP_select     => '0',
         actual_mac_addr => actual_mac_addr,
         actual_ip_addr  => actual_ip_addr,
         Got_IP_addr     => Master_got_IP_addr,
         pkt             => OPEN,
         pkt_oob         => OPEN,
         oob_in          => OPEN,
         oob_out         => OPEN
      );
   U_4 :entity ipbus_lib. mac_arbiter
      GENERIC MAP (
         NSRC => 5
      )
      PORT MAP (
         mac_clk          => mac_clk,
         rst_macclk       => rst_macclk,
         src_tx_data_bus  => src_tx_data_bus,
         src_tx_valid_bus => src_tx_valid_bus,
         src_tx_last_bus  => src_tx_last_bus,
         src_tx_error_bus => src_tx_error_bus,
         src_tx_ready_bus => src_tx_ready_bus,
         mac_tx_data      => mac_tx_data,
         mac_tx_valid     => mac_tx_valid,
         mac_tx_last      => mac_tx_last,
         mac_tx_error     => mac_tx_error,
         mac_tx_ready     => mac_tx_ready
      );
   U_6 : entity ipbus_lib.udp_master_rarp
      PORT MAP (
         mac_clk            => mac_clk,
         rst_macclk         => rst_macclk,
         actual_mac_addr    => actual_mac_addr,
         actual_ip_addr     => actual_ip_addr,
         Master_got_IP_addr => Master_got_IP_addr,
         Slaves_got_IP_addr => slaves_Got_IP_addr,
         rarp_rx_data       => rarp_rx_data,
         rarp_rx_last       => rarp_rx_last,
         rarp_rx_valid      => rarp_rx_valid
      );
   U_7 : entity work.unique_address
      PORT MAP (
         mac_clk         => mac_clk,
         rst_macclk      => rst_macclk,
         hardware_addr   => hardware_addr,
         serial_number   => serial_number,
         mac_addr        => mac_addr,
         ip_addr         => ip_addr,
         enable          => enable
      );

END struct;
