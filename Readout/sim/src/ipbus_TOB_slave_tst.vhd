----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/19/2018 08:33:33 AM
-- Design Name: 
-- Module Name: ipbus_slave_tst - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
---- The signals going from slaves to master - parallel bus
--	type ipb_rbus is
--    record
--			ipb_rdata: std_logic_vector(31 downto 0);
--			ipb_ack: std_logic;
--			ipb_err: std_logic;
--    end record;

---- The signals going from master to slaves - parallel bus
--	type ipb_wbus is
--		record
--			ipb_addr: std_logic_vector(31 downto 0);
--			ipb_wdata: std_logic_vector(31 downto 0);
--			ipb_strobe: std_logic;
--			ipb_write: std_logic;
--		end record;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library ipbus_lib;
use ipbus_lib.ipbus.all;

entity ipbus_TOB_slave_tst is
--generic(N_SLAVES: integer := 0);
    Port ( 
        ipb_rst          : in std_logic ;
        ipb_clk          : in std_logic ;
        IPb_in           : out ipb_wbus;      -- The signals going from master to slaves
        IPb_out          : in   ipb_rbus;   -- The signals going from slaves to master
        L1A_tst_out      : out std_logic    -- test L1A
           );
end ipbus_TOB_slave_tst;

architecture Behavioral of ipbus_TOB_slave_tst is

    constant Clk_period : time := 25 ns;
--    signal  IPb_in  : ipb_wbus_array(N_SLAVES - 1 downto 0);
--    signal  IPb_out : ipb_rbus_array(N_SLAVES-1 downto 0);

begin
u1_proc : process
    begin
        IPb_in.ipb_addr     <= X"00000000" ;    -- set all to ZERO
        IPb_in.ipb_wdata    <= X"00000000" ;
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        L1A_tst_out         <= '0' ;
        wait for Clk_period * 130;     -- wait 10 clk periods
        
-- TOB_WR_ADDR_OFFSET
        IPb_in.ipb_addr     <= X"00000002" ;
        IPb_in.ipb_wdata    <= X"00000012" ;
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
        
--TOB_SLICES_TO_RD
        IPb_in.ipb_addr     <= X"00000005" ;
        IPb_in.ipb_wdata    <= X"00000002" ;
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
    
-- N_SLV_TOB_FIFO_FULL_THRESH_ASSERT
        IPb_in.ipb_addr     <= X"00000006" ;    -- $200 = 512
        IPb_in.ipb_wdata    <= X"FFFFFD2C" ;    -- $12C = 300  -- for test only
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
        
--N_SLV_TOB_FIFO_FULL_THRESH_NEGATE
        IPb_in.ipb_addr     <= X"00000007" ;    -- $200 = 512
        IPb_in.ipb_wdata    <= X"FFFFFCC8" ;    -- $0C8 = 200
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
                
-- N_SLV_XTOB_EG_FIFO_FULL_THRESH_ASSERT
        IPb_in.ipb_addr     <= X"00000010" ;    -- $200 = 512
        IPb_in.ipb_wdata    <= X"FFFFFD2C" ;    -- $12C = 300  -- for test only
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
        
--N_SLV_XTOB_EG_FIFO_FULL_THRESH_NEGATE
        IPb_in.ipb_addr     <= X"00000011" ;    -- $200 = 512
        IPb_in.ipb_wdata    <= X"FFFFFCC8" ;    -- $0C8 = 200
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period * 1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
                
-- N_SLV_XTOB_TAU_FIFO_FULL_THRESH_ASSERT
        IPb_in.ipb_addr     <= X"00000013" ;    -- $200 = 512
        IPb_in.ipb_wdata    <= X"FFFFFD2C" ;    -- $12C = 300  -- for test only
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
        
--N_SLV_XTOB_TAU_FIFO_FULL_THRESH_NEGATE
        IPb_in.ipb_addr     <= X"00000014" ;    -- $200 = 512
        IPb_in.ipb_wdata    <= X"FFFFFCC8" ;    -- $0C8 = 200
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period * 1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
                
                                
-- N_SLV_LINK_OUTPUT_FIFO_FULL_THRESH_ASSERT
        IPb_in.ipb_addr     <= X"00000020" ;    -- $1FFF = 8192
        IPb_in.ipb_wdata    <= X"FFFFF800" ;    -- $1800 = 6144 75%
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
        
--N_SLV_LINK_OUTPUT_FIFO_FULL_THRESH_NEGATE
        IPb_in.ipb_addr     <= X"00000021" ;    -- $1FFF = 8192
        IPb_in.ipb_wdata    <= X"FFFFF000" ;    -- $1000 = 4096 50%
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '1' ;	-- write
        wait for Clk_period * 1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
                
--write L1A
        L1A_tst_out         <= '1' ;   -- write L1A
        wait for Clk_period * 3;     -- wait 3 clk periods
        L1A_tst_out         <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
        
--N_SLV_SPY_TOB_MEM_WR_ADDR
        IPb_in.ipb_addr     <= X"00000031" ;
        IPb_in.ipb_wdata    <= X"00000000" ;
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '0' ;    -- read
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 10;     -- wait 6 clk periods

--write L1A
        L1A_tst_out         <= '1' ;   -- write L1A
        wait for Clk_period * 1;     -- wait 1 clk periods
        L1A_tst_out         <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
        

        wait for Clk_period * 10;     -- wait 100 clk periods         
        
--write L1A
        L1A_tst_out         <= '1' ;   -- write L1A
        wait for Clk_period * 1;     -- wait 1 clk periods
        L1A_tst_out         <= '0' ;
        wait for Clk_period * 10;     -- wait 6 clk periods
                
 --L1A_ID
        IPb_in.ipb_addr     <= X"00000000" ;
        IPb_in.ipb_wdata    <= X"00000000" ;
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '0' ;    -- read
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 10;     -- wait 6 clk periods
                    
--BCN_ID
        IPb_in.ipb_addr     <= X"00000001" ;
        IPb_in.ipb_wdata    <= X"00000000" ;
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '0' ;    -- read
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 10;     -- wait 6 clk periods
                            
 --N_SLV_TOB_FIFO_DATA_COUNT
        IPb_in.ipb_addr     <= X"00000006" ;
        IPb_in.ipb_wdata    <= X"00000000" ;
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '0' ;    -- read
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 10;     -- wait 6 clk periods
                    
--N_SLV_XTOB_EG_FIFO_DATA_COUNT
        IPb_in.ipb_addr     <= X"00000012" ;
        IPb_in.ipb_wdata    <= X"00000000" ;
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '0' ;    -- read
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 10;     -- wait 6 clk periods
                            
--N_SLV_XTOB_TAU_FIFO_DATA_COUNT
        IPb_in.ipb_addr     <= X"00000015" ;
        IPb_in.ipb_wdata    <= X"00000000" ;
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '0' ;    -- read
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
                            
--N_SLV_LINK_OUTPUT_FIFO_RD_DATA_COUNT
        IPb_in.ipb_addr     <= X"00000022" ;
        IPb_in.ipb_wdata    <= X"00000000" ;
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '0' ;    -- read
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
                    
--N_SLV_LINK_OUTPUT_FIFO_WR_DATA_COUNT
        IPb_in.ipb_addr     <= X"00000023" ;
        IPb_in.ipb_wdata    <= X"00000000" ;
        IPb_in.ipb_strobe   <= '1' ;
        IPb_in.ipb_write    <= '0' ;    -- read
        wait for Clk_period*1;     -- wait 1 clk periods
        IPb_in.ipb_strobe   <= '0' ;
        IPb_in.ipb_write    <= '0' ;
        wait for Clk_period * 6;     -- wait 6 clk periods
                    
                
                 
        wait;
    end process;

end Behavioral;
