library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

entity reconfig is
    generic (
        WBSTAR : std_logic_vector(31 downto 0) := x"00000000" -- Warm Boot Start Address
        );
  port (
    TRIGGER : in std_logic;
    SYSCLK  : in std_logic
    );
end reconfig;

architecture behavioral of reconfig is
  
  type FSM_STATE is (IDLE, DATA_00, DATA_01, DATA_02, DATA_03, DATA_04, DATA_05, DATA_06, DATA_07);
  
  signal NEXT_STATE : FSM_STATE                    := IDLE;
  signal CE_n         : std_logic                     := '1';
  signal ICAP_DATA          : std_logic_vector(31 downto 0) := (Others => '0');
  signal ICAP_WRITE_n : std_logic                     := '1';
  
  attribute dont_touch : string;
  attribute dont_touch of ICAP_DATA : signal is "true";
  attribute dont_touch of ICAP_WRITE_n : signal is "true";
  attribute dont_touch of CE_n : signal is "true";

  signal ICAP_DATA_BITSWAP          : std_logic_vector(31 downto 0) := (Others => '0');

  
begin


--ICAP_DATA_BITSWAP <= ICAP_DATA (24 downto 31) & ICAP_DATA (16 downto 23) & ICAP_DATA (8 downto 15) &ICAP_DATA (0 downto 7) ;
bit_swap: for i in 0 to 7 generate
      ICAP_DATA_BITSWAP(i) <= ICAP_DATA(7-i);
      ICAP_DATA_BITSWAP(i+8) <= ICAP_DATA(15-i);
      ICAP_DATA_BITSWAP(i+16) <= ICAP_DATA(23-i);
      ICAP_DATA_BITSWAP(i+24) <= ICAP_DATA(31-i);
    end generate;

  
  ICAPE2_inst : ICAPE2
    generic map (
      ICAP_WIDTH        => "X32",
      SIM_CFG_FILE_NAME => "NONE"
      )
    port map (
      O     => open,                       -- 32-bit output (not used)
      CLK   => SYSCLK,                  -- 1-bit Clock Input
      CSIB  => CE_n,                      -- 1-bit Active-Low ICAP Enable
      I     => ICAP_DATA_BITSWAP,                       -- 32-bit iConfiguration data input bus
      RDWRB => ICAP_WRITE_n               -- 1-bit input: Read/Write Select input
      );

  process(SYSCLK)
  begin
    if (falling_edge(SYSCLK)) then
        case NEXT_STATE is
          when IDLE =>
            ICAP_WRITE_n <= '1';
            CE_n         <= '1';
            ICAP_DATA          <= x"FFFFFFFF";  -- dummy word
				if Trigger = '1' then
            NEXT_STATE <= DATA_00;
				end if;
          when DATA_00 =>
            ICAP_WRITE_n <= '0';
            CE_n         <= '0';
            ICAP_DATA          <= x"FFFFFFFF";  -- dummy word
            NEXT_STATE <= DATA_01;
          when DATA_01 =>
            ICAP_WRITE_n <= '0';
            CE_n         <= '0';
            ICAP_DATA          <= x"AA995566";  -- sync word
            NEXT_STATE <= DATA_02;
          when DATA_02 =>
            ICAP_WRITE_n <= '0';
            CE_n         <= '0';
            ICAP_DATA          <= x"20000000";  -- Type 1 NO OP
            NEXT_STATE <= DATA_03;
          when DATA_03 =>
            ICAP_WRITE_n <= '0';
            CE_n         <= '0';
            ICAP_DATA          <= x"30020001";  -- Type 1 Write 1 word to WBSTAR
            NEXT_STATE <= DATA_04;
          when DATA_04 =>
            ICAP_WRITE_n <= '0';
            CE_n         <= '0';
            ICAP_DATA          <= WBSTAR;  -- Warm boot start address
            NEXT_STATE <= DATA_05;
          when DATA_05 =>
            ICAP_WRITE_n <= '0';
            CE_n         <= '0';
            ICAP_DATA          <= x"30008001";  -- Type 1 write 1 word to CMD
            NEXT_STATE <= DATA_06;
          when DATA_06 =>
            ICAP_WRITE_n <= '0';
            CE_n         <= '0';
            ICAP_DATA          <= x"0000000F";  -- IPROG command
            NEXT_STATE <= DATA_07;
          when DATA_07 =>
            ICAP_WRITE_n <= '0';
            CE_n         <= '0';
            ICAP_DATA          <= x"20000000";  -- Type 1 NO OP
            NEXT_STATE <= IDLE;
			 when others =>
			   ICAP_WRITE_n <= '1';
            CE_n         <= '1';
            ICAP_DATA          <= x"FFFFFFFF";  -- dummy word
				NEXT_STATE <= IDLE;
        end case;
    end if;
  end process;

end behavioral;
