-- Dave Sankey, September 2019

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

LIBRARY infrastructure_lib;
use infrastructure_lib.packet_mux_type.all;

entity fifo_40M_160M is
	Port (
		rst : in STD_LOGIC;
		wr_clk : in STD_LOGIC;
		rd_clk : in STD_LOGIC;
		din : in STD_LOGIC_VECTOR ( 43 downto 0 );
		wr_en : in STD_LOGIC;
		rd_en : in STD_LOGIC;
		dout : out STD_LOGIC_VECTOR ( 43 downto 0 );
		full : out STD_LOGIC;
		empty : out STD_LOGIC
	);
end fifo_40M_160M;

architecture simple of fifo_40M_160M is

Begin

TTC_FIFO_process: process(rst, rd_clk, wr_clk)
  Variable TTC_ram: packet_ttc_array(15 DOWNTO 0) := (Others => (Others => '0'));
  Variable fifo_count: Integer := 0;
  Variable RAM_pointer, RAM_words: unsigned(7 downto 0) := (Others => '0');
  begin
    if rst = '1' then
      fifo_count := 0;
    end if;
    if rising_edge(rd_clk) then
      if fifo_count > 0 then
        empty <= '0' after 2 ns;
      else
        empty <= '1' after 2 ns;
      End If;
	  if rd_en = '1' and fifo_count > 0 then
        dout <= TTC_ram(0) after 2 ns;
	    TTC_ram(14 downto 0) := TTC_ram(15 downto 1);
	    TTC_ram(15) := (Others => '0');
        fifo_count := fifo_count - 1;
      else
        dout <= (Others => '0') after 2 ns;
      end if;
    end if;
    if rising_edge(wr_clk) then
      if fifo_count = 16 then
        full <= '1';
      else
        full <= '0';
        if wr_en = '1' then
          TTC_ram(fifo_count) := din;
          fifo_count := fifo_count + 1;
        end if;
      end if;
    end if;
  end process TTC_FIFO_process;

End architecture simple;
