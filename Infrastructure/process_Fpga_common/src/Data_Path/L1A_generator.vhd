----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/23/2017 12:01:59 PM
-- Design Name: 
-- Module Name: L1A_generator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

entity L1A_generator is

  Port ( 

  clk40     : IN std_logic;
  L1A_pulse : out std_logic
  
  );
  
end L1A_generator;

architecture clk_pulse of L1A_generator is
signal cntr : unsigned (20 downto 0):= (others => '0') ;

begin
  process(clk40)
     begin
        if clk40' event and clk40 ='1' then 
              cntr<= cntr+1;            -- free running counter at 40MHz
              if cntr = 200 then     -- generate pulse every 100 KHz
--              if cntr = 400000 then     -- generate pulse every 100 Hz
                 L1A_pulse <= '1' ; 
                 cntr <= (others => '0'); 
              else   
                 L1A_pulse <= '0';
              end if;
         end if;
       end process;
                
end clk_pulse;

architecture time_pulse of L1A_generator is
signal cntr : unsigned (20 downto 0):= (others => '0') ;

begin
  process
     begin
        L1A_pulse <= '0';
        wait for 7 us;  --alighn to 40MHz clk for 1st data to be BC
        L1A_pulse <= '1';
        wait for 25 ns ;  --for next 25 ns signal is '1'.
        L1A_pulse <= '0';
        wait for 500 ns ;  -- signal is '0'.
        L1A_pulse <= '1';
        wait for 25 ns ;  --for next 25 ns signal is '1'.
        L1A_pulse <= '0';
        wait for 0.4 us;  -- signal is '0'.
        L1A_pulse <= '1';
        wait for 50 ns ;  --for next 25 ns signal is '1'.
        L1A_pulse <= '0';
        wait for 0.3 us;  -- signal is '0'.
        L1A_pulse <= '1';
        wait for 75 ns ;  --for next 25 ns signal is '1'.
        L1A_pulse <= '0';
        wait for 0.4 us;  -- signal is '0'.
        L1A_pulse <= '1';
        wait for 100 ns ;  --for next 25 ns signal is '1'.
        L1A_pulse <= '0';
        wait for 0.5 us;  -- signal is '0'.
        L1A_pulse <= '1';
        wait for 25 ns ;  --for next 25 ns signal is '1'.
        L1A_pulse <= '0';
        wait ;
     end process;
                
end time_pulse;
