--! @file
--! @brief RAW Calorimeter Data Readout Logic for process FPGA
--! @details 
--! \verbatim
--! This module received Calorimeter RAW data and produces events of 32b words for transmission to control FPGA.
--!
--! There are 40 ECAL and 9 HCAL input fibers.
--! 
--! Different RAW event are generated depending on buffer levels and control settings: 
--! 1. Normal operation when only fibres with error flags set, are assembled into the RAW Event.
--!    It takes 598 ticks of 280MHz clock to create one event as the FSM must read data for every fibre.
--! 2. Normal operation when there are no errors on any fibre. 
--!    In this case, the RAW Event consists of two header words, two error flag payload registers and one trailer word.
--!    This RAW event takes 15 ticks of 280MHz clock to create as the fibre data is discarded.
--! 3. Read_All mode and TTC_Privilege Mode at very low L1A rates.
--!    In this case the data for every fibre is assembled into RAW event regardless of error status of the links. 
--!    It takes 598 ticks of 280MHz clock to create one event as the FSM must read data for every fibre.
--! 4. Safe Mode operation when the buffer levels reach a programmable level. 
--!    In this case, the RAW Event consists of two header words and one trailer word.
--!    It takes 11 ticks of 280MHz clock to create one event as the RAW data from all fibres are discarded.
--!
--! Sequence of Buffers occupancy levels:
--! 1. When the Ready signal from Control FPGA is removed, complete RAW Events are stored in RAW Link Output FIFO.
--!    When the occupancy of RAW Link Output FIFO reaches its pFULL occupancy level, then the construction of RAW Events are paused.
--!    Enough headroom must be assigned in Link Output FIFO to be able to store RAW Events under Safe Mode operation.
--! 2. At this point, fibre data are still transfered from Circular DPRAM into de-randomisation RAW Data FIFO.
--!    When the occupancy of de-randomisation RAW Data FIFO reaches its pFULL occupancy level,
--!    no more data is written to this FIFO, and a Safe Mode Flag is set which is stored in TTC FIFO with L1A_ID and BCN.
--!    A BUSY request must be sent to HUB/ROD to reduce L1A rates at this time.
--! 3. At this stage, TTC FIFO and Error Flag FIFO continue to operate and store the L1A_ID, BCN and Error Status of MGT Fibres.
--!    When the occupancy of TTC FIFO reaches its pFULL occupancy level, the FSM enters the Safe Mode Operation.
--!    In Safe Mode, all buffers, except RAW Link Output FIFO, are flushed, and very small RAW Events are generated and stored in Link Output FIFO.
--!
--! Under Safe Mode operation if the occupancy of TTC FIFO or Link Output FIFO, reaches its FULL occupancy level, then the system synchronisation is lost.
--!
--! \endverbatim 
--!
--! \verbatim
--! The GEN_CHANNEL loop, generates 49 copies of the RAW data readout blocks within it,these are:
--!     PISO unit - to convert data from 224b to 7 x 32b words
--!     Dual Port RAM - scrolling memory to store data on every bunch crossing
--!         This is circular Dual Port RAM. It stores 7x32b words per Bunch number.
--!         the DPRAM is 36b wide, and bits 35 to 32 are used to store the 4b error flags for the corresponding MGT Channel.
--!         The Read and Write address have the correct offset in order to read the actual RAW data
--!         associated with the BCN at the time of arrival of L1A
--!    Derandomisation FIFO - to store the correct data word upon receiving an L1A   
--!         This is Derandomisation FIFO. It stores 7x32b words per L1A, which are read out of Circular memory.
--!         The FIFO is 36b wide, bits 35:32 are used to store the 4b error flags for the corresponding MGT Channel.
--! \endverbatim
--!
--! @author Saeed Taghavi


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library ipbus_lib;
use ipbus_lib.ipbus.all;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

library TOB_rdout_lib;
use TOB_rdout_lib.data_type_pkg.all;
use TOB_rdout_lib.TOB_rdout_ip_pkg.all;

--! @copydoc RAW_data_rdout.vhd
entity RAW_data_rdout is
    Generic
        ( 
    --! Integer used to distinguish different FPGAs having a slightly different firmware
    FPGA_NUMBER : integer
        ) ; 
    Port ( 
       RST                  : in std_logic ;
       --! FPGA Hardware Address
       hw_addr              : in STD_LOGIC_VECTOR(1 downto 0) ; 
       --! RST_spy_mem_wr_addr, counter reset Pulse by software command  
       RST_spy_mem_wr_addr         : in std_logic ;    
       --! RAW Readout FIFO reset Pulse by software command  
       RAW_FIFO_sw_rst         : in std_logic ;    
       --! Calorimeter data array 49 x 227b input frames
       RAW_data_in          : in RAW_data_227_type;
       --! 280Mhz input signal  
       clk_280M_in          : in STD_LOGIC;
       --! 40Mhz input signal
       clk_40M_in           : in STD_LOGIC;
       --! ipb_clk signal is input from master to slaves
       ipb_clk              : in std_logic ;
       --! Ready signal from control FPGA to receive RAW calorimeter data
       ctrl_RAW_ready_in        : in  std_logic ;
       --! Calorimeter TXOUTCLK to read Calorimeter data to MGT for transmission to control FPGA
       RAW_TXOUTCLK         : in STD_LOGIC;
       --! L1A input signal
       L1A_in               : in STD_LOGIC;
       --! Bunch Crossing ID 12 bits
       BCN_ID_in            : in  STD_LOGIC_VECTOR (11 downto 0);
       --! 8b Extended L1A ID & 24b LIA_ID of the L1A Counter
       L1A_ID_in            : in  STD_LOGIC_VECTOR (31 downto 0);
       --! ERROR flags array, 49 x 4 bit per link
--       n  : in link_error_type ;                        
       --! readout all raw data links, when set all RAW data from 49 fibres are readout
       raw_rd_all_in        : in STD_LOGIC;                 
       --! latency pre-load for DPRAM write address
       pre_ld_wr_addr       : in STD_LOGIC_VECTOR (8 downto 0);
       --! RAW FIFO full flag assert threshold
       RAW_FIFO_FULL_THRESH_ASSERT   : in STD_LOGIC_VECTOR (8 downto 0);
       --! RAW FIFO full flag negate threshold
       RAW_FIFO_FULL_THRESH_NEGATE   : in STD_LOGIC_VECTOR (8 downto 0);
       --! RAW FIFO occupancy data count
	   RAW_FIFO_data_count           : out  STD_LOGIC_VECTOR(8 downto 0);
       --! latency pre-load enable signal for DRPAM write address
       cntr_load_en         : in STD_LOGIC;
       --! RAW data block FIFO flags       
       RAW_data_FIFO_flags  : out STD_LOGIC_VECTOR (8 downto 0);
       --! calorimeter data is CHAR signal to MGT & Control FPGA 
       RAW_out_to_MGT_is_char     : out STD_LOGIC;
       --! calorimeter 32b data output to MGT & Control FPGA 
       RAW_data_out           : out STD_LOGIC_VECTOR (31 downto 0); -- TOBs 32b out to MGT
       --! numer of frames in the link output FIFO to be transmitted to MGT & Control FPGA
       frame_count      : out STD_LOGIC_VECTOR (31 downto 0); 
       --! Privilege Read signal input
       TTC_read_all_in             : in STD_LOGIC;
       --! Derandomisation FIFO partial full flag assert threshold
       RAW_FIFO_pFULL_THRESH_ASSERT  : in  STD_LOGIC_VECTOR (8 downto 0);  -- MGT_FIFO 
       --! 36b derandomisation FIFO partial full flag negate threshold
       RAW_FIFO_pFULL_THRESH_NEGATE  : in  STD_LOGIC_VECTOR (8 downto 0);  -- MGT_FIFO 
       --! BCN FIFO partial full flag assert threshold
       BCN_FIFO_pFULL_THRESH_assert  : in STD_LOGIC_VECTOR(8 downto 0);
       --! BCN FIFO partial full flag negate threshold
       BCN_FIFO_pFULL_THRESH_negate  : in STD_LOGIC_VECTOR(8 downto 0);
       --! Link output FIFO (before MGT) partial full flag assert threshold
       Link_output_FIFO_RAW_pfull_thresh_assert   : in  STD_LOGIC_VECTOR (12 downto 0);   -- Link_output_FIFO
       --! Link output FIFO (before MGT) partial full flag negate threshold
       Link_output_FIFO_RAW_pfull_thresh_negate   : in  STD_LOGIC_VECTOR (12 downto 0);   -- Link_output_FIFO
       --! Link output FIFO (before MGT) occupancy data count
       Link_output_FIFO_RAW_rd_data_count        : out STD_LOGIC_VECTOR (12 downto 0);    -- occupancy of RAW output link MGT FIFO
       --! RAW Data SPY Memory write address register (read only)
       SPY_mem_wr_addr        : out STD_LOGIC_VECTOR (10 downto 0);   -- SPY memory wr_addr (read only)
       --! IPBus signal coming from RAW SPY DPRAM
	   ipbus_out_raw_dpram    : out ipb_rbus;
       --! IPBus signal going to RAW SPY DPRAM
       ipbus_in_raw_dpram     : in  ipb_wbus;
       --! 54-b error flags from the Error Flag FIFO to IPBUS register
       link_error_flags       : out STD_LOGIC_VECTOR (53 downto 0) 
       
           );
    end RAW_data_rdout;

--! @copydoc RAW_data_rdout.vhd
architecture RTL of RAW_data_rdout is

--***********************************Parameter Declarations********************
    --! number of input fibres
    constant chan_no : integer := 49 ;
--************************** Register Declarations ****************************    
	
	signal  DPR_rd_addr_i       : STD_LOGIC_VECTOR (8 downto 0);
    signal  DPR_wr_addr_i		: STD_LOGIC_VECTOR (8 downto 0);
	signal  DPR_rd_addr_i_1dly  : STD_LOGIC_VECTOR (8 downto 0);
    signal  DPR_wr_addr_i_1dly  : STD_LOGIC_VECTOR (8 downto 0);
	
    signal  FIFO_RAW_Data_dout_i        : DPR_RAW_out_36_type  ;  -- array 49 of 36b = err + data
    signal  FIFO_RAW_Data_dout_i_1dly   : DPR_RAW_out_36_type  ;  -- array 49 of 36b = err + data
	
    signal  link_error_flags_tmp  : link_error_type ;      -- array 49 x 4 bit per link
    signal  link_err_4b_in_i      : STD_LOGIC_VECTOR (3 downto 0);    -- place holder for overall error flags of all optical link inputs.
    signal  channel_error_49b_i    : STD_LOGIC_VECTOR (48 downto 0);
    signal  link_error_flags_54b_i : STD_LOGIC_VECTOR (53 downto 0);
    signal  FIFO_error_flags_54b_i : STD_LOGIC_VECTOR (53 downto 0);
    signal  req_err_rd_raw_i       : std_logic := '0' ; -- flag to request RAW data read out upon error 
    signal  FIFO_error_flags_valid_i  : std_logic; 
    
                  
    signal  PISO_data_out_i     : DPR_RAW_out_36_type;  -- array 49 of 36b = err + data
    signal  DPR_RAW_out_i       : DPR_RAW_out_36_type;  -- array 49 of 36b = err + data
    signal  DPR_RAW_out_i_1dly      : DPR_RAW_out_36_type;  -- array 49 of 36b = err + data

    signal  RST_i                 : STD_LOGIC ; 
    signal  Read_all_i            : STD_LOGIC ; -- privilege read All Flag                  
    signal  ORed_err_flg_i        : STD_LOGIC ; -- 1-b OR of all 49 bibre error flags                 
    signal  RAW_FIFO_sw_rst_i     : STD_LOGIC ;                  
    signal  clk_in_280M_i         : STD_LOGIC ;                   
    signal  clk_in_40M_i          : STD_LOGIC ;                     
    signal  PISO_sync_out_i       : t_49_arr_1b ;   -- 49 sync signals for 49 links              
    signal  PISO_data_out_valid_i : t_49_arr_1b ;   -- 49 sync signals for 49 links              
    
    signal  L1A_in_i, L1A_in_1dly   : std_logic ;
    signal  L1A_in_a                : std_logic ;
    signal  L1A_in_b                : std_logic ;
    signal  DRP_rd_en_i             : std_logic ;
    signal  DRP_rd_en_i_1dly        : std_logic ;

    signal  pre_ld_wr_addr_i        : STD_LOGIC_VECTOR (8 downto 0);

    signal  FIFO_wr_en_i            : std_logic  ;
    signal  FIFO_wr_en_i_1dly       : std_logic  ;
    signal  FIFO_wr_en_i_2dly       : std_logic  ;
    signal  FIFO_rd_en_i            : t_49_arr_1b ;
    
    signal  BCN_FIFO_rd_en_i        : std_logic  ;
    signal  en_error_valid_i        : std_logic ;
    signal  en_error_valid_1dly, en_error_valid_2dly, en_error_valid_3dly     : std_logic ;
    signal  en_error_valid_4dly     : std_logic ;
	
    signal  link_err_FIFO_empty_i   : std_logic ;
    signal  BCN_FIFO_full_i         : std_logic ;
    signal  BCN_FIFO_empty_i        : std_logic ;
    signal  BCN_FIFO_valid_i        : std_logic ;
    signal  BCN_FIFO_prog_full_i    : std_logic ;
    signal  BCN_FIFO_Data_in_i      : STD_LOGIC_VECTOR(46 downto 0) ;
    signal  BCN_FIFO_Data_out_i     : STD_LOGIC_VECTOR(46 downto 0) ;
    
    signal  BCN_FIFO_pFULL_THRESH_assert_i : STD_LOGIC_VECTOR(8 downto 0) ;
    signal  BCN_FIFO_pFULL_THRESH_negate_i : STD_LOGIC_VECTOR(8 downto 0) ;
    
    signal  Link_output_FIFO_RAW_pfull_thresh_assert_i : STD_LOGIC_VECTOR(12 downto 0) ;
    signal  Link_output_FIFO_RAW_pfull_thresh_negate_i : STD_LOGIC_VECTOR(12 downto 0) ;
    
    signal  RAW_data_FIFO_pFULL_THRESH_assert_i  : STD_LOGIC_VECTOR(8 downto 0) ;
    signal  RAW_data_FIFO_pFULL_THRESH_negate_i  : STD_LOGIC_VECTOR(8 downto 0) ;
    signal  RAW_data_FIFO_count_i               : STD_LOGIC_VECTOR(8 downto 0) ;
    signal  FIFO_RAW_Data_prog_full_i           : t_49_arr_1b ;
    signal  FIFO_RAW_Data_full_i                : std_logic ;   -- RAW FIFO full flag based on occupancy
    signal  FIFO_RAW_Data_empty_i               : t_49_arr_1b ;
    signal  FIFO_RAW_Data_valid_i               : t_49_arr_1b ;   -- 49 sync signals for 49 links
    signal  FIFO_RAW_Data_valid_i_1dly          : t_49_arr_1b ;   -- 49 sync signals for 49 links
    signal  sync_280m_i                     : STD_LOGIC; 
        
    signal  frame_cntr_dec_en_i                 : std_logic ;
    signal  frame_counter_dec_en_i              : std_logic ;
    signal  op_fifo_frame_count_i               : STD_LOGIC_VECTOR (11 downto 0) ;      -- Frame counter
	signal  frame_cntr_en_i                     : std_logic ;
    signal  RAW_out_valid_i, RAW_data_out_valid_i, RAW_out_valid_1dly          : std_logic  ;
    signal  RAW_out_char_MGT_i   : std_logic;
    signal  RAW_data_out_MGT_i   : STD_LOGIC_VECTOR(31 downto 0) ;
    signal  RAW_out_is_char_i, RAW_out_to_MGT_is_char_i, RAW_out_char_MGT_1dly   : std_logic;
    signal  RAW_out_i, q_int, RAW_data_out_i, RAW_data_out_1dly   : STD_LOGIC_VECTOR(31 downto 0) ;
    
    signal  FIFO_RAW_in_i       : STD_LOGIC_VECTOR(32 downto 0) ;

    signal  Link_output_FIFO_RAW_out_i    :  STD_LOGIC_VECTOR (32 downto 0);  -- TOBs 33b out to MGT 
    signal  Link_output_FIFO_rd_en_i      : STD_LOGIC;
    signal  Link_output_FIFO_RAW_valid_i  : STD_LOGIC;
    
    signal  Link_output_FIFO_full_i         : std_logic;
    signal  Link_output_FIFO_empty_i        : std_logic;
    signal  Link_output_FIFO_prog_full_i    : std_logic;  

    signal  reg1, reg2          : std_logic := '0' ; 
    signal  tied_to_vcc_i       : std_logic := '1' ;
    --! Control FPGA Ready signal internal
    signal  ctrl_RAW_ready_i    : std_logic ;

    signal  enable_raw_spy_mem_wr  : std_logic ;   
    signal  RST_raw_spy_mem_wr_addr_i  : std_logic ;   
    signal  SPY_mem_wr_addr_i      :  STD_LOGIC_VECTOR (10 downto 0) ;
    signal  SPY_mem_wr_addr_en_i   : STD_LOGIC ; 
    signal  error_probe0, error_probe1      : STD_LOGIC_VECTOR (35 downto 0);
    signal  error_probe2, error_probe3      : STD_LOGIC_VECTOR (35 downto 0);
    signal  SPY_mem_rd_data_i      : STD_LOGIC_VECTOR (35 downto 0);
    signal  RAW_data_FIFO_flags_i  : STD_LOGIC_VECTOR (8 downto 0);
    signal  delayed_crc_error_i    : STD_LOGIC_VECTOR (48 downto 0);
    
    -- this is the array of 49 x 32b 
    TYPE DPRAM_addr_1b_type is array ((chan_no - 1 ) downto 0) of STD_LOGIC_VECTOR (0 downto 0);
    TYPE RAW_fifo_d_count_type is array ((chan_no - 1 ) downto 0) of STD_LOGIC_VECTOR (8 downto 0);
    signal  data_count_i             : RAW_fifo_d_count_type ;
    
    signal  Link_output_FIFO_RAW_rd_data_count_i : STD_LOGIC_VECTOR (12 downto 0);    -- occupancy of RAW output link MGT FIFO
    signal  RAW_data_in_1dly           : RAW_data_227_type;     -- array 49 x 227b input frames
    signal  RAW_data_in_2dly           : RAW_data_227_type;     -- array 49 x 227b input frames

--  ####### Attributes  ########
    attribute mark_debug : string ;
    attribute keep       : string ;
    attribute max_fanout : integer;
    
    attribute keep of       RAW_out_valid_i : signal is "true" ; 
    attribute max_fanout of RAW_out_valid_i : signal is 30;
    
    attribute keep of       FIFO_RAW_in_i : signal is "true" ;
    
    attribute keep of       ctrl_RAW_ready_i : signal is "true" ;
    attribute max_fanout of ctrl_RAW_ready_i : signal is 30;

    attribute keep of       RAW_FIFO_sw_rst_i : signal is "true" ;
    attribute max_fanout of RAW_FIFO_sw_rst_i : signal is 30;

    attribute keep of       FIFO_wr_en_i_2dly : signal is "true" ;
    attribute max_fanout of FIFO_wr_en_i_2dly : signal is 30;

    attribute keep of       FIFO_rd_en_i : signal is "true" ;
    attribute max_fanout of FIFO_rd_en_i : signal is 30;
        
    attribute keep of       FIFO_wr_en_i : signal is "true" ;
    attribute max_fanout of FIFO_wr_en_i : signal is 30;
        
    attribute keep of       SPY_mem_wr_addr_en_i : signal is "true" ;
    attribute max_fanout of SPY_mem_wr_addr_en_i : signal is 30;
        
    attribute keep of       BCN_FIFO_empty_i : signal is "true" ;
    attribute max_fanout of BCN_FIFO_empty_i : signal is 30;
    attribute keep of       Link_output_FIFO_prog_full_i : signal is "true" ;
    attribute max_fanout of Link_output_FIFO_prog_full_i : signal is 30;
	
    attribute keep of       RAW_data_out_1dly    : signal is "true" ; 
    attribute keep of       RAW_out_valid_1dly   : signal is "true" ; 
    attribute keep of       RAW_data_out_MGT_i   : signal is "true" ; 
    attribute keep of       RAW_out_char_MGT_i   : signal is "true" ; 
    attribute keep of       RAW_data_out_i       : signal is "true" ;
    attribute keep of       RAW_data_out_valid_i : signal is "true" ;
    attribute keep of       BCN_FIFO_Data_in_i   : signal is "true" ;
    attribute keep of       RAW_data_in          : signal is "true" ;
    attribute keep of       RAW_data_in_1dly     : signal is "true" ;
    attribute keep of       PISO_data_out_i      : signal is "true" ;
    attribute keep of       FIFO_RAW_Data_dout_i : signal is "true" ;
    attribute keep of       RAW_data_in_2dly     : signal is "true" ;
    attribute keep of       delayed_crc_error_i  : signal is "true" ;
--   #######################################

begin

    tied_to_vcc_i       <=   '1';
    ORed_err_flg_i      <=   '0';  

    clk_in_280M_i       <= clk_280M_in ;
    clk_in_40M_i        <= clk_40M_in ;
    
    L1A_in_i            <= L1A_in ;
    RST_i               <= RST ;    -- from 40MHz MMCM lock signal
    
    RAW_FIFO_sw_rst_i   <= RST_i OR RAW_FIFO_sw_rst ;   -- rst by s/w or RST
    pre_ld_wr_addr_i    <= pre_ld_wr_addr ;        -- latency pre load for DRPRAM wr address

    RAW_data_FIFO_pFULL_THRESH_assert_i <= RAW_FIFO_pFULL_THRESH_ASSERT;      
    RAW_data_FIFO_pFULL_THRESH_negate_i <= RAW_FIFO_pFULL_THRESH_NEGATE;
	BCN_FIFO_pFULL_THRESH_assert_i      <= BCN_FIFO_pFULL_THRESH_assert ;
    BCN_FIFO_pFULL_THRESH_negate_i      <= BCN_FIFO_pFULL_THRESH_negate ;
    Link_output_FIFO_RAW_pfull_thresh_assert_i <= Link_output_FIFO_RAW_pfull_thresh_assert ;
    Link_output_FIFO_RAW_pfull_thresh_negate_i <= Link_output_FIFO_RAW_pfull_thresh_negate ;
    
    frame_count     <= X"00000" & op_fifo_frame_count_i ;
    SPY_mem_wr_addr <= SPY_mem_wr_addr_i ;
    
    RAW_data_out            <=    RAW_data_out_MGT_i;  -- RAW event to MGT
    RAW_out_to_MGT_is_char  <=    RAW_out_char_MGT_i; -- RAW data is char

    -- FIFO flags assignments
    RAW_data_FIFO_flags <= RAW_data_FIFO_flags_i;
    
    RAW_data_FIFO_flags_i(0) <= FIFO_RAW_Data_empty_i(0);
    RAW_data_FIFO_flags_i(1) <= FIFO_RAW_Data_prog_full_i(0) ;   
    RAW_data_FIFO_flags_i(2) <= FIFO_RAW_Data_full_i ;   -- use calculated full flag   
   
    RAW_data_FIFO_flags_i(3) <= BCN_FIFO_empty_i ;
    RAW_data_FIFO_flags_i(4) <= BCN_FIFO_prog_full_i ;    
    RAW_data_FIFO_flags_i(5) <= BCN_FIFO_full_i ;     
    
    RAW_data_FIFO_flags_i(6) <= Link_output_FIFO_empty_i ;     
    RAW_data_FIFO_flags_i(7) <= Link_output_FIFO_prog_full_i ;    
    RAW_data_FIFO_flags_i(8) <= Link_output_FIFO_full_i ; 
    
    RAW_FIFO_data_count <= data_count_i(20); -- use a value in the middle of pFPGA for ease of use
    
    link_error_flags    <=  FIFO_error_flags_54b_i;     -- error flags from the ERROR FLag FIFO
    
--! this FDCE regitsters the cFPGA Ready input signal to receive RAW Data.
U0_FDCE_inst : FDCE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q    => ctrl_RAW_ready_i,      -- Data output
      C    => clk_in_280M_i,      -- Clock input
      CE   => '1',    -- Clock enable input
      CLR  => RAW_FIFO_sw_rst_i ,  -- async rst by s/w or RST from 40MHz MMCM lock signal
      D    => ctrl_RAW_ready_in       -- Data input
   ); 

--! @details
--! \verbatim
--! This module generates a synch signal synchronised to 40MHz and 280MHz clocks
--! one synch pulse is generated - this is 1 in 7 sync'ed to 40M clock
--! \endverbatim 
U1_gen_sync_280 : entity TOB_rdout_lib.gen_sync_280M
    Port map( 
        RST             => RST_i ,      -- RST from 40MHz MMCM lock signal
        clk_40M         => clk_40M_in , -- Clock 40MHz
        clk_280M        => clk_in_280M_i ,
        sync_280m_out   => sync_280m_i  -- this is 1 in 7 sync'ed to 40M clk
        );

--! \verbatim
--! The GEN_CHANNEL loop, generates 49 copies of the RAW data readout blocks within it,these are:
--!     PISO unit - to convert data from 224b to 7 x 32b words
--!     Dual Port RAM - scrolling memory to store data on every bunch crossing
--!         This is circular Dual Port RAM. It stores 7x32b words per Bunch number.
--!         the DPRAM is 36b wide, and bits 35 to 32 are used to store the 4b error flags for the corresponding MGT Channel.
--!         The Read and Write address have the correct offset in order to read the actual RAW data
--!         associated with the BCN at the time of arrival of L1A
--!    Derandomisation FIFO - to store the correct data word upon receiving an L1A   
--!         This is Derandomisation FIFO. It stores 7x32b words per L1A, which are read out of Circular memory.
--!         The FIFO is 36b wide, bits 35:32 are used to store the 4b error flags for the corresponding MGT Channel.
--! \endverbatim
GEN_CHANNEL : for i in 0 to chan_no-1 GENERATE      -- generate for 49 links (chan_no= 49)

-- Pipelines the input RAW data by one 40 MHz clock to move away from the real time circuit
proc1 : process (clk_40M_in)
begin
    if rising_edge (clk_40M_in) then 
        RAW_data_in_1dly(i)      <= RAW_data_in(i)  ; 
--        delayed_crc_error_i(i)   <= RAW_data_in_1dly(i)(226)  ; 
    end if;
  end process;
  
--  RAW_data_in_2dly(i)  <= delayed_crc_error_i(i) & RAW_data_in_1dly(i)(225 downto 0) ; -- delay the CRC error to be in line with input data

--!     PISO unit - to convert data from 224b to 7 x 32b words
	U2_PISO_RAW :  entity TOB_rdout_lib.PISO_RAW_data  -- convert 224b to 7 x 32b words for Circular DPR
		Port map ( 
			RST                 =>  RAW_FIFO_sw_rst_i ,  -- rst by s/w or RST from 40MHz MMCM lock signal
			RAW_link_data_in    =>  RAW_data_in_1dly(i), -- 227b  words for Circular DPR
			data_sync_in        =>  sync_280m_i,     -- this is 1 in 7 sync'ed to 40M clk
			clk_in_280M         =>  clk_in_280M_i,
			data_sync_out       =>  PISO_sync_out_i(i),
			data_out_valid      =>  PISO_data_out_valid_i(i),
			data_out            =>  PISO_data_out_i(i)          -- 7x32b RAW data to DPRAM + 4b error (use J)
			);    

--! RAW Data Circular DPRAM input 36b = ORed 4-bit links error flags + 32-bit Raw Data		
	U3_DPRAM_RAW_Data :  DPR_36b_512  -- it is in the IP package
		PORT map (
			clka   => clk_in_280M_i  ,          -- runs at 200M but writes 1 in 5 clks
			ena    => tied_to_vcc_i  ,          -- clk/rst en for DPRAM port A
			wea    => stdv(PISO_data_out_valid_i(i)), -- this is wr en using PISO_data_out_valid_i
			addra  => DPR_wr_addr_i_1dly    ,        -- TD_LOGIC_VECTOR(8 DOWNTO 0)
			dina   => PISO_data_out_i(i)  ,      -- 7x32b RAW data from DPRAM into FIFO + 4b error
			clkb   => clk_in_280M_i  ,
			enb    => DRP_rd_en_i_1dly  ,        -- clk/rst/addr en for DPRAM port B
			addrb  => DPR_rd_addr_i_1dly  ,      -- 
			doutb  => DPR_RAW_out_i(i)          -- 7x36b RAW data from DPRAM to FIFO (use J)
			);

--! RAW Data FIFO input 36b = ORed 4-bit links error flags + 32-bit Raw Data
	U4_FIFO_RAW_Data : FIFO_36b_512     -- the flags from these 49 FIFOs are not used
		PORT MAP (                      -- if backpressure goes on we stop capturing data
		  srst            => RAW_FIFO_sw_rst_i,   -- sys RST OR RAW_FIFO_sw_rst
		  clk             => clk_in_280M_i,
		  din             => DPR_RAW_out_i_1dly(i),
		  wr_en           => FIFO_wr_en_i_2dly,        --  i/p delayed version of FIFO_wr_en_ii
		  rd_en           => FIFO_rd_en_i(i),
		  dout            => FIFO_RAW_Data_dout_i(i),     -- array 49 of 36b = err + data   
		  valid           => FIFO_RAW_Data_valid_i(i) ,   -- 49 sync signals for 49 links
		  prog_full_thresh_assert => RAW_data_FIFO_pFULL_THRESH_assert_i,    -- 9b  
		  prog_full_thresh_negate => RAW_data_FIFO_pFULL_THRESH_negate_i,
		  data_count      => data_count_i(i),
		  prog_full       => FIFO_RAW_Data_prog_full_i(i),
		  full            => open ,
		  empty           => FIFO_RAW_Data_empty_i(i)
		);

-- AS there are too much path delay between DPRAM and FIFO untis on some input channels, 
-- a register has been added to allow for the path delay, and remove timing error.
    U5_1_clk : process (clk_in_280M_i)
      begin
       if rising_edge (clk_in_280M_i) then
       -- register signals to meet timing 
          FIFO_RAW_Data_dout_i_1dly(i)   <= FIFO_RAW_Data_dout_i(i)  ;     -- register the array 49 of 36b = err + data to remove timing error
          FIFO_RAW_Data_valid_i_1dly(i)  <= FIFO_RAW_Data_valid_i(i) ;     -- register the  49 sync signals for 49 links

          DPR_RAW_out_i_1dly(i)   <= DPR_RAW_out_i(i)  ;   -- register the array 49 of 36b = err + data to remove timing error
          link_error_flags_tmp(i) <= DPR_RAW_out_i_1dly(i)(35 downto 32);   -- array of 4 ERROR bits for each MGT input link 
          
          en_error_valid_1dly <= en_error_valid_i;  -- delay error flags valid by 1 clk
          en_error_valid_2dly <= en_error_valid_1dly;  -- delay error flags valid by 1 clk
          en_error_valid_3dly <= en_error_valid_2dly;  -- delay error flags valid by 1 clk
          en_error_valid_4dly <= en_error_valid_3dly;  -- delay error flags valid by 1 clk
       end if;
    end process;


--    link_error_flags_tmp(i) <= DPR_RAW_out_i_1dly(i)(35 downto 32); 

END GENERATE GEN_CHANNEL;

--proc1_ila0 : ila_227_bits
--PORT MAP (
--	clk => clk_40M_in,
--	probe0 => RAW_data_in(0),
--	probe1 => RAW_data_in_1dly(0)
--);

--proc1_ila47 : ila_227_bits
--PORT MAP (
--	clk => clk_40M_in,
--	probe0 => RAW_data_in(47),
--	probe1 => RAW_data_in_1dly(47)
--);

--proc1_ila48 : ila_227_bits
--PORT MAP (
--	clk => clk_40M_in,
--	probe0 => RAW_data_in(48),
--	probe1 => RAW_data_in_1dly(48)
--);


--! @details
--! \verbatim
--! OR all the input error flags together to create the 4-bit error signal to be added to the event 
--! Also check the 4-bit error flag to create 1-b flag to request data on error
--! \endverbatim 
U5_link_err: entity TOB_rdout_lib.link_errors_ORed
    generic map 
        ( N => 49 )
    port map
        ( clk_in            => clk_in_280M_i ,
          rst_in            => RAW_FIFO_sw_rst_i ,
          err_flag_in       => link_error_flags_tmp ,   -- array of 4 ERROR bits for each MGT input link
          en_error_valid_in => en_error_valid_3dly,        -- Enable capture of error flags from last RAW word
          err_flag_out      => link_err_4b_in_i ,       -- 4-bit link error input into RAW readout
          channel_error_map => channel_error_49b_i,     -- 49-bit channel error map to input into RAW readout
          req_err_rd_raw    => req_err_rd_raw_i         -- flag requesting RAW data readout on error
        );


--! this FDCE regitsters the L1A_in signal to act as write signal for Link Errors FIFO
U5_FDCE_inst : FDCE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q    => L1A_in_1dly,      -- Data output
      C    => clk_in_40M_i,      -- Clock input
      CE   => '1',             -- Clock enable input
      CLR  => '0' ,  -- async rst by s/w or RST from 40MHz MMCM lock signal
      D    => L1A_in_i       -- Data input
      );

    -- input to Link Error FIFO = requesting RAW data readout + ORed 4-bit link error + 49-bit channel error map
    link_error_flags_54b_i <= req_err_rd_raw_i & link_err_4b_in_i & channel_error_49b_i;

--! @details
--! \verbatim
--! This modules is the Link Errors FIFO.. 
--! It stores the following information for use in the construction of RAW Events:
--! Link Error FIFO input 54b = 1-bit RAW data readout on error + ORed 4-bit links error flags + 49-bit channel error map. 
--! \endverbatim 
U5_FIFO_link_err : FIFO_54b_512 
    PORT MAP (
      rst           => RAW_FIFO_sw_rst_i,   -- sys RST OR RAW_FIFO_sw_rst
      wr_clk        => clk_in_280M_i ,
      rd_clk        => clk_in_280M_i ,
      din           => link_error_flags_54b_i ,
      wr_en         => en_error_valid_4dly,
      rd_en         => BCN_FIFO_rd_en_i,
      dout          => FIFO_error_flags_54b_i,
      valid         => FIFO_error_flags_valid_i ,
      full          => open,
      empty         => link_err_FIFO_empty_i
    );

--! @details
--! \verbatim
--! This FSM is responsible for transfering the correct RAW data values from the Circular DPRAMs to de-randomisation FIFOs.
--! The control signals ensure all 49 DPRAMs are read and written to 49 FIFOs in parallel.
--! The write address offset for SPRAM ensures the data associated with required BCN is read out.
--! When the prog_FULL flag of de-randomisation FIFO is set to 1, no more data is written to this FIFO
--! \endverbatim 
U5_RAW_fsm :  entity TOB_rdout_lib.fsm_RAW_data_wr_to_DPR
    PORT map (
        CLK_280M               => clk_in_280M_i ,
        RST                    => RAW_FIFO_sw_rst_i ,    -- sys RST OR RAW_FIFO_sw_rst
        L1A_in                 => L1A_in_i ,
        pre_ld_wr_addr         => pre_ld_wr_addr_i ,
        DPR_rd_addr            => DPR_rd_addr_i ,
        DPR_wr_addr            => DPR_wr_addr_i ,
        DRP_rd_en              => DRP_rd_en_i ,
        FIFO_wr_en             => FIFO_wr_en_i,        -- o/p
        en_error_valid         => en_error_valid_i     -- o/p Enable capture of error flags from last RAW word
        );

--! @details
--! \verbatim
--! Module gen_full_flag monitors the occupancy level of one de-randomisation FIFO for channel 20.
--! Channel 20 has been chosen in order to be in the middle of the FPGA, but any other channel can be selected.
--! The programmable Assert and Negate registers control the occupancy levels at which the FULL Flag is asserted and negated.
--! \endverbatim 
U5b_gen_full_flag : entity TOB_rdout_lib.RAW_fifo_full_flag_gen
    Port map (
        rst_in           => RAW_FIFO_sw_rst_i,
        clk_in           => clk_in_280M_i ,
        fifo_data_count  => data_count_i(20), -- use 20 to be in the middle of pFPGA
        assert_count_in  => RAW_FIFO_FULL_THRESH_ASSERT,
        negate_count_in  => RAW_FIFO_FULL_THRESH_NEGATE,
        full_flag_out    => FIFO_RAW_Data_full_i
    );

--! pipeline registers to meet timing
U5c_1_clk : process (clk_in_280M_i)
  begin
   if rising_edge (clk_in_280M_i) then 
      DPR_rd_addr_i_1dly <= DPR_rd_addr_i ;
      DPR_wr_addr_i_1dly <= DPR_wr_addr_i ;
      DRP_rd_en_i_1dly   <= DRP_rd_en_i   ;
      FIFO_wr_en_i_1dly  <= FIFO_wr_en_i   ;     -- register wr signals for 49 links
      FIFO_wr_en_i_2dly  <= FIFO_wr_en_i_1dly AND  (NOT(FIFO_RAW_Data_prog_full_i(0))) ;
--      FIFO_wr_en_i_2dly  <= FIFO_wr_en_i_1dly AND  (NOT(FIFO_RAW_Data_prog_full_i(0))) when FPGA_NUMBER = 1 else
--                            FIFO_wr_en_i_1dly AND  (NOT(FIFO_RAW_Data_prog_full_i(10))) when FPGA_NUMBER = 2 else
--                            FIFO_wr_en_i_1dly AND  (NOT(FIFO_RAW_Data_prog_full_i(0))) when FPGA_NUMBER = 3 else
--                            FIFO_wr_en_i_1dly AND  (NOT(FIFO_RAW_Data_prog_full_i(20))) ; 
   end if;
end process;

    error_probe0 <= link_error_flags_tmp(8) & link_error_flags_tmp(7) & link_error_flags_tmp(6) & link_error_flags_tmp(5) &
                    link_error_flags_tmp(4) & link_error_flags_tmp(3) & link_error_flags_tmp(2) & link_error_flags_tmp(1) &
                    link_error_flags_tmp(0);
                    
    error_probe1 <= link_error_flags_tmp(17) & link_error_flags_tmp(16) & link_error_flags_tmp(15) & link_error_flags_tmp(14) &
                    link_error_flags_tmp(13) & link_error_flags_tmp(12) & link_error_flags_tmp(11) & link_error_flags_tmp(10) &
                    link_error_flags_tmp(9);
                    
    error_probe2 <= link_error_flags_tmp(26) & link_error_flags_tmp(25) & link_error_flags_tmp(24) & link_error_flags_tmp(23) &
                    link_error_flags_tmp(22) & link_error_flags_tmp(21) & link_error_flags_tmp(20) & link_error_flags_tmp(19) &
                    link_error_flags_tmp(18);
                    
    error_probe3 <= link_error_flags_tmp(48) & link_error_flags_tmp(47) & link_error_flags_tmp(46) & link_error_flags_tmp(45) &
                    link_error_flags_tmp(44) & link_error_flags_tmp(43) & link_error_flags_tmp(42) & link_error_flags_tmp(41) &
                    link_error_flags_tmp(40);
                    
--U5_ila_ink_err : ila_ipbus_fabric_rd_wr
--PORT MAP (
--    clk    => clk_in_280M_i,     -- RAW_TXOUTCLK,
--    probe0  => error_probe0 ,       -- 36b         
--    probe1  => error_probe1,  -- 36b
--    probe2(0) => req_err_rd_raw_i,   -- 1b
--    probe3(0) => L1A_in_i,           -- 1b
--    probe4(0) => link_err_4b_in_i(0) ,   -- 1b
--    probe5  => error_probe2,  -- 36b 
--    probe6  => error_probe3,        -- 36b
--    probe7(0) => link_err_4b_in_i(1),          -- 1b
--    probe8(0) => link_err_4b_in_i(2),          -- 1b
--    probe9(0) => link_err_4b_in_i(3)           -- 1b
--);

--U5_ila_error_fifo : ila_ipbus_fabric_rd_wr
--PORT MAP (
--    clk    => clk_in_280M_i,     -- RAW_TXOUTCLK,
--    probe0 => link_error_flags_54b_i(35 downto 0) ,       -- 36b         
--    probe1(17 downto 0)  => link_error_flags_54b_i(53 downto 36),  -- 36b
--    probe1(35 downto 18) => (others => '0'),  -- 36b
--    probe2(0) => en_error_valid_4dly,   -- 1b
--    probe3(0) => L1A_in_i,           -- 1b
--    probe4(0) => BCN_FIFO_rd_en_i ,   -- 1b
--    probe5 => FIFO_error_flags_54b_i(35 downto 0),  -- 36b 
--    probe6(17 downto 0)  => FIFO_error_flags_54b_i(53 downto 36),        -- 36b
--    probe6(35 downto 18) => (others => '0'),        -- 36b
--    probe7(0) => FIFO_error_flags_valid_i,             -- 1b
--    probe8(0) => link_err_FIFO_empty_i ,          -- 1b
--    probe9(0) => en_error_valid_3dly              -- 1b
--);

--U5C_ila_PISO_DATA_out : ila_ipbus_fabric_rd_wr 
--PORT MAP (
--    clk    => clk_in_280M_i,     -- RAW_TXOUTCLK,
--    probe0 => PISO_data_out_i(0) ,       -- 36b         
--    probe1 => PISO_data_out_i(1),  -- 36b
--    probe2(0) => '0',   -- 1b
--    probe3(0) => L1A_in_i,           -- 1b
--    probe4(0) => '0' ,   -- 1b
--    probe5 => PISO_data_out_i(2),  -- 36b 
--    probe6 => PISO_data_out_i(3),        -- 36b
--    probe7(0) => '0' ,   -- 1b
--    probe8(0) => '0' ,   -- 1b
--    probe9(0) => '0'     -- 1b
--);


    -- write L1A number and BC number into FIFO
    -- 1b FIFO_RAW_Data_prog_full_i + 1b ORed_err_flags + 1b Privilege Read + 12b BCN_ID_in + 8b ECID + 24b L1A_ID_in
    BCN_FIFO_Data_in_i <= FIFO_RAW_Data_prog_full_i(18) & ORed_err_flg_i & TTC_read_all_in & BCN_ID_in & L1A_ID_in ; 

--! @details
--! \verbatim
--! This is TTC FIFO for RAW Data Readout, which stores the following values:.
--! bit 46 = Safe Mode bit set by TTC FIFO full flag.
--! bit 45 = not used
--! bit 44 = privilege read flag
--! bits 43:32 = Bunch Crossing Number - generated in pFPGA
--! bits 31:24 = Extended L1A - received from CFPGA
--! bits 23:0  = L1A_ID - received from cFPGA
--! \endverbatim
U6_FIFO_BCN_L1A : FIFO_47b_512          -- the flags from this FIFO is used
    PORT MAP (
      rst           => RAW_FIFO_sw_rst_i,   -- sys RST OR RAW_FIFO_sw_rst
      wr_clk        => clk_in_40M_i, 
      rd_clk        => clk_in_280M_i,
      din           => BCN_FIFO_Data_in_i ,
      wr_en         => L1A_in_i,
      rd_en         => BCN_FIFO_rd_en_i,
      dout          => BCN_FIFO_Data_out_i,
      valid         => BCN_FIFO_valid_i ,
      prog_full_thresh_assert => BCN_FIFO_pFULL_THRESH_assert_i,
      prog_full_thresh_negate => BCN_FIFO_pFULL_THRESH_negate_i,
      prog_full     => BCN_FIFO_prog_full_i,
      full          => BCN_FIFO_full_i,
      empty         => BCN_FIFO_empty_i
    );

--! @details
--! \verbatim
--! This is FSM to read RAW data from de-randomisation FIFOs and write into link output FIFO.
--! This FSM also handles Header and Trailer construction.
--!
--! Under normal operation mode, only MGT channels with error flag set, are added to the RAW data event frame.
--! If there are no errors, then an event is generated with 2 header and 1 trailer words, and the payload is two 32-b words
--! containing the error flags of all 49 MGT channels.
--!
--! If Privilege Read flag or Read_RAW_all flag is set, then the RAW data for all MGT channels are added to the RAW data event frame.
--! Read_RAW_all flag is bit 1 of Test_Contol_Reg register.
--!
--! Under Safe Mode Operation, the occupancy of TTC FIFO are monitored.
--! If the occupancy reaches a programmable threshold, empty events are generated which consits of 2 header and 1 trailer words.
--!
--! The output of this FSM is:
--!     32-bit data word
--!     1-bit data is CHAR
--!     1-bit valid which is the write enable to Link Ouput FIFO
--! \endverbatim 
U7_rd_RAW_mux_fsm :  entity TOB_rdout_lib.fsm_RAW_to_muxPISO
    PORT map (
        RST                        => RAW_FIFO_sw_rst_i ,   -- sys RST OR RAW_FIFO_sw_rst
        hw_addr                    => hw_addr ,
        rdout_RAW_36b_in           => FIFO_RAW_Data_dout_i_1dly,       -- raw data output of fifo -- array 49 of 36b = err + data
        valid_RAW_in               => FIFO_RAW_Data_valid_i_1dly ,     -- RAW data is valid -- 49 sync signals for 49 links
        bcn_fifo_47b_in            => BCN_FIFO_Data_out_i,         -- BCN & L1A output of fifo
        valid_BCN_in               => BCN_FIFO_valid_i ,           -- BCN & L1A data is valid
        FIFO_error_flags_54b       => FIFO_error_flags_54b_i,   -- 1-b request RAW data readout + ORed 4-bit link error + 49-bit channel error map 
        clk_280M_in                => clk_in_280M_i , 
        RAW_FIFO_prog_full_in      => BCN_FIFO_prog_full_i,     -- i/p TTC FIFO prog full flag  
        LO_FIFO_prog_full_in       => Link_output_FIFO_prog_full_i,   -- if link o/p FIFO pFULL discard i/p data 
        LO_FIFO_data_count_in      => Link_output_FIFO_RAW_rd_data_count_i,  -- link o/p FIFO data count 
        LO_FIFO_full_in            => Link_output_FIFO_full_i,       --  link o/p full flag 
        frame_cntr_en              => frame_cntr_en_i ,       -- enable frame counter count up
        raw_rd_all_in              => raw_rd_all_in ,         -- readout all raw data links
        RAW_rdout_fifo_rd_en_out   => FIFO_rd_en_i ,          -- rd enable to TOB FIFOs        
        BCN_fifo_rd_en_out         => BCN_FIFO_rd_en_i ,      -- rd enable to TOB FIFOs
        BCN_FIFO_empty             => BCN_FIFO_empty_i ,      -- input BCN L1A_ID FIFO empty flag
        link_err_FIFO_empty        => link_err_FIFO_empty_i , -- input link error FIFO empty flag
        RAW_out_valid              => RAW_out_valid_i ,       -- sorted TOBs data valid to U7_Link_output_FIFO
        RAW_out_is_char            => RAW_out_is_char_i,      -- sorted TOBs is CHAR to U7_Link_output_FIFO
        RAW_out                    => RAW_out_i              -- sorted TOBs valid to U7_Link_output_FIFO
        );  


    FIFO_RAW_in_i <= RAW_out_is_char_i & RAW_out_i;  -- 1b char and 32b data

--! @details
--! \verbatim 
--! This is RAW Link Output FIFO, which stores complete RAW events (frames) ready to be transmitted to cFPGA via an MGT link at 11.2Gbps.
--! The output of FIFO is 32-bit data word, and 1-bit data is CHAR, which goes directly to MGT under FSM control.
--!
--! The Frames are only transmitted when cFPGA indicates it is ready to receive data by setting ctrl_RAW_ready signal to 1.
--! If ctrl_RAW_ready signal is set to 0 by cFPGA, this indicates cFPGA is not ready to receive data,
--! in this case events accumulate in Link Output FIFO until occupancy reaches a pre-define level of prog_FULL,
--! at this time, Link Output FIFO stops receiving data.
--!
--! Writing to LO FIFO is controlled by fsm_RAW_to_muxPISO FSM
--! Reading from LO FIFO is controlled by FIFO_to_MGT_RAW_FSM FSM
--! \endverbatim 
U8_RAW_Link_output_FIFO : FIFO_33b_8192
    PORT MAP (
        clk         => clk_in_280M_i,
        srst        => RAW_FIFO_sw_rst_i,   -- sys RST OR RAW_FIFO_sw_rst
        din         => FIFO_RAW_in_i,
        wr_en       => RAW_out_valid_i,
        rd_en       => Link_output_FIFO_rd_en_i,        
        dout        => Link_output_FIFO_RAW_out_i,
        full        => Link_output_FIFO_full_i,
        empty       => Link_output_FIFO_empty_i,
        prog_full   => Link_output_FIFO_prog_full_i,
        valid       => Link_output_FIFO_RAW_valid_i,
        prog_full_thresh_assert     => Link_output_FIFO_RAW_pfull_thresh_assert_i ,   -- 13b
        prog_full_thresh_negate     => Link_output_FIFO_RAW_pfull_thresh_negate_i ,   -- 13b
        data_count  => Link_output_FIFO_RAW_rd_data_count_i     -- 13b occupancy of Link_output_FIFO_RAW link MGT FIFO
    );
    
    Link_output_FIFO_RAW_rd_data_count <= Link_output_FIFO_RAW_rd_data_count_i ;
    
    
--U8A_ila_RAW_LO_FIFO_wr : ila_ipbus_fabric_rd_wr
--PORT MAP (
--	clk => clk_in_280M_i ,
--	probe0 => FIFO_RAW_in_i(31 downto 0) ,     -- 36b  L O FIFO Data in
--	probe1 => op_fifo_frame_count_vec ,    -- 36b  
--	probe2 => Di_is_char_vec ,       -- 1b   L O FIFO is char
--	probe3 => FIFO_wr_en_vec,              -- 1b L O FIFO wr en
--	probe4 => L1A_in_i_vec,                 -- 1b
--	probe5 => (others => '0' ),    --32b L O FIFO Data out
--	probe6 => (others => '0' ),    -- wasFIFO_rd_en_i_vec  ,             -- 36b
--	probe7 => (others => '0' ) ,              -- 1b L O FIFO is char
--	probe8 => (others => '0' ) ,             -- 1b rd en
--    probe9 => Link_output_FIFO_empty_vec              -- 1b
--);


--U8B_ila_RAW_LO_FIFO_rd : ila_ipbus_fabric_rd_wr
--PORT MAP (
--	clk => RAW_TXOUTCLK ,
--	probe0 => Link_output_FIFO_RAW_out_i(31 downto 0) ,     -- 36b  L O FIFO Data in
--	probe1 => (others => '0' ) ,    -- 36b  
--	probe2(0) => Link_output_FIFO_RAW_valid_i ,       -- 1b   L O FIFO is char
--	probe3 => Do_is_char_vec,              -- 1b L O FIFO wr en
--	probe4 => L1A_in_i_vec,                 -- 1b
--	probe5 => (others => '0' ),    --32b L O FIFO Data out
--	probe6 => SPY_mem_wr_addr_ila  ,             -- 36b
--	probe7 => Link_output_FIFO_rd_en_vec ,    -- 1b L O FIFO rd
--	probe8(0) => SPY_mem_wr_addr_en_ila ,             -- 1b rd en
--    probe9 => Link_output_FIFO_empty_vec             -- 1b
--);

-- This FSM reads RAW frames from Link Output FIFO and writes into MGT to transmit to cFPGA.
-- This FSM handles one full frame at a time without pausing.
-- It monitors the RAW_frame_counter to find out if there are Frames waiting to be transmitted to cFPGA.
-- The Frames are only transmitted when cFPGA indicates it is ready to receive data by setting ctrl_RAW_ready signal to 1.
-- If ctrl_RAW_ready signal is set to 0 by cFPGA, this indicates cFPGA is not ready to receive data,
-- in this case events accumulate in Link Output FIFO until occupancy reaches a pre-define prog_FULL level,
-- at this time, Link Output FIFO stops receiving data.
--! @details
--! \verbatim
--! \endverbatim 
U9_RAW_Link_output_FIFO_FSM : entity TOB_rdout_lib.FIFO_to_MGT_RAW_FSM
   Port map ( 
      TOB_FIFO_sw_rst          =>  RAW_FIFO_sw_rst_i ,    -- sys RST OR RAW_FIFO_sw_rst
      clk_in_280M              =>  RAW_TXOUTCLK,
      ctrl_RAW_ready_in        =>  ctrl_RAW_ready_i ,
      frame_counter            =>  op_fifo_frame_count_i,
      FIFO_MGT_TOBs            =>  Link_output_FIFO_RAW_out_i,
      FIFO_MGT_TOB_valid       =>  Link_output_FIFO_RAW_valid_i ,
      LO_FIFO_full             =>  Link_output_FIFO_full_i,
      LO_FIFO_empty            =>  Link_output_FIFO_empty_i,
      LO_FIFO_prog_full        =>  Link_output_FIFO_prog_full_i,
      FIFO_MGT_rd_en           =>  Link_output_FIFO_rd_en_i,   -- output
      frame_counter_dec_en     =>  frame_cntr_dec_en_i,
      RAW_data_out             =>  RAW_data_out_i,         	-- RAW event to MGT
      RAW_data_is_char         =>  RAW_out_to_MGT_is_char_i,   -- RAW data is char
      RAW_data_out_valid       =>  RAW_data_out_valid_i  
      );

--! @details
--! \verbatim 
--! The RAW_frame_counter is a 12-bit generic up/down counter to count the number of complete Frames in Link Output FIFO to be transmitted to cFPGA.
--! When one complete Frame (event) is written to LO FIFO, this counter is incremented.
--! When one complete Frame (event) is read out LO FIFO, this counter is decremented.
--! If the counter is ZERO, no data is available to be send to cFPGA.
--! \endverbatim 
U10_RAW_frame_counter : entity TOB_rdout_lib.cntr_up_dn_generic
   generic map (
        width  => 12    -- count up to 4096 events in Link Output FIFO
       )
   Port map ( 
      CE   =>  tied_to_vcc_i  ,
      CLK  =>  clk_in_280M_i ,
      RST  =>  RAW_FIFO_sw_rst_i ,  -- sys RST OR RAW_FIFO_sw_rst
      UP   =>  frame_cntr_en_i ,
      DOWN =>  frame_cntr_dec_en_i ,   -- this is a 280 MHz signal
      Q    =>  op_fifo_frame_count_i 
      );

--! delay data to SPY RAM
U11_clk_proc : process (clk_in_280M_i)
    begin
        if clk_in_280M_i'event and clk_in_280M_i = '1' then
            RAW_data_out_1dly    <= RAW_data_out_i;        -- data SPY RAM
            RAW_data_out_MGT_i   <= RAW_data_out_1dly;     -- data to MGT
            RAW_out_char_MGT_1dly <= RAW_out_to_MGT_is_char_i;
            RAW_out_char_MGT_i    <= RAW_out_char_MGT_1dly;
            RAW_out_valid_1dly   <= RAW_data_out_valid_i;
        end if;
    end process;

--    SPY_FIFO_RAW_in_i       <= "000" & Link_output_FIFO_RAW_out_i ;
    SPY_mem_wr_addr_en_i <= (RAW_out_valid_1dly AND enable_raw_spy_mem_wr);

--! @details
--! \verbatim 
--! The RAW_SPY_mem is a Dual Port Memory with IPBus interface.
--! It is possible to read/write to/from DPRAM using the IPBus interface.
--! The SPY Memory captures the data that is read out of Link Output FIFO to MGT.
--! The SPY Memory is designed to fill up and then stops accepting new data, 
--! therefore the old data is not written over.
--! \endverbatim 
U12_RAW_SPY_mem : entity ipbus_lib.ipbus_dpram
	generic map(
		ADDR_WIDTH => 11      -- DPRAM 512 locations
	)
	port map(
		clk		=> ipb_clk,
		rst		=> '0'  ,
		ipb_in 	=> ipbus_in_raw_dpram ,    -- i/p signal going to RAW SPY DPRAM
		ipb_out	=> ipbus_out_raw_dpram ,    -- o/p signal coming from RAW SPY DPRAM
		rclk	=> RAW_TXOUTCLK ,
		we		=> SPY_mem_wr_addr_en_i ,   -- wr addr en
		d		=> RAW_data_out_1dly,     -- wr data
		q		=> q_int ,
		addr	=> SPY_mem_wr_addr_i     -- wr addr
	);

     RST_raw_spy_mem_wr_addr_i <= (RST_i  OR RST_spy_mem_wr_addr ) ;

--! @details
--! \verbatim 
--! The spy_mem_wr_addr is a generic counter that provides the write address to RAW SPY Memory.
--! The spy memory address is 11-bits wide, so the depth of memory is 2048.
--! This implies the RAW Spy Memory can hold just over 5 Full RAW Frames. 
--! The write address of the RAW_SPY_mem is reset to ZERO by asserting RST_spy_mem_wr_addr or system RST signals.
--! The signal RST_spy_mem_wr_addr is bit 5 of register rdout_pulse_reg.
--! The rdout_pulse_reg register is pulsed, and after 1 clock cycle all bits are reset to Zeros.
--! \endverbatim 
U13_spy_mem_wr_addr : entity TOB_rdout_lib.cntr_generic
    generic map (
         width  => 11
        )
    Port map ( 
       CE   =>  RAW_data_out_valid_i  ,  -- if a valid data, then increment the address
       CLK  =>  RAW_TXOUTCLK ,
       RST  =>  RST_raw_spy_mem_wr_addr_i ,
       Q    =>  SPY_mem_wr_addr_i 
       );

--! @details
--! \verbatim
--! Process U14_stop_wr disables the write enable signal to Spy Memory when it is full.
--! \endverbatim 
U14_stop_wr : Process (RAW_TXOUTCLK)
   begin
     if rising_edge (RAW_TXOUTCLK) then
        if (RST_raw_spy_mem_wr_addr_i = '1' ) then  
           enable_raw_spy_mem_wr <= '1' ;   -- if not full, en data wr
        else
           if SPY_mem_wr_addr_i = "11111111111" then      -- if terminal count
            enable_raw_spy_mem_wr <= '0' ;     -- disable memory wr to prevent data corruption
           end if;
        end if;
     end if ;
   end process U14_stop_wr;

end RTL;
