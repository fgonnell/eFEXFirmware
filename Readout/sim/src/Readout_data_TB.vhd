--! @file
--! @brief TOB/XTOB and Calorimeter Readout Test Bench for process FPGA
--! @details 
--! This module received Sorted TOBs & XTOBs data and produces events of 32b words for transmission to control FPGA
--! It also generates the 32 bit Calorimeter RAW data into SPY memory.
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.numeric_std.ALL;

--library TOB_rdout_sim;

library TOB_rdout_lib;
use TOB_rdout_lib.TOB_rdout_ip_pkg.all;
use TOB_rdout_lib.data_type_pkg.all;
use TOB_rdout_lib.ipbus_decode_efex_readout.all;	-- decoder package

library ipbus_lib;
use ipbus_lib.ipbus.all;

library algolib;
use algolib.AlgoDataTypes.all;

library infrastructure_lib;
use infrastructure_lib.all;

--! @copydoc Readout_data_TB.vhd
entity Readout_data_TB is
--  Port ( );
end Readout_data_TB;

architecture Behavioral of Readout_data_TB is

    component ClockWizard
    port
     (-- Clock in ports
      -- Clock out ports
      clk200          : out    std_logic;
      load          : out    std_logic;
      clk280          : out    std_logic;
      clk40          : out    std_logic;
      -- Status and control signals
      reset             : in     std_logic;
      locked            : out    std_logic;
      clk_in1_p         : in     std_logic;
      clk_in1_n         : in     std_logic
     );
    end component;

  component clk_wiz_1
    port
      (clk40  : in  std_logic;
       clk160 : out std_logic;
       reset  : in  std_logic;
       locked : out std_logic
       );
  end component;
  
    signal  tied_to_ground_i        : std_logic;
    signal  tied_to_vcc_i           : std_logic;
    signal  tied_high_i             : std_logic_vector(0 downto 0);
    signal  locked_i                : std_logic;

    signal  L1A_in_i                : std_logic ;
    signal  L1A_in_1_dly            : std_logic ;
    signal  BCN_in_i                : STD_LOGIC_VECTOR (11 downto 0) := (others => '0' ); 
    signal  L1A_ID_i                : STD_LOGIC_VECTOR (31 downto 0)  := (others => '0' );
    signal  L1A_ID_Event_i          : STD_LOGIC_VECTOR (31 downto 0)  := (others => '0' );
    
--    signal link_error_flags_i      : link_error_type;      -- array 49 x 4 bit per link
    signal  link_error_flags_54b_i : STD_LOGIC_VECTOR (53 downto 0);

    signal  pre_ld_wr_addr_i        : STD_LOGIC_VECTOR (8 downto 0);

    signal sys_rst                  : std_logic ;
    signal clk_40M                  : std_logic := '0'; 
    signal clk_40M_in               : std_logic := '0'; 
    signal clk_40M_in_n             : std_logic ;
    constant clk_40_period          : time := 25 ns;
    constant clk_160_period         : time := 6.25 ns;
    signal clk_40M_ld               : std_logic := '0';
    signal clk_160M                 : std_logic := '0';
    signal clk_200M                 : std_logic := '0';
    signal clk_280M                 : std_logic := '0';
    
    signal ctrl_TOBs_ready_i       : std_logic := '0';
    signal ctrl_RAW_ready_i        : std_logic := '0';
    signal TOB_eg_512b_in_i        : AlgoXOutput;        -- array 8 x 64b words XTOB e/g
    signal TOB_tau_512b_in_i       : AlgoXOutput;        -- array 8 x 64b words XTOB tau

    signal TX_TOB_topo_DATA_OUT_i         : std_logic_vector(31 downto 0) ;      -- TOB tau
    signal TXCTRL_TOB_topo_OUT_i          : std_logic_vector(3 downto 0) ;       -- TOB tau
    signal  ii        : integer range 0 to 50 := 0;

    signal  hw_addr_i               : std_logic_vector(1 downto 0) ; 

    signal  TX_DATA_OUT_i           : std_logic_vector(226 downto 0);
    
    signal  TXCTRL_OUT_i            : std_logic_vector(3 downto 0) ;
    
    signal TOB_out_to_MGT_is_char_i : std_logic := '0';
    signal TOB_out_to_MGT_i         : STD_LOGIC_VECTOR (31 downto 0);
    signal RAW_out_to_MGT_is_char_i : std_logic := '0';
    signal RAW_data_out_i           : STD_LOGIC_VECTOR (31 downto 0);
    signal RAW_data_in_i            : RAW_data_227_type;    -- array 49 x 224b input frames
    signal RAW_data_in_tmp          : RAW_data_227_type;    -- array 49 x 227b input frames
    
    signal back_pressure_in_i, TTC_read_all_i       : std_logic := '0';


    signal TX_TOB_eg_DATA_OUT_i      : std_logic_vector(31 downto 0) ;       -- TOB e/g
    signal TX_TOB_eg_DATA_OUT_dly_1  : std_logic_vector(31 downto 0) ;       -- TOB e/g
    signal TX_TOB_eg_DATA_OUT_dly_2  : std_logic_vector(31 downto 0) ;       -- TOB e/g
    signal TX_TOB_eg_DATA_OUT_dly_3  : std_logic_vector(31 downto 0) ;       -- TOB e/g
    signal TXCTRL_TOB_eg_OUT_i       : std_logic_vector(3 downto 0) := "0000";        -- TOB e/g
    signal TOB_eg_Valid_flg_in_i     : std_logic_vector(7 downto 0) := "00000000" ;        -- TOB e/g
    signal TX_TOB_tau_DATA_OUT_i     : std_logic_vector(31 downto 0) ;      -- TOB tau
    signal TXCTRL_TOB_tau_OUT_i      : std_logic_vector(3 downto 0) ;       -- TOB tau
    signal TOB_tau_Valid_flg_in_i    : std_logic_vector(7 downto 0) := "00000000" ;       -- TOB tau
    signal link_err_flg_4b_i         : std_logic_vector(3 downto 0) := "0000" ;
    signal rand_out_40M              : std_logic_vector(15 downto 0) ;
    signal rand_out_280M             : std_logic_vector(15 downto 0) ;
    signal DPR_locations_to_rd_i    : STD_LOGIC_VECTOR (2 downto 0) ;

    signal  RAW_FIFO_pFULL_THRESH_ASSERT_i   : STD_LOGIC_VECTOR (8 downto 0) ;   -- MGT_FIFO
    signal  RAW_FIFO_pFULL_THRESH_NEGATE_i   : STD_LOGIC_VECTOR (8 downto 0) ;   -- MGT_FIFO    

    signal  BCN_FIFO_pFULL_THRESH_assert_i   : STD_LOGIC_VECTOR (8 downto 0) ;   -- MGT_FIFO
    signal  BCN_FIFO_pFULL_THRESH_negate_i   : STD_LOGIC_VECTOR (8 downto 0) ;   -- MGT_FIFO    

    signal  Link_output_FIFO_RAW_rd_data_count_i        : STD_LOGIC_VECTOR (12 downto 0) ;   -- occupancy of RAW output link MGT FIFO
    signal  Link_output_FIFO_RAW_wr_data_count_i        : STD_LOGIC_VECTOR (12 downto 0) ;   -- occupancy of RAW output link MGT FIFO
    signal  Link_output_FIFO_RAW_pfull_thresh_assert_i   : STD_LOGIC_VECTOR (12 downto 0) ;   -- MGT_FIFO	
    signal  Link_output_FIFO_RAW_pfull_thresh_negate_i   : STD_LOGIC_VECTOR (12 downto 0) ;   -- MGT_FIFObegin

    signal  XTOB_eg_FIFO_pFULL_THRESH_assert_i	: STD_LOGIC_VECTOR (8 downto 0) ;
    signal  XTOB_eg_FIFO_pFULL_THRESH_negate_i   : STD_LOGIC_VECTOR (8 downto 0) ;

    signal  XTOB_tau_FIFO_pFULL_THRESH_assert_i	: STD_LOGIC_VECTOR (8 downto 0) ;
    signal  XTOB_tau_FIFO_pFULL_THRESH_negate_i  : STD_LOGIC_VECTOR (8 downto 0) ;

    signal  T_TOBs_FIFO_pFULL_THRESH_assert_i	: STD_LOGIC_VECTOR (8 downto 0);
    signal  T_TOBs_FIFO_pFULL_THRESH_negate_i	: STD_LOGIC_VECTOR (8 downto 0);
    
    signal  RAW_FIFO_FULL_THRESH_ASSERT_i	: STD_LOGIC_VECTOR (8 downto 0);
    signal  RAW_FIFO_FULL_THRESH_NEGATE_i	: STD_LOGIC_VECTOR (8 downto 0);
    signal  RAW_FIFO_data_count_i	: STD_LOGIC_VECTOR (8 downto 0);
    
    signal  Link_output_FIFO_pFULL_THRESH_assert_i   : STD_LOGIC_VECTOR (12 downto 0) ;
    signal  Link_output_FIFO_pFULL_THRESH_negate_i   : STD_LOGIC_VECTOR (12 downto 0) ;
    signal  Link_output_FIFO_rd_data_count_i        : STD_LOGIC_VECTOR (12 downto 0) ;   -- occupancy of RAW output link MGT FIFO

	signal  T_TOB_FIFO_data_count_i		: STD_LOGIC_VECTOR (8 downto 0);
	signal  XTOB_eg_FIFO_data_count_i	: STD_LOGIC_VECTOR (8 downto 0);
	signal  XTOB_TAU_FIFO_DATA_COUNT_i	: STD_LOGIC_VECTOR (8 downto 0);
	        
--    signal  frame_count_i           :  STD_LOGIC_VECTOR (31 downto 0);      -- TOB data Frame counter
    signal  RAW_frame_count_i       :  STD_LOGIC_VECTOR (31 downto 0);      -- calo data Frame counter
--    signal  RDOUT_PULSE_REG_i       : STD_LOGIC_VECTOR (31 downto 0);
    signal  RAW_WR_ADDR_OFFSET_i        : STD_LOGIC_VECTOR (8 downto 0);
    signal  TOB_SPY_mem_wr_addr_i       : STD_LOGIC_VECTOR (10 downto 0);        -- calo data SPY memory wr addr pointer  
    signal  TOB_SPY_mem_rd_addr_en_i    : STD_LOGIC := '0';                      -- calo data SPY memory rd addr en      
    signal  TOB_SPY_mem_rd_addr_i       : STD_LOGIC_VECTOR (10 downto 0) ;       -- calo data SPY memory rd addr         
    signal  TOB_SPY_mem_rd_data_i       : STD_LOGIC_VECTOR (31 downto 0) ;       -- calo data SPY memory rd data         

    signal  RAW_SPY_mem_wr_addr_i       : STD_LOGIC_VECTOR (10 downto 0) ;       -- calo data SPY memory wr addr pointer  
        
    signal  ipb_rst_i       : STD_LOGIC ; 
    signal  ipb_clk_i       : STD_LOGIC ; 
    signal  IPb_in_i, RAW_IPb_in_i, TOB_IPb_in_i  : ipb_wbus;      -- The signals going from master to slaves  
    signal  IPb_outI, RAW_IPb_outI, TOB_IPb_outI  : ipb_rbus;     -- The signals going from slaves to master    
	signal  ipbus_out_raw_dpram_i : ipb_wbus;     -- signal going to RAW SPY DPRAM
    signal  ipbus_in_raw_dpram_i  : ipb_rbus;     -- signal coming from RAW SPY DPRAM
	signal  ipbus_out_tob_dpram_i : ipb_wbus;     -- signal going to TOB SPY DPRAM
    signal  ipbus_in_tob_dpram_i  : ipb_rbus;     -- signal coming from TOB SPY DPRAM
    
    signal TOB_data_FIFO_flags_i  : STD_LOGIC_VECTOR (11 downto 0) := X"000"; 
    signal RAW_data_FIFO_flags_i  : STD_LOGIC_VECTOR (8 downto 0) := "000000000";
    signal  reg1 , reg2           : std_logic := '0'; 
    signal  cntr_load_en_i        : std_logic;
    signal  L1A_sw_pulse_i        : std_logic := '0'; 
--    signal  L1A_in_dly            : STD_LOGIC ; -- used to delay L1A by 1 clk so the event counter is updated before it is saved.  
    
    signal  req_err_rd_raw_i      : std_logic := '0' ;
    signal  raw_rd_all_in_i       : std_logic := '0' ;
    signal  TTC_L1A_ID_EXT_in_i   : std_logic_vector (7 downto 0);
    signal  bcn_cntr              : std_logic_vector (11 downto 0);

begin

    tied_to_ground_i        <=   '0';
    tied_to_vcc_i           <=   '1';
    tied_high_i             <= (others => '1') ;
        
--    pre_ld_wr_addr_i      <= "000010000" ;        -- latency pre load for DRP wr address =16
    
    hw_addr_i <= "01" ;
    
    raw_rd_all_in_i <= '0' ;    -- 1 = read all raw data

    TTC_L1A_ID_EXT_in_i   <= (others => '0') ;    -- connect to TTC input
    

U0_ld_en : process 
    begin
       cntr_load_en_i <= '0' ;
       wait for 3.3 us ;
       cntr_load_en_i <= '1' ;
       wait for 3.5 ns ;
       cntr_load_en_i <= '0' ;
       wait;
    end process;

U0_clk_40M : process
      begin
         clk_40M_in <= '1';
         wait for clk_40_period/2;  --for 25 ns signal is '0'.
         clk_40M_in <= '0';
         wait for clk_40_period/2;  --for next 25 ns signal is '1'.
    end process;

U0_ctrl_ready : process 
    begin
       ctrl_TOBs_ready_i <= '0' ;
       ctrl_RAW_ready_i  <= '0' ;
       wait for 10 us ;
       ctrl_TOBs_ready_i <= '1' ;
       ctrl_RAW_ready_i  <= '1' ;
       wait for 5 us ;
       ctrl_TOBs_ready_i <= '0' ;
       ctrl_RAW_ready_i  <= '0' ;
       wait for 5 us ;
       ctrl_TOBs_ready_i <= '1' ;
       ctrl_RAW_ready_i  <= '1' ;
       wait for 5 us ;
       ctrl_TOBs_ready_i <= '0' ;
       ctrl_RAW_ready_i  <= '0' ;
       wait for 5 us ;
       ctrl_TOBs_ready_i <= '1' ;
       ctrl_RAW_ready_i  <= '1' ;
       wait for 5 us ;
       ctrl_TOBs_ready_i <= '0' ;
       ctrl_RAW_ready_i  <= '0' ;
       wait for 5 us ;
       ctrl_TOBs_ready_i <= '1' ;
       ctrl_RAW_ready_i  <= '1' ;
       wait;
    end process;

  clk160_gen : clk_wiz_1
    port map (
      -- Clock in ports
      reset  => '0',
      clk40  => clk_40M_in,
      -- Clock out ports
      clk160 => clk_160M,
      -- Status and control signals
      locked => open
      );



    clk_40M_in_n <= NOT clk_40M_in ;

U2_clk_tst : ClockWizard
  port map (

    -- Clock in ports
    clk_in1_p => clk_40M_in,
    clk_in1_n => clk_40M_in_n,
    -- Clock out ports
    clk200    => clk_200M,
    clk40     => clk_40M,
    clk280    => clk_280M,
    reset     => '0',
    load      => clk_40M_ld,
    -- Status and control signals
    locked    => locked_i
    );

    sys_rst <= NOT locked_i ;       -- system reset

                
U3_TOB_eg_data_gen : entity work.gen_32b_XTOBs_tst  -- get test data TOB e/g
    generic map
    (
        WORDS_IN_BRAM =>   721 ,
        FILE_NAME     => "TOBs_rom_init_tx_tst.dat" 
    )   
    port map
    (
        -- User Interface
        TX_DATA_OUT     => TX_TOB_eg_DATA_OUT_i ,
        TXCTRL_OUT      => TXCTRL_TOB_eg_OUT_i  ,
        -- System Interface
        USER_CLK        => clk_200M ,      
        SYSTEM_RESET    => sys_rst  
    );
    
U4_TOB_tau_data_gen : entity work.gen_32b_XTOBs_tst  -- get test data TOB e/g
    generic map
    (
        WORDS_IN_BRAM =>   721 ,
        FILE_NAME     => "TOBs_rom_init_tx_tst.dat" 
    )   
    port map
    (
        -- User Interface
        TX_DATA_OUT     => TX_TOB_tau_DATA_OUT_i ,
        TXCTRL_OUT      => TXCTRL_TOB_tau_OUT_i  ,
        -- System Interface
        USER_CLK        => clk_200M ,      
        SYSTEM_RESET    => sys_rst  
        );

U5_TOB_topo_data_gen : entity work.gen_32b_TOBs_tst  -- get test data TOPO TOBs
    generic map
    (
        WORDS_IN_BRAM =>   526 ,
        FILE_NAME     => "TOBs_topo_rom_init_tx_tst.dat"  
    )   
    port map
    (
        -- User Interface
        TX_DATA_OUT     => TX_TOB_topo_DATA_OUT_i ,
        TXCTRL_OUT      => TXCTRL_TOB_topo_OUT_i  ,
        -- System Interface
        USER_CLK        => clk_280M ,      
        SYSTEM_RESET    => sys_rst  
    );
        
U6_RAW_data : entity work.gen_224b_raw_data_tst  -- get test data 
    generic map
    (
        WORDS_IN_BRAM =>   515,
        FILE_NAME     => "RAW_data_rom_init_tx_tst.dat"
    )   
    port map
    (
        -- User Interface
        TX_DATA_OUT     =>  TX_DATA_OUT_i ,     -- std_logic_vector(226 downto 0)
        RAW_data_out    =>  RAW_data_in_i ,     -- array 49 x 228b input frames
--        TXCTRL_OUT      =>  TXCTRL_OUT_i  ,     -- start bit for data valid
        TXCTRL_OUT      =>  open  ,     -- start bit for data valid
        -- System Interface
        USER_CLK        => clk_40M ,     -- data changes at 200MHz 
        SYSTEM_RESET    => sys_rst  
    );
    

U7_rand_40M : entity work.ran_num_gen_16b
  Port map (
      rst         =>  sys_rst ,
      clk         =>  clk_40M , 
      MAC_addr    =>  X"abcd0001" ,
      rand_out    =>  rand_out_40M
      );
    
U8_rand_280M : entity work.ran_num_gen_16b
    Port map (
        rst         =>  sys_rst  ,
        clk         =>  clk_280M ,
        MAC_addr    =>  X"abcd0002" ,
        rand_out    =>  rand_out_280M
        );
          

--U9_L1A_gen : entity TOB_rdout_sim.L1A_gen_tst 
--    Port map
--    (   
--        clk_in   => clk_40M ,
--        rst      => sys_rst ,
--        L1A_out  => L1A_in_i
--        );


U9a_L1A_Gen : entity work.L1A_gen_tst(clk_pulse)
--U9a_L1A_Gen : entity TOB_rdout_sim.L1A_gen_tst(time_pulse)
  port map (
     clk_in    => clk_40M ,  
     rst       => sys_rst ,
     L1A_pulse => L1A_in_i   -- 1000 Hz
     );

--U10_BCN_gen : entity TOB_rdout_sim.cntr_ld_12b
--    generic map (
--        T_count       => X"DEC"   ,  -- bunch count to 3564
--        ld_count      => X"001"
--        )

--    Port map ( 
--       CE         => '1' ,
--       CLK        => clk_40M ,
--       RST        => '0' ,
--       LD         => sys_rst ,
--       Q          => BCN_in_i
--       );

---------bcn generator
  U10_BCN_gen : process(clk_40M)
    variable bcn_count : unsigned (11 downto 0) := (others => '0');
  begin

    if clk_40M' event and clk_40M = '1' then
      if sys_rst = '1'  then
        bcn_count := ("000000000001");
      else
        if bcn_count = 3563 then
            bcn_count := (others => '0');
        else
           bcn_count := bcn_count + 1;
        end if;
      end if;
      BCN_in_i <= std_logic_vector(bcn_count);
    end if;
  end process;

--! delay L1A by 1 clk for counter to count up
proc1 : process (clk_40M)
begin
    if rising_edge (clk_40M) then
        -- register the RAW data by one clock to move away from the real time circuit
        L1A_in_1_dly <= L1A_in_i ; 
    end if;
  end process;

--! L1A counter 
U11_L1A_ID_gen : entity TOB_rdout_lib.cntr_L1A_generic
   Port map ( 
      CE   =>  L1A_in_1_dly,   -- L1A pulse from the test generator
--      CE   =>  L1A_sw_pulse_i       ,    -- L1A_sw_pulse_i under s/w control
      CLK  =>  clk_40M ,
      RST  =>  sys_rst,
      Q    =>  L1A_ID_i(23 downto 0)
      ); 

--! delay by 1 clk of 280MHz
proc2 : process (clk_200M)
begin
    if rising_edge (clk_200M) then
        -- register the RAW data by one clock to move away from the real time circuit
        TX_TOB_eg_DATA_OUT_dly_1 <= TX_TOB_eg_DATA_OUT_i  ; 
        TX_TOB_eg_DATA_OUT_dly_2 <= TX_TOB_eg_DATA_OUT_dly_1  ; 
        TX_TOB_eg_DATA_OUT_dly_3 <= TX_TOB_eg_DATA_OUT_dly_2  ; 
    end if;
  end process;

    TOB_eg_512b_in_i(0) <=   TX_TOB_eg_DATA_OUT_dly_1 & TX_TOB_eg_DATA_OUT_i;
    TOB_eg_512b_in_i(1) <=   TX_TOB_eg_DATA_OUT_dly_3 & TX_TOB_eg_DATA_OUT_dly_2;
    TOB_eg_512b_in_i(2) <=   TX_TOB_eg_DATA_OUT_dly_1 & TX_TOB_eg_DATA_OUT_i;
    TOB_eg_512b_in_i(3) <=   TX_TOB_eg_DATA_OUT_dly_3 & TX_TOB_eg_DATA_OUT_dly_2;
    TOB_eg_512b_in_i(4) <=   TX_TOB_eg_DATA_OUT_dly_1 & TX_TOB_eg_DATA_OUT_i;
    TOB_eg_512b_in_i(5) <=   TX_TOB_eg_DATA_OUT_dly_3 & TX_TOB_eg_DATA_OUT_dly_2;
    TOB_eg_512b_in_i(6) <=   TX_TOB_eg_DATA_OUT_dly_1 & TX_TOB_eg_DATA_OUT_i;
    TOB_eg_512b_in_i(7) <=   TX_TOB_eg_DATA_OUT_dly_3 & TX_TOB_eg_DATA_OUT_dly_2;
    
    TOB_tau_512b_in_i(0) <=   TX_TOB_eg_DATA_OUT_dly_1 & TX_TOB_eg_DATA_OUT_i;
    TOB_tau_512b_in_i(1) <=   TX_TOB_eg_DATA_OUT_dly_3 & TX_TOB_eg_DATA_OUT_dly_2;
    TOB_tau_512b_in_i(2) <=   TX_TOB_eg_DATA_OUT_dly_1 & TX_TOB_eg_DATA_OUT_i;
    TOB_tau_512b_in_i(3) <=   TX_TOB_eg_DATA_OUT_dly_3 & TX_TOB_eg_DATA_OUT_dly_2;
    TOB_tau_512b_in_i(4) <=   TX_TOB_eg_DATA_OUT_dly_1 & TX_TOB_eg_DATA_OUT_i;
    TOB_tau_512b_in_i(5) <=   TX_TOB_eg_DATA_OUT_dly_3 & TX_TOB_eg_DATA_OUT_dly_2;
    TOB_tau_512b_in_i(6) <=   TX_TOB_eg_DATA_OUT_dly_1 & TX_TOB_eg_DATA_OUT_i;
    TOB_tau_512b_in_i(7) <=   TX_TOB_eg_DATA_OUT_dly_3 & TX_TOB_eg_DATA_OUT_dly_2;

----  TOB/XTOB Readout Test 

U12_TOBs_readout : entity TOB_rdout_lib.TOBs_rdout        -- data arranged from lower numbers to higher as LSB to MSB
    Generic map 
        ( FPGA_NUMBER => 1 )     -- set to pFPGA 1
    Port map ( 
       RST                  => sys_rst ,
       hw_addr              => hw_addr_i ,
       RST_spy_mem_wr_addr  => sys_rst ,
       TOB_FIFO_sw_rst      => sys_rst ,
       -- XTOBs e/g 64b * 8 as input data is 32, we have 32b * 16    
       XTOB_eg_512b_in       => TOB_eg_512b_in_i,
       XTOB_eg_Valid_flg_in  => TOB_eg_Valid_flg_in_i ,
       XTOB_eg_sync_in       => TXCTRL_TOB_eg_OUT_i (0) ,
       -- XTOBs tau 64b * 8 
       XTOB_tau_512b_in       => TOB_tau_512b_in_i,
       XTOB_tau_Valid_flg_in  => TOB_tau_Valid_flg_in_i ,
       XTOB_tau_sync_in       => TXCTRL_TOB_tau_OUT_i (0),          -- TOB tau  sync signal 
       OUT_XTOB_BCN          => BCN_in_i(6 downto 0),     -- sorted XTOB BC_ID with delay through ALGO/sorting block     
       -- TOPO TOBs read out
       TOBs_32b_in          => TX_TOB_topo_DATA_OUT_i  ,       -- TOPO TOBs e/g 32b * 7 is series
       TOBs_sync_in         => TXCTRL_TOB_topo_OUT_i(0)  ,     -- TOPO TOB e/g start signal
       TOBs_valid_flg_in    => rand_out_280M(0)    ,           -- TOPO TOB e/g write signal
       OUT_TOB_BCN           => BCN_in_i(6 downto 0),      -- sorted TOB BC_ID with delay through ALGO/sorting block
       --! TOB Type 0 = em, 1 = tau                        
       TOB_type_in          => '0' ,

       TOB_err_4b_in        => link_err_flg_4b_i ,   -- overall 4-bit link error input into TOB readout
       clk_40M_in           => clk_40M ,  
       clk_200M_in          => clk_200M ,  
       clk_280M_in          => clk_280M ,  
       ipb_clk              => clk_40M , 
       tob_txoutclk         => clk_280M,    -- was clk_160M ,   
       L1A_in               => L1A_in_i,    -- was L1A_in_1_dly ,    -- L1A pulse from the test generator                       
--       L1A_in               => L1A_sw_pulse_i,                       
       BCN_ID_in            => BCN_in_i ,
       L1A_ID_in            => L1A_ID_i ,
       ctrl_TOB_ready_in    => ctrl_TOBs_ready_i ,  
--       rdout_expected       => req_err_rd_raw_i ,        -- i/p RAW readout expected on ERROR                                                                  
       TOB_out_to_MGT_is_char => TOB_out_to_MGT_is_char_i ,
       TOB_out_to_MGT         => TOB_out_to_MGT_i ,
       pre_ld_TOB_wr_addr        => pre_ld_wr_addr_i ,
       pre_ld_eg_XTOB_wr_addr    => pre_ld_wr_addr_i ,
       pre_ld_tau_XTOB_wr_addr   => pre_ld_wr_addr_i ,
       cntr_load_en           => cntr_load_en_i ,
       DPR_locations_to_rd    => DPR_locations_to_rd_i ,
       TOB_data_FIFO_flags    => TOB_data_FIFO_flags_i,
       
       XTOB_eg_FIFO_data_count          =>   XTOB_eg_FIFO_data_count_i,
       XTOB_eg_FIFO_pFULL_THRESH_assert  =>   XTOB_eg_FIFO_pFULL_THRESH_assert_i, --"111111110",
       XTOB_eg_FIFO_pFULL_THRESH_negate  =>   XTOB_eg_FIFO_pFULL_THRESH_negate_i, --"111011110",
       
       XTOB_tau_FIFO_data_count         => XTOB_tau_FIFO_data_count_i,     
       XTOB_tau_FIFO_pFULL_THRESH_assert =>  XTOB_tau_FIFO_pFULL_THRESH_assert_i, -- "111111110",
       XTOB_tau_FIFO_pFULL_THRESH_negate =>  XTOB_tau_FIFO_pFULL_THRESH_negate_i, --"111011110",
       
       T_TOB_FIFO_data_count           =>  T_TOB_FIFO_data_count_i, 
       T_TOBs_FIFO_pFULL_THRESH_assert  =>  T_TOBs_FIFO_pFULL_THRESH_assert_i,  --"111111110",
       T_TOBs_FIFO_pFULL_THRESH_negate  =>  T_TOBs_FIFO_pFULL_THRESH_negate_i,  --"111011110",
       
       Link_output_FIFO_pFULL_THRESH_assert   =>  Link_output_FIFO_pFULL_THRESH_assert_i,  -- "1111111111110",
       Link_output_FIFO_pFULL_THRESH_negate   =>  Link_output_FIFO_pFULL_THRESH_negate_i,  -- "1111011111110" ,
       Link_output_FIFO_rd_data_count        => Link_output_FIFO_rd_data_count_i,
       
       SPY_TOB_mem_wr_addr     => TOB_SPY_mem_wr_addr_i    ,    -- SPY memory wr_addr (read only) 
	   ipbus_out_tob_dpram    =>  ipbus_in_tob_dpram_i,    -- (o/p) signal from TOB SPY DPRAM
       ipbus_in_tob_dpram     =>  ipbus_out_tob_dpram_i       -- (i/p) signal to TOB SPY DPRAM
       );

slave_1: entity TOB_rdout_lib.slave_TOB_readout
  port map(
	 ipb_clk       => clk_40M,  
	 ipb_rst       => sys_rst,  
	 ipb_in        => TOB_IPb_in_i,
	 ipb_out       => TOB_IPb_outI,
	 L1A_ID_Event  => L1A_ID_Event_i,
	 L1A_ID        => L1A_ID_i,			-- output reg
	 BCN_IN        => BCN_in_i,
	 TOB_WR_ADDR_OFFSET   			  => pre_ld_wr_addr_i,			 
	 TOB_SLICES_TO_RD                 => DPR_locations_to_rd_i,                
	 TOB_FIFO_pFULL_THRESH_ASSERT      => T_TOBs_FIFO_pFULL_THRESH_assert_i,     
	 TOB_FIFO_pFULL_THRESH_NEGATE      => T_TOBs_FIFO_pFULL_THRESH_negate_i,     
	 TOB_FIFO_DATA_COUNT              => T_TOB_FIFO_data_count_i,       
	 XTOB_EG_FIFO_pFULL_THRESH_ASSERT  => XTOB_EG_FIFO_pFULL_THRESH_ASSERT_i, 
	 XTOB_EG_FIFO_pFULL_THRESH_NEGATE  => XTOB_EG_FIFO_pFULL_THRESH_NEGATE_i, 
	 XTOB_EG_FIFO_DATA_COUNT          => XTOB_eg_FIFO_data_count_i,        
	 XTOB_TAU_FIFO_pFULL_THRESH_ASSERT => XTOB_TAU_FIFO_pFULL_THRESH_ASSERT_i,
	 XTOB_TAU_FIFO_pFULL_THRESH_NEGATE => XTOB_TAU_FIFO_pFULL_THRESH_NEGATE_i,
	 XTOB_TAU_FIFO_DATA_COUNT         => XTOB_TAU_FIFO_DATA_COUNT_i,
	 TOB_Link_output_FIFO_pFULL_THRESH_ASSERT => Link_output_FIFO_pFULL_THRESH_assert_i,
	 TOB_Link_output_FIFO_pFULL_THRESH_NEGATE => Link_output_FIFO_pFULL_THRESH_negate_i,
	 TOB_LINK_OUTPUT_FIFO_RD_DATA_COUNT      => Link_output_FIFO_rd_data_count_i     ,
	 TOB_data_FIFO_flags        =>  TOB_data_FIFO_flags_i ,
	 SPY_TOB_mem_wr_addr        =>  TOB_SPY_mem_wr_addr_i       , -- TOB/XTOB data SPY memory wr addr pointer 
	 ipbus_out_tob_dpram    =>  ipbus_out_tob_dpram_i,    -- (o/p) signal from TOB SPY DPRAM
     ipbus_in_tob_dpram     =>  ipbus_in_tob_dpram_i       -- (i/p) signal to TOB SPY DPRAM
	 ); 	   

U13_ipbus_TOB_tst : entity work.ipbus_TOB_slave_tst  -- get test data 
--    generic map (N_SLAVES => 2)
    port map (
       ipb_rst     => sys_rst ,     -- i/p
       ipb_clk     => clk_40M ,     -- i/p
       IPb_in      => TOB_IPb_in_i  ,  -- o/p
       IPb_out     => TOB_IPb_outI ,    -- i/p
       L1A_tst_out => L1A_sw_pulse_i  -- o/p
       );

U13_gen_dat : for i in 0 to 48 generate 
--    RAW_data_in_tmp(i) <= link_error_flags_i(i)(2 downto 0) & RAW_data_in_i(i);
    RAW_data_in_tmp(i) <=  RAW_data_in_i(i);
    end generate U13_gen_dat;
    
------  RAW Calorimeter data Readout Test        
U14_RAW_readout : entity TOB_rdout_lib.RAW_data_rdout
    generic map(FPGA_NUMBER => 1)
    Port map ( 
        RST                 => sys_rst   ,
        hw_addr             => hw_addr_i ,    -- i/p
        RST_spy_mem_wr_addr =>  sys_rst ,
        RAW_FIFO_sw_rst     => sys_rst   ,
        RAW_data_in         => RAW_data_in_tmp  ,  -- array 49 x 227b input frames
        clk_280M_in         => clk_280M ,
        clk_40M_in          => clk_40M   ,
        ipb_clk             => clk_40M ,
        ctrl_RAW_ready_in   => ctrl_RAW_ready_i ,               -- control FPGA is ready to take data
        RAW_TXOUTCLK        => clk_280M,    -- was clk_160M ,   must be changed to 280MHz for production
        L1A_in              => L1A_in_i,    -- was L1A_in_1_dly,    -- L1A pulse from the test generator
--        L1A_in           => L1A_sw_pulse_i       ,    -- L1A_sw_pulse_i under s/w control
        BCN_ID_in           => BCN_in_i       ,
        L1A_ID_in           => L1A_ID_i       ,
--        link_error_flags_in  => link_error_flags_i ,    -- array 49 x 4 bit per link
--        rdout_expected       => req_err_rd_raw_i ,   -- i/p Error Readout Expected
        raw_rd_all_in        => raw_rd_all_in_i ,
        pre_ld_wr_addr       => RAW_WR_ADDR_OFFSET_i ,              -- latency pre load for DRP wr address  
        RAW_FIFO_FULL_THRESH_ASSERT   => RAW_FIFO_FULL_THRESH_ASSERT_i, -- RAW FIFO full flag set threshold
        RAW_FIFO_FULL_THRESH_NEGATE   => RAW_FIFO_FULL_THRESH_NEGATE_i, -- RAW FIFO full flag negate threshold
	    RAW_FIFO_data_count           => RAW_FIFO_data_count_i,
        cntr_load_en         => cntr_load_en_i ,  
        RAW_data_FIFO_flags        => RAW_data_FIFO_flags_i ,
        RAW_out_to_MGT_is_char     => RAW_out_to_MGT_is_char_i,           -- Raw data valid to MGT
        RAW_data_out    => RAW_data_out_i ,                 -- raw data 32b data to MGT
        frame_count     => RAW_frame_count_i,
        TTC_read_all_in => TTC_read_all_i,  -- TTC_read_all_in signal input (David Strom bit)
        RAW_FIFO_pFULL_THRESH_ASSERT => RAW_FIFO_pFULL_THRESH_ASSERT_i,
        RAW_FIFO_pFULL_THRESH_NEGATE => RAW_FIFO_pFULL_THRESH_NEGATE_i,
		
		BCN_FIFO_pFULL_THRESH_assert => BCN_FIFO_pFULL_THRESH_assert_i , 
        BCN_FIFO_pFULL_THRESH_negate => BCN_FIFO_pFULL_THRESH_negate_i ,
        --! Link output FIFO before MGT
        Link_output_FIFO_RAW_pfull_thresh_assert  => Link_output_FIFO_RAW_pfull_thresh_assert_i ,   -- Link_output_FIFO
        Link_output_FIFO_RAW_pfull_thresh_negate  => Link_output_FIFO_RAW_pfull_thresh_negate_i ,  -- Link_output_FIFO
        Link_output_FIFO_RAW_rd_data_count => Link_output_FIFO_RAW_rd_data_count_i,         -- occupancy of RAW output link MGT FIFO
		
        SPY_mem_wr_addr   =>  RAW_SPY_mem_wr_addr_i ,     -- SPY memory wr_addr (read only)
	   ipbus_out_raw_dpram    =>   ipbus_in_raw_dpram_i,    -- (o/p) signal from RAW SPY DPRAM
        ipbus_in_raw_dpram    =>  ipbus_out_raw_dpram_i ,      -- (i/p) signal to RAW SPY DPRAM
        link_error_flags      => link_error_flags_54b_i         -- 54-b error flags from the Error Flag FIFO to IPBUS register
        );


--U15_clk_proc_280 : process (clk_280M)
--    begin
--        if clk_280M'event AND clk_280M = '1' then
--            if  ( TXCTRL_TOB_eg_OUT_i(0) = '1' ) then 
--                BCN_in_reg <= BCN_in_i ;
--            end if;
--        end if;
--    end process;
    
    TXCTRL_OUT_i  <= rand_out_40M(8 downto 5) ;    -- 4 bit assignment for link error flag

--U16_clk_proc_40 : process (sys_rst, rand_out_40M, TXCTRL_OUT_i)
--    begin
--        if sys_rst = '1' then
--            link_error_flags_i <= (others => (others => '0'));
--            ii <= 0;
--        else
----            link_error_flags_i <= (others => (others => '0'));
--            link_error_flags_i(to_integer(unsigned(rand_out_40M(4 downto 0))) +to_integer(unsigned(rand_out_40M(2 downto 0)))) <= TXCTRL_OUT_i ;    -- use this as error flag input
--            link_error_flags_i(to_integer(unsigned(rand_out_40M(4 downto 0))) +to_integer(unsigned(rand_out_40M(5 downto 3)))) <= TXCTRL_OUT_i ;    -- use this as error flag input
--            link_error_flags_i(to_integer(unsigned(rand_out_40M(6 downto 3))) +to_integer(unsigned(rand_out_40M(5 downto 3)))) <= TXCTRL_OUT_i ;    -- use this as error flag input
--            link_error_flags_i(to_integer(unsigned(rand_out_40M(9 downto 6))) +to_integer(unsigned(rand_out_40M(8 downto 6)))) <= TXCTRL_OUT_i ;    -- use this as error flag input
--            link_error_flags_i(to_integer(unsigned(rand_out_40M(12 downto 9)))+to_integer(unsigned(rand_out_40M(11 downto 9)))) <= TXCTRL_OUT_i ;    -- use this as error flag input
--             -- link_error_flags_i(ii+1) <= link_error_flags_i(ii);
--        end if;
--    end process;
   
        
    TOB_eg_Valid_flg_in_i <= rand_out_280M(7 downto 0) ;    -- 8 bit assignment for TOB e/g valid

    TOB_tau_Valid_flg_in_i <= rand_out_280M(15 downto 8) ;    -- 8 bit assignment for TOB tau valid

--    link_err_flg_4b_i  <= rand_out_280M(11 downto 8) ;    -- 4 bit assignment for link error flag
    
    back_pressure_in_i <= '0' ;         -- for test set to 0


slave_2: entity TOB_rdout_lib.slave_RAW_readout
    port map(
        ipb_clk  => clk_40M,  
        ipb_rst  => sys_rst,  
        ipb_in  => RAW_IPb_in_i,
        ipb_out => RAW_IPb_outI,
        RAW_FIFO_pFULL_THRESH_ASSERT  =>  RAW_FIFO_pFULL_THRESH_ASSERT_i,    -- o/p
        RAW_FIFO_pFULL_THRESH_NEGATE  =>  RAW_FIFO_pFULL_THRESH_NEGATE_i,    -- o/p
        
        BCN_FIFO_pFULL_THRESH_assert    =>    BCN_FIFO_pFULL_THRESH_assert_i ,        -- o/p
        BCN_FIFO_pFULL_THRESH_negate    =>    BCN_FIFO_pFULL_THRESH_negate_i ,     -- o/p
        
        Link_output_FIFO_pFULL_THRESH_ASSERT   =>  Link_output_FIFO_RAW_pfull_thresh_assert_i   ,  -- o/p
        Link_output_FIFO_pFULL_THRESH_NEGATE   =>  Link_output_FIFO_RAW_pfull_thresh_negate_i   ,  -- o/p
        Link_output_FIFO_rd_data_count        =>  Link_output_FIFO_RAW_rd_data_count_i        ,  -- i/p
        RAW_FIFO_FULL_THRESH_ASSERT   => RAW_FIFO_FULL_THRESH_ASSERT_i,
	    RAW_FIFO_FULL_THRESH_NEGATE   => RAW_FIFO_FULL_THRESH_NEGATE_i,
	    RAW_FIFO_data_count           => RAW_FIFO_data_count_i,
        RAW_frame_count        =>  RAW_frame_count_i,
        RAW_data_FIFO_flags    =>  RAW_data_FIFO_flags_i ,
        RAW_WR_ADDR_OFFSET     =>  RAW_WR_ADDR_OFFSET_i,
        SPY_RAW_mem_wr_addr    =>  RAW_SPY_mem_wr_addr_i       , -- calo data SPY memory wr addr pointer 
        ipbus_out_raw_dpram    =>  ipbus_out_raw_dpram_i    ,    -- o/p signal going to RAW SPY DPRAM
        ipbus_in_raw_dpram     =>  ipbus_in_raw_dpram_i ,          -- o/p signal coming from RAW SPY DPRAM
        link_error_flags       =>  link_error_flags_54b_i         -- 54-b error flags from the Error Flag FIFO to IPBUS register
    ); 


U19_ipbus_RAW_tst : entity work.ipbus_RAW_slave_tst  -- get test data 
--    generic map (N_SLAVES => 2)
    port map (
       ipb_rst     => sys_rst ,     -- i/p
       ipb_clk     => clk_40M ,     -- i/p
       IPb_in      => RAW_IPb_in_i  ,  -- o/p
       IPb_out     => RAW_IPb_outI ,    -- i/p
       L1A_tst_out => open              -- o/p as used from the TOB
       );

--U14_L1A_gen_sw_tst : process (clk_40M)
--   begin
--    if clk_40M'event and clk_40M = '1' then
--        if sys_rst = '1' then
--            reg1    <= '0' ;
--            reg2    <= '0' ;
--            L1A_sw_pulse_i    <= '0' ;
--        else
--            reg1    <= RDOUT_PULSE_REG_i(0) ;   -- this is test L1A under s/w control
--            reg2    <= reg1 ;
--            L1A_sw_pulse_i    <= reg1 AND (NOT reg2) ;
--        end if;
--    end if;
--        end process;


end Behavioral;