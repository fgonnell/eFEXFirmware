
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;

ENTITY readout_cfpga_sm IS
   PORT( 
      aurora_tready       : IN     std_logic;
      clk                 : IN     std_logic;
      first               : IN     std_logic;
      payload_data        : IN     std_logic_vector (63 DOWNTO 0);
      payload_last        : IN     std_logic;
      payload_len         : IN     std_logic_vector (15 DOWNTO 0);
      payload_trailer_crc : IN     std_logic_vector (63 DOWNTO 0);
      payload_valid       : IN     std_logic;
      --pkt_rdy             : IN     std_logic;
      reset               : IN     std_logic;
      axi4_m_data         : OUT    std_logic_vector (63 DOWNTO 0) BUS;
      axi4_m_tlast        : OUT    std_logic;
      axi4_m_tvalid       : OUT    std_logic;
      tready_data         : OUT    std_logic;
      crc_reset           : OUT    std_logic
   );


END ENTITY readout_cfpga_sm ;


 
ARCHITECTURE fsm OF readout_cfpga_sm IS

 
 TYPE STATE_TYPE IS (
      idle,
      header,
      payload,
      trailer_wt0,
      wt_tready,
      wt_tready1,
      wt_tready2,
      trailer_wt1,
      trailer
   );
 
   -- Declare current and next state signals
   SIGNAL current_state : STATE_TYPE;

BEGIN

   -----------------------------------------------------------------
   clocked_proc : PROCESS ( 
      clk,
      reset
   )
   -----------------------------------------------------------------
   BEGIN
      IF (reset = '1') THEN
         current_state <= idle;
         -- Default Reset Values
         axi4_m_data <= (others => '0');
         axi4_m_tlast <= '0';
         axi4_m_tvalid <= '0';
         tready_data <= '0';
         crc_reset  <= '0';
      ELSIF (clk'EVENT AND clk = '1') THEN
         -- Default Assignment To Internals and Outputs
         axi4_m_tlast <= '0';
         axi4_m_tvalid <= '0';

         -- Combined Actions
         CASE current_state IS
            WHEN idle => 
               axi4_m_tvalid <= '0' ;
               axi4_m_tlast <= '0' ;
               crc_reset  <= '0';
               tready_data <= aurora_tready ;
               IF (aurora_tready = '1' and first= '1') THEN 
                   axi4_m_data <= payload_data;
                   axi4_m_tvalid <= '1' ;
                  current_state <= header;
               ELSE
                  current_state <= idle;
               END IF;
            WHEN header => 
               axi4_m_data <= payload_data;
               axi4_m_tvalid <= '1' ;
               tready_data <= aurora_tready ;
               IF (aurora_tready = '0') THEN 
                  current_state <= wt_tready;
               ELSE
                  current_state <= payload;
               END IF;
            WHEN payload => 
               axi4_m_data <= payload_data ;
               axi4_m_tvalid <= payload_valid ;
               --axi4_m_tlast <= payload_last ;
               tready_data <= aurora_tready ;
               IF (aurora_tready = '0') THEN 
                  current_state <= wt_tready2;
               ELSIF (aurora_tready = '0' AND payload_last = '1') THEN 
                  current_state <= wt_tready1;
               ELSIF (aurora_tready = '1' AND payload_last = '1') THEN 
                  current_state <= trailer_wt0;
               ELSE
                  current_state <= payload;
               END IF;
            WHEN trailer_wt0 => 
               tready_data <= '0';
               IF (aurora_tready = '1') THEN 
                  current_state <= trailer_wt1;
               ELSE
                  current_state <= trailer_wt0;
               END IF;
            WHEN wt_tready => 
               axi4_m_tvalid <= '0' ;
               tready_data <= aurora_tready ;
               IF (aurora_tready = '1') THEN 
                  current_state <= payload;
               ELSE
                  current_state <= wt_tready;
               END IF;
            WHEN wt_tready1 => 
               axi4_m_tvalid <= '0' ;
               tready_data   <= '0';
               IF (aurora_tready = '1') THEN 
                  current_state <= trailer_wt1;
               ELSE
                  current_state <= wt_tready1;
               END IF;
            WHEN wt_tready2 => 
               axi4_m_tvalid <= '0' ;
               tready_data <= aurora_tready ;
               IF (aurora_tready = '1') THEN 
                  current_state <= payload;
               ELSE
                  current_state <= wt_tready2;
               END IF;
            WHEN trailer_wt1 => 
               tready_data <= '0';
               IF (aurora_tready = '1') THEN 
                  current_state <= trailer;
               ELSE
                  current_state <= trailer_wt1;
               END IF;
            WHEN trailer => 
               axi4_m_data <= payload_trailer_crc ;
               axi4_m_tvalid <= '1' ;
               axi4_m_tlast <= '1' ;
               tready_data <= '0' ;
               crc_reset  <= '1';
               current_state <= idle;
            WHEN OTHERS =>
               current_state <= idle;
         END CASE;
      END IF;
   END PROCESS clocked_proc;
 
 
END ARCHITECTURE fsm;
