----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/29/2016 05:11:54 PM
-- Design Name: 
-- Module Name: module_status - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library ipbus_lib;
use ipbus_lib.ipbus.all;


entity module_status is
 generic (addr_width :natural :=1);   
port(
        
		
		ipbus_in   : in ipb_wbus;
		ipbus_out  : out ipb_rbus;
        status  : in std_logic_vector(31 downto 0)
         
	);
	

end module_status;

architecture Behavioral of module_status is

begin

 ipbus_out.ipb_rdata <= status; 
 ipbus_out.ipb_ack <= ipbus_in.ipb_strobe;
 ipbus_out.ipb_err <= '0';


      

       
end Behavioral;










