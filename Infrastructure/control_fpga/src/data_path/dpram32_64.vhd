---------------------------------------------------------------------------------
--
--   Copyright 2017 - Rutherford Appleton Laboratory and University of Bristol
--
--   Licensed under the Apache License, Version 2.0 (the "License");
--   you may not use this file except in compliance with the License.
--   You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--   Unless required by applicable law or agreed to in writing, software
--   distributed under the License is distributed on an "AS IS" BASIS,
--   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--   See the License for the specific language governing permissions and
--   limitations under the License.
--
--                                     - - -
--
--   Additional information about ipbus-firmare and the list of ipbus-firmware
--   contacts are available at
--
--       https://ipbus.web.cern.ch/ipbus
--
---------------------------------------------------------------------------------


-- dpram32_64
--
-- Based on ipbus_ported_dpram36
--
-- Generic 32b/64b wide dual-port memory
-- Note that this takes up *twice* the 32b address space indicated by ADDR_WIDTH
--
-- Should lead to an inferred block RAM in Xilinx parts with modern tools
--
-- Dave Newbold, October 2013, Dave Sankey August 2019

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity dpram32_64 is
	generic(
		ADDR_WIDTH: positive := 12
	);
	port(
		clk32: in std_logic;
		we32: in std_logic := '0';
		d32: in std_logic_vector(31 downto 0);
		q32: out std_logic_vector(31 downto 0);
		addr32: in std_logic_vector(ADDR_WIDTH downto 0);
		clk64: in std_logic;
		we64: in std_logic := '0';
		d64: in std_logic_vector(63 downto 0);
		q64: out std_logic_vector(63 downto 0);
		addr64: in std_logic_vector(ADDR_WIDTH - 1 downto 0)
	);

end dpram32_64;

architecture rtl of dpram32_64 is

	type ram_array is array(2 ** ADDR_WIDTH - 1 downto 0) of std_logic_vector(31 downto 0);
	shared variable ram_l, ram_h: ram_array := (others => (others => '0'));
	signal sel32, sel64: integer range 0 to 2 ** ADDR_WIDTH - 1 := 0;

begin

	sel32 <= to_integer(unsigned(addr32(ADDR_WIDTH downto 1)));

	process(clk32)
	begin
		if rising_edge(clk32) then
			if addr32(0) = '1' then
				q32 <= ram_h(sel32); -- Order of statements is important to infer read-first RAM!
				if we32 = '1' then
					ram_h(sel32) := d32;
				end if;
			else
				q32 <= ram_l(sel32); -- Order of statements is important to infer read-first RAM!
				if we32 = '1' then
					ram_l(sel32) := d32;
				end if;
			end if;
		end if;
	end process;

	sel64 <= to_integer(unsigned(addr64));

	process(clk64)
	begin
		if rising_edge(clk64) then
			q64 <= ram_h(sel64) & ram_l(sel64); -- Order of statements is important to infer read-first RAM!
			if we64 = '1' then
				ram_l(sel64) := d64(31 downto 0);
				ram_h(sel64) := d64(63 downto 32);
			end if;
		end if;
	end process;

end rtl;
