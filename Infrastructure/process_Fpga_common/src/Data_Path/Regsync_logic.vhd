


-- Example/Test VHDL created by IPB 16/12/16
-- Intended to show how EfexDataFormats should be used, and allow me to test VHDL



library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;


  entity RegSyncLogic is
    port (
      DataIn:  in  std_logic_vector(226 downto 0);
      DataOut: out std_logic_vector(226 downto 0)
     -- clk:     in  std_logic
    );
  end RegSyncLogic;
  
----------------------------------------------------------------------------------------------------
Architecture struct of RegSyncLogic is
----------------------------------------------------------------------------------------------------
-- Nothing much here....      
      
----------------------------------------------------------------------------------------------------
Begin
DataOut <= DataIn;

--proc: process (clk)
--  begin
--    --if (clk'event and clk = '1') then
--      DataOut <= DataIn;
--    --end if;
--  end process;
  
End struct;











