

-- Dave Sankey, June 2020

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ila_0 is
	PORT (
		clk : IN STD_LOGIC;
		probe0 : IN STD_LOGIC_VECTOR(67 DOWNTO 0)
	);
END entity ila_0;

architecture simple of ila_0 is

signal probe_sig: STD_LOGIC_VECTOR(67 DOWNTO 0);

Begin

ila_process: process(clk)
  begin
    if rising_edge(clk) then
    	probe_sig <= probe0;
    end if;
  end process ila_process;

End architecture simple;
