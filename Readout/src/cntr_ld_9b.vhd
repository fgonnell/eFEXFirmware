--! @file
--! @brief Loadable counter
--! @details 
--! This is a 9b loadable counter.
--! It is used to generate the read/write address for the DPRAM of the TOB Sorting block
--! The address counter can be pre-loaded with an offset to compensate for the delay between arrival of TOB data and L1A signal
--! @author Saeed Taghavi


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_arith.all;

--! @copydoc cntr_ld_9b.vhd
entity cntr_ld_9b is

    Port ( 
       CE   : in STD_LOGIC;
       CLK  : in STD_LOGIC;
       RST  : in STD_LOGIC;
       LD   : in STD_LOGIC;
       LD_VAL : in STD_LOGIC_VECTOR (8 downto 0);
       Q      : out STD_LOGIC_VECTOR (8 downto 0)
       );
    end cntr_ld_9b;

--! @copydoc cntr_ld_9b.vhd
architecture Behavioral of cntr_ld_9b is

begin
process (CLK)
	variable temp : unsigned (8 downto 0);
	begin
	if CLK'event AND CLK = '1' then
     	if RST = '1' OR LD = '1' then           -- this is a synchronous reset or load
      		temp := unsigned(LD_VAL);           -- set counter to offset value given.
		elsif CE = '1' then
		      temp := temp + 1;
		end if;
	end if;
	Q <= std_logic_vector(temp) ;	 -- push out new value
    end process;


end Behavioral;
