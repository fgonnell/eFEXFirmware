----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 6/28/2018 11:20:02 AM
-- Design Name: 
-- Module Name: lib_registers - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
library ipbus_lib;
use ipbus_lib.ipbus.all;
use ipbus_lib.ipbus_reg_types.all;
library infrastructure_lib;
use infrastructure_lib.ipbus_decode_efex_LibVersion.all;

entity lib_registers is
    
port(
         ipb_clk         :in std_logic;
         ipb_rst         :in std_logic;
	     ipb_in          :in ipb_wbus;
	     ipb_out         :out ipb_rbus;
	     officail_version:in std_logic_vector(31 downto 0);
         hog_gitsha      :in std_logic_vector(31 downto 0);
         top_version     :in std_logic_vector(31 downto 0);
         top_gitsha      :in std_logic_vector(31 downto 0);
         infra_version    :in std_logic_vector(31 downto 0);
         infra_gitsha    :in std_logic_vector(31 downto 0);
         algo_version    :in std_logic_vector(31 downto 0);
		 algo_gitsha     :in std_logic_vector(31 downto 0);
		 readout_version :in std_logic_vector(31 downto 0);
		 readout_gitsha  :in std_logic_vector(31 downto 0);
		 ipbus_gitsha    :in std_logic_vector(31 downto 0)
         
	);
	

end lib_registers;

architecture Behavioral of lib_registers is

signal sel:integer;
signal ack: std_logic;
signal ipbw: ipb_wbus_array(N_SLAVES-1 downto 0);
signal ipbr, ipbr_d: ipb_rbus_array(N_SLAVES-1 downto 0);
signal official_reg,top_reg,infra_reg,algo_reg,readout_reg : ipb_reg_v(1 downto 0) := (others => (others => '0'));
signal ipb_lib_reg : ipb_reg_v(0 downto 0) := (others => (others => '0'));

begin

official_reg(0) <= officail_version;
official_reg(1) <= hog_Gitsha;
top_reg(0)      <= top_version;
top_reg(1)      <= top_Gitsha;
infra_reg(0)    <= infra_version;
infra_reg(1)    <= infra_Gitsha;
algo_reg(0)     <= algo_version;
algo_reg(1)     <= algo_Gitsha;
readout_reg(0)  <= readout_version;
readout_reg(1)  <= readout_Gitsha;
ipb_lib_reg(0)  <= ipbus_Gitsha;


fabric_lib_version: entity ipbus_lib.ipbus_fabric_sel
    generic map(NSLV => N_SLAVES, --defined in ipbus_decode_efex_libVersion
	             SEL_WIDTH => ipbus_sel_width)
          port map(
             ipb_in => ipb_in,
             ipb_out => ipb_out,
             sel => ipbus_sel_efex_LibVersion (ipb_in.ipb_addr),
             ipb_to_slaves => ipbw,
             ipb_from_slaves => ipbr
           ); 


hdl_respository:entity ipbus_lib.ipbus_ctrlreg_v
               generic map (                                        
                 N_CTRL => 0,  --number of control reg         
                 N_STAT => 2)  --number of status reg          
               port map (                                           
                 clk       => ipb_clk,                              
                 reset     => ipb_rst,                              
                 ipbus_in  => ipbw(N_SLV_HDLREPOSITORY),  
                 ipbus_out => ipbr(N_SLV_HDLREPOSITORY),
                 d         => official_reg ,              -- assign offficial registers                
                 q         => open,                             
                -- qmask     => (others => '0'),                      
                 stb       => open);            
	
top_directory:entity ipbus_lib.ipbus_ctrlreg_v
	         generic map (                                        
               N_CTRL => 0,  --number of control reg         
                N_STAT => 2)  --number of status reg          
             port map (                                           
                clk       => ipb_clk,                              
                reset     => ipb_rst,                              
                ipbus_in  => ipbw( N_SLV_TOPDIRECTORY),   
                ipbus_out => ipbr( N_SLV_TOPDIRECTORY),
                d         => top_reg,                   -- assign top registers          
                q         => open,                             
               -- qmask     => (others => '0'),                      
                stb       => open);                                
                                                         
infra_lib:entity ipbus_lib.ipbus_ctrlreg_v
          generic map (                                        
            N_CTRL => 0,  --number of control reg         
            N_STAT => 2)  --number of status reg          
          port map (                                           
            clk       => ipb_clk,                              
            reset     => ipb_rst,                              
            ipbus_in  => ipbw(N_SLV_INFRASTRUCTURE),  
            ipbus_out => ipbr(N_SLV_INFRASTRUCTURE),
            d         => infra_reg,                -- assign infrastructure library registers            
            q         => open,                             
           -- qmask     => (others => '0'),                      
            stb       => open);         	
	
algo_lib:entity ipbus_lib.ipbus_ctrlreg_v
                      generic map (                                        
                        N_CTRL => 0,  --number of control reg         
                        N_STAT => 2)  --number of status reg          
                      port map (                                           
                        clk       => ipb_clk,                              
                        reset     => ipb_rst,                              
                        ipbus_in  => ipbw(N_SLV_ALGORITHM),  
                        ipbus_out => ipbr(N_SLV_ALGORITHM),
                        d         => algo_reg,               --   assign algorithm library registers            
                        q         => open,                             
                       -- qmask     => (others => '0'),                      
                        stb       => open);   	
	
	
readout_lib:entity ipbus_lib.ipbus_ctrlreg_v
                      generic map (                                        
                        N_CTRL => 0,  --number of control reg         
                        N_STAT => 2)  --number of status reg          
                      port map (                                           
                        clk       => ipb_clk,                              
                        reset     => ipb_rst,                              
                        ipbus_in  => ipbw(N_SLV_READOUT),  
                        ipbus_out => ipbr(N_SLV_READOUT),
                        d         => readout_reg,               -- assign readout library registers             
                        q         => open,                             
                       -- qmask     => (others => '0'),                      
                        stb       => open);	
	
ipbus_a:entity ipbus_lib.ipbus_ctrlreg_v
                      generic map (                                        
                        N_CTRL => 0,  --number of control reg         
                        N_STAT => 1)  --number of status reg          
                      port map (                                           
                        clk       => ipb_clk,                              
                        reset     => ipb_rst,                              
                        ipbus_in  => ipbw(N_SLV_IPBUSSHA),  
                        ipbus_out => ipbr(N_SLV_IPBUSSHA),
                        d         => ipb_lib_reg,               -- assign ipbus library registers               
                        q         => open,                             
                       -- qmask     => (others => '0'),                      
                        stb       => open);	
	
	
end Behavioral;	
	
	
	
	
	
	
	
	 


